﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using Newtonsoft.Json;
using LightYear.JunFuTrans.Mappers.StationWithArea;
using LightYear.JunFuTrans.Mappers.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;
using System.Net;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using System.IO.Compression;
using Microsoft.EntityFrameworkCore;
using LightYear.JunFuTrans.DA.Repositories.ScheduleTaskRepo;
using LightYear.JunFuTrans.DA.Repositories.SignImageTransfer;
using System.Threading;

namespace SignPaperPackage
{
    public class Program
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            return config;
        }
        static IConfiguration configs = Configs();
        static string ConnectionStringsJunFuDbContext = configs["ConnectionStrings:JunFuDbContext"];
        static DbContextOptions<JunFuDbContext> options = new DbContextOptionsBuilder<JunFuDbContext>().UseSqlServer(ConnectionStringsJunFuDbContext).Options;
        static JunFuDbContext junFuDbContext = new JunFuDbContext(options);
        static IScheduleTaskRepository scheduleTaskRepo = new ScheduleTaskRepository(junFuDbContext);
        static ISignImageTransferLogRepository signImageTransferLogRepo = new SignImageTransferLogRepository(junFuDbContext);

        static void Main(string[] args)
        {
            var task = scheduleTaskRepo.GetSignImageJob();
            if (task == null)
            {
                return;
            }
            var latestUploadTime = task.StartTime;
            var uploadDate = latestUploadTime;
            var dataSource = "App";
            string sourcePath = configs["SourcePath"];
            string destPath = configs["UploadPath"];
            var dd = DateTime.Parse("2021-11-11 18:14:00");
            List<string> files = new List<string>(Directory.GetFiles(sourcePath).Where(p => new FileInfo(p).CreationTime > latestUploadTime && new FileInfo(p).Extension == ".jpg").OrderBy(p => new FileInfo(p).CreationTime));
            
            //var dirList = new DirectoryInfo(dataSource);
            //var Files = dirList.GetFiles("*.jpg");


            //var aaa = Directory.GetFiles(sourcePath);


            //string sourceFile = Path.Combine(sourcePath, fileName);

            //string destFile = Path.Combine(targetPath, fileName);
            if (!Directory.Exists(destPath))
            {
                try
                {
                    Directory.CreateDirectory(destPath);
                }
                catch (Exception ex)
                {
                    signImageTransferLogRepo.Insert(new SignImageTransferLog
                    {
                        DataSource = dataSource,
                        Status = "NoDirection",
                        Message = "建立目標目錄失敗"
                    });
                    return;
                }
            }
            List<DateTime> tt = new List<DateTime>();
            files.ForEach(c =>
            {
                //tt.Add( File.GetCreationTime(c));
                string destFile = Path.Combine(new string[] { destPath, Path.GetFileName(c) });
                //覆蓋模式
                //if (File.Exists(destFile))
                //{
                //    File.Delete(destFile);
                //}
                //File.Copy(c, destFile);
                string folders = Path.GetFileNameWithoutExtension(c);
                string fileName = Path.GetFileName(c);
                int folderDeepth = folders.Length / 5;
                var sourSubPath = destPath;
                for (var d = 0; d < folderDeepth; d++)
                {
                    sourSubPath += "\\" + folders.Substring(5 * d, 5);
                    if (!Directory.Exists(sourSubPath))
                    {
                        Directory.CreateDirectory(sourSubPath);
                    }

                }
                //Console.WriteLine($@"正在轉移 {c} 至 {destPath}...");
                if (!File.Exists(Path.Combine(sourSubPath, fileName)))
                {
                    try
                    {
                        File.Copy(c, Path.Combine(sourSubPath, fileName));
                        signImageTransferLogRepo.Insert(new SignImageTransferLog
                        {
                            DataSource = dataSource,
                            SourcePath = c,
                            DestPath = Path.Combine(sourSubPath, fileName),
                            Status = "Success"
                        });
                    }
                    catch (Exception e)
                    {
                        signImageTransferLogRepo.Insert(new SignImageTransferLog
                        {
                            DataSource = dataSource,
                            SourcePath = c,
                            DestPath = Path.Combine(sourSubPath, fileName),
                            Status = "Fail",
                            Message = e.Message
                        });
                    }

                }
                else
                {
                    signImageTransferLogRepo.Insert(new SignImageTransferLog
                    {
                        DataSource = dataSource,
                        SourcePath = c,
                        DestPath = Path.Combine(sourSubPath, fileName),
                        Status = "Fail",
                        Message = "檔案已存在"
                    });
                }
                uploadDate = File.GetCreationTime(c);
            });
            //Console.WriteLine(latestUploadTime.ToString("hh:mm:ss.fffff"));
            //Console.WriteLine(uploadDate.ToString("hh:mm:ss.fffff"));
            task.StartTime = uploadDate;
            try
            {
                scheduleTaskRepo.Update(task);
            }
            catch (Exception)
            {
                throw;
            }
            //Console.ReadKey();
            //Thread.Sleep(10000);
        }
    }
}
