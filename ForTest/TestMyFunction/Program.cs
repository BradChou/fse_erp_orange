﻿using System;
using System.Collections.Generic;
using LightYear.JunFuTrans.DA.LightYearTest;
using LightYear.JunFuTrans.DA.Repositories.StudentTest;
using LightYear.JunFuTrans.BL.Services.StudentTest;
using AutoMapper;
using LightYear.JunFuTrans.Mappers.Students;
using LightYear.JunFuTrans.BL.BE.StudentTest;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using System.Security.Policy;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using System.Globalization;

namespace TestMyFunction
{
    class Program
    {
        static void Main(string[] args)
        {
            //string address = "桃園縣觀音鄉忠愛路愛一巷６３弄２６衖４號";
            //string address = "民生東路一段131號3樓305室";

            //FSEAddress fSEAddress = new FSEAddress(address);

            //string result = JsonConvert.SerializeObject(fSEAddress);

            //Console.WriteLine(result);

            JunFuDbContext junFuDbContext = new JunFuDbContext();

            JunFuTransDbContext junFuTransDbContext = new JunFuTransDbContext();

            DeliveryRequestRepository deliveryRequestRepository = new DeliveryRequestRepository(junFuDbContext, junFuTransDbContext);

            OrgAreaRepository orgAreaRepository = new OrgAreaRepository(junFuDbContext, junFuTransDbContext);

            var deliveryRequestList = deliveryRequestRepository.GetAllRequestBetweenDates(DateTime.Now.AddDays(-1), DateTime.Now);

            Console.WriteLine(string.Format("總數：{0}", deliveryRequestList.Count()));

            foreach(TcDeliveryRequest tcDeliveryRequest in deliveryRequestList)
            {
                string data = "";

                if (tcDeliveryRequest.CheckNumber == "880000337814")
                {
                    data = "do";
                }

                FSEAddressEntity fSEAddress = new FSEAddressEntity(tcDeliveryRequest.ReceiveAddress, tcDeliveryRequest.ReceiveCity, tcDeliveryRequest.ReceiveArea);

                string result = JsonConvert.SerializeObject(fSEAddress);

                List<OrgArea> getOrgAreas = orgAreaRepository.GetByFSEAddressEntity(fSEAddress);

                if (getOrgAreas.Count == 0)
                {
                    //Console.WriteLine(string.Format("{0},{1},{2}", tcDeliveryRequest.CheckNumber, tcDeliveryRequest.ReceiveAddress,result));
                }
                else if(getOrgAreas.Count > 1)
                {
                    Console.WriteLine(string.Format("{0}-->{1}", tcDeliveryRequest.CheckNumber, tcDeliveryRequest.ReceiveAddress));

                    foreach(OrgArea orgArea in getOrgAreas)
                    {
                        string eachResult = JsonConvert.SerializeObject(orgArea);

                        Console.WriteLine(eachResult);
                    }
                }
                else
                {
                    Console.WriteLine(string.Format("{0}-->{1}", tcDeliveryRequest.CheckNumber, tcDeliveryRequest.ReceiveAddress));

                    foreach (OrgArea orgArea in getOrgAreas)
                    {
                        string eachResult = JsonConvert.SerializeObject(orgArea);

                        Console.WriteLine(eachResult);
                    }
                }


                //if( fSEAddress.FindNo.Length == 0 && tcDeliveryRequest.CheckNumber.Length > 0)
                //{
                //    Console.WriteLine(tcDeliveryRequest.CheckNumber);
                //    Console.WriteLine(result);
                //}
                //else
                //{
                //    //Console.WriteLine(string.Format("{0},{3},{4},{1},{2}", tcDeliveryRequest.CheckNumber, fSEAddress.FindNo, fSEAddress.OrgAddress, fSEAddress.Road, fSEAddress.Section));
                //}

                //Console.WriteLine(result);
            }

            //string checkNumber = "8440893005";

            //double collectionMoney = deliveryRequestRepository.GetCollectionMoney(checkNumber);

            //Console.WriteLine(string.Format("{0} collection money is {1}", checkNumber, collectionMoney));

            /* //在command line 測試寫入測試資料表
            var config = new MapperConfiguration(cfg => { cfg.AddProfile<StudentMappingProfile>(); });

            var mapper = config.CreateMapper();

            Console.WriteLine("Hello World!");

            var rand = new Random();

            int myTest = rand.Next(1000, 9999);

            int left = myTest % 100;

            Student student = new Student();

            student.Name = string.Format("Max Test{0}" , myTest);

            student.Age = left;

            student.Sex = (left % 2);

            LightYearTestDbContext lightYearTestDbContext = new LightYearTestDbContext();

            StudentRepository studentRepository = new StudentRepository(lightYearTestDbContext);

            int studentId =  studentRepository.Insert(student);

            Console.WriteLine(string.Format("inserted id is {0}", studentId));

            StudentService studentService = new StudentService(studentRepository, mapper);

            List<PersonEntity> students = studentService.GetAllStudents();

            foreach (PersonEntity student1 in students)
            {
                Console.WriteLine(string.Format("student name is {0}", student1.Name));
            }
            */
        }
    }
}
