﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.Model
{
	public class CBMDetailModel
	{
		public int S060 { get; set; }
		public int S090 { get; set; }
		public int S110 { get; set; }
		public int S120 { get; set; }
		public int S150 { get; set; }
		public string CbmSize { get; set; }

	}
}
