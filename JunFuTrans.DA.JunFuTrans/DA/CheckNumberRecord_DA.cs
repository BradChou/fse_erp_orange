﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;


namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class CheckNumberRecord_DA
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
        }

        IConfiguration configs = Configs();
     

        public void Insertckeck_number_record(CheckNumberRecord _condition)
        {
           
            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"INSERT INTO [checknumber_record_for_bags_and_boxes](
customer_code,
total_count,
pieces_count,
used_count,
remain_count,
function_flag,
update_date,
adjustment_reason,
check_number

) VALUES (

@customer_code,
@total_count,
@pieces_count,
@used_count,
@remain_count,
@function_flag,
@update_date,
@adjustment_reason,
@check_number
)"; 
                cn.Execute(sql, _condition);
            }

        }
    }
}
