﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;


namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class ShopeeException_DA
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
        }

        IConfiguration configs = Configs();
     

        public void InsertShopeeException(ShopeeException _condition)
        {
           
            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"INSERT INTO [shopee_exception](

exception,
cdate,
json,
order_number,
send_address,
receive_address,
send_station,
delivery_station,
customer_code,
start_time,
end_time

) VALUES (


@exception,
@cdate,
@json,
@order_number,
@send_address,
@receive_address,
@send_station,
@delivery_station,
@customer_code,
@start_time,
@end_time


)";
                cn.Execute(sql, _condition);
            }

        }


    }
}
