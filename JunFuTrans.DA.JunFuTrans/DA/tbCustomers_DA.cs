﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class tbCustomers_DA
    {
        private static IConfiguration Configs()
        { 
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch 
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
            
            
        }
        IConfiguration configs = Configs();


        public tbCustomers_Condition GetCustomerProductType(string customer_code)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql =
                    @"  SELECT product_type,is_new_customer
                        FROM [dbo].[tbCustomers] WITH (NOLOCK)
                        WHERE customer_code = @customer_code
                    ";
                return cn.QueryFirstOrDefault<tbCustomers_Condition>(sql, new { customer_code });
                
            }

        }


    }
}
