﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;


namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class CBMDetailLog_DA
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
        }

        IConfiguration configs = Configs();
     

        public void InsertCBMDetailLog(CBMDetailLog_Condition _condition)
        {
           
            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"INSERT INTO [CBMDetailLog](

                    ComeFrom,
                    CheckNumber,
                    CBM,
                    Length,
                    Width,
                    Height,
                    CreateDate,
                    CreateUser

                    ) VALUES (


                    @ComeFrom,
                    @CheckNumber,
                    @CBM,
                    @Length,
                    @Width,
                    @Height,
                    @CreateDate,
                    @CreateUser

                    )";
                cn.Execute(sql, _condition);
            }

        }


    }
}
