﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;


namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class PickUppImportRecord_DA
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
        }

        IConfiguration configs = Configs();
     

        public void InsertRecord(PickUppImportRecord _condition)
        {
           
            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"INSERT INTO [PickUppImportRecord](
[AccountCode],
[Status],
[CreateDate]


) VALUES (

@AccountCode,
@Status,
getdate()

)"; 
                cn.Execute(sql, _condition);
            }

        }

        public string GetRecord(string AccountCode)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"
select top 1 status from  [PickUppImportRecord]
where 
CreateDate>CONVERT(varchar(100), GETDATE(), 23) 
and AccountCode=@AccountCode
order by id desc

";

                return cn.QueryFirstOrDefault<string>(sql, new { AccountCode });

            }

        }
    }
}
