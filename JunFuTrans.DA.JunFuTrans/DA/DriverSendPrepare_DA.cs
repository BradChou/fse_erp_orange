﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class DriverSendPrepare_DA
    {
     

        public List<AppNotify> GetNotifyData()
        {

            using (var cn = new SqlConnection("Server=172.30.1.34,1433;Database=JunFuReal;User Id=lyAdmin;Password=P@ssw0rd!!!;Max Pool Size=9000"))
            {
                string sql = @"
select work_Station_Scode,work_Send_code,work_Delivery_Code, T1.driver_code, number

from (select driver_code,count(*) number from DriverSendPrepare
where (cdate> dateadd(MINUTE,-10,getdate()) or assign_date> dateadd(MINUTE,-10,getdate()))
and is_cancel='0'
and is_collect=' 0'
and target_date=format( getdate() , 'yyyyMMdd' ) 

group by driver_code) T1
left join tbDrivers d on T1.driver_code=d.driver_code

";

                return cn.Query<AppNotify>(sql, new { }).AsList();

            }

        }
    }
}
