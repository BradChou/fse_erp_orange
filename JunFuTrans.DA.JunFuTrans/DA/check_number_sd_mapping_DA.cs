﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class check_number_sd_mapping_DA
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }


        }
        IConfiguration configs = Configs();


        public check_number_sd_mapping_Condition CheckSDMD(string check_number)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuTransDbContext"]))
            {
                string sql =
                    @"  SELECT check_number,*
                        FROM [dbo].[check_number_sd_mapping] WITH (NOLOCK)
                        WHERE check_number = @check_number
                    ";
                return cn.QueryFirstOrDefault<check_number_sd_mapping_Condition>(sql, new { check_number });
            }

        }

    
    }
}
