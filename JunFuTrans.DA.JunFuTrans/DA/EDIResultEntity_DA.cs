﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class EDIResultEntity_DA
    {
        private static IConfiguration Configs()
        { 
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch 
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
            
            
        }
        IConfiguration configs = Configs();


        public void InsertEDIResult(string CheckNumber, bool Result, string Msg,string OrderNumber,string Sd,string Md,string Putorder,string Area,string  AreaId, DateTime Cdate)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"Insert into EDIResultEntity
                (CheckNumber,Result,Msg,OrderNumber,Sd,Md,Putorder,Area,AreaId,Cdate)
                values
                (@CheckNumber,@Result,@Msg,@OrderNumber,@Sd,@Md,@Putorder,@Area,@AreaId,@Cdate)

                ";
                cn.Execute(sql, new { CheckNumber, Result, Msg, OrderNumber, Sd , Md, Putorder, Area, AreaId, Cdate });
            }

        }


    }
}
