﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class shopee_cbminfo_DA
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("app.json").Build();
            return config;
        }
        IConfiguration configs = Configs();
     

        public void Insertshopee_cbminfo(List<ShopeeCbminfo> _condition)
        {
           
            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuTransDbContext"]))
            {
                string sql = @"INSERT INTO [shopee_cbminfo_bak1005]
                (
                 cbm,
                 check_number,
                 create_datetime,
                 filename,
                 height,
                 length,
                 Measurement_Time,
                 shopee_cbminfo_number,
                 sidesSum,
                 size,
                 update_datetime,
                 upload_date,
                 weight,                 
                 width 
                 ) 
                VALUES 
                (
                 @cbm,
                 @checknumber,
                 @createdatetime,
                 @filename,
                 @height,
                 @length,
                 @MeasurementTime,
                 @shopeecbminfonumber,
                 @sidesSum,
                 @size,
                 @updatedatetime,
                 @uploaddate,
                 @weight,                 
                 @width  )"; 
                cn.Execute(sql, _condition);
            }

        }
    }
}
