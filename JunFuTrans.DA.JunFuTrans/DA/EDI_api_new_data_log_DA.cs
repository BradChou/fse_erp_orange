﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class EDI_api_new_data_log_DA
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            return config;
        }
        IConfiguration configs = Configs();
     

        public void InsertEDI_api_log(EDI_api_new_data_log_Condition _condition)
        {
           
            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuTransDbContext"]))
            {
                string sql = @"INSERT INTO [ediapi_new_data_log](result,type,action,message,cdate,customer_code,check_number) VALUES (@result,@type,@action,@message,@cdate,@customer_code,@check_number)"; 
                cn.Execute(sql, _condition);
            }

        }

        public void InsertAPIorigin(string customer_code,string origin)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"INSERT INTO [APIorigin](customer_code,origin,cdate) VALUES ('"+customer_code+ "','"+origin+"',GETDATE())";
                cn.Execute(sql);
            }

        }
    }
}
