﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;


namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class ShoppeeApiProblem_DA
    {
        private static IConfiguration Configs()
        {
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
        }

        IConfiguration configs = Configs();
     

        public void InsertShoppeeApiProblem(ShoppeeApiProblem ShoppeeApiProblem)
        {
           
            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"INSERT INTO [ShoppeeApiProblem](


step,
CheckNumber,
OrderNumber,
Exception,
Type,
Cdate

) VALUES (



@step,
@CheckNumber,
@OrderNumber,
@Exception,
@Type,
@Cdate


)";
                cn.Execute(sql, ShoppeeApiProblem);
            }

        }


    }
}
