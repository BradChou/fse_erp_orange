﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class ApiAddressParseLog_DA
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            return config;
        }
        IConfiguration configs = Configs();
     

        public void InsertApiAddressParseLog(ApiAddressParseLog_Condition _condition)
        {
           
            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuTransDbContext"]))
            {
                string sql = @"INSERT INTO [ApiAddressParseLog](check_number,send_station,api_type,customer_code,order_number,other_param,receive_address,receive_station,send_address,result_message,exception_message,source_data,start_time,end_time) VALUES (@check_number,@send_station, @api_type,@customer_code,@order_number,@other_param,@receive_address,@receive_station,@send_address,@result_message,@exception_message,@source_data,@start_time,@end_time)"; 
                cn.Execute(sql, _condition);
            }

        }
    }
}
