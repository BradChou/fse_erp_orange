﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using JunFuTrans.DA.JunFuTrans.Model;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class CBMDetail_DA
    {
        private static IConfiguration Configs()
        { 
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch 
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
            
            
        }
        IConfiguration configs = Configs();
     

        public CBMDetailModel GetCbmBycheck_number(string check_number)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql =
                    @"  Select 
IIF(ISNULL([袋裝],0)>0,'袋裝:'+ CAST([袋裝] as varchar(10)),'') +' '+
IIF(ISNULL([S060],0)>0,'S060:'+ CAST([S060] as varchar(10)),'') +' '+
IIF(ISNULL([S090],0)>0,'S090:'+ CAST([S090] as varchar(10)),'') +' '+
IIF(ISNULL([S110],0)>0,'S110:'+ CAST([S110] as varchar(10)),'') +' '+
IIF(ISNULL([S110],0)>0,'S120:'+ CAST([S120] as varchar(10)),'') +' '+
IIF(ISNULL([S110],0)>0,'S150:'+ CAST([S150] as varchar(10)),'') as 'CbmSize'

From 
(
		SELECT	check_number ,[袋裝], [S060] , [S090] , [S110] , [S120] , [S150]
		FROM (
			--SELECT l.MasterLangId, l.LangType, l.ShowText
			--FROM dbo.Lang l
				select check_number,Cbm,count(*) as c
				from
				(
				select 
				CheckNumber as check_number,
				case　when ComeFrom = 4 then '袋裝'   
				　　when ISNULL(Length + Width + Height,0) < 1 then 'S090'
					 when ISNULL(Length + Width + Height,0) < 650 then 'S060'
				 when ISNULL(Length + Width + Height,0)  < 950 then 'S090'
				 when ISNULL(Length + Width + Height,0) <1150 then 'S110'
				 when ISNULL(Length + Width + Height,0) <1250 then 'S120'
				 else 'S150'
				 end as Cbm
				from CBMDetail With(nolock)
				where CheckNumber = @check_number
				)  a
				group by check_number,Cbm
		) t 
		PIVOT (
			-- 設定彙總欄位及方式
			sum(c) 
			-- 設定轉置欄位，並指定轉置欄位中需彙總的條件值作為新欄位
			FOR Cbm IN ([袋裝],[S090] , [S060] , [S110] , [S120] , [S150] )
		) p
) t
                    "; 
               return cn.QueryFirstOrDefault<CBMDetailModel>(sql, new { check_number });
            }

        }

    }
}
