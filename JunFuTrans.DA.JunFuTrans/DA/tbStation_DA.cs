﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class tbStation_DA
    {
        private static IConfiguration Configs()
        { 
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch 
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
            
            
        }
        IConfiguration configs = Configs();


        public tbStation_Condition GetStationName(string station_scode)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql =
                    @"  SELECT station_name
                        FROM [dbo].[tbStation] WITH (NOLOCK)
                        WHERE  station_scode= @station_scode
                    ";
                return cn.QueryFirstOrDefault<tbStation_Condition>(sql, new { station_scode });
                
            }

        }


    }
}
