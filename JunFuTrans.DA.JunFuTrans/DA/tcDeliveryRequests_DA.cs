﻿using Dapper;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace JunFuTrans.DA.JunFuTrans.DA
{
    public class tcDeliveryRequests_DA
    {
        private static IConfiguration Configs()
        { 
            try
            {
                return new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            }
            catch 
            {
                return new ConfigurationBuilder().AddJsonFile("app.json").Build();
            }
            
            
        }
        IConfiguration configs = Configs();
     

        public List<tcDeliveryRequests_Condition> GettcDeliveryRequestsBycheck_number(string check_number)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql =
                    @"  SELECT *
                        FROM [dbo].[tcDeliveryRequests] WITH (NOLOCK)
                        WHERE check_number = @check_number
                    "; 
               return cn.Query<tcDeliveryRequests_Condition>(sql, new { check_number }).AsList();
            }

        }

        public void Update_return_checknumber(string check_number, string return_check_number)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"update tcDeliveryRequests set return_check_number = @return_check_number where check_number = @check_number";
                cn.Execute(sql, new { check_number, return_check_number });
            }

        }

        public tcDeliveryRequests_Condition CheckSDMD(string order_number)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql =
                    @"  SELECT check_number, B.station_name as station_name,*
                        FROM [dbo].[tcDeliveryRequests] R WITH (NOLOCK)
                        LEFT JOIN tbStation B WITH(NOLOCK) ON R.area_arrive_code = B.station_scode
                        WHERE order_number = @order_number
                        and cancel_date is null       
                    ";
                return cn.QueryFirstOrDefault<tcDeliveryRequests_Condition>(sql, new { order_number });
            }

        }

        public void Update_checknumber(string check_number, string request_id)
        {

            using (var cn = new SqlConnection(configs["ConnectionStrings:JunFuDbContext"]))
            {
                string sql = @"update tcDeliveryRequests set check_number = @check_number where request_id = @request_id";
                cn.Execute(sql, new { check_number, request_id });
            }

        }


    }
}
