﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class PickUppImportRecord
    {
		public long id { get; set; }
		public string AccountCode { get; set; }
		public string Status { get; set; }
		public DateTime CreateDate { get; set; }


	}
}
