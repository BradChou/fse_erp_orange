﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class shopee_cbminfo
    {
		public long shopee_cbminfo_id { get; set; }
		public string shopee_cbminfo_number { get; set; }
		public DateTime upload_date { get; set; }
		public string check_number { get; set; }
		public string size { get; set; }
		public string length { get; set; }
		public string width { get; set; }
		public string height { get; set; }
		public string sidesSum { get; set; }
		public string cbm { get; set; }
		public string weight { get; set; }
		public string measurement_Time { get; set; }
		public string filename { get; set; }
		public DateTime create_datetime { get; set; }
		public DateTime update_datetime { get; set; }

	}
}
