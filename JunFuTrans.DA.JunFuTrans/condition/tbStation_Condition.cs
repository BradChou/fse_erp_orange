﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class tbStation_Condition
    {
		public int id { get; set; }
		public string station_code { get; set; }
		public string station_scode { get; set; }
		public string station_name { get; set; }
		public bool active_flag { get; set; }
		public string BusinessDistrict { get; set; }
		public DateTime udate { get; set; }
		public string uuser { get; set; }
		public int owner { get; set; }
		public string sd_range_start { get; set; }
		public string sd_range_end { get; set; }
		public string md_range_start { get; set; }
		public string md_range_end { get; set; }
		public string tel { get; set; }
		public string station_type { get; set; }
		public bool is_new_station { get; set; }
	}
}
