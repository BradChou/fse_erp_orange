﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class AppNotify
    {
        public string work_Station_Scode { get; set; } //作業站
        public string work_Send_code { get; set; } //集區碼
        public string work_Delivery_Code { get; set; } //配區碼
        public string driver_code { get; set; } //司機代碼
        public string Number { get; set; } // 推播訊息


    }
}
