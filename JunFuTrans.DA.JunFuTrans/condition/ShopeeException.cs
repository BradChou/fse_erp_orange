﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class ShopeeException
    {
        public long id { get; set; }
        public string exception { get; set; }
        public DateTime? cdate { get; set; }

        public string json { get; set; }
        public string order_number { get; set; }
        public string send_address { get; set; }
        public string receive_address { get; set; }
        public string send_station { get; set; }
        public string delivery_station { get; set; }
        public string customer_code { get; set; }
        public DateTime? start_time { get; set; }
        public DateTime? end_time { get; set; }
        public string receiveCity { get; set; }
        public string receive_city { get; set; }
        public string receive_area { get; set; }








    }
}
