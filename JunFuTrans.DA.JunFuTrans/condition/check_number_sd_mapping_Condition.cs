﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class check_number_sd_mapping_Condition
    {
		public long id { get; set; }
		public string check_number { get; set; }
		public int org_area_id { get; set; }
		public string md { get; set; }
		public string sd { get; set; }
		public string put_order { get; set; }
		public long request_id { get; set; }
		public DateTime cdate { get; set; }
		public DateTime udate { get; set; }

	}
}
