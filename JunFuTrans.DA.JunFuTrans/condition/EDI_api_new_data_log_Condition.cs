﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class EDI_api_new_data_log_Condition
    {
        public int? id { get; set; }
        public string type { get; set; }
        public string action { get; set; }
        public string message { get; set; }
        public DateTime? cdate { get; set; }
        public string? customer_code { get; set; }
        public string? check_number { get; set; }
        public string result { get; set; }

    }
}
