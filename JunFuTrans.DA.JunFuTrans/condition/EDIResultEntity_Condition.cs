﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class EDIResultEntity_Condition
    {
		public long id { get; set; }
		public string CheckNumber { get; set; }
		public bool Result { get; set; }
		public string Msg { get; set; }
		public string OrderNumber { get; set; }
		public string Sd { get; set; }
		public string Md { get; set; }
		public string Putorder { get; set; }
		public string Area { get; set; }
		public string AreaId { get; set; }
		public DateTime Cdate { get; set; }
	}
}
