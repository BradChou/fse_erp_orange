﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class ShoppeeApiProblem
    {
        public long id { get; set; }
        public int step { get; set; }
        public string CheckNumber { get; set; }
        public string OrderNumber { get; set; }
        public string Exception { get; set; }
        public string Type { get; set; }
        public DateTime? Cdate { get; set; }
    }
}
