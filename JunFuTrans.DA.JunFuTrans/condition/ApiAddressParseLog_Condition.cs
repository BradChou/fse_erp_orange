﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class ApiAddressParseLog_Condition
    {

		public string api_type { get; set; }
		public string customer_code { get; set; }
		public long log_id { get; set; }
		public string order_number { get; set; }
		public string other_param { get; set; }
		public string receive_address { get; set; }
		public string receive_station { get; set; }
		public string send_address { get; set; }
		public string send_station { get; set; }
		public string result_message { get; set; }
		public string exception_message { get; set; }
		public string source_data { get; set; }
		public DateTime start_time { get; set; }
		public DateTime end_time { get; set; }
		public string check_number { get; set; }

	}
}
