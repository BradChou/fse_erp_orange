﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JunFuTrans.DA.JunFuTrans.condition
{
    public class CheckNumberRecord
    {
        public long id { get; set; }
        public string customer_code { get; set; }
        public string total_count { get; set; }
        public string pieces_count { get; set; }
        public string used_count { get; set; }
        public string remain_count { get; set; }
        public string function_flag { get; set; }        
        public DateTime? update_date { get; set; }
        public string adjustment_reason { get; set; }
        public string check_number { get; set; }


    }
}
