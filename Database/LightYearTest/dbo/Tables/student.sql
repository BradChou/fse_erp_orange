﻿CREATE TABLE [dbo].[student] (
    [student_id] INT            IDENTITY (1, 1) NOT NULL,
    [name]       NVARCHAR (200) NULL,
    [age]        INT            NULL,
    [sex]        INT            NULL,
    [address]    NVARCHAR (200) NULL,
    [city]       NVARCHAR (200) NULL,
    CONSTRAINT [PK_students] PRIMARY KEY CLUSTERED ([student_id] ASC)
);

