﻿CREATE TABLE [dbo].[exception_report]
(
	[id] INT NOT NULL identity PRIMARY KEY, 
    [report_station] NVARCHAR(10) NOT NULL, 
    [report_name] NVARCHAR(10) NOT NULL, 
    [check_number] NVARCHAR(20) NOT NULL, 
    [report_matter] NVARCHAR(MAX) NOT NULL, 
    [sketch_matter] NVARCHAR(MAX) NOT NULL,
    [pic_left] NVARCHAR(100) NULL, 
    [pic_right] NVARCHAR(100) NULL,
    [pic_front] NVARCHAR(100) NULL,
    [pic_back] NVARCHAR(100) NULL,
    [pic_top] NVARCHAR(100) NULL,
    [pic_bottom] NVARCHAR(100) NULL,
    [handle_station] NVARCHAR(10) NULL,
    [handler_code] NVARCHAR(10) NULL, 
    [handle_result] NVARCHAR(300) NULL, 
    [cdate] DATETIME NOT NULL,
    [udate] DATETIME NOT NULL,
    [is_closed] BIT not null default 0,
    [relative_check_number] varchar(20)
)