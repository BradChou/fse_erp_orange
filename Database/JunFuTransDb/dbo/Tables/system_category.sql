﻿CREATE TABLE [dbo].[system_category]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [category_name] NVARCHAR(50) NOT NULL, 
    [category_en_name] NVARCHAR(50) NOT NULL, 
    [create_date] DATETIME NOT NULL, 
    [update_date] DATETIME NOT NULL, 
    [is_menu] BIT NOT NULL DEFAULT 1, 
    [display_sort] INT NOT NULL DEFAULT 99
)
