﻿CREATE TABLE [dbo].[orgArea] (
    [id]                 INT            IDENTITY (1, 1) NOT NULL,
    [OFFICE]             NVARCHAR (255) NULL,
    [ZIP3A]              NVARCHAR (255) NULL,
    [ZIPCODE]            NVARCHAR (255) NULL,
    [CITY]               NVARCHAR (30)  NULL,
    [AREA]               NVARCHAR (30)  NULL,
    [AREA1]              NVARCHAR (255) NULL,
    [ROAD]               NVARCHAR (50)  NULL,
    [SCOOP]              NVARCHAR (255) NULL,
    [EVEN]               FLOAT (53)     NULL,
    [CMP_LABLE]          NVARCHAR (255) NULL,
    [LANE]               FLOAT (53)     NULL,
    [LANE1]              NVARCHAR (255) NULL,
    [ALLEY]              FLOAT (53)     NULL,
    [ALLEY1]             NVARCHAR (255) NULL,
    [NO_BGN]             FLOAT (53)     NULL,
    [NO_BGN1]            NVARCHAR (255) NULL,
    [NO_END]             FLOAT (53)     NULL,
    [NO_END1]            NVARCHAR (255) NULL,
    [FLOOR]              NVARCHAR (255) NULL,
    [FLOOR1]             NVARCHAR (255) NULL,
    [ROAD_NO]            NVARCHAR (255) NULL,
    [ROAD1]              NVARCHAR (255) NULL,
    [EROAD]              NVARCHAR (50)  NULL,
    [RMK]                NVARCHAR (255) NULL,
    [RMK1]               NVARCHAR (255) NULL,
    [ZIP3RMK]            FLOAT (53)     NULL,
    [ECITY]              NVARCHAR (255) NULL,
    [EAREA]              NVARCHAR (255) NULL,
    [UWORD]              NVARCHAR (255) NULL,
    [ISN]                NVARCHAR (255) NULL,
    [station_scode]      VARCHAR (50)   NULL,
    [station_name]       VARCHAR (50)   NULL,
    [md_no]              NVARCHAR (50)  NULL,
    [sd_no]              NVARCHAR (50)  NULL,
    [put_order]          NVARCHAR (50)  NULL,
    [ShuttleStationCode] NVARCHAR (50)  NULL,
    [SendSD]             NVARCHAR (50)  NULL,
    [SendMD]             NVARCHAR (50)  NULL,
    CONSTRAINT [PK_orgArea] PRIMARY KEY CLUSTERED ([id] ASC)
);




GO



GO
CREATE NONCLUSTERED INDEX [IX_orgArea-city-area-road]
    ON [dbo].[orgArea]([CITY] ASC, [AREA] ASC, [ROAD] ASC)
    INCLUDE([EROAD]);




GO



GO
CREATE NONCLUSTERED INDEX [IX_orgArea-station_scode]
    ON [dbo].[orgArea]([station_scode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_orgArea-zipcode]
    ON [dbo].[orgArea]([ZIPCODE] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_area1_road]
    ON [dbo].[orgArea]([AREA1] ASC, [ROAD] ASC);


GO


CREATE TRIGGER [dbo].[Trig_orgArea_Insert]
   ON  [dbo].[orgArea]
   AFTER INSERT,UPDATE 
AS 
BEGIN
	Insert Into [orgArea_Log]
		select	    id , [OFFICE],[ZIP3A],[ZIPCODE],[CITY],[AREA],[AREA1],
					[ROAD],[SCOOP],[EVEN],[CMP_LABLE],[LANE],[LANE1],[ALLEY],[ALLEY1],
					[NO_BGN],[NO_BGN1],[NO_END],[NO_END1],
					[FLOOR],[FLOOR1],[ROAD_NO],[ROAD1],[EROAD],[RMK],[RMK1],
					[ZIP3RMK],[ECITY],[EAREA],[UWORD],[ISN],
					[station_scode],
					[station_name],
					[md_no],
					[sd_no],
					[put_order],
					ShuttleStationCode,
					SendSD,
					SendMD,
					getdate() 
		from inserted

End
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'集貨SD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'orgArea', @level2type = N'COLUMN', @level2name = N'SendSD';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'集貨MD', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'orgArea', @level2type = N'COLUMN', @level2name = N'SendMD';

