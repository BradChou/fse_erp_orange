﻿CREATE TABLE [dbo].[TruckVendorPaymentLog] (
    [id]                 INT            IDENTITY (1, 1) NOT NULL,
    [registered_date]    DATETIME       NULL,
    [fee_type]           NVARCHAR (20)  NULL,
    [vendor_name]        NVARCHAR (20)  NULL,
    [car_license]        NVARCHAR (20)  NULL,
    [payment]            INT            NULL,
    [memo]               NVARCHAR (MAX) NULL,
    [company]            NVARCHAR (20)  NULL,
    [create_date]        DATETIME       NULL,
    [update_date]        DATETIME       NULL,
    [update_user]        NVARCHAR (20)  NULL,
    [create_user]        NVARCHAR (10)  NULL,
    [import_random_code] NVARCHAR (20)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車老闆帳務表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'供應商名稱/車老闆名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'vendor_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'立案/發生日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'registered_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'payment';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'摘要', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'支出項目/費用類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'fee_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司別/車行', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'company';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'TruckVendorPaymentLog', @level2type = N'COLUMN', @level2name = N'car_license';

