﻿CREATE TABLE [dbo].[payment_detail] (
    [payment_detail_id] INT          IDENTITY (1, 1) NOT NULL,
    [payment_id]        INT          NOT NULL,
    [barcode]           VARCHAR (50) NOT NULL,
    [receive_amount]    DECIMAL (18) NOT NULL,
    [actual_amount]     DECIMAL (18) NOT NULL,
    [create_date]       DATETIME     NULL,
    [create_id]         INT          NULL,
    [update_date]       DATETIME     NULL,
    [update_id]         INT          NULL,
    [is_delete]         BIT          CONSTRAINT [DF_payment_detail_is_delete] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_payment_detail] PRIMARY KEY CLUSTERED ([payment_detail_id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_payment_detail-barcode]
    ON [dbo].[payment_detail]([barcode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_payment_detail-payment_id]
    ON [dbo].[payment_detail]([payment_id] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否刪除', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail', @level2type = N'COLUMN', @level2name = N'is_delete';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail', @level2type = N'COLUMN', @level2name = N'update_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail', @level2type = N'COLUMN', @level2name = N'update_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail', @level2type = N'COLUMN', @level2name = N'create_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建檔日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail', @level2type = N'COLUMN', @level2name = N'create_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'實收金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail', @level2type = N'COLUMN', @level2name = N'actual_amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應收金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail', @level2type = N'COLUMN', @level2name = N'receive_amount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'託運單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail', @level2type = N'COLUMN', @level2name = N'barcode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'母單編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail', @level2type = N'COLUMN', @level2name = N'payment_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'代收對帳作業-子單', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'payment_detail';

