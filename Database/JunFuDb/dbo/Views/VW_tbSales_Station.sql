﻿CREATE VIEW VW_tbSales_Station as
Select station_scode,station_name,second_sales_account_code as DistChief ,
third_sales_account_code as NodeManage,fourth_sales_account_code as DistCS
From tbSales_Station
Where version_code = (Select max(version_code) From tbSales_Station)
and station_area not in ('99')