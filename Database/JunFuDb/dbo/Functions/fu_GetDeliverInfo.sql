﻿/***************************
*建立時間:  2017/04/28
*建立者  :  Lisa  
*功能描述:  回傳貨物發送日及配達資訊 (帶入託運單號)
*參數說明: 
 傳入參數:  @Request_id : 託運單id
 回傳格式:  表格
*參數說明: 
 回傳參數1: check_number :託運單號(貨號)
 回傳參數2: send_date :配送日
 回傳參數3: arrive_date :配達日
 回傳參數4: arrive_option :配送狀態碼
 回傳參數5: arrive_state:配達狀況
 回傳參數6: arrive_item :配達說明
 回傳參數7: arrive_scan :已掃描否

*使用範例:   SELECT * FROM dbo.fu_GetDeliverInfo('8283617532') info;	
***************************/

--Create Function dbo.fu_GetDeliverInfo(@check_number NVARCHAR(20))
create Function [dbo].[fu_GetDeliverInfo](@Request_id numeric(18, 0))
Returns @r 
TABLE(
    check_number NVARCHAR(20),
	send_date DATETIME ,
	supplier_time DATETIME ,
	arrive_date  DATETIME,
	arrive_option VARCHAR(2),
    arrive_item NVARCHAR(50),
	arrive_state NVARCHAR(10),
	arrive_scan BIT,
	
	newest_station nvarchar(20),
	newest_scandate DATETIME,
	newest_state nvarchar(20),
	newest_driver_code nvarchar(20), 
	newest_pickupstate nvarchar(20),

	driver_code nvarchar(10),
	driver_name nvarchar(20),
	Arrive_dt DATETIME

)
AS
BEGIN
	  /** 參數宣告 **/
	  DECLARE  @check_number NVARCHAR(20)  --貨號
	  DECLARE  @print_date  DATETIME       --發送日
      DECLARE  @send_date  DATETIME        --配送日
	  DECLARE  @supplier_time DATETIME     --配送日期時間
	  DECLARE  @arrive_date  DATETIME      --配達日
	  DECLARE  @arrive_option VARCHAR(2)   --配送狀態碼
	  DECLARE  @arrive_state NVARCHAR(10)  --配達狀況
	  DECLARE  @arrive_item NVARCHAR(50)   --配達說明
	  DECLARE  @arrive_scan BIT           --是否已掃瞄(針對配達貨件)

	  DECLARE  @Arrive_dt DATETIME			--到著日

	  DECLARE  @newest_station nvarchar(20)  --最新作業站
	  DECLARE  @newest_scandate DATETIME     --最新掃描日
	  DECLARE  @newest_state nvarchar(20)    --最新掃讀狀態
	  DECLARE  @newest_driver_code nvarchar(20)  --最新掃讀司機
	  DECLARE  @DeliveryType varchar(10)     --正/逆物流 
	  DECLARE  @newest_pickupstate nvarchar(20)  --最新取件狀態

	  DECLARE  @driver_code nvarchar(10)  --司機編號
	  DECLARE  @driver_name nvarchar(20)  --司機姓名

	 /** 主要程式(start) **/
		SELECT Top 1  @check_number = check_number, @print_date = CONVERT(CHAR(10), print_date,111) 
		              ,@DeliveryType = DeliveryType  
		FROM tcDeliveryRequests With(Nolock)
		where Request_id IN(@Request_id) --託運單編號

	    IF (@check_number IS Not NULL and @check_number<> '') BEGIN 
		   --配送日
			SELECT Top 1 @send_date = CONVERT(CHAR(10),dlog.scan_date,111) , 
			@supplier_time= CONVERT(CHAR(20),dlog.scan_date,120)
			FROM ttDeliveryScanLog dlog With(Nolock)
			WHERE check_number IN(@check_number) AND  scan_item ='2' --託運單號
			AND cdate >=  DATEADD(MONTH,-6,getdate())
			ORDER BY scan_date DESC
			  
		  --到著日
			SELECT Top 1 @Arrive_dt = CONVERT(CHAR(10),dlog.scan_date,111) 
			FROM ttDeliveryScanLog dlog With(Nolock)
			WHERE check_number IN(@check_number) AND  scan_item ='1' --託運單號
			AND cdate >=  DATEADD(MONTH,-6,getdate())
			ORDER BY scan_date DESC

			--配速區分(配達)
			SELECT TOP 1  @arrive_option = arrive_option
						 ,@arrive_item = code.code_name 
						 ,@arrive_date = dlog.scan_date
						 ,@arrive_scan =CASE WHEN (dlog.sign_field_image IS NULL) THEN 0 ELSE 1 END
						 ,@driver_code = dlog.driver_code 
						 ,@driver_name = dri.driver_name 
			FROM ttDeliveryScanLog dlog With(Nolock)
			LEFT JOIN  tbItemCodes code ON dlog.arrive_option=code.code_id AND code_sclass IN('AO')
			LEFT JOIN tbDrivers dri With(nolock) on dlog.driver_code = dri.driver_code 
			WHERE check_number IN(@check_number) AND  scan_item ='3'
			AND dlog.cdate >=  DATEADD(MONTH,-6,getdate())
			ORDER BY scan_date DESC

			--配達狀況
			SELECT Top 1 @arrive_state = code.code_name, @newest_scandate = dlog.scan_date ,@newest_driver_code = dlog.driver_code
						,@newest_station = dri.station , @newest_state= CASE WHEN @DeliveryType= 'D' THEN ao.code_name  ELSE ro.code_name  END
			FROM ttDeliveryScanLog dlog With(Nolock)
			LEFT JOIN tbItemCodes code With(Nolock) on code.code_bclass = '5' and  code.code_sclass = 'P3' and code.code_id = dlog.scan_item  
			LEFT JOIN tbDrivers dri With(Nolock) on dlog.driver_code = dri.driver_code 
			LEFT JOIN tbItemCodes ao ON dlog.arrive_option=ao.code_id AND ao.code_sclass ='AO' and @DeliveryType ='D' 
			LEFT JOIN tbItemCodes ro ON dlog.arrive_option=ro.code_id AND ro.code_sclass ='RO' and @DeliveryType ='R' 
			WHERE dlog.check_number IN(@check_number) --託運單號
			AND dlog.scan_item != '4'
			AND dlog.cdate >=  DATEADD(MONTH,-6,getdate())
			ORDER BY dlog.scan_date  DESC

			--取件狀態
			SELECT TOP 1 @newest_pickupstate = B.code_name 
			FROM ttReturnDeliveryPickupLog A with(nolock) 
			LEFT JOIN  tbItemCodes B WITH (NOLOCK) ON  B.code_sclass = 'RO' AND A.scan_item = b.code_id 
			WHERE a.check_number IN(@check_number) 
			AND A.scan_date  >=  DATEADD(MONTH,-6,getdate())
			ORDER BY A.cdate DESC 
			


		END

	  BEGIN
		 INSERT @r
         SELECT @check_number,@send_date,@supplier_time, @arrive_date,@arrive_option,@arrive_item,@arrive_state,@arrive_scan, @newest_station,@newest_scandate,@newest_state,@newest_driver_code,@newest_pickupstate, @driver_code, @driver_name ,@Arrive_dt

	  END

	 /** 主要程式(end) **/	

    /**回傳結果**/
    RETURN
END