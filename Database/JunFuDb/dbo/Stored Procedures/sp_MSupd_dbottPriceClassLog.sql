﻿create procedure [sp_MSupd_dbottPriceClassLog]
		@c1 numeric(18,0) = NULL,
		@c2 int = NULL,
		@c3 numeric(10,2) = NULL,
		@c4 numeric(10,2) = NULL,
		@c5 numeric(10,2) = NULL,
		@c6 numeric(10,2) = NULL,
		@c7 numeric(10,2) = NULL,
		@c8 numeric(10,2) = NULL,
		@c9 numeric(10,2) = NULL,
		@c10 char(1) = NULL,
		@c11 datetime = NULL,
		@c12 date = NULL,
		@c13 datetime = NULL,
		@c14 nvarchar(20) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttPriceClassLog] set
		[class_level] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [class_level] end,
		[week1] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [week1] end,
		[week2] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [week2] end,
		[week3] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [week3] end,
		[week4] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [week4] end,
		[week5] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [week5] end,
		[week6] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [week6] end,
		[arrive] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [arrive] end,
		[notify_flag] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [notify_flag] end,
		[notify_time] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [notify_time] end,
		[active_date] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [active_date] end,
		[cdate] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [cdate] end,
		[cuser] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [cuser] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttPriceClassLog]', @param2=@primarykey_text, @param3=13233
		End
end