﻿create procedure [sp_MSupd_dbottInsuranceRecord]
		@c1 bigint = NULL,
		@c2 int = NULL,
		@c3 int = NULL,
		@c4 int = NULL,
		@c5 int = NULL,
		@c6 bit = NULL,
		@c7 nvarchar(50) = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttInsuranceRecord] set
		[Insurance_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Insurance_id] end,
		[year] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [year] end,
		[month] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [month] end,
		[price] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [price] end,
		[ispay] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [ispay] end,
		[memo] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [memo] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttInsuranceRecord]', @param2=@primarykey_text, @param3=13233
		End
end