﻿create procedure [sp_MSupd_dbottTrack]
		@c1 varchar(10) = NULL,
		@c2 varchar(10) = NULL,
		@c3 datetime = NULL,
		@c4 smalldatetime = NULL,
		@c5 float = NULL,
		@c6 float = NULL,
		@c7 bit = NULL,
		@c8 tinyint = NULL,
		@c9 int = NULL,
		@c10 int = NULL,
		@c11 real = NULL,
		@c12 real = NULL,
		@c13 real = NULL,
		@c14 real = NULL,
		@c15 smallint = NULL,
		@c16 int = NULL,
		@c17 int = NULL,
		@c18 int = NULL,
		@c19 int = NULL,
		@c20 varchar(7) = NULL,
		@c21 varchar(5) = NULL,
		@c22 varchar(10) = NULL,
		@c23 varchar(9) = NULL,
		@c24 tinyint = NULL,
		@c25 bit = NULL,
		@c26 float = NULL,
		@c27 float = NULL,
		@c28 int = NULL,
		@c29 int = NULL,
		@c30 int = NULL,
		@pkc1 varchar(10) = NULL,
		@pkc2 varchar(10) = NULL,
		@pkc3 datetime = NULL,
		@bitmap binary(4)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if (substring(@bitmap,1,1) & 1 = 1) or
 (substring(@bitmap,1,1) & 2 = 2) or
 (substring(@bitmap,1,1) & 4 = 4)
begin 
update [dbo].[ttTrack] set
		[Su_No] = case substring(@bitmap,1,1) & 1 when 1 then @c1 else [Su_No] end,
		[car_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [car_id] end,
		[dt] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [dt] end,
		[disp_dt] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [disp_dt] end,
		[x] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [x] end,
		[y] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [y] end,
		[f] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [f] end,
		[qi] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [qi] end,
		[status] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [status] end,
		[status2] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [status2] end,
		[value1] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [value1] end,
		[value2] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [value2] end,
		[value3] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [value3] end,
		[value4] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [value4] end,
		[mileage] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [mileage] end,
		[dt_diff] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [dt_diff] end,
		[stay] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [stay] end,
		[rcv_seq02] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [rcv_seq02] end,
		[node_id] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [node_id] end,
		[drag_car] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [drag_car] end,
		[Course] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [Course] end,
		[WGS_X] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [WGS_X] end,
		[WGS_Y] = case substring(@bitmap,3,1) & 64 when 64 then @c23 else [WGS_Y] end,
		[Sat] = case substring(@bitmap,3,1) & 128 when 128 then @c24 else [Sat] end,
		[IO1] = case substring(@bitmap,4,1) & 1 when 1 then @c25 else [IO1] end,
		[Temperature] = case substring(@bitmap,4,1) & 2 when 2 then @c26 else [Temperature] end,
		[Humidity] = case substring(@bitmap,4,1) & 4 when 4 then @c27 else [Humidity] end,
		[TaskStatus] = case substring(@bitmap,4,1) & 8 when 8 then @c28 else [TaskStatus] end,
		[Task_id] = case substring(@bitmap,4,1) & 16 when 16 then @c29 else [Task_id] end,
		[ReportStatus] = case substring(@bitmap,4,1) & 32 when 32 then @c30 else [ReportStatus] end
	where [Su_No] = @pkc1
  and [car_id] = @pkc2
  and [dt] = @pkc3
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[Su_No] = ' + convert(nvarchar(100),@pkc1,1) + ', '
			set @primarykey_text = @primarykey_text + '[car_id] = ' + convert(nvarchar(100),@pkc2,1) + ', '
			set @primarykey_text = @primarykey_text + '[dt] = ' + convert(nvarchar(100),@pkc3,21)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttTrack]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [dbo].[ttTrack] set
		[disp_dt] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [disp_dt] end,
		[x] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [x] end,
		[y] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [y] end,
		[f] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [f] end,
		[qi] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [qi] end,
		[status] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [status] end,
		[status2] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [status2] end,
		[value1] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [value1] end,
		[value2] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [value2] end,
		[value3] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [value3] end,
		[value4] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [value4] end,
		[mileage] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [mileage] end,
		[dt_diff] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [dt_diff] end,
		[stay] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [stay] end,
		[rcv_seq02] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [rcv_seq02] end,
		[node_id] = case substring(@bitmap,3,1) & 4 when 4 then @c19 else [node_id] end,
		[drag_car] = case substring(@bitmap,3,1) & 8 when 8 then @c20 else [drag_car] end,
		[Course] = case substring(@bitmap,3,1) & 16 when 16 then @c21 else [Course] end,
		[WGS_X] = case substring(@bitmap,3,1) & 32 when 32 then @c22 else [WGS_X] end,
		[WGS_Y] = case substring(@bitmap,3,1) & 64 when 64 then @c23 else [WGS_Y] end,
		[Sat] = case substring(@bitmap,3,1) & 128 when 128 then @c24 else [Sat] end,
		[IO1] = case substring(@bitmap,4,1) & 1 when 1 then @c25 else [IO1] end,
		[Temperature] = case substring(@bitmap,4,1) & 2 when 2 then @c26 else [Temperature] end,
		[Humidity] = case substring(@bitmap,4,1) & 4 when 4 then @c27 else [Humidity] end,
		[TaskStatus] = case substring(@bitmap,4,1) & 8 when 8 then @c28 else [TaskStatus] end,
		[Task_id] = case substring(@bitmap,4,1) & 16 when 16 then @c29 else [Task_id] end,
		[ReportStatus] = case substring(@bitmap,4,1) & 32 when 32 then @c30 else [ReportStatus] end
	where [Su_No] = @pkc1
  and [car_id] = @pkc2
  and [dt] = @pkc3
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[Su_No] = ' + convert(nvarchar(100),@pkc1,1) + ', '
			set @primarykey_text = @primarykey_text + '[car_id] = ' + convert(nvarchar(100),@pkc2,1) + ', '
			set @primarykey_text = @primarykey_text + '[dt] = ' + convert(nvarchar(100),@pkc3,21)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttTrack]', @param2=@primarykey_text, @param3=13233
		End
end 
end