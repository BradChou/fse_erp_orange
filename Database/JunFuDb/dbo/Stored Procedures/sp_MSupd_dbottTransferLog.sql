﻿create procedure [sp_MSupd_dbottTransferLog]
		@c1 int = NULL,
		@c2 nvarchar(10) = NULL,
		@c3 nvarchar(25) = NULL,
		@c4 datetime = NULL,
		@c5 nvarchar(50) = NULL,
		@c6 datetime = NULL,
		@c7 nvarchar(20) = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttTransferLog] set
		[station_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [station_code] end,
		[driver_code] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [driver_code] end,
		[scanning_dt] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [scanning_dt] end,
		[check_number] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [check_number] end,
		[create_dt] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [create_dt] end,
		[Transfer] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [Transfer] end
	where [TransferID] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[TransferID] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttTransferLog]', @param2=@primarykey_text, @param3=13233
		End
end