﻿create procedure [sp_MSins_dbottAssetsExpenditure]
    @c1 bigint,
    @c2 int,
    @c3 varchar(2),
    @c4 nvarchar(50),
    @c5 varchar(10),
    @c6 nvarchar(7),
    @c7 nvarchar(50),
    @c8 varchar(2),
    @c9 varchar(20),
    @c10 datetime,
    @c11 nvarchar(20),
    @c12 nvarchar(20),
    @c13 nvarchar(max),
    @c14 float,
    @c15 float,
    @c16 float,
    @c17 datetime,
    @c18 varchar(20),
    @c19 datetime,
    @c20 varchar(20),
    @c21 datetime,
    @c22 varchar(20)
as
begin  
	insert into [dbo].[ttAssetsExpenditure] (
		[id],
		[type],
		[fee_type],
		[fee_type_name],
		[oil],
		[fee_date],
		[company],
		[pay_kind],
		[paper],
		[pay_date],
		[pay_account],
		[pay_num],
		[memo],
		[money],
		[price],
		[tax],
		[idate],
		[iuser],
		[cdate],
		[cuser],
		[udate],
		[uuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14,
		@c15,
		@c16,
		@c17,
		@c18,
		@c19,
		@c20,
		@c21,
		@c22	) 
end