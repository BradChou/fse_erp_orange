﻿create procedure [sp_MSins_dbottArriveSitesScattered]
    @c1 int,
    @c2 nvarchar(10),
    @c3 nvarchar(10),
    @c4 nvarchar(3),
    @c5 nvarchar(10)
as
begin  
	insert into [dbo].[ttArriveSitesScattered] (
		[seq],
		[post_city],
		[post_area],
		[zip],
		[station_code]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5	) 
end