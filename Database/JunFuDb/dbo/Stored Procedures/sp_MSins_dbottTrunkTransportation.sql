﻿create procedure [sp_MSins_dbottTrunkTransportation]
    @c1 bigint,
    @c2 varchar(10),
    @c3 varchar(10),
    @c4 nvarchar(10),
    @c5 datetime,
    @c6 datetime,
    @c7 datetime
as
begin  
	insert into [dbo].[ttTrunkTransportation] (
		[id],
		[car_license],
		[tel],
		[driver_name],
		[sdate],
		[edate],
		[cdate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end