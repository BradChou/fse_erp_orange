﻿create procedure [sp_MSins_dbotbItemCodes]
    @c1 numeric(18,0),
    @c2 char(1),
    @c3 varchar(20),
    @c4 nvarchar(20),
    @c5 nvarchar(50),
    @c6 bit,
    @c7 nvarchar(255)
as
begin  
	insert into [dbo].[tbItemCodes] (
		[seq],
		[code_bclass],
		[code_sclass],
		[code_id],
		[code_name],
		[active_flag],
		[memo]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end