﻿create procedure [sp_MSupd_dbotbStation]
		@c1 int = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 nvarchar(100) = NULL,
		@c5 bit = NULL,
		@c6 nvarchar(20) = NULL,
		@c7 datetime = NULL,
		@c8 nvarchar(20) = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbStation] set
		[station_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [station_code] end,
		[station_scode] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [station_scode] end,
		[station_name] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [station_name] end,
		[active_flag] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [active_flag] end,
		[BusinessDistrict] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [BusinessDistrict] end,
		[udate] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [udate] end,
		[uuser] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [uuser] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbStation]', @param2=@primarykey_text, @param3=13233
		End
end