﻿create procedure [sp_MSupd_dbottDispatchTaskDetail]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 bigint = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttDispatchTaskDetail] set
		[t_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [t_id] end,
		[request_id] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [request_id] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttDispatchTaskDetail]', @param2=@primarykey_text, @param3=13233
		End
end