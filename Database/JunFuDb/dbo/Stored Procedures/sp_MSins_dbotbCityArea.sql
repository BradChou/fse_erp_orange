﻿create procedure [sp_MSins_dbotbCityArea]
    @c1 int,
    @c2 nvarchar(20),
    @c3 nvarchar(3),
    @c4 nvarchar(15),
    @c5 int,
    @c6 nvarchar(50),
    @c7 int,
    @c8 datetime,
    @c9 nvarchar(20)
as
begin  
	insert into [dbo].[tbCityArea] (
		[seq],
		[CityArea],
		[City],
		[Area],
		[Zip],
		[Note],
		[IsActive],
		[cdate],
		[cuser]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9	) 
end