﻿create procedure [sp_MSins_dbotbDeliveryNumberSetting]
    @c1 int,
    @c2 nvarchar(20),
    @c3 bigint,
    @c4 bigint,
    @c5 bigint,
    @c6 nvarchar(10),
    @c7 datetime,
    @c8 int,
    @c9 int
as
begin  
	insert into [dbo].[tbDeliveryNumberSetting] (
		[seq],
		[customer_code],
		[begin_number],
		[end_number],
		[current_number],
		[cuser],
		[cdate],
		[IsActive],
		[num]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9	) 
end