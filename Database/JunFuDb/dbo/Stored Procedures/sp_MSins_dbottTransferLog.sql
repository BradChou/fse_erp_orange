﻿create procedure [sp_MSins_dbottTransferLog]
    @c1 int,
    @c2 nvarchar(10),
    @c3 nvarchar(25),
    @c4 datetime,
    @c5 nvarchar(50),
    @c6 datetime,
    @c7 nvarchar(20)
as
begin  
	insert into [dbo].[ttTransferLog] (
		[TransferID],
		[station_code],
		[driver_code],
		[scanning_dt],
		[check_number],
		[create_dt],
		[Transfer]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end