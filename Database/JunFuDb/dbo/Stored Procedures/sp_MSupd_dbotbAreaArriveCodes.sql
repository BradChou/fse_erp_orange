﻿create procedure [sp_MSupd_dbotbAreaArriveCodes]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(3) = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 nvarchar(5) = NULL,
		@c5 nvarchar(50) = NULL,
		@c6 nvarchar(10) = NULL,
		@c7 nvarchar(10) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbAreaArriveCodes] set
		[supplier_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [supplier_code] end,
		[supplier_name] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [supplier_name] end,
		[arrive_code] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [arrive_code] end,
		[arrive_address] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [arrive_address] end,
		[city] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [city] end,
		[area] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [area] end
	where [arrive_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[arrive_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbAreaArriveCodes]', @param2=@primarykey_text, @param3=13233
		End
end