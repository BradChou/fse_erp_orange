﻿create procedure [sp_MSupd_dbottDispatchTask]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 varchar(20) = NULL,
		@c4 datetime = NULL,
		@c5 varchar(3) = NULL,
		@c6 nvarchar(3) = NULL,
		@c7 nvarchar(10) = NULL,
		@c8 nvarchar(20) = NULL,
		@c9 nvarchar(10) = NULL,
		@c10 nvarchar(10) = NULL,
		@c11 int = NULL,
		@c12 int = NULL,
		@c13 int = NULL,
		@c14 datetime = NULL,
		@c15 nvarchar(10) = NULL,
		@c16 nvarchar(100) = NULL,
		@c17 nvarchar(10) = NULL,
		@c18 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(3)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttDispatchTask] set
		[task_type] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [task_type] end,
		[task_id] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [task_id] end,
		[task_date] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [task_date] end,
		[serial] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [serial] end,
		[supplier] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [supplier] end,
		[supplier_no] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [supplier_no] end,
		[task_route] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [task_route] end,
		[driver] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [driver] end,
		[car_license] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [car_license] end,
		[plates] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [plates] end,
		[price] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [price] end,
		[status] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [status] end,
		[checkout_close_date] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [checkout_close_date] end,
		[close_randomCode] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [close_randomCode] end,
		[memo] = case substring(@bitmap,2,1) & 128 when 128 then @c16 else [memo] end,
		[c_user] = case substring(@bitmap,3,1) & 1 when 1 then @c17 else [c_user] end,
		[c_time] = case substring(@bitmap,3,1) & 2 when 2 then @c18 else [c_time] end
	where [t_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[t_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttDispatchTask]', @param2=@primarykey_text, @param3=13233
		End
end