﻿create procedure [sp_MSins_dbottKuoYangScanLog]
    @c1 decimal(18,0),
    @c2 nvarchar(20),
    @c3 nvarchar(30),
    @c4 bit,
    @c5 datetime,
    @c6 datetime,
    @c7 nvarchar(100)
as
begin  
	insert into [dbo].[ttKuoYangScanLog] (
		[log_id],
		[check_number],
		[order_number],
		[flag_type],
		[cdate],
		[udate],
		[ImportFile]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7	) 
end