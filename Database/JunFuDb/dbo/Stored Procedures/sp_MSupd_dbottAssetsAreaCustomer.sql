﻿create procedure [sp_MSupd_dbottAssetsAreaCustomer]
		@c1 bigint = NULL,
		@c2 int = NULL,
		@c3 nvarchar(10) = NULL,
		@c4 bit = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAssetsAreaCustomer] set
		[area_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [area_id] end,
		[customer_code] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [customer_code] end,
		[chk] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [chk] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAssetsAreaCustomer]', @param2=@primarykey_text, @param3=13233
		End
end