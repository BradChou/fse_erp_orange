﻿create procedure [sp_MSupd_dbotbCalendar]
		@c1 datetime = NULL,
		@c2 int = NULL,
		@c3 int = NULL,
		@c4 int = NULL,
		@c5 int = NULL,
		@c6 int = NULL,
		@c7 int = NULL,
		@c8 int = NULL,
		@c9 int = NULL,
		@c10 int = NULL,
		@c11 int = NULL,
		@c12 varchar(10) = NULL,
		@c13 varchar(50) = NULL,
		@c14 nvarchar(20) = NULL,
		@c15 datetime = NULL,
		@pkc1 datetime = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
if (substring(@bitmap,1,1) & 1 = 1)
begin 
update [dbo].[tbCalendar] set
		[Date] = case substring(@bitmap,1,1) & 1 when 1 then @c1 else [Date] end,
		[Year] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Year] end,
		[Quarter] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Quarter] end,
		[Month] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Month] end,
		[Week] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [Week] end,
		[Day] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Day] end,
		[DayOfYear] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [DayOfYear] end,
		[Weekday] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [Weekday] end,
		[Fiscal_Year] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [Fiscal_Year] end,
		[Fiscal_Quarter] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [Fiscal_Quarter] end,
		[Fiscal_Month] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [Fiscal_Month] end,
		[KindOfDay] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [KindOfDay] end,
		[Description] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [Description] end,
		[uuser] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [uuser] end,
		[udate] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [udate] end
	where [Date] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[Date] = ' + convert(nvarchar(100),@pkc1,21)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbCalendar]', @param2=@primarykey_text, @param3=13233
		End
end  
else
begin 
update [dbo].[tbCalendar] set
		[Year] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [Year] end,
		[Quarter] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [Quarter] end,
		[Month] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Month] end,
		[Week] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [Week] end,
		[Day] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [Day] end,
		[DayOfYear] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [DayOfYear] end,
		[Weekday] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [Weekday] end,
		[Fiscal_Year] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [Fiscal_Year] end,
		[Fiscal_Quarter] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [Fiscal_Quarter] end,
		[Fiscal_Month] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [Fiscal_Month] end,
		[KindOfDay] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [KindOfDay] end,
		[Description] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [Description] end,
		[uuser] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [uuser] end,
		[udate] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [udate] end
	where [Date] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[Date] = ' + convert(nvarchar(100),@pkc1,21)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbCalendar]', @param2=@primarykey_text, @param3=13233
		End
end 
end