﻿create procedure [sp_MSins_dbottUseLog]
    @c1 bigint,
    @c2 nvarchar(20),
    @c3 nvarchar(100),
    @c4 nvarchar(200),
    @c5 varchar(30),
    @c6 datetime
as
begin  
	insert into [dbo].[ttUseLog] (
		[id],
		[account_code],
		[UseItem],
		[Memo],
		[IP],
		[cdate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6	) 
end