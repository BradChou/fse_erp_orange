﻿create procedure [sp_MSupd_dbottDeliveryDisposeLog]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 nvarchar(100) = NULL,
		@c5 datetime = NULL,
		@c6 nvarchar(10) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttDeliveryDisposeLog] set
		[check_number] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [check_number] end,
		[status_type] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [status_type] end,
		[dispose_desc] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [dispose_desc] end,
		[cdate] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [cdate] end,
		[cuser] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [cuser] end
	where [log_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[log_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttDeliveryDisposeLog]', @param2=@primarykey_text, @param3=13233
		End
end