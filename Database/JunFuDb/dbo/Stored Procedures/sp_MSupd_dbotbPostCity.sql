﻿create procedure [sp_MSupd_dbotbPostCity]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(10) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbPostCity] set
		[city] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [city] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbPostCity]', @param2=@primarykey_text, @param3=13233
		End
end