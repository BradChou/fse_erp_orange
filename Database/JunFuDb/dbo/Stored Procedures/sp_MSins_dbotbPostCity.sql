﻿create procedure [sp_MSins_dbotbPostCity]
    @c1 numeric(18,0),
    @c2 nvarchar(10)
as
begin  
	insert into [dbo].[tbPostCity] (
		[seq],
		[city]
	) values (
		@c1,
		@c2	) 
end