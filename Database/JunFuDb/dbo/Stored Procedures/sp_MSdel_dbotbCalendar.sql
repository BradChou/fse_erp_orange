﻿create procedure [sp_MSdel_dbotbCalendar]
		@pkc1 datetime
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [dbo].[tbCalendar] 
	where [Date] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[Date] = ' + convert(nvarchar(100),@pkc1,21)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbCalendar]', @param2=@primarykey_text, @param3=13234
		End
end