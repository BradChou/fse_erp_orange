﻿create procedure [sp_MSins_dbottAppVersion]
    @c1 numeric(18,0),
    @c2 varchar(20),
    @c3 varchar(30),
    @c4 int,
    @c5 varchar(30),
    @c6 varchar(120),
    @c7 int,
    @c8 datetime
as
begin  
	insert into [dbo].[ttAppVersion] (
		[ver_id],
		[app_id],
		[app_nme],
		[version_code],
		[version_name],
		[version_url],
		[use_flag],
		[version_date]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8	) 
end