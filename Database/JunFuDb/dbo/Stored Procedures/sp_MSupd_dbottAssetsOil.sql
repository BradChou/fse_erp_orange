﻿create procedure [sp_MSupd_dbottAssetsOil]
		@c1 int = NULL,
		@c2 int = NULL,
		@c3 varchar(2) = NULL,
		@c4 nchar(10) = NULL,
		@c5 datetime = NULL,
		@c6 datetime = NULL,
		@c7 datetime = NULL,
		@pkc1 int = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAssetsOil] set
		[a_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [a_id] end,
		[oil] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [oil] end,
		[card_id] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [card_id] end,
		[active_date] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [active_date] end,
		[stop_date] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [stop_date] end,
		[cdate] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [cdate] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAssetsOil]', @param2=@primarykey_text, @param3=13233
		End
end