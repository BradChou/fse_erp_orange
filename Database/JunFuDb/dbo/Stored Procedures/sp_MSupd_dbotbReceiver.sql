﻿create procedure [sp_MSupd_dbotbReceiver]
		@c1 numeric(18,0) = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 nvarchar(20) = NULL,
		@c4 nvarchar(20) = NULL,
		@c5 nvarchar(20) = NULL,
		@c6 nvarchar(10) = NULL,
		@c7 nvarchar(20) = NULL,
		@c8 nvarchar(10) = NULL,
		@c9 nvarchar(10) = NULL,
		@c10 nvarchar(50) = NULL,
		@c11 nvarchar(100) = NULL,
		@c12 nvarchar(20) = NULL,
		@c13 datetime = NULL,
		@c14 nvarchar(20) = NULL,
		@c15 datetime = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(2)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbReceiver] set
		[customer_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [customer_code] end,
		[receiver_code] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [receiver_code] end,
		[receiver_name] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [receiver_name] end,
		[tel] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [tel] end,
		[tel_ext] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [tel_ext] end,
		[tel2] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [tel2] end,
		[address_city] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [address_city] end,
		[address_area] = case substring(@bitmap,2,1) & 1 when 1 then @c9 else [address_area] end,
		[address_road] = case substring(@bitmap,2,1) & 2 when 2 then @c10 else [address_road] end,
		[memo] = case substring(@bitmap,2,1) & 4 when 4 then @c11 else [memo] end,
		[cuser] = case substring(@bitmap,2,1) & 8 when 8 then @c12 else [cuser] end,
		[cdate] = case substring(@bitmap,2,1) & 16 when 16 then @c13 else [cdate] end,
		[uuser] = case substring(@bitmap,2,1) & 32 when 32 then @c14 else [uuser] end,
		[udate] = case substring(@bitmap,2,1) & 64 when 64 then @c15 else [udate] end
	where [receiver_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[receiver_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbReceiver]', @param2=@primarykey_text, @param3=13233
		End
end