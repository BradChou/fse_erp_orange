﻿create procedure [sp_MSupd_dbottAppVersion]
		@c1 numeric(18,0) = NULL,
		@c2 varchar(20) = NULL,
		@c3 varchar(30) = NULL,
		@c4 int = NULL,
		@c5 varchar(30) = NULL,
		@c6 varchar(120) = NULL,
		@c7 int = NULL,
		@c8 datetime = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttAppVersion] set
		[app_id] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [app_id] end,
		[app_nme] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [app_nme] end,
		[version_code] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [version_code] end,
		[version_name] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [version_name] end,
		[version_url] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [version_url] end,
		[use_flag] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [use_flag] end,
		[version_date] = case substring(@bitmap,1,1) & 128 when 128 then @c8 else [version_date] end
	where [ver_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[ver_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAppVersion]', @param2=@primarykey_text, @param3=13233
		End
end