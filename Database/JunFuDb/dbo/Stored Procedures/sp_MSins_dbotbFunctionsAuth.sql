﻿create procedure [sp_MSins_dbotbFunctionsAuth]
    @c1 numeric(18,0),
    @c2 char(1),
    @c3 varchar(20),
    @c4 varchar(10),
    @c5 bit,
    @c6 bit,
    @c7 bit,
    @c8 bit,
    @c9 bit,
    @c10 int,
    @c11 nvarchar(20),
    @c12 datetime,
    @c13 nvarchar(20),
    @c14 datetime
as
begin  
	insert into [dbo].[tbFunctionsAuth] (
		[auth_id],
		[manager_type],
		[account_code],
		[func_code],
		[view_auth],
		[insert_auth],
		[modify_auth],
		[delete_auth],
		[etc_auth],
		[func_type],
		[cuser],
		[cdate],
		[uuser],
		[udate]
	) values (
		@c1,
		@c2,
		@c3,
		@c4,
		@c5,
		@c6,
		@c7,
		@c8,
		@c9,
		@c10,
		@c11,
		@c12,
		@c13,
		@c14	) 
end