﻿create procedure [sp_MSupd_dbottUseLog]
		@c1 bigint = NULL,
		@c2 nvarchar(20) = NULL,
		@c3 nvarchar(100) = NULL,
		@c4 nvarchar(200) = NULL,
		@c5 varchar(30) = NULL,
		@c6 datetime = NULL,
		@pkc1 bigint = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[ttUseLog] set
		[account_code] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [account_code] end,
		[UseItem] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [UseItem] end,
		[Memo] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [Memo] end,
		[IP] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [IP] end,
		[cdate] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [cdate] end
	where [id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttUseLog]', @param2=@primarykey_text, @param3=13233
		End
end