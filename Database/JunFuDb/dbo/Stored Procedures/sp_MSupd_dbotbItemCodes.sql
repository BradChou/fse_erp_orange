﻿create procedure [sp_MSupd_dbotbItemCodes]
		@c1 numeric(18,0) = NULL,
		@c2 char(1) = NULL,
		@c3 varchar(20) = NULL,
		@c4 nvarchar(20) = NULL,
		@c5 nvarchar(50) = NULL,
		@c6 bit = NULL,
		@c7 nvarchar(255) = NULL,
		@pkc1 numeric(18,0) = NULL,
		@bitmap binary(1)
as
begin  
	declare @primarykey_text nvarchar(100) = ''
update [dbo].[tbItemCodes] set
		[code_bclass] = case substring(@bitmap,1,1) & 2 when 2 then @c2 else [code_bclass] end,
		[code_sclass] = case substring(@bitmap,1,1) & 4 when 4 then @c3 else [code_sclass] end,
		[code_id] = case substring(@bitmap,1,1) & 8 when 8 then @c4 else [code_id] end,
		[code_name] = case substring(@bitmap,1,1) & 16 when 16 then @c5 else [code_name] end,
		[active_flag] = case substring(@bitmap,1,1) & 32 when 32 then @c6 else [active_flag] end,
		[memo] = case substring(@bitmap,1,1) & 64 when 64 then @c7 else [memo] end
	where [seq] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[seq] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[tbItemCodes]', @param2=@primarykey_text, @param3=13233
		End
end