﻿create procedure [sp_MSdel_dbottAssets]
		@pkc1 bigint
as
begin  
	declare @primarykey_text nvarchar(100) = ''
	delete [dbo].[ttAssets] 
	where [a_id] = @pkc1
if @@rowcount = 0
    if @@microsoftversion>0x07320000
		Begin
			
			set @primarykey_text = @primarykey_text + '[a_id] = ' + convert(nvarchar(100),@pkc1,1)
			exec sp_MSreplraiserror @errorid=20598, @param1=N'[dbo].[ttAssets]', @param2=@primarykey_text, @param3=13234
		End
end