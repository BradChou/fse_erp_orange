﻿CREATE TABLE [dbo].[ttAssetsSB] (
    [id]                     BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Items]                  NVARCHAR (2)  NULL,
    [Items_Text]             NVARCHAR (10) NULL,
    [DayNight]               INT           NULL,
    [InDate]                 DATETIME      NULL,
    [Temperature]            NVARCHAR (2)  NULL,
    [Temperature_Text]       NVARCHAR (10) NULL,
    [car_license]            NVARCHAR (10) NULL,
    [cabin_type]             INT           NULL,
    [cabin_type_text]        NVARCHAR (50) NULL,
    [Stores]                 INT           NULL,
    [Area]                   NVARCHAR (50) NULL,
    [milage]                 FLOAT (53)    NULL,
    [customer_code]          NVARCHAR (10) NULL,
    [RequestObject]          NVARCHAR (10) NULL,
    [Route]                  NVARCHAR (10) NULL,
    [Route_name]             NVARCHAR (30) NULL,
    [driver]                 NVARCHAR (50) NULL,
    [car_retailer]           INT           NULL,
    [car_retailer_text]      NVARCHAR (50) NULL,
    [PaymentObject]          NVARCHAR (10) NULL,
    [income]                 FLOAT (53)    NULL,
    [expenses]               FLOAT (53)    NULL,
    [starttime]              DATETIME      NULL,
    [endtime]                DATETIME      NULL,
    [startstore_time]        DATETIME      NULL,
    [endstore_time]          DATETIME      NULL,
    [supplier]               NVARCHAR (3)  NULL,
    [supplier_no]            NVARCHAR (10) NULL,
    [supplier_name]          NVARCHAR (20) NULL,
    [customer_id]            NUMERIC (18)  NULL,
    [in_checkout_close_date] DATETIME      NULL,
    [in_close_randomCode]    NVARCHAR (10) NULL,
    [ex_checkout_close_date] DATETIME      NULL,
    [ex_close_randomCode]    NVARCHAR (10) NULL,
    [randomCode]             VARCHAR (10)  NULL,
    [memo]                   NVARCHAR (30) NULL,
    [cdate]                  DATETIME      NULL,
    [cuser]                  VARCHAR (20)  NULL,
    [udate]                  DATETIME      NULL,
    [uuser]                  VARCHAR (20)  NULL,
    CONSTRAINT [PK_ttAssetsSB] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'更新日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'創建者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'創建日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'註解', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'隨機碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'randomCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ex關閉隨機碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'ex_close_randomCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ex確認關閉日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'ex_checkout_close_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'in關閉隨機碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'in_close_randomCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'in確認關閉日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'in_checkout_close_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'customer_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'supplier_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'supplier_no';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'supplier';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結束儲存日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'endstore_time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'開始儲存日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'startstore_time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結束日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'endtime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'開始日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'starttime';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'expenses';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收入', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'income';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'付款對象', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'PaymentObject';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車子零售商文字', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'car_retailer_text';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車子零售商', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'car_retailer';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'driver';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'路線名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'Route_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'路線代號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'Route';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'請求對象', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'RequestObject';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'里程數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'milage';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區域代號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'Area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'儲存', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'Stores';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'座艙', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'cabin_type_text';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'座艙類型 2.3.5T(14呎) 3.6.5T 4.7T 5.8T/8.5T 6.11T/12T', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'cabin_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車牌號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'car_license';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'程度文字', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'Temperature_Text';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'程度', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'Temperature';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到達日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'InDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'跨夜', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'DayNight';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'項目文字', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'Items_Text';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'項目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'Items';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產銷售簿', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsSB';

