﻿CREATE TABLE [dbo].[ttAssetsOil] (
    [id]          INT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [a_id]        INT         NULL,
    [oil]         VARCHAR (2) NULL,
    [card_id]     NCHAR (10)  NULL,
    [active_date] DATETIME    NULL,
    [stop_date]   DATETIME    NULL,
    [cdate]       DATETIME    NULL,
    CONSTRAINT [PK_ttAssetsOil] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsOil', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'停止日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsOil', @level2type = N'COLUMN', @level2name = N'stop_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'活動日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsOil', @level2type = N'COLUMN', @level2name = N'active_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'卡ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsOil', @level2type = N'COLUMN', @level2name = N'card_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'油', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsOil', @level2type = N'COLUMN', @level2name = N'oil';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'aID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsOil', @level2type = N'COLUMN', @level2name = N'a_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產油表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsOil';

