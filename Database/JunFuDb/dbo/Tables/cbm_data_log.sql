﻿CREATE TABLE [dbo].[cbm_data_log] (
    [id]           INT           IDENTITY (1, 1) NOT NULL,
    [file_name]    VARCHAR (100) NULL,
    [file_row]     INT           NULL,
    [cdate]        DATETIME      NULL,
    [check_number] NVARCHAR (20) NULL,
    [length]       FLOAT (53)    NULL,
    [width]        FLOAT (53)    NULL,
    [height]       FLOAT (53)    NULL,
    [cbm]          FLOAT (53)    NULL,
    [s3_pic_uri]   VARCHAR (400) NULL,
    [s3_pic_uri_2] VARCHAR (400) NULL,
    [scan_time]    DATETIME      NULL,
    [data_source]  VARCHAR (30)  NULL,
    [scan_result]  VARCHAR (30)  NULL,
    [is_log]       BIT           NULL,
    [uuser]        VARCHAR (100) NULL,
    [udate]        DATETIME      NULL,
    CONSTRAINT [PK__cbm_data__3213E83F66760388] PRIMARY KEY CLUSTERED ([id] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IDX_check_number]
    ON [dbo].[cbm_data_log]([check_number] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'丈量時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'cbm_data_log', @level2type = N'COLUMN', @level2name = N'scan_time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'掃描結果', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'cbm_data_log', @level2type = N'COLUMN', @level2name = N'scan_result';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否為歷史檔案(true:代表已有更新狀態)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'cbm_data_log', @level2type = N'COLUMN', @level2name = N'is_log';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'cbm_data_log', @level2type = N'COLUMN', @level2name = N'cbm';

