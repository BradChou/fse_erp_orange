﻿CREATE TABLE [dbo].[tbRemoteSections] (
    [seq]              NUMERIC (18)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [area]             NVARCHAR (10)   NULL,
    [post_city]        NVARCHAR (10)   NULL,
    [post_area]        NVARCHAR (10)   NULL,
    [area_content]     NVARCHAR (50)   NULL,
    [road]             NVARCHAR (50)   NULL,
    [number_type]      CHAR (1)        NULL,
    [start_no]         INT             NULL,
    [end_no]           INT             NULL,
    [delivery_section] NVARCHAR (200)  NULL,
    [append_price]     NUMERIC (18, 2) NULL,
    CONSTRAINT [PK_tbRemoteSections] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'附加費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'append_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'delivery_section';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結束門牌號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'end_no';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'起始門牌號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'start_no';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'數字類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'number_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄送路段', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'road';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區域地點', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'area_content';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄送區域', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'post_area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄送縣市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'post_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections', @level2type = N'COLUMN', @level2name = N'seq';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'偏遠區段', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbRemoteSections';

