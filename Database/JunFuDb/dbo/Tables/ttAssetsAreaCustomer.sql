﻿CREATE TABLE [dbo].[ttAssetsAreaCustomer] (
    [id]            BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [area_id]       INT           NULL,
    [customer_code] NVARCHAR (10) NULL,
    [chk]           BIT           NULL,
    CONSTRAINT [PK_ttAssetsAreaCustomer] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'確認', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsAreaCustomer', @level2type = N'COLUMN', @level2name = N'chk';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsAreaCustomer', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區域編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsAreaCustomer', @level2type = N'COLUMN', @level2name = N'area_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsAreaCustomer', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產區域客戶', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssetsAreaCustomer';

