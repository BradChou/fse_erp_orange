﻿CREATE TABLE [dbo].[PickUppImportRecord](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountCode] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[CreateDate] [datetime] NULL
) ;


GO
