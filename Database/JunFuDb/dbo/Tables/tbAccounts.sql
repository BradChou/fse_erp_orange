﻿CREATE TABLE [dbo].[tbAccounts] (
    [account_id]    NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [account_code]  NVARCHAR (20) NULL,
    [password]      NVARCHAR (50) NULL,
    [active_flag]   BIT           NULL,
    [head_code]     NVARCHAR (3)  NULL,
    [body_code]     NVARCHAR (4)  NULL,
    [master_code]   NVARCHAR (7)  NULL,
    [manager_type]  CHAR (1)      NULL,
    [manager_unit]  NVARCHAR (20) NULL,
    [job_title]     NVARCHAR (20) NULL,
    [emp_code]      NVARCHAR (20) NULL,
    [user_name]     NVARCHAR (20) NULL,
    [user_email]    NVARCHAR (50) NULL,
    [cuser]         NVARCHAR (20) NULL,
    [cdate]         DATETIME      NULL,
    [uuser]         NVARCHAR (20) NULL,
    [udate]         DATETIME      NULL,
    [account_type]  BIT           NULL,
    [customer_code] NVARCHAR (20) NULL,
    CONSTRAINT [PK_tbAccounts] PRIMARY KEY CLUSTERED ([account_id] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顧客代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳戶類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'account_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者Email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'user_email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'user_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EMP代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'emp_code';






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'任務名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'job_title';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'管理單位', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'manager_unit';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'管理型別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'manager_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主要代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'master_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'次代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'body_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'首代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'head_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否活動', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'active_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'密碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'password';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts', @level2type = N'COLUMN', @level2name = N'account_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳戶', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAccounts';

