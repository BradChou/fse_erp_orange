﻿CREATE TABLE [dbo].[AccountUpload](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountDate] [datetime] NULL,
	[ExternalNumber] [nvarchar](50) NULL,
	[ReceivedAmount] [int] NULL,
	[CheckNumber] [nvarchar](50) NULL,
	[UploadTime] [datetime] NULL,
	[UploadUser] [nvarchar](50) NULL,
	[IsDel] [int] NULL,
	[UpdateTime] [datetime] NULL,
	[UpdateUser] [nvarchar](50) NULL
) ;


GO
