﻿CREATE TABLE [dbo].[SignImageTransferLog] (
    [id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [DataSource] VARCHAR (30)   NULL,
    [SourcePath] VARCHAR (100)  NULL,
    [DestPath]   VARCHAR (100)  NULL,
    [Status]     VARCHAR (30)   NOT NULL,
    [cdate]      DATETIME       NOT NULL,
    [Message]    NVARCHAR (200) NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'SignImageTransferLog', @level2type = N'COLUMN', @level2name = N'id';

