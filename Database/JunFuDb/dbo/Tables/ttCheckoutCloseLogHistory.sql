﻿CREATE TABLE [dbo].[ttCheckoutCloseLogHistory] (
    [log_id]        NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [customer_code] VARCHAR (10)  NULL,
    [supplier_code] VARCHAR (3)   NULL,
    [customer_type] CHAR (1)      NULL,
    [close_begdate] DATE          NULL,
    [close_enddate] DATE          NULL,
    [close_type]    CHAR (1)      NULL,
    [cdate]         DATE          NULL,
    [cuser]         NVARCHAR (10) NULL,
    CONSTRAINT [PK_ttCheckoutCloseLogHistory] PRIMARY KEY CLUSTERED ([log_id] ASC)
);

