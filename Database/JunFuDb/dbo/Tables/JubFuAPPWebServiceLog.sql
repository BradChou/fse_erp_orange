﻿CREATE TABLE [dbo].[JubFuAPPWebServiceLog](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[sessionid] [nvarchar](100) NULL,
	[type] [nvarchar](50) NULL,
	[action] [nvarchar](300) NULL,
	[message] [nvarchar](max) NULL,
	[cdate] [datetime] NULL,
 CONSTRAINT [PK_JubFuAPPWebServiceLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)
) ;


GO
