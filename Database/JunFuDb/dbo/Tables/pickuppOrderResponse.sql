﻿CREATE TABLE [dbo].[pickuppOrderResponse](
	[id] [nvarchar](50) NOT NULL,
	[order_number] [nvarchar](50) NOT NULL,
	[FSE_check_number] [nvarchar](50) NOT NULL,
	[FSE_sendTime] [datetime] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[sendflag] [bit] NOT NULL
) ;


GO
