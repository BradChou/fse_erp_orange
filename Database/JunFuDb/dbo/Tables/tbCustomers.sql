﻿CREATE TABLE [dbo].[tbCustomers] (
    [customer_id]           NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [supplier_code]         NVARCHAR (3)   NULL,
    [supplier_id]           NVARCHAR (4)   NULL,
    [master_code]           NVARCHAR (7)   NULL,
    [second_id]             NVARCHAR (2)   NULL,
    [pricing_code]          NVARCHAR (2)   NULL,
    [second_code]           NVARCHAR (4)   NULL,
    [customer_code]         NVARCHAR (11)  NULL,
    [customer_name]         NVARCHAR (50)  NULL,
    [customer_shortname]    NVARCHAR (20)  NULL,
    [customer_type]         CHAR (1)       NULL,
    [uniform_numbers]       NVARCHAR (8)   NULL,
    [shipments_principal]   NVARCHAR (20)  NULL,
    [telephone]             NVARCHAR (20)  NULL,
    [fax]                   NVARCHAR (20)  NULL,
    [shipments_city]        NVARCHAR (10)  NULL,
    [shipments_area]        NVARCHAR (10)  NULL,
    [shipments_road]        NVARCHAR (50)  NULL,
    [shipments_email]       NVARCHAR (50)  NULL,
    [stop_shipping_code]    CHAR (1)       NULL,
    [stop_shipping_memo]    NVARCHAR (100) NULL,
    [contract_effect_date]  DATE           NULL,
    [contract_expired_date] DATE           NULL,
    [contract_content]      NVARCHAR (255) NULL,
    [tariffs_effect_date]   DATE           NULL,
    [checkout_date]         INT            NULL,
    [tariffs_table]         NVARCHAR (255) NULL,
    [business_people]       NVARCHAR (20)  NULL,
    [customer_hcode]        NVARCHAR (11)  NULL,
    [contact_1]             NVARCHAR (20)  NULL,
    [email_1]               NVARCHAR (50)  NULL,
    [contact_2]             NVARCHAR (20)  NULL,
    [email_2]               NVARCHAR (50)  NULL,
    [billing_special_needs] NVARCHAR (50)  NULL,
    [ticket_period]         INT            NULL,
    [individual_fee]        INT            NULL,
    [cuser]                 NVARCHAR (10)  NULL,
    [cdate]                 DATETIME       NOT NULL,
    [contract_price]        INT            NULL,
    [type]                  BIT            NULL,
    [delivery_Type]         INT            NULL,
    CONSTRAINT [PK_tbCustomers] PRIMARY KEY CLUSTERED ([customer_id] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'型態', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'合約價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'contract_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'個人費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'individual_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'開票期間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'ticket_period';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'特別開票需求', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'billing_special_needs';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Email2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'email_2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'聯繫２', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'contact_2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Email 1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'email_1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'聯繫１', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'contact_1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顧客ｈ代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'customer_hcode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'商界人士', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'business_people';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'關稅表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'tariffs_table';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結帳日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'checkout_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'關稅效應日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'tariffs_effect_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'合約內容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'contract_content';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'合約到期日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'contract_expired_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'合約生效日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'contract_effect_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'停止裝貨備忘錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'stop_shipping_memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'停止裝貨代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'stop_shipping_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出貨Email', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'shipments_email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出貨路名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'shipments_road';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出貨地區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'shipments_area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出貨城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'shipments_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'傳真', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'fax';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'電話', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'telephone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主要出貨人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'shipments_principal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'統一編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'uniform_numbers';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顧客類型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'customer_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顧客簡稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'customer_shortname';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顧客姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'customer_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顧客代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'次要代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'second_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'價格代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'pricing_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'次要ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'second_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主要代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'master_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'supplier_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers', @level2type = N'COLUMN', @level2name = N'supplier_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顧客資料表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbCustomers';

