﻿CREATE TABLE [dbo].[ttReturnDeliveryPickupLog] (
    [seq]              BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [check_number]     NVARCHAR (20) NULL,
    [scan_item]        VARCHAR (2)   NULL,
    [scan_date]        DATETIME      NULL,
    [driver_code]      VARCHAR (20)  NULL,
    [sign_form_image]  NVARCHAR (50) NULL,
    [sign_field_image] NVARCHAR (50) NULL,
    [next_pickup_time] DATETIME      NULL,
    [cdate]            DATETIME      NULL,
    [re_code]          VARCHAR (2)   NULL,
    CONSTRAINT [PK_ttReturnDeliveryPickupLog] PRIMARY KEY CLUSTERED ([seq] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用者日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'下一次集貨時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog', @level2type = N'COLUMN', @level2name = N'next_pickup_time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'簽署區域圖檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog', @level2type = N'COLUMN', @level2name = N'sign_field_image';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'簽署表格圖檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog', @level2type = N'COLUMN', @level2name = N'sign_form_image';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog', @level2type = N'COLUMN', @level2name = N'driver_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'掃描日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog', @level2type = N'COLUMN', @level2name = N'scan_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'掃描項目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog', @level2type = N'COLUMN', @level2name = N'scan_item';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog', @level2type = N'COLUMN', @level2name = N'check_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog', @level2type = N'COLUMN', @level2name = N'seq';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'退還配送集貨紀錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttReturnDeliveryPickupLog';

