﻿CREATE TABLE [dbo].[tbSuppliers] (
    [supplier_id]        NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [supplier_code]      NVARCHAR (3)   NULL,
    [supplier_no]        NVARCHAR (10)  NULL,
    [supplier_name]      NVARCHAR (20)  NULL,
    [supplier_shortname] NVARCHAR (20)  NULL,
    [uniform_numbers]    NVARCHAR (8)   NULL,
    [id_no]              NVARCHAR (10)  NULL,
    [principal]          NVARCHAR (20)  NULL,
    [contact]            NVARCHAR (20)  NULL,
    [telephone]          NVARCHAR (20)  NULL,
    [city]               NVARCHAR (10)  NULL,
    [area]               NVARCHAR (10)  NULL,
    [road]               NVARCHAR (50)  NULL,
    [email]              NVARCHAR (50)  NULL,
    [receipt_number]     NVARCHAR (8)   NULL,
    [id_image]           NVARCHAR (255) NULL,
    [driver_image]       NVARCHAR (255) NULL,
    [account_image]      NVARCHAR (255) NULL,
    [contract_content]   NVARCHAR (255) NULL,
    [bank_code]          NVARCHAR (10)  NULL,
    [bank_name]          NVARCHAR (20)  NULL,
    [account]            VARCHAR (50)   NULL,
    [city_receipt]       NVARCHAR (10)  NULL,
    [area_receipt]       NVARCHAR (10)  NULL,
    [road_receipt]       NVARCHAR (50)  NULL,
    [warehouse]          VARCHAR (3)    NULL,
    [active_flag]        BIT            NULL,
    [show_trans]         BIT            NULL,
    [cross_region]       BIT            NULL,
    [cuser]              NVARCHAR (20)  NULL,
    [cdate]              DATETIME       NULL,
    [uuser]              NVARCHAR (20)  NULL,
    [udate]              DATETIME       NULL,
    CONSTRAINT [PK_tbSuppliers] PRIMARY KEY CLUSTERED ([supplier_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用者編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'跨區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'cross_region';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顯示運送', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'show_trans';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啟動標記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'active_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'倉庫代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'warehouse';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地址(收據)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'road_receipt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區(收據)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'area_receipt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'縣市(收據)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'city_receipt';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'銀行帳號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'account';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'銀行名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'bank_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'銀行代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'bank_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'契約內容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'contract_content';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'存摺圖檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'account_image';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'駕照圖檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'driver_image';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'身分證圖檔', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'id_image';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收據編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'receipt_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'電子信箱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'road';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'縣市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'電話號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'telephone';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'聯絡人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'contact';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'主要負責人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'principal';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'身分證字號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'id_no';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'統一編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'uniform_numbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商簡稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'supplier_shortname';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商全名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'supplier_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'supplier_no';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'supplier_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers', @level2type = N'COLUMN', @level2name = N'supplier_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbSuppliers';

