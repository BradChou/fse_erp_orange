﻿CREATE TABLE [dbo].[tbEmps] (
    [emp_id]        NUMERIC (18)  IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [customer_code] NVARCHAR (10) NULL,
    [emp_code]      NVARCHAR (20) NULL,
    [emp_name]      NVARCHAR (20) NULL,
    [job_type]      CHAR (1)      NULL,
    [active_flag]   BIT           NULL,
    [account_code]  NVARCHAR (20) NULL,
    [driver_code]   NVARCHAR (20) NULL,
    [station]       VARCHAR (10)  NULL,
    [udate]         DATETIME      NULL,
    [uuser]         NVARCHAR (20) NULL,
    [cdate]         DATETIME      NULL,
    [cuser]         NVARCHAR (20) NULL,
    CONSTRAINT [PK_tbEmp] PRIMARY KEY CLUSTERED ([emp_id] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'站所', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'station';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'driver_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'account_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否活動', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'active_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'工作型態', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'job_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EMP姓名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'emp_name';






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'EMP代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'emp_code';






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'顧客代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'創業管理規劃師', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbEmps';






GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-emp_code]
    ON [dbo].[tbEmps]([emp_code] ASC)
    INCLUDE([station]);


GO


