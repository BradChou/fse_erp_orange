﻿CREATE TABLE [dbo].[ScheduleTask] (
    [id]         BIGINT         IDENTITY (1, 1) NOT NULL,
    [TaskID]     INT            NOT NULL,
    [TaskName]   NVARCHAR (100) NOT NULL,
    [StartTime]  DATETIME2 (7)  NOT NULL,
    [Enable]     BIT            NOT NULL,
    [CreateDate] DATETIME       NOT NULL,
    CONSTRAINT [PK_ScheduleTask] PRIMARY KEY CLUSTERED ([id] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'建立日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScheduleTask', @level2type = N'COLUMN', @level2name = N'CreateDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'工作狀態', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScheduleTask', @level2type = N'COLUMN', @level2name = N'Enable';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'工作名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScheduleTask', @level2type = N'COLUMN', @level2name = N'TaskID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'PK', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ScheduleTask', @level2type = N'COLUMN', @level2name = N'id';

