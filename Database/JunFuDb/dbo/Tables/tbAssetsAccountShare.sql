﻿CREATE TABLE [dbo].[tbAssetsAccountShare] (
    [id]           BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [a_id]         INT           NULL,
    [car_license]  NVARCHAR (10) NULL,
    [account_dept] VARCHAR (2)   NULL,
    [share]        INT           NULL,
    [udate]        DATETIME      NULL,
    [uuser]        VARCHAR (10)  NULL,
    CONSTRAINT [PK_tbAssetsAccountShare] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改人員', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShare', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'修改時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShare', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'比例', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShare', @level2type = N'COLUMN', @level2name = N'share';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'分配月份(訂義待定)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShare', @level2type = N'COLUMN', @level2name = N'account_dept';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車牌', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShare', @level2type = N'COLUMN', @level2name = N'car_license';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產序號(ttAssets 主鍵)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShare', @level2type = N'COLUMN', @level2name = N'a_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產(貨車)分擔表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tbAssetsAccountShare';

