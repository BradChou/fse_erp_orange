﻿CREATE TABLE [dbo].[ttAssets_bak] (
    [a_id]                 BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [year]                 VARCHAR (4)    NULL,
    [month]                VARCHAR (2)    NULL,
    [car_license]          NVARCHAR (10)  NULL,
    [serial]               NVARCHAR (20)  NULL,
    [category]             NVARCHAR (10)  NULL,
    [improved_number]      NVARCHAR (10)  NULL,
    [assets_name]          NVARCHAR (50)  NULL,
    [unit]                 NVARCHAR (10)  NULL,
    [location]             NVARCHAR (20)  NULL,
    [quant]                INT            NULL,
    [get_date]             DATETIME       NULL,
    [get_price]            INT            NULL,
    [old_useful_life]      INT            NULL,
    [useful_life]          INT            NULL,
    [residual_value]       INT            NULL,
    [dept_id]              NVARCHAR (10)  NULL,
    [dept]                 NVARCHAR (20)  NULL,
    [ownership]            NVARCHAR (10)  NULL,
    [user]                 NVARCHAR (10)  NULL,
    [keeper]               NVARCHAR (10)  NULL,
    [business_model]       NVARCHAR (50)  NULL,
    [acntid]               NVARCHAR (10)  NULL,
    [supplier_id]          NVARCHAR (10)  NULL,
    [supplier]             NVARCHAR (20)  NULL,
    [brand]                NVARCHAR (20)  NULL,
    [driver]               NVARCHAR (10)  NULL,
    [tonnes]               NVARCHAR (10)  NULL,
    [car_retailer]         VARCHAR (20)   NULL,
    [owner]                NVARCHAR (10)  NULL,
    [registration]         BIT            NULL,
    [engine_number]        VARCHAR (30)   NULL,
    [cc]                   INT            NULL,
    [age]                  INT            NULL,
    [tires_number]         INT            NULL,
    [cabin_type]           VARCHAR (10)   NULL,
    [tailgate]             BIT            NULL,
    [oil]                  NVARCHAR (20)  NULL,
    [oil_discount]         FLOAT (53)     NULL,
    [ETC]                  NVARCHAR (10)  NULL,
    [tires_monthlyfee]     INT            NULL,
    [license_tax]          INT            NULL,
    [fuel_tax]             INT            NULL,
    [rent]                 INT            NULL,
    [GPS]                  VARCHAR (2)    NULL,
    [Gear]                 NVARCHAR (10)  NULL,
    [account]              NVARCHAR (50)  NULL,
    [account_name]         NVARCHAR (10)  NULL,
    [temperate]            NVARCHAR (10)  NULL,
    [vision_assist]        BIT            NULL,
    [next_inspection_date] DATETIME       NULL,
    [stop_type]            VARCHAR (2)    NULL,
    [stop_date]            DATETIME       NULL,
    [purchase_date]        DATETIME       NULL,
    [memo]                 NVARCHAR (200) NULL,
    [cuser]                NVARCHAR (20)  NULL,
    [cdate]                DATETIME       NULL,
    [uuser]                NVARCHAR (20)  NULL,
    [udate]                DATETIME       NULL,
    CONSTRAINT [PK_ttAssets] PRIMARY KEY CLUSTERED ([a_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'備忘錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'購買日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'purchase_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'停止日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'stop_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'停止型態', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'stop_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'下一檢查日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'next_inspection_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'輔助視力', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'vision_assist';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'溫帶的', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'temperate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳戶名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'account_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'帳戶', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'account';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'齒輪', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'Gear';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'GPS', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'GPS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出租', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'rent';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'燃油稅', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'fuel_tax';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'執照稅', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'license_tax';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'輪胎月費', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'tires_monthlyfee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ETC', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'ETC';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'油折扣', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'oil_discount';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'油', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'oil';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'尾門', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'tailgate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'艙別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'cabin_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'輪胎數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'tires_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'年齡', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'age';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'cc', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'cc';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'引擎號碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'engine_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否註冊', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'registration';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'擁有者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'owner';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'車零售商', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'car_retailer';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'噸數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'tonnes';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'司機', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'driver';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'商標', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'brand';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'supplier';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'區配商ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'supplier_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'痤瘡', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'acntid';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'商業模式', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'business_model';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'保管人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'keeper';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'user';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'所有權', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'ownership';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'部門', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'dept';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'部門ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'dept_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'剩餘價值', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'residual_value';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'有用生命週期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'useful_life';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'過去有用生命週期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'old_useful_life';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'獲取價格', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'get_price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'獲取日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'get_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'何時', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'quant';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'地點', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'location';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'單位', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'unit';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'assets_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'改善數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'improved_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'category';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'系列', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'serial';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'駕照', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'car_license';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'月份', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'month';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'年度', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak', @level2type = N'COLUMN', @level2name = N'year';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資產貝克', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttAssets_bak';

