﻿CREATE TABLE [dbo].[ttPlletlog] (
    [id]         INT    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [pallet_id]  INT    NULL,
    [request_id] BIGINT NULL,
    CONSTRAINT [PK_ttPlletlog] PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'請求編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletlog', @level2type = N'COLUMN', @level2name = N'request_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'棧板編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletlog', @level2type = N'COLUMN', @level2name = N'pallet_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletlog', @level2type = N'COLUMN', @level2name = N'id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'棧板編號紀錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPlletlog';

