﻿CREATE TABLE [dbo].[ttInsuranceRecord] (
    [id]           BIGINT        IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [Insurance_id] INT           NULL,
    [year]         INT           NOT NULL,
    [month]        INT           NOT NULL,
    [price]        INT           NULL,
    [ispay]        BIT           NULL,
    [memo]         NVARCHAR (50) NULL,
    CONSTRAINT [PK_ttInsuranceRecord] PRIMARY KEY CLUSTERED ([id] ASC)
);

