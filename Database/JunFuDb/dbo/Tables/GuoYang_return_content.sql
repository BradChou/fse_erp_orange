﻿CREATE TABLE [dbo].[GuoYang_return_content] (
    [id]                   INT            IDENTITY (1, 1) NOT NULL,
    [number]               INT            NULL,
    [guoyang_check_number] VARCHAR (30)   NULL,
    [receive_contact]      NVARCHAR (40)  NULL,
    [receive_address]      NVARCHAR (120) NULL,
    [receive_tel]          NVARCHAR (20)  NULL,
    [collection_money]     INT            NULL,
    [detail_check_number]  VARCHAR (30)   NULL,
    [piece]                INT            NULL,
    [file_name]            NVARCHAR (100) NULL,
    [cdate]                DATETIME       NULL,
    [schedule_id]          INT            NULL
);






GO
CREATE UNIQUE CLUSTERED INDEX [PK__GuoYangReturnContent]
    ON [dbo].[GuoYang_return_content]([id] ASC);

