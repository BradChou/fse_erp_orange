﻿CREATE TABLE [dbo].[ttDeliveryFile] (
    [log_id]       BIGINT         IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [request_id]   BIGINT         NULL,
    [check_number] NVARCHAR (20)  NULL,
    [file_type]    NVARCHAR (2)   NULL,
    [file_path]    NVARCHAR (100) NULL,
    [file_name]    NVARCHAR (100) NULL,
    [udate]        DATETIME       NULL,
    [uuser]        NVARCHAR (10)  NULL,
    CONSTRAINT [PK_ttDeliveryFile] PRIMARY KEY CLUSTERED ([log_id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryFile', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryFile', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨物名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryFile', @level2type = N'COLUMN', @level2name = N'file_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨物狀態網頁', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryFile', @level2type = N'COLUMN', @level2name = N'file_path';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨物狀態：1: (全都是1)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryFile', @level2type = N'COLUMN', @level2name = N'file_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryFile', @level2type = N'COLUMN', @level2name = N'check_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'需求編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryFile', @level2type = N'COLUMN', @level2name = N'request_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'登入ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryFile', @level2type = N'COLUMN', @level2name = N'log_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發送清單', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttDeliveryFile';

