﻿CREATE TABLE [dbo].[cbm_data_log_record] (
    [record_id]     INT           IDENTITY (1, 1) NOT NULL,
    [data_source]   VARCHAR (30)  NULL,
    [file_name]     VARCHAR (100) NULL,
    [cdate]         DATETIME      NULL,
    [is_success]    BIT           NULL,
    [complete_date] DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([record_id] ASC)
);

