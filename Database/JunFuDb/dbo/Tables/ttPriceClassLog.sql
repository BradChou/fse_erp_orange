﻿CREATE TABLE [dbo].[ttPriceClassLog] (
    [seq]         NUMERIC (18)    IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [class_level] INT             NULL,
    [week1]       NUMERIC (10, 2) NULL,
    [week2]       NUMERIC (10, 2) NULL,
    [week3]       NUMERIC (10, 2) NULL,
    [week4]       NUMERIC (10, 2) NULL,
    [week5]       NUMERIC (10, 2) NULL,
    [week6]       NUMERIC (10, 2) NULL,
    [arrive]      NUMERIC (10, 2) NULL,
    [notify_flag] CHAR (1)        NULL,
    [notify_time] DATETIME        NULL,
    [active_date] DATE            NULL,
    [cdate]       DATETIME        NOT NULL,
    [cuser]       NVARCHAR (20)   NULL,
    CONSTRAINT [PK_ttPriceClassLog] PRIMARY KEY CLUSTERED ([seq] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'廠商', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'公司使用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'啟用日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'active_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'通知時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'notify_time';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'通知標記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'notify_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到達', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'arrive';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'第六週', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'week6';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'第五週', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'week5';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'第四週', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'week4';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'第三週', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'week3';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'第二週', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'week2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'第一週', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'week1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'層級', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'class_level';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'序號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog', @level2type = N'COLUMN', @level2name = N'seq';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'價格分級紀錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ttPriceClassLog';

