﻿CREATE TABLE [dbo].[tcDeliveryRequests] (
    [request_id]                          NUMERIC (18)   IDENTITY (1, 1) NOT FOR REPLICATION NOT NULL,
    [pricing_type]                        NVARCHAR (2)   NULL,
    [customer_code]                       NVARCHAR (20)  NULL,
    [check_number]                        NVARCHAR (20)  NULL,
    [check_type]                          CHAR (10)      NULL,
    [order_number]                        NVARCHAR (32)  NULL,
    [receive_customer_code]               NVARCHAR (20)  NULL,
    [subpoena_category]                   NVARCHAR (10)  NULL,
    [receive_tel1]                        NVARCHAR (20)  NULL,
    [receive_tel1_ext]                    NVARCHAR (10)  NULL,
    [receive_tel2]                        NVARCHAR (20)  NULL,
    [receive_contact]                     NVARCHAR (150) NULL,
    [receive_zip]                         VARCHAR (6)    NULL,
    [receive_city]                        NVARCHAR (10)  NULL,
    [receive_area]                        NVARCHAR (10)  NULL,
    [receive_address]                     NVARCHAR (255) NULL,
    [area_arrive_code]                    NVARCHAR (15)  NULL,
    [receive_by_arrive_site_flag]         BIT            NULL,
    [arrive_address]                      NVARCHAR (100) NULL,
    [pieces]                              INT            NULL,
    [plates]                              INT            NULL,
    [cbm]                                 INT            NULL,
    [CbmSize]                             INT            NULL,
    [collection_money]                    INT            NULL,
    [arrive_to_pay_freight]               INT            NULL,
    [arrive_to_pay_append]                INT            NULL,
    [send_contact]                        NVARCHAR (40)  NULL,
    [send_tel]                            NVARCHAR (40)  NULL,
    [send_zip]                            VARCHAR (6)    NULL,
    [send_city]                           NVARCHAR (10)  NULL,
    [send_area]                           NVARCHAR (10)  NULL,
    [send_address]                        NVARCHAR (150) NULL,
    [donate_invoice_flag]                 BIT            NULL,
    [electronic_invoice_flag]             BIT            NULL,
    [uniform_numbers]                     NVARCHAR (8)   NULL,
    [arrive_mobile]                       NVARCHAR (20)  NULL,
    [arrive_email]                        NVARCHAR (50)  NULL,
    [invoice_memo]                        NVARCHAR (100) NULL,
    [invoice_desc]                        NVARCHAR (200) NULL,
    [product_category]                    NVARCHAR (10)  NULL,
    [special_send]                        NVARCHAR (10)  NULL,
    [arrive_assign_date]                  DATETIME       NULL,
    [time_period]                         NVARCHAR (10)  NULL,
    [receipt_flag]                        BIT            NULL,
    [pallet_recycling_flag]               BIT            NULL,
    [Pallet_type]                         VARCHAR (2)    NULL,
    [supplier_code]                       NVARCHAR (3)   NULL,
    [supplier_name]                       NVARCHAR (20)  NULL,
    [supplier_date]                       DATETIME       NULL,
    [receipt_numbe]                       INT            NULL,
    [supplier_fee]                        INT            NULL,
    [csection_fee]                        INT            NULL,
    [remote_fee]                          INT            NULL,
    [total_fee]                           INT            NULL,
    [print_date]                          DATETIME       NULL,
    [print_flag]                          BIT            NULL,
    [checkout_close_date]                 DATETIME       NULL,
    [add_transfer]                        BIT            NULL,
    [sub_check_number]                    VARCHAR (3)    NULL,
    [import_randomCode]                   NVARCHAR (50)  NULL,
    [close_randomCode]                    NVARCHAR (10)  NULL,
    [turn_board]                          BIT            NULL,
    [upstairs]                            BIT            NULL,
    [difficult_delivery]                  BIT            NULL,
    [turn_board_fee]                      INT            NULL,
    [upstairs_fee]                        INT            NULL,
    [difficult_fee]                       INT            NULL,
    [cancel_date]                         DATETIME       NULL,
    [cuser]                               NVARCHAR (20)  NULL,
    [cdate]                               DATETIME       NULL,
    [uuser]                               NVARCHAR (20)  NULL,
    [udate]                               DATETIME       NULL,
    [HCTstatus]                           INT            NULL,
    [is_pallet]                           BIT            CONSTRAINT [DF_tcDeliveryRequests_is_pallet] DEFAULT ((0)) NULL,
    [pallet_request_id]                   BIGINT         CONSTRAINT [DF_tcDeliveryRequests_pallet_request_id_1] DEFAULT ((0)) NULL,
    [Less_than_truckload]                 BIT            CONSTRAINT [DF_tcDeliveryRequests_Less_than_truckload] DEFAULT ((0)) NULL,
    [Distributor]                         NVARCHAR (50)  NULL,
    [temperate]                           NVARCHAR (10)  NULL,
    [DeliveryType]                        VARCHAR (10)   CONSTRAINT [DF_tcDeliveryRequests_DeliveryType_1] DEFAULT ('D') NULL,
    [cbmLength]                           FLOAT (53)     NULL,
    [cbmWidth]                            FLOAT (53)     NULL,
    [cbmHeight]                           FLOAT (53)     NULL,
    [cbmWeight]                           FLOAT (53)     NULL,
    [cbmCont]                             FLOAT (53)     NULL,
    [bagno]                               NVARCHAR (20)  NULL,
    [ArticleNumber]                       NVARCHAR (20)  NULL,
    [SendPlatform]                        NVARCHAR (20)  NULL,
    [ArticleName]                         NVARCHAR (20)  NULL,
    [round_trip]                          BIT            CONSTRAINT [DF_tcDeliveryRequests_round_trip] DEFAULT ((0)) NULL,
    [ship_date]                           DATETIME       NULL,
    [VoucherMoney]                        INT            NULL,
    [CashMoney]                           INT            NULL,
    [pick_up_good_type]                   NVARCHAR (10)  NULL,
    [latest_scan_date]                    DATETIME       NULL,
    [delivery_complete_date]              DATETIME       NULL,
    [pic_path]                            VARCHAR (MAX)  NULL,
    [send_station_scode]                  NVARCHAR (15)  NULL,
    [Is_write_off]                        BIT            NULL,
    [return_check_number]                 VARCHAR (20)   NULL,
    [freight]                             INT            NULL,
    [latest_dest_date]                    DATETIME       NULL,
    [latest_delivery_date]                DATETIME       NULL,
    [latest_arrive_option_date]           DATETIME       NULL,
    [latest_dest_driver]                  NVARCHAR (10)  NULL,
    [latest_delivery_driver]              NVARCHAR (10)  NULL,
    [latest_arrive_option_driver]         NVARCHAR (10)  NULL,
    [latest_arrive_option]                NVARCHAR (20)  NULL,
    [delivery_date]                       DATETIME       NULL,
    [payment_method]                      NVARCHAR (15)  NULL,
    [catch_cbm_pic_path_from_S3]          VARCHAR (MAX)  NULL,
    [catch_taichung_cbm_pic_path_from_S3] VARCHAR (MAX)  NULL,
    [latest_scan_item]                    VARCHAR (2)    NULL,
    [latest_scan_arrive_option]           NVARCHAR (20)  NULL,
    [latest_scan_driver_code]             NVARCHAR (10)  NULL,
    [latest_dest_exception_option]        NVARCHAR (20)  NULL,
    [holiday_delivery]                    BIT            NULL,
    [roundtrip_checknumber]               NVARCHAR (20)  NULL,
    [closing_date]                        DATETIME       NULL,
    [custom_label]                        INT            NULL,
    [latest_pick_up_scan_log_id]          INT            NULL,
    [orange_r1_1_uuser]                   NVARCHAR (20)  NULL,
    [sign_paper_print_flag]               BIT            NULL,
    [ProductId]                           NVARCHAR (50)  NULL,
    [SpecCodeId]                          NVARCHAR (50)  NULL,
    [ShuttleStationCode]                  VARCHAR (5)    NULL,
    [ProductValue]                        INT            NULL,
    [SpecialAreaFee]                      INT            NULL,
    [SpecialAreaId]                       INT            NULL,
    [SendCode]                            NVARCHAR (10)  NULL,
    [SendSD]                              NVARCHAR (10)  NULL,
    [SendMD]                              NVARCHAR (10)  NULL,
    [ReceiveCode]                         NVARCHAR (10)  NULL,
    [ReceiveSD]                           NVARCHAR (10)  NULL,
    [ReceiveMD]                           NVARCHAR (10)  NULL,
    CONSTRAINT [PK_tcDeliveyRequests] PRIMARY KEY CLUSTERED ([request_id] ASC)
);


















GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-send_contact]
    ON [dbo].[tcDeliveryRequests]([send_contact] ASC)
    INCLUDE([check_number], [supplier_code]);


GO



GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-order_number]
    ON [dbo].[tcDeliveryRequests]([order_number] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-check_number]
    ON [dbo].[tcDeliveryRequests]([check_number] ASC);


GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'來回件', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'round_trip';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'品名', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'ArticleName';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出貨平台', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'SendPlatform';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'產品編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'ArticleNumber';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'袋號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'bagno';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積內容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'cbmCont';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積重', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'cbmWeight';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積高', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'cbmHeight';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積寬', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'cbmWidth';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積長', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'cbmLength';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送類型 D:正物流  R:逆物流', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'DeliveryType';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送溫層 (01:一般 02:冷凍 03:冷藏) 關連tbItemCode', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'temperate';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送商(全速配、宅配通) (關連 tbItemCodes where code_bclass = ''7'' and code_sclass=''distributor'')', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'Distributor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'0：棧板運輸  1：零擔運輸', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'Less_than_truckload';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'托盤需求ID', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'pallet_request_id';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'回板託運單', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'is_pallet';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'HCT狀態：0, 1, 2, 3, 4, 5', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'HCTstatus';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'udate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'uuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'cdate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'使用者', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'cuser';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'取消日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'cancel_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'困難費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'difficult_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'上樓費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'upstairs_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'轉板費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'turn_board_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否配送困難', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'difficult_delivery';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'上樓', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'upstairs';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'轉板', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'turn_board';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'關門隨機碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'close_randomCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'進口隨機碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'import_randomCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'子貨號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'sub_check_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否加入移倉', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'add_transfer';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結帳關門時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'checkout_close_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否打印', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'print_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'打印時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'print_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'合計費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'total_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'遠程費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'remote_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配商費用', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'supplier_fee';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收據單號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receipt_numbe';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配商日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'supplier_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配商名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'supplier_name';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配商代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'supplier_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'棧板種類( 關連 tbItemCodes)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'Pallet_type';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨盤是否回收', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'pallet_recycling_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否開收據', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receipt_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'期間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'time_period';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到達日期', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'arrive_assign_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'全NULL or 0', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'special_send';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'產品類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'product_category';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發票說明', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'invoice_desc';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發票備忘錄', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'invoice_memo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'全NULL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'arrive_email';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'全NULL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'arrive_mobile';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'全NULL', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'uniform_numbers';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否為電子發票', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'electronic_invoice_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否捐贈發票', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'donate_invoice_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件人地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'send_address';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件人地區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'send_area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件人城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'send_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件人郵遞區號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'send_zip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件人電話', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'send_tel';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'寄件人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'send_contact';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'到貨運費', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'arrive_to_pay_freight';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收貨款', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'collection_money';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積大小 (關連tbCbmSize.id )', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'CbmSize';




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'材積', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'cbm';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'板數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'plates';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'件數', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'pieces';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'站所地址?', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'arrive_address';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'是否到站所', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_by_arrive_site_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'配送站所', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'area_arrive_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人地址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_address';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人地區', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_area';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人城市', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_city';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人郵遞區號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_zip';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_contact';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人電話2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_tel2';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人電話1分機', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_tel1_ext';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人電話1', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_tel1';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'傳票類別', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'subpoena_category';






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'收件人代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'receive_customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'訂單編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'order_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'類型', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'check_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'貨號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'check_number';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'客戶代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'customer_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'發送總表', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests';


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-supplier_code-print_date-DeliveryType-customer_code]
    ON [dbo].[tcDeliveryRequests]([print_date] ASC, [supplier_code] ASC, [DeliveryType] ASC, [customer_code] ASC, [pricing_type] ASC, [Less_than_truckload] ASC)
    INCLUDE([subpoena_category], [area_arrive_code], [send_city], [send_area], [special_send]);


GO



GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-latest_scan_date-supplier_code-area_arrive_code]
    ON [dbo].[tcDeliveryRequests]([latest_scan_date] ASC, [supplier_code] ASC, [area_arrive_code] ASC)
    INCLUDE([check_number], [delivery_complete_date]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-delivery_complete_date-supplier_code-area_arrive_code]
    ON [dbo].[tcDeliveryRequests]([delivery_complete_date] ASC, [supplier_code] ASC, [area_arrive_code] ASC)
    INCLUDE([check_number], [latest_scan_date]);


GO



GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-supplier_code]
    ON [dbo].[tcDeliveryRequests]([supplier_code] ASC);


GO



GO



GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-Less_than_truckload]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload] ASC, [print_date] ASC, [DeliveryType] ASC);








GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-latest_dest_date]
    ON [dbo].[tcDeliveryRequests]([latest_dest_date] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-CompoundCondition]
    ON [dbo].[tcDeliveryRequests]([print_date] ASC, [cancel_date] ASC, [Less_than_truckload] ASC);


GO



GO



GO



GO
CREATE STATISTICS [_dta_stat_566293077_8_79_70_76_68_27_4]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [DeliveryType], [cdate], [Less_than_truckload], [cancel_date], [send_contact], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_79_47_70_76_1]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [DeliveryType], [supplier_code], [cdate], [Less_than_truckload], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_79_2_55_76_47_17]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [DeliveryType], [pricing_type], [print_date], [Less_than_truckload], [supplier_code], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_79_2_3]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [DeliveryType], [pricing_type], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_76_3]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [Less_than_truckload], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_70_55_56_1]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [cdate], [print_date], [print_flag], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_60_76_1]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [import_randomCode], [Less_than_truckload], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_60_1_3_70]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [import_randomCode], [request_id], [customer_code], [cdate]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_56_1]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [print_flag], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_55_76_3_1_30_31]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [print_date], [Less_than_truckload], [customer_code], [request_id], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_47_55_3]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [supplier_code], [print_date], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_47_30_31_1_4_27_76_68_79_70]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [supplier_code], [send_city], [send_area], [request_id], [check_number], [send_contact], [Less_than_truckload], [cancel_date], [DeliveryType], [cdate]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_47_30_31_1_27_76_68_79]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [supplier_code], [send_city], [send_area], [request_id], [send_contact], [Less_than_truckload], [cancel_date], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_47_2_55_76]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [supplier_code], [pricing_type], [print_date], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_41_2_17_55_76_1_12_27_4_3]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [special_send], [pricing_type], [area_arrive_code], [print_date], [Less_than_truckload], [request_id], [receive_contact], [send_contact], [check_number], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_41_17_55_2_76_27_4_1]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [special_send], [area_arrive_code], [print_date], [pricing_type], [Less_than_truckload], [send_contact], [check_number], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_41_17_2_55_4_76_1_14_15_47_30_31]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [special_send], [area_arrive_code], [pricing_type], [print_date], [check_number], [Less_than_truckload], [request_id], [receive_city], [receive_area], [supplier_code], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_4_2]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [check_number], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_79_76_17]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [DeliveryType], [Less_than_truckload], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_76_4_17_47]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [Less_than_truckload], [check_number], [area_arrive_code], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_55_76_79_1_47]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [print_date], [Less_than_truckload], [DeliveryType], [request_id], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_55_70_56]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [print_date], [cdate], [print_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_55_47_4_76_79]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [print_date], [supplier_code], [check_number], [Less_than_truckload], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_55_4_76_79]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [print_date], [check_number], [Less_than_truckload], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_55_47_4_76_79_17]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [print_date], [supplier_code], [check_number], [Less_than_truckload], [DeliveryType], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_76_68_1_30]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [cancel_date], [request_id], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_76_68_1_27_17_4_30]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [cancel_date], [request_id], [send_contact], [area_arrive_code], [check_number], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_76_68_1_17]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [cancel_date], [request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_76_27_7_17_12_20_22]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [send_contact], [receive_customer_code], [area_arrive_code], [receive_contact], [pieces], [cbm]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_76_27_4_7_17_12_20]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [send_contact], [check_number], [receive_customer_code], [area_arrive_code], [receive_contact], [pieces]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_76_27_4_12_20_68]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [send_contact], [check_number], [receive_contact], [pieces], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_76_27_12_20]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [send_contact], [receive_contact], [pieces]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_55_76_68_1_30]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [print_date], [Less_than_truckload], [cancel_date], [request_id], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_55_76_27_7_17_12_20_22]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [print_date], [Less_than_truckload], [send_contact], [receive_customer_code], [area_arrive_code], [receive_contact], [pieces], [cbm]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_2_3_47_55_76_27_4_7_17_12_20]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [pricing_type], [customer_code], [supplier_code], [print_date], [Less_than_truckload], [send_contact], [check_number], [receive_customer_code], [area_arrive_code], [receive_contact], [pieces]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_17_47_3_4]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [area_arrive_code], [supplier_code], [customer_code], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_1_70_68_56]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [request_id], [cdate], [cancel_date], [print_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_1_68]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [request_id], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_1_4_30_31_71]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [request_id], [check_number], [send_city], [send_area], [uuser]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_1_30_31_71]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [request_id], [send_city], [send_area], [uuser]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_1_3_47_55]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [request_id], [customer_code], [supplier_code], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_8_1_17_2_79]
    ON [dbo].[tcDeliveryRequests]([subpoena_category], [request_id], [area_arrive_code], [pricing_type], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_8_17_47_3_4_76]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [subpoena_category], [area_arrive_code], [supplier_code], [customer_code], [check_number], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_8_17_47_3_4_2]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [subpoena_category], [area_arrive_code], [supplier_code], [customer_code], [check_number], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_76_68_1_55_47_17_4]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [Less_than_truckload], [cancel_date], [request_id], [print_date], [supplier_code], [area_arrive_code], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_76_68_1_47]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [Less_than_truckload], [cancel_date], [request_id], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_76_68_1_17]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [Less_than_truckload], [cancel_date], [request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_76_3_55_1]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [Less_than_truckload], [customer_code], [print_date], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_70_76_68_27_4_1_47]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [cdate], [Less_than_truckload], [cancel_date], [send_contact], [check_number], [request_id], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_70_76_68_27_4_1_30_31_8]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [cdate], [Less_than_truckload], [cancel_date], [send_contact], [check_number], [request_id], [send_city], [send_area], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_70_47_76_68_27_4_1_30]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [cdate], [supplier_code], [Less_than_truckload], [cancel_date], [send_contact], [check_number], [request_id], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_47_70_76_1_68_30]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [supplier_code], [cdate], [Less_than_truckload], [request_id], [cancel_date], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_47_17_8_68]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [supplier_code], [area_arrive_code], [subpoena_category], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_30_31_47_8]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [send_city], [send_area], [supplier_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_3_55_47_76_1_2_8_30_31]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [customer_code], [print_date], [supplier_code], [Less_than_truckload], [request_id], [pricing_type], [subpoena_category], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_2_3_1_8_76_55_47]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [pricing_type], [customer_code], [request_id], [subpoena_category], [Less_than_truckload], [print_date], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_2_1_41_8_47_30_31_17]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [pricing_type], [request_id], [special_send], [subpoena_category], [supplier_code], [send_city], [send_area], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_2_1_30_31_47]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [pricing_type], [request_id], [send_city], [send_area], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_2_1_17]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [pricing_type], [request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_79_17_8_3_47_55_1_76_2]
    ON [dbo].[tcDeliveryRequests]([DeliveryType], [area_arrive_code], [subpoena_category], [customer_code], [supplier_code], [print_date], [request_id], [Less_than_truckload], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_8_60_70_1_3]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [subpoena_category], [import_randomCode], [cdate], [request_id], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_8_47_30_31_68_79_70_39_27_4]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [subpoena_category], [supplier_code], [send_city], [send_area], [cancel_date], [DeliveryType], [cdate], [invoice_desc], [send_contact], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_8_47_30_31_4_1]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [subpoena_category], [supplier_code], [send_city], [send_area], [check_number], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_8_17_47_3_4_2]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [subpoena_category], [area_arrive_code], [supplier_code], [customer_code], [check_number], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_68_79_70_39_47_8_30_31_1_27]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [cancel_date], [DeliveryType], [cdate], [invoice_desc], [supplier_code], [subpoena_category], [send_city], [send_area], [request_id], [send_contact]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_68_79_47]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [cancel_date], [DeliveryType], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_6]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [order_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_55_79_47_3_30_31_17_41_2]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [print_date], [DeliveryType], [supplier_code], [customer_code], [send_city], [send_area], [area_arrive_code], [special_send], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_55_4]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [print_date], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_55_17_68_2_41_8]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [print_date], [area_arrive_code], [cancel_date], [pricing_type], [special_send], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_55_17_2_8_1_3_41]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [print_date], [area_arrive_code], [pricing_type], [subpoena_category], [request_id], [customer_code], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_55_17_2_4_12_27_68_3_8_41_47]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [print_date], [area_arrive_code], [pricing_type], [check_number], [receive_contact], [send_contact], [cancel_date], [customer_code], [subpoena_category], [special_send], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_55_17_2_4_12_27_3_8_47_1_49_41]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [print_date], [area_arrive_code], [pricing_type], [check_number], [receive_contact], [send_contact], [customer_code], [subpoena_category], [supplier_code], [request_id], [supplier_date], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_55_1_47_3_30_31_17]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [print_date], [request_id], [supplier_code], [customer_code], [send_city], [send_area], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_47_30_31_17_41_3_8]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [supplier_code], [send_city], [send_area], [area_arrive_code], [special_send], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_47_30_31_17_41_2_3_8_55_79]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [supplier_code], [send_city], [send_area], [area_arrive_code], [special_send], [pricing_type], [customer_code], [subpoena_category], [print_date], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_47_17_4_1_2_68_79_55]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [supplier_code], [area_arrive_code], [check_number], [request_id], [pricing_type], [cancel_date], [DeliveryType], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_47_17]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [supplier_code], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_4_1]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [check_number], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_30_31_71_3_8_55]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [send_city], [send_area], [uuser], [customer_code], [subpoena_category], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_30_31_47_8_2_79_55]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [send_city], [send_area], [supplier_code], [subpoena_category], [pricing_type], [DeliveryType], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_30_31_17_40_3]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [send_city], [send_area], [area_arrive_code], [product_category], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_3_60_8]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [customer_code], [import_randomCode], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_2_68_79_47_17_4]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [pricing_type], [cancel_date], [DeliveryType], [supplier_code], [area_arrive_code], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_2]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_17_8_3_47_55]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [area_arrive_code], [subpoena_category], [customer_code], [supplier_code], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_17_8_2]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [area_arrive_code], [subpoena_category], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_17_47_8_4]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [area_arrive_code], [supplier_code], [subpoena_category], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_17_3_8_41_47]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_17_2_3_41_8_55]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [area_arrive_code], [pricing_type], [customer_code], [special_send], [subpoena_category], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_14_15_47_30_31_17_3_8_41_2]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_76_14_15_47_17_3_8_41]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload], [receive_city], [receive_area], [supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_79_76_68_27_4_1_8_47_30]
    ON [dbo].[tcDeliveryRequests]([cdate], [DeliveryType], [Less_than_truckload], [cancel_date], [send_contact], [check_number], [request_id], [subpoena_category], [supplier_code], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_79_47_4_76_68]
    ON [dbo].[tcDeliveryRequests]([cdate], [DeliveryType], [supplier_code], [check_number], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_76_3_60_8]
    ON [dbo].[tcDeliveryRequests]([cdate], [Less_than_truckload], [customer_code], [import_randomCode], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_68_56_55_8]
    ON [dbo].[tcDeliveryRequests]([cdate], [cancel_date], [print_flag], [print_date], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_68_56_3_55_8]
    ON [dbo].[tcDeliveryRequests]([cdate], [cancel_date], [print_flag], [customer_code], [print_date], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_55_56_1_68_8]
    ON [dbo].[tcDeliveryRequests]([cdate], [print_date], [print_flag], [request_id], [cancel_date], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_4_27_76_55_3]
    ON [dbo].[tcDeliveryRequests]([cdate], [check_number], [send_contact], [Less_than_truckload], [print_date], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_3_55_56_68]
    ON [dbo].[tcDeliveryRequests]([cdate], [customer_code], [print_date], [print_flag], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_3_1_56_68]
    ON [dbo].[tcDeliveryRequests]([cdate], [customer_code], [request_id], [print_flag], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_2_68_56_3_8]
    ON [dbo].[tcDeliveryRequests]([cdate], [pricing_type], [cancel_date], [print_flag], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_2_56_68]
    ON [dbo].[tcDeliveryRequests]([cdate], [pricing_type], [print_flag], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_2_55_56_68_3]
    ON [dbo].[tcDeliveryRequests]([cdate], [pricing_type], [print_date], [print_flag], [cancel_date], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_1_8_47_30_31_76_68_79_27]
    ON [dbo].[tcDeliveryRequests]([cdate], [request_id], [subpoena_category], [supplier_code], [send_city], [send_area], [Less_than_truckload], [cancel_date], [DeliveryType], [send_contact]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_1_8_47_30_31_4_76_68]
    ON [dbo].[tcDeliveryRequests]([cdate], [request_id], [subpoena_category], [supplier_code], [send_city], [send_area], [check_number], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_1_4_55_76_3]
    ON [dbo].[tcDeliveryRequests]([cdate], [request_id], [check_number], [print_date], [Less_than_truckload], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_1_3]
    ON [dbo].[tcDeliveryRequests]([cdate], [request_id], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_70_1_2_56_3_68]
    ON [dbo].[tcDeliveryRequests]([cdate], [request_id], [pricing_type], [print_flag], [customer_code], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_8_47_30_31_76_79_70_27_4]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [subpoena_category], [supplier_code], [send_city], [send_area], [Less_than_truckload], [DeliveryType], [cdate], [send_contact], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_8_47_30_31_4_1_76_79_70_39_27]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [subpoena_category], [supplier_code], [send_city], [send_area], [check_number], [request_id], [Less_than_truckload], [DeliveryType], [cdate], [invoice_desc], [send_contact]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_6_4_76]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [order_number], [check_number], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_56_8]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [print_flag], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_47_17_4_1_76]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [supplier_code], [area_arrive_code], [check_number], [request_id], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_41]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_4_1_55_76_17]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [check_number], [request_id], [print_date], [Less_than_truckload], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_30_31_40]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [send_city], [send_area], [product_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_30_31_17_40_3_55_76_79]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [send_city], [send_area], [area_arrive_code], [product_category], [customer_code], [print_date], [Less_than_truckload], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_3_8_1]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [customer_code], [subpoena_category], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_17_47_8_4_2_76]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [area_arrive_code], [supplier_code], [subpoena_category], [check_number], [pricing_type], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_17_47_8_2_76_55_4]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [area_arrive_code], [supplier_code], [subpoena_category], [pricing_type], [Less_than_truckload], [print_date], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_17_3_8_41_47_76_55_2_4_12]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code], [Less_than_truckload], [print_date], [pricing_type], [check_number], [receive_contact]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_14_15_41]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [receive_city], [receive_area], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_14_15_30_31_41]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [receive_city], [receive_area], [send_city], [send_area], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_1_55]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [request_id], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_1_41_17_2_3]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [request_id], [special_send], [area_arrive_code], [pricing_type], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_1_41_14_15_47_30_31_17_3]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [request_id], [special_send], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [area_arrive_code], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_1_41_14_15_47_17_3]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [request_id], [special_send], [receive_city], [receive_area], [supplier_code], [area_arrive_code], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_1_2_55_47]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [request_id], [pricing_type], [print_date], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_68_1_14_15_30]
    ON [dbo].[tcDeliveryRequests]([cancel_date], [request_id], [receive_city], [receive_area], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_60_76_1_3_8]
    ON [dbo].[tcDeliveryRequests]([import_randomCode], [Less_than_truckload], [request_id], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_6_76_55]
    ON [dbo].[tcDeliveryRequests]([order_number], [Less_than_truckload], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_6_4_76]
    ON [dbo].[tcDeliveryRequests]([order_number], [check_number], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_6_1_4_55]
    ON [dbo].[tcDeliveryRequests]([order_number], [request_id], [check_number], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_56_3_8_70_68_2_55]
    ON [dbo].[tcDeliveryRequests]([print_flag], [customer_code], [subpoena_category], [cdate], [cancel_date], [pricing_type], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_56_1_3_8_68]
    ON [dbo].[tcDeliveryRequests]([print_flag], [request_id], [customer_code], [subpoena_category], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_79_76_68]
    ON [dbo].[tcDeliveryRequests]([print_date], [DeliveryType], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_79_4_17_30]
    ON [dbo].[tcDeliveryRequests]([print_date], [DeliveryType], [check_number], [area_arrive_code], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_79_2_76_47_17]
    ON [dbo].[tcDeliveryRequests]([print_date], [DeliveryType], [pricing_type], [Less_than_truckload], [supplier_code], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_76_79_3_4]
    ON [dbo].[tcDeliveryRequests]([print_date], [Less_than_truckload], [DeliveryType], [customer_code], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_76_4_30_31_71_3_8]
    ON [dbo].[tcDeliveryRequests]([print_date], [Less_than_truckload], [check_number], [send_city], [send_area], [uuser], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_76_1_47_30_31_17_41_3]
    ON [dbo].[tcDeliveryRequests]([print_date], [Less_than_truckload], [request_id], [supplier_code], [send_city], [send_area], [area_arrive_code], [special_send], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_76_1_4_30_31_71_3_8]
    ON [dbo].[tcDeliveryRequests]([print_date], [Less_than_truckload], [request_id], [check_number], [send_city], [send_area], [uuser], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_76_1_4_3_27_70]
    ON [dbo].[tcDeliveryRequests]([print_date], [Less_than_truckload], [request_id], [check_number], [customer_code], [send_contact], [cdate]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_68_3_17]
    ON [dbo].[tcDeliveryRequests]([print_date], [cancel_date], [customer_code], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_68_3_1_45]
    ON [dbo].[tcDeliveryRequests]([print_date], [cancel_date], [customer_code], [request_id], [pallet_recycling_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_6]
    ON [dbo].[tcDeliveryRequests]([print_date], [order_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_47_30_31_17_41_3_8_2_79]
    ON [dbo].[tcDeliveryRequests]([print_date], [supplier_code], [send_city], [send_area], [area_arrive_code], [special_send], [customer_code], [subpoena_category], [pricing_type], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_47_30_31_17_41_2_3_8]
    ON [dbo].[tcDeliveryRequests]([print_date], [supplier_code], [send_city], [send_area], [area_arrive_code], [special_send], [pricing_type], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_47_3_1_17]
    ON [dbo].[tcDeliveryRequests]([print_date], [supplier_code], [customer_code], [request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_47_2_76_79_1_30_31]
    ON [dbo].[tcDeliveryRequests]([print_date], [supplier_code], [pricing_type], [Less_than_truckload], [DeliveryType], [request_id], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_47_2_3_1_68]
    ON [dbo].[tcDeliveryRequests]([print_date], [supplier_code], [pricing_type], [customer_code], [request_id], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_47_17_8_76_79]
    ON [dbo].[tcDeliveryRequests]([print_date], [supplier_code], [area_arrive_code], [subpoena_category], [Less_than_truckload], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_47_17_8_2_79]
    ON [dbo].[tcDeliveryRequests]([print_date], [supplier_code], [area_arrive_code], [subpoena_category], [pricing_type], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_45_68_21_3_1_17]
    ON [dbo].[tcDeliveryRequests]([print_date], [pallet_recycling_flag], [cancel_date], [plates], [customer_code], [request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_4_1_76_6]
    ON [dbo].[tcDeliveryRequests]([print_date], [check_number], [request_id], [Less_than_truckload], [order_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_30_31_17_40_3_76_79_4]
    ON [dbo].[tcDeliveryRequests]([print_date], [send_city], [send_area], [area_arrive_code], [product_category], [customer_code], [Less_than_truckload], [DeliveryType], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_3_8_1_70_68]
    ON [dbo].[tcDeliveryRequests]([print_date], [customer_code], [subpoena_category], [request_id], [cdate], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_2_76_79_1_47]
    ON [dbo].[tcDeliveryRequests]([print_date], [pricing_type], [Less_than_truckload], [DeliveryType], [request_id], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_2_76_4_1_17_14_15]
    ON [dbo].[tcDeliveryRequests]([print_date], [pricing_type], [Less_than_truckload], [check_number], [request_id], [area_arrive_code], [receive_city], [receive_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_2_47_76_1_8_17_41]
    ON [dbo].[tcDeliveryRequests]([print_date], [pricing_type], [supplier_code], [Less_than_truckload], [request_id], [subpoena_category], [area_arrive_code], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_2_47_3_79]
    ON [dbo].[tcDeliveryRequests]([print_date], [pricing_type], [supplier_code], [customer_code], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_17_47_8_2]
    ON [dbo].[tcDeliveryRequests]([print_date], [area_arrive_code], [supplier_code], [subpoena_category], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_17_3_8_41_47_2_76_4_12_27]
    ON [dbo].[tcDeliveryRequests]([print_date], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code], [pricing_type], [Less_than_truckload], [check_number], [receive_contact], [send_contact]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_17_3_8_41_47]
    ON [dbo].[tcDeliveryRequests]([print_date], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_14_15_47_30_31_17_3_8_41_2_76]
    ON [dbo].[tcDeliveryRequests]([print_date], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [pricing_type], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_55_14_15_47_17_3_8_41_2_76_4]
    ON [dbo].[tcDeliveryRequests]([print_date], [receive_city], [receive_area], [supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [pricing_type], [Less_than_truckload], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_49_55_2_76_68]
    ON [dbo].[tcDeliveryRequests]([supplier_date], [print_date], [pricing_type], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_49_47_17_2_55_4_76_68]
    ON [dbo].[tcDeliveryRequests]([supplier_date], [supplier_code], [area_arrive_code], [pricing_type], [print_date], [check_number], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_49_2_17_55_76_68]
    ON [dbo].[tcDeliveryRequests]([supplier_date], [pricing_type], [area_arrive_code], [print_date], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_8_1_2_79_76]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [subpoena_category], [request_id], [pricing_type], [DeliveryType], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_79_70_76_68_27_4]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [DeliveryType], [cdate], [Less_than_truckload], [cancel_date], [send_contact], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_79_70_76_1_68_8_30]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [DeliveryType], [cdate], [Less_than_truckload], [request_id], [cancel_date], [subpoena_category], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_76_2_79]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [Less_than_truckload], [pricing_type], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_30_31_17_41_3_8]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [send_city], [send_area], [area_arrive_code], [special_send], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_30_31_17_41_2_3_8]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [send_city], [send_area], [area_arrive_code], [special_send], [pricing_type], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_2_55_76_79_1_8_30_31_39]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [pricing_type], [print_date], [Less_than_truckload], [DeliveryType], [request_id], [subpoena_category], [send_city], [send_area], [invoice_desc]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_8_76_2_55]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [subpoena_category], [Less_than_truckload], [pricing_type], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_8_68_76_79_2]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [subpoena_category], [cancel_date], [Less_than_truckload], [DeliveryType], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_8_4_2_76_1_68_3_30_31_27_12_20]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [subpoena_category], [check_number], [pricing_type], [Less_than_truckload], [request_id], [cancel_date], [customer_code], [send_city], [send_area], [send_contact], [receive_contact], [pieces]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_8_2_79]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [subpoena_category], [pricing_type], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_8_2_76_55_4_1_68]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [subpoena_category], [pricing_type], [Less_than_truckload], [print_date], [check_number], [request_id], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_79_3_55_76_1_30]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [DeliveryType], [customer_code], [print_date], [Less_than_truckload], [request_id], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_3_8_1_2_76_55_4_49_14_15_41]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [request_id], [pricing_type], [Less_than_truckload], [print_date], [check_number], [supplier_date], [receive_city], [receive_area], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_3_8_1_2_76_55_4_49_14_15_30_31_41]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [request_id], [pricing_type], [Less_than_truckload], [print_date], [check_number], [supplier_date], [receive_city], [receive_area], [send_city], [send_area], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_2_55_76_79_1_8]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [pricing_type], [print_date], [Less_than_truckload], [DeliveryType], [request_id], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_2_55_4_76]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [pricing_type], [print_date], [check_number], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_2_3_8_1_76_55_79_30_31]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [pricing_type], [customer_code], [subpoena_category], [request_id], [Less_than_truckload], [print_date], [DeliveryType], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_2_3_55_4_76_79_1]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [pricing_type], [customer_code], [print_date], [check_number], [Less_than_truckload], [DeliveryType], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_1_76_68]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [request_id], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_17_1_30]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [area_arrive_code], [request_id], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_1_8_76]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [request_id], [subpoena_category], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_47_1_30_31_17_41_3_8_76_55_2]
    ON [dbo].[tcDeliveryRequests]([supplier_code], [request_id], [send_city], [send_area], [area_arrive_code], [special_send], [customer_code], [subpoena_category], [Less_than_truckload], [print_date], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_45_1]
    ON [dbo].[tcDeliveryRequests]([pallet_recycling_flag], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_41_8_79_2]
    ON [dbo].[tcDeliveryRequests]([special_send], [subpoena_category], [DeliveryType], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_41_8_68_1_14_15_47_30_31_17]
    ON [dbo].[tcDeliveryRequests]([special_send], [subpoena_category], [cancel_date], [request_id], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_41_8_68_1_14_15_47_17]
    ON [dbo].[tcDeliveryRequests]([special_send], [subpoena_category], [cancel_date], [request_id], [receive_city], [receive_area], [supplier_code], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_41_8_1_68_17]
    ON [dbo].[tcDeliveryRequests]([special_send], [subpoena_category], [request_id], [cancel_date], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_41_8_1_47_30_31_17]
    ON [dbo].[tcDeliveryRequests]([special_send], [subpoena_category], [request_id], [supplier_code], [send_city], [send_area], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_41_2_8_68_1_17_76_55_3]
    ON [dbo].[tcDeliveryRequests]([special_send], [pricing_type], [subpoena_category], [cancel_date], [request_id], [area_arrive_code], [Less_than_truckload], [print_date], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_41_2_8_68_1_17_3_76_47]
    ON [dbo].[tcDeliveryRequests]([special_send], [pricing_type], [subpoena_category], [cancel_date], [request_id], [area_arrive_code], [customer_code], [Less_than_truckload], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_41_2_8_1_47_30_31_17_3_76_55_79]
    ON [dbo].[tcDeliveryRequests]([special_send], [pricing_type], [subpoena_category], [request_id], [supplier_code], [send_city], [send_area], [area_arrive_code], [customer_code], [Less_than_truckload], [print_date], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_41_1]
    ON [dbo].[tcDeliveryRequests]([special_send], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_8_2_55_76_17]
    ON [dbo].[tcDeliveryRequests]([check_number], [subpoena_category], [pricing_type], [print_date], [Less_than_truckload], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_79_17_55_76]
    ON [dbo].[tcDeliveryRequests]([check_number], [DeliveryType], [area_arrive_code], [print_date], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_76_68_79_70_39_47_8_30_31]
    ON [dbo].[tcDeliveryRequests]([check_number], [Less_than_truckload], [cancel_date], [DeliveryType], [cdate], [invoice_desc], [supplier_code], [subpoena_category], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_76_55_1_17]
    ON [dbo].[tcDeliveryRequests]([check_number], [Less_than_truckload], [print_date], [request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_55_76_68]
    ON [dbo].[tcDeliveryRequests]([check_number], [print_date], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_55_76_3_27]
    ON [dbo].[tcDeliveryRequests]([check_number], [print_date], [Less_than_truckload], [customer_code], [send_contact]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_55_76_27]
    ON [dbo].[tcDeliveryRequests]([check_number], [print_date], [Less_than_truckload], [send_contact]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_55_6_76_68_17]
    ON [dbo].[tcDeliveryRequests]([check_number], [print_date], [order_number], [Less_than_truckload], [cancel_date], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_47_17_8_2_79_76_55]
    ON [dbo].[tcDeliveryRequests]([check_number], [supplier_code], [area_arrive_code], [subpoena_category], [pricing_type], [DeliveryType], [Less_than_truckload], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_30_31_71_3_8]
    ON [dbo].[tcDeliveryRequests]([check_number], [send_city], [send_area], [uuser], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_27_1_55_76_70]
    ON [dbo].[tcDeliveryRequests]([check_number], [send_contact], [request_id], [print_date], [Less_than_truckload], [cdate]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_2_79_76_8_17_47]
    ON [dbo].[tcDeliveryRequests]([check_number], [pricing_type], [DeliveryType], [Less_than_truckload], [subpoena_category], [area_arrive_code], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_2_79_76_1_8_17_47]
    ON [dbo].[tcDeliveryRequests]([check_number], [pricing_type], [DeliveryType], [Less_than_truckload], [request_id], [subpoena_category], [area_arrive_code], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_2_3_79_76_8_17]
    ON [dbo].[tcDeliveryRequests]([check_number], [pricing_type], [customer_code], [DeliveryType], [Less_than_truckload], [subpoena_category], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_17_30_31]
    ON [dbo].[tcDeliveryRequests]([check_number], [area_arrive_code], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_17_3_8_41_47_76_55]
    ON [dbo].[tcDeliveryRequests]([check_number], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code], [Less_than_truckload], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_17_3_8_41_47_2_76]
    ON [dbo].[tcDeliveryRequests]([check_number], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code], [pricing_type], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_14_15_47_30_31_17_3_8_41_2_76_55]
    ON [dbo].[tcDeliveryRequests]([check_number], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [pricing_type], [Less_than_truckload], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_14_15_47_17_3_8_41_2_76]
    ON [dbo].[tcDeliveryRequests]([check_number], [receive_city], [receive_area], [supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [pricing_type], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_1_8_76_55]
    ON [dbo].[tcDeliveryRequests]([check_number], [request_id], [subpoena_category], [Less_than_truckload], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_4_1_8_47_30_31]
    ON [dbo].[tcDeliveryRequests]([check_number], [request_id], [subpoena_category], [supplier_code], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_39_47_30_31_1]
    ON [dbo].[tcDeliveryRequests]([invoice_desc], [supplier_code], [send_city], [send_area], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_8_2_3_47_76_68_1]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [cancel_date], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_8_2_3_47_55_76_68_1_17]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [subpoena_category], [pricing_type], [customer_code], [supplier_code], [print_date], [Less_than_truckload], [cancel_date], [request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_79_70_76_68_27_4]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [DeliveryType], [cdate], [Less_than_truckload], [cancel_date], [send_contact], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_79_70_47_76_68_27_4_1]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [DeliveryType], [cdate], [supplier_code], [Less_than_truckload], [cancel_date], [send_contact], [check_number], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_79_47_70_76_1_68_8]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [DeliveryType], [supplier_code], [cdate], [Less_than_truckload], [request_id], [cancel_date], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_79_4]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [DeliveryType], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_79_2]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [DeliveryType], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_68_1_17_40_3_55_76_79]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [cancel_date], [request_id], [area_arrive_code], [product_category], [customer_code], [print_date], [Less_than_truckload], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_47_8_1_2_79_76]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [supplier_code], [subpoena_category], [request_id], [pricing_type], [DeliveryType], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_41_1]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [special_send], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_40_1_68]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [product_category], [request_id], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_17_40_3]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [area_arrive_code], [product_category], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_17_40_1_3_55_76_79]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [area_arrive_code], [product_category], [request_id], [customer_code], [print_date], [Less_than_truckload], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_30_31_1_47_17_41_3_8_2_79_76]
    ON [dbo].[tcDeliveryRequests]([send_city], [send_area], [request_id], [supplier_code], [area_arrive_code], [special_send], [customer_code], [subpoena_category], [pricing_type], [DeliveryType], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_8_70_68_56]
    ON [dbo].[tcDeliveryRequests]([customer_code], [subpoena_category], [cdate], [cancel_date], [print_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_8_1_4_55_76_30_31]
    ON [dbo].[tcDeliveryRequests]([customer_code], [subpoena_category], [request_id], [check_number], [print_date], [Less_than_truckload], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_79_55_47_76_1_30_31_17_41_2]
    ON [dbo].[tcDeliveryRequests]([customer_code], [DeliveryType], [print_date], [supplier_code], [Less_than_truckload], [request_id], [send_city], [send_area], [area_arrive_code], [special_send], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_79_4_17_55]
    ON [dbo].[tcDeliveryRequests]([customer_code], [DeliveryType], [check_number], [area_arrive_code], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_76_2]
    ON [dbo].[tcDeliveryRequests]([customer_code], [Less_than_truckload], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_68_1]
    ON [dbo].[tcDeliveryRequests]([customer_code], [cancel_date], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_55_76_79_17_4_68_30_31_40]
    ON [dbo].[tcDeliveryRequests]([customer_code], [print_date], [Less_than_truckload], [DeliveryType], [area_arrive_code], [check_number], [cancel_date], [send_city], [send_area], [product_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_55_76_30_31_71]
    ON [dbo].[tcDeliveryRequests]([customer_code], [print_date], [Less_than_truckload], [send_city], [send_area], [uuser]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_55_76_1_30_31_71_8]
    ON [dbo].[tcDeliveryRequests]([customer_code], [print_date], [Less_than_truckload], [request_id], [send_city], [send_area], [uuser], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_55_2_76_4_1_14_15_47]
    ON [dbo].[tcDeliveryRequests]([customer_code], [print_date], [pricing_type], [Less_than_truckload], [check_number], [request_id], [receive_city], [receive_area], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_55_2_47_76_1_17_41]
    ON [dbo].[tcDeliveryRequests]([customer_code], [print_date], [pricing_type], [supplier_code], [Less_than_truckload], [request_id], [area_arrive_code], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_47_17_8_76_55_79]
    ON [dbo].[tcDeliveryRequests]([customer_code], [supplier_code], [area_arrive_code], [subpoena_category], [Less_than_truckload], [print_date], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_4_1_55_76]
    ON [dbo].[tcDeliveryRequests]([customer_code], [check_number], [request_id], [print_date], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_2_79_76_8_17]
    ON [dbo].[tcDeliveryRequests]([customer_code], [pricing_type], [DeliveryType], [Less_than_truckload], [subpoena_category], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_2_79_76_1_4_8_17]
    ON [dbo].[tcDeliveryRequests]([customer_code], [pricing_type], [DeliveryType], [Less_than_truckload], [request_id], [check_number], [subpoena_category], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_2_79_76_1_17]
    ON [dbo].[tcDeliveryRequests]([customer_code], [pricing_type], [DeliveryType], [Less_than_truckload], [request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_2_55_70_56]
    ON [dbo].[tcDeliveryRequests]([customer_code], [pricing_type], [print_date], [cdate], [print_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_2_55_47_76_79_1_30_31]
    ON [dbo].[tcDeliveryRequests]([customer_code], [pricing_type], [print_date], [supplier_code], [Less_than_truckload], [DeliveryType], [request_id], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_2_1]
    ON [dbo].[tcDeliveryRequests]([customer_code], [pricing_type], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_17_8_47_55]
    ON [dbo].[tcDeliveryRequests]([customer_code], [area_arrive_code], [subpoena_category], [supplier_code], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_17_55_76_1_2_41]
    ON [dbo].[tcDeliveryRequests]([customer_code], [area_arrive_code], [print_date], [Less_than_truckload], [request_id], [pricing_type], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_17_55_2_76_27_4_1]
    ON [dbo].[tcDeliveryRequests]([customer_code], [area_arrive_code], [print_date], [pricing_type], [Less_than_truckload], [send_contact], [check_number], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_17_47_8_4_2_76_1]
    ON [dbo].[tcDeliveryRequests]([customer_code], [area_arrive_code], [supplier_code], [subpoena_category], [check_number], [pricing_type], [Less_than_truckload], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_17_2_55_4_76_1_14_15]
    ON [dbo].[tcDeliveryRequests]([customer_code], [area_arrive_code], [pricing_type], [print_date], [check_number], [Less_than_truckload], [request_id], [receive_city], [receive_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_1_55_45]
    ON [dbo].[tcDeliveryRequests]([customer_code], [request_id], [print_date], [pallet_recycling_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_3_1_30_31]
    ON [dbo].[tcDeliveryRequests]([customer_code], [request_id], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_27_8_47_30_31_76_68_79_70]
    ON [dbo].[tcDeliveryRequests]([send_contact], [subpoena_category], [supplier_code], [send_city], [send_area], [Less_than_truckload], [cancel_date], [DeliveryType], [cdate]);


GO
CREATE STATISTICS [_dta_stat_566293077_27_8_2_3_47_76_68]
    ON [dbo].[tcDeliveryRequests]([send_contact], [subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_27_8_2_3_47_55_76_68_1_17_4_30]
    ON [dbo].[tcDeliveryRequests]([send_contact], [subpoena_category], [pricing_type], [customer_code], [supplier_code], [print_date], [Less_than_truckload], [cancel_date], [request_id], [area_arrive_code], [check_number], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_27_7_17_12_20_22_8_2_3_47_55]
    ON [dbo].[tcDeliveryRequests]([send_contact], [receive_customer_code], [area_arrive_code], [receive_contact], [pieces], [cbm], [subpoena_category], [pricing_type], [customer_code], [supplier_code], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_27_4_7_17_12_20_22_8_2_3_47_76_68]
    ON [dbo].[tcDeliveryRequests]([send_contact], [check_number], [receive_customer_code], [area_arrive_code], [receive_contact], [pieces], [cbm], [subpoena_category], [pricing_type], [customer_code], [supplier_code], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_27_4_7_17_12_20_22_8_2_3_47_55_76_68]
    ON [dbo].[tcDeliveryRequests]([send_contact], [check_number], [receive_customer_code], [area_arrive_code], [receive_contact], [pieces], [cbm], [subpoena_category], [pricing_type], [customer_code], [supplier_code], [print_date], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_27_4_12_20_8_2_3_47]
    ON [dbo].[tcDeliveryRequests]([send_contact], [check_number], [receive_contact], [pieces], [subpoena_category], [pricing_type], [customer_code], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_27_17_3_8_41_47_2_76_55_4]
    ON [dbo].[tcDeliveryRequests]([send_contact], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code], [pricing_type], [Less_than_truckload], [print_date], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_27_12_20_8_2_3_47]
    ON [dbo].[tcDeliveryRequests]([send_contact], [receive_contact], [pieces], [subpoena_category], [pricing_type], [customer_code], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_8_41_79_3_55_47_76_1_30_31]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [subpoena_category], [special_send], [DeliveryType], [customer_code], [print_date], [supplier_code], [Less_than_truckload], [request_id], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_8_41_55_47_76_1]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [subpoena_category], [special_send], [print_date], [supplier_code], [Less_than_truckload], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_79_76_8_17_47_3_4_1]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [DeliveryType], [Less_than_truckload], [subpoena_category], [area_arrive_code], [supplier_code], [customer_code], [check_number], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_79_76_55_68]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [DeliveryType], [Less_than_truckload], [print_date], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_79_76_55_39_47_30_31_8]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [DeliveryType], [Less_than_truckload], [print_date], [invoice_desc], [supplier_code], [send_city], [send_area], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_79_76_1_17_8_3_47]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [DeliveryType], [Less_than_truckload], [request_id], [area_arrive_code], [subpoena_category], [customer_code], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_8_68_47_3_17_4]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [subpoena_category], [cancel_date], [supplier_code], [customer_code], [area_arrive_code], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_8_68_47_3_17_1_30_31_27_12_20]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [subpoena_category], [cancel_date], [supplier_code], [customer_code], [area_arrive_code], [request_id], [send_city], [send_area], [send_contact], [receive_contact], [pieces]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_8_55_68_47_3_17_4]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [subpoena_category], [print_date], [cancel_date], [supplier_code], [customer_code], [area_arrive_code], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_8_55_68_47_3_17_1_30]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [subpoena_category], [print_date], [cancel_date], [supplier_code], [customer_code], [area_arrive_code], [request_id], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_8_4_1_17]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [subpoena_category], [check_number], [request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_55_17_68_3_41_8]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [print_date], [area_arrive_code], [cancel_date], [customer_code], [special_send], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_55_17_4_68_14_15_47_30_31_3_8_41]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [print_date], [area_arrive_code], [check_number], [cancel_date], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [customer_code], [subpoena_category], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_55_17_4_68_14_15_47_3_8_41]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [print_date], [area_arrive_code], [check_number], [cancel_date], [receive_city], [receive_area], [supplier_code], [customer_code], [subpoena_category], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_55_17_4_47_3_8]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [print_date], [area_arrive_code], [check_number], [supplier_code], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_55_17_3_8]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [print_date], [area_arrive_code], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_76_1_47_17]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [Less_than_truckload], [request_id], [supplier_code], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_55_76_79_1]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [print_date], [Less_than_truckload], [DeliveryType], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_55_70_56_68_8]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [print_date], [cdate], [print_flag], [cancel_date], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_55_47_4_76_79_1_17_8]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [print_date], [supplier_code], [check_number], [Less_than_truckload], [DeliveryType], [request_id], [area_arrive_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_55_47_3_68]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [print_date], [supplier_code], [customer_code], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_30_31_47_8_79]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [send_city], [send_area], [supplier_code], [subpoena_category], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_3_8_1_68_56]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [customer_code], [subpoena_category], [request_id], [cancel_date], [print_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_3_79_76_17_47]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [customer_code], [DeliveryType], [Less_than_truckload], [area_arrive_code], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_3_55_47_4_76_79_17]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [customer_code], [print_date], [supplier_code], [check_number], [Less_than_truckload], [DeliveryType], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_3_55_47_4_76_79_1]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [customer_code], [print_date], [supplier_code], [check_number], [Less_than_truckload], [DeliveryType], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_17_47_8]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [area_arrive_code], [supplier_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_17_47_79_76_68]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [area_arrive_code], [supplier_code], [DeliveryType], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_17_3_8_41_47_76]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_14_15_47_30_31_17_3_8_41]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [area_arrive_code], [customer_code], [subpoena_category], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_14_15_47_17_3_8_41_76]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [receive_city], [receive_area], [supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_2_1_8_3]
    ON [dbo].[tcDeliveryRequests]([pricing_type], [request_id], [subpoena_category], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_8_1_3_47_55_76]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [subpoena_category], [request_id], [customer_code], [supplier_code], [print_date], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_76_2]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [Less_than_truckload], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_55_68]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [print_date], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_55_2_76_27_4_1_8_3_41_47]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [print_date], [pricing_type], [Less_than_truckload], [send_contact], [check_number], [request_id], [subpoena_category], [customer_code], [special_send], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_55_2_47_76_1_41]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [print_date], [pricing_type], [supplier_code], [Less_than_truckload], [request_id], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_8_4_1_30_31_2_76_68]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [subpoena_category], [check_number], [request_id], [send_city], [send_area], [pricing_type], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_8_4_1_30_31_2_76_55_68]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [subpoena_category], [check_number], [request_id], [send_city], [send_area], [pricing_type], [Less_than_truckload], [print_date], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_8_2_3_76]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [subpoena_category], [pricing_type], [customer_code], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_8_2_3_55_76_68_1_4_30_31_27_7_12_20]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [subpoena_category], [pricing_type], [customer_code], [print_date], [Less_than_truckload], [cancel_date], [request_id], [check_number], [send_city], [send_area], [send_contact], [receive_customer_code], [receive_contact], [pieces]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_8_1_30_31_2_76_68_3_27_7_12_20_22]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [subpoena_category], [request_id], [send_city], [send_area], [pricing_type], [Less_than_truckload], [cancel_date], [customer_code], [send_contact], [receive_customer_code], [receive_contact], [pieces], [cbm]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_8_1_30_31_2_76_55_68_3_27_7_12_20_22]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [subpoena_category], [request_id], [send_city], [send_area], [pricing_type], [Less_than_truckload], [print_date], [cancel_date], [customer_code], [send_contact], [receive_customer_code], [receive_contact], [pieces], [cbm]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_79_76_68_1_4]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [DeliveryType], [Less_than_truckload], [cancel_date], [request_id], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_55_2_76_27_4_1_3_8]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [print_date], [pricing_type], [Less_than_truckload], [send_contact], [check_number], [request_id], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_30_31_8_2_3_76_68_1]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [send_city], [send_area], [subpoena_category], [pricing_type], [customer_code], [Less_than_truckload], [cancel_date], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_30_31_8_2_3_55_76_68]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [send_city], [send_area], [subpoena_category], [pricing_type], [customer_code], [print_date], [Less_than_truckload], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_2_79]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [pricing_type], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_47_2_3_79]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [supplier_code], [pricing_type], [customer_code], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_45_21_55_1]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [pallet_recycling_flag], [plates], [print_date], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_4_76_55]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [check_number], [Less_than_truckload], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_30_31_79_4_55_76_3]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [send_city], [send_area], [DeliveryType], [check_number], [print_date], [Less_than_truckload], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_3_8_41_47_1_76_55_2_4_12]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code], [request_id], [Less_than_truckload], [print_date], [pricing_type], [check_number], [receive_contact]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_2_8_1_3_76]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [pricing_type], [subpoena_category], [request_id], [customer_code], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_2_79_76]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [pricing_type], [DeliveryType], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_2_55_4_76_1_8_14_15_47_30_31]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [pricing_type], [print_date], [check_number], [Less_than_truckload], [request_id], [subpoena_category], [receive_city], [receive_area], [supplier_code], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_2_55_4_76_1_47_14_15_30_31]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [pricing_type], [print_date], [check_number], [Less_than_truckload], [request_id], [supplier_code], [receive_city], [receive_area], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_2_55_4_76_1_47_14_15_3_8_41_68_49]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [pricing_type], [print_date], [check_number], [Less_than_truckload], [request_id], [supplier_code], [receive_city], [receive_area], [customer_code], [subpoena_category], [special_send], [cancel_date], [supplier_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_2_55_4_76_1_3_14_15_47_30_31_8_41_68_49]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [pricing_type], [print_date], [check_number], [Less_than_truckload], [request_id], [customer_code], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [subpoena_category], [special_send], [cancel_date], [supplier_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_2_41_8_1_3_76]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [pricing_type], [special_send], [subpoena_category], [request_id], [customer_code], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_2_3_41_8]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [pricing_type], [customer_code], [special_send], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_14_15_47_30_31_2_55_4_76]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [pricing_type], [print_date], [check_number], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_14_15_47_2_55_4_76]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [receive_city], [receive_area], [supplier_code], [pricing_type], [print_date], [check_number], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_1_76_79]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [request_id], [Less_than_truckload], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_1_55_68_3_45]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [request_id], [print_date], [cancel_date], [customer_code], [pallet_recycling_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_1_45_21]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [request_id], [pallet_recycling_flag], [plates]);


GO
CREATE STATISTICS [_dta_stat_566293077_17_1_3_55_76_79_4_30_31_40_68]
    ON [dbo].[tcDeliveryRequests]([area_arrive_code], [request_id], [customer_code], [print_date], [Less_than_truckload], [DeliveryType], [check_number], [send_city], [send_area], [product_category], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_14_15_68_1_47_17_3_8_41_2_76_55]
    ON [dbo].[tcDeliveryRequests]([receive_city], [receive_area], [cancel_date], [request_id], [supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [pricing_type], [Less_than_truckload], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_14_15_47_30_31_17_3_8_41_1_2_76_55]
    ON [dbo].[tcDeliveryRequests]([receive_city], [receive_area], [supplier_code], [send_city], [send_area], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [request_id], [pricing_type], [Less_than_truckload], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_14_15_47_17_3_8_41]
    ON [dbo].[tcDeliveryRequests]([receive_city], [receive_area], [supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [special_send]);


GO
CREATE STATISTICS [_dta_stat_566293077_14_15_41_1]
    ON [dbo].[tcDeliveryRequests]([receive_city], [receive_area], [special_send], [request_id]);


GO
CREATE STATISTICS [_dta_stat_566293077_14_15_30_31_68_1_47_17_3_8_41_2_76_55]
    ON [dbo].[tcDeliveryRequests]([receive_city], [receive_area], [send_city], [send_area], [cancel_date], [request_id], [supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [pricing_type], [Less_than_truckload], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_12_17_3_8_41_47_76_55_2]
    ON [dbo].[tcDeliveryRequests]([receive_contact], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code], [Less_than_truckload], [print_date], [pricing_type]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_8_76_55_47_4_2_79_3]
    ON [dbo].[tcDeliveryRequests]([request_id], [subpoena_category], [Less_than_truckload], [print_date], [supplier_code], [check_number], [pricing_type], [DeliveryType], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_8_47_30_31]
    ON [dbo].[tcDeliveryRequests]([request_id], [subpoena_category], [supplier_code], [send_city], [send_area]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_76_55_47_4]
    ON [dbo].[tcDeliveryRequests]([request_id], [Less_than_truckload], [print_date], [supplier_code], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_76_2_4_47]
    ON [dbo].[tcDeliveryRequests]([request_id], [Less_than_truckload], [pricing_type], [check_number], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_71_4_30]
    ON [dbo].[tcDeliveryRequests]([request_id], [uuser], [check_number], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_70_8_60]
    ON [dbo].[tcDeliveryRequests]([request_id], [cdate], [subpoena_category], [import_randomCode]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_70_68_56]
    ON [dbo].[tcDeliveryRequests]([request_id], [cdate], [cancel_date], [print_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_70_56]
    ON [dbo].[tcDeliveryRequests]([request_id], [cdate], [print_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_70_4]
    ON [dbo].[tcDeliveryRequests]([request_id], [cdate], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_70_3_8_2_68_56]
    ON [dbo].[tcDeliveryRequests]([request_id], [cdate], [customer_code], [subpoena_category], [pricing_type], [cancel_date], [print_flag]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_68_41_17_3_8_47_76_55_2_4_12]
    ON [dbo].[tcDeliveryRequests]([request_id], [cancel_date], [special_send], [area_arrive_code], [customer_code], [subpoena_category], [supplier_code], [Less_than_truckload], [print_date], [pricing_type], [check_number], [receive_contact]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_68_30]
    ON [dbo].[tcDeliveryRequests]([request_id], [cancel_date], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_60_70_76]
    ON [dbo].[tcDeliveryRequests]([request_id], [import_randomCode], [cdate], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_55_76_79]
    ON [dbo].[tcDeliveryRequests]([request_id], [print_date], [Less_than_truckload], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_55_76_30_31_71_8_4]
    ON [dbo].[tcDeliveryRequests]([request_id], [print_date], [Less_than_truckload], [send_city], [send_area], [uuser], [subpoena_category], [check_number]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_55_45_68_21]
    ON [dbo].[tcDeliveryRequests]([request_id], [print_date], [pallet_recycling_flag], [cancel_date], [plates]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_55]
    ON [dbo].[tcDeliveryRequests]([request_id], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_47_4_8_2_79_3_17]
    ON [dbo].[tcDeliveryRequests]([request_id], [supplier_code], [check_number], [subpoena_category], [pricing_type], [DeliveryType], [customer_code], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_47_17_8_76_55_79]
    ON [dbo].[tcDeliveryRequests]([request_id], [supplier_code], [area_arrive_code], [subpoena_category], [Less_than_truckload], [print_date], [DeliveryType]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_47_17_8_2_79_76]
    ON [dbo].[tcDeliveryRequests]([request_id], [supplier_code], [area_arrive_code], [subpoena_category], [pricing_type], [DeliveryType], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_41_47_30_31_17_2_3]
    ON [dbo].[tcDeliveryRequests]([request_id], [special_send], [supplier_code], [send_city], [send_area], [area_arrive_code], [pricing_type], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_4_8_2_79_3_17]
    ON [dbo].[tcDeliveryRequests]([request_id], [check_number], [subpoena_category], [pricing_type], [DeliveryType], [customer_code], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_4_55_76_79_3]
    ON [dbo].[tcDeliveryRequests]([request_id], [check_number], [print_date], [Less_than_truckload], [DeliveryType], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_4_30]
    ON [dbo].[tcDeliveryRequests]([request_id], [check_number], [send_city]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_4_27_70]
    ON [dbo].[tcDeliveryRequests]([request_id], [check_number], [send_contact], [cdate]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_4_17_47_8_2_76_68_3_30_31_27_7_12_20_22]
    ON [dbo].[tcDeliveryRequests]([request_id], [check_number], [area_arrive_code], [supplier_code], [subpoena_category], [pricing_type], [Less_than_truckload], [cancel_date], [customer_code], [send_city], [send_area], [send_contact], [receive_customer_code], [receive_contact], [pieces], [cbm]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_30_31_71_3_8_55]
    ON [dbo].[tcDeliveryRequests]([request_id], [send_city], [send_area], [uuser], [customer_code], [subpoena_category], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_30_31_4_71_3_8_55]
    ON [dbo].[tcDeliveryRequests]([request_id], [send_city], [send_area], [check_number], [uuser], [customer_code], [subpoena_category], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_3_8_70_68_56_55]
    ON [dbo].[tcDeliveryRequests]([request_id], [customer_code], [subpoena_category], [cdate], [cancel_date], [print_flag], [print_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_3_55_68_21]
    ON [dbo].[tcDeliveryRequests]([request_id], [customer_code], [print_date], [cancel_date], [plates]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_3_4_27_70]
    ON [dbo].[tcDeliveryRequests]([request_id], [customer_code], [check_number], [send_contact], [cdate]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_2_55_47]
    ON [dbo].[tcDeliveryRequests]([request_id], [pricing_type], [print_date], [supplier_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_2_3_70_68]
    ON [dbo].[tcDeliveryRequests]([request_id], [pricing_type], [customer_code], [cdate], [cancel_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_17_41_2_3]
    ON [dbo].[tcDeliveryRequests]([request_id], [area_arrive_code], [special_send], [pricing_type], [customer_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_17_3_8_41_47_2_76_55_4_12_27_68_49]
    ON [dbo].[tcDeliveryRequests]([request_id], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [supplier_code], [pricing_type], [Less_than_truckload], [print_date], [check_number], [receive_contact], [send_contact], [cancel_date], [supplier_date]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_17_2_41_8_76]
    ON [dbo].[tcDeliveryRequests]([request_id], [area_arrive_code], [pricing_type], [special_send], [subpoena_category], [Less_than_truckload]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_17]
    ON [dbo].[tcDeliveryRequests]([request_id], [area_arrive_code]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_14_15_47_30_31_17_3_8]
    ON [dbo].[tcDeliveryRequests]([request_id], [receive_city], [receive_area], [supplier_code], [send_city], [send_area], [area_arrive_code], [customer_code], [subpoena_category]);


GO
CREATE STATISTICS [_dta_stat_566293077_1_14_15_47_17_3_8_41_2_76_55]
    ON [dbo].[tcDeliveryRequests]([request_id], [receive_city], [receive_area], [supplier_code], [area_arrive_code], [customer_code], [subpoena_category], [special_send], [pricing_type], [Less_than_truckload], [print_date]);


GO

CREATE TRIGGER [dbo].[gbDeliveryRequestsa] ON [dbo].[tcDeliveryRequests]
FOR INSERT
AS
BEGIN
-- =============================================
-- Author:		Sky
-- Create date: 2020/07/28
-- Description:	1.逆物流由發送縣市和行政區判斷發送站代碼並寫入send_station_scode
--							2.正物流由發送supplier_code判斷發送站
-- =============================================	

	DECLARE
		@send_station_scode_inserted nvarchar(15),
		@send_station_scode nvarchar(15),
		@send_city nvarchar(10),
		@send_area nvarchar(10),
		@supplier_code nvarchar(3),
		@delivery_type  varchar(10),
		@check_number nvarchar(20),
		@Less_than_truckload bit

	SELECT
		@send_station_scode_inserted=send_station_scode,
		@check_number=check_number,
		@send_city=send_city,
		@send_area=send_area,
		@supplier_code=supplier_code,
		@delivery_type=DeliveryType,
		@Less_than_truckload=Less_than_truckload
	FROM INSERTED
	IF (@send_station_scode_inserted is null or @send_station_scode_inserted='')
		BEGIN
			IF (@delivery_type='R')
				BEGIN
					SELECT @send_station_scode =B1.station_scode
					FROM ttArriveSitesScattered A1 with(nolock)
					LEFT JOIN tbStation B1 with(nolock) on B1.station_scode =  A1.station_code
					WHERE A1.post_city=@send_city and A1.post_area=@send_area
				END

			ELSE
				BEGIN
					IF (@Less_than_truckload=1)
						BEGIN
							SET @send_station_scode=REPLACE(@supplier_code,'F','')
						END
					ELSE
						BEGIN
							SET @send_station_scode=@supplier_code
						END
				END

			UPDATE tcDeliveryRequests SET send_station_scode= @send_station_scode WHERE check_number=@check_number and Less_than_truckload = '1'
	END
-- =============================================
-- Author:		Sky
-- Create date: 2020/09/09
-- Description:	1.當 insert request_id 沒有 cdate，以GETDATE() 補入 cdate,print_date
-- =============================================
	DECLARE
		@request_id int,
		@create_date datetime,
		@date_to_be_updated datetime

		SELECT
			@request_id=request_id,
			@create_date=cdate
		FROM INSERTED

		IF @create_date is NULL
		BEGIN
			SET @date_to_be_updated=GETDATE()

			UPDATE tcDeliveryRequests SET cdate=@date_to_be_updated,print_date=@date_to_be_updated where request_id=@request_id
		END

-- Author:		Sky
-- Create date: 2020/10/20
-- Description:	1.momo逆物流單號對應之正物流單號補進 return_check_number
-- =============================================
	Declare 
		@customer_code nvarchar(20),
		@send_contact nvarchar(20),
		@return_check_number nvarchar(20)
	
	SELECT
		@customer_code=customer_code,
		@send_contact=send_contact
	FROM INSERTED

	IF (@customer_code='F3500010002' and @check_number like '102%')
		BEGIN
			IF(@send_contact like '101%' or @send_contact like '201%')
				BEGIN
					SET @return_check_number=@send_contact
					UPDATE tcDeliveryRequests SET return_check_number=@return_check_number where request_id=@request_id
				END
		END

-- =============================================
-- Author:		Zhi-Yu
-- Create date: 2021/01/26
-- Description:	寫入預設運價 = 4號袋
-- =============================================
		declare @freight int;
		declare @shipping_fee table(
			no4_bag int,
			piece_rate int,
			holiday int
		);		
		declare @print_date datetime;
		declare @pieces int;
		declare @isHoliday bit;

		select @print_date = print_date,
		@pieces = pieces, 
		@isHoliday = holiday_delivery
		from inserted

		declare @is_valid bit = (
			select top 1 count(1) 
			from CustomerShippingFee f 
			where f.customer_code = @customer_code
			and f.effective_date < @print_date
		)

		if @is_valid = 1
		begin 
			insert into @shipping_fee 
			select f.no4_bag, f.piece_rate, f.holiday
			from CustomerShippingFee f 
			where customer_code = @customer_code
		end 
		else begin
			insert into @shipping_fee 
			select top 1 fh.no4_bag, fh.piece_rate, fh.holiday
			from CustomerShippingFeeHistory fh 
			where effective_date < @print_date and fh.customer_code = @customer_code
			order by fh.update_date desc
		end;
		
		-- 三邊和對應規格
		set @freight = (select no4_bag from @shipping_fee)	

		set @freight = @freight 
		+ (@pieces -1) * isnull((select piece_rate from @shipping_fee), 0)      -- 續件
		+ @isHoliday * @pieces * isnull((select holiday from @shipping_fee), 0) -- 假日配加價

		update tcDeliveryRequests set freight = @freight
		where request_id = @request_id;

END
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '振興卷金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'VoucherMoney';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'出貨日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'ship_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'集貨區分袋種', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'pick_up_good_type';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新歷程掃讀時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_scan_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'第一次正常配交時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'delivery_complete_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = '現金金額', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'CashMoney';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'運價', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'freight';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'應配日(ship_date + 1/當日有掃配送/超過ship_date + 1沒配送也沒銷單)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'delivery_date';


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-latest_delivery_date]
    ON [dbo].[tcDeliveryRequests]([latest_delivery_date] ASC);


GO



GO
-- =============================================
-- Author:		Zhi-Yu
-- Create date: 2021-01-20
-- Description:	寫入丈量機尺寸的時候，寫入運價
-- =============================================
CREATE TRIGGER [dbo].[DeliveryRequests_AfterUpdate]
   ON  [dbo].[tcDeliveryRequests]
   AFTER UPDATE 
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if update (cbmWidth)
	begin

		declare @customer_code varchar(20) = (select customer_code from inserted);
		declare @id int = (select request_id from inserted);
		declare @freight int;
		declare @cbmSum int;
		declare @shipping_fee table(
			s60 int,
			s90 int,
			s110 int,
			s120 int,
			s150 int,
			piece_rate int,
			holiday int
		);		
		declare @print_date datetime = (select print_date from inserted);
		declare @pieces int = (select pieces from inserted);
		declare @isHoliday bit = (select holiday_delivery from inserted);

		declare @is_valid bit = (
			select top 1 count(1) 
			from CustomerShippingFee f 
			where f.customer_code = @customer_code
			and f.effective_date < @print_date
		)

		if @is_valid = 1
		begin 
			insert into @shipping_fee 
			select f.s60_cm, f.s90_cm, f.s110_cm, f.s120_cm, f.s150_cm, f.piece_rate, f.holiday
			from CustomerShippingFee f 
			where customer_code = @customer_code
		end 
		else begin
			insert into @shipping_fee 
			select top 1 fh.s60_cm, fh.s90_cm, fh.s110_cm, fh.s120_cm, fh.s150_cm, fh.piece_rate, fh.holiday
			from CustomerShippingFeeHistory fh 
			where effective_date < @print_date and fh.customer_code = @customer_code
			order by fh.update_date desc
		end;

		-- 三邊和
		select @cbmSum = isnull(i.cbmWidth, 0) + isnull(i.cbmHeight, 0) + isnull(i.cbmLength, 0) 
		from inserted i;
		
		-- 三邊和對應規格
		set @freight = 
		case when @cbmSum >= 1550
				then (select s150 from @shipping_fee)
			when @cbmSum >= 1250 
				then (select s120 from @shipping_fee)
			when @cbmSum >= 1150 
				then (select s110 from @shipping_fee) 
			when @cbmSum >= 950 
				then (select s90 from @shipping_fee)
			else
				(select s60 from @shipping_fee)	
		end;	

		set @freight = @freight 
		+ (@pieces -1) * isnull((select piece_rate from @shipping_fee), 0)      -- 續件
		+ @isHoliday * @pieces * isnull((select holiday from @shipping_fee), 0) -- 假日配加價

		update tcDeliveryRequests set freight = @freight
		where request_id = @id;

	end

-- =============================================
-- Author:		Sky
-- Create date: 2021/02/01
-- Description:	update 地址更新發送站簡碼(只是暫時)，等系統查詢及補單作業開發完成後即刪除此part
-- =============================================

	--if (UPDATE (send_city) or UPDATE (send_area))
	--	BEGIN

	--		DECLARE @request_id int = (select request_id FROM  inserted)
	--		DECLARE @DeliveryType nvarchar(2) = (select DeliveryType FROM  inserted)
	--		DECLARE @send_city nvarchar(10) = (select send_city FROM  inserted)
	--		DECLARE @send_area nvarchar(10) = (select send_area FROM  inserted)
	--		DECLARE @supplier_code nvarchar(5) = (select supplier_code FROM  inserted)
			
	--		DECLARE @send_station_scode nvarchar(5) = 
	--			(SELECT top 1 case 
	--				when @DeliveryType = 'D' THEN REPLACE(@supplier_code,'F','') 
	--				when @DeliveryType = 'R' THEN A1.station_code 
	--				else null end as send_station_scode
	--			FROM ttArriveSitesScattered A1 with(nolock) WHERE A1.post_city=@send_city and A1.post_area = @send_area)
			
	--		UPDATE tcDeliveryRequests SET send_station_scode= @send_station_scode WHERE request_id = @request_id and Less_than_truckload = '1'
			
	--	END
END
GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新到著司機編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_dest_driver';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新到著時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_dest_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新配送司機編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_delivery_driver';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新配送時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_delivery_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新配達司機編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_arrive_option_driver';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新配達時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_arrive_option_date';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新配達項目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_arrive_option';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新掃描項目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_scan_item';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新掃描司機編號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_scan_driver_code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'最新掃描配達項目', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'latest_scan_arrive_option';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'標籤不顯示寄件人資訊1/不顯示地址2', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'custom_label';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'結帳日', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'closing_date';


GO



GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-send_city,send_area,send_address]
    ON [dbo].[tcDeliveryRequests]([send_city] ASC, [send_area] ASC, [send_address] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-return_check_number]
    ON [dbo].[tcDeliveryRequests]([return_check_number] ASC);


GO



GO



GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-latesr_arrive_option_date]
    ON [dbo].[tcDeliveryRequests]([latest_arrive_option_date] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-DeliveryType]
    ON [dbo].[tcDeliveryRequests]([DeliveryType] ASC, [latest_arrive_option_date] ASC)
    INCLUDE([latest_arrive_option], [area_arrive_code]);


GO



GO



GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-cdate_combo]
    ON [dbo].[tcDeliveryRequests]([cdate] ASC)
    INCLUDE([customer_code], [area_arrive_code], [supplier_code], [latest_dest_date], [latest_delivery_date], [latest_arrive_option_date], [latest_scan_item], [print_date], [Less_than_truckload], [DeliveryType], [cancel_date]);






GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'規格CodeId', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'SpecCodeId';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'簽單是否列印', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'sign_paper_print_flag';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'接駁區代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'ShuttleStationCode';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'產品id,用於快速識別產品', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'ProductId';






GO



GO



GO



GO



GO



GO



GO



GO



GO



GO



GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'保值費', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'tcDeliveryRequests', @level2type = N'COLUMN', @level2name = N'ProductValue';


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-ship_date_combo]
    ON [dbo].[tcDeliveryRequests]([ship_date] ASC, [Less_than_truckload] ASC, [check_number] ASC, [request_id] ASC);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-print_date_combo]
    ON [dbo].[tcDeliveryRequests]([Less_than_truckload] ASC, [print_date] ASC, [DeliveryType] ASC)
    INCLUDE([request_id], [pricing_type], [customer_code], [supplier_code], [subpoena_category]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-pricing_type_combo]
    ON [dbo].[tcDeliveryRequests]([pricing_type] ASC, [Less_than_truckload] ASC, [print_date] ASC, [DeliveryType] ASC)
    INCLUDE([request_id], [check_number], [subpoena_category], [area_arrive_code], [supplier_code]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-customer_code_combo_2]
    ON [dbo].[tcDeliveryRequests]([customer_code] ASC, [Less_than_truckload] ASC, [DeliveryType] ASC, [latest_scan_date] ASC)
    INCLUDE([request_id], [check_number], [order_number], [receive_customer_code], [pieces], [CbmSize], [collection_money], [print_date]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-customer_code_combo]
    ON [dbo].[tcDeliveryRequests]([customer_code] ASC, [cdate] ASC)
    INCLUDE([latest_arrive_option], [latest_arrive_option_date], [check_number], [order_number]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-cancel_date_combo]
    ON [dbo].[tcDeliveryRequests]([cancel_date] ASC, [Less_than_truckload] ASC, [DeliveryType] ASC)
    INCLUDE([cdate], [receive_area], [customer_code], [ship_date]);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex_ship_date_combo_2]
    ON [dbo].[tcDeliveryRequests]([ship_date] ASC, [latest_scan_item] ASC, [cancel_date] ASC)
    INCLUDE([customer_code], [Less_than_truckload]);

