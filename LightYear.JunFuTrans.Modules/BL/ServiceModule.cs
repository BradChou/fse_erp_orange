﻿using LightYear.JunFuTrans.BL.Services.StudentTest;
using Autofac;
using System.Reflection;
using LightYear.JunFuTrans.Utilities.Reports;

namespace LightYear.JunFuTrans.Modules.BL
{
    /// <summary>
    /// ServiceModule
    /// </summary>
    public class ServiceModule : Autofac.Module
    {
        /// <summary>
        /// Load
        /// </summary>
        /// <param name="builder">ContainerBuilder</param>
        protected override void Load(ContainerBuilder builder)
        {
            //// Register Services
            var service = Assembly.Load("LightYear.JunFuTrans.BL.Services");

            builder.RegisterAssemblyTypes(service)
                   .AsImplementedInterfaces();

            builder.RegisterType<SsrsHelper>()
                   .As<IReportHelper>();

        }
    }
}
