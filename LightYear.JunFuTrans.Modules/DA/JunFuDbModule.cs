﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.Modules.DA
{
    public class JunFuDbModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder);

            var connectionStringKey = "JunFuDbContext";

            builder.RegisterType<JunFuDbContext>()
                .AsSelf()
                .WithParameter("connectionString", connectionStringKey)
                .InstancePerLifetimeScope();
        }
    }
}
