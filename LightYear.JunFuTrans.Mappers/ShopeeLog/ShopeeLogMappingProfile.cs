﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.BL.BE.Api;


namespace LightYear.JunFuTrans.Mappers.ShopeeLog
{
    public class ShopeeLogMappingProfile : AutoMapper.Profile
    {
        public ShopeeLogMappingProfile()
        {
            CreateMap<ApiCbmInfoEntity, ShopeeCbminfo>()
               .ForMember(i => i.CheckNumber, s => s.MapFrom(d => d.CheckNumber))
               .ForMember(i => i.Size, s => s.MapFrom(d => d.Size))
               .ForMember(i => i.Length, s => s.MapFrom(d => d.Length))
               .ForMember(i => i.Width, s => s.MapFrom(d => d.Width))
               .ForMember(i => i.Height, s => s.MapFrom(d => d.Height))
               .ForMember(i => i.SidesSum, s => s.MapFrom(d => d.SidesSum))
               .ForMember(i => i.Cbm, s => s.MapFrom(d => d.Cbm))
               .ForMember(i => i.SidesSum, s => s.MapFrom(d => d.SidesSum))
               .ForMember(i => i.Weight, s => s.MapFrom(d => d.Weight))
               .ForMember(i => i.MeasurementTime, s => s.MapFrom(d => d.MeasurementTime))
               .AfterMap((source, destination) =>
               {
                   destination.ShopeeCbminfoNumber = DateTime.Now.ToString("yyyyMMddHHmmss");
                   destination.UploadDate = DateTime.Now;
                   destination.CreateDatetime = DateTime.Now;
                   destination.UpdateDatetime = DateTime.Now;
               });

            CreateMap<ApiPickupLogEntity, ShopeePickupInfo>()
               .ForMember(i => i.CheckNumber, s => s.MapFrom(d => d.CheckNumber))
               .ForMember(i => i.ShipDate, s => s.MapFrom(d => d.ShipDate))
               .ForMember(i => i.ActualPickup, s => s.MapFrom(d => d.ActualPickup))
               .ForMember(i => i.FalseItem, s => s.MapFrom(d => d.FalseItem))
               .ForMember(i => i.ScanDate, s => s.MapFrom(d => d.ScanDate))
               .ForMember(i => i.DeliveryType, s => s.MapFrom(d => d.DeliveryType))
               .ForMember(i => i.ScanName, s => s.MapFrom(d => d.ScanName))
               .ForMember(i => i.SupplierCode, s => s.MapFrom(d => d.SupplierCode))
               .ForMember(i => i.DriverType, s => s.MapFrom(d => d.DriverType))
               .ForMember(i => i.DriverCode, s => s.MapFrom(d => d.DriverCode))
               .AfterMap((source, destination) =>
               {
                   destination.ShopeePickupInfoNumber = DateTime.Now.ToString("yyyyMMddHHmmss");
                   destination.UploadDate = DateTime.Now;
                   destination.CreateDatetime = DateTime.Now;
                   destination.UpdateDatetime = DateTime.Now;
               });

            CreateMap<ApiScanLogEntity, ShopeeStatusDetail>()
               .ForMember(i => i.CheckNumber, s => s.MapFrom(d => d.checkNumber))
               .ForMember(i => i.ScanDate, s => s.MapFrom(d => d.scanDate))
               .ForMember(i => i.DeliveryType, s => s.MapFrom(d => d.deliveryType))
               .ForMember(i => i.ScanName, s => s.MapFrom(d => d.scanName))
               .ForMember(i => i.SupplierName, s => s.MapFrom(d => d.supplierName))
               .ForMember(i => i.Status, s => s.MapFrom(d => d.status))
               .ForMember(i => i.ItemCode, s => s.MapFrom(d => d.itemCodes))
               .ForMember(i => i.DriverCode, s => s.MapFrom(d => d.driverCode))
               .ForMember(i => i.DriverName, s => s.MapFrom(d => d.driverName))
               .ForMember(i => i.StationName, s => s.MapFrom(d => d.stationName))
               .ForMember(i => i.StationCode, s => s.MapFrom(d => d.stationCode))
               .AfterMap((source, destination) =>
               {
                   destination.ShopeeStatusDetailNumber = DateTime.Now.ToString("yyyyMMddHHmmss");
                   destination.UploadDate = DateTime.Now;
                   destination.CreateDatetime = DateTime.Now;
                   destination.UpdateDatetime = DateTime.Now;
               });
        }

    }
}
