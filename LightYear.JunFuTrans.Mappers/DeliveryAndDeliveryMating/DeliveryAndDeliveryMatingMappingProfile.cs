﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.Mappers.DeliveryAndDeliveryMating
{
    public class DeliveryAndDeliveryMatingMappingProfile : AutoMapper.Profile
    {
        public DeliveryAndDeliveryMatingMappingProfile()
        {
            CreateMap<TbItemCode, ArriveOptionEntity>()
                .ForMember(i => i.CodeId, s => s.MapFrom(i => i.CodeId))
                .ForMember(i => i.CodeName, s => s.MapFrom(i => i.CodeName));

            CreateMap<TbDriver, DriversEntity>()
               .ForMember(i => i.DriverCode, s => s.MapFrom(i => i.DriverCode))
               .ForMember(i => i.DriverName, s => s.MapFrom(i => (i.DriverCode+"-"+i.DriverName)));

            CreateMap<TbStation, StationEntity>()
               .ForMember(i => i.Name, s => s.MapFrom(i => (i.StationScode+"-"+ i.StationName)))
               .ForMember(i => i.Scode, s => s.MapFrom(i => i.StationScode));

        }
                
    }
}
