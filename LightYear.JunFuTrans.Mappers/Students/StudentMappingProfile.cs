﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.StudentTest;
using LightYear.JunFuTrans.DA.LightYearTest;

namespace LightYear.JunFuTrans.Mappers.Students
{
    public class StudentMappingProfile : AutoMapper.Profile
    {
        public StudentMappingProfile()
        {
            //from Student to PersonEntity
            this.CreateMap<Student, PersonEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.StudentId))
                .ForMember(i => i.Name, s => s.MapFrom(i => i.Name))
                .ForMember(i => i.Age, s => s.MapFrom(i => i.Age))
                .ForMember(i => i.Sex, s => s.MapFrom(i => (i.Sex > 0) ? true : false));

            //from PersonEntity to Student
            this.CreateMap<PersonEntity, Student>()
                .ForMember(i => i.StudentId, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Name, s => s.MapFrom(i => i.Name))
                .ForMember(i => i.Age, s => s.MapFrom(i => i.Age))
                .ForMember(i => i.Sex, s => s.MapFrom(i => (i.Sex) ? 1 : 0));
        }
    }
}
