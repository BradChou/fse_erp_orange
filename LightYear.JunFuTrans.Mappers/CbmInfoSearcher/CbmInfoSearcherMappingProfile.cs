﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.BL.BE.CbmInfo;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.Mappers.CbmInfoSearcher
{
    public class CbmInfoSearcherMappingProfile : Profile
    {
        public CbmInfoSearcherMappingProfile()
        {
            this.CreateMap<TcDeliveryRequest, CbmInfoEntity>()
            .ForMember(c => c.PrintDate, d => d.MapFrom(c => c.PrintDate))
            .ForMember(c => c.CheckNumber, d => d.MapFrom(c => c.CheckNumber))
            .ForMember(c => c.Height, d => d.MapFrom(c => c.CbmHeight))
            .ForMember(c => c.Width, d => d.MapFrom(c => c.CbmWidth))
            .ForMember(c => c.Length, d => d.MapFrom(c => c.CbmLength))
            .ForMember(c => c.SidesSum, d => d.MapFrom(c => c.CbmLength + c.CbmHeight + c.CbmWidth))
            .ForMember(c => c.Size, d => d.MapFrom(c => c.CbmCont))
            .ForMember(c => c.DaYuanPicURL, d => d.MapFrom(c => c.CatchCbmPicPathFromS3))
            .ForMember(c => c.TaiChungPicURL, d => d.MapFrom(c => c.CatchTaichungCbmPicPathFromS3))
            .ForMember(c => c.MeasurementTime, d => d.MapFrom(c => (c.PicPath == null || c.PicPath.Length == 0) ? (DateTime?)null :
                DateTime.ParseExact(c.PicPath.Substring(0, 19), "yyyy/MM/dd-HH:mm:ss", null)));
        }
    }
}
