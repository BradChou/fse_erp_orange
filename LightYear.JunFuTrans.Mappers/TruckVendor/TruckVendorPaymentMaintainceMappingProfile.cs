﻿using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.TruckVendor
{
    public class TruckVendorPaymentMaintainceMappingProfile : AutoMapper.Profile
    {
        public TruckVendorPaymentMaintainceMappingProfile()
        {
            CreateMap<FrontEndShowEntity, TbItemCode>()
                .ForMember(i => i.Seq, s => s.MapFrom(i => i.Seq))
                .ForMember(i => i.CodeId, s => s.MapFrom(i => i.CodeId))
                .ForMember(i => i.CodeName, s => s.MapFrom(i => i.CodeName))
                .ForMember(i => i.ActiveFlag, s => s.MapFrom(i => i.ActiveFlag));
        }
    }
}
