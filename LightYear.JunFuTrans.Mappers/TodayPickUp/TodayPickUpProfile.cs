﻿using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.TodayPickUp;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.TodayPickUp
{
    public class TodayPickUpProfile : AutoMapper.Profile
    {
        public TodayPickUpProfile()
        {
            this.CreateMap<TtDeliveryScanLog, TodayPickUpEntity>()
                .ForMember(t => t.CheckNumber, s => s.MapFrom(t => t.CheckNumber))
                .ForMember(t => t.Pieces, s => s.MapFrom(t => t.Pieces))
                .ForMember(t => t.ScanItem, s => s.MapFrom(t => t.ScanItem))
                .ForMember(t => t.ScanDate, s => s.MapFrom(t => t.ScanDate));

            this.CreateMap<TodayPickUpEntity, TtDeliveryScanLog>()
                .ForMember(s => s.CheckNumber, t => t.MapFrom(s => s.CheckNumber))
                .ForMember(s => s.Pieces, t => t.MapFrom(s => s.Pieces))
                .ForMember(s => s.ScanItem, t => t.MapFrom(s => s.ScanItem))
                .ForMember(s => s.ScanDate, t => t.MapFrom(s => s.ScanDate));
        }
    }
}
