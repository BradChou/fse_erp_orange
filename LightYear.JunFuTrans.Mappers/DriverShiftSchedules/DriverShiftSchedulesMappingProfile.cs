﻿using LightYear.JunFuTrans.BL.BE.DriverShiftSchedules;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Mappers.DriverShiftSchedules
{
    public class DriverShiftSchedulesMappingProfile : AutoMapper.Profile
    {
        public DriverShiftSchedulesMappingProfile()
        {
            CreateMap<DriverShiftArrangement, InputEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.TakeOffDate, s => s.MapFrom(i => i.TakeOffDate))
                .ForMember(i => i.StationScode, s => s.MapFrom(i => i.StationScode))
                .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
                .ForMember(i => i.TakeOffDriverWithCode, s => s.MapFrom(i => (i.TakeOffDriverCode + "-" + i.TakeOffDriver)))
                .ForMember(i => i.SubstituteDriverWithCode, s => s.MapFrom(i => (i.SubstituteDriverCode + "-" + i.SubstituteDriver)))
                .ForMember(i => i.UpdateDate, s => s.MapFrom(i => i.UpdateDate))
                .ForMember(i => i.UpdateUser, s => s.MapFrom(i => i.UpdateUser));

            CreateMap<InputEntity, DriverShiftArrangement>()
               .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
               .ForMember(i => i.TakeOffDate, s => s.MapFrom(i => i.TakeOffDate))
               .ForMember(i => i.StationScode, s => s.MapFrom(i => i.StationScode))
               .ForMember(i => i.StationName, s => s.MapFrom(i => i.StationName))
               .ForMember(i => i.TakeOffDriverCode, s => s.MapFrom(i => i.TakeOffDriverWithCode.Substring(0,i.TakeOffDriverWithCode.IndexOf("-"))))
               .ForMember(i => i.TakeOffDriver, s => s.MapFrom(i => i.TakeOffDriverWithCode.Substring(i.TakeOffDriverWithCode.IndexOf("-")+1)))
               .ForMember(i => i.SubstituteDriverCode, s => s.MapFrom(i => i.SubstituteDriverWithCode.Substring(0,i.SubstituteDriverWithCode.IndexOf("-"))))
               .ForMember(i => i.SubstituteDriver, s => s.MapFrom(i => i.SubstituteDriverWithCode.Substring(i.SubstituteDriverWithCode.IndexOf("-")+1)))
               .ForMember(i => i.UpdateDate, s => s.MapFrom(i => i.UpdateDate))
               .ForMember(i => i.UpdateUser, s => s.MapFrom(i => i.UpdateUser));
        }
    }
}
