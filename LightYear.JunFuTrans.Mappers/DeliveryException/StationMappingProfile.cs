﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.DeliveryException;

namespace LightYear.JunFuTrans.Mappers.DeliveryException
{
    public class StationMappingProfile :AutoMapper.Profile
    {
        public StationMappingProfile()
        {
            this.CreateMap<TbStation, StationEntity>()
                .ForMember(s => s.Code, t => t.MapFrom(s => s.StationScode))
                .ForMember(s => s.Name, t => t.MapFrom(s => s.StationName));

            this.CreateMap<StationEntity, TbStation>()
                .ForMember(t => t.StationScode, s => s.MapFrom(t => t.Code))
                .ForMember(t => t.StationName, s => s.MapFrom(t => t.Name));
        }
    }
}
