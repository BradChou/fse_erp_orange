﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.BusinessReport;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.Mappers.BusinessReport
{
    public class BusinessReportMappingProfile : AutoMapper.Profile
    {
        public BusinessReportMappingProfile()
        {
            this.CreateMap<BusinessReportEntity, BusinessReportRepositoryEntity>()
                .ForMember(r => r.AreaArriveCode, b => b.MapFrom(r => r.AreaArriveCode))
                .ForMember(r => r.CheckNumber, b => b.MapFrom(r => r.CheckNumber))
                .ForMember(r => r.SupplierCode, b => b.MapFrom(r => r.SupplierCode))
                .ForMember(r => r.ShipDate, b => b.MapFrom(r => r.ShipDate))
                .ForMember(r => r.CustomerCode, b => b.MapFrom(r => r.CustomerCode))
                .ForMember(r => r.Pieces, b => b.MapFrom(r => r.Pieces));

            this.CreateMap<BusinessReportRepositoryEntity, BusinessReportEntity>()
                .ForMember(b => b.AreaArriveCode, r => r.MapFrom(b => b.AreaArriveCode))
                .ForMember(b => b.CheckNumber, r => r.MapFrom(b => b.CheckNumber))
                .ForMember(b => b.SupplierCode, r => r.MapFrom(b => b.SupplierCode))
                .ForMember(b => b.ShipDate, r => r.MapFrom(b => b.ShipDate))
                .ForMember(b => b.CustomerCode, r => r.MapFrom(b => b.CustomerCode))
                .ForMember(b => b.Pieces, r => r.MapFrom(b => b.Pieces));

            this.CreateMap<StationEntity, TbStation>()
                .ForMember(t => t.StationScode, s => s.MapFrom(t => t.Code))
                .ForMember(t => t.StationName, s => s.MapFrom(t => t.Name))
                .ForMember(t => t.Owner, s => s.MapFrom(t => t.Owner));

            this.CreateMap<TbStation, StationEntity > ()
                .ForMember(s => s.Code, t => t.MapFrom(s => s.StationScode))
                .ForMember(s => s.Name, t => t.MapFrom(s => s.StationScode + s.StationName))
                .ForMember(s => s.Owner, t => t.MapFrom(s => s.Owner));
        }
    }
}
