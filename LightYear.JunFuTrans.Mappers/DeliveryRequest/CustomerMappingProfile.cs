﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.Mappers.DeliveryRequest
{
    public class CustomerMappingProfile : AutoMapper.Profile
    {
        public CustomerMappingProfile()
        {
            this.CreateMap<TbCustomer, CustomerEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.CustomerId))
                .ForMember(i => i.SupplierCode, s => s.MapFrom(i => i.SupplierCode))
                .ForMember(i => i.MasterCode, s => s.MapFrom(i => i.MasterCode))
                .ForMember(i => i.SecondId, s => s.MapFrom(i => i.SecondId))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CustomerName, s => s.MapFrom(i => i.CustomerName))
                .ForMember(i => i.CustomerShortName, s => s.MapFrom(i => i.CustomerShortname))
                .ForMember(i => i.CustomerType, s => s.MapFrom(i => i.CustomerType))
                .ForMember(i => i.UniformNumber, s => s.MapFrom(i => i.UniformNumbers))
                .ForMember(i => i.ShipmentsPrincipal, s => s.MapFrom(i => i.ShipmentsPrincipal))
                .ForMember(i => i.Tel, s => s.MapFrom(i => i.Telephone))
                .ForMember(i => i.ShipmentsCity, s => s.MapFrom(i => i.ShipmentsCity))
                .ForMember(i => i.ShipmentsArea, s => s.MapFrom(i => i.ShipmentsArea))
                .ForMember(i => i.ShipmentAddress, s => s.MapFrom(i => i.ShipmentsRoad))
                .ForMember(i => i.ShipmentEmail, s => s.MapFrom(i => i.ShipmentsEmail))
                .ForMember(i => i.StopShippingCode, s => s.MapFrom(i => i.StopShippingCode))
                .ForMember(i => i.StopShippingMemo, s => s.MapFrom(i => i.StopShippingMemo))
                .ForMember(i => i.ContractContent, s => s.MapFrom(i => i.ContractContent))
                .ForMember(i => i.ShowName, s => s.MapFrom(i => i.CustomerName + "(" + i.CustomerCode + ")"));
        }
    }
}
