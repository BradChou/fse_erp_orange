﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.Mappers.DeliveryRequest
{
    public class DeliveryRequestMappingProfile : AutoMapper.Profile
    {
        public DeliveryRequestMappingProfile()
        {
            this.CreateMap<DeliveryRequestEntity, TcDeliveryRequest>()
                .ForMember(i => i.RequestId, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.PricingType, s => s.MapFrom(i => i.PricingType))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.CheckType, s => s.MapFrom(i => i.CheckType))
                .ForMember(i => i.OrderNumber, s => s.MapFrom(i => i.OrderNumber))
                .ForMember(i => i.ReceiveCustomerCode, s => s.MapFrom(i => i.ReceiveCustomerCode))
                .ForMember(i => i.SubpoenaCategory, s => s.MapFrom(i => i.SubpoenaCategory))
                .ForMember(i => i.ReceiveTel1, s => s.MapFrom(i => i.ReceiveTel1))
                .ForMember(i => i.ReceiveTel1Ext, s => s.MapFrom(i => i.ReceiveTel1Ext))
                .ForMember(i => i.ReceiveTel2, s => s.MapFrom(i => i.ReceiveTel2))
                .ForMember(i => i.ReceiveContact, s => s.MapFrom(i => i.ReceiverName))
                .ForMember(i => i.ReceiveZip, s => s.MapFrom(i => i.Zip))
                .ForMember(i => i.ReceiveArea, s => s.MapFrom(i => i.Area))
                .ForMember(i => i.ReceiveAddress, s => s.MapFrom(i => i.Address))
                .ForMember(i => i.AreaArriveCode, s => s.MapFrom(i => i.AreaArriveCode))
                .ForMember(i => i.ReceiveByArriveSiteFlag, s => s.MapFrom(i => i.ReceiveByArriveSiteFlag))
                .ForMember(i => i.ArriveAddress, s => s.MapFrom(i => i.ArriveAddress))
                .ForMember(i => i.Pieces, s => s.MapFrom(i => i.Pieces))
                .ForMember(i => i.Plates, s => s.MapFrom(i => i.Plates))
                .ForMember(i => i.Cbm, s => s.MapFrom(i => i.Cbm))
                .ForMember(i => i.CbmSize, s => s.MapFrom(i => i.CbmSize))
                .ForMember(i => i.CollectionMoney, s => s.MapFrom(i => i.CollectionMoney))
                .ForMember(i => i.ArriveToPayAppend, s => s.MapFrom(i => i.ArriveToPayAppend))
                .ForMember(i => i.ArriveToPayFreight, s => s.MapFrom(i => i.ArriveToPayFreight))
                .ForMember(i => i.SendContact, s => s.MapFrom(i => i.SendContact))
                .ForMember(i => i.SendTel, s => s.MapFrom(i => i.SendTel))
                .ForMember(i => i.SendZip, s => s.MapFrom(i => i.SendZip))
                .ForMember(i => i.SendCity, s => s.MapFrom(i => i.SendCity))
                .ForMember(i => i.SendArea, s => s.MapFrom(i => i.SendArea))
                .ForMember(i => i.SendAddress, s => s.MapFrom(i => i.SendAddress))
                .ForMember(i => i.DonateInvoiceFlag, s => s.MapFrom(i => i.DonateInvoiceFlag))
                .ForMember(i => i.ElectronicInvoiceFlag, s => s.MapFrom(i => i.ElectronicInvoiceFlag))
                .ForMember(i => i.UniformNumbers, s => s.MapFrom(i => i.UniformNumber))
                .ForMember(i => i.ArriveMobile, s => s.MapFrom(i => i.ArriveMobile))
                .ForMember(i => i.ArriveEmail, s => s.MapFrom(i => i.ArriveEmail))
                .ForMember(i => i.InvoiceMemo, s => s.MapFrom(i => i.InvoiceMemo))
                .ForMember(i => i.InvoiceDesc, s => s.MapFrom(i => i.InvoiceDesc))
                .ForMember(i => i.ProductCategory, s => s.MapFrom(i => i.ProductCategory))
                .ForMember(i => i.SpecialSend, s => s.MapFrom(i => i.SpecialSend))
                .ForMember(i => i.ArriveAssignDate, s => s.MapFrom(i => i.ArriveAssignDate))
                .ForMember(i => i.TimePeriod, s => s.MapFrom(i => i.TimePeriod))
                .ForMember(i => i.ReceiptFlag, s => s.MapFrom(i => i.ReceiptFlag))
                .ForMember(i => i.PalletRecyclingFlag, s => s.MapFrom(i => i.PalletRecyclingFlag))
                .ForMember(i => i.PalletType, s => s.MapFrom(i => i.PalletType))
                .ForMember(i => i.SupplierCode, s => s.MapFrom(i => i.SupplierCode))
                .ForMember(i => i.SupplierName, s => s.MapFrom(i => i.SupplierName))
                .ForMember(i => i.SupplierDate, s => s.MapFrom(i => i.SupplierDate))
                .ForMember(i => i.ReceiptNumbe, s => s.MapFrom(i => i.ReceiptNumber))
                .ForMember(i => i.SupplierFee, s => s.MapFrom(i => i.SupplierFee))
                .ForMember(i => i.CsectionFee, s => s.MapFrom(i => i.CSectionFee))
                .ForMember(i => i.RemoteFee, s => s.MapFrom(i => i.RemoteFee))
                .ForMember(i => i.TotalFee, s => s.MapFrom(i => i.TotalFee))
                .ForMember(i => i.PrintDate, s => s.MapFrom(i => i.PrintDate))
                .ForMember(i => i.PrintFlag, s => s.MapFrom(i => i.PrintFlag))
                .ForMember(i => i.CheckoutCloseDate, s => s.MapFrom(i => i.CheckoutCloseDate))
                .ForMember(i => i.AddTransfer, s => s.MapFrom(i => i.AddTransfer))
                .ForMember(i => i.SubCheckNumber, s => s.MapFrom(i => i.SubCheckNumber))
                .ForMember(i => i.ImportRandomCode, s => s.MapFrom(i => i.ImportRandomCode))
                .ForMember(i => i.CloseRandomCode, s => s.MapFrom(i => i.CloseRandomCode))
                .ForMember(i => i.TurnBoard, s => s.MapFrom(i => i.TurnBoard))
                .ForMember(i => i.Upstairs, s => s.MapFrom(i => i.UpStairs))
                .ForMember(i => i.DifficultDelivery, s => s.MapFrom(i => i.DifficultDelivery))
                .ForMember(i => i.TurnBoardFee, s => s.MapFrom(i => i.TurnBoardFee))
                .ForMember(i => i.UpstairsFee, s => s.MapFrom(i => i.UpStairsFee))
                .ForMember(i => i.DifficultFee, s => s.MapFrom(i => i.DifficultFee))
                .ForMember(i => i.CancelDate, s => s.MapFrom(i => i.CancelDate))
                .ForMember(i => i.Cuser, s => s.MapFrom(i => i.CreateUser))
                .ForMember(i => i.Cdate, s => s.MapFrom(i => i.CreateDate))
                .ForMember(i => i.Uuser, s => s.MapFrom(i => i.UpdateUser))
                .ForMember(i => i.Udate, s => s.MapFrom(i => i.UpdateDate))
                .ForMember(i => i.HcTstatus, s => s.MapFrom(i => i.HctStatus))
                .ForMember(i => i.IsPallet, s => s.MapFrom(i => i.IsPallet))
                .ForMember(i => i.PalletRequestId, s => s.MapFrom(i => i.PalletRequestId))
                .ForMember(i => i.LessThanTruckload, s => s.MapFrom(i => i.LessThanTruckload))
                .ForMember(i => i.Distributor, s => s.MapFrom(i => i.Distributor))
                .ForMember(i => i.Temperate, s => s.MapFrom(i => i.Temperate))
                .ForMember(i => i.DeliveryType, s => s.MapFrom(i => i.DeliveryType))
                .ForMember(i => i.CbmLength, s => s.MapFrom(i => i.CbmLength))
                .ForMember(i => i.CbmWidth, s => s.MapFrom(i => i.CbmWidth))
                .ForMember(i => i.CbmHeight, s => s.MapFrom(i => i.CbmHeight))
                .ForMember(i => i.CbmWeight, s => s.MapFrom(i => i.CbmWeight))
                .ForMember(i => i.CbmCont, s => s.MapFrom(i => i.CbmCont))
                .ForMember(i => i.Bagno, s => s.MapFrom(i => i.BagNo))
                .ForMember(i => i.ArticleNumber, s => s.MapFrom(i => i.ArticleNumber))
                .ForMember(i => i.SendPlatform, s => s.MapFrom(i => i.SendPlatform))
                .ForMember(i => i.ArticleName, s => s.MapFrom(i => i.ArticleName))
                .ForMember(i => i.RoundTrip, s => s.MapFrom(i => i.RoundTrip));

            this.CreateMap<TcDeliveryRequest, DeliveryRequestEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.RequestId))
                .ForMember(i => i.PricingType, s => s.MapFrom(i => i.PricingType))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.CheckType, s => s.MapFrom(i => i.CheckType))
                .ForMember(i => i.OrderNumber, s => s.MapFrom(i => i.OrderNumber))
                .ForMember(i => i.ReceiveCustomerCode, s => s.MapFrom(i => i.ReceiveCustomerCode))
                .ForMember(i => i.SubpoenaCategory, s => s.MapFrom(i => i.SubpoenaCategory))
                .ForMember(i => i.ReceiveTel1, s => s.MapFrom(i => i.ReceiveTel1))
                .ForMember(i => i.ReceiveTel1Ext, s => s.MapFrom(i => i.ReceiveTel1Ext))
                .ForMember(i => i.ReceiveTel2, s => s.MapFrom(i => i.ReceiveTel2))
                .ForMember(i => i.ReceiverName, s => s.MapFrom(i => i.ReceiveContact))
                .ForMember(i => i.Zip, s => s.MapFrom(i => i.ReceiveZip))
                .ForMember(i => i.City, s => s.MapFrom(i => i.ReceiveCity))
                .ForMember(i => i.Area, s => s.MapFrom(i => i.ReceiveArea))
                .ForMember(i => i.Address, s => s.MapFrom(i => i.ReceiveAddress))
                .ForMember(i => i.AreaArriveCode, s => s.MapFrom(i => i.AreaArriveCode))
                .ForMember(i => i.ReceiveByArriveSiteFlag, s => s.MapFrom(i => i.ReceiveByArriveSiteFlag))
                .ForMember(i => i.ArriveAddress, s => s.MapFrom(i => i.ArriveAddress))
                .ForMember(i => i.Pieces, s => s.MapFrom(i => i.Pieces))
                .ForMember(i => i.Plates, s => s.MapFrom(i => i.Plates))
                .ForMember(i => i.Cbm, s => s.MapFrom(i => i.Cbm))
                .ForMember(i => i.CbmSize, s => s.MapFrom(i => i.CbmSize))
                .ForMember(i => i.CollectionMoney, s => s.MapFrom(i => i.CollectionMoney))
                .ForMember(i => i.ArriveToPayAppend, s => s.MapFrom(i => i.ArriveToPayAppend))
                .ForMember(i => i.ArriveToPayFreight, s => s.MapFrom(i => i.ArriveToPayFreight))
                .ForMember(i => i.SendContact, s => s.MapFrom(i => i.SendContact))
                .ForMember(i => i.SendTel, s => s.MapFrom(i => i.SendTel))
                .ForMember(i => i.SendZip, s => s.MapFrom(i => i.SendZip))
                .ForMember(i => i.SendCity, s => s.MapFrom(i => i.SendCity))
                .ForMember(i => i.SendArea, s => s.MapFrom(i => i.SendArea))
                .ForMember(i => i.SendAddress, s => s.MapFrom(i => i.SendAddress))
                .ForMember(i => i.DonateInvoiceFlag, s => s.MapFrom(i => i.DonateInvoiceFlag))
                .ForMember(i => i.ElectronicInvoiceFlag, s => s.MapFrom(i => i.ElectronicInvoiceFlag))
                .ForMember(i => i.UniformNumber, s => s.MapFrom(i => i.UniformNumbers))
                .ForMember(i => i.ArriveMobile, s => s.MapFrom(i => i.ArriveMobile))
                .ForMember(i => i.ArriveEmail, s => s.MapFrom(i => i.ArriveEmail))
                .ForMember(i => i.InvoiceMemo, s => s.MapFrom(i => i.InvoiceMemo))
                .ForMember(i => i.InvoiceDesc, s => s.MapFrom(i => i.InvoiceDesc))
                .ForMember(i => i.ProductCategory, s => s.MapFrom(i => i.ProductCategory))
                .ForMember(i => i.SpecialSend, s => s.MapFrom(i => i.SpecialSend))
                .ForMember(i => i.ArriveAssignDate, s => s.MapFrom(i => i.ArriveAssignDate))
                .ForMember(i => i.TimePeriod, s => s.MapFrom(i => i.TimePeriod))
                .ForMember(i => i.ReceiptFlag, s => s.MapFrom(i => i.ReceiptFlag))
                .ForMember(i => i.PalletRecyclingFlag, s => s.MapFrom(i => i.PalletRecyclingFlag))
                .ForMember(i => i.PalletType, s => s.MapFrom(i => i.PalletType))
                .ForMember(i => i.SupplierCode, s => s.MapFrom(i => i.SupplierCode))
                .ForMember(i => i.SupplierName, s => s.MapFrom(i => i.SupplierName))
                .ForMember(i => i.SupplierDate, s => s.MapFrom(i => i.SupplierDate))
                .ForMember(i => i.ReceiptNumber, s => s.MapFrom(i => i.ReceiptNumbe))
                .ForMember(i => i.SupplierFee, s => s.MapFrom(i => i.SupplierFee))
                .ForMember(i => i.CSectionFee, s => s.MapFrom(i => i.CsectionFee))
                .ForMember(i => i.RemoteFee, s => s.MapFrom(i => i.RemoteFee))
                .ForMember(i => i.TotalFee, s => s.MapFrom(i => i.TotalFee))
                .ForMember(i => i.PrintDate, s => s.MapFrom(i => i.PrintDate))
                .ForMember(i => i.PrintFlag, s => s.MapFrom(i => i.PrintFlag))
                .ForMember(i => i.CheckoutCloseDate, s => s.MapFrom(i => i.CheckoutCloseDate))
                .ForMember(i => i.AddTransfer, s => s.MapFrom(i => i.AddTransfer))
                .ForMember(i => i.SubCheckNumber, s => s.MapFrom(i => i.SubCheckNumber))
                .ForMember(i => i.ImportRandomCode, s => s.MapFrom(i => i.ImportRandomCode))
                .ForMember(i => i.CloseRandomCode, s => s.MapFrom(i => i.CloseRandomCode))
                .ForMember(i => i.TurnBoard, s => s.MapFrom(i => i.TurnBoard))
                .ForMember(i => i.UpStairs, s => s.MapFrom(i => i.Upstairs))
                .ForMember(i => i.DifficultDelivery, s => s.MapFrom(i => i.DifficultDelivery))
                .ForMember(i => i.TurnBoardFee, s => s.MapFrom(i => i.TurnBoardFee))
                .ForMember(i => i.UpStairsFee, s => s.MapFrom(i => i.UpstairsFee))
                .ForMember(i => i.DifficultFee, s => s.MapFrom(i => i.DifficultFee))
                .ForMember(i => i.CancelDate, s => s.MapFrom(i => i.CancelDate))
                .ForMember(i => i.CreateUser, s => s.MapFrom(i => i.Cuser))
                .ForMember(i => i.CreateDate, s => s.MapFrom(i => i.Cdate))
                .ForMember(i => i.UpdateUser, s => s.MapFrom(i => i.Uuser))
                .ForMember(i => i.UpdateDate, s => s.MapFrom(i => i.Udate))
                .ForMember(i => i.HctStatus, s => s.MapFrom(i => i.HcTstatus))
                .ForMember(i => i.IsPallet, s => s.MapFrom(i => i.IsPallet))
                .ForMember(i => i.PalletRequestId, s => s.MapFrom(i => i.PalletRequestId))
                .ForMember(i => i.LessThanTruckload, s => s.MapFrom(i => i.LessThanTruckload))
                .ForMember(i => i.Distributor, s => s.MapFrom(i => i.Distributor))
                .ForMember(i => i.Temperate, s => s.MapFrom(i => i.Temperate))
                .ForMember(i => i.DeliveryType, s => s.MapFrom(i => i.DeliveryType))
                .ForMember(i => i.CbmLength, s => s.MapFrom(i => i.CbmLength))
                .ForMember(i => i.CbmWidth, s => s.MapFrom(i => i.CbmWidth))
                .ForMember(i => i.CbmHeight, s => s.MapFrom(i => i.CbmHeight))
                .ForMember(i => i.CbmWeight, s => s.MapFrom(i => i.CbmWeight))
                .ForMember(i => i.CbmCont, s => s.MapFrom(i => i.CbmCont))
                .ForMember(i => i.BagNo, s => s.MapFrom(i => i.Bagno))
                .ForMember(i => i.ArticleNumber, s => s.MapFrom(i => i.ArticleNumber))
                .ForMember(i => i.SendPlatform, s => s.MapFrom(i => i.SendPlatform))
                .ForMember(i => i.ArticleName, s => s.MapFrom(i => i.ArticleName))
                .ForMember(i => i.ShipDate, s => s.MapFrom(i => i.ShipDate))
                .ForMember(i => i.RoundTrip, s => s.MapFrom(i => i.RoundTrip));

            this.CreateMap<TcDeliveryRequest, DeliveryRequestMenuVerifyEntity>()
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.Station, s => s.MapFrom(i => i.AreaArriveCode))
                .ForMember(i => i.ScanDate, s => s.MapFrom(i => i.LatestScanDate));

            this.CreateMap<WriteOffCheckList, WriteOffCheckListEntity>()
                .ForMember(i => i.Id, s => s.MapFrom(i => i.Id))
                .ForMember(i => i.Information, s => s.MapFrom(i => i.Information))
                .ForMember(i => i.Type, s => s.MapFrom(i => i.Type));

            CreateMap<TcDeliveryRequest, DeliveryRequestJoinScanLogForMatingRate>()
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.AreaArriveCode, s => s.MapFrom(i => i.AreaArriveCode));

            this.CreateMap<DeliveryRequestApiEntity, TcDeliveryRequest>()
                .ForMember(i => i.RequestId, s => s.MapFrom(i => i.RequestId))
                .ForMember(i => i.ReceiptFlag, s => s.MapFrom(i => false))
                .ForMember(i => i.RoundTrip, s => s.MapFrom(i => false))
                .ForMember(i => i.CustomerCode, s => s.MapFrom(i => i.CustomerCode))
                .ForMember(i => i.CheckNumber, s => s.MapFrom(i => i.CheckNumber))
                .ForMember(i => i.OrderNumber, s => s.MapFrom(i => i.OrderNumber))
                .ForMember(i => i.ReceiveCustomerCode, s => s.MapFrom(i => i.ReceiveCustomerCode))
                .ForMember(i => i.SubpoenaCategory, s => s.MapFrom(i => i.SubpoenaCategory))
                .ForMember(i => i.ReceiveTel1, s => s.MapFrom(i => i.ReceiveTel1))
                .ForMember(i => i.ReceiveTel2, s => s.MapFrom(i => i.ReceiveTel2))
                .ForMember(i => i.ReceiveContact, s => s.MapFrom(i => i.ReceiveContact))
                .ForMember(i => i.ReceiveAddress, s => s.MapFrom(i => i.ReceiverAddress))
                .ForMember(i => i.Pieces, s => s.MapFrom(i => i.Pieces))
                .ForMember(i => i.Plates, s => s.MapFrom(i => i.Plates))
                .ForMember(i => i.CbmSize, s => s.MapFrom(i => i.CbmSize))
                .ForMember(i => i.CollectionMoney, s => s.MapFrom(i => i.CollectionMoney))
                .ForMember(i => i.SendContact, s => s.MapFrom(i => i.SendContact))
                .ForMember(i => i.SendTel, s => s.MapFrom(i => i.SendTel))
                .ForMember(i => i.SendAddress, s => s.MapFrom(i => i.SenderAddress))
                .ForMember(i => i.InvoiceDesc, s => s.MapFrom(i => i.InvoiceDesc))
                .ForMember(i => i.ArriveAssignDate, s => s.MapFrom(i => i.ArriveAssignDate))
                .ForMember(i => i.TimePeriod, s => s.MapFrom(i => i.TimePeriod))
                .ForMember(i => i.SupplierCode, s => s.MapFrom(i => i.SupplierCode))
                .ForMember(i => i.PrintDate, s => s.MapFrom(i => i.PrintDate))
                .ForMember(i => i.ArticleNumber, s => s.MapFrom(i => i.ArticleNumber))
                .ForMember(i => i.SendPlatform, s => s.MapFrom(i => i.SendPlatform))
                .ForMember(i => i.ArticleName, s => s.MapFrom(i => i.ArticleName))
                .ForMember(i => i.CbmWeight, s => s.MapFrom(i => i.Weight))
                .AfterMap((source, destination) =>
                {
                    destination.PricingType = "02";
                    destination.CheckType = "001";
                    destination.ReceiveTel1Ext = string.Empty;
                    destination.ReceiveZip = string.Empty;
                    destination.ReceiveArea = string.Empty;
                    destination.AreaArriveCode = string.Empty;
                    destination.ReceiveByArriveSiteFlag = false;
                    destination.ArriveAddress = string.Empty;
                    destination.Cbm = null;
                    destination.SendZip = string.Empty;
                    destination.SendCity = string.Empty;
                    destination.SendArea = string.Empty;
                    destination.ReceiptFlag = (source.ReceiptFlag == "Y" || source.ReceiptFlag == "1") ? true : false;
                    destination.PalletRecyclingFlag = false;
                    destination.SupplierName = string.Empty;
                    destination.SubCheckNumber = "000";
                    destination.ImportRandomCode = "";
                    destination.ArriveToPayFreight = null;
                    destination.ArriveToPayAppend = null;
                    destination.SendZip = null;
                    destination.DonateInvoiceFlag = null;
                    destination.ElectronicInvoiceFlag = null;
                    destination.UniformNumbers = null;
                    destination.ArriveMobile = null;
                    destination.ArriveEmail = null;
                    destination.InvoiceMemo = null;
                    destination.ProductCategory = "001";
                    destination.SpecialSend = null;
                    destination.TimePeriod = "";
                    destination.PalletType = null;
                    destination.SupplierDate = null;
                    destination.ReceiptNumbe = null;
                    destination.SupplierFee = null;
                    destination.CsectionFee = null;
                    destination.RemoteFee = null;
                    destination.TotalFee = null;
                    destination.PrintFlag = false;
                    destination.CheckoutCloseDate = null;
                    destination.AddTransfer = false;
                    destination.CloseRandomCode = null;
                    destination.TurnBoard = false;
                    destination.Upstairs = false;
                    destination.DifficultDelivery = false;
                    destination.TurnBoardFee = 0;
                    destination.UpstairsFee = 0;
                    destination.DifficultFee = 0;
                    destination.CancelDate = null;
                    destination.HcTstatus = null;
                    destination.IsPallet = false;
                    destination.PalletRequestId = 0;
                    destination.LessThanTruckload = true;
                    destination.Distributor = null;
                    destination.Temperate = null;
                    destination.CbmLength = null;
                    destination.CbmWidth = null;
                    destination.CbmHeight = null;
                    destination.CbmCont = null;
                    destination.ShipDate = null;
                    destination.DeliveryType = "D";
                    destination.Bagno = string.Empty;
                    destination.RoundTrip = (source.RoundTrip == "Y" || source.RoundTrip == "1") ? true : false;
                    if( source.CollectionMoney > 0 )
                    {
                        //代收貨款
                        destination.SubpoenaCategory = "41";
                    }
                    else
                    {
                        //元付
                        destination.SubpoenaCategory = "11";
                    }
                    Regex periodreg = new Regex("-");
                    string timePeriod = "";
                    if (periodreg.IsMatch(source.TimePeriod))
                    {
                        int s = source.TimePeriod.IndexOf("-", 0, source.TimePeriod.Length);
                        timePeriod = source.TimePeriod.Substring(s + 1, 1);
                    }
                    else
                    {
                        timePeriod = "不指定";
                    }
                    destination.TimePeriod = timePeriod;
                    destination.Uuser = "skyeyes";
                });

        }
    }
}
