using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Models
{
    public class APIResponse<T>
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
