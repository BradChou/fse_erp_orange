﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.BL.Services.Reports;
using Microsoft.AspNetCore.Mvc;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class AddressUtilityController : Controller
    {
        IDeliveryRequestService DeliveryRequestService;
        IReportPrintService ReportPrintService;

        public AddressUtilityController(IReportPrintService reportPrintService, IDeliveryRequestService deliveryRequestService)
        {
            this.ReportPrintService = reportPrintService;
            this.DeliveryRequestService = deliveryRequestService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetAddressArea(string address)
        {
            var data = this.DeliveryRequestService.GetCityAreaEntitiesByAddress(address);

            return Json(data);
        }

        [HttpGet]
        [Produces("application/xml")]
        public IEnumerable<AreaArriveCodeForLabelEntity> NormalizeAddress(string requestIds)
        {
            var result = DeliveryRequestService.GetAreaArriveCodeByRequestIds(requestIds);

            return result;
        }

        [HttpGet]
        [Produces("application/xml")]
        public IEnumerable<AreaArriveCodeForLabelEntity> NormalizeAddressByInfoKey(string infoKey)
        {
            var result = DeliveryRequestService.GetAreaArriveCodeForLabelEntitiesByInfoKey(infoKey);

            return result;
        }
    }
}
