﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.PalletCustomerWriteOff;
using LightYear.JunFuTrans.BL.Services.PalletCustomerWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class PalletCustomerWriteOffController : Controller
    {
        IPalletCustomerWriteOffService PalletCustomerWriteOffService; 
        public PalletCustomerWriteOffController(IPalletCustomerWriteOffService palletCustomerWriteOffService)
        {
            PalletCustomerWriteOffService = palletCustomerWriteOffService;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Manual()
        {
            return View();
        }

        [Authorize]
        public IActionResult History()
        {
            return View();
        }

        [Authorize]
        public IActionResult CustomerMonthWriteOff(DateTime startDate, DateTime endDate, string shortCustomerCode, string shortCustomerName)
        {
            ViewBag.Date = endDate.ToString("d", CultureInfo.CreateSpecificCulture("ja-JP"));
            ViewBag.Month = endDate.ToString("MM");
            ViewBag.Year = endDate.ToString("yyyy");
            ViewBag.StartWriteOffDate = startDate.ToString("d", CultureInfo.CreateSpecificCulture("ja-JP"));
            //ViewBag.StartWritreOffDate = date.AddMonths(-1).AddDays(1).ToString("d", CultureInfo.CreateSpecificCulture("ja-JP"));
            ViewBag.CustomerCode = shortCustomerCode;
            ViewBag.ShortCustomerName = shortCustomerName;

            return View();
        }
       
        public IActionResult ForManualGetLastMonthClosingDateOrLastManualClosingDate(DateTime closingDate, string shortCustomerCode)
        {
            string output = PalletCustomerWriteOffService.ForManualGetLastMonthClosingDateOrLastManualClosingDate(closingDate, shortCustomerCode).ToString("d", CultureInfo.CreateSpecificCulture("ja-JP"));
            return Json(output);
        }

        public IActionResult GetNotWriteOffCustomerMonthDetail(DateTime date, string shortCustomerCode = null, string callback = "callback")
        {
            var startDate = PalletCustomerWriteOffService.ForManualGetLastClosingDate(date, shortCustomerCode);

            List<PalletCustomerWriteOffEntity> Entities = PalletCustomerWriteOffService.GetNotWriteOffCustomerMonthDetail(startDate, date, shortCustomerCode);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult GetAlreadyWriteOffMonthCustomer(DateTime startDate, DateTime endDate, string shortCustomerCode = null, string callback = "callback")
        {
            List<PalletCustomerWriteOffEntity> Entities = PalletCustomerWriteOffService.GetAlreadyWriteOffMonthCustomer(startDate, endDate, shortCustomerCode);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult GetMonthCustomerAlreadyWriteOffDetail(DateTime startDate, DateTime endDate, string shortCustomerCode = null, string callback = "callback")
        {
            List<Detail> Entities = PalletCustomerWriteOffService.GetMonthCustomerAlreadyWriteOffDetail(startDate, endDate, shortCustomerCode);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult GetTotalPaymentAndPlatesByOneSupplier(DateTime date, string callback = "callback")
        {
            List<ShowInFrontend> Entities = PalletCustomerWriteOffService.GetTotalPaymentAndPlatesByOneSupplier(date);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult GetPalletCustomers()
        {
            return Json(PalletCustomerWriteOffService.GetTbSuppliersAndCheckoutDate());
        }

        public IActionResult FindCustomers(string shortCustomerCode)
        {
            return Json(PalletCustomerWriteOffService.FindCustomerEntities(shortCustomerCode));
        }

        public IActionResult GetHistoryByCustomerCodeAndMonths(DateTime startMonth, DateTime endMonth, string shortCustomerCode, string callback = "callback")
        {
            List<PalletCustomerRequestPaymentLog> Entities = PalletCustomerWriteOffService.GetHistoryByCustomerCodeAndMonths(startMonth, endMonth, shortCustomerCode);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult UpdateClosingDateAndInsertPaymentLog(string models, string closingDate, string shortCustomerCode)
        {
            DateTime closingDate1 = DateTime.Parse(closingDate, CultureInfo.CreateSpecificCulture("ja-JP"), DateTimeStyles.NoCurrentDateDefault);
            string message = "成功";
            List<PalletCustomerWriteOffEntity> Entities = JsonConvert.DeserializeObject<List<PalletCustomerWriteOffEntity>>(models);
           
            List<string> checknumbers = Entities.Select(a => a.CheckNumber).ToList();

            PalletCustomerWriteOffService.UpdateClosingDate(checknumbers, closingDate1);
            
            if (Entities[0].PaymentEntity.Total > 0)
                PalletCustomerWriteOffService.InsertPaymentLog(Entities[0], closingDate1, shortCustomerCode);

            return Json(JsonConvert.SerializeObject(message));
        }

        public IActionResult GetLineChartByCustomerCodeAndYear(string customerCode, DateTime year, string callback = "callback")
        {
            var Entities = PalletCustomerWriteOffService.GetLineChart(customerCode, year.Year);

            string data = JsonConvert.SerializeObject(Entities);

            //string output = string.Format("{0}({1})", callback, data);

            return Content(data);
        }

        /// <summary>
        /// 下載單一客戶的已結帳帳單Excel
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public IActionResult ExportFile(string json)
        {
            try
            {
                InputJson ans = JsonConvert.DeserializeObject<InputJson>(json);
                var bytes = PalletCustomerWriteOffService.ExportAlreadyWriteOffExcel(ans.StartDate, ans.EndDate, ans.ShortCustomerCode);
                var customerName = PalletCustomerWriteOffService.FindCustomerEntities(ans.ShortCustomerCode).FirstOrDefault()?.CustomerShortname;
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", DateTime.Now.ToString("yyyy/MM/dd") + customerName + "帳單.xlsx");
            }
            catch (Exception e)
            {
                string resultCode = "400";
                string errorMessage = e.Message;
                object obj = new { resultCode, errorMessage };
                string response = JsonConvert.SerializeObject(obj);
                return Content(response);
            }
        }

        /// <summary>
        /// 下載單一客戶的未結帳帳單Excel
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public IActionResult ExportNotWriteOffFile(string json)
        {
            try
            {
                InputJson ans = JsonConvert.DeserializeObject<InputJson>(json);
                var startDate = PalletCustomerWriteOffService.ForManualGetLastClosingDate(ans.EndDate, ans.ShortCustomerCode);
                var bytes = PalletCustomerWriteOffService.ExportNotWriteOffExcel(startDate, ans.EndDate, ans.ShortCustomerCode);
                var customerName = PalletCustomerWriteOffService.FindCustomerEntities(ans.ShortCustomerCode).FirstOrDefault()?.CustomerShortname;
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", DateTime.Now.ToString("yyyy/MM/dd") + customerName + "帳單.xlsx");
            }
            catch (Exception e)
            {
                string resultCode = "400";
                string errorMessage = e.Message;
                object obj = new { resultCode, errorMessage };
                string response = JsonConvert.SerializeObject(obj);
                return Content(response);
            }
        }

        /// <summary>
        /// 產生所有客戶的帳單壓縮檔
        /// </summary>
        /// <param name="forZipJson"></param>
        /// <returns></returns>
        public IActionResult GenerateZipFile(string forZipJson)
        {
            try
            {
                List<InputJson> ans = JsonConvert.DeserializeObject<List<InputJson>>(forZipJson);
                var zipStream = PalletCustomerWriteOffService.GenerateZipFile(ans); 
                return File(zipStream, "application/octet-stream", DateTime.Now.ToString("yyyy/MM/dd") + "棧板客戶帳單.zip");
            }
            catch (Exception e)
            {
                string resultCode = "400";
                string errorMessage = e.Message;
                object obj = new { resultCode, errorMessage };
                string response = JsonConvert.SerializeObject(obj);
                return Content(response);
            }
        }
    }
}