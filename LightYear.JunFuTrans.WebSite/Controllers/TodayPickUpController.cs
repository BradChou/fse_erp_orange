﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services.TodayPickUp;
using LightYear.JunFuTrans.BL.BE.TodayPickUp;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.PerformanceReport;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class TodayPickUpController : Controller
    {
        ITodayPickUpService TodayPickUpService;
        public TodayPickUpController(ITodayPickUpService todayPickUpService)
        {
            TodayPickUpService = todayPickUpService;
        }

        public IActionResult TodayPickUpGetData(string json, string callback ="callback")
        {
            TodayPickUpInputEntity input = JsonConvert.DeserializeObject<TodayPickUpInputEntity>(json);

            List<TodayPickUpEntity> todayPickUpEntities = TodayPickUpService.TodayPickUpEntities(input);

            string jsonOutput = JsonConvert.SerializeObject(todayPickUpEntities);

            string output = string.Format("{0}({1})", callback, jsonOutput);

            return Content(output);
        }

        public IActionResult TodayPickUp()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.UserName = userInfoEntity.UserName;
            ViewBag.StationScode = userInfoEntity.Station;


            ViewBag.StationShowName = userInfoEntity.StationShowName;


            var PickUpCount = TodayPickUpCount(userInfoEntity.UserAccount);

            ViewBag.SuccessCount = PickUpCount.SuccessCount;
            ViewBag.FailCount = PickUpCount.FailCount;


            return View();
        }

        public TodayPickUpCount TodayPickUpCount(string driverCode)
        {

            TodayPickUpInputEntity input = new TodayPickUpInputEntity();
            input.DriverCode = driverCode;

            TodayPickUpCount TodayPickUpCount = TodayPickUpService.TodayPickUpCount(input);


            return TodayPickUpCount;


        }

    }
}
