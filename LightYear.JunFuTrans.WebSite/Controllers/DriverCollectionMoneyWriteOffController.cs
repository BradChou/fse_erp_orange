﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.Services.DriverCollectionMoneyWriteOff;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.BE.DriverCollectionMoneyWriteOff;
using LightYear.JunFuTrans.BL.BE.Account;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DriverCollectionMoneyWriteOffController : Controller
    {
        IDriverCollectionMoneyWriteOffService DriverCollectionMoneyWriteOffService;

        public DriverCollectionMoneyWriteOffController(IDriverCollectionMoneyWriteOffService driverCollectionMoneyWriteOffService)
        {
            this.DriverCollectionMoneyWriteOffService = driverCollectionMoneyWriteOffService;
        }

        public IActionResult Index()
        {

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            string station_level = userInfoEntity.station_level;
            string station = userInfoEntity.Station;

            if (station_level == "9")
            {
                station = "-9999";
            }

            if (station.StartsWith("F"))
            {
                station = "";
            }

            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UsersId = userInfoEntity.UsersId;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = station;
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;

            return View();
        }

        public IActionResult GetAllByStationAndDatetime(string scode, DateTime start, DateTime end,  string callback = "callback",string station_level="",string management = "", string station_area = "")
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UsersId = userInfoEntity.UsersId;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;

            List<DriverCollectionMoneyWriteOffEntity> Entities = DriverCollectionMoneyWriteOffService.GetAllByStationAndDatetime(scode, start, end, userInfoEntity.station_level, userInfoEntity.management, userInfoEntity.station_area, userInfoEntity.station_scode);
            string performance = JsonConvert.SerializeObject(Entities);
            string output = string.Format("{0}({1})", callback, performance);
            return Content(output);

        }

    }
}