﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.Services.PickUpInfo;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class PickUpInfoController : Controller
    {
        readonly IPickUpInfoService PickUpInfoService;

        public IStationRepository StationRepository { get; set; }

        private readonly IConfiguration config;

        public PickUpInfoController(IPickUpInfoService pickUpInfoService, IConfiguration configuration, IStationRepository stationRepository)
        {
            this.PickUpInfoService = pickUpInfoService;
            this.config = configuration;
            StationRepository = stationRepository;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetPickUpInfo()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string stationScode = userInfoEntity.Station;
            if (stationScode.StartsWith("F"))
                ViewBag.Station = "";
            else
                ViewBag.Station = StationRepository.GetStationCodeByStationScode(stationScode);

            ViewBag.AccountCode = userInfoEntity.UserAccount;

            return View();
        }

        public IActionResult GetMdDropDown()
        {
            List<object> mdList = new List<object>();

            for(int i=1; i<=20; i++)
            {
                mdList.Add(new { value = i, showName = i.ToString() });
            }

            return Json(mdList);
        }

        public IActionResult GetPickUpInfoGrid(string stationSCode, DateTime start, DateTime end, string customerCode = "", int isPickUped = -1, string isNormal = "2", string callback = "callback")
        {
            string entities = string.Empty;

            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            //ViewBag.Station_level = userInfoEntity.station_level;
            //ViewBag.Station_scode = userInfoEntity.station_scode;
            //ViewBag.Station_area = userInfoEntity.station_area;
            //ViewBag.Management = userInfoEntity.management;


            List<PickUpInfoShowEntity> data;

            if (isNormal == "2") // 一般
            {
                data = this.PickUpInfoService.GetFrontendPickUpEntitiesByStationCode(stationSCode, start, end, customerCode, isPickUped, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management, userInfoEntity.station_scode);
            }
            else // 甲配
            {
                data = PickUpInfoService.GetFrontendPickUpEntitiesBySendArea(stationSCode, start, end, customerCode, isPickUped, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management, userInfoEntity.station_scode);
            }

            entities = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, entities);

            return Content(output);
        }

        public IActionResult UpdateAbnormalPickUpInfo(string models, string accountCode, string isNormal = "2", string callback = "callback")
        {
            IEnumerable<PickUpInfoShowEntity> updateData = JsonConvert.DeserializeObject<List<PickUpInfoShowEntity>>(models);
            PickUpInfoService.UpdateRoInfo(updateData, accountCode, isNormal);            
            string output = string.Format("{0}({1})", callback, models);
            return Content(output);
        }

        public IActionResult GetRoArray()
        {
            IEnumerable<PickUpOptionsEntity> options = PickUpInfoService.GetItemCodesOptionsAllowToEdit();
            var data = JsonConvert.SerializeObject(options);
            return Content(data);
        }
    }
}
