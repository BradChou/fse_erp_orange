﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.RemittanceAndCheckWriteOff;
using LightYear.JunFuTrans.BL.Services.RemittanceAndCheckWriteOff;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class RemittanceAndCheckWriteOffController : Controller
    {
        IRemittanceAndCheckWriteOffService RemittanceAndCheckWriteOffService;

        public RemittanceAndCheckWriteOffController(IRemittanceAndCheckWriteOffService remittanceAndCheckWriteOffService)
        {
            RemittanceAndCheckWriteOffService = remittanceAndCheckWriteOffService;
        }

        public IActionResult Index()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            return View();
        }

        public IActionResult GetAllByDateAndStation(DateTime start, DateTime end, string scode, string callback = "callback" )
        {
            var Entities = RemittanceAndCheckWriteOffService.GetAllByDateAndStation( start,  end,  scode);
            string performance = JsonConvert.SerializeObject(Entities);
            string output = string.Format("{0}({1})", callback, performance);
            return Content(output);
        }

        public IActionResult ChangeWriteOffByRequestId(string requestId, int writeoff)
        {
            List<int> ids = new List<int>();

            string[] tempArray = requestId.Split(',');

            for (var i = 0; i < tempArray.Length; i++)
            {
                var data = int.Parse(tempArray[i]);
                ids.Add(data);
            }

            this.RemittanceAndCheckWriteOffService.ChangePaymentWriteOffByRequestId(ids,writeoff);

            string json = JsonConvert.SerializeObject(requestId);

            return Json(json);
        }
    }
}