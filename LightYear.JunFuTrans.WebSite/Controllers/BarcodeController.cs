﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.BL.BE.Account;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE;
using LightYear.JunFuTrans.BL.BE.Barcode;
using LightYear.JunFuTrans.BL.Services.Barcode;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel;
using System.Text;
using System.Reflection;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class BarcodeController : Controller
    {
        IDeliveryRequestService DeliveryRequestService { get; set; }
        IScanItemService ScanItemService { get; set; }

        private readonly IConfiguration config;

        public BarcodeController(IDeliveryRequestService deliveryRequestService, IScanItemService scanItemService, IConfiguration config)
        {
            DeliveryRequestService = deliveryRequestService;
            ScanItemService = scanItemService;
            this.config = config;
        }

        public IActionResult Login(string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [Authorize]
        public IActionResult Index()
        {
            // 先去輸入車牌號碼
            var carNumber = HttpContext.Session.GetString("CarNumber");
            if (carNumber == null)
            {
                return RedirectToAction("EnterCarNumber");
            }

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();

            CookieOptions options = new CookieOptions
            {
                Expires = DateTime.Now.AddDays(-1),
                Domain = this.config.GetValue<string>("CookieDomain")
            };
            Response.Cookies.Append("OtherLogin", "", options);

            return RedirectToAction("Login", "Barcode");//導至登入頁
        }

        public IActionResult SaveCarNumber(string carNumber)
        {
            HttpContext.Session.SetString("CarNumber", carNumber);

            return RedirectToAction("Index");
        }

        public IActionResult EnterCarNumber()
        {
            return View();
        }

        // 批次掃(集貨、卸集) 先做集貨
        public IActionResult BatchScan(ScanItem scanItem, int pickUpType = 0)
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.DriverCode = userInfoEntity.UserAccount;
            ViewBag.ScanItem = (int)scanItem;
            ViewBag.Action = ScanItemService.GetScanItemChinese(scanItem);

            if (pickUpType > 0)
            {
                ViewBag.PickUpType = pickUpType;
                ViewBag.PickUpTypeDesc = ScanItemService.GetEnumDescription((PickUpGoodTypeEntity)pickUpType);
            }

            return View();
        }

        public IActionResult Scan(ScanItem scanItem)
        {
            // 集貨要先選貨件種類，之後導向 BatchScan
            if (scanItem == ScanItem.PickUp)
                return RedirectToAction("SelectPickUpType");

            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            var arriveOptionList = Enum.GetValues(typeof(ArriveOption)).Cast<ArriveOption>().Select(v => new SelectListItem
            {
                Text = ScanItemService.GetArriveOptionChinese(v),
                Value = ((int)v).ToString(),
                Selected = ((int)v).Equals(3)
            }).ToList();

            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.DriverCode = userInfoEntity.UserAccount;
            ViewBag.ScanItem = (int)scanItem;
            ViewBag.Action = ScanItemService.GetScanItemChinese(scanItem);
            ViewBag.ArriveOption = arriveOptionList;

            return View();
        }

        public IActionResult SelectPickUpType()
        {
            var enums = Enum.GetValues(typeof(PickUpGoodTypeEntity));
            List<int> enumValues = new List<int>();
            var descriptions = new List<string>();

            foreach (int e in enums)
            {
                descriptions.Add(ScanItemService.GetEnumDescription((PickUpGoodTypeEntity)e));
                enumValues.Add(e);
            }

            ViewBag.Values = enumValues;
            ViewBag.Descriptions = descriptions;

            return View();
        }

        public IActionResult GetBarcodeData(string barcode)
        {
            var deliverRequestEntity = this.DeliveryRequestService.GetDeliveryRequest(barcode);

            if (deliverRequestEntity != null)
            {
                return Json(new { Success = 1, deliverRequestEntity.CheckNumber, deliverRequestEntity.ReceiverName, Status = deliverRequestEntity.CurrentStatusName, LastScanDate = deliverRequestEntity.LastScanDateTime });
            }
            else
            {
                return Json(new { Success = 0 });
            }
        }

        private ScanDataEntity GetScanData(string barcode, string driverCode, ScanItem scanItem)
        {
            var deliverRequestEntity = this.DeliveryRequestService.GetDeliveryRequest(barcode);

            if (deliverRequestEntity != null)
            {
                ScanDataEntity scanData = new ScanDataEntity
                {
                    CheckNumber = deliverRequestEntity.CheckNumber,
                    Platform = deliverRequestEntity.SendPlatform,
                    Area = deliverRequestEntity.Area,
                    AreaArriveCode = deliverRequestEntity.AreaArriveCode,
                    GoodsType = deliverRequestEntity.ProductCategory ?? "",
                    Pieces = deliverRequestEntity.Pieces.ToString(),
                    Weight = deliverRequestEntity.CbmWeight.ToString(),
                    Plates = deliverRequestEntity.Plates.ToString()
                };
                scanData.ScanDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                scanData.DriverCode = driverCode;
                scanData.ScanItem = ((int)scanItem).ToString();
                return scanData;
            }
            else
            {
                return null;
            }
        }


        public async Task<IActionResult> PickUpCallWebService(string driverCode, ScanItem scanItem, string pickUpType, string barcode)
        {
            List<Object> responses = new List<Object>();

            JArray barcodes = JArray.Parse(barcode);
            foreach(var e in barcodes)
            {
                var data = GetScanData(e.ToString(), driverCode, scanItem);
                
                JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
                string scanlogResponse = await webServiceSoapClient.sendUploadData3Async(data.ScanItem, data.DriverCode, e.ToString(), data.ScanDate, string.Empty,
                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, data.ShipMode,
                    data.GoodsType, string.Empty, string.Empty, string.Empty, string.Empty, data.SignFormImage, data.SignFieldImage);

                string pickupTypeResponse = await webServiceSoapClient.SelectPickUpTypeAsync(e.ToString(), pickUpType);

                responses.Add(new { sendUpLoadData = scanlogResponse, SelectPickUpType = pickupTypeResponse });
            }

            string output = JsonConvert.SerializeObject(responses);
            return Content(output);
        }

        public async Task<IActionResult> CallWebService(string barcode, string driverCode, ScanItem scanItem, string json)
        {
            // 司機輸入的
            json = json.Replace("undefined", "");
            DriverEnteredScanDataEntity driverEnteredEntity = JsonConvert.DeserializeObject<DriverEnteredScanDataEntity>(json);

            // 資料庫裏面的
            var data = GetScanData(barcode, driverCode, scanItem);

            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
            string response = await webServiceSoapClient.sendUploadData3Async(data.ScanItem, data.DriverCode, driverEnteredEntity.CheckNumber, data.ScanDate, driverEnteredEntity.AreaArriveCode,
                driverEnteredEntity.Platform, driverEnteredEntity.CarNumber, driverEnteredEntity.SowageRate, driverEnteredEntity.Area, driverEnteredEntity.ExceptionOption, driverEnteredEntity.ArriveOption, data.ShipMode,
                data.GoodsType, driverEnteredEntity.Pieces, driverEnteredEntity.Weight, driverEnteredEntity.Runs, driverEnteredEntity.Plates, data.SignFormImage, data.SignFieldImage);

            return Content(response);
        }

        public async Task<IActionResult> WebServiceSelectPickUpType(string barcode, string pickUpType)
        {
            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
            string response = await webServiceSoapClient.SelectPickUpTypeAsync(barcode, pickUpType);

            return Content(response);
        }
    }
}