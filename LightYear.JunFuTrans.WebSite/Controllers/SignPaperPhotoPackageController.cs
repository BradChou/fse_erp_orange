﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;
using LightYear.JunFuTrans.BL.Services.SignPaperPhotoPackage;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class SignPaperPhotoPackageController : Controller
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            return config;
        }
        public ISignPaperPhotoPackageService SignPaperPhotoPackageService;
        public SignPaperPhotoPackageController(ISignPaperPhotoPackageService signPaperPhotoPackageService)
        {
            SignPaperPhotoPackageService = signPaperPhotoPackageService;
        }

        [Authorize]
        public IActionResult GetRequestsInfo(string json, string callback = "callback")
        {
            SignPaperPhotoInputEntity signPaperPhotoInputEntities = JsonConvert.DeserializeObject<SignPaperPhotoInputEntity>(json);
            IEnumerable<SignPaperPhotoOutputEntity> signPaperPhotoEntities = SignPaperPhotoPackageService.GetArrivedDeliveryRequest(signPaperPhotoInputEntities.CustomerCode, signPaperPhotoInputEntities.PrintDateStart, signPaperPhotoInputEntities.PrintDateEnd, signPaperPhotoInputEntities.ShipDateStart, signPaperPhotoInputEntities.ShipDateEnd);
            string jsonSignPaperEntities = JsonConvert.SerializeObject(signPaperPhotoEntities);
            string output = string.Format("{0}({1})", callback, jsonSignPaperEntities);

            return Content(output);
        }

        public IActionResult SignPaperPhoto()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginAccountCode = userInfoEntity.UserAccount;
            return View();
        }

        public bool DownloadPics(string obj)
        {
            obj = obj.Replace(@"\","/");              //AWS路徑的斜線方向不一樣
            //RequestEntity reqEntity = JsonConvert.DeserializeObject<RequestEntity>(obj);        //未完成，產生AWS需要的物件
            var configs = Configs();

            string folderPath = configs["OriginalPath"];
            if(!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            DirectoryInfo dirs = new DirectoryInfo(folderPath);

            string fileName = folderPath + @"/" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
            var f = System.IO.File.Create(fileName);
            f.Close();

            if (System.IO.File.Exists(fileName))
            {
                System.IO.File.WriteAllText(fileName, obj);
            }

            return true;
        }

        public IActionResult GetProgressData(string loginAccountCode, string callback = "callback")
        {
            List<ProgressEntity> progresses = SignPaperPhotoPackageService.GetProgress(loginAccountCode);
            string jsonReturn = JsonConvert.SerializeObject(progresses);
            string output = string.Format("{0}({1})", callback, jsonReturn);
            return Content(output);
        }
    }
}
