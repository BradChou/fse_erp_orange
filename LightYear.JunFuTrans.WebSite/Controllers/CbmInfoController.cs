﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.BL.Services.CbmInfoSearcher;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.BL.BE.CbmInfo;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.BE.Account;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class CbmInfoController : Controller
    {
        ICbmInfoSearcherService CbmInfoSearcherService;
        public CbmInfoController(ICbmInfoSearcherService cbmInfoService)
        {
            CbmInfoSearcherService = cbmInfoService;
        }

        [Authorize]
        public IActionResult GetCbmInfo(string json, string callback = "callback")
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            CbmInfoInputEntity cbmInfoInputEntities = JsonConvert.DeserializeObject<CbmInfoInputEntity>(json);
            cbmInfoInputEntities.End = cbmInfoInputEntities.End?.AddDays(1);

            CbmInfoEntity[] cbmInfoEntities = CbmInfoSearcherService.GetCbmInfoSearcher(cbmInfoInputEntities,userInfoEntity.station_level, userInfoEntity.management, userInfoEntity.station_area, userInfoEntity.station_scode);
            string jsonCbmEntities = JsonConvert.SerializeObject(cbmInfoEntities);
            string output = string.Format("{0}({1})", callback, jsonCbmEntities);

            return Content(output);
        }

        [Authorize]
        public IActionResult CbmInfo()
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            //若工號未綁定站所
            if (userInfoEntity.Station.Length == 0)
            { ViewBag.Station = "0"; }
            else { ViewBag.Station = userInfoEntity.Station; }
            return View();
        }
    }
}
