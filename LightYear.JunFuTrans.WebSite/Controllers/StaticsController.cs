﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.BL.Services.Dashboard;
using LightYear.JunFuTrans.BL.BE.Statics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.BL.BE.Account;
using Newtonsoft.Json;
using System.Security.Claims;
using NPOI.SS.Formula.Functions;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.Services.StationWithArea;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class StaticsController : Controller
    {
        readonly IDeliveryRequestService DeliveryRequestService;
        readonly IDashboardService DashboardService;

        private readonly IConfiguration config;

        readonly IStationAreaService StationAreaService;

        public JunFuDbContext JunFuDbContext { get; set; }

        public StaticsController(IDeliveryRequestService deliveryRequestService, IDashboardService dashboardService, IConfiguration configuration, JunFuDbContext junFuDbContext, IStationAreaService stationAreaService)
        {
            this.DeliveryRequestService = deliveryRequestService;
            this.DashboardService = dashboardService;
            this.config = configuration;
            this.JunFuDbContext = junFuDbContext;
            StationAreaService = stationAreaService;
        }

        [Authorize]
        public IActionResult Index()
        {
            List<DeliveryRequestStaticEntity> deliveryRequestStaticEntities = this.DeliveryRequestService.GetPassWeeksStaticsData(10);

            string strTotal = string.Empty;
            string strMomo = string.Empty;
            string strNonMomo = string.Empty;
            string timeZoneStr = string.Empty;

            foreach (DeliveryRequestStaticEntity deliveryRequestStaticEntity in deliveryRequestStaticEntities)
            {
                strTotal = string.Format(",{0}{1}", deliveryRequestStaticEntity.TotalCount, strTotal);
                strMomo = string.Format(",{0}{1}", deliveryRequestStaticEntity.MomoCount, strMomo);
                strNonMomo = string.Format(",{0}{1}", deliveryRequestStaticEntity.NonMomoCount, strNonMomo);
                timeZoneStr = string.Format(",\"{0}\"{1}", deliveryRequestStaticEntity.ZoneString, timeZoneStr);
            }

            strTotal = (strTotal.Length > 0) ? strTotal.Substring(1) : string.Empty;
            strMomo = (strMomo.Length > 0) ? strMomo.Substring(1) : string.Empty;
            strNonMomo = (strNonMomo.Length > 0) ? strNonMomo.Substring(1) : string.Empty;
            timeZoneStr = (timeZoneStr.Length > 0) ? timeZoneStr.Substring(1) : string.Empty;

            ViewBag.Total = strTotal;
            ViewBag.Momo = strMomo;
            ViewBag.NonMomo = strNonMomo;
            ViewBag.TimeZone = timeZoneStr;

            return View();
        }

        [Authorize]
        public IActionResult NotDeliveryList(string checkDate = "", int days = 1)
        {
            DateTime checkDateTime = DateTime.Today.AddDays(-1);

            try
            {
                checkDateTime = Convert.ToDateTime(checkDate);
            }
            catch
            {
                //dummy
            }

            ViewBag.CheckDate = checkDateTime.ToString("yyyy-MM-dd");
            ViewBag.Days = days;

            return View();
        }

        [Authorize]
        public IActionResult DPlusOneNotDeliverySuccessList(string deliveryDate = "")
        {
            DateTime deliveryDateTime = DateTime.Today;

            try
            {
                deliveryDateTime = Convert.ToDateTime(deliveryDate);
            }
            catch
            {
                //dummy
            }
            ViewBag.DeliveryDate = deliveryDateTime.ToString("yyyy-MM-dd");

            return View();
        }

        [Authorize]
        public IActionResult GetDPlusOneNotDeliverySuccessList(string deliveryDate = "", string callback = "callback")
        {
            //取得登入訊息
            UserInfoEntity userInfoEntity = new UserInfoEntity();

            foreach (Claim claim in HttpContext.User.Claims)
            {
                if (claim.Type == "UserInfo")
                {
                    string userInfoString = claim.Value;

                    userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoString);
                }
            }

            DateTime deliveryDateTime = DateTime.Today;

            try
            {
                deliveryDateTime = Convert.ToDateTime(deliveryDate);
            }
            catch
            {
                //dummy
            }
            List<string> stationList = this.DeliveryRequestService.GetAccountStationList(userInfoEntity.UserAccount);
            var notSuccessDeliveryEntities = new List<DashboardDeliveryRequestFrontendShowEntity>();
            if (stationList.Count > 0)
            {
                notSuccessDeliveryEntities = this.DeliveryRequestService.GetDPlusOneNotSuccessDeliveryList(stationList, deliveryDateTime);
            }
            else
            {
                notSuccessDeliveryEntities = this.DeliveryRequestService.GetDPlusOneNotSuccessDeliveryList(userInfoEntity.Station, deliveryDateTime);
            }
            

            string entities = JsonConvert.SerializeObject(notSuccessDeliveryEntities);

            string output = string.Format("{0}({1})", callback, entities);

            return Content(output);

        }

        [Authorize]
        public IActionResult ShowDepotNonFinishListView(int depotCategory, string checkDate = "")
        {
            DateTime checkDateTime = DateTime.Today;

            try
            {
                checkDateTime = Convert.ToDateTime(checkDate);
            }
            catch
            {
                //dummy
            }
            ViewBag.CheckDate = checkDateTime.ToString("yyyy-MM-dd");
            ViewBag.DepotCategory = depotCategory;
            return View();
        }

        [Authorize]
        public IActionResult GetDepotNonFinishList(string checkDate = "", int depotCategory = 0, string callback = "callback")
        {
            //取得登入訊息
            UserInfoEntity userInfoEntity = new UserInfoEntity();

            foreach (Claim claim in HttpContext.User.Claims)
            {
                if (claim.Type == "UserInfo")
                {
                    string userInfoString = claim.Value;

                    userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoString);
                }
            }

            DateTime checkyDateTime = DateTime.Today;

            try
            {
                checkyDateTime = Convert.ToDateTime(checkDate);
            }
            catch
            {
                //dummy
            }
            List<string> stationList = this.DeliveryRequestService.GetAccountStationList(userInfoEntity.UserAccount);
            var depotList = new List<DashboardDeliveryRequestFrontendShowEntity>();
            if (stationList.Count>0)
            {
                depotList = this.DeliveryRequestService.GetDepotCategoryCount(stationList, checkyDateTime, depotCategory);
            }
            else
            {
                depotList = this.DeliveryRequestService.GetDepotCategoryCount(userInfoEntity.Station, checkyDateTime, depotCategory);
            }

            string entities = JsonConvert.SerializeObject(depotList);

            string output = string.Format("{0}({1})", callback, entities);

            return Content(output);

        }

        [Authorize]
        public IActionResult GetNotRequestDeliveryList(string checkDate = "", int days = 1, string callback = "callback")
        {
            //取得登入訊息
            UserInfoEntity userInfoEntity = new UserInfoEntity();

            foreach (Claim claim in HttpContext.User.Claims)
            {
                if (claim.Type == "UserInfo")
                {
                    string userInfoString = claim.Value;

                    userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoString);
                }
            }

            DateTime checkDateTime = DateTime.Today.AddDays(-1);

            try
            {
                checkDateTime = Convert.ToDateTime(checkDate);
            }
            catch
            {
                //dummy
            }

            var personEntities = this.DeliveryRequestService.GetNotDeliverList(userInfoEntity.Station, checkDateTime, days);

            string students = JsonConvert.SerializeObject(personEntities);

            string output = string.Format("{0}({1})", callback, students);

            return Content(output);
        }

        [Authorize]
        public IActionResult StationShowNums()
        {
            //取得登入訊息
            UserInfoEntity userInfoEntity = new UserInfoEntity();

            foreach (Claim claim in HttpContext.User.Claims)
            {
                if (claim.Type == "UserInfo")
                {
                    string userInfoString = claim.Value;

                    userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoString);
                }
            }

            DateTime checkDateTime = DateTime.Today;

            List<string> stationList = this.DeliveryRequestService.GetAccountStationList(userInfoEntity.UserAccount);
            //計算配達率
            int total = 0;
            int successNum = 0;
            if (stationList.Count > 0)
            {
                total = this.DeliveryRequestService.GetDPlusOneShouldDeliveryCount(stationList, checkDateTime);
                successNum = this.DeliveryRequestService.GetDPlusOneSuccessDeliveryCount(stationList, checkDateTime);
            }
            else
            {
                total = this.DeliveryRequestService.GetDPlusOneShouldDeliveryCount(userInfoEntity.Station, checkDateTime);
                successNum = this.DeliveryRequestService.GetDPlusOneSuccessDeliveryCount(userInfoEntity.Station, checkDateTime);
            }

            decimal successRate = (total == 0) ? 0 : (successNum * 100 / total);

            //計算留庫
            Dictionary<int, int> depotCount = new Dictionary<int, int>();
            if (stationList.Count > 0)
            {
                depotCount = this.DeliveryRequestService.GetDepotCount(stationList, checkDateTime);
            }
            else
            {
                depotCount = this.DeliveryRequestService.GetDepotCount(userInfoEntity.Station, checkDateTime);
            }

            if (stationList.Count > 0)
            {
                ViewBag.StationShowName = string.Join(',', stationList);
            }
            else
            {
                ViewBag.StationShowName = userInfoEntity.StationShowName;
            }

            ViewBag.Total = total;
            ViewBag.NotYetNum = (total - successNum);
            ViewBag.Percentage = Math.Round(successRate);
            ViewBag.DPlus1Depot = depotCount[1];
            ViewBag.DPlus2Depot = depotCount[2];
            ViewBag.DPlus3Depot = depotCount[3];
            return View();
        }

        public IActionResult GetDeliveryAnomaly()
        {
            return View();
        }

        public IActionResult MenuVerifyErrorDeliverRequest()
        {
            //var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            //var userInfoStr = claim.Value;
            //UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            //ViewBag.Station = userInfoEntity.Station;
            //ViewBag.Area = StationAreaService.GetByStationScode(userInfoEntity.Station).Area;
            //ViewBag.Account = userInfoEntity.UserAccount;
            return View();
        }

        public async Task<IActionResult> VerifyDeliveryRequest(string models, string callback = "callback")
        {
            List<DeliveryRequestMenuVerifyEntity> deliveryRequestMenuVerifyEntities = JsonConvert.DeserializeObject<List<DeliveryRequestMenuVerifyEntity>>(models);

            // 存到 write off 
            this.DeliveryRequestService.SaveMenuVerifyDeliveryRequest(deliveryRequestMenuVerifyEntities[0]);

            // 存到 delivery scanlog

            string info = deliveryRequestMenuVerifyEntities[0].VerifyOption.Information.Trim();

            var arriveoption = (from aa in JunFuDbContext.TbItemCodes where aa.CodeName == info select aa.CodeId).FirstOrDefault();

            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);

            await webServiceSoapClient.InsertDeliveryScanLogAsync(deliveryRequestMenuVerifyEntities[0].VerifyStation.StationCode,
               deliveryRequestMenuVerifyEntities[0].VerifyEmpCode, arriveoption, deliveryRequestMenuVerifyEntities[0].CheckNumber);

            await webServiceSoapClient.UpdateDeliveryRequestAsync(deliveryRequestMenuVerifyEntities[0].CheckNumber);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult FindErrorDeliveryRequest(DateTime start, DateTime end, string empCode = "", string checkNumber = "", string areaName = "", string scode = "", string callback = "callback")
        {
            string entities = string.Empty;

            if (empCode != null && empCode.Length > 0)
            {
                var data = this.DeliveryRequestService.GetNeedToVerifyDeliveryRequestByEmpCode(empCode);

                entities = JsonConvert.SerializeObject(data);
            }
            else if (checkNumber != null && checkNumber.Length > 0)
            {
                var data = this.DeliveryRequestService.GetNeedToVerifyDeliveryRequestByCheckNumber(checkNumber);

                entities = JsonConvert.SerializeObject(data);
            }
            else
            {
                var data = this.DeliveryRequestService.GetNeedToVerifyDeliveryRequestByScanDate(start, end.AddDays(1), areaName, scode);

                entities = JsonConvert.SerializeObject(data);
            }

            string output = string.Format("{0}({1})", callback, entities);

            return Content(output);
        }

        public IActionResult GetDeliveryAnomalyGrid(string station, string customer, DateTime start, DateTime end, DeliveryErrorTypeConfig checkType, string callback = "callback")
        {
            string entities = string.Empty;

            DateTime realEnd = end.AddDays(1);


            if (customer != null && customer.Length > 0)
            {
                var data = this.DeliveryRequestService.GetFrontendErrorListByCustomerCode(customer, start, realEnd, checkType);

                entities = JsonConvert.SerializeObject(data);
            }
            else if (station != null && station.Length > 0)
            {
                var data = this.DeliveryRequestService.GetFrontendErrorListByArriveCode(station, start, realEnd, checkType);

                entities = JsonConvert.SerializeObject(data);
            }

            string output = string.Format("{0}({1})", callback, entities);

            return Content(output);
        }
        public IActionResult Dashboard()
        {
            ViewBag.TotalShouldDeliveryCount = this.DashboardService.TotalShouldDeliveryCount;
            ViewBag.TotalNotYetNum = this.DashboardService.TotalNotYetNum;
            ViewBag.SuccessRate = this.DashboardService.SuccessRate;
            ViewBag.TotalDPlus3Depot = this.DashboardService.TotalDPlus3Depot;

            System.Diagnostics.Debug.WriteLine(this.DashboardService.TotalShouldDeliveryCount);
            System.Diagnostics.Debug.WriteLine(this.DashboardService.TotalNotYetNum);
            System.Diagnostics.Debug.WriteLine(this.DashboardService.SuccessRate);
            System.Diagnostics.Debug.WriteLine(this.DashboardService.TotalDPlus3Depot);

            return View();
        }

        public IActionResult GetAllStationStatisticsList(string callback = "callback")
        {
            var data = this.DashboardService.GetAllStationStatistics();

            string entities = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, entities);

            //統計指標

            ViewBag.TotalShouldDeliveryCount = this.DashboardService.TotalShouldDeliveryCount;
            ViewBag.TotalNotYetNum = this.DashboardService.TotalNotYetNum;
            ViewBag.SuccessRate = this.DashboardService.SuccessRate;
            ViewBag.TotalDPlus3Depot = this.DashboardService.TotalDPlus3Depot;

            System.Diagnostics.Debug.WriteLine(this.DashboardService.TotalShouldDeliveryCount);
            System.Diagnostics.Debug.WriteLine(this.DashboardService.TotalNotYetNum);
            System.Diagnostics.Debug.WriteLine(this.DashboardService.SuccessRate);
            System.Diagnostics.Debug.WriteLine(this.DashboardService.TotalDPlus3Depot);
            return Content(output);
        }
    }
}