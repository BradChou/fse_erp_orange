﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.Services.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.BL.Services.DeliveryException;
using Microsoft.AspNetCore.Authorization;
using ExcelDataReader;
using JunFuTrans.DA.JunFuTrans.condition;
using JunFuTrans.DA.JunFuTrans.DA;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class DeliveryAndDeliveryMatingController : Controller
    {
        private IWebHostEnvironment webHostEnvironment;
        IDeliveryAndDeliveryMatingService DeliveryAndDeliveryMatingService;
        private readonly IConfiguration config;

        public DeliveryAndDeliveryMatingController(IDeliveryAndDeliveryMatingService deliveryAndDeliveryMatingService, IWebHostEnvironment webHostEnvironment, IConfiguration config, IDeliveryExceptionService deliveryExceptionService)
        {
            DeliveryAndDeliveryMatingService = deliveryAndDeliveryMatingService;
            this.webHostEnvironment = webHostEnvironment;
            this.config = config;
        }

        [Authorize]
        public IActionResult DriverView()
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;

            //另一種傳遞的方法
            //List<KendoSelectEntity> kendoSelectEntities = DeliveryAndDeliveryMatingService.GetAllByDriverCodeStationScodeArriveOption(userInfoEntity.UserAccount, userInfoEntity.Station);
            //string countpieces = string.Empty;
            //string countarriveroption = string.Empty;
            //foreach(KendoSelectEntity kendoSelectEntity in kendoSelectEntities)
            //{
            //    countpieces = string.Format(",{0}{1}",kendoSelectEntity.CountPieces,countpieces);
            //    countarriveroption = string.Format(",{0}{1}",kendoSelectEntity.CountArriveOptions,countarriveroption);
            //}
            //countpieces = (countpieces.Length > 0) ? countpieces.Substring(1) : string.Empty;
            //countarriveroption = (countarriveroption.Length > 0) ? countarriveroption.Substring(1) : string.Empty;
            //ViewBag.countpieces = countpieces;
            //ViewBag.countarriveoption = countarriveroption;
            return View();
        }

        public IActionResult GetAll(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string callback = "callback")
        {

            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;


            try
            {

                var Entities = DeliveryAndDeliveryMatingService.GetAll(start, end, driverCode, stationscode, arriveOption, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management,userInfoEntity.Station);

                if (Entities.Count() > 1)
                {
                    TempData["Sum"] = Entities[Entities.Count() - 1].ListSum;
                }

                string performance = JsonConvert.SerializeObject(Entities);
                string output = string.Format("{0}({1})", callback, performance);
                return Content(output);
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        public IActionResult GetArriveOptions()
        {

            return Json(DeliveryAndDeliveryMatingService.GetArriveOptions());
        }

        public IActionResult GetAllDrivers(string stationscode = "")
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;
            stationscode = stationscode ?? string.Empty;

            if (stationscode.Length > 0 && stationscode != "" && stationscode != null)
            {
                return Json(DeliveryAndDeliveryMatingService.GetDriversByStation(userInfoEntity.station_level,userInfoEntity.station_area, userInfoEntity.management, stationscode, userInfoEntity.Station));
            }
            else
            {
                return Json(DeliveryAndDeliveryMatingService.GetAllDrivers(userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management, stationscode, userInfoEntity.station_scode, userInfoEntity.Station));
            }
        }

        public IActionResult GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string callback = "callback")
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;

            int count = DeliveryAndDeliveryMatingService.GetCountPieces(start, end, driverCode, stationscode, arriveOption, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management, userInfoEntity.Station);
            string serializedStr = JsonConvert.SerializeObject(count);
            string output = string.Format("{0}({1})", callback, serializedStr);
            return Content(output);
        }

        public IActionResult GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string callback = "callback")
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;

            int count = DeliveryAndDeliveryMatingService.GetCountArriveOptions(start, end, driverCode, stationscode, arriveOption, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management, userInfoEntity.Station);
            string serializedStr = JsonConvert.SerializeObject(count);
            string output = string.Format("{0}({1})", callback, serializedStr);
            return Content(output);
        }

        public IActionResult GetSumOfCollectionMoney(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string callback = "callback")
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;

            int count = DeliveryAndDeliveryMatingService.GetAll(start, end, driverCode, stationscode, arriveOption, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management,userInfoEntity.Station).Sum(x => x.CollectionMoney);
            string serializedStr = JsonConvert.SerializeObject(count);
            string output = string.Format("{0}({1})", callback, serializedStr);
            return Content(output);
        }


        [HttpPost]
        public async Task<IActionResult> ImportFile(IFormFile file)
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");


            if (file == null)
            {
                TempData["Response"] = "請選擇檔案";

                return RedirectToAction("DriverView");
            }
            try
            {
                List<DeliveryAndDeliveryMatingEntity> Entities = new List<DeliveryAndDeliveryMatingEntity>();

                string fullPath = string.Empty;
                var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
                var userInfoStr = claim.Value;
                UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
                var AccountCode = userInfoEntity.UserAccount;


                if (file.Length > 0)
                {
                    //拆解前記錄
                    PickUppImportRecord _PickUppImportRecord = new PickUppImportRecord
                    {
                        AccountCode = AccountCode,
                        Status = "Start"
                    };
                    PickUppImportRecord_DA _PickUppImportRecord_DA = new PickUppImportRecord_DA();

                    //查詢
                    if (_PickUppImportRecord_DA.GetRecord(AccountCode) == "Start")
                    {
                        TempData["Status"] = "作業處理中，請稍待";
                        return RedirectToAction("DriverView");
                    }

                    _PickUppImportRecord_DA.InsertRecord(_PickUppImportRecord);

                    var savePath = this.config.GetValue<string>("UploadFileUrl");
                    string uniqueFileName = DeliveryAndDeliveryMatingService.GetUniqueFileName(file.FileName);
                    fullPath = string.Format("{0}/{1}", savePath, uniqueFileName);
                    using (FileStream stream = new FileStream(fullPath, FileMode.Append))
                    {
                        await file.CopyToAsync(stream);
                    }

                    System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                    using (var stream = System.IO.File.Open(fullPath, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet();

                            var rows = result.Tables[0].Rows.Count;//獲取總行數
                                                                   //var columns = result.Tables[0].Columns.Count;//獲取總列數

                            var info = "";
                            if (rows > 1) //除了標題行外有資料才讀取
                            {
                                for (int i = 1; i < rows; i++) // 避開標題行，從第二行開始讀取
                                {
                                    var table_result = result.Tables[0].Rows[i];


                                    DeliveryAndDeliveryMatingEntity Entity = new DeliveryAndDeliveryMatingEntity(); // i每加1，就new一個Entity
                                    int resultint;
                                    Entity.Id = Convert.ToInt32(table_result[0]);  //序號
                                    Entity.CheckNumber = table_result[1].ToString();  //貨號
                                    Entity.SubpoenaCategory = table_result[2].ToString();  //傳票類別
                                    Entity.Pieces = int.TryParse(table_result[3].ToString(), out resultint) ? resultint : 0; //件數
                                    Entity.CbmSize = table_result[4].ToString();   //材積
                                    Entity.ArriveOption = table_result[5].ToString();   //配達區分
                                    Entity.ReceiveContact = table_result[6].ToString();   //收件人姓名
                                    Entity.ReceiveTel = table_result[7].ToString();  //收件人電話
                                    Entity.RoundTrip = table_result[8].ToString();  //來回件備註
                                    Entity.TimePeriod = table_result[9].ToString();  //指定配送時間
                                    Entity.ReceiveAddress = table_result[10].ToString();  //配送地址
                                    Entity.ScanDate = Convert.ToDateTime(table_result[11].ToString());  //掃讀時間                                   
                                    Entity.DriverNameCode = table_result[12].ToString();  //作業司機
                                    Entity.CollectionMoney = Convert.ToInt32(table_result[13]);  //代收貨款
                                    Entity.SDMDCode = table_result[14].ToString();  //配區碼
                                    Entity.ThirdPartyFlag = table_result[15].ToString();  //3PL註記

                                    Entities.Add(Entity);

                                }

                                info = DeliveryAndDeliveryMatingService.CheckNumberToPickupp(Entities);

                                //結束記錄
                                PickUppImportRecord _PickUppImportRecordEnd = new PickUppImportRecord
                                {
                                    AccountCode = AccountCode,
                                    Status = "End"
                                };
                                PickUppImportRecord_DA _PickUppImportRecordEnd_DA = new PickUppImportRecord_DA();
                                _PickUppImportRecord_DA.InsertRecord(_PickUppImportRecordEnd);

                                //info=info.Replace("\"},{\"checknumber\":\"", "\r\n貨號: ").Replace("\", \"message\":\"",":").Replace("[{\"success_count\"","成功筆數").Replace("\"error_count\"","失敗筆數").Replace("\"error\":[{","\r\n錯誤訊息: \r\n").Replace("\",\"message\":\"", "，  訊息: ").Replace("\"checknumber\":\"","貨號: ").Replace("\"}]}]","").Replace("貨號:", "\r\n貨號:");


                                info = info.Replace("\"},{\"checknumber\":\"", "\n貨號: ").Replace("\", \"message\":\"", ":").Replace("[{\"success_count\"", "成功筆數").Replace("\"error_count\"", "失敗筆數").Replace("\"error\":[{", "錯誤訊息: ").Replace("\",\"message\":\"", "，  訊息: ").Replace("\"checknumber\":\"", "\n貨號: ").Replace("\"}]}]", "");//.Replace("貨號:", "\n貨號:");

                            }
                            else
                            {
                                //結束記錄
                                PickUppImportRecord _PickUppImportRecordEnd = new PickUppImportRecord
                                {
                                    AccountCode = AccountCode,
                                    Status = "End"
                                };
                                PickUppImportRecord_DA _PickUppImportRecordEnd_DA = new PickUppImportRecord_DA();
                                _PickUppImportRecord_DA.InsertRecord(_PickUppImportRecordEnd);

                                return Content("沒有資料無法上傳");
                            }
                            return Content(info);
                            //TempData["Response"] = info;

                            //return RedirectToAction("DriverView");


                        }
                    }
                }

                JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);



                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }

                return Ok("上傳成功");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });

            return Content(response);
        }

    }
}
