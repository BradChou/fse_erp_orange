﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.DeliveryException;
using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.BL.Services.CustomerStationBinding;
using LightYear.JunFuTrans.BL.Services.StationWithArea;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using LightYear.JunFuTrans.DA.Repositories.Station;
using ExcelDataReader;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;


namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class StationAreaController : Controller
    {
        IStationAreaService StationAreaService { get; set; }

        ICustomerStationBindingService CustomerStationBindingService { get; set; }

        public IOrgAreaRepository OrgAreaRepository { get; set; }

        private readonly IConfiguration config;

        public StationAreaController(IStationAreaService stationAreaService, ICustomerStationBindingService customerStationBindingService, IOrgAreaRepository orgAreaRepository, IConfiguration config)
        {
            StationAreaService = stationAreaService;
            CustomerStationBindingService = customerStationBindingService;
            OrgAreaRepository = orgAreaRepository;
            this.config = config;
        }

        [Authorize]
        public IActionResult List()
        {
            return View();
        }

        [HttpPost, ActionName("Export")]
        public IActionResult Export()
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                var bytes = StationAreaService.Export();
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "路段新增修改明細表.xlsx");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }

        [HttpPost]
        public async Task<IActionResult> ImportFile(IFormFile file)
        {
            bool resultCode = true;
            string errorMessage = "";

            try
            {
                List<DA.JunFuTransDb.OrgArea> Entities = new List<DA.JunFuTransDb.OrgArea>();

                string fullPath = string.Empty;

                if (file.Length > 0)
                {
                    var savePath = this.config.GetValue<string>("UploadFileUrl");
                    string uniqueFileName = StationAreaService.GetUniqueFileName(file.FileName);
                    fullPath = string.Format("{0}/{1}", savePath, uniqueFileName);
                    using (FileStream stream = new FileStream(fullPath, FileMode.Append))
                    {
                        await file.CopyToAsync(stream);
                    }

                    System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                    using (var stream = System.IO.File.Open(fullPath, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet();

                            var rows = result.Tables[0].Rows.Count;//獲取總行數

                            if (rows > 1) //除了標題外有資料才讀取
                            {
                                for (int i = 1; i < rows; i++) // 避開標題行，從第2列開始讀取
                                {
                                    DA.JunFuTransDb.OrgArea tempEntity = new DA.JunFuTransDb.OrgArea();
                                    tempEntity.Office = result.Tables[0].Rows[i][0].ToString();
                                    tempEntity.Zip3A = result.Tables[0].Rows[i][1].ToString();
                                    tempEntity.Zipcode = result.Tables[0].Rows[i][2].ToString();
                                    tempEntity.City = result.Tables[0].Rows[i][3].ToString();
                                    tempEntity.Area = result.Tables[0].Rows[i][4].ToString();
                                    tempEntity.Road = result.Tables[0].Rows[i][5].ToString();
                                    tempEntity.Scoop = result.Tables[0].Rows[i][6].ToString();
                                    tempEntity.StationScode = result.Tables[0].Rows[i][7].ToString();
                                    tempEntity.StationName = result.Tables[0].Rows[i][8].ToString();
                                    tempEntity.SdNo = result.Tables[0].Rows[i][9].ToString();
                                    tempEntity.MdNo = result.Tables[0].Rows[i][10].ToString();
                                    tempEntity.PutOrder = result.Tables[0].Rows[i][11].ToString();
                                    tempEntity.ShuttleStationCode = result.Tables[0].Rows[i][12].ToString();
                                    tempEntity.SendSD = result.Tables[0].Rows[i][13].ToString();
                                    tempEntity.SendMD = result.Tables[0].Rows[i][14].ToString();


                                    if (String.IsNullOrEmpty(tempEntity.Zip3A) || String.IsNullOrEmpty(tempEntity.Zipcode) || String.IsNullOrEmpty(tempEntity.City) || String.IsNullOrEmpty(tempEntity.Area) || String.IsNullOrEmpty(tempEntity.Road) && String.IsNullOrEmpty(tempEntity.Scoop) || String.IsNullOrEmpty(tempEntity.StationScode) || String.IsNullOrEmpty(tempEntity.StationName))
                                    { return Content("請填妥必填欄位"); }
                                    //else if (!StationAreaService.GetAllStationName().Contains(tempEntity.StationName) || !StationAreaService.GetAllStationScode().Contains(tempEntity.StationScode))
                                    //{ return Content("此站所代碼或站所不存在"); }
                                    else
                                    {

                                        Entities.Add(tempEntity);

                                    }

                                }
                            }
                            else
                            {
                                return Content("沒有資料無法上傳");
                            }

                        }
                    }
                }

                StationAreaService.InsertBatch(Entities);

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            TempData["Response"] = string.Concat("上傳", resultCode ? "成功。" : "失敗。", errorMessage);
            return Redirect("RoadMaintenance");
        }


        public IActionResult GetAll(string callback = "callback")
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;


            List<StationAreaEntity> stationAreaEntities = StationAreaService.GetAll(ViewBag.Station_scode, ViewBag.Station_level, ViewBag.Station_area, ViewBag.Management);
            string stationArea = JsonConvert.SerializeObject(stationAreaEntities);
            string output = string.Format("{0}({1})", callback, stationArea);

            return Content(output);
        }

        public IActionResult Insert(string model, string callback = "callback")
        {
            StationAreaEntity stationAreaEntity = JsonConvert.DeserializeObject<StationAreaEntity>(model);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            stationAreaEntity.UpdateUser = userInfoEntity.UserAccount;

            stationAreaEntity = StationAreaService.Insert(stationAreaEntity);
            string jsonEntity = JsonConvert.SerializeObject(stationAreaEntity);

            string output = string.Format("{0}({1})", callback, jsonEntity);

            return Content(output);
        }

        public IActionResult Update(string model, string callback = "callback")
        {
            StationAreaEntity stationAreaEntity = JsonConvert.DeserializeObject<StationAreaEntity>(model);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            stationAreaEntity.UpdateUser = userInfoEntity.UserAccount;

            StationAreaService.Update(stationAreaEntity);
            string jsonEntity = JsonConvert.SerializeObject(stationAreaEntity);

            string output = string.Format("{0}({1})", callback, jsonEntity);

            return Content(output);
        }

        public IActionResult Delete(string model, string callback = "callback")
        {
            StationAreaEntity stationAreaEntity = JsonConvert.DeserializeObject<StationAreaEntity>(model);
            StationAreaService.Delete(stationAreaEntity.Id ?? 0);
            string jsonEntity = JsonConvert.SerializeObject(stationAreaEntity);
            string output = string.Format("{0}({1})", callback, jsonEntity);

            return Content(output);
        }

        public IActionResult GetPost5Code(string station, string callback = "callback")
        {
            //取得登入資料

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            var data = this.StationAreaService.GetPost5CodeMappingEntityByStationSCode(station,userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management,userInfoEntity.station_scode);
            string stationArea = JsonConvert.SerializeObject(data);
            string output = string.Format("{0}({1})", callback, stationArea);

            return Content(output);
        }

        [Authorize]
        public IActionResult Post5MappingSD()
        {
            //取得登入資料

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            string station = userInfoEntity.Station;

            if (station == null || station.Length == 0)
            {
                station = "-9999";
            }

            if (station.StartsWith("F"))
            {
                station = "";
            }

            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UsersId = userInfoEntity.UsersId;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = station;

            return View();
        }

        public IActionResult EditPost5Mapping(string models, string callback = "callback", string station_level="", string station_area="", string management="", string station_scode="")
        {


            List<Post5CodeMappingEntity> post5CodeMappingEntities = JsonConvert.DeserializeObject<List<Post5CodeMappingEntity>>(models);

            try
            {
                var check = this.StationAreaService.SavePost5CodeMappingEntity(post5CodeMappingEntities[0], station_level, station_area, management, station_scode);

                if (check)
                {
                    string output = string.Format("{0}({1})", callback, models);
                    return Content(output);
                }
               
            }
            catch (Exception)
            {

                
            }

            return NotFound("");


        }













        public IActionResult BatchDeletePost5Mapping(string ids)
        {
            string[] aryIds = JsonConvert.DeserializeObject<string[]>(ids);

            int[] intAryIds = Array.ConvertAll(aryIds, s => int.Parse(s));

            OrgAreaRepository.BatchDelete(intAryIds);
            return Content("success");
        }


        [Authorize]
        public IActionResult RoadMaintenance()
        {
            //取得登入資料

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            string station = userInfoEntity.Station;

            if (station == null || station.Length == 0)
            {
                station = "-9999";
            }

            if (station.StartsWith("F"))
            {
                station = "";
            }

            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UsersId = userInfoEntity.UsersId;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = station;

            return View();
        }


        public IActionResult BatchEditPost5Mapping([FromBody] OrgAreaMutiEditEntity orgAreaMutiEditEntity, string type = "0")
        {
            var data = JsonConvert.SerializeObject(orgAreaMutiEditEntity);

            var allIds = orgAreaMutiEditEntity.OrgAreaIds.Split(',');

            List<int> mutiIds = new List<int>();

            foreach (string eachId in allIds)
            {
                try
                {
                    int dataId = Convert.ToInt32(eachId);

                    mutiIds.Add(dataId);
                }
                catch
                {
                    //dummy
                }

            }

            this.StationAreaService.BatchChangOrgData(mutiIds, orgAreaMutiEditEntity.EditValue, type);

            return Content(data);
        }
    }
}