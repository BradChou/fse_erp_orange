﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.BL.Services.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using LightYear.JunFuTrans.BL.Services.GuoYangExportAndImport;
using ExcelDataReader;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using System.Globalization;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class TruckVendorPaymentAndMaintenanceController : Controller
    {
        ITruckVendorPaymentService TruckVendorPaymentService { get; set; }
        private readonly IConfiguration config;
        IGuoYangExportAndImportService GuoYangExportAndImportService { get; set; }

        public ITbItemCodesRepository TbItemCodesRepository { get; private set; }

        public ITruckVendorMaintenanceRepository TruckVendorMaintenanceRepository { get; set; }

        public ITruckVendorPaymentRepository TruckVendorPaymentRepository { get; set; }

        public IMapper Mapper { get; set; }


        public TruckVendorPaymentAndMaintenanceController(ITruckVendorPaymentService truckVendorPaymentService, IConfiguration config, 
            IGuoYangExportAndImportService guoYangExportAndImportService, IMapper mapper, ITbItemCodesRepository itemCodesRepository,
            ITruckVendorMaintenanceRepository truckVendorMaintenanceRepository, ITruckVendorPaymentRepository truckVendorPaymentRepository)
        {
            TruckVendorPaymentService = truckVendorPaymentService;
            this.config = config;
            GuoYangExportAndImportService = guoYangExportAndImportService;
            Mapper = mapper;
            TbItemCodesRepository = itemCodesRepository;
            TruckVendorMaintenanceRepository = truckVendorMaintenanceRepository;
            TruckVendorPaymentRepository = truckVendorPaymentRepository;
        }

        public IActionResult Index()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            ViewBag.UserAccount = userInfoEntity.UserAccount;
            
            string truckVendorId = TruckVendorPaymentService.GetTruckVendorByTruckVendorAccountCode(userInfoEntity.UserAccount)?.Id.ToString();
            
            ViewBag.TruckVendorId = truckVendorId ?? "";
            
            return View();
        }

        public IActionResult GetSuppliersAccountCode()
        {
            return Json(TruckVendorPaymentService.GetSuppliersAccount().Select(a=>a.AccountCode).ToList());
        }

        public IActionResult Upload()
        {
            return View();
        }

        public IActionResult GetAllByMonthAndTruckVendor(DateTime month, string vendor, string callback = "callback")
        {
            DateTime start = new DateTime(month.Year, month.Month, 01, 00, 00, 00);

            DateTime end = start.AddMonths(1);

            var Entities = TruckVendorPaymentService.GetAllByMonthAndTruckVendor(start,end, vendor);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult GetDisbursement(DateTime month, string vendor, string callback = "callback")
        {
            DateTime start = new DateTime(month.Year, month.Month, 01, 00, 00, 00);

            DateTime end = start.AddMonths(1);

            var Entities = TruckVendorPaymentService.GetDisbursementByMonthAndTruckVendor(start, end, vendor);

            string data = JsonConvert.SerializeObject(Entities);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult Save(string models, string callback = "callback")
        {
            List<TruckVendorPaymentLog> truckVendorPaymentLog = JsonConvert.DeserializeObject<List<TruckVendorPaymentLog>>(models);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            for (int i = 0; i < truckVendorPaymentLog.Count; i++)
            {
                if (truckVendorPaymentLog[i].Id == 0)
                {
                    truckVendorPaymentLog[i].CreateUser = userInfoEntity.UserAccount;
                }
                else
                {
                    truckVendorPaymentLog[i].UpdateUser = userInfoEntity.UserAccount;
                }
            }

            var ids = TruckVendorPaymentService.Save(truckVendorPaymentLog);

            for (int i = 0; i < ids.Count; i++)
            {
                truckVendorPaymentLog[i].Id = ids[i];
                truckVendorPaymentLog[i].VendorName = TruckVendorPaymentService.GetTruckVendors().Where(a => a.Id.ToString().Equals(truckVendorPaymentLog[i].VendorName)).FirstOrDefault()?.VendorName;
                truckVendorPaymentLog[i].Company = TruckVendorPaymentService.GetCompanies().Where(a => a.Id.ToString().Equals(truckVendorPaymentLog[i].Company)).FirstOrDefault()?.Name;
                truckVendorPaymentLog[i].FeeType = TruckVendorPaymentService.GetFeeTypes().Where(a => a.Id.ToString().Equals(truckVendorPaymentLog[i].FeeType)).FirstOrDefault()?.Name;
                truckVendorPaymentLog[i].RegisteredDate = truckVendorPaymentLog[i].RegisteredDate.Value.AddHours(-8);
            }

            models = JsonConvert.SerializeObject(truckVendorPaymentLog);

            string output = string.Format("{0}({1})", callback, models);
            return Content(output);
        }

        public IActionResult Update(string models, string callback = "callback")
        {
            List<TruckVendorPaymentLog> truckVendorPaymentLog = JsonConvert.DeserializeObject<List<TruckVendorPaymentLog>>(models);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            truckVendorPaymentLog[0].UpdateUser = userInfoEntity.UserAccount;

            TruckVendorPaymentService.Update(truckVendorPaymentLog[0]);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult GetAllTruckVendors()
        {
           return Json(TruckVendorPaymentService.GetTruckVendors());
        }

        public IActionResult GetFeeTypes()
        {
            return Json(TruckVendorPaymentService.GetFeeTypes());
        }

        public IActionResult GetCompanies()
        {
            return Json(TruckVendorPaymentService.GetCompanies());
        }

        public IActionResult GetIncomeAndExpenditure(DateTime month, string vendor)
        {
            DateTime start = new DateTime(month.Year, month.Month, 01, 00, 00, 00);

            DateTime end = start.AddMonths(1);

            var Income = TruckVendorPaymentService.GetIncomeAndExpenditure(start, end, vendor);

            string data = JsonConvert.SerializeObject(Income);

            return Json(data);    
        }

        [HttpPost]
        public async Task<IActionResult> ImportFile(IFormFile file)
        {
            bool resultCode = true;
            string errorMessage = "";
            List<string> errorMessages = new List<string>();
            List<bool> resultCodeList = new List<bool>();

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            string importRandomCode = Guid.NewGuid().ToString().Substring(0, 10);
            TempData["importRandomCode"] = importRandomCode;

            var truckVendors = TruckVendorPaymentService.GetTruckVendors();

            try
            {
                List<TruckVendorPaymentLog> Entities = new List<TruckVendorPaymentLog>();
                string fullPath = string.Empty;

                if (file.Length > 0)
                {
                    var savePath = this.config.GetValue<string>("UploadFileUrl");
                    string uniqueFileName = GuoYangExportAndImportService.GetUniqueFileName(file.FileName);
                    fullPath = string.Format("{0}/{1}", savePath, uniqueFileName);
                    using (FileStream stream = new FileStream(fullPath, FileMode.Append))
                    {
                        await file.CopyToAsync(stream);
                    }

                    using (var stream = System.IO.File.Open(fullPath, FileMode.Open, FileAccess.Read))
                    {
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet();

                            var rows = result.Tables[0].Rows.Count;//獲取總列數

                            if (rows > 2) //除了標題列與範例列外有資料才讀取
                            {
                                for (int i = 2; i < rows; i++) // 避開標題與範例列，從第三列開始讀取
                                {
                                    TruckVendorPaymentLog Entity = new TruckVendorPaymentLog(); // i每加1，就new一個Entity

                                    decimal resultint;
                                    DateTime Test;

                                    if (String.IsNullOrEmpty(result.Tables[0].Rows[i][0].ToString()))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列發生日期未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    if (DateTime.TryParse(result.Tables[0].Rows[i][0].ToString(), out Test) == true)
                                    { 
                                        resultCode = true;
                                        resultCodeList.Add(resultCode);
                                    }
                                    else
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列發生日期格式有誤，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    Entity.RegisteredDate = Convert.ToDateTime(result.Tables[0].Rows[i][0].ToString());

                                    Entity.FeeType = result.Tables[0].Rows[i][1].ToString();
                                    if (String.IsNullOrEmpty(Entity.FeeType))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列交易項目未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    var feeTypes = TruckVendorPaymentService.GetFeeTypes();

                                    var id = feeTypes.Where(a => a.Name == Entity.FeeType).FirstOrDefault()?.Id;

                                    if (id == null)
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列交易項目有誤，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }


                                    Entity.VendorName = result.Tables[0].Rows[i][2].ToString();
                                    var truckId = truckVendors.Where(a => a.VendorName.Equals(Entity.VendorName) && a.IsActive.Equals(true)).FirstOrDefault()?.Id;
                                    if (truckId == null)
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列供應商名稱出現錯誤，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    if (String.IsNullOrEmpty(Entity.VendorName))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列供應商名稱未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }

                                    Entity.CarLicense = result.Tables[0].Rows[i][4].ToString()?? "";

                                    if (String.IsNullOrEmpty(result.Tables[0].Rows[i][5].ToString()))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列金額未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    Entity.Payment = Convert.ToInt32(Math.Round(Decimal.TryParse(result.Tables[0].Rows[i][5].ToString(), out resultint) ? resultint : 0));

                                    Entity.Memo = result.Tables[0].Rows[i][6].ToString();
                                    if (String.IsNullOrEmpty(Entity.Memo))
                                    {
                                        resultCode = false;
                                        resultCodeList.Add(resultCode);
                                        errorMessage = "第" + (i - 1 + 2).ToString() + "列摘要未填，請更正後重新上傳。";
                                        errorMessages.Add(errorMessage);
                                    }
                                    
                                    Entity.Company = result.Tables[0].Rows[i][7].ToString() ?? "";
                                    Entity.CreateUser = userInfoEntity.UserAccount;
                                    Entity.ImportRandomCode = importRandomCode;

                                    if (resultCode == true)
                                       Entities.Add(Entity);
                                }
                            }
                            else
                            {
                                return Content("沒有資料無法上傳");
                            }
                        }
                    }
                }

                if (!resultCodeList.Contains(false))
                    TruckVendorPaymentService.Save(Entities);

                if (System.IO.File.Exists(fullPath))
                    System.IO.File.Delete(fullPath);
                //return View();
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessages.Add(e.Message);
            }

            TempData["Response"] = string.Concat("上傳", resultCodeList.Contains(false) ? "失敗。" : "成功。", string.Join("", errorMessages));
            
            return RedirectToAction("Upload");
        }

        [HttpPost, ActionName("Export")]
        public IActionResult Export()
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                var bytes = TruckVendorPaymentService.Export();
                return File(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "供應商空白檔案.xlsx");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);
        }

        public async Task<IActionResult> ExportFileByUrl()
        {
            bool resultCode = true;
            string errorMessage = string.Format("沒有問題");

            try
            {
                var path = Path.Combine(
                     Directory.GetCurrentDirectory(), "DownloadFiles",
                      "供應商空白檔案_第二版.xlsx");

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;
                return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "供應商空白檔案_第二版.xlsx");
            }
            catch (Exception e)
            {
                resultCode = false;
                errorMessage = e.Message;
            }
            string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
            return Content(response);

        }

        #region 維護
        public IActionResult Maintenance()
        {
            return View();
        }

        public async Task<IActionResult> SaveExpenditureFeeType(string models, string callback = "callback")
        {
            List<FrontEndShowEntity> entities = JsonConvert.DeserializeObject<List<FrontEndShowEntity>>(models);

            await TruckVendorPaymentService.SaveExpenditureFeeType(entities[0]);

            foreach(var e in entities)
            {
                e.DutyDept = TruckVendorMaintenanceRepository.GetDeptNameByCodeId(e.DutyDept);
            }

            string output = string.Format("{0}({1})", callback, JsonConvert.SerializeObject(entities));

            return Content(output);
        }

        public async Task<IActionResult> SaveIncomeFeeType(string models, string callback = "callback")
        {
            List<FrontEndShowEntity> entities = JsonConvert.DeserializeObject<List<FrontEndShowEntity>>(models);

            await TruckVendorPaymentService.SaveIncomeFeeType(entities[0]);

            foreach (var e in entities)
            {
                e.DutyDept = TruckVendorMaintenanceRepository.GetDeptNameByCodeId(e.DutyDept);
            }

            string output = string.Format("{0}({1})", callback, JsonConvert.SerializeObject(entities));

            return Content(output);
        }

        public IActionResult GetIncomeFeeType(string callback = "callback")
        {
            var vendor = TruckVendorPaymentService.GetIncomeFeeType();

            string data = JsonConvert.SerializeObject(vendor);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult GetExpenditureFeeType(string callback = "callback")
        {
            var vendor = TruckVendorPaymentService.GetExpenditureFeeType();

            string data = JsonConvert.SerializeObject(vendor);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }
        #endregion

        public IActionResult GetTruckVendorForMainten(string callback = "callback")
        {
            var vendor = TruckVendorPaymentService.GetTruckVendorsForMaintenance();

            string data = JsonConvert.SerializeObject(vendor);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public IActionResult SaveTruckVendor(string models, string callback = "callback")
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            List<TruckVendor> truckVendor = JsonConvert.DeserializeObject<List<TruckVendor>>(models);

            if (truckVendor[0].Id == 0)
            {
                truckVendor[0].CreateUser = userInfoEntity.UserAccount;
                TruckVendorPaymentService.SaveTruckVendor(truckVendor[0]);
            }
            else
            {
                truckVendor[0].UpdateUser = userInfoEntity.UserAccount;
                TruckVendorPaymentService.SaveTruckVendor(truckVendor[0]);
            }

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult GetCompanyForMainten(string callback = "callback")
        {
            var vendor = TruckVendorPaymentService.GetCompanyTbItemCodes();

            string data = JsonConvert.SerializeObject(vendor);

            string output = string.Format("{0}({1})", callback, data);

            return Content(output);
        }

        public async Task<IActionResult> SaveCompany(string models, string callback = "callback")
        {
            List<TbItemCode> tbItemCodes = JsonConvert.DeserializeObject<List<TbItemCode>>(models);

            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);

            await webServiceSoapClient.SaveCompanyAsync(tbItemCodes[0].CodeName.Trim(), tbItemCodes[0].ActiveFlag ?? false, tbItemCodes[0].Seq);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }        

        public IActionResult GetLatestUploadData(string ImportRandomCode = "", string callback = "callback")
        {
            var data = TruckVendorPaymentService.GetLatestUploadData(ImportRandomCode);

            string entity = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, entity);

            return Content(output);
        }

        #region 編輯
        [Authorize]
        public IActionResult Edit()
        {
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            string role = userInfoEntity.RoleName.First();

            int codeId;
            switch (role)
            {
                case "業務部":
                    codeId = 1;
                    break;
                case "財務部":
                    codeId = 2;
                    break;
                case "車管部":
                    codeId = 3;
                    break;
                case "行政部":
                    codeId = 4;
                    break;
                case "分析部":
                    codeId = 5;
                    break;
                default:
                    codeId = 0;
                    break;
            }

            ViewBag.Department = codeId;

            return View();
        }

        public IActionResult GetEditPageFrontEntity(string start, string end, string duty, string callback = "callback")
        {
            List<TruckVendorPaymentLogFrontEndShowEntity> data;

            if (duty == "*")
            {
                data = TruckVendorPaymentService.GetFrontEndEntityByTimePeriod(DateTime.Parse(start), DateTime.Parse(end).AddDays(1));
            }
            else if (start == null || end == null)
            {
                data = TruckVendorPaymentService.GetFrontEndEntityByDuty(duty);
            }
            else
            {
                data = TruckVendorPaymentService.GetFrontEndEntityByTimePeriodAndDuty(DateTime.Parse(start), DateTime.Parse(end).AddDays(1), duty);
            }

            string entity = JsonConvert.SerializeObject(data);

            string output = string.Format("{0}({1})", callback, entity);

            return Content(output);
        }

        public IActionResult UpdateVendorPaymentLog(string models, string callback = "callback")
        {
            TruckVendorPaymentLogFrontEndShowEntity truckVendorPaymentEntity = JsonConvert.DeserializeObject<TruckVendorPaymentLogFrontEndShowEntity>(models);

            TruckVendorPaymentLog truckVendorPaymentLog = Mapper.Map<TruckVendorPaymentLog>(truckVendorPaymentEntity);

            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");

            var userInfoStr = claim.Value;

            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            truckVendorPaymentLog.UpdateUser = userInfoEntity.UserAccount;
            truckVendorPaymentLog.UpdateDate = DateTime.Now;

            TruckVendorPaymentService.UpdateVendorLog(truckVendorPaymentLog);

            string output = string.Format("{0}({1})", callback, models);

            return Content(output);
        }

        public IActionResult DeleteVendorPaymentLog(string models, string callback = "callback")
        {
            TruckVendorPaymentLogFrontEndShowEntity truckVendorPaymentEntity = JsonConvert.DeserializeObject<TruckVendorPaymentLogFrontEndShowEntity>(models);

            TruckVendorPaymentService.Delete(truckVendorPaymentEntity);

            string output = string.Format("{0}", callback);

            return Content(output);
        }

        public IActionResult BatchDeleteVendorPaymentLog(string ids)
        {
            string[] aryIds = JsonConvert.DeserializeObject<string[]>(ids);

            int[] intAryIds = Array.ConvertAll(aryIds, s => int.Parse(s));

            TruckVendorPaymentRepository.BatchDelete(intAryIds);
            return Content("success");
        }
        #endregion
    }
}