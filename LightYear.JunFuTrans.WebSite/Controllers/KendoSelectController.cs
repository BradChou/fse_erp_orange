﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.BL.Services.StationWithArea;
using LightYear.JunFuTrans.BL.Services.TruckVendorPaymentAndMaintenance;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.BL.BE.Account;

namespace LightYear.JunFuTrans.WebSite.Controllers
{
    public class KendoSelectController : Controller
    {
        readonly IStationAreaService StationAreaService;
        readonly IDeliveryRequestService DeliveryRequestService;
        readonly IStationRepository StationRepository;
        public ITruckVendorPaymentService TruckVendorPaymentService { get; set; }

        private readonly IConfiguration config;

        public KendoSelectController(IStationAreaService stationAreaService, IDeliveryRequestService deliveryRequestService, IConfiguration configuration,
            IStationRepository stationRepository, ITruckVendorPaymentService truckVendorPaymentService)
        {
            this.StationAreaService = stationAreaService;
            this.DeliveryRequestService = deliveryRequestService;
            this.config = configuration;
            StationRepository = stationRepository;
            TruckVendorPaymentService = truckVendorPaymentService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetErrorWriteOffCheckList()
        {
            var data = Request.QueryString.Value;

            var getQuery = QueryHelpers.ParseQuery(data);

            var callBack = getQuery["$callback"].ToString();

            KendoSelectWriteOffCheckListEntity getReturn = new KendoSelectWriteOffCheckListEntity();

            //取得有異常的項目
            List<WriteOffCheckListEntity> writeOffCheckListEntities = this.DeliveryRequestService.GetErrorWriteOffCheckLists();

            getReturn.results = writeOffCheckListEntities;

            getReturn.__count = writeOffCheckListEntities.Count();

            var objectStr = JsonConvert.SerializeObject(getReturn);

            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }

        public IActionResult GetArea(bool addTotal = false)
        {
            var data = Request.QueryString.Value;
            var getQuery = QueryHelpers.ParseQuery(data);
            var callBack = getQuery["$callback"].ToString();
            var getReturn = StationAreaService.GetAreaList(addTotal);

            var objectStr = JsonConvert.SerializeObject(getReturn);
            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }

        public IActionResult GetAreaFromTbStation(bool addTotal = false)
        {
            var data = Request.QueryString.Value;
            var getQuery = QueryHelpers.ParseQuery(data);
            var callBack = getQuery["$callback"].ToString();
            var getReturn = StationAreaService.GetAreaListFromTbStation(addTotal);

            var objectStr = JsonConvert.SerializeObject(getReturn);
            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }

        public IActionResult GetStation(string areaName = "", string stationSCode = "", bool isNeedAllStation = true, bool addAllInArea = true)
        {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);

            /*
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            */
            /*
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;
            */


            areaName = areaName ?? string.Empty;
            var data = Request.QueryString.Value;

            var getQuery = QueryHelpers.ParseQuery(data);

            var callBack = getQuery["$callback"].ToString();

            //取得開始字串
            string startWith = string.Empty;

            if (getQuery.ContainsKey("$filter"))
            {
                var innerData = getQuery["$filter"].ToString();

                if (innerData.StartsWith("startswith"))
                {
                    startWith = innerData.Substring(innerData.IndexOf('\'') + 1, innerData.LastIndexOf('\'') - innerData.IndexOf('\'') - 1);
                }
            }

            KendoSelectStationEntity getReturn;
            if (stationSCode != null && stationSCode.Length > 0)
            {
                getReturn = StationAreaService.GetStationsByStationSCode(stationSCode, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management, userInfoEntity.station_scode);
            }
            else
            {

                if (areaName.Length > 0 && areaName != "全選")
                {
                    getReturn = StationAreaService.GetStationsByAreaName(areaName, addAllInArea, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management);
                }
                else
                {
                    getReturn = StationAreaService.GetStarWith(startWith, true, isNeedAllStation, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management, userInfoEntity.station_scode,userInfoEntity.Station);
                }
            }

            var objectStr = JsonConvert.SerializeObject(getReturn);

            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }

        public IActionResult GetStationFromTbStation(string areaName = "", string stationSCode = "", bool isNeedAllStation = true, bool addAllInArea = true)
        {

            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            /*
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;
            */
            areaName = areaName ?? string.Empty;
            var data = Request.QueryString.Value;

            var getQuery = QueryHelpers.ParseQuery(data);

            var callBack = getQuery["$callback"].ToString();

            //取得開始字串
            string startWith = string.Empty;

            if (getQuery.ContainsKey("$filter"))
            {
                var innerData = getQuery["$filter"].ToString();

                if (innerData.StartsWith("startswith"))
                {
                    startWith = innerData.Substring(innerData.IndexOf('\'') + 1, innerData.LastIndexOf('\'') - innerData.IndexOf('\'') - 1);
                }
            }

            KendoSelectStationEntity getReturn;
            if (stationSCode != null && stationSCode.Length > 0)
            {
                getReturn = StationAreaService.GetStationsByStationSCode(stationSCode, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management, userInfoEntity.station_scode);
            }
            else
            {
                if (areaName.Length > 0 && areaName != "全選")
                {
                    getReturn = StationAreaService.GetStationsByAreaNameFromTbStation(areaName, addAllInArea);
                }
                else
                {
                    getReturn = StationAreaService.GetStarWith(startWith, true, isNeedAllStation);
                }
            }

            var objectStr = JsonConvert.SerializeObject(getReturn);

            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }

        public IActionResult GetCustomer(string stationSCode = "")
       {
            //取得登入資料
            var claim = HttpContext.User.Claims.First(c => c.Type == "UserInfo");
            var userInfoStr = claim.Value;
            UserInfoEntity userInfoEntity = JsonConvert.DeserializeObject<UserInfoEntity>(userInfoStr);
            ViewBag.LoginName = userInfoEntity.UserName;
            ViewBag.UserAccount = userInfoEntity.UserAccount;
            ViewBag.Station = userInfoEntity.Station;
            ViewBag.Response = TempData["Status"];
            ViewBag.Station_level = userInfoEntity.station_level;
            ViewBag.Station_scode = userInfoEntity.station_scode;
            ViewBag.Station_area = userInfoEntity.station_area;
            ViewBag.Management = userInfoEntity.management;

            string realStationSCode = stationSCode ?? "";

            var data = Request.QueryString.Value;

            var getQuery = QueryHelpers.ParseQuery(data);

            var callBack = getQuery["$callback"].ToString();

            //取得開始字串
            string startWith = string.Empty;

            if (getQuery.ContainsKey("$filter"))
            {
                var innerData = getQuery["$filter"].ToString();

                if (innerData.StartsWith("startswith"))
                {
                    startWith = innerData.Substring(innerData.IndexOf('\'') + 1, innerData.LastIndexOf('\'') - innerData.IndexOf('\'') - 1);
                }
            }

            var getReturn = this.DeliveryRequestService.GetByStationAndStartWith(realStationSCode, startWith, true, userInfoEntity.station_level, userInfoEntity.station_area, userInfoEntity.management, userInfoEntity.station_scode);

            var objectStr = JsonConvert.SerializeObject(getReturn);

            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }

        public IActionResult GetJFDept(bool addTotal = false)
        {
            var data = Request.QueryString.Value;
            var getQuery = QueryHelpers.ParseQuery(data);
            var callBack = getQuery["$callback"].ToString();
            var getReturn = TruckVendorPaymentService.GetJFDept(addTotal);

            var objectStr = JsonConvert.SerializeObject(getReturn);
            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }

        public IActionResult GetStationList(string stationLevel = "", string stationArea = "", string management = "", string stationSCode = "", bool isNeedAllStation = true)
        {

            var data = Request.QueryString.Value;

            var getQuery = QueryHelpers.ParseQuery(data);

            var callBack = getQuery["$callback"].ToString();

            //取得開始字串
            string startWith = string.Empty;

            if (getQuery.ContainsKey("$filter"))
            {
                var innerData = getQuery["$filter"].ToString();

                if (innerData.StartsWith("startswith"))
                {
                    startWith = innerData.Substring(innerData.IndexOf('\'') + 1, innerData.LastIndexOf('\'') - innerData.IndexOf('\'') - 1);
                }
            }

            KendoSelectStationEntity getReturn;
            if (stationSCode != null && stationSCode.Length > 0)
            {
                getReturn = StationAreaService.GetStationsByLevel(stationLevel, stationArea, management, stationSCode);
            }
            else
            {
                if (stationLevel == "5") { isNeedAllStation = true; }
                getReturn = StationAreaService.GetStarWith(startWith, true, isNeedAllStation);
            }

            var objectStr = JsonConvert.SerializeObject(getReturn);

            var output = string.Format("{0}({{d:{1} }})", callBack, objectStr);

            return Content(output);
        }



    }
}