﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Newtonsoft.Json;
using LightYear.JunFuTrans.Utilities.Api;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using JunFuTrans.DA.JunFuTrans.DA;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using RestSharp;
using RestSharp.Authenticators;
using System.Net;
using LightYear.JunFuTrans.BL.BE.AppNotify;

namespace AppNotify
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("開始執行!");

            List<AppNotifyEntity> Entities = new List<AppNotifyEntity>();

            DriverSendPrepare_DA _DriverSendPrepare_DA = new DriverSendPrepare_DA();

            var result = _DriverSendPrepare_DA.GetNotifyData();
            //查詢
            if (result != null)            
            {
                for (int i = 1; i < result.Count; i++) 
                {

                    AppNotifyEntity Entity = new AppNotifyEntity(); // 

                    var work_Station_Scode =  result[i].work_Station_Scode==null?"": result[i].work_Station_Scode;  //作業站 
                    var work_Send_code = result[i].work_Send_code == null ? "" : result[i].work_Send_code;  //集區碼 
                    var work_Delivery_Code =  result[i].work_Delivery_Code == null ? "" : result[i].work_Delivery_Code;  //配區碼
                    var driver_code = result[i].driver_code == null ? "" : result[i].driver_code;//司機代號
                    var TITLE =  "行動派遣";   // 資料來源
                    var sendValue =  "您有 " + result[i].Number + " 筆新的派遣任務";   // 推播訊息

                    //test
                    //var work_Station_Scode= "30";  //作業站 
                    //var work_Send_code = "51";  //集區碼 
                    //var work_Delivery_Code = "81";  //配區碼
                    //var driver_code = "Z010006";//司機代號
                    //var TITLE = Entity.TITLE = "行動派遣";   // 資料來源
                    //var sendValue ="0309test";   // 推播訊息


                    string Info = "";
                    var apiUrl = "http://testwebservice.fs-express.com.tw/Webservice.asmx/AppMsgPush";

                    var client = new RestClient(apiUrl);
                    var request = new RestRequest();
                    request.AddHeader("content-type", "application/x-www-form-urlencoded");
                    request.AddParameter("application/x-www-form-urlencoded", $"work_Station_Scode={work_Station_Scode}&work_Send_code={work_Send_code}&work_Delivery_Code={work_Delivery_Code}&driver_code={driver_code}&TITLE={TITLE}&sendValue={sendValue}", ParameterType.RequestBody);
                    var response = client.Post<AppNotifyEntity>(request);


                    Info = response.Content;


                }

                



            }


               



        }
    }
}
