﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class APIController : ControllerBase
    {
        IDeliveryRequestSimpleService DeliveryRequestSimpleService;

        public APIController(IDeliveryRequestSimpleService deliveryRequestSimpleService)
        {
            this.DeliveryRequestSimpleService = deliveryRequestSimpleService;
        }


        public TcDeliveryRequest Get(long requestId)
        {
            var returnData = this.DeliveryRequestSimpleService.GetRequest(requestId);

            return returnData;
        }
    }
}
