﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EDI_NewController : ControllerBase
    {
        INewDeliveryRequestSimpleService NewDeliveryRequestSimpleService;

        public EDI_NewController(INewDeliveryRequestSimpleService NewdeliveryRequestSimpleService)
        {
            this.NewDeliveryRequestSimpleService = NewdeliveryRequestSimpleService;
        }
        //增加逆物流打單
        public List<EDIResultEntity> Get([FromForm]  string Token, [FromForm] string getJson, [FromForm] bool addPickUpRequest = true)
        {
            return this.NewDeliveryRequestSimpleService.EDIAPI_New(Token, getJson, addPickUpRequest);
        }
    }
}
