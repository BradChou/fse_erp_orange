﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    public class AddressParsingInfo
    {
        /// <summary>
        /// 地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 鄉鎮區
        /// </summary>
        public string Town { get; set; }
        /// <summary>
        /// 路段
        /// </summary>
        public string Road { get; set; }
        /// <summary>
        /// 郵局三碼
        /// </summary>
        public string PostZip3 { get; set; }
        /// <summary>
        /// 郵局3+2
        /// </summary>
        public string PostZip32 { get; set; }
        /// <summary>
        /// 郵局3+3
        /// </summary>
        public string PostZip33 { get; set; }
        /// <summary>
        /// 站所代碼
        /// </summary>
        public string StationCode { get; set; }
        /// <summary>
        /// 站所名稱
        /// </summary>
        public string StationName { get; set; }
        /// <summary>
        /// 配送貨車司機
        /// </summary>
        public string SalesDriverCode { get; set; }
        /// <summary>
        /// 配送機車司機
        /// </summary>
        public string MotorcycleDriverCode { get; set; }
        /// <summary>
        ///  堆疊區
        /// </summary>
        public string StackCode { get; set; }
        public string ShuttleStationCode { get; set; }
        /// <summary>
        /// 集貨貨車司機
        /// </summary>
        public string sendSalesDriverCode { get; set; }
        /// <summary>
        /// 集貨機車司機
        /// </summary>
        public string sendMotorcycleDriverCode { get; set; }

    }
}
