﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    public class SpecialAreaManage
    {
		public long id { get; set; }
		public string City { get; set; }
		public string Town { get; set; }
		public string Road { get; set; }
		public int Lane { get; set; }
		public int Alley { get; set; }
		public int Even { get; set; }
		public int NoStart { get; set; }
		public int NoEnd { get; set; }
		public string KeyWord { get; set; }
		public string StationCode { get; set; }
		public string ShuttleStationCode { get; set; }
		public string SalesDriverCode { get; set; }
		public string MotorcycleDriverCode { get; set; }
		public string StackCode { get; set; }
		public int? Fee { get; set; }
		public int TimeLimit { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime UpdateDate { get; set; }
		public string CreateUser { get; set; }
		public string UpdateUser { get; set; }

	}
}
