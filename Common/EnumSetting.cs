﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public static class EnumSetting
    {
        public enum ResponseCodeEnum
        {
            #region 系統相關
            Success = 200,//成功
            Error = 500,//失敗
            #endregion

      
        }

        #region 方法
        public static string GetString(this ResponseCodeEnum res)
        {
            try
            {
                var type = res.GetType();
                string s = EnumStr.ResponseCodeEnumStr[res];
                return s;
            }
            catch (Exception e)
            {
                return "系統異常";
            }
        }

        #endregion
        class EnumStr
        {
            public static Dictionary<ResponseCodeEnum, string> ResponseCodeEnumStr = new Dictionary<ResponseCodeEnum, string>
            {
            {ResponseCodeEnum.Success,"成功"},
            {ResponseCodeEnum.Error,"失敗"}




            };
        }
    }

}
