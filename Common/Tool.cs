﻿using Common.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Common
{
    public class Tool
    {
        /// <summary>
        /// 地圖解析(API)
        /// </summary>
        /// <param name="Address"></param>
        /// <param name="CustomerCode"></param>
        /// <param name="ComeFrom"></param>
        /// <param name="Type"></param>
        /// <returns></returns>
        public AddressParsingInfo GetAddressParsing(string Address, string CustomerCode, string ComeFrom, string Type = "1")
        {
            AddressParsingInfo Info = new AddressParsingInfo();
            var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetDetail";

            //打API
            var client = new RestClient(AddressParsingURL);
            var request = new RestRequest(Method.POST);
            request.Timeout = 5000;
            request.ReadWriteTimeout = 5000;
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

            var response = client.Post<ResData<AddressParsingInfo>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    Info = response.Data.Data;


                }
                catch (Exception e)
                {
                    throw e;

                }

            }
            else
            {
              throw new Exception("地址解析有誤");
            }

            return Info;
        }

        public SpecialAreaManage GetSpecialAreaFee(string Address, string CustomerCode, string ComeFrom, string Type = "1")
        {
            SpecialAreaManage Info = new SpecialAreaManage();
            var AddressParsingURL = "http://map.fs-express.com.tw/Address/GetSpecialAreaDetail";

            //打API
            var client = new RestClient(AddressParsingURL);
            var request = new RestRequest(Method.POST);
            request.Timeout = 5000;
            request.ReadWriteTimeout = 5000;
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body

            var response = client.Post<ResData<SpecialAreaManage>>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    Info = response.Data.Data;


                }
                catch (Exception e)
                {
                    throw e;

                }

            }
            else
            {
                throw new Exception("地址解析有誤");
            }

            return Info;
        }

    }
}
