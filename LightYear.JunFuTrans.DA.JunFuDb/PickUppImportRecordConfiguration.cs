using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;


namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // PickUppImportRecord
    public class PickUppImportRecordConfiguration : IEntityTypeConfiguration<PickUppImportRecord>
    {
        public void Configure(EntityTypeBuilder<PickUppImportRecord> builder)
        {
            builder.ToTable("PickUppImportRecord", "dbo");
            builder.HasKey(x => x.id).HasName("PK_PickUppImportRecord").IsClustered();

            builder.Property(x => x.id).HasColumnName(@"id").HasColumnType("bigint").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.AccountCode).HasColumnName(@"AccountCode").HasColumnType("nvarchar(50)").IsRequired(false);
            builder.Property(x => x.Status).HasColumnName(@"Status").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50);            
            builder.Property(x => x.CreateDate).HasColumnName(@"CreateDate").HasColumnType("datetime").IsRequired();

        }
    }

}
// </auto-generated>

