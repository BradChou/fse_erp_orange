﻿#pragma warning disable 1591
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
	/// <summary>
	/// ScheduleTask
	/// </summary>
	public class ScheduleTask
    {
		public long id { get; set; }
		public int TaskID { get; set; }
		public string TaskName { get; set; }
		public DateTime StartTime { get; set; }
		public bool Enable { get; set; }
		public DateTime CreateDate { get; set; }
		public ScheduleTask()
		{
			CreateDate = DateTime.Now;
		}
	}
}
