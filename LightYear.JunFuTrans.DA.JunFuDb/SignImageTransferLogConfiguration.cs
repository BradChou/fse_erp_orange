﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.JunFuDb
{

    public class SignImageTransferLogConfiguration : IEntityTypeConfiguration<SignImageTransferLog>
    {
        public void Configure(EntityTypeBuilder<SignImageTransferLog> builder)
        {
            builder.ToTable("SignImageTransferLog", "dbo");
            builder.HasKey(x => x.id).HasName("PK_SignImageTransferLog").IsClustered();

            builder.Property(x => x.id).HasColumnName(@"id").HasColumnType("bigint").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.DataSource).HasColumnName(@"DataSource").HasColumnType("varchar(30)").IsRequired(true);
            builder.Property(x => x.SourcePath).HasColumnName(@"SourcePath").HasColumnType("varchar(MAX)").IsRequired(true);
            builder.Property(x => x.DestPath).HasColumnName(@"DestPath").HasColumnType("varchar(MAX)").IsRequired(true);
            builder.Property(x => x.Status).HasColumnName(@"Status").HasColumnType("varchar(30)").IsRequired(true);
            builder.Property(x => x.cdate).HasColumnName(@"cdate").HasColumnType("datetime").IsRequired(true);
            builder.Property(x => x.Message).HasColumnName(@"Message").HasColumnType("varchar(200)").IsRequired(true);
        }
    }
}
