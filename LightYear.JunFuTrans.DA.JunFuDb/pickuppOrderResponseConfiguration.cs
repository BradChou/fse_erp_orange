using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;


namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // pickuppOrderResponse
    public class pickuppOrderResponseConfiguration : IEntityTypeConfiguration<pickuppOrderResponse>
    {
        public void Configure(EntityTypeBuilder<pickuppOrderResponse> builder)
        {
            builder.ToTable("pickuppOrderResponse", "dbo");
            builder.HasKey(x => x.id).HasName("PK_pickuppOrderResponse").IsClustered();

            builder.Property(x => x.id).HasColumnName(@"id").HasColumnType("nvarchar(50)").IsRequired().HasMaxLength(50);
            builder.Property(x => x.order_number).HasColumnName(@"order_number").HasColumnType("nvarchar(50)").IsRequired().HasMaxLength(50);
            builder.Property(x => x.FSE_check_number).HasColumnName(@"FSE_check_number").HasColumnType("nvarchar(50)").IsRequired().HasMaxLength(50);
            builder.Property(x => x.FSE_sendTime).HasColumnName(@"FSE_sendTime").HasColumnType("DateTime").IsRequired();
            builder.Property(x => x.CreateDate).HasColumnName(@"CreateDate").HasColumnType("DateTime").IsRequired();
            builder.Property(x => x.sendflag).HasColumnName(@"sendflag").HasColumnType("bit").IsRequired();
        }
    }

}
// </auto-generated>

