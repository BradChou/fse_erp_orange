﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    public class SignImageTransferLog
    {
        public long  id { get; set; }
        public string DataSource { get; set; }
        public string SourcePath { get; set; }
        public string DestPath { get; set; }
        public string Status { get; set; }
        public DateTime cdate { get; set; }
        public string Message { get; set; }
        public SignImageTransferLog()
        {
            cdate = DateTime.Now;
        }
    }

}
