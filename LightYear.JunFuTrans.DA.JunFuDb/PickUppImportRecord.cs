using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
	// PickUppImportRecord
	/// <summary>
	/// 
	/// </summary>
	public class PickUppImportRecord
	{
		public long id { get; set; }
		public string AccountCode { get; set; }
		public string Status { get; set; }
		public DateTime CreateDate { get; set; }
	}

}


