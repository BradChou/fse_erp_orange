using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;


namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // AccountUpload
    public class AccountUploadConfiguration : IEntityTypeConfiguration<AccountUpload>
    {
        public void Configure(EntityTypeBuilder<AccountUpload> builder)
        {
            builder.ToTable("AccountUpload", "dbo");
            builder.HasKey(x => x.Id).HasName("PK_AccountUpload").IsClustered();

            builder.Property(x => x.Id).HasColumnName(@"Id").HasColumnType("bigint").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.AccountDate).HasColumnName(@"AccountDate").HasConversion<DateTime>().IsRequired();
            builder.Property(x => x.ExternalNumber).HasColumnName(@"ExternalNumber").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50); 
            builder.Property(x => x.ReceivedAmount).HasColumnName(@"ReceivedAmount").HasColumnType("int").IsRequired();
            builder.Property(x => x.CheckNumber).HasColumnName(@"CheckNumber").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50); 
            builder.Property(x => x.UploadTime).HasColumnName(@"UploadTime").HasColumnType("datetime").IsRequired();
            builder.Property(x => x.UploadUser).HasColumnName(@"UploadUser").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50);
            builder.Property(x => x.IsDel).HasColumnName(@"IsDel").HasColumnType("int").IsRequired();
            builder.Property(x => x.UpdateTime).HasColumnName(@"UpdateTime").HasColumnType("DateTime").IsRequired();
            builder.Property(x => x.UpdateUser).HasColumnName(@"UpdateUser").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50);
        }
    }

}
// </auto-generated>

