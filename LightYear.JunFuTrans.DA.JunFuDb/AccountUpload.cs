using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
	// AccountUpload
	/// <summary>
	/// 代收匯入檔
	/// </summary>
	public class AccountUpload
    {
		public long Id { get; set; }

		public DateTime AccountDate { get; set; }
		public string ExternalNumber { get; set; }
		public int ReceivedAmount { get; set; }
		public string CheckNumber { get; set; }
		public DateTime UploadTime { get; set; }
		public string UploadUser { get; set; }
		public int IsDel { get; set; }
		public DateTime UpdateTime { get; set; }
		public string UpdateUser { get; set; }
	}

}


