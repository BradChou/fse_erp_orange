// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // ttReturnDeliveryPickupLog
    /// <summary>
    /// 退還配送集貨紀錄
    /// </summary>
    public class TtReturnDeliveryPickupLog
    {
        /// <summary>
        /// 序號
        /// </summary>
        public long Seq { get; set; } // seq (Primary key)

        /// <summary>
        /// 貨號
        /// </summary>
        public string CheckNumber { get; set; } // check_number (length: 20)

        /// <summary>
        /// 掃描項目
        /// </summary>
        public string ScanItem { get; set; } // scan_item (length: 2)

        /// <summary>
        /// 掃描日期
        /// </summary>
        public DateTime? ScanDate { get; set; } // scan_date

        /// <summary>
        /// 司機代碼
        /// </summary>
        public string DriverCode { get; set; } // driver_code (length: 20)

        /// <summary>
        /// 簽署表格圖檔
        /// </summary>
        public string SignFormImage { get; set; } // sign_form_image (length: 50)

        /// <summary>
        /// 簽署區域圖檔
        /// </summary>
        public string SignFieldImage { get; set; } // sign_field_image (length: 50)

        /// <summary>
        /// 下一次集貨時間
        /// </summary>
        public DateTime? NextPickupTime { get; set; } // next_pickup_time

        /// <summary>
        /// 公司使用者日期
        /// </summary>
        public DateTime? Cdate { get; set; } // cdate

        /// <summary>
        /// 狀態編碼(1:另約時間、2:電聯不上、3:資料有誤、4:無件可取、5:超大超重、6:同業已取、7:商品未到、8:不退、9:公司休息)
        /// </summary>
        public string ReCode { get; set; } // re_code (length: 2)

        public TtReturnDeliveryPickupLog()
        {
            Cdate = DateTime.Now;
        }
    }

}
// </auto-generated>

