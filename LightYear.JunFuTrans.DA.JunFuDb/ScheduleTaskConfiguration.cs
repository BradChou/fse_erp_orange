﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    public class ScheduleTaskConfiguration : IEntityTypeConfiguration<ScheduleTask>
    {
        public void Configure(EntityTypeBuilder<ScheduleTask> builder)
        {
            builder.ToTable("ScheduleTask", "dbo");
            builder.HasKey(x => x.id).HasName("PK_ScheduleTask").IsClustered();

            builder.Property(x => x.id).HasColumnName(@"id").HasColumnType("bigint").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.TaskID).HasColumnName(@"TaskID").HasColumnType("int").IsRequired(true);
            builder.Property(x => x.TaskName).HasColumnName(@"TaskName").HasColumnType("nvarchar(100)").IsRequired(true);
            builder.Property(x => x.StartTime).HasColumnName(@"StartTime").HasColumnType("datetime2(7)").IsRequired(true);
            builder.Property(x => x.Enable).HasColumnName(@"Enable").HasColumnType("bit").IsRequired(true);
            builder.Property(x => x.CreateDate).HasColumnName(@"CreateDate").HasColumnType("datetime").IsRequired(true);
        }
    }
}
