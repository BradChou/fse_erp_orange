using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
	// JubFuAPPWebServiceLog
	/// <summary>
	/// SDMDCode
	/// </summary>
	public class JubFuAPPWebServiceLog
	{
		public long id { get; set; }
		public string sessionid { get; set; }
		public string type { get; set; }
		public string action { get; set; }
		public string message { get; set; }
		public DateTime cdate { get; set; }

	}

}


