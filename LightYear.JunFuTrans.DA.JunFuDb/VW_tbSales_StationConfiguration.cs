﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace LightYear.JunFuTrans.DA.JunFuDb
{
    public class VW_tbSales_StationConfiguration : IEntityTypeConfiguration<VW_tbSales_Station>
    {
        public void Configure(EntityTypeBuilder<VW_tbSales_Station> builder)
        {
            builder.ToView("VW_tbSales_Station", "dbo");
            builder.HasNoKey();

            builder.Property(x => x.station_scode).HasColumnName(@"station_scode").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.station_name).HasColumnName(@"station_name").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.DistChief).HasColumnName(@"DistChief").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.NodeManage).HasColumnName(@"NodeManage").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
            builder.Property(x => x.DistCS).HasColumnName(@"DistCS").HasColumnType("nvarchar(20)").IsRequired(false).HasMaxLength(20);
        }
    }
}
