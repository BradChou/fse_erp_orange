using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
	// pickuppOrderResponse
	/// <summary>
	/// 代收匯入檔
	/// </summary>
	public class pickuppOrderResponse
	{
		public string id { get; set; }
		public string order_number { get; set; }
		public string FSE_check_number { get; set; }
		public DateTime FSE_sendTime { get; set; }
		public DateTime CreateDate { get; set; }
		public bool sendflag { get; set; }
	}

}


