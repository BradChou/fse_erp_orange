// ------------------------------------------------------------------------------------------------
// This code was generated by EntityFramework Reverse POCO Generator (http://www.reversepoco.co.uk/).
// Created by Simon Hughes (https://about.me/simon.hughes).
//
// Registered to: Light-Year Technology
// Company      : Light-Year Technology
// Licence Type : Commercial
// Licences     : 1
// Valid until  : 21 五月 2025
//
// Do not make changes directly to this file - edit the template instead.
//
// The following connection settings were used to generate this file:
//     Connection String Name: "JunFuDbContext"
//     Connection String:      "Data Source=junfurdsdb.cioqpfywhsfk.us-east-2.rds.amazonaws.com,1433;Initial Catalog=JunFuReal;Persist Security Info=True;User ID=admin;password=**zapped**;;MultiSubnetFailover=Yes;"
// ------------------------------------------------------------------------------------------------
// Database Edition       : Standard Edition (64-bit)
// Database Engine Edition: Standard
// Database Version       : 13.0.5820.21

// <auto-generated>
// ReSharper disable CheckNamespace
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable NotAccessedVariable
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantCast
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// ReSharper disable UsePatternMatching
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // tbAssetsAccountShare
    /// <summary>
    /// 帳務比例表
    /// </summary>
    public class TbAssetsAccountShare
    {
        public long Id { get; set; } // id (Primary key)

        /// <summary>
        /// 資產序號(ttAssets 主鍵)
        /// </summary>
        public int? AId { get; set; } // a_id

        /// <summary>
        /// 車牌
        /// </summary>
        public string CarLicense { get; set; } // car_license (length: 10)

        /// <summary>
        /// 帳務部門
        /// </summary>
        public string AccountDept { get; set; } // account_dept (length: 2)

        /// <summary>
        /// 分攤比例
        /// </summary>
        public int? Share { get; set; } // share

        /// <summary>
        /// 修改時間
        /// </summary>
        public DateTime? Udate { get; set; } // udate

        /// <summary>
        /// 修改人員
        /// </summary>
        public string Uuser { get; set; } // uuser (length: 10)
    }

}
// </auto-generated>

