﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.JunFuDb
{
    public class VW_tbSales_Station
    {
        public string station_scode { get; set; }
        public string station_name { get; set; }
        public string DistChief { get; set; }
        public string NodeManage { get; set; }
        public string DistCS { get; set; }
    }
}
