using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;


namespace LightYear.JunFuTrans.DA.JunFuDb
{
    // JubFuAPPWebServiceLog
    public class JubFuAPPWebServiceLogConfiguration : IEntityTypeConfiguration<JubFuAPPWebServiceLog>
    {
        public void Configure(EntityTypeBuilder<JubFuAPPWebServiceLog> builder)
        {
            builder.ToTable("JubFuAPPWebServiceLog", "dbo");
            builder.HasKey(x => x.id).HasName("PK_JubFuAPPWebServiceLog").IsClustered();

            builder.Property(x => x.id).HasColumnName(@"id").HasColumnType("bigint").IsRequired().ValueGeneratedOnAdd().UseIdentityColumn();
            builder.Property(x => x.sessionid).HasColumnName(@"sessionid").HasColumnType("nvarchar(100)").IsRequired(false).HasMaxLength(100); 
            builder.Property(x => x.type).HasColumnName(@"type").HasColumnType("nvarchar(50)").IsRequired(false).HasMaxLength(50); 
            builder.Property(x => x.action).HasColumnName(@"action").HasColumnType("nvarchar(300)").IsRequired(false).HasMaxLength(300);
            builder.Property(x => x.message).HasColumnName(@"message").HasColumnType("nvarchar(max)").IsRequired(false);
            builder.Property(x => x.cdate).HasColumnName(@"cdate").HasColumnType("DateTime").IsRequired();
        }
    }

}
// </auto-generated>

