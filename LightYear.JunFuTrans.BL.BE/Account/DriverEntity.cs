﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class DriverEntity
    {
        public DriverEntity()
        {
            this.Id = 0;
            this.WgsX = 0;
            this.WgsY = 0;
        }

        public int Id { get; set; }

        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public string DriverMobile { get; set; }

        public string SupplierCode { get; set; }

        public string EmpCode { get; set; }

        public string SiteCode { get; set; }

        public string Area { get; set; }

        public string LoginPassword { get; set; }

        public DateTime? LoginDate { get; set; }

        public bool ActiveFlag { get; set; }

        public bool ExternalDriver { get; set; }

        public string PushId { get; set; }

        public double WgsX { get; set; }

        public double WgsY { get; set; }

        public string Station { get; set; }

        public DateTime? CDate { get; set; }

        public string CUser { get; set; }

        public DateTime? UDate { get; set; }

        public string UUser { get; set; }

        public string Office { get; set; }
    }
}
