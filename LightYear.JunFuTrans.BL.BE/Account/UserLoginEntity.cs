﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class UserLoginEntity
    {
        public string Account { get; set; }

        public string Password { get; set; }
    }
}
