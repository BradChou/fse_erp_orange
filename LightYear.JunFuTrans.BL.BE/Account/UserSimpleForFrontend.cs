﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Account
{
    public class UserSimpleForFrontend
    {
        public UserSimpleForFrontend()
        {
            this.Id = 0;
            this.AccountType = 1;
        }

        public int Id { get; set; }

        public int AccountType { get; set; }

        public string UserName { get; set; }

        public string TypeName { get; set; }
    }
}
