﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Menu
{
    public class SystemFunctionEntity
    {
        public SystemFunctionEntity()
        {
            this.Id = 0;
            this.SubCategoryId = 0;
            this.DisplaySort = 0;
            this.IsMenu = true;
            this.Url = string.Empty;
        }

        public int Id { get; set; }

        public int SubCategoryId { get; set; }

        public string FunctionName { get; set; }

        public string FunctionEnName { get; set; }

        public string Url { get; set; }

        public bool IsMenu { get; set; }

        public int DisplaySort { get; set; }
    }
}
