﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Account;

namespace LightYear.JunFuTrans.BL.BE.Menu
{
    public class FrontendMenuEntity
    {

        public IEnumerable<SystemCategoryEntity> Categories { get; set; }

        public UserInfoEntity UserInfoEntity { get; set; }
    }
}
