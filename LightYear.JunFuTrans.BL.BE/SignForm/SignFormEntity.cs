﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.SignForm
{
    public class SignFormInputEntity
    {
        public DateTime DeliveryDateTimeStart { get; set; }
        public DateTime DeliveryDateTimeEnd { get; set; }
        public string AreaArriveStation { get; set; }
        public string CustomerCode { get; set; }
        public int SignPaperExistType { get; set; }   //是否有簽單 0:無，1:有，2:全部
    }
    public class SignFormEntity
    {
        public decimal RequestId { get; set; }
        public string CheckNumber { get; set; }
        public string ArriveOption { get; set; }
        public string SignPaperPath { get; set; }
        public string SignPaperString { get; set; }
        public string DeliverDriver { get; set; }
        public string ArriveDate { get; set; }
        public string AreaArriveStation { get; set; }
        public bool IsSignPaperExist { get; set; }
        public bool IsReturn { get; set; }
    }
}
