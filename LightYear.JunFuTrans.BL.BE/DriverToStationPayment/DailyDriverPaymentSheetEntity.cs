﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class DailyDriverPaymentSheetEntity
    {
        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public string StationName { get; set; }

        public string StationScode { get; set; }

        public DateTime CreateDate { get; set; }

        public DriverPaymentDetailEntity[] Details { get; set; }

        public CategoryCountEntity CategoryCount { get; set; }

        public PaidHistoryEntity[] PaidHistories { get; set; }

        public int TotalDriverShouldPaid { get; set; }
    }
}
