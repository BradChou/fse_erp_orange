﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class ViewCreatedSheetEntity
    {
        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public string StationScode { get; set; }

        public string StationName { get; set; }

        /// <summary>
        /// 今日已製作精算書
        /// </summary>
        public DriverPaymentEntity[] TodaysPaymentSheet { get; set; }

        /// <summary>
        /// 今日應繳金額
        /// </summary>
        public int TodaysShouldCollectMoney { get; set; }

        public int YuanFuCount { get; set; }

        public int YuanFuAmount { get; set; }

        public int CashOnDeliveryCount { get; set; }

        public int CashOnDeliveryAmount { get; set; }

        public int DestinationCount { get; set; }

        public int DestinationAmount { get; set; }

        public CategoryCountEntity CategoryCount { get; set; }

        /// <summary>
        /// 今日已製作精算書金額
        /// </summary>
        public int TodaysPaidMoney { get; set; }

        /// <summary>
        /// 應繳-已繳差額
        /// </summary>
        public int Difference { get; set; }
    }
}
