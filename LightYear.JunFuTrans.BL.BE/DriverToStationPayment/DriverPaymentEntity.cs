﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DriverToStationPayment
{
    public class DriverPaymentEntity
    {
        public DriverPaymentEntity()
        {
            Id = 0;
            RemittanceAmount = 0;
            CheckAmount = 0;
            CoinsAmount = 0;
        }

        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        [Display(Name = "工號")]
        public string DriverCode { get; set; }

        public string DriverName { get; set; }

        public string StationName { get; set; }

        [Display(Name = "站所代碼")]
        public string StationScode { get; set; }

        public DriverPaymentDetailEntity[] Details { get; set; }

        [Display(Name = "匯款明細末五碼")]
        [StringLength(5, MinimumLength = 5, ErrorMessage = "請輸入5碼")]
        public string TranscationLastFive { get; set; }

        [Display(Name = "匯款金額")]
        [DivideHundred]
        public int? RemittanceAmount { get; set; }

        [Display(Name = "支票金額")]
        public int? CheckAmount { get; set; }

        [Display(Name = "零錢金額")]
        public int? CoinsAmount { get; set; }

        public int PaidTotal { get; set; }
    }

    public class DivideHundred : ValidationAttribute
    {
        public DivideHundred() { }

        public string GetErrorMessage() =>
            "只接受百元整數";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            var entity = (DriverPaymentEntity)validationContext.ObjectInstance;

            if (entity.RemittanceAmount == null || entity.RemittanceAmount % 100 == 0)
                return ValidationResult.Success;

            return new ValidationResult(GetErrorMessage());
        }
    }
}
