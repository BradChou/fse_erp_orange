﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TMS
{
    public class TmsPalletCountViewEntity
    {
        public List<TmsPositionStationEntity> Stations { get; set; }

        public List<TMSPlateCountEntity> CountEntity { get; set; }
    }

    public class TmsPositionStationEntity
    {
        public string Position { get; set; }

        public List<string> Stations { get; set; }
    }

    public class TMSPlateCountEntity
    {
        public string AreaArrive { get; set; }

        public List<PalletCountEntity> RowData { get; set; }
    }

    public class PalletCountEntity
    {
        public string SendStation { get; set; }

        public int Count { get; set; }
    }
}
