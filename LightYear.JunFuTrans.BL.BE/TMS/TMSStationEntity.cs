﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TMS
{
    public class SupplierEntity
    {
        [JsonProperty("id")]
        public decimal Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }
    }

    public class StationEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("city_id")]
        public decimal CityId { get; set; }

        [JsonProperty("district_id")]
        public decimal DistrictId { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("position")]
        public string Position { get; set; }

        [JsonProperty("is_active")]
        public bool IsActive { get; set; }

        [JsonProperty("suppliers")]
        public List<SupplierEntity> Suppliers { get; set; }
    }
}
