﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TMS
{
    public class TMSScheduleEntity
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("run_number")]
        public int RunNumber { get; set; }

        [JsonProperty("route")]
        public string Route { get; set; }

        [JsonProperty("from")]
        public string From { get; set; }

        [JsonProperty("to")]
        public string To { get; set; }

        [JsonProperty("relay")]
        public string Relay { get; set; }

        [JsonProperty("car_type")]
        public string CarType { get; set; }

        [JsonProperty("max_loading")]
        public string MaxLoading { get; set; }

        [JsonProperty("run_type")]
        public string RunType { get; set; }

        [JsonProperty("dispatch_days")]
        public string DispatchDays { get; set; }

        [JsonProperty("go_time")]
        public string GoTime { get; set; }

        [JsonProperty("back_time")]
        public string BackTime { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("attribute")]
        public string Attribute { get; set; }

        [JsonProperty("relay_list")]
        public List<TMSScheduleRelayEntity> RelayList { get; set; }
    }

}
