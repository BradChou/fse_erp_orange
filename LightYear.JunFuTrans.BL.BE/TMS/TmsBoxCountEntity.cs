﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TMS
{
    public class TmsBoxCountEntity
    {
        public int Id { get; set; }

        public DateTime ShipDate { get; set; }

        public int BSectionStopId { get; set; }

        public string BSectionStop { get; set; }

        public int EstimateMixBoxCount { get; set; }

        public int? ActualMixBoxCount { get; set; }

        public int EstimateFlowBoxCount { get; set; }

        public int? ActualFlowBoxCount { get; set; }
    }
}
