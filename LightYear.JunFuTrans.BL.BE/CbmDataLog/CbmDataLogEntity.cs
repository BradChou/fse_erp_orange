﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.BE.CbmDataLog
{
    public class CbmDataLogInputEntity
    {
        public DateTime CbmScanStartDate { get; set; }
        public DateTime CbmScanEndDate { get; set; }
    }
    public class GetDataForCheckNumberEntity
    {
        public string CheckNumber  { get; set; }
     
    }

    public class CbmUpdateDataEntity
    { 
        public List<DA.JunFuDb.CbmDataLog> Models { get; set; }
    }
    public class CbmDataEntity
    {
        public int Id { get; set; } // id (Primary key)
        public string FileName { get; set; } // file_name (length: 100)
        public int? FileRow { get; set; } // file_row
        public DateTime? Cdate { get; set; } // cdate
        public string CheckNumber { get; set; } // check_number (length: 20)
        public double? Length { get; set; } // length
        public double? Width { get; set; } // width
        public double? Height { get; set; } // height
        public double? Sum { get; set; } // CbmSum
        public string Size { get; set; } // Size
        /// <summary>

        /// 材積
        /// </summary>
        public double? Cbm { get; set; } // cbm
        public string S3PicUri { get; set; } // s3_pic_uri (length: 400)

        public string S3PicUri2 { get; set; } // s3_pic_uri2 (length: 400)

        /// <summary>
        /// 丈量時間
        /// </summary>
        public DateTime? ScanTime { get; set; } // scan_time
        public string DataSource { get; set; } // data_source (length: 30)

        /// <summary>
        /// 掃描結果
        /// </summary>
        public string ScanResult { get; set; } // scan_result (length: 30)

        /// <summary>
        /// 是否為歷史檔案(true:代表已有更新狀態)
        /// </summary>
        public bool? IsLog { get; set; } // is_log
        public string Uuser { get; set; } // uuser (length: 100)
        public DateTime? Udate { get; set; } // udate

    }
}
