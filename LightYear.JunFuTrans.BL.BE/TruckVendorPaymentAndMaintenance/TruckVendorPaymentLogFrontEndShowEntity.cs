﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentLogFrontEndShowEntity
    {
        public int Id { get; set; }

        public DateTime UploadDate { get; set; }

        public string DutyDept { get; set; }

        public DateTime RegisterDate { get; set; }

        public string FeeType { get; set; }

        public string VendorName { get; set; }

        public string CarNumber { get; set; }

        public int Payment { get; set; }

        public string Memo { get; set; }

        public string CompanyType { get; set; }
    }
}
