﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance
{
    public class DropDownListEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class DisbursementEntity
    {
        public string FeeType { get; set; }
        public int Payment { get; set; }
    }

    public class PaymentEntity
    {
        public string Income { get; set; }
        public string Expenditure { get; set; }
        public string Remainder { get; set; }
    }

    public class JFDepartmentDropDownListEntity
    {
        public JFDepartmentDropDownListEntity(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public JFDepartmentDropDownListEntity()
        {

        }

        public string Id { get; set; }
        public string Name { get; set; }
    }

}
