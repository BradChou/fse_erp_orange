﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Barcode
{
    public class DriverEnteredScanDataEntity
    {
        public string CheckNumber { get; set; }

        public string Plates { get; set; }

        public string AreaArriveCode { get; set; }

        public string Pieces { get; set; }

        public string Weight { get; set; }

        public string Platform { get; set; }

        public string Runs { get; set; }

        public string CarNumber { get; set; }

        public string SowageRate { get; set; }

        public string Area { get; set; }

        public string ExceptionOption { get; set; }

        public string ArriveOption { get; set; }
    }
}
