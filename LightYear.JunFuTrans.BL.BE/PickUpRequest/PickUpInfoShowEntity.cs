﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.PickUpRequest
{
    public class PickUpInfoShowEntity
    {
        public PickUpInfoShowEntity()
        {
            this.Id = 0;
            this.No = 0;
            ShipType = "";
            VehicleType = "";
            Remark = "";
            PickUpAvailTime = "";
            this.ShouldPickUpPieces = 0;
            this.ActualPickUpPieces = 0;
        }

        public Decimal Id { get; set; }

        public int No { get; set; }

        public int PickupRequestId { get; set; }

        public int RequestId { get; set; }

        public DateTime NotifyDate { get; set; }

        public string CustomerName { get; set; }

        public string CustomerCode { get; set; }

        public string AssignedMd { get; set; }

        public string AssignedSd { get; set; }

        public string ShipType { get; set; }

        public string VehicleType { get; set; }

        public int ShouldPickUpPieces { get; set; }

        public int ActualPickUpPieces { get; set; }

        public string PickUpAvailTime { get; set; }

        public string Remark { get; set; }

        public string PickUpState { get; set; }

        public string PickUpDriver { get; set; }

        public DateTime? LatestUpdateDate { get; set; }

        public string SendContact { get; set; }

        public string SendPhone { get; set; }

        public string SendAddress { get; set; }

        public string CheckNumber { get; set; }

        public string PickUpStation { get; set; }

        public string ArriveStation { get; set; }

        public string ReceiveAddress { get; set; }

        public string Ruser { get; set; }

        public string StatusInCode { get; set; }

        public string StatusInName { get; set; }

        public bool IsAllowEdit { get; set; }
        public string CBM { get; set; }

        public PickUpOptionsEntity OptionSelector { get; set; }

        public string PutOrder { get; set; }

        public string ReassignMd { get; set; }

        public string MdUpdateUser { get; set; }

        public string LatestStatus { get; set; }

        public int Size { get; set; }

        public string CBMSize { get; set; }


    }

    public class PickUpOptionsEntity
    {
        public string Name { get; set; }
        public string OptionId { get; set; }
    }
}
