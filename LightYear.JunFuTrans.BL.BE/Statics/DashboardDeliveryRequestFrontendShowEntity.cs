﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.Statics
{
    public class DashboardDeliveryRequestFrontendShowEntity
    {
        public Decimal Id { get; set; }

        public string Station { get; set; }

        public string DeliveryDriver { get; set; }

        public string CheckNumber { get; set; }

        public string ReceiverName { get; set; }

        public string ReceiverTel1 { get; set; }

        public string ReceiverAddress { get; set; }

        public string LatestScanStatus { get; set; }

        public string LatestScanArriveOption { get; set; }

        public string LatestScanDate { get; set; }


    }
}
