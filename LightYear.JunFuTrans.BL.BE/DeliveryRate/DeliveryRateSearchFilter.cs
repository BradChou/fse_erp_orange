﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRate
{
    public class DeliveryRateSearchFilter
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string Area { get; set; }
        public string Station { get; set; }
        public string Customer { get; set; }
        public string DriverCode { get; set; }
        public bool ShowPercentage { get; set; }
    }
}
