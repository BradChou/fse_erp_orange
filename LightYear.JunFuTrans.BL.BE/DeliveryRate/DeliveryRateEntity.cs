﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRate
{
    public class DeliveryRateEntity
    {
        public DeliveryRateEntity()
        {
            OrdersNumber = 0;
            MatingNumBeforeEighteen = 0;
            MatingNumBeforeFifteen = 0;
            MatingNumBeforeTwenty = 0;
            MatingNumBeforeNoon = 0;
            MatingNumber = 0;
            NotDeliverNum = 0;
            NotWriteOffNumber = 0;
            DeliveryNumber = 0;
        }

        public string StationScode { get; set; }

        public string StationName { get; set; }

        public string District { get; set; }

        public float OrdersNumber { get; set; }

        public float MatingNumBeforeNoon { get; set; }

        public float MatingNumBeforeFifteen { get; set; }

        public float MatingNumBeforeEighteen { get; set; }
        
        public float MatingNumBeforeTwenty { get; set; }
        
        public float MatingNumber { get; set; }

        public float NotWriteOffNumber { get; set; }

        public float DeliveryNumber { get; set; }

        public float NotDeliverNum { get; set; }        
    }
}
