﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.TodayPickUp
{
    public class TodayPickUpEntity
    {
        public int? Index { get; set; }
        public string CheckNumber { get; set; }
        public int? Pieces { get; set; }
        public string ScanItem { get; set; }
        public string PickUpStatus { get; set; }        //集貨狀態
        public DateTime? ScanDate { get; set; }         //掃讀時間
        public string FrontendShipDate { get; set; }
        public string ReceiveStatus { get; set; }
    }

    public class TodayPickUpInputEntity
    { 
        public string DriverCode { get; set; }          //工號
        public string Name { get; set; }
        public string StationScode { get; set; }
        public string StationName { get; set; }
    }

    public class TodayPickUpCount
    {
        public string SuccessCount { get; set; }          
        public string FailCount { get; set; }
       
    }
}
