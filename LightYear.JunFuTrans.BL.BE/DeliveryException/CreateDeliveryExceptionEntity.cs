﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryException
{
    public class CreateDeliveryExceptionEntity
    {
        public int Id { get; set; }
        public string CheckNumber { get; set; }
        public string SendContact { get; set; }
        public string Pieces { get; set; }
        public string ReceiveContact { get; set; }
        public string AreaArriveCode { get; set; }
        public string SupplierCode { get; set; }
    }
}
