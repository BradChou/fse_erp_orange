﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class AreaArriveCodeForLabelEntity
    {
        public string CheckNumber { get; set; }

        public string AreaArriveCode { get; set; }
    }
}
