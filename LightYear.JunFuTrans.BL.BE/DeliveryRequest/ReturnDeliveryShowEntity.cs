﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class ReturnDeliveryShowEntity
    {   
        public ReturnDeliveryShowEntity()
        {
            this.Id = 0;
            this.No = 0;
            this.Pieces = 0;
        }
        public Decimal Id { get; set; }

        public int No { get; set; }

        public string PrintDate { get; set; }

        public string ShipDate { get; set; }

        public string SendStationName { get; set; }

        public string ArriveStationName { get; set; }

        public string CheckNumber { get; set; }

        public string ReturnerName { get; set; }

        public string ReturnerPhone { get; set; }

        public string ReturnerAddress { get; set; }

        public int Pieces { get; set; }

        public string ReturnItem { get; set; }

        public string DeliveryState { get; set; }

        public string PickUpState { get; set; }

        public string LatestScanItem { get; set; }

        public string LatestArriveOption { get; set; }

        public DateTime? LatestScanDate { get; set; }
    }
}

