﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class AccountsReceivableValueEntity
    {
        public int Count { get; set; }

        public decimal Amount { get; set; }
    }
}
