﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class CustomerEntity
    {
        public CustomerEntity()
        {
            this.Id = 0;
        }

        public int Id { get; set; }

        public string SupplierCode { get; set; }

        public string SupplierId { get; set; }

        public string MasterCode { get; set; }

        public string SecondId { get; set; }

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public string CustomerShortName { get; set; }

        public string CustomerType { get; set; }

        public string UniformNumber { get; set; }

        public string ShipmentsPrincipal { get; set; }

        public string Tel { get; set; }

        public string ShipmentsCity { get; set; }

        public string ShipmentsArea { get; set; }

        public string ShipmentAddress { get; set; }

        public string ShipmentEmail { get; set; }

        public string StopShippingCode { get; set; }

        public string StopShippingMemo { get; set; }

        public string ContractContent { get; set; }

        public string ShowName { get; set; }
    }
}
