﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class DeliveryScanLogEntity
    {
        public DeliveryScanLogEntity()
        {
            this.Id = 0;
            this.RealScanStation = string.Empty;
        }

        public Int64 Id { get; set; }

        public string DriverCode { get; set; }

        public DateTime ScanDate { get; set; }

        public int ScanStatus { get; set; }

        public DateTime CreateDate { get; set; }

        public string SignFormImage { get; set; }

        public string SignFieldImage { get; set; }

        public string RealScanStation { get; set; }
    }

}
