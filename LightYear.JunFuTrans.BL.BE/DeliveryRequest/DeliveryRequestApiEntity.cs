﻿using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class DeliveryRequestApiEntity
    {
        public DeliveryRequestApiEntity()
        {
            this.RequestId = 0;
            this.CheckNumber = string.Empty;
            this.CustomerCode = string.Empty;
            this.OrderNumber = string.Empty;
            this.ReceiveCustomerCode = string.Empty;
            this.ReceiveContact = string.Empty;
            this.ReceiveTel1 = string.Empty;
            this.ReceiveTel2 = string.Empty;
            this.ReceiverAddress = string.Empty;
            this.Pieces = 1;
            this.Plates = 1;
            this.SubpoenaCategory = "11";
            this.CollectionMoney = 0;
            this.SendContact = string.Empty;
            this.SendTel = string.Empty;
            this.SenderAddress = string.Empty;
            this.SupplierCode = string.Empty;
            this.PrintDate = null;
            this.CbmSize = "1";
            this.ArriveAssignDate = null;
            this.TimePeriod = "不指定";
            this.InvoiceDesc = string.Empty;
            this.ReceiptFlag = "N";
            this.RoundTrip = "N";
            this.Reverse = "N";
            this.ArticleNumber = string.Empty;
            this.SendPlatform = string.Empty;
            this.ArticleName = string.Empty;
            this.ReturnCheckNumber = string.Empty;
            this.SendStationScode = string.Empty;
        }

        public long RequestId { get; set; }
        
        public string CheckNumber { get; set; }

        public string CustomerCode { get; set; }

        public string OrderNumber { get; set; }

        public string ReceiveCustomerCode { get; set; }

        public string ReceiveContact { get; set; }

        public string ReceiveTel1 { get; set; }

        public string ReceiveTel2 { get; set; }

        public string ReceiverAddress { get; set; }

        public int Plates { get; set; }

        public int Pieces { get; set; }

        public string SubpoenaCategory { get; set; }

        public int CollectionMoney { get; set; }

        public string SendContact { get; set; }

        public string SendTel { get; set; }

        public string SenderAddress { get; set; }

        public string SupplierCode { get; set; }

        public DateTime? PrintDate { get; set; }

        public string CbmSize { get; set; }

        public DateTime? ArriveAssignDate { get; set; }

        public string TimePeriod { get; set; }

        public string InvoiceDesc { get; set; }

        public string ReceiptFlag { get; set; }

        public string RoundTrip { get; set; }

        public string Reverse { get; set; }

        public string ArticleNumber { get; set; }

        public string SendPlatform { get; set; }

        public string ArticleName { get; set; }

        public float Weight { get; set; }

        public string ReturnCheckNumber { get; set; }
        public string SendStationScode { get; set; }

    }
}
