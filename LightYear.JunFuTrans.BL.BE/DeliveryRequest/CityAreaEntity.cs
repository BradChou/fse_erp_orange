﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class CityAreaEntity
    {
        public CityAreaEntity()
        {
            this.City = string.Empty;
            this.Area = string.Empty;
            this.Zip = string.Empty;
        }

        public string City { get; set; }

        public string Area { get; set; }

        public string Zip { get; set; }

    }
}
