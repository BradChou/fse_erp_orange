﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class AccountsReceivableFrontendShowEntity
    {
        public int SerialNo { get; set; }

        public string ReceiverNumber { get; set; }

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }

        public int TotalCount { get; set; }

        public decimal AccountReceivable { get; set; }

        public bool AlreadyPay { get; set; }

        public DateTime? PayDate { get; set; }

        public string InvoiceNumber { get; set; }

    }
}
