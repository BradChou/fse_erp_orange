﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.StationArea;

namespace LightYear.JunFuTrans.BL.BE.DeliveryRequest
{
    public class DeliveryRequestMenuVerifyEntity
    {
        public DeliveryRequestMenuVerifyEntity()
        {
            this.SerialNo = 0;
            this.VerifyStation = new StationAreaEntity
            {
                ShowName = "",
                StationSCode = "",
                StationName = ""
            };

            this.VerifyOption = new WriteOffCheckListEntity
            { 
                Id = 0, 
                Information = "", 
                Type = "" 
            };
        }

        public int SerialNo { get; set; }

        public string Station { get; set; }

        public string DeliveryArea { get; set; }

        public string EmpCode { get; set; }

        public string CheckNumber { get; set; }

        public DateTime? ScanDate { get; set; }

        public string ArriveOption { get; set; }

        public WriteOffCheckListEntity VerifyOption { get; set; }

        public string VerifyEmpCode { get; set; }

        public StationAreaEntity VerifyStation { get; set; }
    }
}
