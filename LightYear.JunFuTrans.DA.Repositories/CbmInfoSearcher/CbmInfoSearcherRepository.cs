﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.CbmInfo;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.CbmInfoSearcher
{
    public class CbmInfoSearcherRepository : ICbmInfoSearcherRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public CbmInfoSearcherRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }


        public TcDeliveryRequest[] GetCbmInfoByCheckNumber(string checkNumber)
        {
            var data =
                this.JunFuDbContext.TcDeliveryRequests.Where(x => x.CheckNumber == checkNumber);
            return Filter(data.ToArray());
        }

        public TcDeliveryRequest[] GetCbmInfoByDateAndCustomerCode(DateTime? start, DateTime? end, string customerCode)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start && x.CustomerCode == customerCode);
            return Filter(data.ToArray());
        }
        public TcDeliveryRequest[] GetCbmInfoByDateAndStation(DateTime? start, DateTime? end, string stationCode, string station_level, string management, string station_area, string station_scode)
        {
            if (stationCode.Equals("-1"))
            {
                string managementList = "";
                string station_scodeList = "";
                string station_areaList = "";

                if (station_level.Equals("1"))
                {
                    var da = (from tbstation in JunFuDbContext.TbStations
                              where tbstation.management == management
                              select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                    if (da.Count() > 0)
                    {
                        foreach (var item in da)
                        {
                            managementList += "'";
                            managementList += item.StationScode;
                            managementList += "'";
                            managementList += ",";
                        }
                        managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                    }
                    var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start && managementList.Contains(x.SendStationScode));
                    return Filter(data.ToArray());

                }
                else if (station_level.Equals("2"))
                {
                    var da = (from tbstation in JunFuDbContext.TbStations
                              where tbstation.StationScode == station_scode
                              select new TbStation { StationScode = tbstation.StationScode });

                    if (da.Count() > 0)
                    {
                        foreach (var item in da)
                        {
                            station_scodeList += "'";
                            station_scodeList += item.StationScode;
                            station_scodeList += "'";
                            station_scodeList += ",";
                        }
                        station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                    }
                    var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start && station_scodeList.Contains(x.SendStationScode));
                    return Filter(data.ToArray());
                }
                else if (station_level.Equals("4"))
                {
                    var da = (from tbstation in JunFuDbContext.TbStations
                              where tbstation.station_area == Int32.Parse(station_area)
                              select new TbStation { station_area = tbstation.station_area, StationScode = tbstation.StationScode });

                    if (da.Count() > 0)
                    {
                        foreach (var item in da)
                        {
                            station_areaList += "'";
                            station_areaList += item.StationScode;
                            station_areaList += "'";
                            station_areaList += ",";
                        }
                        station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                    }
                    var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start && station_areaList.Contains(x.SendStationScode));
                    return Filter(data.ToArray());
                }
                else {

                    var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start);
                    return Filter(data.ToArray());
                }
            }
            else
            {
                
                var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start && stationCode.Contains(x.SendStationScode));
                
                return Filter(data.ToArray());
            }
            return null;
        }
        public TcDeliveryRequest[] GetCbmInfoByCustomerCode(string customerCode)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.CustomerCode == customerCode);
            return Filter(data.ToArray());
        }
        public TcDeliveryRequest[] GetCbmInfoByStation(string stationCode)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.SupplierCode == stationCode);
            return Filter(data.ToArray());
        }
        public TcDeliveryRequest[] GetCbmInfoByDate(DateTime? start, DateTime? end)
        {
            var data = this.JunFuDbContext.TcDeliveryRequests.Where(x => x.PrintDate < end && x.PrintDate > start);
            return Filter(data.ToArray());
        }

        private TcDeliveryRequest[] Filter(TcDeliveryRequest[] input)
        {
            return input.Where(x => !x.CheckNumber.StartsWith("600")
                        && x.LatestScanItem != null && !x.LatestScanItem.Equals("5") && !x.LatestScanItem.Equals("6")
                        && x.LessThanTruckload == true).ToArray();
        }
    }
}
