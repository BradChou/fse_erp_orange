﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.CbmInfo;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.CbmInfoSearcher
{
    public interface ICbmInfoSearcherRepository
    {
        //本頁站所皆為發送站所
        //此表有四個篩選器，優先順位:貨號->日期+客代->日期+站所->客代->站所->日期->錯誤
        public TcDeliveryRequest[] GetCbmInfoByCheckNumber(string checkNumber);
        public TcDeliveryRequest[] GetCbmInfoByDateAndCustomerCode(DateTime? start, DateTime? end, string customerCode);
        public TcDeliveryRequest[] GetCbmInfoByDateAndStation(DateTime? start, DateTime? end, string stationCode, string station_level, string management, string station_area, string station_scode);
        public TcDeliveryRequest[] GetCbmInfoByCustomerCode(string customerCode);
        public TcDeliveryRequest[] GetCbmInfoByStation(string stationCode);
        public TcDeliveryRequest[] GetCbmInfoByDate(DateTime? start, DateTime? end);
    }
}
