﻿using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRate
{
    public class DeliveryRateRepository : IDeliveryRateRepository
    {
        public JunFuDbContext JunFuDbContext { get; set; }

        public DeliveryRateRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }

        #region 很慢的寫法，想要改掉

        public int GetOrderNumBetweenDate(DateTime start, DateTime end, string stationCode, string customerCode)
        {
            int count = 0;
            DateTime s = start;
            while (s <= end)
            {
                count += GetCheckNumFiltered(s, stationCode, customerCode).Count();
                s = s.AddDays(1);
            }

            return count;
        }

        public int GetMatingNumBeforeTime(DateTime start, DateTime end, byte timeInDay, string stationCode, string customerCode)
        {
            int count = 0;
            DateTime startTime = start;
            timeInDay = (byte)(timeInDay + 24);
            while (startTime <= end)
            {
                var orderList = GetCheckNumFiltered(startTime, stationCode, customerCode).ToList();
                count += JunFuDbContext.TtDeliveryScanLogs.Where(s => orderList.Contains(s.CheckNumber))
                    .Where(s => s.ScanItem.Equals("3")).Where(s => s.ArriveOption.Equals("3"))
                    .Where(s => s.ScanDate < startTime.AddHours(timeInDay)).Count();
                startTime = startTime.AddDays(1);
            }

            return count;
        }

        public int GetNotWriteOffNum(DateTime start, DateTime end, string stationCode, string customerCode)
        {
            int count = 0;
            DateTime startTime = start;
            while (startTime <= end)
            {
                var orderList = GetCheckNumFiltered(startTime, stationCode, customerCode).ToList();
                count += JunFuDbContext.TtDeliveryScanLogs.Where(s => orderList.Contains(s.CheckNumber))
                    .Where(s => s.ScanItem.Equals("3")).Where(s => !s.ArriveOption.Equals("3")).Count();
                startTime = startTime.AddDays(1);
            }

            return count;
        }

        public int GetScanNumber(DateTime start, DateTime end, string stationCode, string customerCode)
        {
            int count = 0;
            DateTime startTime = start;
            while (startTime <= end)
            {
                var orderList = GetCheckNumFiltered(startTime, stationCode, customerCode).ToList();
                count += JunFuDbContext.TtDeliveryScanLogs.Where(s => orderList.Contains(s.CheckNumber))
                    .Where(s => s.ScanItem.Equals("2")).Select(s => s.CheckNumber).Distinct().Count();
                startTime = startTime.AddDays(1);
            }

            return count;
        }

        public int GetNotDeliveryNum(DateTime start, DateTime end, string stationCode, string customerCode)
        {
            int count = 0;
            DateTime startTime = start;
            while (startTime <= end)
            {
                var orderList = GetCheckNumFiltered(startTime, stationCode, customerCode).ToList();
                count += JunFuDbContext.TtDeliveryScanLogs.Where(s => orderList.Contains(s.CheckNumber))
                    .Where(s => s.AreaArriveCode == null).Count();
                startTime = startTime.AddDays(1);
            }

            return count;
        }

        private IQueryable<string> GetCheckNumFiltered(DateTime date, string stationCode, string customerCode)
        {
            var result = JunFuDbContext.TcDeliveryRequests.Where(r => r.ShipDate > date)
                .Where(r => r.ShipDate < date.AddDays(1))
                .Where(r => r.CheckNumber.Length == 12)
                .Where(r => !r.CheckNumber.StartsWith("102"))
                .Where(r => !r.CheckNumber.StartsWith("99"));

            string stationScode = stationCode.Replace("F", "");

            if (stationCode != "All")
            {
                result = result.Where(r => r.AreaArriveCode.Equals(stationScode));
            }

            if (customerCode != "All")
            {
                result = result.Where(r => r.CustomerCode.Equals(customerCode));
            }

            return result.Select(r => r.CheckNumber);
        }

        private IQueryable<string> GetCheckNumFiltered(DateTime date)
        {
            var result = JunFuDbContext.TcDeliveryRequests.Where(r => r.ShipDate > date.AddHours(5))
                .Where(r => r.ShipDate < date.AddHours(29))
                .Where(r => r.CheckNumber.Length == 12)
                .Where(r => !r.CheckNumber.StartsWith("102"))
                .Where(r => !r.CheckNumber.StartsWith("99"));

            return result.Select(r => r.CheckNumber);
        }

        public int GetOrderNumByDriver(DateTime start, DateTime end, string driverCode)
        {
            int count = 0;
            DateTime s = start;
            while (s <= end)
            {
                var checkNumbers = GetCheckNumFiltered(s);
                count += JunFuDbContext.TtDeliveryScanLogs
                    .Where(s => checkNumbers.Contains(s.CheckNumber))
                    .Where(s=>s.DriverCode.Equals(driverCode)).Count();
                s = s.AddDays(1);
            }

            return count;
        }

        public int GetMatingNumByDriver(DateTime start, DateTime end, byte timeInDay, string driverCode)
        {
            int count = 0;
            DateTime startTime = start;
            timeInDay = (byte)(timeInDay + 24);
            while (startTime <= end)
            {
                var orderList = GetCheckNumFiltered(startTime);
                count += JunFuDbContext.TtDeliveryScanLogs
                    .Where(s => orderList.Contains(s.CheckNumber))
                    .Where(s => s.DriverCode.Equals(driverCode))
                    .Where(s => s.ScanItem.Equals("3")).Where(s => s.ArriveOption.Equals("3"))
                    .Where(s => s.ScanDate < startTime.AddHours(timeInDay)).Distinct().Count();
                startTime = startTime.AddDays(1);
            }

            return count;
        }

        public int GetNotWriteOffNumByDriver(DateTime start, DateTime end, string driverCode)
        {
            int count = 0;
            DateTime startTime = start;
            while (startTime <= end)
            {
                var orderList = GetCheckNumFiltered(startTime);
                count += JunFuDbContext.TtDeliveryScanLogs
                    .Where(s => orderList.Contains(s.CheckNumber))
                    .Where(s => s.DriverCode.Equals(driverCode))
                    .Where(s => s.ScanItem.Equals("3")).Where(s => !s.ArriveOption.Equals("3")).Distinct().Count();
                startTime = startTime.AddDays(1);
            }

            return count;
        }

        public int GetScanNumberByDriver(DateTime start, DateTime end, string driverCode)
        {
            int count = 0;
            DateTime startTime = start;
            while (startTime <= end)
            {
                var orderList = GetCheckNumFiltered(startTime);
                count += JunFuDbContext.TtDeliveryScanLogs.Where(s => orderList.Contains(s.CheckNumber))
                    .Where(s => s.DriverCode.Equals(driverCode))
                    .Where(s => s.ScanItem.Equals("2")).Select(s => s.CheckNumber).Distinct().Count();
                startTime = startTime.AddDays(1);
            }

            return count;
        }

        public int GetNotDeliveryNumByDriver(DateTime start, DateTime end, string driverCode)
        {
            int count = 0;
            DateTime startTime = start;
            while (startTime <= end)
            {
                var orderList = GetCheckNumFiltered(startTime);
                count += JunFuDbContext.TtDeliveryScanLogs.Where(s => orderList.Contains(s.CheckNumber))
                    .Where(s => s.DriverCode.Equals(driverCode))
                    .Where(s => s.AreaArriveCode == null).Distinct().Count();
                startTime = startTime.AddDays(1);
            }

            return count;
        }

        #endregion
    }
}
