﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public interface IDeliveryRequestRepository
    {
        Dictionary<string, TbStation> GetAreaArriveCodeStation(List<string> checkNumber);

        /// <summary>
        /// 從託運單號陣列取得零擔DelievetRequest
        /// </summary>
        /// <param name="checkNumber">託運單號陣列</param>
        /// <returns></returns>
        List<TcDeliveryRequest> GetNotLessThanTruckloadByCheckNumberList(List<string> checkNumbers);
        Dictionary<string, TcDeliveryRequest> GetDictionaryNotLessThanTruckloadByCheckNumberList(List<string> checkNumbers);

        double GetCollectionMoney(string checkNumber);

        Dictionary<string, int> GetCollectionMoneyBatch(IEnumerable<string> checkNumbers);

        TcDeliveryRequest GetNotLessThanTruckloadByCheckNumber(string checkNumber);

        TcDeliveryRequest GetByCheckNumber(string checkNumber);

        IEnumerable<TcDeliveryRequest> GetByCheckNumberList(IEnumerable<string> checkNumbers);

        /// <summary>
        /// 查詢時間內收到的數量
        /// </summary>
        /// <param name="startDate">首刷件啟時間</param>
        /// <param name="endDate">首刷件迄止時間</param>
        /// <returns></returns>
        int GetAllCount(DateTime startDate, DateTime endDate);

        /// <summary>
        /// 查詢客戶在時間內寄送的數量
        /// </summary>
        /// <param name="startDate">首刷件啟時間</param>
        /// <param name="endDate">首刷件迄止時間</param>
        /// <param name="customerCode">客戶代碼</param>
        /// <returns></returns>
        int GetAllCountWithCustomer(DateTime startDate, DateTime endDate, string customerCode);

        /// <summary>
        /// 取得配送日後計算該日前一日出貨該配送站所之 D+1 配達率應配
        /// </summary>
        /// <param name="station"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        Dictionary<TcDeliveryRequest, string> GetDPlusOneDeliveryDictionary(string station, DateTime deliveryDate);

        /// <summary>
        /// 取得配送日後計算該日前一日出貨該配送站所之 D+1 配達率應配
        /// </summary>
        /// <param name="stationList"></param>
        /// <param name="deliveryDate"></param>
        /// <returns></returns>
        Dictionary<TcDeliveryRequest, string> GetDPlusOneDeliveryDictionary(List<string> stationList, DateTime deliveryDate);

        /// <summary>
        /// 取得當日結算前30天出貨的留庫明細
        /// </summary>
        /// <param name="station"></param>
        /// <param name="shipStartDate"></param>
        /// <param name="checkDate"></param>
        /// <returns></returns>
        List<TcDeliveryRequest> GetDepotDeliveryList(string station, DateTime checkDate, DateTime shipStartDate);
        /// <summary>
        /// 取得當日結算前30天出貨的留庫明細
        /// </summary>
        /// <param name="stationList"></param>
        /// <param name="shipStartDate"></param>
        /// <param name="checkDate"></param>
        /// <returns></returns>
        List<TcDeliveryRequest> GetDepotDeliveryList(List<string> stationList, DateTime checkDate, DateTime shipStartDate);

        /// <summary>
        /// 取得該站的應配資料
        /// </summary>
        /// <param name="station">站所代碼</param>
        /// <param name="checkDate"></param>
        /// <returns></returns>
        int GetCountByArriveCodeAndDate(string station, DateTime checkDate);

        /// <summary>
        /// 取得該站應配清單
        /// </summary>
        /// <param name="station">站所代碼</param>
        /// <param name="checkDate"></param>
        /// <returns></returns>
        List<TcDeliveryRequest> GetListByArriveCodeAndDate(string station, DateTime checkDate);

        int GetSuccessCountByArriveCodeAndDate(string station, DateTime checkDate, int days);

        List<TcDeliveryRequest> GetSuccessListByArriveCodeAndDate(string station, DateTime checkDate, int days);

        List<string> GetCheckNumberListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end);

        List<string> GetSuccessCheckNumberListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end);

        List<string> GetCheckNumberListByActArriveCodeAndDateZone(string station, DateTime start, DateTime end);

        List<string> GetSuccessCheckNumberListByActArriveCodeAndDateZone(string station, DateTime start, DateTime end);

        /// <summary>
        /// 以客代及shipdate區間首次成功配達的checknumber及配達時間
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        Dictionary<string, DateTime?> GetSuccessCheckNumberAndTimeListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end);

        /// <summary>
        /// 以客代及shipdate區間查最終掃讀時間
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        Dictionary<string, DateTime?> GetLastScanDateListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end);

        /// <summary>
        /// 以應到著站及shipdate區間取得首次成功配達的checknumber及配達時間
        /// </summary>
        /// <param name="station"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        Dictionary<string, DateTime?> GetSuccessCheckNumberAndTimeListByActArriveCodeAndDateZone(string station, DateTime start, DateTime end);

        /// <summary>
        /// 以應到著站及shipdate區間取得最終掃讀時間
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        Dictionary<string, DateTime?> GetLastScanDateListByActArriveCodeAndDateZone(string station, DateTime start, DateTime end);

        /// <summary>
        /// 以託運單號取得整個託運單清單
        /// </summary>
        /// <param name="checkNumbers">託運單號清單</param>
        /// <returns></returns>
        List<TcDeliveryRequest> GetDeliveryRequestsByCheckNumberList(List<string> checkNumbers);

        /// <summary>
        /// 以託運單號取得最後一個掃讀歷程資料
        /// </summary>
        /// <param name="checkNumbers"></param>
        /// <returns></returns>
        Dictionary<string, TtDeliveryScanLog> GetLastScanLog(List<string> checkNumbers);

        /// <summary>
        /// 以起訖時間和發送站取得逆物流託運單清單
        /// </summary>
        /// <param name="station"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<TcDeliveryRequest> GetReturnDeliveryCheckNumberLisyBySendStationCodeAndDateZone(string station, DateTime start, DateTime end);

        /// <summary>
        /// 以起訖時間和客戶代碼取得逆物流託運單清單
        /// </summary>
        /// <param name="customerCode"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<TcDeliveryRequest> GetReturnDeliveryCheckNumberListByCustomerCodeAndDateZone(string customerCode, DateTime start, DateTime end);

        /// <summary>
        /// 以起訖時間取得逆物流託運單清單
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<TcDeliveryRequest> GetAllReturnDeliveryCheckNumberLisyByDateZone(DateTime start, DateTime end);
        List<TcDeliveryRequest> GetErrorDoneDeliveryRequestsByScanDateArea(DateTime startDate, DateTime endDate, string areaName);

        List<TcDeliveryRequest> GetErrorDoneDeliveryRequestsByDriverCode(string driverCode);


        IEnumerable<TcDeliveryRequest> GetAllRequestBetweenDates(DateTime start, DateTime end);

        IEnumerable<TcDeliveryRequest> GetRequestByArriveCodeAndTimePeriod(string stationScode, DateTime start, DateTime end);

        IEnumerable<TcDeliveryRequest> GetRequestsByCustomerCodeAndTimePeriod(string customerCode, DateTime start, DateTime end);

        IEnumerable<TcDeliveryRequest> GetRequestsBySupplyStationAndTimePeriod(string stationScode, DateTime start, DateTime end);

        List<string> GetCheckNumbersByIds(List<decimal> ids);

        IEnumerable<TcDeliveryRequest> GetNonFinishByCustomerCodeAndTimePeriod(string customerCode, DateTime start, DateTime end);

        IEnumerable<TcDeliveryRequest> GetErrorFinishByCustomerCodeAndTimePeriod(string customerCode, DateTime start, DateTime end);

        IEnumerable<TcDeliveryRequest> GetNonFinishByActArriveCodeAndTimePeriod(string stationScode, DateTime start, DateTime end);

        IEnumerable<TcDeliveryRequest> GetErrorFinishByActArriveCodeAndTimePeriod(string stationScode, DateTime start, DateTime end);

        List<TcDeliveryRequest> GetAllAreaErrorDoneDeliveryRequestsByScanDate(DateTime startDate, DateTime endDate);

        List<TcDeliveryRequest> GetErrorDoneDeliveryRequestsByScanDateStationScode(DateTime startDate, DateTime endDate, string scode);

        List<TcDeliveryRequest> GetAllByRequestIds(List<decimal> requestIds);

        IEnumerable<TcDeliveryRequest> GetNotMatingByShipDatePeriod(DateTime start, DateTime end);

        IEnumerable<TcDeliveryRequest> GetNotMatingByStationListAndShipDatePeriod(IEnumerable<string> stationScode, DateTime start, DateTime end);

        IEnumerable<TcDeliveryRequest> GetNotMatingByCustomerAndShipDatePeriod(string customerCode, DateTime start, DateTime end);

        void CallUpdateProcedure(string customerCode);

        List<TcDeliveryRequest> GetPalletByPrintDate(DateTime printDate);

        List<TcDeliveryRequest> GetByShipDatePeriod(DateTime shipDateStart, DateTime shipDateEnd);
    }
}
