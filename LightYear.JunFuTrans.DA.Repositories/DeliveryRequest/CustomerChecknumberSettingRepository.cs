﻿using LightYear.JunFuTrans.BL.BE.PayCustomerPayment;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.CustomerChecknumberSetting
{
    public class CustomerChecknumberSettingRepository : ICustomerChecknumberSettingRepository
    {
        public JunFuDbContext JunFuDbContext { get; set; }
        public string CustomerCode { get; }

        public CustomerChecknumberSettingRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }

        public CustomerChecknumberSettingRepository(string customerCode)
        {
            CustomerCode = customerCode;
        }

        public string GetRemainCount(string customerCode)
        {

            var totalCount = (from s1 in JunFuDbContext.CustomerChecknumberSettings
                              where s1.CustomerCode == customerCode
                              orderby s1.Id descending
                              select s1.TotalCount)
                        .ToArray();

            var usedCount = (from s1 in JunFuDbContext.CustomerChecknumberSettings
                             where s1.CustomerCode == customerCode
                             orderby s1.Id descending
                             select s1.UsedCount)
                                     .ToArray();

            int remainCount= 0;

            if (totalCount.Length > 1 && usedCount.Length > 1)
            {
                remainCount = (int)(totalCount[0] - usedCount[0]);
            }
            else
            {
                
            }
            return remainCount.ToString();
        }

        public JunFuDb.CustomerChecknumberSetting GetUsedCount(string customerCode)
        {



            var ss = from s1 in JunFuDbContext.CustomerChecknumberSettings
                            where s1.CustomerCode == customerCode
                            orderby s1.Id descending
                            select s1;
                            

            return ss.FirstOrDefault();

        }


        public void UpdateUsedCount(JunFuDb.CustomerChecknumberSetting customerChecknumberSetting)
        {
            


            this.JunFuDbContext.Update(customerChecknumberSetting);

         

        }
    }
}
