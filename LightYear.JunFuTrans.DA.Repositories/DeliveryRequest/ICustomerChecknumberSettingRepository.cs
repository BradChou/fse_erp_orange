﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.CustomerChecknumberSetting
{
    public interface ICustomerChecknumberSettingRepository
    {
        public string GetRemainCount(string customerCode);

        public JunFuDb.CustomerChecknumberSetting GetUsedCount(string customerCode);

        public void UpdateUsedCount(JunFuDb.CustomerChecknumberSetting customerChecknumberSetting);
    }

    

}
