﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class VW_tbSales_StationRepository : IVW_tbSales_StationRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public VW_tbSales_StationRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }
        public List<VW_tbSales_Station> GetAll()
        {
            return JunFuDbContext.VW_tbSales_Stations.ToList();
        }

    }
}
