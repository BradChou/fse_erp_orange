﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public interface IVW_tbSales_StationRepository
    {
        public List<VW_tbSales_Station> GetAll();
    }
}
