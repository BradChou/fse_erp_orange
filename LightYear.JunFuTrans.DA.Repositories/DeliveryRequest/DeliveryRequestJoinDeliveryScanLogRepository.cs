﻿using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryRequest
{
    public class DeliveryRequestJoinDeliveryScanLogRepository : IDeliveryRequestJoinDeliveryScanLogRepository
    {
        public JunFuDbContext JunFuDbContext { get; set; }

        public JunFuTransDbContext JunFuTransDbContext { get; set; }

        public DeliveryRequestJoinDeliveryScanLogRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            JunFuDbContext = junFuDbContext;
            JunFuTransDbContext = junFuTransDbContext;
        }

        public IEnumerable<DeliveryDetailEntity> GetDeliveryDetailByDriverCodeAndDeliveryDatePeriod(string driverCode, DateTime deliveryDateStart, DateTime deliveryDateEnd)
        {
            var deliveryDetailEntities = (from r in JunFuDbContext.TcDeliveryRequests
                        join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                        join d in JunFuDbContext.TbDrivers on sl.DriverCode equals d.DriverCode
                        join s in JunFuDbContext.TbStations on r.AreaArriveCode equals s.StationScode
                        where sl.DriverCode == driverCode && r.LatestDeliveryDriver == driverCode && (sl.ScanItem == "2" || sl.ScanItem == "3")
                        && (sl.ScanDate > deliveryDateStart && sl.ScanDate < deliveryDateEnd.AddDays(1)) || (r.PrintDate >= deliveryDateStart && r.PrintDate < deliveryDateEnd.AddDays(1)) && r.LessThanTruckload == true
                        select new DeliveryDetailEntity
                        {
                            //ShipDate = r.ShipDate,
                            //ScanItem = sl.ScanItem,
                            //ScanDate = sl.ScanDate,                            
                            StationName = s.StationName,
                            DriverName = d.DriverName,
                            CheckNumber = r.CheckNumber,
                            SendContact = r.SendContact,
                            ReceiveContact = r.ReceiveContact,
                            ReceiveAddress = r.ReceiveAddress,
                            ReceivePhone = r.ReceiveTel1,
                            Pieces = r.Pieces ?? 1,                            
                            DestinationTime = r.LatestDestDate,
                            DeliveryTime = r.LatestDeliveryDate,
                            DeliveryMatingTime = r.LatestArriveOptionDate,
                            ArriveOption = r.LatestArriveOption,
                            WriteOffTime = r.DeliveryCompleteDate
                        }).Distinct();

            return deliveryDetailEntities;
        }

        public IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntitiesByStationAndShipDatePeriod(IEnumerable<string> stationScode, DateTime start, DateTime end)
        {
            return from r in JunFuDbContext.TcDeliveryRequests
                   join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                   where r.ShipDate > start && r.ShipDate <= end.AddDays(1) && stationScode.Contains(r.AreaArriveCode) && r.LessThanTruckload == true
                   select new DeliveryRequestJoinScanLogForMatingRate
                   {
                       ShipDate = r.ShipDate ?? DateTime.MinValue,
                       ScanDate = sl.ScanDate ?? DateTime.MinValue,
                       ScanItem = sl.ScanItem,
                       ArriveOption = sl.ArriveOption,
                       AreaArriveCode = r.AreaArriveCode,
                       DriverCode = r.LatestDeliveryDriver,
                       CheckNumber = r.CheckNumber
                   };
        }

        public IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntitiesByShipDatePeriod(DateTime start, DateTime end)
        {
            return from r in JunFuDbContext.TcDeliveryRequests
                   join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                   join s in JunFuDbContext.TbStations on r.AreaArriveCode equals s.StationScode
                   where r.ShipDate > start && r.ShipDate <= end.AddDays(1) && r.LessThanTruckload == true
                   orderby s.Owner
                   select new DeliveryRequestJoinScanLogForMatingRate
                   {
                       ShipDate = r.ShipDate ?? DateTime.MinValue,
                       ScanDate = sl.ScanDate ?? DateTime.MinValue,
                       ScanItem = sl.ScanItem,
                       ArriveOption = sl.ArriveOption,
                       AreaArriveCode = r.AreaArriveCode,
                       DriverCode = sl.DriverCode,
                       CheckNumber = r.CheckNumber
                   };
        }

        public IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntitiesByAreaAndShipDatePeriod(string areaName, DateTime start, DateTime end)
        {
            IEnumerable<string> stationScode = JunFuTransDbContext.StationAreas.Where(s => s.Area == areaName).Select(s => s.StationScode).ToList();
            return from r in JunFuDbContext.TcDeliveryRequests
                   join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                   where r.ShipDate > start && r.ShipDate <= end.AddDays(1) && r.LessThanTruckload == true
                   && stationScode.Contains(r.AreaArriveCode)
                   select new DeliveryRequestJoinScanLogForMatingRate
                   {
                       ShipDate = r.ShipDate ?? DateTime.MinValue,
                       ScanDate = sl.ScanDate ?? DateTime.MinValue,
                       ScanItem = sl.ScanItem,
                       ArriveOption = sl.ArriveOption,
                       AreaArriveCode = r.AreaArriveCode,
                       DriverCode = sl.DriverCode,
                       CheckNumber = r.CheckNumber
                   };
        }

        public IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntityByStationListAndDeliveryDate(IEnumerable<string> stationScode, DateTime deliveryDateStart, DateTime deliveryDateEnd)
        {
            var checkNumbers = from r in JunFuDbContext.TcDeliveryRequests
                               join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                               where stationScode.Contains(r.AreaArriveCode) && sl.ScanDate > deliveryDateStart && sl.ScanDate < deliveryDateEnd.AddDays(1)
                               && sl.ScanItem == "2" && r.LessThanTruckload == true
                               select r.CheckNumber;

            return from r in JunFuDbContext.TcDeliveryRequests
                   join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                   where checkNumbers.Contains(r.CheckNumber)
                   select new DeliveryRequestJoinScanLogForMatingRate
                   {
                       ShipDate = r.ShipDate ?? DateTime.MinValue,
                       ScanDate = sl.ScanDate ?? DateTime.MinValue,
                       ScanItem = sl.ScanItem,
                       ArriveOption = sl.ArriveOption,
                       AreaArriveCode = r.AreaArriveCode,
                       DriverCode = r.LatestDeliveryDriver,
                       CheckNumber = r.CheckNumber
                   };
        }

        public IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntityByCustomerAndDeliveryDate(string customerCode, DateTime deliveryDateStart, DateTime deliveryDateEnd)
        {
            var checkNumbers = from r in JunFuDbContext.TcDeliveryRequests
                               join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                               where r.CustomerCode == customerCode && sl.ScanDate > deliveryDateStart && sl.ScanDate < deliveryDateEnd.AddDays(1)
                               && sl.ScanItem == "2" && r.LessThanTruckload == true
                               select r.CheckNumber;

            return from r in JunFuDbContext.TcDeliveryRequests
                   join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                   where checkNumbers.Contains(r.CheckNumber)
                   select new DeliveryRequestJoinScanLogForMatingRate
                   {
                       ShipDate = r.ShipDate ?? DateTime.MinValue,
                       ScanDate = sl.ScanDate ?? DateTime.MinValue,
                       ScanItem = sl.ScanItem,
                       ArriveOption = sl.ArriveOption,
                       AreaArriveCode = r.AreaArriveCode,
                       DriverCode = sl.DriverCode,
                       CheckNumber = r.CheckNumber
                   };
        }

        public IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntityByDeliveryDate(DateTime deliveryDateStart, DateTime deliveryDateEnd)
        {
            var checkNumbers = (from r in JunFuDbContext.TcDeliveryRequests
                                join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                                where sl.ScanDate > deliveryDateStart && sl.ScanDate < deliveryDateEnd.AddDays(1)
                                && sl.ScanItem == "2" && r.LessThanTruckload == true
                                select r.CheckNumber).ToList();

            var requestJoinScanlog = (from r in JunFuDbContext.TcDeliveryRequests
                                      join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                                      where checkNumbers.Contains(r.CheckNumber)
                                      select new
                                      {
                                          ShipDate = r.ShipDate,
                                          ScanDate = sl.ScanDate,
                                          ScanItem = sl.ScanItem,
                                          ArriveOption = sl.ArriveOption,
                                          AreaArriveCode = r.AreaArriveCode,
                                          DriverCode = sl.DriverCode,
                                          CheckNumber = r.CheckNumber
                                      }).ToList();

            List<DeliveryRequestJoinScanLogForMatingRate> result = new List<DeliveryRequestJoinScanLogForMatingRate>();
            foreach (var e in requestJoinScanlog)
            {
                try
                {
                    var t = new DeliveryRequestJoinScanLogForMatingRate
                    {
                        ShipDate = e.ShipDate ?? DateTime.MinValue,
                        ScanDate = e.ScanDate ?? DateTime.MinValue,
                        ScanItem = e.ScanItem,
                        ArriveOption = e.ArriveOption,
                        AreaArriveCode = e.AreaArriveCode,
                        DriverCode = e.DriverCode,
                        CheckNumber = e.CheckNumber
                    };
                    result.Add(t);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return result;
        }

        public IEnumerable<DeliveryRequestJoinScanLogForMatingRate> GetJoinedEntitiesByCustomerAndShipDatePeriod(string customerCode, DateTime start, DateTime end)
        {
            return from r in JunFuDbContext.TcDeliveryRequests
                   join sl in JunFuDbContext.TtDeliveryScanLogs on r.CheckNumber equals sl.CheckNumber
                   join s in JunFuDbContext.TbStations on r.AreaArriveCode equals s.StationScode
                   where r.ShipDate > start && r.ShipDate <= end.AddDays(1) && r.LessThanTruckload == true
                   && r.CustomerCode == customerCode
                   orderby s.Owner
                   select new DeliveryRequestJoinScanLogForMatingRate
                   {
                       ShipDate = r.ShipDate ?? DateTime.MinValue,
                       ScanDate = sl.ScanDate ?? DateTime.MinValue,
                       ScanItem = sl.ScanItem,
                       ArriveOption = sl.ArriveOption,
                       AreaArriveCode = r.AreaArriveCode,
                       DriverCode = sl.DriverCode,
                       CheckNumber = r.CheckNumber
                   };
        }

        public IEnumerable<TcDeliveryRequest> GetRequestsAlreadyPickUpByCustomerAndMonth(string customerCode, int year, int month)
        {
            var pickedUpRequests = (from r in JunFuDbContext.TcDeliveryRequests
                                    where ((r.ShipDate.Value.Year == year && r.ShipDate.Value.Month == month ) || (r.PrintDate.Value.Year == year && r.PrintDate.Value.Month == month)) && r.LessThanTruckload == true
                                    && r.CustomerCode == customerCode && r.ShipDate != null
                                    select r.CheckNumber).Distinct();

            var allRecord = JunFuDbContext.TcDeliveryRequests.Where(r => pickedUpRequests.Contains(r.CheckNumber));
            var checkNumAndCDate = JunFuDbContext.TcDeliveryRequests.Where(r => pickedUpRequests.Contains(r.CheckNumber))
                .Select(r => new { CheckNumber = r.CheckNumber, CDate = r.Cdate });
            var latestRecord = from c in checkNumAndCDate
                                group c by c.CheckNumber into g
                                select new { CheckNumber = g.Key, CDate = g.Max(r => r.CDate) };

            return (from a in allRecord
                    join l in latestRecord on new { a.CheckNumber, Cdate = a.Cdate } equals new { l.CheckNumber, Cdate = l.CDate }
                    select a);
        }

        public IEnumerable<TcDeliveryRequest> GetRequestsAlreadyPickUpByCustomerListAndMonth(IEnumerable<string> customerCodes, int year, int month)
        {
            var pickedUpRequests = (from r in JunFuDbContext.TcDeliveryRequests
                                    where ((r.ShipDate.Value.Year == year && r.ShipDate.Value.Month == month) || (r.PrintDate.Value.Year == year && r.PrintDate.Value.Month == month)) && r.LessThanTruckload == true
                                    && customerCodes.Contains(r.CustomerCode) && r.ShipDate != null
                                    select r.CheckNumber).Distinct();

            // 去除重複 check_number
            var allRecord = JunFuDbContext.TcDeliveryRequests.Where(r => pickedUpRequests.Contains(r.CheckNumber));
            var checkNumAndCDate = JunFuDbContext.TcDeliveryRequests.Where(r => pickedUpRequests.Contains(r.CheckNumber))
                .Select(r => new { CheckNumber = r.CheckNumber, CDate = r.Cdate });
            var laltestRecord = from c in checkNumAndCDate
                                group c by c.CheckNumber into g
                                select new { CheckNumber = g.Key, CDate = g.Max(r => r.CDate) };

            return (from a in allRecord
                    join l in laltestRecord on new { a.CheckNumber, Cdate = a.Cdate } equals new { l.CheckNumber, Cdate = l.CDate }
                    select a);
        }

        public Dictionary<Tuple<DateTime, string>, int> GetPickUpAmountByCustomersAndDate(IEnumerable<string> customerCodes, DateTime start, DateTime end)
        {
            //防死結
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
            , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                var joinedRequest = from r in JunFuDbContext.TcDeliveryRequests
                                    where customerCodes.Contains(r.CustomerCode) && r.ShipDate > start && r.ShipDate < end.AddDays(1)
                                    select new { CheckNumber = r.CheckNumber, CustomerCode = r.CustomerCode, ScanDate = r.ShipDate.Value.Date };

                var grouped = from j in joinedRequest
                              group j by new { j.CustomerCode, j.ScanDate } into g
                              // select new Tuple<DateTime, string, int> ( g.Key.ScanDate, g.Key.CustomerCode, g.Count());
                              select new { g.Key.ScanDate, g.Key.CustomerCode, Count = g.Count() };

                var dic = grouped.ToDictionary(g => new Tuple<DateTime, string>(g.ScanDate, g.CustomerCode), g => g.Count);

                return dic;
            }
        }
    }
}
