﻿using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public interface ITruckVendorPaymentRepository
    {
        List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendor1(DateTime start, DateTime end, string vendor);

        void Update(TruckVendorPaymentLog truckVendorPaymentLog);

        void UpdateVendorPayment(TruckVendorPaymentLog truckVendorPaymentLog);

        void Delete(TruckVendorPaymentLog truckVendorPaymentLog);

        void BatchDelete(int[] ids);

        int Insert(TruckVendorPaymentLog truckVendorPaymentLog);
        List<TruckVendor> GetTruckVendors();
        List<DropDownListEntity> GetFeeTypes();
        List<DropDownListEntity> GetCompanies();
        List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor(DateTime start, DateTime end, string vendor);
        List<TbItemCode> GetFeeTypeTbItemCodes();
        PaymentEntity GetIncomeAndExpenditure(DateTime start, DateTime end, string vendor);
        List<TruckVendorPaymentLog> GetLatestUploadData(string ImportRandomCode = "");
        List<int> InsertBatch(List<TruckVendorPaymentLog> truckVendorPaymentLogs);
        TruckVendor GetTruckVendorByTruckVendorAccountCode(string accountCode);

        string GetVenderNameById(int id);

        List<TruckVendorPaymentLog> GetAllByMonth(DateTime start, DateTime end);

        List<TruckVendorPaymentLog> GetPaymentByTimePeriodAndFeeType(DateTime start, DateTime end, string[] feeTypes);

        List<TruckVendorPaymentLog> GetPaymentByFeeType(string[] feeTypes);

        List<TruckVendorPaymentLog> GetPaymentByTimePeriod(DateTime start, DateTime end);
    }
}
