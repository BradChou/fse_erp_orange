﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class TbItemCodesRepository : ITbItemCodesRepository
    {
        public JunFuDbContext DbContext { get; set; }

        public TbItemCodesRepository(JunFuDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public List<string> GetFeeTypesCodeByDeptCodeId(string id)
        {
            return DbContext.TbItemMappingCodes.Where(i => i.MappingAId == id).Select(i => i.MappingBId).ToList();
        }

        public string GetDepartmentFromFeeType(string id)
        {
            string codeId = DbContext.TbItemMappingCodes.Where(i => i.MappingBId == id).Select(i => i.MappingAId).FirstOrDefault();

            return GetDepartmentNameById(codeId);
        }

        public Dictionary<string, string> GetDutyDeptMapping()
        {
            Dictionary<string, string> feeTypeToDuty = DbContext.TbItemMappingCodes.ToDictionary(c => c.MappingBId, c => c.MappingAId);

            return feeTypeToDuty;
        }

        public string GetFeeTypeNameById(string id)
        {
            return DbContext.TbItemCodes.Where(i => i.CodeId == id && i.CodeBclass == "6" && i.CodeSclass == "fee_type")
                .Select(i => i.CodeName).FirstOrDefault();
        }

        public string GetDepartmentNameById(string id)
        {
            return DbContext.TbItemCodes.Where(i => i.CodeId == id && i.CodeBclass == "6" && i.CodeSclass == "jf_dept")
                .Select(i => i.CodeName).FirstOrDefault();
        }        
        
        public string GetCompanyNameById(string id)
        {
            return DbContext.TbItemCodes.Where(i => i.CodeId == id && i.CodeBclass == "6" && i.CodeSclass == "CD")
                .Select(i => i.CodeName).FirstOrDefault();
        }

        public string GetNameByCodeId(string id)
        {
            return DbContext.TbItemCodes.Where(i => i.CodeId == id).Select(i => i.CodeName).FirstOrDefault();
        }
    }
}
