﻿using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentRepository : ITruckVendorPaymentRepository
    {
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }
        public JunFuDbContext JunFuDbContext { get; set; }

        public TruckVendorPaymentRepository(JunFuTransDbContext junFuTransDbContext, JunFuDbContext junFuDbContext)
        {
            JunFuTransDbContext = junFuTransDbContext;
            JunFuDbContext = junFuDbContext;
        }

        public int Insert(TruckVendorPaymentLog truckVendorPaymentLog)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogs.Add(truckVendorPaymentLog);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLog.Id;
        }

        public List<int> InsertBatch(List<TruckVendorPaymentLog> truckVendorPaymentLogs)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogs.AddRange(truckVendorPaymentLogs);

            this.JunFuTransDbContext.SaveChanges();

            return truckVendorPaymentLogs.Select(a => a.Id).ToList();
        }

        public void Update(TruckVendorPaymentLog truckVendorPaymentLog)
        {

            this.JunFuTransDbContext.TruckVendorPaymentLogs.Attach(truckVendorPaymentLog);

            var entry = this.JunFuTransDbContext.Entry(truckVendorPaymentLog);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.RegisteredDate).IsModified = false;
            entry.Property(x => x.CreateDate).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }

        public void UpdateVendorPayment(TruckVendorPaymentLog truckVendorPaymentLog)
        {

            this.JunFuTransDbContext.TruckVendorPaymentLogs.Attach(truckVendorPaymentLog);

            var entry = this.JunFuTransDbContext.Entry(truckVendorPaymentLog);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            entry.Property(x => x.CreateDate).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }

        public void Delete(TruckVendorPaymentLog truckVendorPaymentLog)
        {
            this.JunFuTransDbContext.TruckVendorPaymentLogs.Attach(truckVendorPaymentLog);
            this.JunFuTransDbContext.TruckVendorPaymentLogs.Remove(truckVendorPaymentLog);

            this.JunFuTransDbContext.SaveChanges();
        }

        public List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendor1(DateTime start, DateTime end, string vendor)
        {
            var data = JunFuTransDbContext.TruckVendorPaymentLogs.Where(a => a.RegisteredDate >= start && a.RegisteredDate < end && a.VendorName == vendor).ToList();
            return data;
        }

        public List<TruckVendor> GetTruckVendors()
        {
            return JunFuTransDbContext.TruckVendors.Where(a => a.IsActive.Equals(true)).ToList();
        }

        public TruckVendor GetTruckVendorByTruckVendorAccountCode(string accountCode)
        {
            var data = JunFuTransDbContext.TruckVendors.Where(a => a.IsActive.Equals(true) && a.AccountCode.Equals(accountCode));

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public string GetVenderNameById(int id)
        {
            return JunFuTransDbContext.TruckVendors.Where(a => a.IsActive.Equals(true) && a.Id == id).Select(a => a.VendorName).FirstOrDefault();
        }

        public List<DropDownListEntity> GetFeeTypes()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true)).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .Select(a => new DropDownListEntity { Id = a.CodeId, Name = a.CodeName }).ToList();
        }
        public List<DropDownListEntity> GetCompanies()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "CD" && a.ActiveFlag.Equals(true)).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .Select(a => new DropDownListEntity { Id = a.CodeId, Name = a.CodeName }).ToList();
        }

        public List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor(DateTime start, DateTime end, string vendor)
        {
            var data = (from log in JunFuTransDbContext.TruckVendorPaymentLogs
                        orderby log.FeeType
                        where log.RegisteredDate >= start && log.RegisteredDate < end && log.VendorName == vendor
                        select new DisbursementEntity
                        {
                            FeeType = log.FeeType,
                            Payment = log.Payment ?? 0,
                            //VendorName = g.Key.VendorName,
                            //Date = new DateTime (g.Key.Year,g.Key.Month,02)
                        }).ToList();

            var feetype = JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true)).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .Select(a => new DisbursementEntity { FeeType = a.CodeId, Payment = 0 }).ToList();

            data.AddRange(feetype);
            var output = data.GroupBy(a => a.FeeType).Select(a => new DisbursementEntity { FeeType = a.Key, Payment = a.Sum(x => x.Payment) }).OrderBy(a => Convert.ToInt32(a.FeeType)).ToList();

            return output;
        }

        public List<TbItemCode> GetFeeTypeTbItemCodes()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true)).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .ToList();
        }

        public PaymentEntity GetIncomeAndExpenditure(DateTime start, DateTime end, string vendor)
        {
            var total = GetDisbursementByMonthAndTruckVendor(start, end, vendor).Sum(a => a.Payment);

            var incomeCodeId = JunFuDbContext.TbItemCodes.Where(a => a.CodeBclass == "6" && a.CodeSclass == "fee_type" && a.ActiveFlag.Equals(true) && a.Memo.Contains("收入")).OrderBy(a => Convert.ToInt32(a.CodeId))
                  .Select(a => a.CodeId).ToList();

            var income = GetDisbursementByMonthAndTruckVendor(start, end, vendor).Where(a => incomeCodeId.Contains(a.FeeType)).Sum(a => a.Payment);

            int expenditure = total - income;

            PaymentEntity paymentEntity = new PaymentEntity()
            {
                Income = income.ToString(),
                Expenditure = expenditure.ToString(),
                Remainder = (income - expenditure).ToString()
            };
            return paymentEntity;
        }

        public List<TruckVendorPaymentLog> GetLatestUploadData(string ImportRandomCode = "")
        {
            return JunFuTransDbContext.TruckVendorPaymentLogs.Where(s => s.ImportRandomCode == ImportRandomCode).ToList();
        }

        public List<TruckVendorPaymentLog> GetAllByMonth(DateTime start, DateTime end)
        {
            var data = JunFuTransDbContext.TruckVendorPaymentLogs.Where(a => a.RegisteredDate >= start && a.RegisteredDate < end).ToList();
            return data;
        }

        public List<TruckVendorPaymentLog> GetPaymentByTimePeriodAndFeeType(DateTime start, DateTime end, string[] feeTypes)
        {
            var data = JunFuTransDbContext.TruckVendorPaymentLogs.Where(a => a.CreateDate >= start && a.CreateDate < end
                && feeTypes.Contains(a.FeeType)).ToList();
            return data;
        }

        public List<TruckVendorPaymentLog> GetPaymentByFeeType(string[] feeTypes)
        {
            var data = JunFuTransDbContext.TruckVendorPaymentLogs.Where(a => feeTypes.Contains(a.FeeType)).ToList();
            return data;
        }

        public List<TruckVendorPaymentLog> GetPaymentByTimePeriod(DateTime start, DateTime end)
        {
            var data = JunFuTransDbContext.TruckVendorPaymentLogs.Where(a => a.CreateDate >= start && a.CreateDate < end).ToList();
            return data;
        }

        public void BatchDelete(int[] ids)
        {
            var data = JunFuTransDbContext.TruckVendorPaymentLogs.Where(t => ids.Contains(t.Id));

            JunFuTransDbContext.RemoveRange(data);

            JunFuTransDbContext.SaveChanges();
        }
    }
}
