﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public interface IFunctionUserMappingRepository
    {
        int Insert(SystemFunctionUserMapping systemFunctionUserMapping);

        void Update(SystemFunctionUserMapping systemFunctionUserMapping);

        List<SystemFunctionUserMapping> GetAllSystemFunctionUserMappingsByUserIdAndUserType(int userId, int userType);

        List<SystemFunctionUserMapping> GetAllSystemFunctionUserMappings();

        SystemFunctionUserMapping GetSystemFunctionUserMapping(int id);

        void Delete(int id);

        void DeleteByUserIdAndAccountType(int userId, int accountType);
    }
}
