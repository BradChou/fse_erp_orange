﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public class AccountsRepository : IAccountsRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public AccountsRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public TbAccount GetByAccountCode(string account)
        {
            var data = from tbAccount in JunFuDbContext.TbAccounts where tbAccount.AccountCode.Equals(account) 
                       select new TbAccount {
                           AccountCode = tbAccount.AccountCode,
                           Password = tbAccount.Password,
                           AccountId = tbAccount.AccountId,
                           ActiveFlag = tbAccount.ActiveFlag,
                           BodyCode = tbAccount.BodyCode,
                           Cdate = tbAccount.Cdate,
                           Cuser = tbAccount.Cuser,
                           CustomerCode = tbAccount.CustomerCode,
                           EmpCode = tbAccount.EmpCode,
                           HeadCode = tbAccount.HeadCode,
                           JobTitle = tbAccount.JobTitle,
                           ManagerType = tbAccount.ManagerType,
                           ManagerUnit = tbAccount.ManagerUnit,
                           MasterCode = tbAccount.MasterCode,
                           PasswordChanged = tbAccount.PasswordChanged,
                           Udate = tbAccount.Udate,
                           UserEmail = tbAccount.UserEmail,
                           UserName = tbAccount.UserName,
                           Uuser = tbAccount.Uuser,
                           station_level = "",
                           station_scode = "",
                           station_area = "",
                           management = ""
                       };

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public TbAccount GetByAccountAndPassword(string account, string password)
        {
            // var data = from tbAccount in JunFuDbContext.TbAccounts where tbAccount.AccountCode.Equals(account) && tbAccount.Password.Equals(password) select tbAccount;

            var data = from tbAccount in JunFuDbContext.TbAccounts 
                       where tbAccount.AccountCode.Equals(account) && tbAccount.Password.Equals(password) 
                       select new TbAccount
                       {
                           AccountCode = tbAccount.AccountCode,
                           Password = tbAccount.Password,
                           AccountId = tbAccount.AccountId,
                           ActiveFlag = tbAccount.ActiveFlag,
                           BodyCode = tbAccount.BodyCode,
                           Cdate = tbAccount.Cdate,
                           Cuser = tbAccount.Cuser,
                           CustomerCode = tbAccount.CustomerCode,
                           EmpCode = tbAccount.EmpCode,
                           HeadCode = tbAccount.HeadCode,
                           JobTitle = tbAccount.JobTitle,
                           ManagerType = tbAccount.ManagerType,
                           ManagerUnit = tbAccount.ManagerUnit,
                           MasterCode = tbAccount.MasterCode,
                           PasswordChanged = tbAccount.PasswordChanged,
                           Udate = tbAccount.Udate,
                           UserEmail = tbAccount.UserEmail,
                           UserName = tbAccount.UserName,
                           Uuser = tbAccount.Uuser,
                           station_level = "",
                           station_scode = "",
                           station_area = "",
                           management = ""
                       };




            var data1 = from tbAccount in JunFuDbContext.TbAccounts
                       join tbemp in JunFuDbContext.TbEmps
                       on tbAccount.EmpCode equals tbemp.EmpCode
                       join tbstation in JunFuDbContext.TbStations
                       on tbemp.Station equals Convert.ToString(tbstation.Id)
                       where tbAccount.AccountCode.Equals(account) && 
                       tbAccount.Password.Equals(password) 
                       select new TbAccount
                       {
                           AccountCode = tbAccount.AccountCode,
                           Password = tbAccount.Password,
                           AccountId= tbAccount.AccountId,
                           ActiveFlag= tbAccount.ActiveFlag,
                           BodyCode= tbAccount.BodyCode,
                           Cdate= tbAccount.Cdate,
                           Cuser= tbAccount.Cuser,
                           CustomerCode= tbAccount.CustomerCode,
                           EmpCode= tbAccount.EmpCode,
                           HeadCode= tbAccount.HeadCode,
                           JobTitle= tbAccount.JobTitle,
                           ManagerType= tbAccount.ManagerType,
                           ManagerUnit= tbAccount.ManagerUnit,
                           MasterCode= tbAccount.MasterCode,
                           PasswordChanged= tbAccount.PasswordChanged,
                           Udate= tbAccount.Udate,
                           UserEmail= tbAccount.UserEmail,
                           UserName= tbAccount.UserName,
                           Uuser= tbAccount.Uuser,
                           station_level = Convert.ToString(tbstation.station_level),
                           station_scode = Convert.ToString(tbstation.StationScode),
                           station_area = Convert.ToString(tbstation.station_area),
                           management = tbstation.management
                       };


            //select tbAccount;
            /*var data = @"
Select* from tbAccounts a
left join tbEmps b

    --on a.account_code = b.emp_code
on case when a.emp_code is null then a.account_code else a.emp_code end = b.emp_code

    left join tbStation c

    on c.id = b.station
where a.account_code = 'skyeyes'";*/

            if (data1.Count() > 0)
            {
                return data1.FirstOrDefault();
            }
            else {
                if (data.Count() > 0)
                {
                    return data.FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }


            
        }

        /// <summary>
        /// 取得最新的幾個帳號
        /// </summary>
        /// <returns></returns>
        public List<TbAccount> GetNumAccounts(int num)
        {
            /*
            var data = from tbAccount in this.JunFuDbContext.TbAccounts.OrderByDescending(u => u.AccountId).Take(num).OrderBy(o => o.AccountCode)
                select new TbAccount {
                    AccountCode = tbAccount.AccountCode,
                    Password = tbAccount.Password,
                    AccountId = tbAccount.AccountId,
                    ActiveFlag = tbAccount.ActiveFlag,
                    BodyCode = tbAccount.BodyCode,
                    Cdate = tbAccount.Cdate,
                    Cuser = tbAccount.Cuser,
                    CustomerCode = tbAccount.CustomerCode,
                    EmpCode = tbAccount.EmpCode,
                    HeadCode = tbAccount.HeadCode,
                    JobTitle = tbAccount.JobTitle,
                    ManagerType = tbAccount.ManagerType,
                    ManagerUnit = tbAccount.ManagerUnit,
                    MasterCode = tbAccount.MasterCode,
                    PasswordChanged = tbAccount.PasswordChanged,
                    Udate = tbAccount.Udate,
                    UserEmail = tbAccount.UserEmail,
                    UserName = tbAccount.UserName,
                    Uuser = tbAccount.Uuser,
                    station_level = "",
                    station_scode = "",
                    station_area = "",
                    management = ""

                };
            */
            var data = from tbAccount in JunFuDbContext.TbAccounts//.OrderByDescending(u => u.AccountId).Take(num).OrderBy(o => o.AccountCode)
                           //where tbAccount.AccountCode.Equals(account) && tbAccount.Password.Equals(password)
                       select new TbAccount
                       {
                           AccountCode = tbAccount.AccountCode,
                           Password = tbAccount.Password,
                           AccountId = tbAccount.AccountId,
                           ActiveFlag = tbAccount.ActiveFlag,
                           BodyCode = tbAccount.BodyCode,
                           Cdate = tbAccount.Cdate,
                           Cuser = tbAccount.Cuser,
                           CustomerCode = tbAccount.CustomerCode,
                           EmpCode = tbAccount.EmpCode,
                           HeadCode = tbAccount.HeadCode,
                           JobTitle = tbAccount.JobTitle,
                           ManagerType = tbAccount.ManagerType,
                           ManagerUnit = tbAccount.ManagerUnit,
                           MasterCode = tbAccount.MasterCode,
                           PasswordChanged = tbAccount.PasswordChanged,
                           Udate = tbAccount.Udate,
                           UserEmail = tbAccount.UserEmail,
                           UserName = tbAccount.UserName,
                           Uuser = tbAccount.Uuser,
                           station_level = "",
                           station_scode = "",
                           station_area = "",
                           management = ""
                       };
            var output = data.OrderByDescending(u => u.AccountId).Take(num).OrderBy(o => o.AccountCode);

            return output.ToList();
        }

        /// <summary>
        /// 取得棧板最新的幾個帳號
        /// </summary>
        /// <param name="num"></param>
        /// <param name="managerType">1:俊富管理者, 2:俊富一般員工, 3: 俊富自營, 4:區配商, 5:區配商自營</param>
        /// <returns></returns>
        public List<TbAccount> GetJFNumAccounts(int num, string managerType = "1")
        {
            var data = this.JunFuDbContext.TbAccounts.Where(u => u.AccountType == false && u.ManagerType == managerType)
                .Take(num).OrderBy(o => o.AccountCode);

            return data.ToList();
        }

        /// <summary>
        /// 取得帳號代碼開頭的帳號
        /// </summary>
        /// <param name="startWith"></param>
        /// <returns></returns>
        public List<TbAccount> GetAccountsStartWith(string startWith, int num)
        {
            //var data = from m in this.JunFuDbContext.TbAccounts where m.AccountCode.StartsWith(startWith) || m.UserName.StartsWith(startWith) orderby m.AccountCode select m;

            var data= from tbAccount in JunFuDbContext.TbAccounts//.OrderByDescending(u => u.AccountId).Take(num).OrderBy(o => o.AccountCode)
                      where tbAccount.AccountCode.StartsWith(startWith) || tbAccount.UserName.StartsWith(startWith)                              //where tbAccount.AccountCode.Equals(account) && tbAccount.Password.Equals(password)
                      select new TbAccount
                      {
                          AccountCode = tbAccount.AccountCode,
                          Password = tbAccount.Password,
                          AccountId = tbAccount.AccountId,
                          ActiveFlag = tbAccount.ActiveFlag,
                          BodyCode = tbAccount.BodyCode,
                          Cdate = tbAccount.Cdate,
                          Cuser = tbAccount.Cuser,
                          CustomerCode = tbAccount.CustomerCode,
                          EmpCode = tbAccount.EmpCode,
                          HeadCode = tbAccount.HeadCode,
                          JobTitle = tbAccount.JobTitle,
                          ManagerType = tbAccount.ManagerType,
                          ManagerUnit = tbAccount.ManagerUnit,
                          MasterCode = tbAccount.MasterCode,
                          PasswordChanged = tbAccount.PasswordChanged,
                          Udate = tbAccount.Udate,
                          UserEmail = tbAccount.UserEmail,
                          UserName = tbAccount.UserName,
                          Uuser = tbAccount.Uuser,
                          station_level = "",
                          station_scode = "",
                          station_area = "",
                          management = ""
                      };
            var output = data.Take(num);

            return output.ToList();
        }


        /// <summary>
        /// 取得區配商或區配商自營的帳號
        /// </summary>
        /// <returns></returns>
        public List<TbAccount> GetSuppliersAccount()
        {
            var data = (from m in this.JunFuDbContext.TbAccounts
                        where (m.ManagerType.Equals("4") || m.ManagerType.Equals("5")) && m.ActiveFlag.Equals(true) && m.AccountType.Equals(false)
                        select m).ToList();

            return data;
        }

        public List<TbAccount> GetJFAccountsStartWith(string startWith, int num, string managerType = "1")
        {
            var data = from m in this.JunFuDbContext.TbAccounts
                       where (m.AccountType == false && m.ManagerType == managerType)
&& (m.AccountCode.StartsWith(startWith) || m.UserName.StartsWith(startWith))
                       orderby m.AccountCode
                       select m;

            var output = data.Take(num);

            return output.ToList();
        }

        public TbAccount GetById(decimal id)
        {
            var data = from tbAccount in JunFuDbContext.TbAccounts//.OrderByDescending(u => u.AccountId).Take(num).OrderBy(o => o.AccountCode)
                           where tbAccount.AccountId==id
                       select new TbAccount
                       {
                           AccountCode = tbAccount.AccountCode,
                           Password = tbAccount.Password,
                           AccountId = tbAccount.AccountId,
                           ActiveFlag = tbAccount.ActiveFlag,
                           BodyCode = tbAccount.BodyCode,
                           Cdate = tbAccount.Cdate,
                           Cuser = tbAccount.Cuser,
                           CustomerCode = tbAccount.CustomerCode,
                           EmpCode = tbAccount.EmpCode,
                           HeadCode = tbAccount.HeadCode,
                           JobTitle = tbAccount.JobTitle,
                           ManagerType = tbAccount.ManagerType,
                           ManagerUnit = tbAccount.ManagerUnit,
                           MasterCode = tbAccount.MasterCode,
                           PasswordChanged = tbAccount.PasswordChanged,
                           Udate = tbAccount.Udate,
                           UserEmail = tbAccount.UserEmail,
                           UserName = tbAccount.UserName,
                           Uuser = tbAccount.Uuser,
                           station_level = "",
                           station_scode = "",
                           station_area = "",
                           management = ""
                       };

           /*

            var data = this.JunFuDbContext.TbAccounts.Find(id);
           */
            return data.FirstOrDefault();
        }

        public TbAccount GetJunFuAccounts(string account, string password)
        {
            var data = from tbAccount in JunFuDbContext.TbAccounts
                       where tbAccount.AccountCode.Equals(account)
                       && tbAccount.Password.Equals(password)
                       && tbAccount.ManagerType != "5"
                       && tbAccount.ManagerUnit != "管理者"
                       && tbAccount.ManagerUnit != "一般員工"
                       && !tbAccount.ManagerUnit.Contains("全速配")
                       && !tbAccount.UserName.StartsWith("FSE")
                       && !tbAccount.UserName.EndsWith("主管")
                       && !tbAccount.AccountCode.Contains("skyeyes")
                       select tbAccount; ;

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }


    }
}
