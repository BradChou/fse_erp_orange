﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public interface IUserRoleMappingRepository
    {
        int Insert(UserRoleMapping userRoleMapping);

        void Update(UserRoleMapping userRoleMapping);

        List<UserRoleMapping> GetAllUserRoleMappingsByUserIdAndUserType(int userId, int userType);

        List<UserRoleMapping> GetAllUserRoleMappingsByRoleId(int roleId);

        List<UserRoleMapping> GetAllUserRoleMappings();

        UserRoleMapping GetUserRoleMapping(int id);

        void Delete(int id);

        void DeleteByUserIdAndAccountType(int userId, int accountType);

        void DeleteByRoleId(int roleId);
    }
}
