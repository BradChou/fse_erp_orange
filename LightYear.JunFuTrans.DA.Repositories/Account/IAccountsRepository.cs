﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.Account
{
    public interface IAccountsRepository
    {
        public TbAccount GetByAccountAndPassword(string account, string password);

        public TbAccount GetJunFuAccounts(string account, string password);

        public TbAccount GetByAccountCode(string account);

        public TbAccount GetById(decimal id);

        public List<TbAccount> GetAccountsStartWith(string startWith, int num);

        List<TbAccount> GetJFAccountsStartWith(string startWith, int num, string managerType = "1");

        public List<TbAccount> GetNumAccounts(int num);

        List<TbAccount> GetSuppliersAccount();

        List<TbAccount> GetJFNumAccounts(int num, string managerType = "1");
       

    }
}
