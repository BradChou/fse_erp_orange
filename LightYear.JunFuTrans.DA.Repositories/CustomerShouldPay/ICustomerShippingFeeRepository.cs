﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.CustomerShouldPay
{
    public interface ICustomerShippingFeeRepository
    {
        CustomerShippingFee GetCustomerShippingFeeByCustomerCode(string customerCode);

        IEnumerable<CustomerShippingFee> GetCustomerShippingFeeByCustomerCodes(IEnumerable<string> customerCode);

        IEnumerable<CustomerShippingFee> GetAll();

        IEnumerable<CustomerShippingFee> GetByStationCode(string stationCode);

        void UpdateShippingFee(CustomerShippingFee entity);

        List<TbCalendar> GetAllTbCalendars();

        CustomerShippingFee GetCustomerShippingFeeHistoryByCustomerCode(string customerCode);
    }
}
