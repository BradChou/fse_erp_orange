﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.PickUpRequest
{
    public interface IPickupRequestForApiuserRepository
    {
        PickupRequestForApiuser Insert(PickupRequestForApiuser pickupRequestForApiuser);

        List<PickUpInfoShowEntity> GetByTimePeriodAndStation(DateTime start, DateTime end, string stationCode);

        List<PickUpInfoShowEntity> GetByTimePeriod(DateTime start, DateTime end);

        List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCode(DateTime start, DateTime end, string customerCode);
        List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCodeAll(DateTime start, DateTime end, string customerCode);
        List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCodeAndStation(DateTime start, DateTime end, string customerCode, string stationCode);
        List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCodeAllAndStation(DateTime start, DateTime end, string customerCode, string stationCode);

    }
}
