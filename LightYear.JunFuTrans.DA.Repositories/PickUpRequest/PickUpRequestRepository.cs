﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.PickUpRequest
{
    public class PickUpRequestRepository : IPickUpRequestRepository
    {
        public JunFuDbContext JunFuDbContext { get; set; }

        public PickUpRequestRepository(JunFuDbContext dbContext)
        {
            JunFuDbContext = dbContext;
        }

        public IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriod(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd)  //篩選:時間、站所
        {
            supplierStationCode = "F" + supplierStationCode;
            return JunFuDbContext.PickUpRequestLogs.Where(p => p.Cdate >= requestTimeStart && p.Cdate < requestTimeEnd)
                    .Where(p => p.RequestCustomerCode.StartsWith(supplierStationCode));
        }

        public IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriod2(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd, string station_level, string station_area, string management,string station_scode)  //篩選:時間、站所
        {
            string managementList = "";
            string station_scodeList = "";
            string station_areaList = "";

            if (station_level.Equals("1"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.management == management
                          select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        managementList += "'";
                        managementList += item.StationScode;
                        managementList += "'";
                        managementList += ",";
                    }
                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                }
                var data = from pick in JunFuDbContext.PickUpRequestLogs
                           join customer in JunFuDbContext.TbCustomers
                           on pick.RequestCustomerCode equals (customer.CustomerCode)
                           where managementList.Contains(customer.station_scode)
                           && pick.Cdate >= requestTimeStart && pick.Cdate < requestTimeEnd
                           select new PickUpRequestLog
                           {
                               PickUpId = pick.PickUpId,
                               CustomerCode = pick.CustomerCode,
                               PickUpPieces = pick.PickUpPieces,
                               Remark = pick.Remark,
                               PackageType = pick.PackageType,
                               VechileType = pick.VechileType,
                               Cdate = pick.Cdate,
                               Udate = pick.Udate,
                               PickUpDate = pick.PickUpDate,
                               RequestCustomerCode = pick.RequestCustomerCode,
                               CheckNumber = pick.CheckNumber,
                               ReassignMd = pick.ReassignMd,
                               MdUuser = pick.MdUuser,

                           };
                return data.ToList();

            }
            else if (station_level.Equals("2"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.StationScode == station_scode
                          select new TbStation { StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_scodeList += "'";
                        station_scodeList += item.StationScode;
                        station_scodeList += "'";
                        station_scodeList += ",";
                    }
                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                }
                var data = from pick in JunFuDbContext.PickUpRequestLogs
                           join customer in JunFuDbContext.TbCustomers
                           on pick.RequestCustomerCode equals (customer.CustomerCode)
                           where station_scodeList.Contains(customer.station_scode)
                           && pick.Cdate >= requestTimeStart && pick.Cdate < requestTimeEnd
                           select new PickUpRequestLog
                           {
                               PickUpId = pick.PickUpId,
                               CustomerCode = pick.CustomerCode,
                               PickUpPieces = pick.PickUpPieces,
                               Remark = pick.Remark,
                               PackageType = pick.PackageType,
                               VechileType = pick.VechileType,
                               Cdate = pick.Cdate,
                               Udate = pick.Udate,
                               PickUpDate = pick.PickUpDate,
                               RequestCustomerCode = pick.RequestCustomerCode,
                               CheckNumber = pick.CheckNumber,
                               ReassignMd = pick.ReassignMd,
                               MdUuser = pick.MdUuser,

                           };
                return data.ToList();
            }
            else if (station_level.Equals("4"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.station_area == Int32.Parse(station_area)
                          select new TbStation { StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_areaList += "'";
                        station_areaList += item.StationScode;
                        station_areaList += "'";
                        station_areaList += ",";
                    }
                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                }
                var data = from pick in JunFuDbContext.PickUpRequestLogs
                           join customer in JunFuDbContext.TbCustomers
                           on pick.RequestCustomerCode equals (customer.CustomerCode)
                           where station_areaList.Contains(customer.station_scode)
                           && pick.Cdate >= requestTimeStart && pick.Cdate < requestTimeEnd
                           select new PickUpRequestLog
                           {
                               PickUpId = pick.PickUpId,
                               CustomerCode = pick.CustomerCode,
                               PickUpPieces = pick.PickUpPieces,
                               Remark = pick.Remark,
                               PackageType = pick.PackageType,
                               VechileType = pick.VechileType,
                               Cdate = pick.Cdate,
                               Udate = pick.Udate,
                               PickUpDate = pick.PickUpDate,
                               RequestCustomerCode = pick.RequestCustomerCode,
                               CheckNumber = pick.CheckNumber,
                               ReassignMd = pick.ReassignMd,
                               MdUuser = pick.MdUuser,

                           };
                return data.ToList();
            }
            else if (station_level.Equals("5"))
            {
                supplierStationCode = "F" + supplierStationCode;
                return JunFuDbContext.PickUpRequestLogs.Where(p => p.Cdate >= requestTimeStart && p.Cdate < requestTimeEnd)
                        .Where(p => p.RequestCustomerCode.StartsWith(supplierStationCode));
            }
            else
            {

                supplierStationCode = "F" + supplierStationCode;
                return JunFuDbContext.PickUpRequestLogs.Where(p => p.Cdate >= requestTimeStart && p.Cdate < requestTimeEnd)
                        .Where(p => p.RequestCustomerCode.StartsWith(supplierStationCode));
            }
        }

        public IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriodAndCustomerCode(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd, string customerCode) //篩選:時間、客代
        {
            return GetPickUpRequestByRequestTimePeriod(requestTimeStart, requestTimeEnd).Where(x => x.RequestCustomerCode == customerCode);

        }

        public IEnumerable<PickUpRequestLog> GetPickUpRequestByRequestTimePeriod(DateTime requestTimeStart, DateTime requestTimeEnd) //篩選:時間
        {
            return JunFuDbContext.PickUpRequestLogs.Where(p => p.Cdate >= requestTimeStart && p.Cdate < requestTimeEnd);
        }

        public IEnumerable<PickUpRequestLog> GetPickUpRequestByRequestTimePeriod2(DateTime requestTimeStart, DateTime requestTimeEnd, string station_level, string station_area, string management) //篩選:時間
        {
            return JunFuDbContext.PickUpRequestLogs.Where(p => p.Cdate >= requestTimeStart && p.Cdate < requestTimeEnd);
        }

        public long Insert(PickUpRequestLog pickUpRequestLog)
        {
            pickUpRequestLog.Cdate = DateTime.Now;

            pickUpRequestLog.Udate = DateTime.Now;

            JunFuDbContext.PickUpRequestLogs.Add(pickUpRequestLog);

            JunFuDbContext.SaveChanges();

            return pickUpRequestLog.PickUpId;
        }

        public IEnumerable<TbItemCode> GetTbItemCodesAllowToEdit()
        {
            //return JunFuDbContext.TbItemCodes.Where(x => x.CodeSclass == "RO" && new string[4] { "16", "21", "18", "19" }.Contains(x.CodeId));     //此四個號碼是議題#789指定
            string[] itemCodesArray = new string[9] { "16", "6", "12", "18", "8", "7", "9", "22", "23" }; //此八個號碼是議題#816指定
            TbItemCode[] result = new TbItemCode[9];
            for (var i = 0; i < itemCodesArray.Length; i++)
            {
                result[i] = JunFuDbContext.TbItemCodes.Where(x => x.CodeSclass == "RO" && x.CodeId == itemCodesArray[i]).FirstOrDefault();
            }
            return result;
        }

        public bool InsertIntoScanLogRo(IEnumerable<TtDeliveryScanLog> scanLogs)
        {
            JunFuDbContext.TtDeliveryScanLogs.AddRange(scanLogs);
            JunFuDbContext.SaveChanges();
            return true;
        }

        public bool UpdateDeliveryRequests(IEnumerable<string> checkNumbers, string accountCode)
        {
            IEnumerable<TcDeliveryRequest> requests = JunFuDbContext.TcDeliveryRequests.Where(x => checkNumbers.Contains(x.CheckNumber));
            foreach (var r in requests)
            {
                r.OrangeR11Uuser = accountCode;
            }
            JunFuDbContext.TcDeliveryRequests.AttachRange(requests);
            JunFuDbContext.SaveChanges();
            return true;
        }

        public bool UpdatePickupRequest(PickUpRequestLog log)
        {
            var dataInDb = JunFuDbContext.PickUpRequestLogs.FirstOrDefault(p => p.PickUpId == log.PickUpId);

            dataInDb.ReassignMd = log.ReassignMd;
            dataInDb.MdUuser = log.MdUuser;

            JunFuDbContext.SaveChanges();
            return true;
        }

        public bool UpdatePickupRequestForApi(PickupRequestForApiuser log)
        {
            var dataInDb = JunFuDbContext.PickupRequestForApiusers.FirstOrDefault(p => p.Id == log.Id);

            dataInDb.ReassignMd = log.ReassignMd;
            dataInDb.MdUuser = log.MdUuser;

            JunFuDbContext.SaveChanges();
            return true;
        }

        public IEnumerable<TtDeliveryScanLog> GetPickUpInfoByCheckNumber(IEnumerable<string> checkNumbers)
        {
            return JunFuDbContext.TtDeliveryScanLogs.Where(x => checkNumbers.Contains(x.CheckNumber) && x.ScanItem == "5");
        }

        public IEnumerable<TtDeliveryScanLog> GetPickUpInfoByCheckNumber2(string checkNumber)
        {
            return JunFuDbContext.TtDeliveryScanLogs.Where(x => checkNumber.Contains(x.CheckNumber) && x.ScanItem == "5");
        }

        public IEnumerable<TtDeliveryScanLog> SelectCheckNumber(string checkNumbers)
        {
            return JunFuDbContext.TtDeliveryScanLogs.Where(x => checkNumbers.Contains(x.CheckNumber) && x.ScanItem == "5");
        }

        public TbStation[] GetAllStations()
        {




            return JunFuDbContext.TbStations.ToArray();
        }

        public TtDeliveryScanLog Check_checkNumber(string check_number)
        {
            var dataInDb = JunFuDbContext.TtDeliveryScanLogs.OrderByDescending(e => e.Cdate).FirstOrDefault(p => p.CheckNumber == check_number && p.ScanItem == "5");

            JunFuDbContext.SaveChanges();
            return dataInDb;
        }
    }
}
