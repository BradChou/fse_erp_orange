﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.PickUpRequest
{
    public interface IPickUpRequestRepository
    {
        /// <summary>
        /// 從發送站跟客戶送出"派員收件"的時間
        /// </summary>
        /// <param name="supplierStationCode"></param>
        /// <param name="requestTimeStart"></param>
        /// <param name="requestTimeEnd"></param>
        /// <returns></returns>
        IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriod(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd);

        IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriod2(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd, string station_level, string station_area, string management,string station_scode);


        IEnumerable<PickUpRequestLog> GetPickUpRequestByStationAndRequestTimePeriodAndCustomerCode(string supplierStationCode, DateTime requestTimeStart, DateTime requestTimeEnd, string customerCode);

        IEnumerable<PickUpRequestLog> GetPickUpRequestByRequestTimePeriod(DateTime requestTimeStart, DateTime requestTimeEnd);

        Int64 Insert(PickUpRequestLog pickUpRequestLog);

        IEnumerable<TbItemCode> GetTbItemCodesAllowToEdit();

        bool InsertIntoScanLogRo(IEnumerable<TtDeliveryScanLog> scanLogs);

        bool UpdateDeliveryRequests(IEnumerable<string> requests, string accountCode);

        bool UpdatePickupRequest(PickUpRequestLog log);

        bool UpdatePickupRequestForApi(PickupRequestForApiuser log);

        TtDeliveryScanLog Check_checkNumber(string check_number);

        IEnumerable<TtDeliveryScanLog> GetPickUpInfoByCheckNumber(IEnumerable<string> checkNumbers);

        IEnumerable<TtDeliveryScanLog> GetPickUpInfoByCheckNumber2(string checkNumbers);

        TbStation[] GetAllStations();
    }
}
