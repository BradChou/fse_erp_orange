﻿using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;

namespace LightYear.JunFuTrans.DA.Repositories.PickUpRequest
{
    public class PickupRequestForApiuserRepository : IPickupRequestForApiuserRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public PickupRequestForApiuserRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;

        }

        public PickupRequestForApiuser Insert(PickupRequestForApiuser pickupRequestForApiuser)
        {
            JunFuDbContext.PickupRequestForApiusers.Add(pickupRequestForApiuser);

            JunFuDbContext.SaveChanges();

            return pickupRequestForApiuser;
        }

        public List<PickUpInfoShowEntity> GetByTimePeriodAndStation(DateTime start, DateTime end, string stationCode)
        {
            var s = (from pra in JunFuDbContext.PickupRequestForApiusers
                     join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
                     join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
                     where dr.SendStationScode ==stationCode && pra.RequestDate >= start && pra.RequestDate <= end && dr.CancelDate == null
                     && dr.DeliveryType != "R"
                     select new 
                     {
                         ReassignMd = pra.ReassignMd,
                         PickupRequestId = pra.Id,
                         RequestId = Convert.ToInt32(dr.RequestId),
                         NotifyDate = pra.RequestDate??new DateTime(),
                         CustomerCode = pra.CustomerCode,
                         CheckNumber = pra.CheckNumber,
                         AssignedMd = pra.Md,
                         AssignedSd = pra.Sd,
                         ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                         PutOrder = pra.Putorder,
                         PickUpStation = pra.SupplierCode,
                         ArriveStation = ts.StationName,
                         ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress,
                         CBM = (dr.CbmLength).ToString() + '*' + (dr.CbmWidth).ToString() + '*' + (dr.CbmHeight).ToString()
                     }
                    ).Distinct().ToList();
            var firstRequest = (from origin in s
                                group origin by origin.CheckNumber into g
                                select new
                                {
                                    CheckNumber = g.Key,
                                    NotifyDate = g.Min(r => r.NotifyDate),

                                }).Distinct().ToList();


            var finalReturn = (from origin in s
                               join f in firstRequest on new { origin.CheckNumber, NotifyDate = origin.NotifyDate } equals new { f.CheckNumber, NotifyDate = f.NotifyDate }
                               select new PickUpInfoShowEntity
                               {
                                   ReassignMd = origin.ReassignMd,
                                   PickupRequestId = origin.PickupRequestId,
                                   RequestId = origin.RequestId,
                                   NotifyDate = f.NotifyDate,
                                   CustomerCode = origin.CustomerCode,
                                   CheckNumber = f.CheckNumber,
                                   AssignedMd = origin.AssignedMd,
                                   AssignedSd = origin.AssignedSd,
                                   ShouldPickUpPieces = origin.ShouldPickUpPieces,
                                   PutOrder = origin.PutOrder,
                                   PickUpStation = origin.PickUpStation,
                                   ArriveStation = origin.ArriveStation,
                                   ReceiveAddress = origin.ReceiveAddress,
                                   CBM = origin.CBM
                               }).Distinct().ToList();

            return finalReturn;

        }
        public List<PickUpInfoShowEntity> GetByTimePeriodAndStationAndCustomerCodeAll(DateTime start, DateTime end, string stationCode)
        {
            var s = (from pra in JunFuDbContext.PickupRequestForApiusers
                     join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
                     join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
                     where dr.SendStationScode == stationCode && pra.RequestDate >= start && pra.RequestDate <= end && dr.CancelDate == null
                     select new
                     {
                         ReassignMd = pra.ReassignMd,
                         PickupRequestId = pra.Id,
                         RequestId = Convert.ToInt32(dr.RequestId),
                         NotifyDate = pra.RequestDate ?? new DateTime(),
                         CustomerCode = pra.CustomerCode,
                         CheckNumber = pra.CheckNumber,
                         AssignedMd = pra.Md,
                         AssignedSd = pra.Sd,
                         ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                         PutOrder = pra.Putorder,
                         PickUpStation = pra.SupplierCode,
                         ArriveStation = ts.StationName,
                         ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress,
                         CBM = (dr.CbmLength).ToString() + '*' + (dr.CbmWidth).ToString() + '*' + (dr.CbmHeight).ToString()
                     }
                    ).Distinct().ToList();
            var firstRequest = (from origin in s
                                group origin by origin.CheckNumber into g
                                select new
                                {
                                    CheckNumber = g.Key,
                                    NotifyDate = g.Min(r => r.NotifyDate),

                                }).Distinct().ToList();


            var finalReturn = (from origin in s
                               join f in firstRequest on new { origin.CheckNumber, NotifyDate = origin.NotifyDate } equals new { f.CheckNumber, NotifyDate = f.NotifyDate }
                               select new PickUpInfoShowEntity
                               {
                                   ReassignMd = origin.ReassignMd,
                                   PickupRequestId = origin.PickupRequestId,
                                   RequestId = origin.RequestId,
                                   NotifyDate = f.NotifyDate,
                                   CustomerCode = origin.CustomerCode,
                                   CheckNumber = f.CheckNumber,
                                   AssignedMd = origin.AssignedMd,
                                   AssignedSd = origin.AssignedSd,
                                   ShouldPickUpPieces = origin.ShouldPickUpPieces,
                                   PutOrder = origin.PutOrder,
                                   PickUpStation = origin.PickUpStation,
                                   ArriveStation = origin.ArriveStation,
                                   ReceiveAddress = origin.ReceiveAddress,
                                   CBM = origin.CBM
                               }).Distinct().ToList();

            return finalReturn;

        }

        public List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCode(DateTime start, DateTime end, string customerCode)
        {
            var s= (from pra in JunFuDbContext.PickupRequestForApiusers
                     join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
                     join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
                     //join prb in JunFuDbContext.PickUpRequestLogs on pra.CheckNumber equals prb.CheckNumber
                     where pra.CustomerCode == customerCode && pra.RequestDate >= start && pra.RequestDate <= end && dr.CancelDate == null
                     && dr.DeliveryType != "R"
                     select new 
                     {
                         ReassignMd = pra.ReassignMd,
                         PickupRequestId = pra.Id,
                         RequestId = Convert.ToInt32(dr.RequestId),
                         NotifyDate = pra.RequestDate ?? new DateTime(),
                         CustomerCode = pra.CustomerCode,
                         CheckNumber = pra.CheckNumber,
                         AssignedMd = pra.Md,
                         AssignedSd = pra.Sd,
                         ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                         PutOrder = pra.Putorder,
                         PickUpStation = pra.SupplierCode,
                         ArriveStation = ts.StationName,
                         ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress,
                         Size = Convert.ToInt32(dr.CbmHeight) + Convert.ToInt32(dr.CbmLength) + Convert.ToInt32(dr.CbmWidth)

                     }
                    ).Distinct().ToList();


            var firstRequest = (from origin in s
                                group origin by origin.CheckNumber into g
                                select new
                                {
                                    CheckNumber = g.Key,
                                    NotifyDate = g.Min(r => r.NotifyDate),

                                }).Distinct().ToList();


            var finalReturn = (from origin in s
                               join f in firstRequest on new { origin.CheckNumber, NotifyDate = origin.NotifyDate } equals new { f.CheckNumber, NotifyDate = f.NotifyDate }
                               select new PickUpInfoShowEntity
                               {
                                   ReassignMd = origin.ReassignMd,
                                   PickupRequestId = origin.PickupRequestId,
                                   RequestId = origin.RequestId,
                                   NotifyDate = f.NotifyDate,
                                   CustomerCode = origin.CustomerCode,
                                   CheckNumber = f.CheckNumber,
                                   AssignedMd = origin.AssignedMd,
                                   AssignedSd = origin.AssignedSd,
                                   ShouldPickUpPieces = origin.ShouldPickUpPieces,
                                   PutOrder = origin.PutOrder,
                                   PickUpStation = origin.PickUpStation,
                                   ArriveStation = origin.ArriveStation,
                                   ReceiveAddress = origin.ReceiveAddress,
                                   Size = origin.Size
                               }).Distinct().ToList();

            return finalReturn;

        }

        public List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCodeAll(DateTime start, DateTime end, string customerCode)
        {
            var s = (from pra in JunFuDbContext.PickupRequestForApiusers
                     join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
                     join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
                     //join prb in JunFuDbContext.PickUpRequestLogs on pra.CheckNumber equals prb.CheckNumber
                     where  pra.RequestDate >= start && pra.RequestDate <= end && dr.CancelDate == null
                     && dr.DeliveryType != "R"
                     select new
                     {
                         ReassignMd = pra.ReassignMd,
                         PickupRequestId = pra.Id,
                         RequestId = Convert.ToInt32(dr.RequestId),
                         NotifyDate = pra.RequestDate ?? new DateTime(),
                         CustomerCode = pra.CustomerCode,
                         CheckNumber = pra.CheckNumber,
                         AssignedMd = pra.Md,
                         AssignedSd = pra.Sd,
                         ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                         PutOrder = pra.Putorder,
                         PickUpStation = pra.SupplierCode,
                         ArriveStation = ts.StationName,
                         ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress,
                         Size = Convert.ToInt32(dr.CbmHeight) + Convert.ToInt32(dr.CbmLength) + Convert.ToInt32(dr.CbmWidth)

                     }
                    ).Distinct().ToList();


            var firstRequest = (from origin in s
                                group origin by origin.CheckNumber into g
                                select new
                                {
                                    CheckNumber = g.Key,
                                    NotifyDate = g.Min(r => r.NotifyDate),

                                }).Distinct().ToList();


            var finalReturn = (from origin in s
                               join f in firstRequest on new { origin.CheckNumber, NotifyDate = origin.NotifyDate } equals new { f.CheckNumber, NotifyDate = f.NotifyDate }
                               select new PickUpInfoShowEntity
                               {
                                   ReassignMd = origin.ReassignMd,
                                   PickupRequestId = origin.PickupRequestId,
                                   RequestId = origin.RequestId,
                                   NotifyDate = f.NotifyDate,
                                   CustomerCode = origin.CustomerCode,
                                   CheckNumber = f.CheckNumber,
                                   AssignedMd = origin.AssignedMd,
                                   AssignedSd = origin.AssignedSd,
                                   ShouldPickUpPieces = origin.ShouldPickUpPieces,
                                   PutOrder = origin.PutOrder,
                                   PickUpStation = origin.PickUpStation,
                                   ArriveStation = origin.ArriveStation,
                                   ReceiveAddress = origin.ReceiveAddress,
                                   Size = origin.Size
                               }).Distinct().ToList();

            return finalReturn;

        }




        public List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCodeAndStation(DateTime start, DateTime end, string customerCode, string stationCode)
        {
            var s = (from pra in JunFuDbContext.PickupRequestForApiusers
                     join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
                     join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
                     where pra.CustomerCode == customerCode && pra.RequestDate >= start && pra.RequestDate <= end && dr.CancelDate == null &&
                     dr.SendStationScode == stationCode
                     && dr.DeliveryType !="R"
                     select new
                     {
                         ReassignMd = pra.ReassignMd,
                         PickupRequestId = pra.Id,
                         RequestId = Convert.ToInt32(dr.RequestId),
                         NotifyDate = pra.RequestDate ?? new DateTime(),
                         CustomerCode = pra.CustomerCode,
                         CheckNumber = pra.CheckNumber,
                         AssignedMd = pra.Md,
                         AssignedSd = pra.Sd,
                         ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                         PutOrder = pra.Putorder,
                         PickUpStation = pra.SupplierCode,
                         ArriveStation = ts.StationName,
                         ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress,
                         Size = Convert.ToInt32(dr.CbmHeight) + Convert.ToInt32(dr.CbmLength)+Convert.ToInt32(dr.CbmWidth)

                         //ReassignMd = pra.ReassignMd,
                         //PickupRequestId = pra.Id,
                         //RequestId = Convert.ToInt32(dr.RequestId),
                         //NotifyDate = pra.RequestDate ?? new DateTime(),
                         //CustomerCode = pra.CustomerCode,
                         //CheckNumber = pra.CheckNumber,
                         //AssignedMd = pra.Md,
                         //AssignedSd = pra.Sd,
                         //ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                         //PutOrder = pra.Putorder,
                         //PickUpStation = pra.SupplierCode,
                         //ArriveStation = ts.StationName,
                         //ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress,
                         //Size = Convert.ToInt32(dr.CbmHeight) + Convert.ToInt32(dr.CbmLength)+Convert.ToInt32(dr.CbmWidth)
                     }
                    ).Distinct().ToList();
            var firstRequest = (from origin in s
                                group origin by origin.CheckNumber into g
                                select new
                                {
                                    CheckNumber = g.Key,
                                    NotifyDate = g.Min(r => r.NotifyDate),

                                }).Distinct().ToList();


            var finalReturn = (from origin in s
                               join f in firstRequest on new { origin.CheckNumber, NotifyDate = origin.NotifyDate } equals new { f.CheckNumber, NotifyDate = f.NotifyDate }
                               select new PickUpInfoShowEntity
                               {
                                   ReassignMd = origin.ReassignMd,
                                   PickupRequestId = origin.PickupRequestId,
                                   RequestId = origin.RequestId,
                                   NotifyDate = f.NotifyDate,
                                   CustomerCode = origin.CustomerCode,
                                   CheckNumber = f.CheckNumber,
                                   AssignedMd = origin.AssignedMd,
                                   AssignedSd = origin.AssignedSd,
                                   ShouldPickUpPieces = origin.ShouldPickUpPieces,
                                   PutOrder = origin.PutOrder,
                                   PickUpStation = origin.PickUpStation,
                                   ArriveStation = origin.ArriveStation,
                                   ReceiveAddress = origin.ReceiveAddress,
                                   Size = origin.Size
                               }).Distinct().ToList();





            return finalReturn;
        }

        public List<PickUpInfoShowEntity> GetByTimePeriodAndCustomerCodeAllAndStation(DateTime start, DateTime end, string customerCode, string stationCode)
        {
            var s = (from pra in JunFuDbContext.PickupRequestForApiusers
                     join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
                     join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
                     where  pra.RequestDate >= start && pra.RequestDate <= end && dr.CancelDate == null &&
                    dr.SendStationScode == stationCode
                     && dr.DeliveryType != "R"
                     select new
                     {
                         ReassignMd = pra.ReassignMd,
                         PickupRequestId = pra.Id,
                         RequestId = Convert.ToInt32(dr.RequestId),
                         NotifyDate = pra.RequestDate ?? new DateTime(),
                         CustomerCode = pra.CustomerCode,
                         CheckNumber = pra.CheckNumber,
                         AssignedMd = pra.Md,
                         AssignedSd = pra.Sd,
                         ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                         PutOrder = pra.Putorder,
                         PickUpStation = pra.SupplierCode,
                         ArriveStation = ts.StationName,
                         ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress,
                         Size = Convert.ToInt32(dr.CbmHeight) + Convert.ToInt32(dr.CbmLength) + Convert.ToInt32(dr.CbmWidth)

                         //ReassignMd = pra.ReassignMd,
                         //PickupRequestId = pra.Id,
                         //RequestId = Convert.ToInt32(dr.RequestId),
                         //NotifyDate = pra.RequestDate ?? new DateTime(),
                         //CustomerCode = pra.CustomerCode,
                         //CheckNumber = pra.CheckNumber,
                         //AssignedMd = pra.Md,
                         //AssignedSd = pra.Sd,
                         //ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                         //PutOrder = pra.Putorder,
                         //PickUpStation = pra.SupplierCode,
                         //ArriveStation = ts.StationName,
                         //ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress,
                         //Size = Convert.ToInt32(dr.CbmHeight) + Convert.ToInt32(dr.CbmLength)+Convert.ToInt32(dr.CbmWidth)
                     }
                    ).Distinct().ToList();
            var firstRequest = (from origin in s
                                group origin by origin.CheckNumber into g
                                select new
                                {
                                    CheckNumber = g.Key,
                                    NotifyDate = g.Min(r => r.NotifyDate),

                                }).Distinct().ToList();


            var finalReturn = (from origin in s
                               join f in firstRequest on new { origin.CheckNumber, NotifyDate = origin.NotifyDate } equals new { f.CheckNumber, NotifyDate = f.NotifyDate }
                               select new PickUpInfoShowEntity
                               {
                                   ReassignMd = origin.ReassignMd,
                                   PickupRequestId = origin.PickupRequestId,
                                   RequestId = origin.RequestId,
                                   NotifyDate = f.NotifyDate,
                                   CustomerCode = origin.CustomerCode,
                                   CheckNumber = f.CheckNumber,
                                   AssignedMd = origin.AssignedMd,
                                   AssignedSd = origin.AssignedSd,
                                   ShouldPickUpPieces = origin.ShouldPickUpPieces,
                                   PutOrder = origin.PutOrder,
                                   PickUpStation = origin.PickUpStation,
                                   ArriveStation = origin.ArriveStation,
                                   ReceiveAddress = origin.ReceiveAddress,
                                   Size = origin.Size
                               }).Distinct().ToList();

            return finalReturn;
        }

        public List<PickUpInfoShowEntity> GetByTimePeriod(DateTime start, DateTime end)
        {
            var s =
            (from pra in JunFuDbContext.PickupRequestForApiusers
             join dr in JunFuDbContext.TcDeliveryRequests on pra.CheckNumber equals dr.CheckNumber
             join ts in JunFuDbContext.TbStations on dr.AreaArriveCode equals ts.StationScode
             where pra.RequestDate >= start && pra.RequestDate <= end
             && dr.DeliveryType != "R"
             select new
             {
                 ReassignMd = pra.ReassignMd,
                 PickupRequestId = pra.Id,
                 RequestId = Convert.ToInt32(dr.RequestId),
                 NotifyDate = pra.RequestDate ?? new DateTime(),
                 CustomerCode = pra.CustomerCode,
                 CheckNumber = pra.CheckNumber,
                 AssignedMd = pra.Md,
                 AssignedSd = pra.Sd,
                 ShouldPickUpPieces = Convert.ToInt32(pra.Pieces),
                 PutOrder = pra.Putorder,
                 PickUpStation = pra.SupplierCode,
                 ArriveStation = ts.StationName,
                 ReceiveAddress = dr.ReceiveCity + dr.ReceiveArea + dr.ReceiveAddress,
                 Size = Convert.ToInt32(dr.CbmHeight) + Convert.ToInt32(dr.CbmLength) + Convert.ToInt32(dr.CbmWidth)

             }
             ).Distinct().ToList();

            var firstRequest = (from origin in s
                                group origin by origin.CheckNumber into g
                                select new
                                {
                                    CheckNumber = g.Key,
                                    NotifyDate = g.Min(r => r.NotifyDate),

                                }).Distinct().ToList();

            var finalReturn = (from origin in s
                               join f in firstRequest on new { origin.CheckNumber, NotifyDate = origin.NotifyDate } equals new { f.CheckNumber, NotifyDate = f.NotifyDate }
                               select new PickUpInfoShowEntity
                               {
                                   ReassignMd = origin.ReassignMd,
                                   PickupRequestId = origin.PickupRequestId,
                                   RequestId = origin.RequestId,
                                   NotifyDate = f.NotifyDate,
                                   CustomerCode = origin.CustomerCode,
                                   CheckNumber = f.CheckNumber,
                                   AssignedMd = origin.AssignedMd,
                                   AssignedSd = origin.AssignedSd,
                                   ShouldPickUpPieces = origin.ShouldPickUpPieces,
                                   PutOrder = origin.PutOrder,
                                   PickUpStation = origin.PickUpStation,
                                   ArriveStation = origin.ArriveStation,
                                   ReceiveAddress = origin.ReceiveAddress,
                                   Size = origin.Size
                               }).Distinct().ToList();


            return finalReturn;

        }
    }
}
