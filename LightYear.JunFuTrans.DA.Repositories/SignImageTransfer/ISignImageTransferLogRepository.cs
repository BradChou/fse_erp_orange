﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.SignImageTransfer
{
    public interface ISignImageTransferLogRepository
    {
        SignImageTransferLog Insert(SignImageTransferLog scheduleTask);
    }
}
