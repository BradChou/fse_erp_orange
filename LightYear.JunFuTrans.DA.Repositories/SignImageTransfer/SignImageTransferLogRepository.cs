﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.SignImageTransfer
{
    public class SignImageTransferLogRepository:ISignImageTransferLogRepository
    {

        public JunFuDbContext JunFuDbContext { get; private set; }
        public SignImageTransferLogRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }
        public SignImageTransferLog Insert(SignImageTransferLog log)
        {
            log.cdate = DateTime.Now;

            JunFuDbContext.SignImageTransferLog.Add(log);

            JunFuDbContext.SaveChanges();

            return log;
        }
    }
}
