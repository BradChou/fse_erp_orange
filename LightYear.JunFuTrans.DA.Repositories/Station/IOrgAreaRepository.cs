﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.Station
{
    public interface IOrgAreaRepository
    {
        public void Insert(OrgArea orgArea);
        public void Update(OrgArea orgArea);
        public void Delete(OrgArea orgArea);

        public void BatchDelete(OrgArea orgArea);

        public List<OrgArea> GetByFSEAddressEntity(FSEAddressEntity fSEAddressEntity);

        public List<OrgArea> GetByStationSCode(string stationSCode,string station_level,string managment,string station_area,string station_scode);

        public void BatchUpdateSD(List<int> ids, string sdValue);

        public void BatchUpdateMD(List<int> ids, string mdValue);

        public void BatchUpdatePutOrder(List<int> ids, string poValue);
        public void BatchUpdateShuttleStationCode(List<int> ids, string sscValue);
        public void BatchUpdateSendSD(List<int> ids, string ssdValue);
        public void BatchUpdateSendMD(List<int> ids, string smdValue);

        public List<CityAreaEntity> GetCityAreas(string road);
        void BatchDelete(int[] intAryIds);

        void InsertBatch(List<JunFuTransDb.OrgArea> Entities);

        List<JunFuTransDb.OrgArea> IsInsertDataOrNot(List<string> city, List<string> area, List<string> road, List<string> scoop);


        List<TbStation> GetAll();
    }
}
