﻿using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.Station
{
    public class OrgAreaRepository : IOrgAreaRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public OrgAreaRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public List<OrgArea> GetByFSEAddressEntity(FSEAddressEntity fSEAddressEntity)
        {
            string postRoad = fSEAddressEntity.PostRoad;
            string postStreet = fSEAddressEntity.PostStreet;

            if (fSEAddressEntity.PostRoad != null && fSEAddressEntity.PostRoad.Length > 0 && fSEAddressEntity.PostRoad.Substring(fSEAddressEntity.PostRoad.Length - 1) == "里")
            {
                postRoad = fSEAddressEntity.PostRoad[0..^1];
            }

            var data = from orgArea in this.JunFuTransDbContext.OrgAreas where orgArea.City.Equals(fSEAddressEntity.City) && orgArea.Area.Equals(fSEAddressEntity.District) && (orgArea.Road.Equals(postRoad) || orgArea.Road.Equals(postStreet)) select orgArea;

            List<OrgArea> output = new List<OrgArea>();

            List<OrgArea> orgAreas = data.ToList();

            if (orgAreas.Count == 0)
            {
                var data1 = from orgArea in this.JunFuTransDbContext.OrgAreas where orgArea.City.Equals(fSEAddressEntity.City) && (orgArea.Road.Equals(fSEAddressEntity.PostRoad) || orgArea.Road.Equals(fSEAddressEntity.PostStreet)) select orgArea;

                orgAreas = data1.ToList();
            }

            if (orgAreas.Count == 0)
            {
                var data1 = from orgArea in this.JunFuTransDbContext.OrgAreas where orgArea.City.Equals(fSEAddressEntity.City) && (orgArea.Eroad.Equals(fSEAddressEntity.PinYinRoad) || orgArea.Eroad.Equals(fSEAddressEntity.PinYinStreet)) select orgArea;

                orgAreas = data1.ToList();
            }

            if (orgAreas.Count == 1)
            {
                output = orgAreas;
            }
            else if (orgAreas.Count > 1)
            {
                double findNo = 0;

                try
                {
                    findNo = Convert.ToDouble(fSEAddressEntity.FindNo);
                }
                catch
                {

                }

                if (findNo == 0)
                {
                    output.Add(orgAreas[0]);
                }
                else
                {
                    bool even = true;

                    if ((findNo % 2) == 1)
                    {
                        even = false;
                    }

                    if (even)
                    {
                        var eachData = (from aa in orgAreas
                                        where aa.NoBgn <= findNo && aa.NoEnd >= findNo && aa.Even == 2
                                        select aa).ToList();

                        if (eachData.Count == 0)
                        {
                            eachData = (from aa in orgAreas
                                        where aa.NoBgn <= findNo && aa.NoEnd >= findNo && aa.Even == 0
                                        select aa).ToList();
                        }

                        if (eachData.Count == 0)
                        {
                            eachData = (from aa in orgAreas
                                        where aa.NoBgn <= findNo && aa.NoEnd >= findNo
                                        select aa).ToList();
                        }

                        if (eachData.Count == 0)
                        {
                            output.Add(orgAreas[0]);
                        }
                        else
                        {
                            output.Add(eachData[0]);
                        }
                    }
                    else
                    {
                        var eachData = (from aa in orgAreas
                                        where aa.NoBgn <= findNo && aa.NoEnd >= findNo && aa.Even == 1
                                        select aa).ToList();

                        if (eachData.Count == 0)
                        {
                            eachData = (from aa in orgAreas
                                        where aa.NoBgn <= findNo && aa.NoEnd >= findNo && aa.Even == 0
                                        select aa).ToList();
                        }

                        if (eachData.Count == 0)
                        {
                            eachData = (from aa in orgAreas
                                        where aa.NoBgn <= findNo && aa.NoEnd >= findNo
                                        select aa).ToList();
                        }

                        if (eachData.Count == 0)
                        {
                            output.Add(orgAreas[0]);
                        }
                        else
                        {
                            output.Add(eachData[0]);
                        }
                    }

                }
            }

            return output;
        }

        public List<OrgArea> GetByStationSCode(string stationSCode, string station_level, string managment, string station_area,string station_scode)
        {
            if (stationSCode == "-1" || stationSCode.Equals(null) || stationSCode.Equals("null") || stationSCode ==null)
            {

                string managementList = "";
                string station_scodeList = "";
                string station_areaList = "";

                if (station_level.Equals("1"))
                {
                    var da = (from tbstation in JunFuDbContext.TbStations
                              where tbstation.management == managment
                              select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                    if (da.Count() > 0)
                    {
                        foreach (var item in da)
                        {
                            managementList += "'";
                            managementList += item.StationScode;
                            managementList += "'";
                            managementList += ",";
                        }
                        managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                    }
                    var data = from orgArea in this.JunFuTransDbContext.OrgAreas where managementList.Contains(orgArea.StationScode) select orgArea;

                    List<OrgArea> orgAreas = data.ToList();

                    return orgAreas;

                }
                else if (station_level.Equals("2"))
                {
                    var da = (from tbstation in JunFuDbContext.TbStations
                              where tbstation.StationScode == station_scode
                              select new TbStation { StationScode = tbstation.StationScode });

                    if (da.Count() > 0)
                    {
                        foreach (var item in da)
                        {
                            station_scodeList += "'";
                            station_scodeList += item.StationScode;
                            station_scodeList += "'";
                            station_scodeList += ",";
                        }
                        station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                    }
                    var data = from orgArea in this.JunFuTransDbContext.OrgAreas where station_scodeList.Contains(orgArea.StationScode) select orgArea;

                    List<OrgArea> orgAreas = data.ToList();

                    return orgAreas;
                }
                else if (station_level.Equals("4"))
                {
                    var da = (from tbstation in JunFuDbContext.TbStations
                              where tbstation.station_area == Int32.Parse(station_area)
                              select new TbStation { station_area = tbstation.station_area, StationScode = tbstation.StationScode });

                    if (da.Count() > 0)
                    {
                        foreach (var item in da)
                        {
                            station_areaList += "'";
                            station_areaList += item.StationScode;
                            station_areaList += "'";
                            station_areaList += ",";
                        }
                        station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                    }
                    var data = from orgArea in this.JunFuTransDbContext.OrgAreas where station_areaList.Contains(orgArea.StationScode) select orgArea;

                    List<OrgArea> orgAreas = data.ToList();

                    return orgAreas;
                }
                else
                {
                    var data = from orgArea in this.JunFuTransDbContext.OrgAreas select orgArea;

                    List<OrgArea> orgAreas = data.ToList();

                    return orgAreas;
                }
                return null;
            }
            else
            {

                var data = from orgArea in this.JunFuTransDbContext.OrgAreas where orgArea.StationScode.Equals(stationSCode) select orgArea;

                List<OrgArea> orgAreas = data.ToList();

                return orgAreas;
            }
        }

        public void Insert(OrgArea orgArea)
        {
            this.JunFuTransDbContext.OrgAreas.Add(orgArea);
            this.JunFuTransDbContext.SaveChanges();

        }

        public void Update(OrgArea orgArea)
        {
            this.JunFuTransDbContext.OrgAreas.Attach(orgArea);

            var entry = this.JunFuTransDbContext.Entry(orgArea);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;

            //entry.Property(x => x.Office).IsModified = false;
            //entry.Property(x => x.Zip3A).IsModified = false;
            //entry.Property(x => x.Zipcode).IsModified = false;
            //entry.Property(x => x.City).IsModified = false;
            //entry.Property(x => x.Area).IsModified = false;
            entry.Property(x => x.Area1).IsModified = false;
            //entry.Property(x => x.Road).IsModified = false;
            //entry.Property(x => x.Scoop).IsModified = false;
            entry.Property(x => x.Even).IsModified = false;
            entry.Property(x => x.CmpLable).IsModified = false;
            entry.Property(x => x.Lane).IsModified = false;
            entry.Property(x => x.Lane1).IsModified = false;
            entry.Property(x => x.Alley).IsModified = false;
            entry.Property(x => x.Alley1).IsModified = false;
            entry.Property(x => x.NoBgn).IsModified = false;
            entry.Property(x => x.NoBgn1).IsModified = false;
            entry.Property(x => x.NoEnd).IsModified = false;
            entry.Property(x => x.NoEnd1).IsModified = false;
            entry.Property(x => x.Floor).IsModified = false;
            entry.Property(x => x.Floor1).IsModified = false;
            entry.Property(x => x.RoadNo).IsModified = false;
            entry.Property(x => x.Road1).IsModified = false;
            entry.Property(x => x.Eroad).IsModified = false;
            entry.Property(x => x.Rmk).IsModified = false;
            entry.Property(x => x.Rmk1).IsModified = false;
            entry.Property(x => x.Zip3Rmk).IsModified = false;
            entry.Property(x => x.Ecity).IsModified = false;
            entry.Property(x => x.Earea).IsModified = false;
            entry.Property(x => x.Uword).IsModified = false;
            entry.Property(x => x.Isn).IsModified = false;

            this.JunFuTransDbContext.SaveChanges();
        }

        public void Delete(OrgArea orgArea)
        {
            this.JunFuTransDbContext.OrgAreas.Attach(orgArea);
            this.JunFuTransDbContext.OrgAreas.Remove(orgArea);

            this.JunFuTransDbContext.SaveChanges();
        }

        public void BatchDelete(int[] ids)
        {
            var data = JunFuTransDbContext.OrgAreas.Where(t => ids.Contains(t.Id));

            JunFuTransDbContext.RemoveRange(data);

            JunFuTransDbContext.SaveChanges();
        }


        public void BatchUpdateSD(List<int> ids, string sdValue)
        {
            var orgAreas = this.JunFuTransDbContext.OrgAreas.Where(f => ids.Contains(f.Id)).ToList();

            orgAreas.ForEach(a => a.SdNo = sdValue);

            this.JunFuTransDbContext.SaveChanges();
        }

        public void BatchUpdateMD(List<int> ids, string mdValue)
        {
            var orgAreas = this.JunFuTransDbContext.OrgAreas.Where(f => ids.Contains(f.Id)).ToList();

            orgAreas.ForEach(a => a.MdNo = mdValue);

            this.JunFuTransDbContext.SaveChanges();
        }

        public void BatchUpdatePutOrder(List<int> ids, string poValue)
        {
            var orgAreas = this.JunFuTransDbContext.OrgAreas.Where(f => ids.Contains(f.Id)).ToList();

            orgAreas.ForEach(a => a.PutOrder = poValue);

            this.JunFuTransDbContext.SaveChanges();
        }
        public void BatchUpdateShuttleStationCode(List<int> ids, string sscValue)
        {
            var orgAreas = this.JunFuTransDbContext.OrgAreas.Where(f => ids.Contains(f.Id)).ToList();

            orgAreas.ForEach(a => a.ShuttleStationCode = sscValue);

            this.JunFuTransDbContext.SaveChanges();
        }

        public void BatchUpdateSendSD(List<int> ids, string ssdValue)
        {
            var orgAreas = this.JunFuTransDbContext.OrgAreas.Where(f => ids.Contains(f.Id)).ToList();

            orgAreas.ForEach(a => a.SendSD = ssdValue);

            this.JunFuTransDbContext.SaveChanges();
        }

        public void BatchUpdateSendMD(List<int> ids, string smdValue)
        {
            var orgAreas = this.JunFuTransDbContext.OrgAreas.Where(f => ids.Contains(f.Id)).ToList();

            orgAreas.ForEach(a => a.SendMD = smdValue);

            this.JunFuTransDbContext.SaveChanges();
        }

        public List<CityAreaEntity> GetCityAreas(string road)
        {
            if (road.Length > 0)
            {

                var data = (from orgArea in this.JunFuTransDbContext.OrgAreas
                            where orgArea.Road.StartsWith(road)
                            select new CityAreaEntity { City = orgArea.City, Area = orgArea.Area, Zip = orgArea.Zip3A }).Distinct().ToList();

                var output = (from t in data orderby t.Zip select t).ToList();

                return output;
            }
            else
            {
                return new List<CityAreaEntity>();
            }
        }

        public void BatchDelete(OrgArea orgArea)
        {
            throw new NotImplementedException();
        }
        public void InsertBatch(List<JunFuTransDb.OrgArea> Entities)
        {
            this.JunFuTransDbContext.OrgAreas.AddRange(Entities);

            this.JunFuTransDbContext.SaveChanges();
        }


        public List<JunFuTransDb.OrgArea> IsInsertDataOrNot(List<string> city, List<string> area, List<string> road, List<string> scoop)
        {
            var data = (from dp in JunFuTransDbContext.OrgAreas
                        where city.Contains(dp.City) && area.Contains(dp.Area) && road.Contains(dp.Road) && scoop.Contains(dp.Scoop)
                        select dp).ToList();
            if (data.Count > 0)
            {
                return data;
            }
            return null;
        }


        public List<TbStation> GetAll()                                         //站所依照區屬排序
        {
            var data = JunFuDbContext.TbStations.ToList();

            return data;
        }

    }
}
