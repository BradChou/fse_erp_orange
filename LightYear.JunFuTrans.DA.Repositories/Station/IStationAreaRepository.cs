﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;

namespace LightYear.JunFuTrans.DA.Repositories.Station
{
    public interface IStationAreaRepository
    {
        public StationArea GetStationAreaById(int id);

        public StationArea GetByStationScode(string stationScode);

        List<string> GetAreaList();

        List<StationArea> GetStationsByAreaName(string areaName);

        public int Insert(StationArea stationArea);

        public int Update(StationArea stationArea);

        public void Delete(int id);

        /// <summary>
        /// 回傳 code_id, 部門名稱
        /// </summary>
        /// <returns></returns>
        public List<JFDepartmentDropDownListEntity> GetJFDepartment();
    }
}
