﻿using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Station
{
    public class StationRepository : IStationRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public StationRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        /// <summary>
        /// 取得站所資料
        /// </summary>
        /// <param name="id">站所序號</param>
        /// <returns>站所</returns>
        public TbStation GetById(int id)
        {
            var data = from tbStation in this.JunFuDbContext.TbStations where tbStation.Id.Equals(id) select tbStation;

            List<TbStation> stations = data.ToList();

            if (stations.Count > 0)
            {
                return stations[0];
            }
            else
            {
                return null;
            }
        }

        public TbStation GetByScode(string scode)
        {
            return JunFuDbContext.TbStations.SingleOrDefault(s => s.StationScode == scode);
        }

        public List<TbStation> GetByLevel(string stationLevel, string stationArea, string management, string stationSCode)
        {
            

            if (stationLevel == "1")
            {
                var data = from s in this.JunFuDbContext.TbStations where (s.management.Equals(management)) orderby s.StationScode select s;
                return data.ToList();
            }
            else if (stationLevel == "2")
            {
                var data = from s in this.JunFuDbContext.TbStations where (s.StationScode.Equals(stationSCode)) orderby s.StationScode select s;
                return data.ToList();
                
            }
            else if (stationLevel == "4")
            {
                var data = from s in this.JunFuDbContext.TbStations where (s.station_area.Equals(stationArea)) orderby s.StationScode select s;
                return data.ToList();

            }
            else if (stationLevel == "5")
            {
                return JunFuDbContext.TbStations.ToList();
            }
            else
            {
                return JunFuDbContext.TbStations.ToList();
            }


        }

        public TbStation GetByStationCode(string stationCode)
        {
            return JunFuDbContext.TbStations.SingleOrDefault(s => s.StationCode == stationCode);
        }

        public List<TbStation> GetAll()                                         //站所依照區屬排序
        {
            var data = JunFuDbContext.TbStations.ToList();
            data.Sort((x, y) => x.BusinessDistrict is null || y.BusinessDistrict is null ? (x.BusinessDistrict is null ? "infinite".CompareTo(y.BusinessDistrict) : x.BusinessDistrict.CompareTo("infinite")) : x.BusinessDistrict.CompareTo(y.BusinessDistrict));
            return data;
        }

        public List<TbStation> GetAll2(string station_level, string station_area, string management, string station_scode,string station)                                         //站所依照區屬排序
        {
            string managementList = "";
            string station_scodeList = "";
            string station_areaList = "";
            if (station_level==null)
            {
                var data = JunFuDbContext.TbStations.ToList().Where(x => station.Contains(x.StationScode));
                //data.Sort((x, y) => x.BusinessDistrict is null || y.BusinessDistrict is null ? (x.BusinessDistrict is null ? "infinite".CompareTo(y.BusinessDistrict) : x.BusinessDistrict.CompareTo("infinite")) : x.BusinessDistrict.CompareTo(y.BusinessDistrict));
                return data.ToList();
            }
            else
            
            if (station_level.Equals("1"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.management == management
                          select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        managementList += "'";
                        managementList += item.StationScode;
                        managementList += "'";
                        managementList += ",";
                    }
                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                }


                var data =  JunFuDbContext.TbStations.ToList().Where(x=> managementList.Contains(x.StationScode));
                //data.Sort((x, y) => x.BusinessDistrict is null || y.BusinessDistrict is null ? (x.BusinessDistrict is null ? "infinite".CompareTo(y.BusinessDistrict) : x.BusinessDistrict.CompareTo("infinite")) : x.BusinessDistrict.CompareTo(y.BusinessDistrict));
                return data.ToList();

            }
            else if (station_level.Equals("2"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.StationScode == station_scode
                          select new TbStation { StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_scodeList += "'";
                        station_scodeList += item.StationScode;
                        station_scodeList += "'";
                        station_scodeList += ",";
                    }
                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                }

                var data = JunFuDbContext.TbStations.ToList().Where(x => station_scodeList.Contains(x.StationScode));
                //data.Sort((x, y) => x.BusinessDistrict is null || y.BusinessDistrict is null ? (x.BusinessDistrict is null ? "infinite".CompareTo(y.BusinessDistrict) : x.BusinessDistrict.CompareTo("infinite")) : x.BusinessDistrict.CompareTo(y.BusinessDistrict));
                return data.ToList();
            }
            else if (station_level.Equals("4"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.station_area == Int32.Parse(station_area)
                          select new TbStation { station_area = tbstation.station_area,StationScode=tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_areaList += "'";
                        station_areaList += item.StationScode;
                        station_areaList += "'";
                        station_areaList += ",";
                    }
                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                }

                var data = JunFuDbContext.TbStations.ToList().Where(x => station_areaList.Contains(x.StationScode));
                //data.Sort((x, y) => x.BusinessDistrict is null || y.BusinessDistrict is null ? (x.BusinessDistrict is null ? "infinite".CompareTo(y.BusinessDistrict) : x.BusinessDistrict.CompareTo("infinite")) : x.BusinessDistrict.CompareTo(y.BusinessDistrict));
                return data.ToList();
            }
            else if (station_level.Equals("5"))
            {
                var data = JunFuDbContext.TbStations.ToList();
                data.Sort((x, y) => x.BusinessDistrict is null || y.BusinessDistrict is null ? (x.BusinessDistrict is null ? "infinite".CompareTo(y.BusinessDistrict) : x.BusinessDistrict.CompareTo("infinite")) : x.BusinessDistrict.CompareTo(y.BusinessDistrict));
                return data;
            }
            else
            {
                var data = JunFuDbContext.TbStations.ToList();
                data.Sort((x, y) => x.BusinessDistrict is null || y.BusinessDistrict is null ? (x.BusinessDistrict is null ? "infinite".CompareTo(y.BusinessDistrict) : x.BusinessDistrict.CompareTo("infinite")) : x.BusinessDistrict.CompareTo(y.BusinessDistrict));
                return data;
            }
            return null;
        }

        public string GetStationNameByScode(string scode)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationScode == scode).Select(s => s.StationName).FirstOrDefault();
        }

        public string GetStationNameByStationCode(string stationCode)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationCode == stationCode).Select(s => s.StationName).FirstOrDefault();
        }
        public List<TbStation> GetListStationNameByListStationScode(List<string> stationScode)
        {
            var data = (from t in JunFuDbContext.TbStations
                        where stationScode.Contains(t.StationScode)
                        select new TbStation { StationName = t.StationName }).ToList();
            return data;
        }


        public string GetStationScodeByName(string stationName)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationName.Equals(stationName)).Select(s => s.StationScode).FirstOrDefault();
        }

        public string GetStationCodeByName(string stationName)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationName.Equals(stationName)).Select(s => s.StationCode).FirstOrDefault();
        }

        public List<TbStation> GetStartWith(string startWith)
        {
            var data = from s in this.JunFuDbContext.TbStations where (s.StationCode.StartsWith(startWith) || s.StationScode.StartsWith(startWith) || s.StationName.StartsWith(startWith)) orderby s.StationScode select s;
            return data.ToList();
        }

        public List<StationArea> GetAreaStation()
        {
            return JunFuTransDbContext.StationAreas.ToList();
        }

        public string GetStationSCodeByStationCode(string stationCode)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationCode.Equals(stationCode)).Select(s => s.StationScode).FirstOrDefault();
        }

        public string GetStationCodeByStationScode(string stationScode)
        {
            return JunFuDbContext.TbStations.Where(s => s.StationScode.Equals(stationScode)).Select(s => s.StationCode).FirstOrDefault();
        }

        public Dictionary<string, TbStation> GetTbStations(List<string> stationCodes)
        {
            var data = (from s in this.JunFuDbContext.TbStations where stationCodes.Contains(s.StationScode) select s).ToDictionary(p => p.StationScode);

            return data;
        }

        public Dictionary<string, TbStation> GetTbStationsByIds(List<string> ids)
        {
            var data = (from s in this.JunFuDbContext.TbStations where ids.Contains(s.Id.ToString()) select s).ToDictionary(p => p.Id.ToString());

            return data;
        }

        public IEnumerable<TbStation> GetStationsByScodes(IEnumerable<string> stationScodes)
        {
            return JunFuDbContext.TbStations.Where(s => stationScodes.Contains(s.StationScode));
        }

        public List<TbStation> GetStationByArea(string areaName)
        {
            return JunFuDbContext.TbStations.Where(s => s.BusinessDistrict.Equals(areaName)).ToList();
        }

        public List<string> GetAreaList()
        {
            return JunFuDbContext.TbStations.Select(s => s.BusinessDistrict).Distinct().ToList();
        }
    }
}
