﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;
using LightYear.JunFuTrans.BL.BE.SignPaperPhoto;
using Microsoft.Extensions.Configuration;

namespace LightYear.JunFuTrans.DA.Repositories.SignPaperPhotoPackage
{
    public class SignPaperPhotoPackageRepository : ISignPaperPhotoPackageRepository
    {
        public IJunFuDbContext JunFuDbContext { get; set; }
        public IConfiguration Configuration { get; set; }

        public SignPaperPhotoPackageRepository(JunFuDbContext junFuDbContext, IConfiguration configuration)
        {
            JunFuDbContext = junFuDbContext;
            Configuration = configuration;
        }

        public IEnumerable<SignPaperPhotoOutputEntity> GetRequestsByAccountCodeAndPrintDateAndShipDate(string customerCode, DateTime printDateStart, DateTime printDateEnd, DateTime shipDateStart, DateTime shipDateEnd)        //***測試看看效能會不會比原來的好
        {
            var data = from scanLog3 in JunFuDbContext.TtDeliveryScanLogs
                       join scanLog4 in JunFuDbContext.TtDeliveryScanLogs on scanLog3.CheckNumber equals scanLog4.CheckNumber into t
                       from scanLog4 in t.DefaultIfEmpty()
                       join checkPhoto in JunFuDbContext.CheckPhotoToS3File on scanLog4.CheckNumber equals checkPhoto.FileName into t2
                       from checkPhoto in t2.DefaultIfEmpty()
                       join request in JunFuDbContext.TcDeliveryRequests on checkPhoto.FileName equals request.CheckNumber into t3
                       from request in t3.DefaultIfEmpty()
                       where request.CustomerCode == customerCode && request.PrintDate >= printDateStart && request.PrintDate < printDateEnd.AddDays(1) && request.ShipDate >= shipDateStart && request.ShipDate < shipDateEnd.AddDays(1) && (scanLog3.ScanItem == "3" || scanLog4.ScanItem == "4")
                       orderby request.RequestId
                       select new SignPaperPhotoOutputEntity
                       {
                           Id = (int)request.RequestId,
                           CheckNumber = request.CheckNumber,
                           OrderNumber = request.OrderNumber,
                           ReceiveContact = request.ReceiveContact,
                           ReceiveAddress = request.ReceiveAddress,
                           PrintDate = request.PrintDate == null ? "" : ((DateTime)request.PrintDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           ShipDate = request.ShipDate == null ? "" : ((DateTime)request.ShipDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           SignPaperPath = checkPhoto.S3Path == null ? scanLog4.SignFormImage == null ? "" : Configuration.GetValue<string>("80Url") + scanLog4.SignFormImage : checkPhoto.S3Path.Replace(Configuration.GetValue<string>("S3SignPaperPhotoIP"), Configuration.GetValue<string>("S3Url")),
                           SignPaperString = checkPhoto.S3Path == null ? scanLog4.SignFormImage == null ? "" : "查看簽單" : "查看簽單",
                           ArriveDate = scanLog4.ScanDate == null ? "" : ((DateTime)scanLog3.ScanDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           SourceType = checkPhoto.S3Path == null ? scanLog4.SignFormImage == null ? 0 : 2 : 1       //請參考entity的備註欄位
                       };


            return data.Distinct();
        }

        public IEnumerable<SignPaperPhotoOutputEntity> GetRequestsByCustomerCodeAndShipDate(string customerCode, DateTime shipDateStart, DateTime shipDateEnd)
        {
            var data = from scanLog3 in JunFuDbContext.TtDeliveryScanLogs
                       join scanLog4 in JunFuDbContext.TtDeliveryScanLogs on scanLog3.CheckNumber equals scanLog4.CheckNumber into t
                       from scanLog4 in t.DefaultIfEmpty()
                       join checkPhoto in JunFuDbContext.CheckPhotoToS3File on scanLog4.CheckNumber equals checkPhoto.FileName into t2
                       from checkPhoto in t2.DefaultIfEmpty()
                       join request in JunFuDbContext.TcDeliveryRequests on checkPhoto.FileName equals request.CheckNumber into t3
                       from request in t3.DefaultIfEmpty()
                       where request.CustomerCode == customerCode && request.ShipDate >= shipDateStart && request.ShipDate < shipDateEnd.AddDays(1) && (scanLog3.ScanItem == "3" || scanLog4.ScanItem == "4")
                       orderby request.RequestId
                       select new SignPaperPhotoOutputEntity
                       {
                           Id = (int)request.RequestId,
                           CheckNumber = request.CheckNumber,
                           OrderNumber = request.OrderNumber,
                           ReceiveContact = request.ReceiveContact,
                           ReceiveAddress = request.ReceiveAddress,
                           PrintDate = request.PrintDate == null ? "" : ((DateTime)request.PrintDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           ShipDate = request.ShipDate == null ? "" : ((DateTime)request.ShipDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           SignPaperPath = checkPhoto.S3Path == null ? scanLog4.SignFormImage == null ? "" : Configuration.GetValue<string>("80Url") + scanLog4.SignFormImage : checkPhoto.S3Path.Replace(Configuration.GetValue<string>("S3SignPaperPhotoIP"), Configuration.GetValue<string>("S3Url")),
                           SignPaperString = checkPhoto.S3Path == null ? scanLog4.SignFormImage == null ? "" : "查看簽單" : "查看簽單",
                           ArriveDate = scanLog4.ScanDate == null ? "" : ((DateTime)scanLog3.ScanDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           SourceType = checkPhoto.S3Path == null ? scanLog4.SignFormImage == null ? 0 : 2 : 1       //請參考entity的備註欄位
                       };


            return data.Distinct();
        }

        public IEnumerable<SignPaperPhotoOutputEntity> GetRequestsByCustomerCodeAndPrintDate(string customerCode, DateTime printDateStart, DateTime printDateEnd)
        {
            var data = from scanLog3 in JunFuDbContext.TtDeliveryScanLogs
                       join scanLog4 in JunFuDbContext.TtDeliveryScanLogs on scanLog3.CheckNumber equals scanLog4.CheckNumber into t
                       from scanLog4 in t.DefaultIfEmpty()
                       join checkPhoto in JunFuDbContext.CheckPhotoToS3File on scanLog4.CheckNumber equals checkPhoto.FileName into t2
                       from checkPhoto in t2.DefaultIfEmpty()
                       join request in JunFuDbContext.TcDeliveryRequests on checkPhoto.FileName equals request.CheckNumber into t3
                       from request in t3.DefaultIfEmpty()
                       where request.CustomerCode == customerCode && request.PrintDate >= printDateStart && request.PrintDate < printDateEnd.AddDays(1) && (scanLog3.ScanItem == "3" || scanLog4.ScanItem == "4")
                       orderby request.RequestId
                       select new SignPaperPhotoOutputEntity
                       {
                           Id = (int)request.RequestId,
                           CheckNumber = request.CheckNumber,
                           OrderNumber = request.OrderNumber,
                           ReceiveContact = request.ReceiveContact,
                           ReceiveAddress = request.ReceiveAddress,
                           PrintDate = request.PrintDate == null ? "" : ((DateTime)request.PrintDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           ShipDate = request.ShipDate == null ? "" : ((DateTime)request.ShipDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           SignPaperPath = checkPhoto.S3Path == null ? scanLog4.SignFormImage == null ? "" : Configuration.GetValue<string>("80Url") + scanLog4.SignFormImage : checkPhoto.S3Path.Replace(Configuration.GetValue<string>("S3SignPaperPhotoIP"), Configuration.GetValue<string>("S3Url")),
                           SignPaperString = checkPhoto.S3Path == null ? scanLog4.SignFormImage == null ? "" : "查看簽單" : "查看簽單",
                           ArriveDate = scanLog4.ScanDate == null ? "" : ((DateTime)scanLog3.ScanDate).ToString("yyyy-MM-dd HH:mm:ss"),
                           SourceType = checkPhoto.S3Path == null ? scanLog4.SignFormImage == null ? 0 : 2 : 1       //請參考entity的備註欄位
                       };


            return data.Distinct();
        }


        public IEnumerable<CheckNumberPackageProcess> GetCheckNumberPackageProcesses(string loginAccountCode)
        {
            return JunFuDbContext.CheckNumberPackageProcesses.Where(c => c.AccountCode == loginAccountCode);
        }

        public IEnumerable<CheckNumberWithPhoto> GetCaterpillarPhotoFileName(IEnumerable<string> checkNumbers)
        {
            var data = from requests in JunFuDbContext.TcDeliveryRequests
                       join scanLogs in JunFuDbContext.TtDeliveryScanLogs on requests.CheckNumber equals scanLogs.CheckNumber
                       where checkNumbers.Contains(requests.CheckNumber) && scanLogs.ScanItem.Equals("3") && scanLogs.SignFormImage != null
                       select new CheckNumberWithPhoto
                       {
                           CheckNumber = requests.CheckNumber,
                           SignPhotoPath = scanLogs.SignFormImage
                       };

            return data;
        }

        public CheckPhotoToS3File GetTaiChungSchedulePhotoById(string checkNumber)
        {
            return JunFuDbContext.CheckPhotoToS3File.Where(c => c.FileName == checkNumber).FirstOrDefault();
        }
    }
}
