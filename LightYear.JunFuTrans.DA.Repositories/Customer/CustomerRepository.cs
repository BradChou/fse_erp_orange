﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.Customer
{
    public class CustomerRepository : ICustomerRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public CustomerRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public TbCustomer GetByCustomerCode(string customerCode)
        {
            var data = from TbCustomer in this.JunFuDbContext.TbCustomers where TbCustomer.CustomerCode.Equals(customerCode) select TbCustomer;

            if (data.Count() > 0)
            {
                return data.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public List<TbCustomer> GetAll()
        {
            return JunFuDbContext.TbCustomers.Distinct().ToList();
        }

        public List<TbCustomer> GetCustomersByStation(string stationCode)
        {
            return JunFuDbContext.TbCustomers.Where(c => c.CustomerCode.StartsWith(stationCode)).Distinct().ToList();
        }

        public string GetCustomerCodeByName(string customerName)
        {
            return JunFuDbContext.TbCustomers.Where(c => c.CustomerName.Equals(customerName)).Select(c => c.CustomerCode).FirstOrDefault();
        }

        public List<TbCustomer> GetByStationCodeAndStartWith(string stationCode, string startWith, int nums, string station_level, string station_area, string management, string station_scode)
        {
            var data = from s in this.JunFuDbContext.TbCustomers
                       where s.station_scode.Equals(stationCode) && (s.CustomerCode.StartsWith(startWith) || s.CustomerName.StartsWith(startWith))
&& s.CustomerCode.StartsWith("F") && !s.CustomerCode.StartsWith("F0")
                       orderby s.CustomerCode
                       select s;

            var output = data.Take(nums);

            return output.ToList();
        }

        public List<TbCustomer> GetStartWith(string startWith, int nums, string station_level, string station_area, string management, string station_scode)
        {
            string managementList = "";
            string station_scodeList = "";
            string station_areaList = "";

            if (station_level.Equals("1"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.management == management
                          select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        managementList += "'";
                        managementList += item.StationScode;
                        managementList += "'";
                        managementList += ",";
                    }
                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                }
                /*
                var data = from s in this.JunFuDbContext.TbCustomers
                           where (s.CustomerCode.StartsWith(startWith) || s.CustomerName.StartsWith(startWith))
&& s.CustomerCode.StartsWith("F") && !s.CustomerCode.StartsWith("F0")&&managementList.Contains(s.station_scode)
                           orderby s.CustomerCode
                           select s;
                */
                var data = from s in this.JunFuDbContext.TbCustomers
                           where managementList.Contains(s.station_scode)
                           orderby s.CustomerCode
                           select s;

                var output = data.Take(nums);

                return output.ToList();
            }
            else if (station_level.Equals("2"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.StationScode == station_scode
                          select new TbStation { StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_scodeList += "'";
                        station_scodeList += item.StationScode;
                        station_scodeList += "'";
                        station_scodeList += ",";
                    }
                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                }
                /*
                var data = from s in this.JunFuDbContext.TbCustomers
                           where (s.CustomerCode.StartsWith(startWith) || s.CustomerName.StartsWith(startWith))
&& s.CustomerCode.StartsWith("F") && !s.CustomerCode.StartsWith("F0")&&managementList.Contains(s.station_scode)
                           orderby s.CustomerCode
                           select s;
                */
                var data = from s in this.JunFuDbContext.TbCustomers
                           where station_scodeList.Contains(s.station_scode)
                           orderby s.CustomerCode
                           select s;

                var output = data.Take(nums);

                return output.ToList();
            }
            else if (station_level.Equals("4"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.station_area == Int32.Parse(station_area)
                          select new TbStation { station_area = tbstation.station_area, StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_areaList += "'";
                        station_areaList += item.StationScode;
                        station_areaList += "'";
                        station_areaList += ",";
                    }
                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                }
                var data = from s in this.JunFuDbContext.TbCustomers
                           where station_areaList.Contains(s.station_scode)
                           orderby s.CustomerCode
                           select s;

                var output = data.Take(nums);

                return output.ToList();

            }
            else if (station_level.Equals("5"))
            {
                var data = from s in this.JunFuDbContext.TbCustomers
                           where (s.CustomerCode.StartsWith(startWith) || s.CustomerName.StartsWith(startWith))
&& s.CustomerCode.StartsWith("F") && !s.CustomerCode.StartsWith("F0") 
                           orderby s.CustomerCode
                           select s;
                var output = data.Take(nums);

                return output.ToList();
            }
            else
            {
                var data = from s in this.JunFuDbContext.TbCustomers
                           where (s.CustomerCode.StartsWith(startWith) || s.CustomerName.StartsWith(startWith))
&& s.CustomerCode.StartsWith("F") && !s.CustomerCode.StartsWith("F0")
                           orderby s.CustomerCode
                           select s;
                var output = data.Take(nums);

                return output.ToList();
            }


        }

        public Dictionary<string, TbCustomer> GetCustomersByCustomerCodes(List<string> customerCodes)
        {
            var data = (from customer in this.JunFuDbContext.TbCustomers where customer.StopShippingCode.Equals("0") && customerCodes.Contains(customer.CustomerCode) select customer).ToLookup(p => p.CustomerCode).ToDictionary(p => p.Key.ToUpper(), p => p.First());

            return data;
        }

        public Dictionary<string, string> GetCustomerNameByCustomerCode(IEnumerable<string> customerCodes)
        {
            return JunFuDbContext.TbCustomers.Where(c => customerCodes.Contains(c.CustomerCode)).ToDictionary(c => c.CustomerCode, c => c.CustomerName);
        }
    }
}
