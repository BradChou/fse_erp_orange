﻿using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryAndDeliveryMating
{
    public class DeliveryAndDeliveryMatingRepository : IDeliveryAndDeliveryMatingRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }

        public DeliveryAndDeliveryMatingRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }

        public List<DeliveryAndDeliveryMatingEntity> GetAllByDriverCode(DateTime start, DateTime end, string driverCode, string arriveOption)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
            , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                var sdmdCodePre = (from tc in JunFuDbContext.JubFuAPPWebServiceLog.Where(a => a.type.Equals("getArriveSDMDCode"))
                                   select new
                                   {
                                       check_number = tc.message.Substring(17, 12),
                                       sdmd_code = tc.message.Substring(tc.message.IndexOf("sdmd_code") + 12, tc.message.Length - tc.message.IndexOf("\"}")).Replace("\"", "").Replace("}", "")

                                   }).Distinct().ToList();

                //取其一駐區碼
                var sdmdCode = (from sc in sdmdCodePre
                                group sc by sc.check_number into g

                                select new
                                {
                                    check_number = g.Key,
                                    sdmd_code = g.Max(a => a.sdmd_code)

                                }).Distinct().ToList();


                //第三方註記
                var thirdPartyFlag = (from por in JunFuDbContext.pickuppOrderResponse.Where(a => a.sendflag.Equals(true))
                                      select new
                                      {
                                          check_number = por.FSE_check_number,

                                      }).ToList();


                var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                            JunFuDbContext.TcDeliveryRequests
                            .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                   && a.LatestDeliveryDriver == driverCode)
                            .Select(a => new DeliveryAndDeliveryMatingEntity
                            {
                                CheckNumber = a.CheckNumber,
                                SubpoenaCategory = a.SubpoenaCategory,
                                Pieces = a.Pieces,
                                ArriveOption = a.LatestScanArriveOption,
                                ScanDate = a.LatestScanDate,
                                ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                CollectionMoney = a.CollectionMoney ?? 0,
                                ReceiveContact = a.ReceiveContact,
                                ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                TimePeriod = a.TimePeriod,
                                Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                            }).Distinct().ToList() :
                            JunFuDbContext.TcDeliveryRequests
                            .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                   && a.LatestDeliveryDriver == driverCode && a.LatestScanArriveOption == arriveOption)
                            .Select(a => new DeliveryAndDeliveryMatingEntity
                            {
                                CheckNumber = a.CheckNumber,
                                SubpoenaCategory = a.SubpoenaCategory,
                                Pieces = a.Pieces,
                                ArriveOption = a.LatestScanArriveOption,
                                ScanDate = a.LatestScanDate,
                                ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                CollectionMoney = a.CollectionMoney ?? 0,
                                ReceiveContact = a.ReceiveContact,
                                ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                TimePeriod = a.TimePeriod,
                                Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth

                            }).Distinct().ToList();

                var data = (from d in delivery
                            join s in sdmdCode on d.CheckNumber equals s.check_number into t1
                            from s in t1.DefaultIfEmpty()
                            join t in thirdPartyFlag on d.CheckNumber equals t.check_number into t2
                            from t in t2.DefaultIfEmpty()
                            select new DeliveryAndDeliveryMatingEntity
                            {

                                CheckNumber = d.CheckNumber,  //貨號
                                SubpoenaCategory = d.SubpoenaCategory, //傳票類別
                                Pieces = d.Pieces,  //件數
                                ArriveOption = d.ArriveOption, //配達區分
                                ScanDate = d.ScanDate,   //掃讀時間
                                ReceiveAddress = d.ReceiveAddress,  //配送地址
                                DriverNameCode = d.DriverNameCode, //作業司機
                                CollectionMoney = d.CollectionMoney, //代收貨款
                                ReceiveContact = d.ReceiveContact, //收件人姓名
                                ReceiveTel = d.ReceiveTel,   //收件人電話
                                RoundTrip = d.RoundTrip,  //來回件備註                                   
                                TimePeriod = d.TimePeriod, //指定配送時間
                                Cbm = d.Cbm,   //材積
                                ThirdPartyFlag = t == null ? "" : "皮卡",  //3PL註記
                                SDMDCode = s == null ? "" : s.sdmd_code  //駐區碼
                            });
                return data.ToList();


                //return delivery;
            }
        }

        public List<DeliveryAndDeliveryMatingEntity> GetAllByStationScode(DateTime start, DateTime end, string stationscode, string arriveOption, string station_level, string station_area, string management,string station)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
               , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                string managementList = "";
                string station_scodeList = "";
                string station_areaList = "";

                if (string.IsNullOrEmpty(stationscode) || stationscode == "-1")
                {
                    if (station_level==null)
                    {
                        var specificDrivers = JunFuDbContext.TbDrivers.Where(a => station.Contains(a.Station)).Select(a => a.DriverCode).ToList();

                        var sdmdCodePre = (from tc in JunFuDbContext.JubFuAPPWebServiceLog.Where(a => a.type.Equals("getArriveSDMDCode"))
                                           select new
                                           {
                                               check_number = tc.message.Substring(17, 12),
                                               sdmd_code = tc.message.Substring(tc.message.IndexOf("sdmd_code") + 12, tc.message.Length - tc.message.IndexOf("\"}")).Replace("\"", "").Replace("}", "")

                                           }).Distinct().ToList();

                        //取其一駐區碼
                        var sdmdCode = (from sc in sdmdCodePre
                                        group sc by sc.check_number into g

                                        select new
                                        {
                                            check_number = g.Key,
                                            sdmd_code = g.Max(a => a.sdmd_code)

                                        }).Distinct().ToList();



                        var thirdPartyFlag = (from por in JunFuDbContext.pickuppOrderResponse.Where(a => a.sendflag.Equals(true))
                                              select new
                                              {
                                                  check_number = por.FSE_check_number,

                                              }).ToList();


                        var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver)
                                           )
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList() :
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption)
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList();

                        var data = (from d in delivery
                                    join s in sdmdCode on d.CheckNumber equals s.check_number into t1
                                    from s in t1.DefaultIfEmpty()
                                    join t in thirdPartyFlag on d.CheckNumber equals t.check_number into t2
                                    from t in t2.DefaultIfEmpty()
                                    select new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = d.CheckNumber,  //貨號
                                        SubpoenaCategory = d.SubpoenaCategory, //傳票類別
                                        Pieces = d.Pieces,  //件數
                                        ArriveOption = d.ArriveOption, //配達區分
                                        ScanDate = d.ScanDate,   //掃讀時間
                                        ReceiveAddress = d.ReceiveAddress,  //配送地址
                                        DriverNameCode = d.DriverNameCode, //作業司機
                                        CollectionMoney = d.CollectionMoney, //代收貨款
                                        ReceiveContact = d.ReceiveContact, //收件人姓名
                                        ReceiveTel = d.ReceiveTel,   //收件人電話
                                        RoundTrip = d.RoundTrip,  //來回件備註                                   
                                        TimePeriod = d.TimePeriod, //指定配送時間
                                        Cbm = d.Cbm,   //材積
                                        ThirdPartyFlag = t == null ? "" : "皮卡",  //3PL註記
                                        SDMDCode = s == null ? "" : s.sdmd_code  //駐區碼
                                    });
                        return data.ToList();

                    }
                    else

                    if (station_level.Equals("1"))
                    {
                        var da = (from tbstation in JunFuDbContext.TbStations
                                  where tbstation.management == management
                                  select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                        if (da.Count() > 0)
                        {
                            foreach (var item in da)
                            {
                                managementList += "'";
                                managementList += item.StationScode;
                                managementList += "'";
                                managementList += ",";
                            }
                            managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                        }

                        var specificDrivers = JunFuDbContext.TbDrivers.Where(a => managementList.Contains(a.Station)).Select(a => a.DriverCode).ToList();

                        var sdmdCodePre = (from tc in JunFuDbContext.JubFuAPPWebServiceLog.Where(a => a.type.Equals("getArriveSDMDCode"))
                                           select new
                                           {
                                               check_number = tc.message.Substring(17, 12),
                                               sdmd_code = tc.message.Substring(tc.message.IndexOf("sdmd_code") + 12, tc.message.Length - tc.message.IndexOf("\"}")).Replace("\"", "").Replace("}", "")

                                           }).Distinct().ToList();

                        //取其一駐區碼
                        var sdmdCode = (from sc in sdmdCodePre
                                        group sc by sc.check_number into g

                                        select new
                                        {
                                            check_number = g.Key,
                                            sdmd_code = g.Max(a => a.sdmd_code)

                                        }).Distinct().ToList();



                        var thirdPartyFlag = (from por in JunFuDbContext.pickuppOrderResponse.Where(a => a.sendflag.Equals(true))
                                              select new
                                              {
                                                  check_number = por.FSE_check_number,

                                              }).ToList();


                        var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver)
                                           )
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList() :
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption)
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList();

                        var data = (from d in delivery
                                    join s in sdmdCode on d.CheckNumber equals s.check_number into t1
                                    from s in t1.DefaultIfEmpty()
                                    join t in thirdPartyFlag on d.CheckNumber equals t.check_number into t2
                                    from t in t2.DefaultIfEmpty()
                                    select new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = d.CheckNumber,  //貨號
                                        SubpoenaCategory = d.SubpoenaCategory, //傳票類別
                                        Pieces = d.Pieces,  //件數
                                        ArriveOption = d.ArriveOption, //配達區分
                                        ScanDate = d.ScanDate,   //掃讀時間
                                        ReceiveAddress = d.ReceiveAddress,  //配送地址
                                        DriverNameCode = d.DriverNameCode, //作業司機
                                        CollectionMoney = d.CollectionMoney, //代收貨款
                                        ReceiveContact = d.ReceiveContact, //收件人姓名
                                        ReceiveTel = d.ReceiveTel,   //收件人電話
                                        RoundTrip = d.RoundTrip,  //來回件備註                                   
                                        TimePeriod = d.TimePeriod, //指定配送時間
                                        Cbm = d.Cbm,   //材積
                                        ThirdPartyFlag = t == null ? "" : "皮卡",  //3PL註記
                                        SDMDCode = s == null ? "" : s.sdmd_code  //駐區碼
                                    });
                        return data.ToList();
                    }
                    else if (station_level.Equals("2"))
                    {
                        var da = (from tbstation in JunFuDbContext.TbStations
                                  where tbstation.StationScode == stationscode
                                  select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                        if (da.Count() > 0)
                        {
                            foreach (var item in da)
                            {
                                station_scodeList += "'";
                                station_scodeList += item.StationScode;
                                station_scodeList += "'";
                                station_scodeList += ",";
                            }
                            station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                        }

                        var specificDrivers = JunFuDbContext.TbDrivers.Where(a => station_scodeList.Contains(a.Station)).Select(a => a.DriverCode).ToList();

                        var sdmdCodePre = (from tc in JunFuDbContext.JubFuAPPWebServiceLog.Where(a => a.type.Equals("getArriveSDMDCode"))
                                           select new
                                           {
                                               check_number = tc.message.Substring(17, 12),
                                               sdmd_code = tc.message.Substring(tc.message.IndexOf("sdmd_code") + 12, tc.message.Length - tc.message.IndexOf("\"}")).Replace("\"", "").Replace("}", "")

                                           }).Distinct().ToList();

                        //取其一駐區碼
                        var sdmdCode = (from sc in sdmdCodePre
                                        group sc by sc.check_number into g

                                        select new
                                        {
                                            check_number = g.Key,
                                            sdmd_code = g.Max(a => a.sdmd_code)

                                        }).Distinct().ToList();



                        var thirdPartyFlag = (from por in JunFuDbContext.pickuppOrderResponse.Where(a => a.sendflag.Equals(true))
                                              select new
                                              {
                                                  check_number = por.FSE_check_number,

                                              }).ToList();


                        var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver)
                                           )
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList() :
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption)
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList();

                        var data = (from d in delivery
                                    join s in sdmdCode on d.CheckNumber equals s.check_number into t1
                                    from s in t1.DefaultIfEmpty()
                                    join t in thirdPartyFlag on d.CheckNumber equals t.check_number into t2
                                    from t in t2.DefaultIfEmpty()
                                    select new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = d.CheckNumber,  //貨號
                                        SubpoenaCategory = d.SubpoenaCategory, //傳票類別
                                        Pieces = d.Pieces,  //件數
                                        ArriveOption = d.ArriveOption, //配達區分
                                        ScanDate = d.ScanDate,   //掃讀時間
                                        ReceiveAddress = d.ReceiveAddress,  //配送地址
                                        DriverNameCode = d.DriverNameCode, //作業司機
                                        CollectionMoney = d.CollectionMoney, //代收貨款
                                        ReceiveContact = d.ReceiveContact, //收件人姓名
                                        ReceiveTel = d.ReceiveTel,   //收件人電話
                                        RoundTrip = d.RoundTrip,  //來回件備註                                   
                                        TimePeriod = d.TimePeriod, //指定配送時間
                                        Cbm = d.Cbm,   //材積
                                        ThirdPartyFlag = t == null ? "" : "皮卡",  //3PL註記
                                        SDMDCode = s == null ? "" : s.sdmd_code  //駐區碼
                                    });
                        return data.ToList();
                    }
                    else if (station_level.Equals("4"))
                    {
                        var da = (from tbstation in JunFuDbContext.TbStations
                                  where tbstation.station_area == Int32.Parse(station_area)
                                  select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                        if (da.Count() > 0)
                        {
                            foreach (var item in da)
                            {
                                station_areaList += "'";
                                station_areaList += item.StationScode;
                                station_areaList += "'";
                                station_areaList += ",";
                            }
                            station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                        }

                        var specificDrivers = JunFuDbContext.TbDrivers.Where(a => station_areaList.Contains(a.Station)).Select(a => a.DriverCode).ToList();

                        var sdmdCodePre = (from tc in JunFuDbContext.JubFuAPPWebServiceLog.Where(a => a.type.Equals("getArriveSDMDCode"))
                                           select new
                                           {
                                               check_number = tc.message.Substring(17, 12),
                                               sdmd_code = tc.message.Substring(tc.message.IndexOf("sdmd_code") + 12, tc.message.Length - tc.message.IndexOf("\"}")).Replace("\"", "").Replace("}", "")

                                           }).Distinct().ToList();

                        //取其一駐區碼
                        var sdmdCode = (from sc in sdmdCodePre
                                        group sc by sc.check_number into g

                                        select new
                                        {
                                            check_number = g.Key,
                                            sdmd_code = g.Max(a => a.sdmd_code)

                                        }).Distinct().ToList();



                        var thirdPartyFlag = (from por in JunFuDbContext.pickuppOrderResponse.Where(a => a.sendflag.Equals(true))
                                              select new
                                              {
                                                  check_number = por.FSE_check_number,

                                              }).ToList();


                        var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver)
                                           )
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList() :
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption)
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList();

                        var data = (from d in delivery
                                    join s in sdmdCode on d.CheckNumber equals s.check_number into t1
                                    from s in t1.DefaultIfEmpty()
                                    join t in thirdPartyFlag on d.CheckNumber equals t.check_number into t2
                                    from t in t2.DefaultIfEmpty()
                                    select new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = d.CheckNumber,  //貨號
                                        SubpoenaCategory = d.SubpoenaCategory, //傳票類別
                                        Pieces = d.Pieces,  //件數
                                        ArriveOption = d.ArriveOption, //配達區分
                                        ScanDate = d.ScanDate,   //掃讀時間
                                        ReceiveAddress = d.ReceiveAddress,  //配送地址
                                        DriverNameCode = d.DriverNameCode, //作業司機
                                        CollectionMoney = d.CollectionMoney, //代收貨款
                                        ReceiveContact = d.ReceiveContact, //收件人姓名
                                        ReceiveTel = d.ReceiveTel,   //收件人電話
                                        RoundTrip = d.RoundTrip,  //來回件備註                                   
                                        TimePeriod = d.TimePeriod, //指定配送時間
                                        Cbm = d.Cbm,   //材積
                                        ThirdPartyFlag = t == null ? "" : "皮卡",  //3PL註記
                                        SDMDCode = s == null ? "" : s.sdmd_code  //駐區碼
                                    });
                        return data.ToList();
                    }
                    else if (station_level.Equals("5") || station_level.Equals(""))
                    {

                        var specificDrivers = JunFuDbContext.TbDrivers.Select(a => a.DriverCode).ToList();

                        var sdmdCodePre = (from tc in JunFuDbContext.JubFuAPPWebServiceLog.Where(a => a.type.Equals("getArriveSDMDCode"))
                                           select new
                                           {
                                               check_number = tc.message.Substring(17, 12),
                                               sdmd_code = tc.message.Substring(tc.message.IndexOf("sdmd_code") + 12, tc.message.Length - tc.message.IndexOf("\"}")).Replace("\"", "").Replace("}", "")

                                           }).Distinct().ToList();

                        //取其一駐區碼
                        var sdmdCode = (from sc in sdmdCodePre
                                        group sc by sc.check_number into g

                                        select new
                                        {
                                            check_number = g.Key,
                                            sdmd_code = g.Max(a => a.sdmd_code)

                                        }).Distinct().ToList();



                        var thirdPartyFlag = (from por in JunFuDbContext.pickuppOrderResponse.Where(a => a.sendflag.Equals(true))
                                              select new
                                              {
                                                  check_number = por.FSE_check_number,

                                              }).ToList();


                        var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver)
                                           )
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList() :
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption)
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList();

                        var data = (from d in delivery
                                    join s in sdmdCode on d.CheckNumber equals s.check_number into t1
                                    from s in t1.DefaultIfEmpty()
                                    join t in thirdPartyFlag on d.CheckNumber equals t.check_number into t2
                                    from t in t2.DefaultIfEmpty()
                                    select new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = d.CheckNumber,  //貨號
                                        SubpoenaCategory = d.SubpoenaCategory, //傳票類別
                                        Pieces = d.Pieces,  //件數
                                        ArriveOption = d.ArriveOption, //配達區分
                                        ScanDate = d.ScanDate,   //掃讀時間
                                        ReceiveAddress = d.ReceiveAddress,  //配送地址
                                        DriverNameCode = d.DriverNameCode, //作業司機
                                        CollectionMoney = d.CollectionMoney, //代收貨款
                                        ReceiveContact = d.ReceiveContact, //收件人姓名
                                        ReceiveTel = d.ReceiveTel,   //收件人電話
                                        RoundTrip = d.RoundTrip,  //來回件備註                                   
                                        TimePeriod = d.TimePeriod, //指定配送時間
                                        Cbm = d.Cbm,   //材積
                                        ThirdPartyFlag = t == null ? "" : "皮卡",  //3PL註記
                                        SDMDCode = s == null ? "" : s.sdmd_code  //駐區碼
                                    });
                        return data.ToList();
                    }
                    else
                    {
                        var specificDrivers = JunFuDbContext.TbDrivers.Where(a => a.Station == stationscode).Select(a => a.DriverCode).ToList();

                        var sdmdCodePre = (from tc in JunFuDbContext.JubFuAPPWebServiceLog.Where(a => a.type.Equals("getArriveSDMDCode"))
                                           select new
                                           {
                                               check_number = tc.message.Substring(17, 12),
                                               sdmd_code = tc.message.Substring(tc.message.IndexOf("sdmd_code") + 12, tc.message.Length - tc.message.IndexOf("\"}")).Replace("\"", "").Replace("}", "")

                                           }).Distinct().ToList();

                        //取其一駐區碼
                        var sdmdCode = (from sc in sdmdCodePre
                                        group sc by sc.check_number into g

                                        select new
                                        {
                                            check_number = g.Key,
                                            sdmd_code = g.Max(a => a.sdmd_code)

                                        }).Distinct().ToList();



                        var thirdPartyFlag = (from por in JunFuDbContext.pickuppOrderResponse.Where(a => a.sendflag.Equals(true))
                                              select new
                                              {
                                                  check_number = por.FSE_check_number,

                                              }).ToList();


                        var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver)
                                           )
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList() :
                                    JunFuDbContext.TcDeliveryRequests
                                    .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                           && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption)
                                    .Select(a => new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = a.CheckNumber,
                                        SubpoenaCategory = a.SubpoenaCategory,
                                        Pieces = a.Pieces,
                                        ArriveOption = a.LatestScanArriveOption,
                                        ScanDate = a.LatestScanDate,
                                        ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                        DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                        CollectionMoney = a.CollectionMoney ?? 0,
                                        ReceiveContact = a.ReceiveContact,
                                        ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                        RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                        TimePeriod = a.TimePeriod,
                                        Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                    }).Distinct().ToList();

                        var data = (from d in delivery
                                    join s in sdmdCode on d.CheckNumber equals s.check_number into t1
                                    from s in t1.DefaultIfEmpty()
                                    join t in thirdPartyFlag on d.CheckNumber equals t.check_number into t2
                                    from t in t2.DefaultIfEmpty()
                                    select new DeliveryAndDeliveryMatingEntity
                                    {
                                        CheckNumber = d.CheckNumber,  //貨號
                                        SubpoenaCategory = d.SubpoenaCategory, //傳票類別
                                        Pieces = d.Pieces,  //件數
                                        ArriveOption = d.ArriveOption, //配達區分
                                        ScanDate = d.ScanDate,   //掃讀時間
                                        ReceiveAddress = d.ReceiveAddress,  //配送地址
                                        DriverNameCode = d.DriverNameCode, //作業司機
                                        CollectionMoney = d.CollectionMoney, //代收貨款
                                        ReceiveContact = d.ReceiveContact, //收件人姓名
                                        ReceiveTel = d.ReceiveTel,   //收件人電話
                                        RoundTrip = d.RoundTrip,  //來回件備註                                   
                                        TimePeriod = d.TimePeriod, //指定配送時間
                                        Cbm = d.Cbm,   //材積
                                        ThirdPartyFlag = t == null ? "" : "皮卡",  //3PL註記
                                        SDMDCode = s == null ? "" : s.sdmd_code  //駐區碼
                                    });
                        return data.ToList();
                    }
                }
                else
                {
                    var specificDrivers = JunFuDbContext.TbDrivers.Where(a => a.Station == stationscode).Select(a => a.DriverCode).ToList();

                    var sdmdCodePre = (from tc in JunFuDbContext.JubFuAPPWebServiceLog.Where(a => a.type.Equals("getArriveSDMDCode"))
                                       select new
                                       {
                                           check_number = tc.message.Substring(17, 12),
                                           sdmd_code = tc.message.Substring(tc.message.IndexOf("sdmd_code") + 12, tc.message.Length - tc.message.IndexOf("\"}")).Replace("\"", "").Replace("}", "")

                                       }).Distinct().ToList();

                    //取其一駐區碼
                    var sdmdCode = (from sc in sdmdCodePre
                                    group sc by sc.check_number into g

                                    select new
                                    {
                                        check_number = g.Key,
                                        sdmd_code = g.Max(a => a.sdmd_code)

                                    }).Distinct().ToList();



                    var thirdPartyFlag = (from por in JunFuDbContext.pickuppOrderResponse.Where(a => a.sendflag.Equals(true))
                                          select new
                                          {
                                              check_number = por.FSE_check_number,

                                          }).ToList();


                    var delivery = (arriveOption.Equals("all")) ? //是否為全部配達區分
                                JunFuDbContext.TcDeliveryRequests
                                .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                       && specificDrivers.Contains(a.LatestDeliveryDriver)
                                       )
                                .Select(a => new DeliveryAndDeliveryMatingEntity
                                {
                                    CheckNumber = a.CheckNumber,
                                    SubpoenaCategory = a.SubpoenaCategory,
                                    Pieces = a.Pieces,
                                    ArriveOption = a.LatestScanArriveOption,
                                    ScanDate = a.LatestScanDate,
                                    ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                    DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                    CollectionMoney = a.CollectionMoney ?? 0,
                                    ReceiveContact = a.ReceiveContact,
                                    ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                    RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                    TimePeriod = a.TimePeriod,
                                    Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                }).Distinct().ToList() :
                                JunFuDbContext.TcDeliveryRequests
                                .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                       && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption)
                                .Select(a => new DeliveryAndDeliveryMatingEntity
                                {
                                    CheckNumber = a.CheckNumber,
                                    SubpoenaCategory = a.SubpoenaCategory,
                                    Pieces = a.Pieces,
                                    ArriveOption = a.LatestScanArriveOption,
                                    ScanDate = a.LatestScanDate,
                                    ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                    DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                    CollectionMoney = a.CollectionMoney ?? 0,
                                    ReceiveContact = a.ReceiveContact,
                                    ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                    RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                    TimePeriod = a.TimePeriod,
                                    Cbm = (int)a.CbmHeight + (int)a.CbmLength + (int)a.CbmWidth
                                }).Distinct().ToList();

                    var data = (from d in delivery
                                join s in sdmdCode on d.CheckNumber equals s.check_number into t1
                                from s in t1.DefaultIfEmpty()
                                join t in thirdPartyFlag on d.CheckNumber equals t.check_number into t2
                                from t in t2.DefaultIfEmpty()
                                select new DeliveryAndDeliveryMatingEntity
                                {
                                    CheckNumber = d.CheckNumber,  //貨號
                                    SubpoenaCategory = d.SubpoenaCategory, //傳票類別
                                    Pieces = d.Pieces,  //件數
                                    ArriveOption = d.ArriveOption, //配達區分
                                    ScanDate = d.ScanDate,   //掃讀時間
                                    ReceiveAddress = d.ReceiveAddress,  //配送地址
                                    DriverNameCode = d.DriverNameCode, //作業司機
                                    CollectionMoney = d.CollectionMoney, //代收貨款
                                    ReceiveContact = d.ReceiveContact, //收件人姓名
                                    ReceiveTel = d.ReceiveTel,   //收件人電話
                                    RoundTrip = d.RoundTrip,  //來回件備註                                   
                                    TimePeriod = d.TimePeriod, //指定配送時間
                                    Cbm = d.Cbm,   //材積
                                    ThirdPartyFlag = t == null ? "" : "皮卡",  //3PL註記
                                    SDMDCode = s == null ? "" : s.sdmd_code  //駐區碼
                                });
                    return data.ToList();
                }
            }
            return null;
        }

        public int GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station)
        {
            var count = (driverCode.Equals("all")) ? GetAllByStationScode(start, end, stationscode, arriveOption, station_level, station_area, management,station).Count() : GetAllByDriverCode(start, end, driverCode, arriveOption).Count();
            return count;
        }

        public int GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station)
        {
            int count = 0;

            if (driverCode.Equals("all"))
            {

                if (station_level == null)
                {
                    var specificDrivers = JunFuDbContext.TbDrivers.Where(a => a.Station == station).Select(a => a.DriverCode).ToList();

                    count = (arriveOption.Equals("all")) ? //是否為全部配達區分
                            JunFuDbContext.TcDeliveryRequests
                            .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                   && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption != null)
                            .Select(a => new DeliveryAndDeliveryMatingEntity
                            {
                                CheckNumber = a.CheckNumber,
                                SubpoenaCategory = a.SubpoenaCategory,
                                Pieces = a.Pieces,
                                ArriveOption = a.LatestScanArriveOption,
                                ScanDate = a.LatestScanDate,
                                ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                CollectionMoney = a.CollectionMoney ?? 0,
                                ReceiveContact = a.ReceiveContact,
                                ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                TimePeriod = a.TimePeriod

                            }).Distinct().ToList().Count() :
                            JunFuDbContext.TcDeliveryRequests
                            .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                                   && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption && a.LatestScanArriveOption != null)
                            .Select(a => new DeliveryAndDeliveryMatingEntity
                            {
                                CheckNumber = a.CheckNumber,
                                SubpoenaCategory = a.SubpoenaCategory,
                                Pieces = a.Pieces,
                                ArriveOption = a.LatestScanArriveOption,
                                ScanDate = a.LatestScanDate,
                                ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                                DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                                CollectionMoney = a.CollectionMoney ?? 0,
                                ReceiveContact = a.ReceiveContact,
                                ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                                RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                                TimePeriod = a.TimePeriod
                            }).Distinct().ToList().Count();
                }
                else { 



                var specificDrivers = JunFuDbContext.TbDrivers.Where(a => a.Station == stationscode).Select(a => a.DriverCode).ToList();

                count = (arriveOption.Equals("all")) ? //是否為全部配達區分
                        JunFuDbContext.TcDeliveryRequests
                        .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                               && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption != null)
                        .Select(a => new DeliveryAndDeliveryMatingEntity
                        {
                            CheckNumber = a.CheckNumber,
                            SubpoenaCategory = a.SubpoenaCategory,
                            Pieces = a.Pieces,
                            ArriveOption = a.LatestScanArriveOption,
                            ScanDate = a.LatestScanDate,
                            ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                            DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                            CollectionMoney = a.CollectionMoney ?? 0,
                            ReceiveContact = a.ReceiveContact,
                            ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                            RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                            TimePeriod = a.TimePeriod

                        }).Distinct().ToList().Count() :
                        JunFuDbContext.TcDeliveryRequests
                        .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                               && specificDrivers.Contains(a.LatestDeliveryDriver) && a.LatestScanArriveOption == arriveOption && a.LatestScanArriveOption != null)
                        .Select(a => new DeliveryAndDeliveryMatingEntity
                        {
                            CheckNumber = a.CheckNumber,
                            SubpoenaCategory = a.SubpoenaCategory,
                            Pieces = a.Pieces,
                            ArriveOption = a.LatestScanArriveOption,
                            ScanDate = a.LatestScanDate,
                            ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                            DriverNameCode = a.LatestScanDriverCode ?? a.LatestDeliveryDriver,
                            CollectionMoney = a.CollectionMoney ?? 0,
                            ReceiveContact = a.ReceiveContact,
                            ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                            RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                            TimePeriod = a.TimePeriod
                        }).Distinct().ToList().Count();
                }
            }
            else
            {
                count = (arriveOption.Equals("all")) ? //是否為全部配達區分
                        JunFuDbContext.TcDeliveryRequests
                        .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                               && a.LatestDeliveryDriver == driverCode && a.LatestScanArriveOption != null)
                        .Select(a => new DeliveryAndDeliveryMatingEntity
                        {
                            CheckNumber = a.CheckNumber,
                            SubpoenaCategory = a.SubpoenaCategory,
                            Pieces = a.Pieces,
                            ArriveOption = a.LatestScanArriveOption,
                            ScanDate = a.LatestScanDate,
                            ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                            DriverNameCode = a.LatestScanDriverCode,
                            CollectionMoney = a.CollectionMoney ?? 0,
                            ReceiveContact = a.ReceiveContact,
                            ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                            RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                            TimePeriod = a.TimePeriod
                        }).Distinct().ToList().Count() :
                        JunFuDbContext.TcDeliveryRequests
                        .Where(a => a.LessThanTruckload.Equals(true) && a.LatestDeliveryDate > start && a.LatestDeliveryDate < end.AddDays(1)
                               && a.LatestDeliveryDriver == driverCode && a.LatestScanArriveOption == arriveOption && a.LatestScanArriveOption != null)
                        .Select(a => new DeliveryAndDeliveryMatingEntity
                        {
                            CheckNumber = a.CheckNumber,
                            SubpoenaCategory = a.SubpoenaCategory,
                            Pieces = a.Pieces,
                            ArriveOption = a.LatestScanArriveOption,
                            ScanDate = a.LatestScanDate,
                            ReceiveAddress = a.ReceiveCity + a.ReceiveArea + a.ReceiveAddress,
                            DriverNameCode = a.LatestScanDriverCode,
                            CollectionMoney = a.CollectionMoney ?? 0,
                            ReceiveContact = a.ReceiveContact,
                            ReceiveTel = a.ReceiveTel1 + "#" + (a.ReceiveTel1Ext == null ? "" : a.ReceiveTel1Ext) + "/ " + (a.ReceiveTel2 == null ? "" : a.ReceiveTel2),
                            RoundTrip = (a.RoundTrip == true && a.RoundtripChecknumber.Length == 12) ? "來回件" : "",
                            TimePeriod = a.TimePeriod
                        }).Distinct().ToList().Count();
            }
            return count;
        }

        public List<ArriveOptionEntity> GetArriveOptions()
        {
            var data = (from c in JunFuDbContext.TbItemCodes
                        where c.CodeId.CompareTo("9") <= 0 & c.CodeId != "10" & c.CodeId != "11" & c.CodeSclass == "AO"
                        select new ArriveOptionEntity { CodeName = c.CodeName, CodeId = c.CodeId });
            return data.ToList();
        }

        public List<DriversEntity> GetAllDrivers(string station_level, string station_area, string management, string stationscode, string station_scode,string station)
        {

            DateTime updateDate = DateTime.Now.AddDays(-60);
            string managementList = "";
            string station_scodeList = "";
            string station_areaList = "";
            if (station_level == null)
            {
                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && station.Contains(d.Station)
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8) && d.ActiveFlag == true
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false && sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();

            }
            else
            if (station_level.Equals("1"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.management == management
                          select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        managementList += "'";
                        managementList += item.StationScode;
                        managementList += "'";
                        managementList += ",";
                    }
                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                }

                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && managementList.Contains(d.Station)
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8) && d.ActiveFlag == true
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false && sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });


                return data.ToList();
            }
            else if (station_level.Equals("2"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.StationScode == station_scode
                          select new TbStation { StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_scodeList += "'";
                        station_scodeList += item.StationScode;
                        station_scodeList += "'";
                        station_scodeList += ",";
                    }
                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                }

                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && station_scodeList.Contains(d.Station)
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8) && d.ActiveFlag == true
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false && sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            else if (station_level.Equals("4"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.station_area == Int32.Parse(station_area)
                          select new TbStation { station_area = tbstation.station_area, StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_areaList += "'";
                        station_areaList += item.StationScode;
                        station_areaList += "'";
                        station_areaList += ",";
                    }
                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                }

                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && station_areaList.Contains(d.Station)
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8) && d.ActiveFlag == true
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false && sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            else if (station_level.Equals("5"))
            {
                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && d.ActiveFlag == true
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false && sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            else
            {

                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && d.ActiveFlag==true 
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false && sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            return null;
        }

        public List<DriversEntity> GetDriversByStation(string station_level, string station_area, string management, string stationscode,string station)
        {
            DateTime updateDate = DateTime.Now.AddDays(-60);
            /*
            var data = (from d in JunFuDbContext.TbDrivers
                        join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                        where d.DriverCode.StartsWith("Z") && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                        && d.Station == stationscode
                        && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                        && (sl.ActiveFlag == true || (sl.ActiveFlag == false & sl.Udate > updateDate))
                        select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
            */
            string managementList = "";
            string station_areaList = "";
            string station_scodeList = "";

            if (stationscode != "" && stationscode != "-1")
            {
                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && stationscode.Contains(d.Station) && d.ActiveFlag==true
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false & sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            else if (station_level==null)
            {

                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && station.Contains(d.Station) && d.ActiveFlag == true
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false & sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }



            else if (station_level.Equals("1"))
            {

                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.management == management
                          select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        managementList += "'";
                        managementList += item.StationScode;
                        managementList += "'";
                        managementList += ",";
                    }
                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                }


                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && managementList.Contains(d.Station) && d.ActiveFlag == true
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false & sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            else if (station_level.Equals("2"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.StationScode == stationscode
                          select new TbStation { StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_scodeList += "'";
                        station_scodeList += item.StationScode;
                        station_scodeList += "'";
                        station_scodeList += ",";
                    }
                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                }

                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && station_scodeList.Contains(d.Station) && d.ActiveFlag == true
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false & sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            else if (station_level.Equals("4"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where Convert.ToString(tbstation.station_area) == station_area
                          select new TbStation { station_area = tbstation.station_area, StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_areaList += "'";
                        station_areaList += item.StationScode;
                        station_areaList += "'";
                        station_areaList += ",";
                    }
                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                }

                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && station_areaList.Contains(d.Station) && d.ActiveFlag == true
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false & sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            else if (station_level.Equals("5"))
            {
                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8) && d.ActiveFlag == true
                            //&& station_scodeList.Contains(d.Station)
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false & sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            else
            {
                var data = (from d in JunFuDbContext.TbDrivers
                            join sl in JunFuDbContext.TbEmps on d.DriverCode equals sl.DriverCode
                            where d.DriverCode.StartsWith("Z") && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8) && d.ActiveFlag == true
                            //&& stationscode.Contains(d.Station)
                            && (d.DriverCode.Length == 7 || d.DriverCode.Length == 8)
                            && (sl.ActiveFlag == true || (sl.ActiveFlag == false & sl.Udate > updateDate))
                            select new DriversEntity { DriverCode = d.DriverCode, DriverName = d.DriverCode + "-" + d.DriverName });
                return data.ToList();
            }
            return null;
        }

        public List<StationEntity> GetAllStations(string station_level, string station_area, string management, string stationscode)
        {
            string managementList = "";
            string station_areaList = "";
            string station_scodeList = "";

            if (station_level.Equals("1"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.management == management
                          select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        managementList += "'";
                        managementList += item.StationScode;
                        managementList += "'";
                        managementList += ",";
                    }
                    managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                }
                var data = (from s in JunFuDbContext.TbStations
                            where managementList.Contains(management)
                            select new StationEntity { Name = s.StationScode + "-" + s.StationName, Scode = s.StationScode });
                return data.ToList();

            }
            else if (station_level.Equals("2"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.StationScode == stationscode
                          select new TbStation { StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_scodeList += "'";
                        station_scodeList += item.StationScode;
                        station_scodeList += "'";
                        station_scodeList += ",";
                    }
                    station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                }
                var data = (from s in JunFuDbContext.TbStations
                            where station_scodeList.Contains(stationscode)
                            select new StationEntity { Name = s.StationScode + "-" + s.StationName, Scode = s.StationScode });
                return data.ToList();
            }
            else if (station_level.Equals("4"))
            {
                var da = (from tbstation in JunFuDbContext.TbStations
                          where tbstation.StationScode == station_area
                          select new TbStation { StationScode = tbstation.StationScode });

                if (da.Count() > 0)
                {
                    foreach (var item in da)
                    {
                        station_areaList += "'";
                        station_areaList += item.StationScode;
                        station_areaList += "'";
                        station_areaList += ",";
                    }
                    station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                }
                var data = (from s in JunFuDbContext.TbStations
                            where station_areaList.Contains(station_area)
                            select new StationEntity { Name = s.StationScode + "-" + s.StationName, Scode = s.StationScode });
                return data.ToList();
            }
            else if (station_level.Equals("5"))
            {
                var data = (from s in JunFuDbContext.TbStations select new StationEntity { Name = s.StationScode + "-" + s.StationName, Scode = s.StationScode });
                return data.ToList();
            }
            return null;

        }

        public IEnumerable<TbItemCode> GetAllTbItemCodesWhichSclassAO()
        {
            return JunFuDbContext.TbItemCodes.Where(x => x.CodeSclass == "AO");
        }
    }
}
