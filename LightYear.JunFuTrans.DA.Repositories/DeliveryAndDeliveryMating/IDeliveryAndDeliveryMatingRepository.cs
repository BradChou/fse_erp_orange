﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryAndDeliveryMating
{
    public interface IDeliveryAndDeliveryMatingRepository
    {
        List<ArriveOptionEntity> GetArriveOptions();
        List<DriversEntity> GetAllDrivers(string station_level, string station_area, string management, string stationscode,string station_scode,string station);
        List<StationEntity> GetAllStations(string station_level, string station_area, string management, string stationscode);
        int GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station);
        int GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station);
        List<DeliveryAndDeliveryMatingEntity> GetAllByDriverCode(DateTime start, DateTime end, string driverCode, string arriveOption);
        List<DeliveryAndDeliveryMatingEntity> GetAllByStationScode(DateTime start, DateTime end, string stationscode, string arriveOption, string station_level, string station_area, string management,string station);
        List<DriversEntity> GetDriversByStation(string station_level,string station_area,string management, string stationscode,string station);
        IEnumerable<TbItemCode> GetAllTbItemCodesWhichSclassAO();
    }
}
