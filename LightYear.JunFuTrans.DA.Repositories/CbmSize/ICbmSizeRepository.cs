﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.CbmSize
{
    public interface ICbmSizeRepository
    {
        TcCbmSize[] GetAllCbmSize();
    }
}
