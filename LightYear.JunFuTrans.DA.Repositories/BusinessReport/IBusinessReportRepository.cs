﻿using LightYear.JunFuTrans.BL.BE.BusinessReport;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.BusinessReport
{
    public interface IBusinessReportRepository
    {
        List<BusinessReportRepositoryEntity> PieceList(DateTime start, DateTime end, string areaArriveCode);

        List<TbStation> StationEntities();
    }
}
