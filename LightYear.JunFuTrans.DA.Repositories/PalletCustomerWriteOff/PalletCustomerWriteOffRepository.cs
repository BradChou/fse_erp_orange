﻿using LightYear.JunFuTrans.BL.BE.PalletCustomerWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Station;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.PalletCustomerWriteOff
{
    public class PalletCustomerWriteOffRepository : IPalletCustomerWriteOffRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public IStationRepository StationRepository { get; set; }

        public PalletCustomerWriteOffRepository(JunFuDbContext junFuDbContext, IStationRepository stationRepository)
        {
            JunFuDbContext = junFuDbContext;
            StationRepository = stationRepository;
        }

        /// <summary>
        /// 取得所有棧板客戶與所屬結帳日
        /// </summary>
        /// <returns></returns>
        public List<PalletCustomerDropDownList> GetTbSuppliersAndCheckoutDate()
        {
            var TbCustomers = JunFuDbContext.TbCustomers.ToList();
            string[] SelfEmployed = new string[] { "000", "002", "101", "001" }; //自營客戶
            string[] SpecialSuppliers = new string[] { "B04", "B05", "C01" };

            IEnumerable<string> LessThanTruckload = StationRepository.GetAll().Select(a => a.StationCode);

            //SupplierCode 3碼客戶
            var supplier = (from c in TbCustomers
                            join s in JunFuDbContext.TbSuppliers on c.SupplierCode equals s.SupplierCode
                            orderby c.SupplierCode
                            where !SelfEmployed.Contains(c.SupplierCode) && !LessThanTruckload.Contains(c.SupplierCode)
                            && !SpecialSuppliers.Contains(c.SupplierCode) && c.StopShippingCode == "0" //正常使用
                            select new PalletCustomerDropDownList { SupplierCode = c.SupplierCode, SupplierCodeAndName = s.SupplierCode + "-" + s.SupplierName, CheckOutDate = c.CheckoutDate }
                       ).Distinct().ToList();

            //SupplierCode 8碼客戶
            var SelfEmployedList = (from c in TbCustomers
                                    orderby c.SupplierCode
                                    where (SelfEmployed.Contains(c.SupplierCode) || SpecialSuppliers.Contains(c.SupplierCode)) && c.StopShippingCode == "0" //正常使用
                                    group c by new { c.MasterCode, c.SecondId, c.CheckoutDate } into g
                                    select new PalletCustomerDropDownList
                                    { SupplierCode = g.Key.MasterCode + g.Key.SecondId, SupplierCodeAndName = g.Key.MasterCode + g.Key.SecondId + "-" + g.Max(a => a.CustomerShortname), CheckOutDate = g.Key.CheckoutDate }).Distinct().ToList();

            var total = supplier.Union(SelfEmployedList).ToList();

            var needDeal = (from t in total
                            orderby t.SupplierCode
                            group t by new { t.SupplierCode, t.SupplierCodeAndName } into g
                            where g.Count() > 1
                            select new PalletCustomerDropDownList { SupplierCode = g.Key.SupplierCode, SupplierCodeAndName = g.Key.SupplierCodeAndName, CheckOutDate = (g.Key.SupplierCode.Length == 3) ? 25 : 31 }).ToList();

            foreach (var temp in total)
            {
                foreach (var n in needDeal)
                {
                    if (temp.SupplierCode == n.SupplierCode)
                        temp.CheckOutDate = n.CheckOutDate;
                }
            }
            //total.ForEach(a => a.CheckOutDate = needDeal.FirstOrDefault(b => b.SupplierCode == a.SupplierCode)?.CheckOutDate); 

            var finish = total.GroupBy(a => new { a.CheckOutDate, a.SupplierCode, a.SupplierCodeAndName }).Select(a => new PalletCustomerDropDownList { SupplierCode = a.Key.SupplierCode, SupplierCodeAndName = a.Key.SupplierCodeAndName, CheckOutDate = a.Key.CheckOutDate }).ToList();

            return finish;
        }

        /// <summary>
        /// 尚未結帳的帳單，手動結帳使用
        /// </summary>
        /// <param name="startDate" ></ param >
        /// <param name="checkoutDate"></param>
        /// <param name="shortCustomerCode"></param>
        /// <returns></returns>
        public List<PalletCustomerWriteOffEntity> GetNotWriteOffCustomerMonthDetail(DateTime startDate, DateTime checkoutDate, string shortCustomerCode = null)
        {
            var TbCustomers = JunFuDbContext.TbCustomers.Distinct().ToList();

            var customer = shortCustomerCode.Length > 3 ? //自營客戶
                           (from c in TbCustomers
                            where string.Concat(c.MasterCode, c.SecondId) == shortCustomerCode
                            select c.CustomerCode)
                            : (from c in TbCustomers //區配商
                               where c.SupplierCode == shortCustomerCode
                               select c.CustomerCode);

            var grid = from tc in JunFuDbContext.TcDeliveryRequests
                       orderby tc.CustomerCode, tc.CheckNumber
                       where tc.LessThanTruckload == false && tc.PrintDate >= startDate && tc.PrintDate < checkoutDate.AddDays(1) && tc.ClosingDate == null
                       && customer.Contains(tc.CustomerCode) && tc.CheckNumber.Length > 0 && tc.CancelDate == null
                       select new PalletCustomerWriteOffEntity
                       {
                           CustomerCode = tc.CustomerCode,
                           PrintDate = tc.PrintDate,
                           SupplierDate = tc.SupplierDate,
                           CustomerName = JunFuDbContext.TbCustomers.Where(a => a.CustomerCode == tc.CustomerCode).FirstOrDefault().CustomerName,
                           CheckNumber = tc.CheckNumber,
                           DeliveryType = tc.PricingType,
                           SupplierArea = tc.SendCity + "-" + tc.SendContact,
                           SendCity = tc.SendCity,
                           ArriveArea = tc.ReceiveCity + "-" + tc.ReceiveContact,
                           ReceiveCity = tc.ReceiveCity,
                           Pieces = tc.Pieces,
                           Plates = tc.Plates,
                           SupplierFee = tc.SupplierFee ?? 0,
                           CSectionFee = tc.CsectionFee ?? 0,
                           AddFee = tc.TurnBoardFee + tc.UpstairsFee + tc.DifficultFee
                       };

            return grid.ToList();
        }

        /// <summary>
        /// 客戶已結帳的帳單
        /// </summary>
        /// <param name="startCheckoutDate"></param>
        /// <param name="endCheckoutDate"></param>
        /// <param name="shortCustomerCode"></param>
        /// <returns></returns>
        public List<PalletCustomerWriteOffEntity> GetAlreadyWriteOffMonthCustomer(DateTime startCheckoutDate, DateTime endCheckoutDate, string shortCustomerCode = null)
        {
            var TbCustomers = JunFuDbContext.TbCustomers.Distinct().ToList();

            var customer = shortCustomerCode.Length > 3 ? //自營客戶
                           (from c in TbCustomers
                            where string.Concat(c.MasterCode, c.SecondId) == shortCustomerCode
                            select c.CustomerCode)
                            : (from c in TbCustomers //區配商
                               where c.SupplierCode == shortCustomerCode
                               select c.CustomerCode);

            var grid = from tc in JunFuDbContext.TcDeliveryRequests
                       orderby tc.CustomerCode, tc.CheckNumber
                       where tc.LessThanTruckload == false && tc.PrintDate >= startCheckoutDate && tc.PrintDate < endCheckoutDate && tc.ClosingDate != null
                       && customer.Contains(tc.CustomerCode) && tc.CheckNumber.Length > 0 && tc.CancelDate == null
                       select new PalletCustomerWriteOffEntity
                       {
                           CustomerCode = tc.CustomerCode,
                           PrintDate = tc.PrintDate,
                           SupplierDate = tc.SupplierDate,
                           //CustomerName = TbCustomers.Where(a => a.CustomerCode == tc.CustomerCode).FirstOrDefault().CustomerName,
                           CheckNumber = tc.CheckNumber,
                           DeliveryType = tc.PricingType,
                           SupplierArea = tc.SendCity + "-" + tc.SendContact,
                           SendCity = tc.SendCity,
                           ArriveArea = tc.ReceiveCity + "-" + tc.ReceiveContact,
                           ReceiveCity = tc.ReceiveCity,
                           Pieces = tc.Pieces,
                           Plates = tc.Plates,
                           SupplierFee = tc.SupplierFee ?? 0,
                           CSectionFee = tc.CsectionFee ?? 0,
                           AddFee = tc.TurnBoardFee + tc.UpstairsFee + tc.DifficultFee
                       };

            return grid.ToList();
        }

        public IEnumerable<TbPriceBusinessDefine> GetLatestDefineFee()
        {
            var TbPriceBusinessDefines = JunFuDbContext.TbPriceBusinessDefines.ToList();
            //取得自訂運價客戶「可用的」最新生效日運價
            var latestFee = TbPriceBusinessDefines
                           .Where(a => DateTime.Now >= a.ActiveDate)
                           .GroupBy(a => a.CustomerCode)
                           .Select(a => new { CustomerCode = a.Key, ActiveDate = a.Max(b => b.ActiveDate) });

            var latestDefineFee = (from l in latestFee
                                   join p in TbPriceBusinessDefines on new { CustomerCode = l.CustomerCode, ActiveDate = l.ActiveDate } equals new { CustomerCode = p.CustomerCode, ActiveDate = p.ActiveDate }
                                   select new { p.StartCity, p.EndCity, p.CustomerCode, p.ActiveDate, p.PricingCode, p.Plate1Price, p.Plate2Price, p.Plate3Price, p.Plate4Price, p.Plate5Price, p.Plate6Price }).Distinct();

            var output = latestDefineFee.Select(p => new TbPriceBusinessDefine { StartCity = p.StartCity, EndCity = p.EndCity, CustomerCode = p.CustomerCode, ActiveDate = p.ActiveDate, PricingCode = p.PricingCode, Plate1Price = p.Plate1Price, Plate2Price = p.Plate2Price, Plate3Price = p.Plate3Price, Plate4Price = p.Plate4Price, Plate5Price = p.Plate5Price, Plate6Price = p.Plate6Price });
            return output;
        }

        public List<TbPriceSupplier> GetPriceSuppliers()
        {
            return JunFuDbContext.TbPriceSuppliers.Where(a => a.PricingCode == "04" || a.ClassLevel == 5).ToList();
        }

        /// <summary>
        /// 取得結帳日前一個月的總表資料
        /// </summary>
        /// <param name="closingDate"></param>
        /// <returns></returns>
        public List<TcDeliveryRequest> GetOneMonthPalletTcDeliveryRequests(DateTime closingDate)
        {
            // with nolock in linq 
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
            , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                return JunFuDbContext.TcDeliveryRequests
                .Where(a => a.LessThanTruckload == false && a.ClosingDate.Equals(closingDate.Date) && a.CancelDate == null).ToList();
            }
        }

        public List<TbItemCode> GetPricingCode()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeSclass == "PM" && a.CodeBclass == "1").ToList();
        }

        public List<TbItemCode> GetSubpoenaCategory()
        {
            return JunFuDbContext.TbItemCodes.Where(a => a.CodeSclass == "S2" && a.CodeBclass == "2").ToList();
        }

        /// <summary>
        /// 客戶已結帳的明細
        /// </summary>
        /// <param name="startCheckoutDate"></param>
        /// <param name="endCheckoutDate"></param>
        /// <param name="shortCustomerCode"></param>
        /// <returns></returns>
        public List<Detail> GetMonthCustomerAlreadyWriteOffDetail(DateTime startCheckoutDate, DateTime endCheckoutDate, string shortCustomerCode = null)
        {
            var TbCustomers = JunFuDbContext.TbCustomers.Distinct().ToList();

            var customer = shortCustomerCode.Length > 3 ? //自營客戶
                           (from c in TbCustomers
                            where string.Concat(c.MasterCode, c.SecondId) == shortCustomerCode
                            select c.CustomerCode)
                            : (from c in TbCustomers //區配商
                               where c.SupplierCode == shortCustomerCode
                               select c.CustomerCode);

            var grid = from tc in JunFuDbContext.TcDeliveryRequests
                       orderby tc.CustomerCode, tc.CheckNumber
                       where tc.LessThanTruckload == false && tc.PrintDate >= startCheckoutDate && tc.PrintDate < endCheckoutDate && tc.ClosingDate != null
                       && customer.Contains(tc.CustomerCode) && tc.CheckNumber.Length > 0 && tc.CancelDate == null
                       select new Detail
                       {
                           CustomerCode = tc.CustomerCode,
                           PrintDate = tc.PrintDate,
                           SupplierDate = tc.SupplierDate,
                           CustomerName = JunFuDbContext.TbCustomers.Where(a => a.CustomerCode == tc.CustomerCode).FirstOrDefault().CustomerName,
                           CheckNumber = tc.CheckNumber,
                           PricingCode = tc.PricingType,
                           SendCity = tc.SendCity,
                           ReceiveCity = tc.ReceiveCity,
                           OrderNumber = tc.OrderNumber,
                           ReceiveContact = tc.ReceiveContact,
                           ReceiveAddress = tc.ReceiveCity + tc.ReceiveArea + tc.ReceiveAddress,
                           LatestArriveOptionDriver = tc.LatestArriveOptionDriver,
                           SendContact = tc.SendContact,
                           SendAddress = tc.SendCity + tc.SendArea + tc.SendAddress,
                           InvoiceDesc = tc.InvoiceDesc,
                           Pieces = tc.Pieces,
                           Plates = tc.Plates,
                           SupplierFee = tc.SupplierFee ?? 0,
                           AddFee = tc.TurnBoardFee + tc.DifficultFee + tc.UpstairsFee
                       };

            return grid.ToList();
        }

        public List<FindCustomerEntity> FindCustomerEntities(string shortCustomerCode)
        {   //若有重複，取pricing_code = 01的客戶資料
            var supplier = GetTbSuppliersAndCheckoutDate();

            var TbCustomers = JunFuDbContext.TbCustomers.Distinct().ToList();

            string[] specialCustomer = new string[] { "0000143002", "00001151005" };

            string[] NotCount = new string[] { "A020000101", "A020001001", "B020001001", "B030000101", "B030000201", "B030000301" };

            var temp = shortCustomerCode.Length > 3 ?
                            (from c in TbCustomers
                             join s in supplier on string.Concat(c.MasterCode, c.SecondId) equals s.SupplierCode
                             where c.StopShippingCode == "0"
                             select new
                             {
                                 PricingCode = c.PricingCode,
                                 SupplierCode = s.SupplierCode,
                                 SupplierCodeAndName = s.SupplierCodeAndName,
                                 CustomerName = c.CustomerName,
                                 CustomerShortname = c.CustomerShortname,
                                 UniformNumber = c.UniformNumbers,
                                 Telephone = c.Telephone,
                                 ShipmentsPrincipal = c.ShipmentsPrincipal,
                                 ShipmentsAddress = c.ShipmentsCity + c.ShipmentsArea + c.ShipmentsRoad,
                                 ShipmentsEmail = c.ShipmentsEmail,
                                 BillingSpecialNeeds = c.BillingSpecialNeeds
                             }) :
                             (from c in TbCustomers
                              join s in supplier on c.SupplierCode equals s.SupplierCode
                              where c.StopShippingCode == "0" && !NotCount.Contains(c.CustomerCode)
                              select new
                              {
                                  PricingCode = c.PricingCode,
                                  SupplierCode = s.SupplierCode,
                                  SupplierCodeAndName = s.SupplierCodeAndName,
                                  CustomerName = c.CustomerName,
                                  CustomerShortname = c.CustomerShortname,
                                  UniformNumber = c.UniformNumbers,
                                  Telephone = c.Telephone,
                                  ShipmentsPrincipal = c.ShipmentsPrincipal,
                                  ShipmentsAddress = c.ShipmentsCity + c.ShipmentsArea + c.ShipmentsRoad,
                                  ShipmentsEmail = c.ShipmentsEmail,
                                  BillingSpecialNeeds = c.BillingSpecialNeeds
                              });
            var repeatData = from t in temp
                             group t by new { t.SupplierCode, t.SupplierCodeAndName } into g
                             where g.Count() > 1
                             select new { g.Key.SupplierCode, g.Key.SupplierCodeAndName };

            var repeatDataCustomer = from r in repeatData
                                     from c in TbCustomers
                                     where ((r.SupplierCode == c.SupplierCode) || (r.SupplierCode == string.Concat(c.MasterCode, c.SecondId)))
                                     && c.PricingCode != "01"
                                     select new
                                     {
                                         PricingCode = c.PricingCode,
                                         SupplierCode = r.SupplierCode,
                                         SupplierCodeAndName = r.SupplierCodeAndName,
                                         CustomerName = c.CustomerName,
                                         CustomerShortname = c.CustomerShortname,
                                         UniformNumber = c.UniformNumbers,
                                         Telephone = c.Telephone,
                                         ShipmentsPrincipal = c.ShipmentsPrincipal,
                                         ShipmentsAddress = c.ShipmentsCity + c.ShipmentsArea + c.ShipmentsRoad,
                                         ShipmentsEmail = c.ShipmentsEmail,
                                         BillingSpecialNeeds = c.BillingSpecialNeeds
                                     };

            //pricing code 不為 01 論板的客戶
            var special = from c in TbCustomers
                          join s in supplier on string.Concat(c.MasterCode, c.SecondId) equals s.SupplierCode
                          where c.StopShippingCode == "0" && specialCustomer.Contains(c.CustomerCode)
                          select new
                          {
                              PricingCode = c.PricingCode,
                              SupplierCode = s.SupplierCode,
                              SupplierCodeAndName = s.SupplierCodeAndName,
                              CustomerName = c.CustomerName,
                              CustomerShortname = c.CustomerShortname,
                              UniformNumber = c.UniformNumbers,
                              Telephone = c.Telephone,
                              ShipmentsPrincipal = c.ShipmentsPrincipal,
                              ShipmentsAddress = c.ShipmentsCity + c.ShipmentsArea + c.ShipmentsRoad,
                              ShipmentsEmail = c.ShipmentsEmail,
                              BillingSpecialNeeds = c.BillingSpecialNeeds
                          };

            var customer = temp.Except(repeatDataCustomer).Union(special)
                           .Where(a => a.SupplierCode == shortCustomerCode).Select(a => new FindCustomerEntity
                           {
                               PricingCode = a.PricingCode,
                               CustomerName = a.CustomerName,
                               CustomerShortname = a.CustomerShortname,
                               UniformNumber = a.UniformNumber,
                               Telephone = a.Telephone,
                               ShipmentsPrincipal = a.ShipmentsPrincipal,
                               ShipmentsAddress = a.ShipmentsAddress,
                               ShipmentsEmail = a.ShipmentsEmail,
                               BillingSpecialNeeds = a.BillingSpecialNeeds
                           });

            return customer.ToList();
        }

        public List<PalletCustomerRequestPaymentLog> GetPalletCustomerRequestPaymentLogs(DateTime startMonth, DateTime endMonth, string shortCustomerCode)
        {
            return JunFuDbContext.PalletCustomerRequestPaymentLogs.Where(a => a.ShortCustomerCode.Equals(shortCustomerCode) && a.ClosingDate < endMonth && a.ClosingDate >= startMonth && a.Payment > 0).ToList();
        }

        public List<PalletCustomerRequestPaymentLog> GetPalletCustomerRequestPaymentLogsByClosingDate(DateTime closingDate)
        {
            return (from p in JunFuDbContext.PalletCustomerRequestPaymentLogs
                    where p.ClosingDate.Date.Equals(closingDate.Date) && p.Payment > 0
                    select p
                    ).ToList();
        }

        public void InsertPalletCustomerRequestPaymentLog(PalletCustomerRequestPaymentLog palletCustomerRequestPaymentLog)
        {
            palletCustomerRequestPaymentLog.CreateDate = DateTime.Now;

            JunFuDbContext.PalletCustomerRequestPaymentLogs.Add(palletCustomerRequestPaymentLog);

            JunFuDbContext.SaveChanges();
        }

        public PalletCustomerRequestPaymentLog GetManualRequestPaymentDateByCustomer(string shortCustomerCode)
        {
            var data = (from p in JunFuDbContext.PalletCustomerRequestPaymentLogs
                        orderby p.ClosingDate descending
                        where p.ShortCustomerCode.Equals(shortCustomerCode)
                        select p).FirstOrDefault();
            if (data == null)
            {
                return null;
            }
            return data;
        }

        /// <summary>
        /// 手動結帳時取得客戶歷史帳單的上一次結帳日
        /// </summary>
        /// <param name="closingDate"></param>
        /// <param name="shortCustomerCode"></param>
        /// <returns></returns>
        public DateTime ForManualGetLastClosingDate(DateTime closingDate, string shortCustomerCode)
        {
            var supplier = GetTbSuppliersAndCheckoutDate();

            int? checkoutDate = supplier.Where(a => a.SupplierCode == shortCustomerCode).FirstOrDefault().CheckOutDate;

            DateTime LastMonthClosingDate = new DateTime();

            if (checkoutDate == 31 || checkoutDate == 30)
                LastMonthClosingDate = new DateTime(closingDate.Year, closingDate.Month, 01); //上個月結帳日加1天都會是下個月1號 
            else
                LastMonthClosingDate = new DateTime(closingDate.AddMonths(-1).Year, closingDate.AddMonths(-1).Month, (checkoutDate + 1) ?? 0);

            var data = GetManualRequestPaymentDateByCustomer(shortCustomerCode);

            DateTime LastClosingDate;

            if (data != null)
                return LastClosingDate = data.ClosingDate.AddDays(1).Date;
            else
                return LastMonthClosingDate;
        }

        public decimal[] GetLineChartArray(string customerCode, int year)
        {
            var data = JunFuDbContext.PalletCustomerRequestPaymentLogs.Where(x => x.RequestEndDate == null ? false : ((DateTime)(x.RequestEndDate)).Year == year && x.ShortCustomerCode == customerCode).ToArray();//.Select(x=>x.Key).ToArray();

            decimal[] result = new decimal[12];
            for (var i = 0; i < result.Length; i++)
            {
                result[i] = Math.Round(data.Where(x => x.RequestEndDate.GetValueOrDefault().Month == i + 1).Sum(x => x.Payment ?? 0) / (decimal)1.05);
            }

            return result;
        }

        /// <summary>
        /// 論件自訂運價
        /// </summary>
        /// <returns></returns>
        public List<PalletPiecesShippingFeeDefine> GetPalletPiecesShippingFeeDefine()
        {
            var PalletPiecesShippingFeeDefine = JunFuDbContext.PalletPiecesShippingFeeDefines.ToList();

            var latest = PalletPiecesShippingFeeDefine
                         .Where(a => DateTime.Now >= a.ActiveDate)
                         .GroupBy(a => a.CustomerCode)
                         .Select(a => new { CustomerCode = a.Key, ActiveDate = a.Max(b => b.ActiveDate) });
            
            var output = (from l in latest
                          join p in PalletPiecesShippingFeeDefine on new { CustomerCode = l.CustomerCode, ActiveDate = l.ActiveDate } equals new { CustomerCode = p.CustomerCode, ActiveDate = p.ActiveDate }
                          select p).ToList();
            return output;
        }

        /// <summary>
        /// 論件公版運價
        /// </summary>
        /// <returns></returns>
        public List<PalletPiecesShippingFeePublicVersion> GetShippingFeePublicVersions()
        {
            return JunFuDbContext.PalletPiecesShippingFeePublicVersions.ToList();
        }
    }
}
