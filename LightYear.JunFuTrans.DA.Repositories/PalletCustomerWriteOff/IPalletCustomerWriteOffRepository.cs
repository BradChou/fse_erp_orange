﻿using LightYear.JunFuTrans.BL.BE.PalletCustomerWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.PalletCustomerWriteOff
{
    public interface IPalletCustomerWriteOffRepository
    {
        List<PalletCustomerDropDownList> GetTbSuppliersAndCheckoutDate();
        IEnumerable<TbPriceBusinessDefine> GetLatestDefineFee();
        List<PalletCustomerWriteOffEntity> GetNotWriteOffCustomerMonthDetail(DateTime startDate, DateTime checkoutDate, string shortCustomerCode = null);
        List<TbPriceSupplier> GetPriceSuppliers();
        List<TcDeliveryRequest> GetOneMonthPalletTcDeliveryRequests(DateTime closingDate);
        List<PalletCustomerWriteOffEntity> GetAlreadyWriteOffMonthCustomer(DateTime startCheckoutDate, DateTime endCheckoutDate, string shortCustomerCode = null);
        List<TbItemCode> GetPricingCode();
        List<Detail> GetMonthCustomerAlreadyWriteOffDetail(DateTime startCheckoutDate, DateTime endCheckoutDate, string shortCustomerCode = null);
        List<TbItemCode> GetSubpoenaCategory();
        List<FindCustomerEntity> FindCustomerEntities(string shortCustomerCode);
        List<PalletCustomerRequestPaymentLog> GetPalletCustomerRequestPaymentLogs(DateTime startMonth, DateTime endMonth, string shortCustomerCode);
        List<PalletCustomerRequestPaymentLog> GetPalletCustomerRequestPaymentLogsByClosingDate(DateTime closingDate);
        void InsertPalletCustomerRequestPaymentLog(PalletCustomerRequestPaymentLog palletCustomerRequestPaymentLog);
        PalletCustomerRequestPaymentLog GetManualRequestPaymentDateByCustomer(string shortCustomerCode);
        DateTime ForManualGetLastClosingDate(DateTime closingDate, string shortCustomerCode);
        decimal[] GetLineChartArray(string customerCode, int year);
        List<PalletPiecesShippingFeeDefine> GetPalletPiecesShippingFeeDefine();
        List<PalletPiecesShippingFeePublicVersion> GetShippingFeePublicVersions();
    }
}
