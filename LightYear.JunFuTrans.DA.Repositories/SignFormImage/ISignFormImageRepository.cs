﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.SignForm;

namespace LightYear.JunFuTrans.DA.Repositories.SignFormImage
{
    public interface ISignFormImageRepository
    {
        IEnumerable<SignFormEntity> GetDataByDatePeriodAndCustomerCode(DateTime deliveryDateStart, DateTime deliveryDateEnd, string customerCode, string stationScode);
        IEnumerable<SignFormEntity> GetDataByCustomerCode(DateTime deliveryDateStart, DateTime deliveryDateEnd, string customerCode);
        IEnumerable<SignFormEntity> GetDataByDatePeriod(DateTime deliveryDateStart, DateTime deliveryDateEnd, string stationScode);
    }
}
