﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.DA.Repositories.ShopeeLog
{
    public interface IShopeePickupInfoRepository
    {
        long Insert(ShopeePickupInfo shopeePickupInfo);

        void InsertDatas(List<ShopeePickupInfo> shopeeCbminfos);
    }
}
