﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using System.Linq;
using System.Security.Cryptography;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

namespace LightYear.JunFuTrans.DA.Repositories.TMS
{

    public class BSectionFixedRunRepository : IBSectionFixedRunRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public BSectionFixedRunRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        /// <summary>
        /// 取得固定班次列表
        /// </summary>
        /// <param name="operationStatus">運行狀態</param>
        /// <returns></returns>
        public List<BSectionFixedRun> GetBSectionFixedRunList(OperationStatus operationStatus)
        {
            List<BSectionFixedRun> result = null;
            IQueryable<BSectionFixedRun> data = null;

            switch (operationStatus)
            {
                case OperationStatus.Normal:
                    data = from bSectionFixedRun in this.JunFuDbContext.BSectionFixedRuns where bSectionFixedRun.IsDeleted != true && bSectionFixedRun.IsActive.Equals(true) select bSectionFixedRun;
                    result = data.ToList();
                    break;

                case OperationStatus.Stopping:
                    data = from bSectionFixedRun in this.JunFuDbContext.BSectionFixedRuns where bSectionFixedRun.IsDeleted != true && bSectionFixedRun.IsActive.Equals(false) select bSectionFixedRun;
                    result = data.ToList();
                    break;
                case OperationStatus.All:
                    data = from bSectionFixedRun in this.JunFuDbContext.BSectionFixedRuns where bSectionFixedRun.IsDeleted != true select bSectionFixedRun;
                    result = data.ToList();
                    break;
                default:
                    data = null;
                    result = data.ToList();
                    break;
            }

            return result;
        }

        public List<BSectionRelayStop> GetBSectionRelayStopList(int runId)
        {
            List<BSectionRelayStop> result = new List<BSectionRelayStop>();

            var data = from bSectionMiddleStop in this.JunFuDbContext.BSectionStops join bSectionMiddleStopMapping in this.JunFuDbContext.BSectionMiddleStopMappings on bSectionMiddleStop.Id equals bSectionMiddleStopMapping.StopId where bSectionMiddleStopMapping.RunId.Equals(runId) select new { bSectionMiddleStop.Id, bSectionMiddleStop.Name, bSectionMiddleStopMapping.ArriveAt, bSectionMiddleStopMapping.DispatchAt, bSectionMiddleStopMapping.RunFlowTypeId };

            foreach (var stop in data)
            {
                BSectionRelayStop newRelay = new BSectionRelayStop(stop.Id, stop.Name, stop.ArriveAt, stop.DispatchAt, stop.RunFlowTypeId);
                if (newRelay != null)
                {
                    result.Add(newRelay);
                }
            }

            return result;
        }

        public BSectionStop GetStopById(int Id)
        {
            BSectionStop result = (from bSectionStop in this.JunFuDbContext.BSectionStops where bSectionStop.Id.Equals(Id) select bSectionStop).FirstOrDefault();

            return result;
        }

        public CarType GetCarTypeById(int Id)
        {
            CarType result = (from carType in this.JunFuDbContext.CarTypes where carType.Id.Equals(Id) select carType).FirstOrDefault();

            return result;
        }

        public List<CarType> GetCarTypes()
        {
            List<CarType> result = (from carType in this.JunFuDbContext.CarTypes select carType).ToList();

            return result;
        }

        public List<RunFlowType> GetRunFlowTypes()
        {
            List<RunFlowType> result = (from runFlowType in this.JunFuDbContext.RunFlowTypes select runFlowType).ToList();

            return result;
        }

        public RunFlowType GetRunFlowTypeById(int Id)
        {
            RunFlowType result = (from runFlowType in this.JunFuDbContext.RunFlowTypes where runFlowType.Id.Equals(Id) select runFlowType).FirstOrDefault();

            return result;
        }

        public int GetMaxNumberByAttribute(bool isLtl)
        {
            var maxRunNumber = (from bSectionFixedRun in this.JunFuDbContext.BSectionFixedRuns where bSectionFixedRun.IsLtl.Equals(isLtl) orderby bSectionFixedRun.RunNumber descending select bSectionFixedRun.RunNumber).FirstOrDefault();

            return maxRunNumber;
        }

        public BSectionFixedRun InsertBSectionFixedRun(BSectionFixedRun bSectionFixedRun)
        {
            JunFuDbContext.Add(bSectionFixedRun);
            JunFuDbContext.SaveChanges();
            return bSectionFixedRun;
        }

        public BSectionFixedRun UpdateBSectionFixedRun(BSectionFixedRun newRunData)
        {
            var updatedRun = (from storedRun in this.JunFuDbContext.BSectionFixedRuns where storedRun.Id == newRunData.Id select storedRun).FirstOrDefault();


            if (updatedRun != null)
            {
                // 不可更改 ID, 班次號, 以及屬性
                updatedRun.FromId = newRunData.FromId;
                updatedRun.ToId = newRunData.ToId;
                updatedRun.CarTypeId = newRunData.CarTypeId;
                updatedRun.RunFlowTypeId = newRunData.RunFlowTypeId;
                updatedRun.DoesSundayDispatch = newRunData.DoesSundayDispatch;
                updatedRun.DoesMondayDispatch = newRunData.DoesMondayDispatch;
                updatedRun.DoesTuesdayDispatch = newRunData.DoesTuesdayDispatch;
                updatedRun.DoesWednesdayDispatch = newRunData.DoesWednesdayDispatch;
                updatedRun.DoesThursdayDispatch = newRunData.DoesThursdayDispatch;
                updatedRun.DoesFridayDispatch = newRunData.DoesFridayDispatch;
                updatedRun.DoesSaturdayDispatch = newRunData.DoesSaturdayDispatch;
                updatedRun.GoStartAt = newRunData.GoStartAt;
                updatedRun.GoEndAt = newRunData.GoEndAt;
                updatedRun.BackStartAt = newRunData.BackStartAt;
                updatedRun.BackEndAt = newRunData.BackEndAt;
                updatedRun.IsActive = newRunData.IsActive;
            }

            JunFuDbContext.SaveChanges();

            return updatedRun;
        }

        public BSectionMiddleStopMapping InsertBSectionMiddleStopMapping(BSectionMiddleStopMapping bSectionMiddleStopMapping)
        {
            JunFuDbContext.Add(bSectionMiddleStopMapping);
            JunFuDbContext.SaveChanges();
            return bSectionMiddleStopMapping;
        }

        public int DeleteAllBSectionMiddleStopMappingsByRunID(int id)
        {
            var mappings = from mapping in this.JunFuDbContext.BSectionMiddleStopMappings where mapping.RunId == id select mapping;

            int deletedCount = mappings.Count();

            foreach (var mapping in mappings)
            {
                this.JunFuDbContext.BSectionMiddleStopMappings.Remove(mapping);
            }

            this.JunFuDbContext.SaveChanges();

            return deletedCount;
        }

        public List<BSectionStop> GetBSectionStops()
        {
            List<BSectionStop> result = (from bSectionStop in this.JunFuDbContext.BSectionStops select bSectionStop).ToList();

            return result;
        }

        public List<TbSupplier> GetSuppliersByStationID(int id)
        {
            List<TbSupplier> result = (from stopSupplierMapping in this.JunFuDbContext.BSectionStopSupplierMappings join tbSupplier in this.JunFuDbContext.TbSuppliers on stopSupplierMapping.StopId equals id where stopSupplierMapping.SupplierId == tbSupplier.SupplierId select tbSupplier).ToList();

            return result;
        }

        public Dictionary<int, List<decimal>> GetMappingByStopIDs(List<int> ids)
        {
            var q = JunFuDbContext.BSectionStopSupplierMappings.Where(s => ids.Contains(s.StopId)).ToList();

            Dictionary<int, List<decimal>> result = q.GroupBy(s => s.StopId)
                .Select(s => new { s.Key, SupplierId = s.Select(s2 => s2.SupplierId).ToList() })
                .ToDictionary(s => s.Key, s => s.SupplierId);

            return result;
        }

        public List<TbSupplier> GetSuppliers()
        {
            List<TbSupplier> result = (from tbSupplier in this.JunFuDbContext.TbSuppliers where !string.IsNullOrWhiteSpace(tbSupplier.SupplierCode) orderby tbSupplier.SupplierCode select tbSupplier).ToList();

            return result;
        }

        public BSectionStop CreateNewBSectionStop(BSectionStop newStopData)
        {
            var newStop = new BSectionStop
            {
                Name = newStopData.Name,
                CityId = newStopData.CityId,
                DistrictId = newStopData.DistrictId,
                Address = newStopData.Address,
                Position = newStopData.Position,
                IsActive = newStopData.IsActive,
            };

            JunFuDbContext.Add(newStop);
            JunFuDbContext.SaveChanges();
            return newStop;
        }

        public BSectionStopSupplierMapping CreateNewStopSupplierMapping(int stopID, int supplierID)
        {
            var newMapping = new BSectionStopSupplierMapping
            {
                StopId = stopID,
                SupplierId = supplierID,
            };

            JunFuDbContext.Add(newMapping);
            JunFuDbContext.SaveChanges();
            return newMapping;
        }

        public int DeleteAllBSectionStopSupplierMappingByStopId(int id)
        {
            var mappings = (from mapping in this.JunFuDbContext.BSectionStopSupplierMappings where mapping.StopId == id select mapping).ToList();

            int deletedCount = mappings.Count();

            foreach (var mapping in mappings)
            {
                this.JunFuDbContext.BSectionStopSupplierMappings.Remove(mapping);
            }

            this.JunFuDbContext.SaveChanges();

            return deletedCount;
        }

        public BSectionStop UpdateBSectionStop(BSectionStop newStopData)
        {
            var updatedStop = (from storedStop in this.JunFuDbContext.BSectionStops where storedStop.Id == newStopData.Id select storedStop).FirstOrDefault();

            if (updatedStop != null)
            {
                updatedStop.Name = newStopData.Name;
                updatedStop.CityId = newStopData.CityId;
                updatedStop.DistrictId = newStopData.DistrictId;
                updatedStop.Address = newStopData.Address;
                updatedStop.Position = newStopData.Position;
                updatedStop.IsActive = newStopData.IsActive;
            };

            JunFuDbContext.SaveChanges();
            return updatedStop;
        }
    }
}
