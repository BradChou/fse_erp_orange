﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.TMS
{
    public class LtWarehouseRepository : ILtWarehouseRepository
    {
        public JunFuDbContext DbContext { get; }

        public LtWarehouseRepository(JunFuDbContext junFuDbContext)
        {
            DbContext = junFuDbContext;
        }

        public List<LtWarehouse> GetLtWarehouses()
        {
            return DbContext.LtWarehouses.ToList();
        }

        public Dictionary<int, List<int>> GetWarehouseStationMapping()
        {
            var q = DbContext.LtWarehouseStationMappings.ToList();

            Dictionary<int, List<int>> result = q.GroupBy(s => s.WarehouseId)
                .Select(s => new { s.Key, SupplierId = s.Select(s2 => s2.StationId).ToList() })
                .ToDictionary(s => s.Key, s => s.SupplierId);

            return result;
        }

        public List<LtWarehouseBoxCount> GetBoxCountsByPrintDate(DateTime shipDate)
        {
            return DbContext.LtWarehouseBoxCounts.Where(c => c.ShipDate >= shipDate && c.ShipDate < shipDate.AddDays(1)).ToList();
        }

        public bool InsertBoxCountEntity(LtWarehouseBoxCount entity)
        {
            DbContext.LtWarehouseBoxCounts.Add(entity);
            DbContext.SaveChanges();

            return true;
        }

        public bool UpdateBoxCountEntity(LtWarehouseBoxCount entity)
        {
            var dataInDb = DbContext.LtWarehouseBoxCounts.FirstOrDefault(c => c.Id == entity.Id);

            dataInDb.Udate = DateTime.Now;
            dataInDb.Uuser = entity.Uuser;
            dataInDb.ActualFlowCount = entity.ActualFlowCount;
            dataInDb.ActualMixCount = entity.ActualMixCount;

            DbContext.SaveChanges();

            return true;
        }
    }
}
