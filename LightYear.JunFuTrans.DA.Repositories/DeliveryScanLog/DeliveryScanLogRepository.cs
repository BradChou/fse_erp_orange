﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryScanLog
{
    public class DeliveryScanLogRepository : IDeliveryScanLogRepository
    {
        public JunFuDbContext JunFuDbContext;
        public DeliveryScanLogRepository(JunFuDbContext junFuDbContext)
        {
            JunFuDbContext = junFuDbContext;
        }
        public bool InsertIntoScanLogSend(string driverCode, string checkNumber, string scanItem, string scanDate)
        {
            DateTime dt = new DateTime(int.Parse(scanDate.Substring(0, 4)), int.Parse(scanDate.Substring(4, 2)), int.Parse(scanDate.Substring(6, 2)), int.Parse(scanDate.Substring(8, 2)), int.Parse(scanDate.Substring(10, 2)), int.Parse(scanDate.Substring(12, 2)));
            TtDeliveryScanLog entity = new TtDeliveryScanLog() { DriverCode = driverCode, CheckNumber = checkNumber, ScanItem = scanItem, ScanDate = dt , Cdate= DateTime.Now, Pieces = 1};
            JunFuDbContext.TtDeliveryScanLogs.Add(entity);
            try
            {
                JunFuDbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
