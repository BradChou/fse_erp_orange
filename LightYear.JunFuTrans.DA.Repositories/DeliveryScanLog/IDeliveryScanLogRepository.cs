﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DeliveryScanLog
{
    public interface IDeliveryScanLogRepository
    {
        bool InsertIntoScanLogSend(string driverCode, string checkNumber, string scanItem, string scanDate);
    }
}
