﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.ScheduleTaskRepo
{
    public interface IScheduleTaskRepository
    {
        ScheduleTask Insert(ScheduleTask scheduleTask);

        ScheduleTask Update(ScheduleTask scheduleTask);
        ScheduleTask GetSignImageJob();
    }
}
