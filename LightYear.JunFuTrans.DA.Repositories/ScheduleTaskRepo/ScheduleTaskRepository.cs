﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.DA.Repositories.ScheduleTaskRepo
{
    public class ScheduleTaskRepository: IScheduleTaskRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public ScheduleTaskRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }
        public ScheduleTask Insert(ScheduleTask scheduleTask)
        {
            scheduleTask.CreateDate = DateTime.Now;

            JunFuDbContext.ScheduleTask.Add(scheduleTask);

            JunFuDbContext.SaveChanges();

            return scheduleTask;
        }
        public ScheduleTask Update(ScheduleTask scheduleTask)
        {

            JunFuDbContext.ScheduleTask.Update(scheduleTask);

            JunFuDbContext.SaveChanges();

            return scheduleTask;
        }
        public ScheduleTask GetSignImageJob()
        {
            //var a = JunFuDbContext.ScheduleTask.Where(p=>true) ;
            var result = JunFuDbContext.ScheduleTask.FirstOrDefault(p => p.TaskID == 5);
            return result;
        }
    }
}
