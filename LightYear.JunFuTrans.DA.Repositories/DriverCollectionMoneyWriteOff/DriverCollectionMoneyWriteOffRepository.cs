﻿using LightYear.JunFuTrans.BL.BE.DriverCollectionMoneyWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System.Transactions;

namespace LightYear.JunFuTrans.DA.Repositories.DriverCollectionMoneyWriteOff
{
    public class DriverCollectionMoneyWriteOffRepository : IDriverCollectionMoneyWriteOffRepository
    {
        public JunFuDbContext JunFuDbContext { get; private set; }
        public JunFuTransDbContext JunFuTransDbContext { get; private set; }

        public DriverCollectionMoneyWriteOffRepository(JunFuDbContext junFuDbContext, JunFuTransDbContext junFuTransDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
            this.JunFuTransDbContext = junFuTransDbContext;
        }

        public List<DriverCollectionMoneyWriteOffEntity> GetAllByDateAndStation(string scode, DateTime start, DateTime end)
        {
            // with nolock in linq 
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
            , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {
                var data = (from dr in JunFuDbContext.TcDeliveryRequests
                            join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
                            from ta in temp3.DefaultIfEmpty()
                            join au in JunFuDbContext.AccountUpload on dr.CheckNumber equals au.CheckNumber into temp4
                            from au in temp4.DefaultIfEmpty()
                                //join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
                                //from sl in temp4.DefaultIfEmpty()
                            where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                            & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                            & dr.DeliveryType != "R" & dr.AreaArriveCode == scode
                            & dr.LatestArriveOption == "3"
                            select new DriverCollectionMoneyWriteOffEntity
                            {
                                ArriveStationName = dr.AreaArriveCode,
                                CheckNumber = dr.CheckNumber,
                                CustomerCode = dr.CustomerCode,
                                CustomerName = ta.CustomerName,
                                ReceiveContact = dr.ReceiveContact,
                                CollectionMoney = dr.CollectionMoney,
                                DriverCode = dr.LatestArriveOptionDriver,
                                ScanDate = dr.LatestArriveOptionDate,
                                IsUpload = (au.IsDel == 0 ? "Y" : "N"),
                                ReceivedAmount = (au.IsDel == 0 ? au.ReceivedAmount.ToString() : ""),
                                //AccountDate = (string.IsNullOrEmpty(au.AccountDate.ToShortDateString())?"": au.AccountDate.ToShortDateString())
                                AccountDate = au.AccountDate == null ? "" : au.AccountDate.ToShortDateString()
                                //Cdate = sl.Cdate
                            }).Distinct().ToList();
                return data;
                //var latestScan = from dr in JunFuDbContext.TcDeliveryRequests
                //                 join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
                //                 from sl in temp4.DefaultIfEmpty()
                //                 where dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                //                 & dr.AreaArriveCode == scode
                //                 & dr.LatestArriveOption == "3"
                //                 & dr.DeliveryType != "R"
                //                 & dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                //                 group sl by sl.CheckNumber into g
                //                 select new  { CheckNumber = g.Key, Cdate = g.Max(s => s.Cdate) };



                //var returnData = from ls in latestScan
                //                 join dr in JunFuDbContext.TcDeliveryRequests on ls.CheckNumber equals dr.CheckNumber into temp5
                //                 from dr in temp5.DefaultIfEmpty()
                //                 join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
                //                 from ta in temp3.DefaultIfEmpty()

                //                 select new DriverCollectionMoneyWriteOffEntity
                //                 {
                //                     ArriveStationName = dr.AreaArriveCode,
                //                     CheckNumber = dr.CheckNumber,
                //                     CustomerCode = dr.CustomerCode,
                //                     CustomerName = ta.CustomerName,
                //                     ReceiveContact = dr.ReceiveContact,
                //                     CollectionMoney = dr.CollectionMoney,
                //                     DriverCode = dr.LatestArriveOptionDriver,
                //                     ScanDate = dr.LatestArriveOptionDate,
                //                     Cdate = ls.Cdate
                //                 };



                //return returnData.Distinct().ToList();


            }
        }

        public List<DriverCollectionMoneyWriteOffEntity> GetAllByDate(DateTime start, DateTime end, string station_level, string management, string station_area,string station_scode)
        {
            string managementList = "";
            string station_scodeList = "";
            string station_areaList = "";
            // with nolock in linq 
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
             , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
            {

                if (station_level.Equals("1"))
                {
                    var da = (from tbstation in JunFuDbContext.TbStations
                              where management.Contains(tbstation.management) 
                              select new TbStation { management = tbstation.management, StationScode = tbstation.StationScode });

                    if (da.Count() > 0)
                    {
                        foreach (var item in da)
                        {
                            managementList += "'";
                            managementList += item.StationScode;
                            managementList += "'";
                            managementList += ",";
                        }
                        managementList = managementList.Remove(managementList.ToString().LastIndexOf(','), 1);
                    }

                    var data = (from dr in JunFuDbContext.TcDeliveryRequests
                                join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
                                from ta in temp3.DefaultIfEmpty()
                                join au in JunFuDbContext.AccountUpload on dr.CheckNumber equals au.CheckNumber into temp4
                                from au in temp4.DefaultIfEmpty()
                                    //join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
                                    //from sl in temp4.DefaultIfEmpty()
                                where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                                & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                                & managementList.Contains(dr.AreaArriveCode)
                                & dr.DeliveryType != "R" & dr.LatestArriveOption == "3"
                                select new DriverCollectionMoneyWriteOffEntity
                                {

                                    CheckNumber = dr.CheckNumber,
                                    ScanDate = dr.LatestArriveOptionDate,
                                    ArriveStationName = dr.AreaArriveCode,
                                    CustomerCode = dr.CustomerCode,
                                    CustomerName = ta.CustomerName,
                                    ReceiveContact = dr.ReceiveContact,
                                    CollectionMoney = dr.CollectionMoney,
                                    DriverCode = dr.LatestArriveOptionDriver,
                                    IsUpload = (au.IsDel == 0 ? "Y" : "N"),
                                    ReceivedAmount = (au.IsDel == 0 ? au.ReceivedAmount.ToString() : ""),
                                    //AccountDate = (string.IsNullOrEmpty(au.AccountDate.ToShortDateString()) ? "" : au.AccountDate.ToShortDateString())
                                    AccountDate = au.AccountDate == null ? "" : au.AccountDate.ToShortDateString()

                                    //Cdate = sl.Cdate

                                }).OrderBy(e=>e.ArriveStationName).Distinct().ToList();

                    var data1 = data.OrderBy(a=>a.ArriveStationName).ToList();

                    return data1;

                }
                else if (station_level.Equals("2"))
                {
                    var da = (from tbstation in JunFuDbContext.TbStations
                              where station_scode.Contains(tbstation.StationScode) 
                              select new TbStation { station_area = tbstation.station_area, StationScode = tbstation.StationScode });

                    if (da.Count() > 0)
                    {
                        foreach (var item in da)
                        {
                            station_scodeList += "'";
                            station_scodeList += item.StationScode;
                            station_scodeList += "'";
                            station_scodeList += ",";
                        }
                        station_scodeList = station_scodeList.Remove(station_scodeList.ToString().LastIndexOf(','), 1);
                    }

                    var data = (from dr in JunFuDbContext.TcDeliveryRequests
                                join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
                                from ta in temp3.DefaultIfEmpty()
                                join au in JunFuDbContext.AccountUpload on dr.CheckNumber equals au.CheckNumber into temp4
                                from au in temp4.DefaultIfEmpty()
                                    //join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
                                    //from sl in temp4.DefaultIfEmpty()
                                where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                                & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                                & station_scodeList.Contains(dr.AreaArriveCode)
                                & dr.DeliveryType != "R" & dr.LatestArriveOption == "3"
                                select new DriverCollectionMoneyWriteOffEntity
                                {

                                    CheckNumber = dr.CheckNumber,
                                    ScanDate = dr.LatestArriveOptionDate,
                                    ArriveStationName = dr.AreaArriveCode,
                                    CustomerCode = dr.CustomerCode,
                                    CustomerName = ta.CustomerName,
                                    ReceiveContact = dr.ReceiveContact,
                                    CollectionMoney = dr.CollectionMoney,
                                    DriverCode = dr.LatestArriveOptionDriver,
                                    IsUpload = (au.IsDel == 0 ? "Y" : "N"),
                                    ReceivedAmount = (au.IsDel == 0 ? au.ReceivedAmount.ToString() : ""),
                                    //AccountDate = (string.IsNullOrEmpty(au.AccountDate.ToShortDateString()) ? "" : au.AccountDate.ToShortDateString())
                                    AccountDate = au.AccountDate == null ? "" : au.AccountDate.ToShortDateString()

                                    //Cdate = sl.Cdate

                                }).OrderBy(e => e.ArriveStationName).Distinct().ToList();
                    var data1 = data.OrderBy(a => a.ArriveStationName).ToList();

                    return data1;
                }
                else if (station_level.Equals("4"))
                {
                    var da = (from tbstation in JunFuDbContext.TbStations
                              where tbstation.station_area == Int32.Parse(station_area)
                              select new TbStation { station_area = tbstation.station_area, StationScode = tbstation.StationScode });

                    if (da.Count() > 0)
                    {
                        foreach (var item in da)
                        {
                            station_areaList += "'";
                            station_areaList += item.StationScode;
                            station_areaList += "'";
                            station_areaList += ",";
                        }
                        station_areaList = station_areaList.Remove(station_areaList.ToString().LastIndexOf(','), 1);
                    }

                    var data = (from dr in JunFuDbContext.TcDeliveryRequests
                                join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
                                from ta in temp3.DefaultIfEmpty()
                                join au in JunFuDbContext.AccountUpload on dr.CheckNumber equals au.CheckNumber into temp4
                                from au in temp4.DefaultIfEmpty()
                                    //join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
                                    //from sl in temp4.DefaultIfEmpty()
                                where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                                & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                                & station_areaList.Contains(dr.AreaArriveCode)
                                & dr.DeliveryType != "R" & dr.LatestArriveOption == "3"
                                select new DriverCollectionMoneyWriteOffEntity
                                {

                                    CheckNumber = dr.CheckNumber,
                                    ScanDate = dr.LatestArriveOptionDate,
                                    ArriveStationName = dr.AreaArriveCode,
                                    CustomerCode = dr.CustomerCode,
                                    CustomerName = ta.CustomerName,
                                    ReceiveContact = dr.ReceiveContact,
                                    CollectionMoney = dr.CollectionMoney,
                                    DriverCode = dr.LatestArriveOptionDriver,
                                    IsUpload = (au.IsDel == 0 ? "Y" : "N"),
                                    ReceivedAmount = (au.IsDel == 0 ? au.ReceivedAmount.ToString() : ""),
                                    //AccountDate = (string.IsNullOrEmpty(au.AccountDate.ToShortDateString()) ? "" : au.AccountDate.ToShortDateString())
                                    AccountDate = au.AccountDate == null ? "" : au.AccountDate.ToShortDateString()

                                    //Cdate = sl.Cdate

                                }).OrderBy(e => e.ArriveStationName).Distinct().ToList();
                    var data1 = data.OrderBy(a => a.ArriveStationName).ToList();

                    return data1;
                }
                else
                {

                    var data = (from dr in JunFuDbContext.TcDeliveryRequests
                                join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
                                from ta in temp3.DefaultIfEmpty()
                                join au in JunFuDbContext.AccountUpload on dr.CheckNumber equals au.CheckNumber into temp4
                                from au in temp4.DefaultIfEmpty()
                                    //join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
                                    //from sl in temp4.DefaultIfEmpty()
                                where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                                & dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                                & dr.DeliveryType != "R" & dr.LatestArriveOption == "3"
                                select new DriverCollectionMoneyWriteOffEntity
                                {

                                    CheckNumber = dr.CheckNumber,
                                    ScanDate = dr.LatestArriveOptionDate,
                                    ArriveStationName = dr.AreaArriveCode,
                                    CustomerCode = dr.CustomerCode,
                                    CustomerName = ta.CustomerName,
                                    ReceiveContact = dr.ReceiveContact,
                                    CollectionMoney = dr.CollectionMoney,
                                    DriverCode = dr.LatestArriveOptionDriver,
                                    IsUpload = (au.IsDel == 0 ? "Y" : "N"),
                                    ReceivedAmount = (au.IsDel == 0 ? au.ReceivedAmount.ToString() : ""),
                                    //AccountDate = (string.IsNullOrEmpty(au.AccountDate.ToShortDateString()) ? "" : au.AccountDate.ToShortDateString())
                                    AccountDate = au.AccountDate == null ? "" : au.AccountDate.ToShortDateString()

                                    //Cdate = sl.Cdate

                                }).Distinct().ToList();
                    return data;
                }
                return null;
                //var latestScan = from dr in JunFuDbContext.TcDeliveryRequests
                //                 join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
                //                 from sl in temp4.DefaultIfEmpty()
                //                 where dr.LatestArriveOptionDate < end & dr.LatestArriveOptionDate >= start & dr.CheckNumber.Length > 0
                //                 & dr.LatestArriveOption == "3"
                //                 & dr.DeliveryType != "R" 
                //                 & dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
                //                 group sl by sl.CheckNumber into g
                //                 select new  { CheckNumber = g.Key,  Cdate = g.Max(s => s.Cdate) };



                //var returnData = from ls in latestScan
                //                 join dr in JunFuDbContext.TcDeliveryRequests on ls.CheckNumber equals dr.CheckNumber into temp5
                //                 from dr in temp5.DefaultIfEmpty()
                //                 join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
                //                 from ta in temp3.DefaultIfEmpty()

                //                 select new DriverCollectionMoneyWriteOffEntity
                //                 {
                //                     ArriveStationName = dr.AreaArriveCode,
                //                     CheckNumber = dr.CheckNumber,
                //                     CustomerCode = dr.CustomerCode,
                //                     CustomerName = ta.CustomerName,
                //                     ReceiveContact = dr.ReceiveContact,
                //                     CollectionMoney = dr.CollectionMoney,
                //                     DriverCode = dr.LatestArriveOptionDriver,
                //                     ScanDate = dr.LatestArriveOptionDate,
                //                     Cdate = ls.Cdate
                //                 };




                //return returnData.Distinct().ToList();
            }
        }

        ////作業時間or建立時間
        //public List<DriverCollectionMoneyWriteOffEntity> GetAllByDateAndStationAndType(string scode, DateTime start, DateTime end)
        //{
        //    // with nolock in linq 
        //    using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
        //    , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
        //    {
        //        //var data = (from dr in JunFuDbContext.TcDeliveryRequests
        //        //            join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
        //        //            from ta in temp3.DefaultIfEmpty()
        //        //            join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
        //        //            from sl in temp4.DefaultIfEmpty()
        //        //            where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
        //        //            & sl.Cdate < end & sl.Cdate >= start & dr.CheckNumber.Length > 0
        //        //            & dr.DeliveryType != "R" & dr.AreaArriveCode == scode
        //        //            & dr.LatestArriveOption == "3"
        //        //            select new DriverCollectionMoneyWriteOffEntity
        //        //            {
        //        //                ArriveStationName = dr.AreaArriveCode,
        //        //                CheckNumber = dr.CheckNumber,
        //        //                CustomerCode = dr.CustomerCode,
        //        //                CustomerName = ta.CustomerName,
        //        //                ReceiveContact = dr.ReceiveContact,
        //        //                CollectionMoney = dr.CollectionMoney,
        //        //                DriverCode = dr.LatestArriveOptionDriver,
        //        //                ScanDate = dr.LatestArriveOptionDate,
        //        //                Cdate = sl.Cdate
        //        //            });

        //        var latestScan = from dr in JunFuDbContext.TcDeliveryRequests
        //                         join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
        //                         from sl in temp4.DefaultIfEmpty()
        //                         where sl.Cdate < end & sl.Cdate >= start & dr.CheckNumber.Length > 0
        //                         & dr.AreaArriveCode == scode
        //                         & dr.LatestArriveOption == "3"
        //                         & dr.DeliveryType != "R"
        //                         & dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
        //                         group sl by sl.CheckNumber into g
        //                         select new  { CheckNumber = g.Key, Cdate = g.Max(s => s.Cdate) };


        //        var returnData = from ls in latestScan
        //                         join dr in JunFuDbContext.TcDeliveryRequests on ls.CheckNumber equals dr.CheckNumber into temp5
        //                         from dr in temp5.DefaultIfEmpty()
        //                         join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
        //                         from ta in temp3.DefaultIfEmpty()

        //                         select new DriverCollectionMoneyWriteOffEntity
        //                         {
        //                             ArriveStationName = dr.AreaArriveCode,
        //                             CheckNumber = dr.CheckNumber,
        //                             CustomerCode = dr.CustomerCode,
        //                             CustomerName = ta.CustomerName,
        //                             ReceiveContact = dr.ReceiveContact,
        //                             CollectionMoney = dr.CollectionMoney,
        //                             DriverCode = dr.LatestArriveOptionDriver,
        //                             ScanDate = dr.LatestArriveOptionDate,
        //                             Cdate = ls.Cdate
        //                         };




        //        return returnData.Distinct().ToList();
        //    }
        //}


        //public List<DriverCollectionMoneyWriteOffEntity> GetAllByDateAndType(DateTime start, DateTime end)
        //{
        //    // with nolock in linq 
        //    using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required
        //     , new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted }))
        //    {
        //        //var data = (from dr in JunFuDbContext.TcDeliveryRequests
        //        //            join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
        //        //            from ta in temp3.DefaultIfEmpty()
        //        //            join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
        //        //            from sl in temp4.DefaultIfEmpty()
        //        //            where dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
        //        //            & sl.Cdate < end & sl.Cdate >= start & dr.CheckNumber.Length > 0
        //        //            & dr.DeliveryType != "R" & dr.LatestArriveOption == "3"
        //        //            select new DriverCollectionMoneyWriteOffEntity
        //        //            {
        //        //                ArriveStationName = dr.AreaArriveCode,
        //        //                CheckNumber = dr.CheckNumber,
        //        //                CustomerCode = dr.CustomerCode,
        //        //                CustomerName = ta.CustomerName,
        //        //                ReceiveContact = dr.ReceiveContact,
        //        //                CollectionMoney = dr.CollectionMoney,
        //        //                DriverCode = dr.LatestArriveOptionDriver,
        //        //                ScanDate = dr.LatestArriveOptionDate,
        //        //                Cdate = sl.Cdate
        //        //            });

        //        var latestScan = from dr in JunFuDbContext.TcDeliveryRequests
        //                         join sl in JunFuDbContext.TtDeliveryScanLogs on new { ScanDate = dr.LatestArriveOptionDate, CheckNumber = dr.CheckNumber } equals new { sl.ScanDate, sl.CheckNumber } into temp4
        //                         from sl in temp4.DefaultIfEmpty()
        //                         where sl.Cdate < end & sl.Cdate >= start & dr.CheckNumber.Length > 0
        //                         & dr.LatestArriveOption == "3"
        //                         & dr.DeliveryType != "R"
        //                         & dr.CollectionMoney != 0 & dr.CollectionMoney != null & dr.LessThanTruckload.Equals(true)
        //                         group sl by sl.CheckNumber into g
        //                         select new  { CheckNumber = g.Key, Cdate = g.Max(s => s.Cdate) };





        //        var returnData = from ls in latestScan
        //                         join dr in JunFuDbContext.TcDeliveryRequests on ls.CheckNumber equals dr.CheckNumber into temp5
        //                         from dr in temp5.DefaultIfEmpty()
        //                         join ta in JunFuDbContext.TbCustomers on dr.CustomerCode equals ta.CustomerCode into temp3
        //                         from ta in temp3.DefaultIfEmpty()

        //                         select new DriverCollectionMoneyWriteOffEntity
        //                         {
        //                             ArriveStationName = dr.AreaArriveCode,
        //                             CheckNumber = dr.CheckNumber,
        //                             CustomerCode = dr.CustomerCode,
        //                             CustomerName = ta.CustomerName,
        //                             ReceiveContact = dr.ReceiveContact,
        //                             CollectionMoney = dr.CollectionMoney,
        //                             DriverCode = dr.LatestArriveOptionDriver,
        //                             ScanDate = dr.LatestArriveOptionDate,
        //                             Cdate = ls.Cdate
        //                         };






        //        return returnData.Distinct().ToList();
        //    }
        //}



        public Dictionary<string, DriverPaymentDetail> GetIsDriverPaymentSheetOrNot(List<string> checknumbers)
        {
            var data = (from dp in JunFuTransDbContext.DriverPaymentDetails
                        where checknumbers.Contains(dp.CheckNumber)
                        select dp).ToDictionary(s => s.CheckNumber);
            return data;
        }

        public bool IsDriverPaymentSheetOrNot(string checknumber)
        {
            bool result = false;

            var data = (from dp in JunFuTransDbContext.DriverPaymentDetails
                        where dp.CheckNumber == checknumber
                        select dp).ToList();
            if (data.Count > 0)
            {
                result = true;
                return result;
            }
            return result;
        }

    }
}
