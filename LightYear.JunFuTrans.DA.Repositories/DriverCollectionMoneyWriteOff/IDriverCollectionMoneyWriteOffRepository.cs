﻿using LightYear.JunFuTrans.BL.BE.DriverCollectionMoneyWriteOff;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.DriverCollectionMoneyWriteOff
{
    public interface IDriverCollectionMoneyWriteOffRepository
    {
        List<DriverCollectionMoneyWriteOffEntity> GetAllByDateAndStation(string scode, DateTime start, DateTime end);

        List<DriverCollectionMoneyWriteOffEntity> GetAllByDate(DateTime start, DateTime end,string station_level, string management, string station_area,string station_scode);

        Dictionary<string, DriverPaymentDetail> GetIsDriverPaymentSheetOrNot(List<string> checknumbers);

        bool IsDriverPaymentSheetOrNot(string checknumber);

        //List<DriverCollectionMoneyWriteOffEntity> GetAllByDateAndStationAndType(string scode, DateTime start, DateTime end);

        //List<DriverCollectionMoneyWriteOffEntity> GetAllByDateAndType(DateTime start, DateTime end);



    }
}
