﻿using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.DA.Repositories.PickUppImportRecord
{
    public class PickUppImportRecordRepository : IPickUppImportRecordRepository
    {

        public JunFuDbContext JunFuDbContext { get; private set; }
        public PickUppImportRecordRepository(JunFuDbContext junFuDbContext)
        {
            this.JunFuDbContext = junFuDbContext;
        }


    }
}
