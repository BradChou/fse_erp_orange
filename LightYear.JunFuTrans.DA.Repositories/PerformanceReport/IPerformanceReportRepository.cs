﻿using LightYear.JunFuTrans.BL.BE.PerformanceReport;
using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.DA.Repositories.PerformanceReport
{
    public interface IPerformanceReportRepository
    {
        List<PerformanceReportRepositoryEntity> PerformanceReportRepositoryBySendStationList(FrontendInputEntity frontendInput);

        List<PerformanceReportRepositoryEntity> PerformanceReportRepositoryByAccountCodeList(FrontendInputEntity frontendInput);

        List<AccountEntity> GetAccountEntities(string station);

        List<AccountEntity> MasterGetAccountEntities(string station);

        TbCustomer GetByAccountCode(string accountCode);
    }
}
