﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.Services.DeliveryRequest;
using LightYear.JunFuTrans.BL.Services.LocalLog;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.PickUpRequest;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.DA.Repositories.CbmSize;
using LightYear.JunFuTrans.DA.Repositories.ShopeeLog;
using AutoMapper;
using Newtonsoft.Json;
using LightYear.JunFuTrans.Mappers.DeliveryRequest;
using LightYear.JunFuTrans.Mappers.StationWithArea;
using LightYear.JunFuTrans.Utilities.Api;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using LightYear.JunFuTrans.Mappers.ShopeeLog;
using JunFuTrans.DA.JunFuTrans.DA;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using RestSharp;
using RestSharp.Authenticators;
using System.Net;

namespace ShopeeStatusDetail
{
    class Program
    {
        private static IConfiguration Configs()
        {
            var config = new ConfigurationBuilder().AddJsonFile("app.json").Build();
            return config;
        }
        static void Log(string action, string message)
        {
            string ShopeeStatusDetailLogType = "2000";
            shopee_api_log_DA _shopee_api_log_DA = new shopee_api_log_DA();
            _shopee_api_log_DA.Insertshopee_api_log(new shopee_api_log { type = ShopeeStatusDetailLogType, action = action, message = message, cdate = DateTime.Now });

        }


        static void Main(string[] args)
        {
            try
            {
                var configs = Configs();

                string ConnectionStringsJunFuDbContext = configs["ConnectionStrings:JunFuDbContext"];
                string ConnectionStringsJunFuTransDbContext = configs["ConnectionStrings:JunFuTransDbContext"];

                DbContextOptions<JunFuDbContext> options = new DbContextOptionsBuilder<JunFuDbContext>().UseSqlServer(ConnectionStringsJunFuDbContext).Options;
                DbContextOptions<JunFuTransDbContext> options2 = new DbContextOptionsBuilder<JunFuTransDbContext>().UseSqlServer(ConnectionStringsJunFuTransDbContext).Options;


                Log("Start", "OK");
                Console.WriteLine("開始執行！");
                //初始化Service
                JunFuDbContext junFuDbContext = new JunFuDbContext(options);

                JunFuTransDbContext junFuTransDbContext = new JunFuTransDbContext(options2);

                DeliveryRequestModifyRepository deliveryRequestModifyRepository = new DeliveryRequestModifyRepository(junFuDbContext, junFuTransDbContext);

                PickUpRequestRepository pickUpRequestRepository = new PickUpRequestRepository(junFuDbContext);

                CheckNumberSDMappingRepository checkNumberSDMappingRepository = new CheckNumberSDMappingRepository(junFuTransDbContext);

                OrgAreaRepository orgAreaRepository = new OrgAreaRepository(junFuDbContext, junFuTransDbContext);

                StationRepository stationRepository = new StationRepository(junFuDbContext, junFuTransDbContext);

                CustomerRepository customerRepository = new CustomerRepository(junFuDbContext);

                DriverRepository driverRepository = new DriverRepository(junFuDbContext);

                DeliveryScanLogRepository deliveryScanLogRepository = new DeliveryScanLogRepository(junFuDbContext);

                PickupRequestForApiuserRepository pickupRequestForApiuserRepository = new PickupRequestForApiuserRepository(junFuDbContext);

                CheckNumberPreheadRepository checkNumberPreheadRepository = new CheckNumberPreheadRepository(junFuDbContext);

                ShopeeStatusDetailRepository shopeeStatusDetailRepository = new ShopeeStatusDetailRepository(junFuTransDbContext);

                LocalLogService localLogService = new LocalLogService();

                CbmSizeRepository cbmSizeRepository = new CbmSizeRepository(junFuDbContext);

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<DeliveryRequestMappingProfile>();
                    cfg.AddProfile<DeliveryScanLogMappingProfile>();
                    cfg.AddProfile<StationAreaMappingProfile>();
                    cfg.AddProfile<ShopeeLogMappingProfile>();
                });

                var mapper = config.CreateMapper();

                DeliveryRequestSimpleService deliveryRequestSimpleService = new DeliveryRequestSimpleService(deliveryRequestModifyRepository, pickUpRequestRepository, checkNumberSDMappingRepository, orgAreaRepository, stationRepository, customerRepository, driverRepository, deliveryScanLogRepository, pickupRequestForApiuserRepository, mapper, checkNumberPreheadRepository, cbmSizeRepository);

                Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();


                string customerCode = configs["CustomerCode"];

                string apiUrl = configs["StatusDetailUrl"];

                string localPath = configs["LogPath"];

                string input = configs["ParametersString"].Substring(1, configs["ParametersString"].Length - 2);        //去掉頭尾"{","}"，以防string.Format出錯
                input = "{" + string.Format(input, customerCode, apiUrl) + "}";

                if (args.Length > 0)
                {
                    input = args[0];
                }

                //取得來自輸入設定檔
                try
                {
                    DeliveryStatusConfigEntity deliveryStatusConfigEntity = JsonConvert.DeserializeObject<DeliveryStatusConfigEntity>(input);

                    DateTime start = DateTime.Now.AddHours(-36);

                    DateTime end = DateTime.Now;

                    Log("抓取範圍", start.ToString() + " 到 " + end.ToString());


                    if (deliveryStatusConfigEntity.StartDate != DateTime.MinValue)
                    {
                        start = deliveryStatusConfigEntity.StartDate;
                    }

                    if (deliveryStatusConfigEntity.EndDate != DateTime.MinValue)
                    {
                        end = deliveryStatusConfigEntity.EndDate;
                    }

                    if (deliveryStatusConfigEntity.CustomerCode.Length > 0)
                    {
                        customerCode = deliveryStatusConfigEntity.CustomerCode;
                    }

                    if (deliveryStatusConfigEntity.ApiUrl.Length > 0)
                    {
                        apiUrl = deliveryStatusConfigEntity.ApiUrl;
                    }

                    var result = deliveryRequestSimpleService.GetScanLogByCustomerCodeAndTimeZone(customerCode, start, end, false);

                    List<List<ApiScanLogEntity>> listGroup = new List<List<ApiScanLogEntity>>();
                    int c = 50;//50為一組
                    int j = c;
                    for (int i = 0; i < result.Count; i += c)
                    {
                        List<ApiScanLogEntity> cList = new List<ApiScanLogEntity>();
                        cList = result.Take(j).Skip(i).ToList();
                        j += c;
                        listGroup.Add(cList);
                    }
                    int cc = 0;

                    foreach (var list in listGroup)
                    {
                        var resultJson = new ApiPickupLogEntityForJsonRequest() { StatusDetail = list };
                        string szResult = JsonConvert.SerializeObject(resultJson);

                        Log("API URL", apiUrl);
                        Log("數量", (cc + 1).ToString() + " 到 " + (cc + list.Count()).ToString());
                        cc = cc + list.Count();
                        Log("request", szResult);
                        //Call API

                        string postResult = string.Empty;
                        var client = new RestClient(apiUrl);
                        var request = new RestRequest().AddJsonBody(szResult);
                        var response = client.Post<object>(request);


                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            Log("response", response.Content);
                            postResult = response.Content;
                        }
                        else
                        {
                            Log("StatusCode", response.StatusCode.ToString());
                            if (response.ErrorMessage != null)
                            {
                                Log("ErrorMessage", response.ErrorMessage);
                            }

                        }

                        // var timeline = await client.GetAsync<object>(request, cancellationToken);


                        // PostDataToWebClass postDataToWebClass = new PostDataToWebClass(apiUrl, szResult, keyValuePairs);
                        // Console.WriteLine("已送出訊息，等待回應中...");
                        //  string postResult = postDataToWebClass.PostData("application/json");


                        Log("開始TXTLOG", localPath);
                        localLogService.Log(localPath, "StatusDetail", postResult, list);
                        Log("結束TXTLOG", "OK");

                    }

                    //寫入log
                    Log("開始DB LOG", "ShopeeStatusDetail");
                    var shopeeStatusDetails = mapper.Map<List<LightYear.JunFuTrans.DA.JunFuTransDb.ShopeeStatusDetail>>(result);
                    shopeeStatusDetailRepository.InsertDatas(shopeeStatusDetails);
                    Log("結束DB LOG", "OK");
                    //Console.WriteLine(result);

                }
                catch (Exception ex)
                {
                    Log("失敗 Message", ex.Message);
                    Log("失敗 StackTrace", ex.StackTrace);
                    Log("失敗 Source", ex.Source);
                    Log("ConnectionStringsJunFuDbContext", ConnectionStringsJunFuDbContext);
                    Log("ConnectionStringsJunFuTransDbContext", ConnectionStringsJunFuTransDbContext);
                    // Console.WriteLine(ex.ToString());
                }
                Log("End", "OK");
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
                //Console.ReadLine();

            }
        }
    }
}
