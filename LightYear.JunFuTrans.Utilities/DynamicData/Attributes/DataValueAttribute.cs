﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.Utilities.DynamicData.Attributes
{
    /// <summary>
    /// DataValue
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class DataValueAttribute : System.Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataValueAttribute"/> class.
        /// </summary>
        public DataValueAttribute()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataValueAttribute"/> class.
        /// </summary>
        /// <param name="data">請以 text:value;text:value ....  方式填值</param>
        public DataValueAttribute(string data)
        {
            this.Data = data;
        }

        /// <summary>
        /// Data 請以 text:value;text:value ....  方式填值
        /// </summary>
        public string Data { get; set; }
    }
}
