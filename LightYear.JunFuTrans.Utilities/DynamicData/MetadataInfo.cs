﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using LightYear.JunFuTrans.Utilities.DynamicData.Attributes;

namespace LightYear.JunFuTrans.Utilities.DynamicData
{
    /// <summary>
    /// MetadataInfo
    /// </summary>
    public class MetadataInfo
    {
        /// <summary>
        /// DisplayNameCustomAttribute
        /// </summary>
        public DisplayNameCustomAttribute DisplayNameCustom { get; set; }

        /// <summary>
        /// 搜尋用欄位
        /// </summary>
        public SearchCriteriaAttribute SearchCriteria { get; set; }

        /// <summary>
        /// UIType
        /// </summary>
        public UITypeAttribute UIType { get; set; }

        /// <summary>
        /// DisplayFormatAttribute
        /// </summary>
        public DisplayFormatAttribute DisplayFormat { get; set; }

        /// <summary>
        /// 欄位名稱
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// StringLengthAttribute
        /// </summary>
        public StringLengthAttribute Length { get; set; }

        /// <summary>
        /// 屬性型別
        /// </summary>
        public Type PropertyType { get; set; }

        /// <summary>
        /// RangeAttribute
        /// </summary>
        public RangeAttribute Range { get; set; }

        /// <summary>
        /// RegularExpressionAttribute
        /// </summary>
        public RegularExpressionAttribute RegularExpression { get; set; }

        /// <summary>
        /// RequiredAttribute
        /// </summary>
        public RequiredAttribute Required { get; set; }

        /// <summary>
        /// UIHintAttribute
        /// </summary>
        public UIHintAttribute UIHint { get; set; }

        /// <summary>
        /// HiddenInputAttribute
        /// </summary>
        public HiddenInputAttribute HiddenInput { get; set; }

        /// <summary>
        /// DefinitionAttribute
        /// </summary>
        public DefinitionAttribute Definition { get; set; }

        /// <summary>
        /// DataValue
        /// </summary>
        public DataValueAttribute DataValue { get; set; }

        /// <summary>
        /// Query
        /// </summary>
        public QueryAttribute Query { get; set; }
    }
}
