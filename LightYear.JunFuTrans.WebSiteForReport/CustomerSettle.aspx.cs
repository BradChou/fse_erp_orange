﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LightYear.JunFuTrans.WebSiteForReport
{
    public partial class CustomerSettle : System.Web.UI.Page
    {
        CurrentUser currentUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            currentUser = new CurrentUser(Request.Cookies);

            if (!IsPostBack)
            {
                String yy = DateTime.Now.Year.ToString();
                String mm = DateTime.Now.Month.ToString();
                String days = DateTime.DaysInMonth(int.Parse(yy), int.Parse(mm)).ToString();
                DateTime FirstDay = DateTime.Parse(yy + "/" + mm + "/1");

                Microsoft.Reporting.WebForms.ReportParameter[] reportParameters = new Microsoft.Reporting.WebForms.ReportParameter[1];

                reportParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("start", FirstDay.ToString("yyyy-MM-dd"));

                SetReportViewerAuth(this.ReportViewer1, "全速配報表專區/客戶結帳明細", reportParameters);
            }
        }

        public void SetReportViewerAuth(Microsoft.Reporting.WebForms.ReportViewer sender, string ReportName, Microsoft.Reporting.WebForms.ReportParameter[] _params)
        {
            string strReportsServer = WebConfigurationManager.AppSettings["ReportUrl"]; //報表位置IP
            string strUserName = WebConfigurationManager.AppSettings["ReportAccount"]; //Windows驗證非SQL驗證
            string strPassword = WebConfigurationManager.AppSettings["ReportPassword"];

            Microsoft.Reporting.WebForms.IReportServerCredentials mycred = new CustomReportCredentials(strUserName, strPassword, strReportsServer);
            sender.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;

            Uri reportUri = new Uri("http://" + strReportsServer + "/reportserver");
            var _with1 = sender.ServerReport;
            _with1.ReportServerUrl = reportUri;
            _with1.ReportPath = "/" + ReportName;
            _with1.ReportServerCredentials = mycred;
            _with1.SetParameters(_params);

            //sender.ShowParameterPrompts = false;
            sender.Visible = true;
            //sender.ZoomPercent = 75;
        }
    }
}