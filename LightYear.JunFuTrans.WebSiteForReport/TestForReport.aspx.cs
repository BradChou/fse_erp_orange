﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LightYear.JunFuTrans.WebSiteForReport
{
    public partial class TestForReport : System.Web.UI.Page
    {
        CurrentUser currentUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            currentUser = new CurrentUser(Request.Cookies);

            if (!IsPostBack)
            {
                //string stationCode = (currentUser.UserInfoEntity.Station != null && currentUser.UserInfoEntity.Station.Length > 0) ? currentUser.UserInfoEntity.Station : "22"; 


                //Microsoft.Reporting.WebForms.ReportParameter[] reportParameters = new Microsoft.Reporting.WebForms.ReportParameter[4];

                //reportParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("Date", DateTime.Today.AddDays(-4).ToString("yyyy-MM-dd"));
                //reportParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("deliver_status", "5");
                //reportParameters[2] = new Microsoft.Reporting.WebForms.ReportParameter("deadline", DateTime.Today.AddDays(4).ToString("yyyy-MM-dd"));
                //reportParameters[3] = new Microsoft.Reporting.WebForms.ReportParameter("station_id", stationCode);

                //SetReportViewerAuth(this.ReportViewer1, "峻富報表專區/六大歷程掃讀明細", reportParameters);
                Microsoft.Reporting.WebForms.ReportParameter[] reportParameters = new Microsoft.Reporting.WebForms.ReportParameter[2];

                reportParameters[0] = new Microsoft.Reporting.WebForms.ReportParameter("ScanDeadLine", DateTime.Today.ToString("yyyy-MM-dd"));
                reportParameters[1] = new Microsoft.Reporting.WebForms.ReportParameter("ScanStartDate", DateTime.Today.AddDays(-30).ToString("yyyy-MM-dd"));


                SetReportViewerAuth(this.ReportViewer1, "全速配報表專區/留庫未結查詢明細", reportParameters);
            }
        }

        public void SetReportViewerAuth(Microsoft.Reporting.WebForms.ReportViewer sender, string ReportName, Microsoft.Reporting.WebForms.ReportParameter[] _params)
        {
            string strReportsServer = WebConfigurationManager.AppSettings["ReportUrl"]; //報表位置IP
            string strUserName = WebConfigurationManager.AppSettings["ReportAccount"]; //Windows驗證非SQL驗證
            string strPassword = WebConfigurationManager.AppSettings["ReportPassword"];

            Microsoft.Reporting.WebForms.IReportServerCredentials mycred = new CustomReportCredentials(strUserName, strPassword, strReportsServer);
            sender.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;

            Uri reportUri = new Uri("http://" + strReportsServer + "/reportserver");
            var _with1 = sender.ServerReport;
            _with1.ReportServerUrl = reportUri;
            _with1.ReportPath = "/" + ReportName;
            _with1.ReportServerCredentials = mycred;
            _with1.SetParameters(_params);

            //sender.ShowParameterPrompts = false;
            sender.Visible = true;
            //sender.ZoomPercent = 75;
        }

    }



    public class CustomReportCredentials : Microsoft.Reporting.WebForms.IReportServerCredentials
    {
        // local variable for network credential
        private string strUserName;
        private string strPassWord;
        private string strDomainName;
        public CustomReportCredentials(string UserName, string PassWord, string DomainName)
        {
            strUserName = UserName;
            strPassWord = PassWord;
            strDomainName = DomainName;
        }
        public System.Security.Principal.WindowsIdentity ImpersonationUser
        {
            // not use ImpersonationUser
            get { return null; }
        }
        public System.Net.ICredentials NetworkCredentials
        {
            // use NetworkCredentials
            get { return new System.Net.NetworkCredential(strUserName, strPassWord, strDomainName); }
        }
        public bool GetFormsCredentials(out System.Net.Cookie authCookie, out string userName, out string password, out string authority)
        {
            // not use FormsCredentials unless you have implements a custom autentication.
            authCookie = null;
            userName = null;
            password = null;
            authority = null;
            return false;
        }
    }
}