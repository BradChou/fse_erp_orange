﻿using LightYear.JunFuTrans.BL.BE.BusinessReport;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.DA.Repositories.BusinessReport;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.DeliveryException;
using LightYear.JunFuTrans.BL.Services.Barcode;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Razor.Language.Extensions;
using System.Linq;

namespace LightYear.JunFuTrans.BL.Services.BusinessReport
{
    public class BusinessReportService : IBusinessReportService
    {
        public BusinessReportService(IBusinessReportRepository businessReportRepository, IMapper mapper, IStationRepository stationRepository, IDeliveryExceptionRepository deliveryExceptionRepository)
        {
            this.BusinessReportRepository = businessReportRepository;
            this.Mapper = mapper;
            this.StationRepository = stationRepository;
            this.DeliveryExceptionRepository = deliveryExceptionRepository;
        }
        public IBusinessReportRepository BusinessReportRepository { get; set; }
        public IStationRepository StationRepository { get; private set; }
        public IMapper Mapper { get; private set; }
        public IDeliveryExceptionRepository DeliveryExceptionRepository { get; set; }
        public List<BusinessReportEntity> PieceData(DateTime start, DateTime end, string areaArriveCode)
        {
            List<StationEntity> stationEntities = StationEntities();

            string station = "";
            if (areaArriveCode == "不指定站所")
            {
                station = "All";
            }
            else
            {
                foreach (var i in stationEntities)
                {
                    if (i.Name == areaArriveCode)
                        station = i.Code;
                }
            }

            var data = BusinessReportRepository.PieceList(start, end, station);

            var file = Mapper.Map<List<BusinessReportEntity>>(data);

            List<StationArea> stationArea = StationRepository.GetAreaStation();

            foreach (var i in file)
            {
                foreach (var j in stationArea)
                {
                    if (i.SupplierCode == j.StationCode)
                    {
                        i.SupplierCode = j.StationScode;
                        i.SupplierStation = j.StationName;
                        i.Area = j.Area;
                        break;
                    }
                }
            }

            foreach (var i in file)
            {
                if (i.ShipDate.Hour < 5)
                {
                    i.ShipDate = DateTime.Parse(i.ShipDate.AddDays(-1).ToShortDateString());
                }
                else
                {
                    i.ShipDate = DateTime.Parse(i.ShipDate.ToShortDateString());
                }
            }

            return file;
        }

        public List<AreaReportEntity> FrontendData(DateTime start, DateTime end, bool detailOrNot, string areaArriveCode)      //其實這段主要花時間的Code在PieceData
        {
            List<BusinessReportEntity> areaResidueEntities = new List<BusinessReportEntity>();       //紀錄未計入區屬報表的異常資料，不顯示在前端

            List<BusinessReportEntity> stationResidueEntities = new List<BusinessReportEntity>();       //紀錄未計入站所報表的異常資料，不顯示在前端

            List<BusinessReportEntity> entityList = PieceData(start, end, areaArriveCode);

            List<string> necessaryArea = new List<string>();

            List<AreaReportEntity> reportEntityList = new List<AreaReportEntity>();

            List<DateTime> daysList = new List<DateTime>();                         //存放日期區間(判別PrintDate用)        

            bool isMomoExist = false;

            int indexOfOther = -1;                                                  //儲存其他(資料不全)在necessaryList的第幾個位置

            int days = -1;                                                          //計算areaReportEntity所需List長度

            List<int> summaryOfWeek = new List<int>();                              //每週統計

            string momoCustomerCode = "F3500010002";

            if (end != start)
            {
                days = int.Parse((end - start).ToString().Split(".")[0]) + 1;
            }
            else
            {
                days = 1;
            }

            List<int> daysPerWeek = DaysPerWeek(start, end, days);                  //計算每周天數

            necessaryArea.Add("全公司(含momo)");                                               //第一行

            necessaryArea.Add("全公司");

            foreach (var i in entityList)
            {
                if (necessaryArea.IndexOf(i.Area) == -1 & (i.CustomerCode.Equals(momoCustomerCode) == false))
                {
                    necessaryArea.Add(i.Area);
                }
                else if (i.CustomerCode.Equals(momoCustomerCode))
                {
                    isMomoExist = true;
                }
            }

            for (var i = 0; i < necessaryArea.Count; i++)           //排序用
            {
                if (necessaryArea[i] == "峻富")
                {
                    necessaryArea.Remove(necessaryArea[i]);
                    necessaryArea.Add("峻富");
                }
            }

            if (necessaryArea.IndexOf(null) != -1)          //有些AreaArriveCode在資料庫時就是空的
            {
                indexOfOther = necessaryArea.Count - 1;     //抓出"其他"(資料不全)的位置
                necessaryArea.Remove(null);
                necessaryArea.Add("資料不全");
            }

            if (isMomoExist == true)                        //讓momo出現在"其他"(資料不全)之後
            {
                necessaryArea.Add("富邦momo");
            }

            foreach (var i in entityList)                   //判讀ScanItem in {1,2,3}，則ShipDate - 1天
            {
                if (i.ScanItem == "1" || i.ScanItem == "2" || i.ScanItem == "3")
                {
                    i.ShipDate = i.ShipDate.AddDays(-1);
                }
            }

            int tempInt = 0;                                //為了改來改去的需求新增的五行程式(隱藏"全公司"而設)
            for (var i = 0; i < necessaryArea.Count; i++)
            {        
                if (!necessaryArea[i].Equals("全公司"))
                {
                    tempInt += 1;
                }
                AreaReportEntity reportEntity = new AreaReportEntity()
                {
                    IndexOrStationCode = tempInt.ToString(),
                    Area = necessaryArea[i],
                    SumList = new List<int>(),
                    WeekList = new List<int>(),
                    Total = -1,
                    Average = -1
                };

                for (var j = 0; j < days; j++)              //不確定有沒有更好的寫法
                {
                    reportEntity.SumList.Add(0);
                }
                reportEntityList.Add(reportEntity);
            }

            for (var i = 0; i < days; i++)          //寫入日期列
            {
                daysList.Add(start.AddDays(i));
            }

            foreach (var i in entityList)
            {
                if (i.CustomerCode.Equals(momoCustomerCode) == false)
                {
                    foreach (var j in reportEntityList)
                    {
                        if (i.Area == null || i.Area == "")
                        {
                            for (var k = 0; k < daysList.Count; k++)
                            {
                                if (i.ShipDate == daysList[k])
                                {
                                    reportEntityList[indexOfOther].SumList[k] += 1;
                                    break;
                                }
                            }
                            break;
                        }
                        else if (i.Area == j.Area)
                        {
                            for (var k = 0; k < daysList.Count; k++)
                            {
                                if (i.ShipDate == daysList[k])
                                {
                                    j.SumList[k] += 1;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                else
                {
                    for (var k = 0; k < daysList.Count; k++)
                    {
                        if (i.ShipDate == daysList[k])
                        {
                            reportEntityList[reportEntityList.Count - 1].SumList[k] += 1;       //因為momo固定在最後才出現
                            break;
                        }
                        else if (k == daysList.Count - 1)
                        {
                            areaResidueEntities.Add(i);
                        }
                    }
                }
            }

            for (var i = 0; i < days; i++)                              //計算全公司(含momo)
            {
                for (var j = 2; j < necessaryArea.Count; j++)
                {
                    reportEntityList[0].SumList[i] += reportEntityList[j].SumList[i];
                }
            }

            for (var i = 0; i < days; i++)                              //計算全公司(不含momo)
            {
                reportEntityList[1].SumList[i] = reportEntityList[0].SumList[i] - reportEntityList[necessaryArea.Count - 1].SumList[i];
            }

            foreach (var i in reportEntityList)
            {
                int calculatedDays = 0;                                  //已經計算的天數
                for (var j = 0; j < daysPerWeek.Count; j++)
                {
                    int weekSum = 0;
                    for (var k = calculatedDays; k < calculatedDays + daysPerWeek[j]; k++)
                    {
                        weekSum += i.SumList[k];
                    }
                    calculatedDays += daysPerWeek[j];
                    i.WeekList.Add(weekSum);
                }
            }

            foreach (var i in reportEntityList)
            {
                var totalSum = 0;
                for (var j = 0; j < i.WeekList.Count; j++)
                {
                    totalSum += i.WeekList[j];
                }
                i.Total = totalSum;
                i.Average = totalSum / days;
            }

            for (var i = 0; i < reportEntityList.Count; i++)            //一下要，一下不要的程式碼
            {
                if (reportEntityList[i].Area == "全公司")
                {
                    reportEntityList.Remove(reportEntityList[i]);
                }
            }

            //以上計算區屬，以下加入站所

            if (detailOrNot == true)                            //以下加入站所資訊
            {
                List<StationEntity> necessaryStation = new List<StationEntity>();
                List<string> necessaryStationWithoutCode = new List<string>();
                int areaLength = reportEntityList.Count;                //補丁
                List<StationEntity> allStation = StationEntities();

                AreaReportEntity dividingData = new AreaReportEntity()      //給前端用於區分 區屬、站所
                {
                    IndexOrStationCode = "所簡碼",
                    Area = "站所",
                    SumList = new List<int>(),
                    WeekList = new List<int>(),
                    Total = -1,
                    Average = -1
                };
                reportEntityList.Add(dividingData);

                foreach (var i in entityList)
                {
                    if (necessaryStationWithoutCode.IndexOf(i.SupplierStation) == -1 & i.SupplierStation != null & i.SupplierStation != "")
                    {
                        StationEntity station = new StationEntity()
                        {
                            Code = i.SupplierCode,
                            Name = i.SupplierStation
                        };
                        necessaryStation.Add(station);
                        necessaryStationWithoutCode.Add(i.SupplierStation);
                    }
                    else if (necessaryStationWithoutCode.IndexOf("資料不全") != -1 & i.SupplierStation != null & i.SupplierStation != "")
                    {
                        StationEntity stationOther = new StationEntity()
                        {
                            Code = "Other",
                            Name = "資料不全"
                        };
                        necessaryStation.Add(stationOther);
                        necessaryStationWithoutCode.Add("資料不全");
                    }
                }

                if (necessaryStation[necessaryStation.Count - 1].Name == "峻富總公司")
                {
                    for (var i = 0; i < necessaryStation.Count; i++)
                    {
                        if (necessaryStation[i].Name.IndexOf("倉") != -1)
                        {
                            StationEntity temp = necessaryStation[i];
                            necessaryStation.RemoveAt(i);
                            necessaryStation.Insert(necessaryStation.Count - 1, temp);
                        }
                    }
                }
                else
                {
                    for (var i = 0; i < necessaryStation.Count; i++)
                    {
                        if (necessaryStation[i].Name.IndexOf("倉") != -1)
                        {
                            StationEntity temp = necessaryStation[i];
                            necessaryStation.RemoveAt(i);
                            necessaryStation.Insert(necessaryStation.Count, temp);
                        }
                    }
                }

                necessaryStation = allStation;                  //從不全為0的站所改成全站所
                List<StationEntity> prepareToMoveList = new List<StationEntity>();
                List<int> prepareToRemoveList = new List<int>();
                for (var i = 0; i < necessaryStation.Count; i++)
                {
                    if (necessaryStation[i].Owner == 1)         //1:聯運
                    {
                        necessaryStation.RemoveAt(i);
                        i--;
                    }
                }
                for (var i = 0; i < necessaryStation.Count; i++)
                {
                    if (necessaryStation[i].Owner == 2)         //2:峻富
                    {
                        prepareToMoveList.Add(necessaryStation[i]);
                        prepareToRemoveList.Add(i);
                    }
                }
                for (var i = 0; i < prepareToRemoveList.Count; i++)
                {
                    necessaryStation.RemoveAt(prepareToRemoveList[i] - i);
                }
                necessaryStation.AddRange(prepareToMoveList);

                foreach (var i in necessaryStation)
                {
                    AreaReportEntity reportEntity = new AreaReportEntity()
                    {
                        IndexOrStationCode = i.Code,
                        Area = i.Name,
                        SumList = new List<int>(),
                        WeekList = new List<int>(),
                        Total = -1,
                        Average = -1
                    };

                    for (var j = 0; j < days; j++)
                    {
                        reportEntity.SumList.Add(0);
                    }
                    reportEntityList.Add(reportEntity);
                }

                foreach (var i in entityList)
                {
                    if (!i.CustomerCode.Equals(momoCustomerCode))                               //這行排除momo
                    {
                        for (var j = areaLength; j < reportEntityList.Count; j++)
                        {
                            if (i.SupplierCode == reportEntityList[j].IndexOrStationCode.ToString())
                            {
                                for (var k = 0; k < daysList.Count; k++)
                                {
                                    if (i.ShipDate == daysList[k])
                                    {
                                        reportEntityList[j].SumList[k] += 1;
                                        break;
                                    }
                                }
                                break;
                            }
                            else if (j == reportEntityList.Count - 1)
                            {
                                stationResidueEntities.Add(i);
                            }
                        }
                    }
                }

                for (var i = areaLength + 1; i < reportEntityList.Count; i++)
                {
                    int calculatedDays = 0;                                  //已經計算的天數
                    for (var j = 0; j < daysPerWeek.Count; j++)
                    {
                        int weekSum = 0;
                        for (var k = calculatedDays; k < calculatedDays + daysPerWeek[j]; k++)
                        {
                            weekSum += reportEntityList[i].SumList[k];
                        }
                        calculatedDays += daysPerWeek[j];
                        reportEntityList[i].WeekList.Add(weekSum);
                    }
                }

                foreach (var i in reportEntityList)
                {
                    var totalSum = 0;
                    for (var j = 0; j < i.WeekList.Count; j++)
                    {
                        totalSum += i.WeekList[j];
                    }
                    i.Total = totalSum;
                    i.Average = totalSum / days;
                }
            }

            return reportEntityList;
        }

        private int WeekDayCalculator(DateTime firstday)                //計算星期幾
        {
            if (firstday.DayOfWeek.ToString() == "Monday")
            {
                return 1;
            }
            else if (firstday.DayOfWeek.ToString() == "Tuesday")
            {
                return 2;
            }
            else if (firstday.DayOfWeek.ToString() == "Wednesday")
            {
                return 3;
            }
            else if (firstday.DayOfWeek.ToString() == "Thursday")
            {
                return 4;
            }
            else if (firstday.DayOfWeek.ToString() == "Friday")
            {
                return 5;
            }
            else if (firstday.DayOfWeek.ToString() == "Saturday")
            {
                return 6;
            }
            else
            {
                return 7;
            }
        }
        private List<int> DaysPerWeek(DateTime start, DateTime end, int days)
        {
            int startWeekDay = WeekDayCalculator(start);
            int endWeekDay = WeekDayCalculator(end);
            List<int> result = new List<int>();
            int startWeekLack = startWeekDay - 1;
            int endWeekLack = 7 - endWeekDay;
            int totalDays = startWeekLack + endWeekLack + days;
            int totalWeeks = totalDays / 7;
            int fullWeeks = totalWeeks;

            if (totalWeeks >= 2)
            {
                result.Add(8 - startWeekDay);

                if (totalWeeks > 2)
                {
                    for (var i = 0; i < totalWeeks - 2; i++)
                    {
                        result.Add(7);
                    }
                }

                result.Add(endWeekDay);
            }
            else
            {
                result.Add(days);
            }

            return result;
        }

        public List<int> DaysAndSumAndAverage(StartAndEndEntity startAndEnd, List<AreaReportEntity> areaReportEntityList)        //固定return三個數字:工作天、全公司件數累計、全公司日均
        {
            int days = new TimeSpan(startAndEnd.End.Ticks - startAndEnd.Start.Ticks).Days;
            days += 1;
            int sum = 0;
            foreach (var i in areaReportEntityList[0].WeekList)
            {
                sum += i;
            }
            int average = sum / days;
            List<int> result = new List<int>();
            result.Add(days);
            result.Add(sum);
            result.Add(average);

            return result;
        }

        public List<StationEntity> StationEntities()
        {
            var data = this.BusinessReportRepository.StationEntities();

            var station = this.Mapper.Map<List<StationEntity>>(data);

            return station;
        }
    }
}
