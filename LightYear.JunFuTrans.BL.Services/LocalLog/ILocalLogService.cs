﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.LocalLog
{
    public interface ILocalLogService
    {
        void Log<T>(string logPath, string logFileName, string result, T logContent);
    }
}
