﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.Services.PickUpInfo
{
    public interface IPickUpInfoService
    {
        List<PickUpInfoShowEntity> GetFrontendPickUpEntitiesByStationCode(string stationCode, DateTime start, DateTime end, string customerCode, int isPickUped,string station_level,string station_area,string management,string station_scode);
        
        List<PickUpInfoShowEntity> GetFrontendPickUpEntitiesBySendArea(string stationCode, DateTime start, DateTime end, string customerCode, int isPickUped, string station_level, string station_area, string management, string station_scode);

        IEnumerable<PickUpOptionsEntity> GetItemCodesOptionsAllowToEdit();

        bool UpdateRoInfo(IEnumerable<PickUpInfoShowEntity> updateData, string accountCode, string isNormal);
    }
}
