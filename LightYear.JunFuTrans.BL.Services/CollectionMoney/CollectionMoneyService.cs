﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.BL.BE.CollectionMoney;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.CollectionMoney;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.Mappers;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.BL.Services.CollectionMoney
{
    public class CollectionMoneyService : ICollectionMoneyService
    {
        public CollectionMoneyService(
            IDeliveryRequestRepository deliveryRequestRepository, 
            IPaymentRepository paymentRepository, 
            IPaymentDetailRepository paymentDetailRepository, 
            IMapper mapper)
        {
            this.DeliveryRequestRepository = deliveryRequestRepository;
            this.PaymentRepository = paymentRepository;
            this.PaymentDetailRepository = paymentDetailRepository;
            this.Mapper = mapper;
        }

        public IDeliveryRequestRepository DeliveryRequestRepository { get; set; }

        public IPaymentDetailRepository PaymentDetailRepository { get; set; }

        public IPaymentRepository PaymentRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public double GetCollectionMoney(string checkNumber)
        {
            return this.DeliveryRequestRepository.GetCollectionMoney(checkNumber);
        }

        /// <summary>
        /// 寫入代收流程的資料庫
        /// </summary>
        /// <param name="collectionMoneyEntity"></param>
        /// <returns></returns>
        public int SaveCollectionMoney(CollectionMoneyEntity collectionMoneyEntity)
        {
            var payment = this.Mapper.Map<Payment>(collectionMoneyEntity);

            var paymentDetails = this.Mapper.Map<List<PaymentDetail>>(collectionMoneyEntity.Details);

            int id = this.PaymentRepository.Insert(payment);

            foreach(PaymentDetail paymentDetail in paymentDetails )
            {
                paymentDetail.PaymentId = id;

                this.PaymentDetailRepository.Insert(paymentDetail);
            }

            return id;
        }
    }
}
