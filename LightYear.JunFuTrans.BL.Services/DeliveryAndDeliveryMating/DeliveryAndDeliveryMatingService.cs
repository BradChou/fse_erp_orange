﻿using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using LightYear.JunFuTrans.DA.Repositories.DeliveryAndDeliveryMating;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.Services.Barcode;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Reflection;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.JunFuDb;
using JunFuTrans.DA.JunFuTrans.DA;
using JunFuTrans.DA.JunFuTrans.Model;
using System.IO;
using RestSharp;
using Common;
using System.Net;
using Newtonsoft.Json;

namespace LightYear.JunFuTrans.BL.Services.DeliveryAndDeliveryMating
{
    public class DeliveryAndDeliveryMatingService : IDeliveryAndDeliveryMatingService
    {
        public IDeliveryAndDeliveryMatingRepository DeliveryAndDeliveryMatingRepository { get; set; }
        public IMapper Mapper { get; private set; }
        public IScanItemService ScanItemService { get; set; }
        public IDriverRepository DriverRepository { get; set; }

        private CBMDetail_DA _CBMDetail_DA = new CBMDetail_DA();

        public DeliveryAndDeliveryMatingService(IDeliveryAndDeliveryMatingRepository deliveryAndDeliveryMatingRepository, IMapper mapper, IScanItemService scanItemService, IDriverRepository driverRepository)
        {
            this.DeliveryAndDeliveryMatingRepository = deliveryAndDeliveryMatingRepository;
            this.Mapper = mapper;
            ScanItemService = scanItemService;
            DriverRepository = driverRepository;
        }

        public List<DeliveryAndDeliveryMatingEntity> GetAll(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station)
        {
            List<DeliveryAndDeliveryMatingEntity> deliveryAndDeliveryMatingEntities = new List<DeliveryAndDeliveryMatingEntity>();
            IEnumerable<TbItemCode> itemCodes = DeliveryAndDeliveryMatingRepository.GetAllTbItemCodesWhichSclassAO();

            if (driverCode.Equals("all"))
            {
                deliveryAndDeliveryMatingEntities = DeliveryAndDeliveryMatingRepository.GetAllByStationScode(start, end, stationscode, arriveOption,station_level,station_area,management,station);
            }
            else
            {
                deliveryAndDeliveryMatingEntities = DeliveryAndDeliveryMatingRepository.GetAllByDriverCode(start, end, driverCode, arriveOption);
            }

            int count = 0;
            foreach (var entity in deliveryAndDeliveryMatingEntities)
            {
                SubpoenaCategoryConfig subpoenaCategoryConfig = (SubpoenaCategoryConfig)Convert.ToInt32(entity.SubpoenaCategory);
                entity.SubpoenaCategory = ScanItemService.GetEnumDescription(subpoenaCategoryConfig);

                entity.ArriveOption = itemCodes.FirstOrDefault(item => item.CodeId == entity.ArriveOption) != null ? itemCodes.Where(item => item.CodeId == entity.ArriveOption).FirstOrDefault().CodeName : "尚未配達";

                var driverName = DriverRepository.GetDriverNameByDriverCode(entity.DriverNameCode);
                entity.DriverNameCode += driverName;

                
                var cbm = _CBMDetail_DA.GetCbmBycheck_number(entity.CheckNumber);

                entity.CbmSize = cbm != null ? cbm.CbmSize : "";

                count++;
                entity.Id = count;

            }

            if (deliveryAndDeliveryMatingEntities.Count() > 1)
            {
                deliveryAndDeliveryMatingEntities[deliveryAndDeliveryMatingEntities.Count() - 1].ExcelSum = "合計代收貨款";
                deliveryAndDeliveryMatingEntities[deliveryAndDeliveryMatingEntities.Count() - 1].ListSum = GetSumOfCollectionMoney(deliveryAndDeliveryMatingEntities);
            }
            return deliveryAndDeliveryMatingEntities;
        }

        public int GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station)
        {
            return DeliveryAndDeliveryMatingRepository.GetCountPieces(start, end, driverCode, stationscode, arriveOption, station_level, station_area, management,station);
        }
        public int GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station)
        {
            return DeliveryAndDeliveryMatingRepository.GetCountArriveOptions(start, end, driverCode, stationscode, arriveOption, station_level, station_area, management,station);
        }
        public List<ArriveOptionEntity> GetArriveOptions()
        {
            List<ArriveOptionEntity> arriveOptionEntities = DeliveryAndDeliveryMatingRepository.GetArriveOptions();
            ArriveOptionEntity a = new ArriveOptionEntity();
            a.CodeId = "all";
            a.CodeName = "全選";
            List<ArriveOptionEntity> arr = new List<ArriveOptionEntity>();
            arr.Add(a);  //加了一個選項
            foreach (var entity in arriveOptionEntities)
            {
                arr.Add(entity); //arr List原本只有一個全選選項，加入其他已有的選項
            }
            return arr;
        }
        public List<DriversEntity> GetAllDrivers(string station_level, string station_area, string management, string stationscode,string station_scode,string station)
        {
            var data = DeliveryAndDeliveryMatingRepository.GetAllDrivers(station_level, station_area, management, stationscode, station_scode, station);
            var dataInfo = this.Mapper.Map<List<DriversEntity>>(data);
            dataInfo.Insert(0, new DriversEntity() { DriverCode = "all", DriverName = "全選" });
            return dataInfo;
        }

        public List<DriversEntity> GetDriversByStation(string station_level, string station_area, string management, string stationscode,string station)
        {
            var data = DeliveryAndDeliveryMatingRepository.GetDriversByStation(station_level,station_area,management,stationscode, station);
            var dataInfo = this.Mapper.Map<List<DriversEntity>>(data);
            dataInfo.Insert(0, new DriversEntity() { DriverCode = "all", DriverName = "全選" });
            return dataInfo;
        }

        private int GetSumOfCollectionMoney(List<DeliveryAndDeliveryMatingEntity> entities)
        {
            int result = 0;
            for (int i = 0; i < entities.Count(); i++)
            {
                result += entities[i].CollectionMoney;
            }
            return result;
        }

        public string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) //只有檔名
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName); //.副檔名
        }

        //轉皮卡
        public string CheckNumberToPickupp(List<DeliveryAndDeliveryMatingEntity> entities)
        {
            //DeliveryAndDeliveryMatingEntity Info = new DeliveryAndDeliveryMatingEntity();
            string Info = "";
            var apiUrl = "http://172.30.1.33:63814/Pickupp/SendOrder";

            //打API
            //var client = new RestClient(AddressParsingURL);
            //var request = new RestRequest(Method.POST);
            //request.Timeout = 5000;
            //request.ReadWriteTimeout = 5000;
            //request.RequestFormat = DataFormat.Json;
            //request.AddJsonBody(entities); // Anonymous type object is converted to Json body

            //var response = client.Post<ResData<DeliveryAndDeliveryMatingEntity>>(request);

            string szResult = JsonConvert.SerializeObject(entities);
            //Call API

            //string postResult = string.Empty;
            var client = new RestClient(apiUrl);
            var request = new RestRequest().AddJsonBody(szResult);
            var response = client.Post<DeliveryAndDeliveryMatingEntity>(request);

            Info = response.Content;

            return Info;
        }
    }
}
