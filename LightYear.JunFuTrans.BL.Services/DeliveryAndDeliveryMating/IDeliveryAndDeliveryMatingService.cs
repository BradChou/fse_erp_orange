﻿using LightYear.JunFuTrans.BL.BE.DeliveryAndDeliveryMating;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DeliveryAndDeliveryMating
{
    public interface IDeliveryAndDeliveryMatingService
    {
        List<DeliveryAndDeliveryMatingEntity> GetAll(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station);
        List<ArriveOptionEntity> GetArriveOptions();
        List<DriversEntity> GetAllDrivers(string station_level, string station_area, string management, string stationscode,string station_scode,string Station);
        int GetCountPieces(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station);
        int GetCountArriveOptions(DateTime start, DateTime end, string driverCode, string stationscode, string arriveOption, string station_level, string station_area, string management,string station);
        List<DriversEntity> GetDriversByStation(string station_level,string station_area,string management, string stationscode, string station );
        string GetUniqueFileName(string fileName);
        string CheckNumberToPickupp(List<DeliveryAndDeliveryMatingEntity> enities);
    }
}
