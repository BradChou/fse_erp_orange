﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.CbmInfoSearcher;
using LightYear.JunFuTrans.BL.BE.CbmInfo;
using LightYear.JunFuTrans.Mappers.CbmInfoSearcher;
using AutoMapper;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace LightYear.JunFuTrans.BL.Services.CbmInfoSearcher
{
    public class CbmInfoSearcherService : ICbmInfoSearcherService
    {
        public ICbmInfoSearcherRepository CbmInfoSearcherRepository { get; set; }
        public IConfiguration Configuration { get; private set; }
        public IMapper Mapper { get; private set; }

        public CbmInfoSearcherService(ICbmInfoSearcherRepository cbmInfoSearcherRepository, IMapper mapper, IConfiguration configuration)
        {
            this.CbmInfoSearcherRepository = cbmInfoSearcherRepository;
            this.Mapper = mapper;
            this.Configuration = configuration;
        }

        public CbmInfoEntity[] GetCbmInfoSearcher(CbmInfoInputEntity inputEntity, string station_level, string management, string station_area, string station_scode)
        {
            //順位請參考Repository註解
            if (inputEntity.CheckNumber != "")
            {
                CbmInfoEntity[] cbmInfoEntities = this.Mapper.Map<CbmInfoEntity[]>(CbmInfoSearcherRepository.GetCbmInfoByCheckNumber(inputEntity.CheckNumber));
                cbmInfoEntities = CbmInfoArrayEditor(cbmInfoEntities);
                return cbmInfoEntities;
            }
            else if (inputEntity.Start != null && inputEntity.End != null && inputEntity.CustomerCode != "")
            {
                CbmInfoEntity[] cbmInfoEntities = this.Mapper.Map<CbmInfoEntity[]>(CbmInfoSearcherRepository.GetCbmInfoByDateAndCustomerCode(inputEntity.Start, inputEntity.End, inputEntity.CustomerCode));
                cbmInfoEntities = CbmInfoArrayEditor(cbmInfoEntities);
                return cbmInfoEntities;
            }
            else if (inputEntity.Start != null && inputEntity.End != null && inputEntity.Station != "")
            {
                CbmInfoEntity[] cbmInfoEntities = this.Mapper.Map<CbmInfoEntity[]>(CbmInfoSearcherRepository.GetCbmInfoByDateAndStation(inputEntity.Start, inputEntity.End, inputEntity.Station,station_level,management,station_area,station_scode));
                cbmInfoEntities = CbmInfoArrayEditor(cbmInfoEntities);
                return cbmInfoEntities;
            }
            else if (inputEntity.CustomerCode != "")
            {
                CbmInfoEntity[] cbmInfoEntities = this.Mapper.Map<CbmInfoEntity[]>(CbmInfoSearcherRepository.GetCbmInfoByCustomerCode(inputEntity.CustomerCode));
                cbmInfoEntities = CbmInfoArrayEditor(cbmInfoEntities);
                return cbmInfoEntities;
            }
            else if (inputEntity.Station != "")
            {
                CbmInfoEntity[] cbmInfoEntities = this.Mapper.Map<CbmInfoEntity[]>(CbmInfoSearcherRepository.GetCbmInfoByStation(inputEntity.Station));
                cbmInfoEntities = CbmInfoArrayEditor(cbmInfoEntities);
                return cbmInfoEntities;
            }
            else if (inputEntity.Start != null && inputEntity.End != null)
            {
                CbmInfoEntity[] cbmInfoEntities = this.Mapper.Map<CbmInfoEntity[]>(CbmInfoSearcherRepository.GetCbmInfoByDate(inputEntity.Start, inputEntity.End));
                cbmInfoEntities = CbmInfoArrayEditor(cbmInfoEntities);
                return cbmInfoEntities;
            }
            else
            {
                CbmInfoEntity[] enti = new CbmInfoEntity[3];            //前端應該會擋下這種情況
                return enti;
            }
        }

        private CbmInfoEntity[] CbmInfoArrayEditor(CbmInfoEntity[] entities)
        {
            for (var i = 0; i < entities.Length; i++)
            {
                if (entities[i].PrintDate == null)
                {
                    entities[i].PrintDateFrontEnd = "";
                }
                else
                {
                    entities[i].PrintDateFrontEnd = ((DateTime)entities[i].PrintDate).ToString("yyyy-MM-dd");
                }

                if (entities[i].Length != null && entities[i].Width != null && entities[i].Height != null)
                {
                    entities[i].Size = Math.Round((double)(entities[i].Length * entities[i].Width * entities[i].Height / 27000000), 2);
                    entities[i].Weight = Math.Round((double)(entities[i].Length * entities[i].Width * entities[i].Height / 27000000) * 10, 1);        //


                    double threeSidesSum = (double)(entities[i].Length + entities[i].Width + entities[i].Height);
                    for (var j = 0; j < CbmRuler.Length; j++)
                    {
                        if (threeSidesSum <= CbmRuler[j] * 10 + 50)     // 多5cm的範圍都算在較小的規格內
                        {
                            entities[i].Scale = "S" + CbmRuler[j].ToString();
                            break;
                        }
                    }
                }

                if (entities[i].PrintDate != null)
                {
                    entities[i].PrintDateFrontEnd = ((DateTime)entities[i].PrintDate).ToString("yyyy-MM-dd");
                }

                if (entities[i].DaYuanPicURL != null)
                {
                    entities[i].DaYuanPicText = "檢視";
                    entities[i].DaYuanPicURL = Configuration.GetValue<string>("S3Url") + entities[i].DaYuanPicURL;//.Replace(@"\pictures", "");
                }
                else
                {
                    entities[i].DaYuanPicText = "";
                }

                if (entities[i].TaiChungPicURL != null)
                {
                    entities[i].TaiChungPicText = "檢視";
                    entities[i].TaiChungPicURL = Configuration.GetValue<string>("S3Url") + entities[i].TaiChungPicURL;//.Replace(@"\pictures", "");
                }
                else
                {
                    entities[i].TaiChungPicText = "";
                }
                
            }
            return entities;
        }

        private int[] CbmRuler = new int[7] { 30, 60, 90, 110, 120, 150, 180 };
    }
}
