﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.CbmInfo;

namespace LightYear.JunFuTrans.BL.Services.CbmInfoSearcher
{
    public interface ICbmInfoSearcherService
    {
        public CbmInfoEntity[] GetCbmInfoSearcher(CbmInfoInputEntity inputEntity, string station_level, string management, string station_area, string station_scode);
    }
}
