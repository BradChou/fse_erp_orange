﻿using LightYear.JunFuTrans.BL.BE.Menu;
using LightYear.JunFuTrans.DA.Repositories.MenuBar;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Formatters;
using LightYear.JunFuTrans.BL.BE.Account;

namespace LightYear.JunFuTrans.BL.Services.SystemFunction
{
    public class SystemFunctionService : ISystemFunctionService
    {
        public IMenuCategoryRepository MenuCategoryRepository { get; set; }
        public IMenuSubCategoryRepository MenuSubCategoryRepository { get; set; }
        public IMenuFunctionRepository MenuFunctionRepository { get; set; }
        public IFunctionRoleMappingRepository FunctionRoleMappingRepository { get; set; }
        public IFunctionUserMappingRepository FunctionUserMappingRepository { get; set; }

        public IUserRoleMappingRepository UserRoleMappingRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public SystemFunctionService(IMenuCategoryRepository menuCategoryRepository,IMapper mapper,IMenuSubCategoryRepository menuSubCategoryRepository, IMenuFunctionRepository menuFunctionRepository, IFunctionRoleMappingRepository functionRoleMappingRepository, IFunctionUserMappingRepository functionUserMappingRepository, IUserRoleMappingRepository userRoleMappingRepository)
        {
            this.MenuCategoryRepository = menuCategoryRepository;
            this.MenuSubCategoryRepository = menuSubCategoryRepository;
            this.MenuFunctionRepository = menuFunctionRepository;
            this.Mapper = mapper;
            this.FunctionUserMappingRepository = functionUserMappingRepository;
            this.FunctionRoleMappingRepository = functionRoleMappingRepository;
            this.UserRoleMappingRepository = userRoleMappingRepository;
        }

        public List<SystemCategoryEntity> GetAllMenuCategory(string company = "")
        {
            List<SystemCategory> data;

            if(company.Length == 0)
                data = this.MenuCategoryRepository.GetAllMenuBarCategory();
            else
                data = this.MenuCategoryRepository.GetJFAllMenuBarCategory();

            var menuCategoryResult = this.Mapper.Map<List<SystemCategoryEntity>>(data);

            return menuCategoryResult;
        }

        public List<SystemSubCategoryEntity> GetMenuSubCategoriesByCategoryId(int categoryId)
        {
            var data = this.MenuSubCategoryRepository.GetSubCategoryByCategoryId(categoryId);

            var menuSubCategoriesResult = this.Mapper.Map<List<SystemSubCategoryEntity>>(data);

            return menuSubCategoriesResult;
        }

        public List<SystemFunctionEntity> GetMenuFunctionsBySubCategoryId(int subCategoryId)
        {
            var data = this.MenuFunctionRepository.GetAllSystemFucntionBySubCategoryId(subCategoryId);

            var menuFunctionsResult = this.Mapper.Map<List<SystemFunctionEntity>>(data);

            return menuFunctionsResult;
        }

        public List<SystemCategoryEntity> GetCategoryEntities(string company = "")
        {
            List<SystemCategoryEntity> allMenuCategory = GetAllMenuCategory(company);

            foreach(SystemCategoryEntity category in allMenuCategory)
            {

                List<SystemSubCategoryEntity> subCategoryies = GetMenuSubCategoriesByCategoryId(category.Id);

                foreach(SystemSubCategoryEntity subCategoryEntity in subCategoryies)
                {
                    category.SystemSubCategoryEntities.Add(subCategoryEntity);

                    List<SystemFunctionEntity> systemFunctions = GetMenuFunctionsBySubCategoryId(subCategoryEntity.Id);

                    foreach(SystemFunctionEntity function in systemFunctions)
                    {
                        subCategoryEntity.SystemFunctionEntities.Add(function);
                    }
                }
            }
            return allMenuCategory;
        }

        public FrontendMenuEntity GetCategoryEntitiesByUserInfo(UserInfoEntity userInfoEntity)
        {
            //有權限的functionId
            List<int> hasFunctionIds = new List<int>();

            //取得帳號的權限
            var userFunctions = FunctionUserMappingRepository.GetAllSystemFunctionUserMappingsByUserIdAndUserType(userInfoEntity.UsersId, (int)userInfoEntity.AccountType);

            foreach(SystemFunctionUserMapping systemFunctionUserMapping in userFunctions )
            {
                if( !hasFunctionIds.Contains(systemFunctionUserMapping.FunctionId) )
                {
                    hasFunctionIds.Add(systemFunctionUserMapping.FunctionId);
                }
            }

            //取得角色權限
            foreach(int roleId in userInfoEntity.RoleId)
            {
                var roleFunctions = FunctionRoleMappingRepository.GetAllSystemFunctionRoleMappingsByRoleId(roleId);

                foreach(SystemFunctionRoleMapping systemFunctionRoleMapping in roleFunctions)
                {
                    if( !hasFunctionIds.Contains(systemFunctionRoleMapping.FunctionId) )
                    {
                        hasFunctionIds.Add(systemFunctionRoleMapping.FunctionId);
                    }
                }
            }

            List<SystemCategoryEntity> realSystems = new List<SystemCategoryEntity>();

            List<SystemCategoryEntity> allMenuCategory = GetAllMenuCategory(userInfoEntity.Company);

            foreach (SystemCategoryEntity category in allMenuCategory)
            {
                bool addMain = false;
                List<SystemSubCategoryEntity> subCategoryies = GetMenuSubCategoriesByCategoryId(category.Id);

                foreach (SystemSubCategoryEntity subCategoryEntity in subCategoryies)
                {
                    bool addSub = false;

                    List<SystemFunctionEntity> systemFunctions = GetMenuFunctionsBySubCategoryId(subCategoryEntity.Id);

                    foreach (SystemFunctionEntity function in systemFunctions)
                    {
                        if( hasFunctionIds.Contains(function.Id) )
                        {
                            addMain = true;
                            addSub = true;
                            subCategoryEntity.SystemFunctionEntities.Add(function);
                        }
                    }

                    if( addSub )
                    {
                        category.SystemSubCategoryEntities.Add(subCategoryEntity);
                    }
                }

                if( addMain )
                {
                    realSystems.Add(category);
                }
            }

            FrontendMenuEntity frontendMenuEntity = new FrontendMenuEntity
            {
                Categories = realSystems,
                UserInfoEntity = userInfoEntity
            };

            return frontendMenuEntity;
        }

        public List<FunctionFrontendSettingEntity> GetFrontendFunctionInfos(int roleId, string company)
        {
            //取得該角色有的權限
            var functionRoleMappingList = this.FunctionRoleMappingRepository.GetAllSystemFunctionRoleMappingsByRoleId(roleId);

            var findData = functionRoleMappingList.ToDictionary(p => p.FunctionId);

            //取得權限相關清單
            List<SystemCategoryEntity> allMenuCategory = GetCategoryEntities(company);

            List<FunctionFrontendSettingEntity> functionFrontendSettingEntities = new List<FunctionFrontendSettingEntity>();
            
            //主分類
            foreach(SystemCategoryEntity systemCategoryEntity in allMenuCategory)
            {
                FunctionFrontendSettingEntity functionFrontendSettingEntity = new FunctionFrontendSettingEntity
                {
                    id = systemCategoryEntity.Id + 100000,
                    text = systemCategoryEntity.CategoryName,

                    items = new List<FunctionFrontendSettingEntity>()
                };

                functionFrontendSettingEntities.Add(functionFrontendSettingEntity);

                if (systemCategoryEntity.SystemSubCategoryEntities.Count > 0)
                {
                    functionFrontendSettingEntity.expanded = true;
                }

                //次分類
                foreach(SystemSubCategoryEntity systemSubCategoryEntity in systemCategoryEntity.SystemSubCategoryEntities)
                {
                    FunctionFrontendSettingEntity subFunctionFrontendSettingEntity = new FunctionFrontendSettingEntity
                    {
                        id = systemSubCategoryEntity.Id + 1000,
                        text = systemSubCategoryEntity.SubCategoryName,
                        items = new List<FunctionFrontendSettingEntity>()
                    };

                    if (systemSubCategoryEntity.SystemFunctionEntities.Count > 0)
                    {
                        subFunctionFrontendSettingEntity.expanded = true;
                    }

                    functionFrontendSettingEntity.items.Add(subFunctionFrontendSettingEntity);

                    //功能項目
                    foreach (SystemFunctionEntity systemFunctionEntity in systemSubCategoryEntity.SystemFunctionEntities)
                    {
                        bool checkedFunction = false;

                        if( findData.ContainsKey(systemFunctionEntity.Id) )
                        {
                            checkedFunction = true;
                        }

                        FunctionFrontendSettingEntity finalFunctionFrontendSettingEntity = new FunctionFrontendSettingEntity
                        {
                            id = systemFunctionEntity.Id,
                            text = systemFunctionEntity.FunctionName,
                            @checked = checkedFunction
                        };

                        subFunctionFrontendSettingEntity.items.Add(finalFunctionFrontendSettingEntity);
                    }

                }

            }

            return functionFrontendSettingEntities;
        }

        public List<FunctionFrontendSettingEntity> GetFrontendUserFunctionInfos(int userId, int accountType, string company = "")
        {
            //取得使用者該有的權限
            var functionUserMappingList = this.FunctionUserMappingRepository.GetAllSystemFunctionUserMappingsByUserIdAndUserType(userId, accountType);

            var findData = functionUserMappingList.ToDictionary(p => p.FunctionId);

            //取得使用者的角色清單
            List<int> roleFuncitonIds = new List<int>();
            var roleInfoDatas = this.UserRoleMappingRepository.GetAllUserRoleMappingsByUserIdAndUserType(userId, accountType);
            foreach(UserRoleMapping userRoleMapping in roleInfoDatas)
            {
                var functionRoleMappingList = this.FunctionRoleMappingRepository.GetAllSystemFunctionRoleMappingsByRoleId(userRoleMapping.RoleId);

                foreach(SystemFunctionRoleMapping systemFunctionRoleMapping in functionRoleMappingList)
                {
                    if( !roleFuncitonIds.Contains(systemFunctionRoleMapping.FunctionId) )
                    {
                        roleFuncitonIds.Add(systemFunctionRoleMapping.FunctionId);
                    }
                }
            }

            //取得權限相關清單
            List<SystemCategoryEntity> allMenuCategory = GetCategoryEntities(company);

            List<FunctionFrontendSettingEntity> functionFrontendSettingEntities = new List<FunctionFrontendSettingEntity>();

            //主分類
            foreach (SystemCategoryEntity systemCategoryEntity in allMenuCategory)
            {
                FunctionFrontendSettingEntity functionFrontendSettingEntity = new FunctionFrontendSettingEntity
                {
                    id = systemCategoryEntity.Id + 100000,
                    text = systemCategoryEntity.CategoryName,

                    items = new List<FunctionFrontendSettingEntity>()
                };

                functionFrontendSettingEntities.Add(functionFrontendSettingEntity);

                if (systemCategoryEntity.SystemSubCategoryEntities.Count > 0)
                {
                    functionFrontendSettingEntity.expanded = true;
                }

                //次分類
                foreach (SystemSubCategoryEntity systemSubCategoryEntity in systemCategoryEntity.SystemSubCategoryEntities)
                {
                    FunctionFrontendSettingEntity subFunctionFrontendSettingEntity = new FunctionFrontendSettingEntity
                    {
                        id = systemSubCategoryEntity.Id + 1000,
                        text = systemSubCategoryEntity.SubCategoryName,
                        items = new List<FunctionFrontendSettingEntity>()
                    };

                    if (systemSubCategoryEntity.SystemFunctionEntities.Count > 0)
                    {
                        subFunctionFrontendSettingEntity.expanded = true;
                    }

                    functionFrontendSettingEntity.items.Add(subFunctionFrontendSettingEntity);

                    //功能項目
                    foreach (SystemFunctionEntity systemFunctionEntity in systemSubCategoryEntity.SystemFunctionEntities)
                    {
                        bool checkedFunction = false;
                        bool checkedEnable = true;

                        if (findData.ContainsKey(systemFunctionEntity.Id))
                        {
                            checkedFunction = true;
                        }

                        if(roleFuncitonIds.Contains(systemFunctionEntity.Id))
                        {
                            checkedFunction = true;
                            checkedEnable = false;
                        }

                        FunctionFrontendSettingEntity finalFunctionFrontendSettingEntity = new FunctionFrontendSettingEntity
                        {
                            id = systemFunctionEntity.Id,
                            text = systemFunctionEntity.FunctionName,
                            @checked = checkedFunction,
                            enabled = checkedEnable
                        };

                        subFunctionFrontendSettingEntity.items.Add(finalFunctionFrontendSettingEntity);
                    }

                }

            }

            return functionFrontendSettingEntities;
        }

        //存入角色對應表
        public void SaveRoleFunction(int roleId, int[] funcitonIds)
        {
            //先刪除目前有的角色資料
            this.FunctionRoleMappingRepository.DeleteByRoleId(roleId);

            foreach(int functionId in funcitonIds)
            {
                if (functionId < 1000)
                {
                    SystemFunctionRoleMapping systemFunctionRoleMapping = new SystemFunctionRoleMapping
                    {
                        FunctionId = functionId,
                        RoleId = roleId,
                        FunctionValue = 1
                    };

                    this.FunctionRoleMappingRepository.Insert(systemFunctionRoleMapping);
                }
            }
        }

        public void SaveUserFunction(int userId, int accountType, int[] funcitonIds)
        {
            //先刪除目前有的使用者資料
            this.FunctionUserMappingRepository.DeleteByUserIdAndAccountType(userId, accountType);

            foreach (int functionId in funcitonIds)
            {
                if (functionId < 1000)
                {
                    SystemFunctionUserMapping systemFunctionUserMapping = new SystemFunctionUserMapping
                    {
                        FunctionId = functionId,
                        UserId = userId,
                        UserType = accountType,
                        FunctionValue = 1
                    };

                    this.FunctionUserMappingRepository.Insert(systemFunctionUserMapping);
                }
            }
        }
    }
}
