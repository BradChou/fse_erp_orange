﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.DriverShiftSchedules;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.DriverShiftSchedules;
using System;
using System.Collections.Generic;
using System.Text;


namespace LightYear.JunFuTrans.BL.Services.DriverShiftSchedules
{
    public class DriverShiftSchedulesService : IDriverShiftSchedulesService
    {
        IDriverShiftSchedulesRepository DriverShiftSchedulesRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public DriverShiftSchedulesService(IDriverShiftSchedulesRepository driverShiftSchedulesRepository, IMapper mapper)
        {
            this.DriverShiftSchedulesRepository = driverShiftSchedulesRepository;
            this.Mapper = mapper;
        }


        public List<InputEntity> GetAllByStationAndDate(string scode, DateTime date)
        {
            var data = DriverShiftSchedulesRepository.GetAllByStationAndDate(scode, date);

            var inputEntities = this.Mapper.Map<List<InputEntity>>(data);

            return inputEntities;
        }
            
        public List<DriverShiftArrangement> GetAllByStationScode(string scode)
        {
            var data = DriverShiftSchedulesRepository.GetAllByStationScode(scode);

            var driverShiftArrangementEntities = this.Mapper.Map<List<DriverShiftArrangement>>(data);

            return driverShiftArrangementEntities;
        }

        public int GetDriversCount(string scode)
        {
            var data = DriverShiftSchedulesRepository.GetDriversCount(scode);
            return data;
        }

        public int GetTakeOffDriversCount(string scode, DateTime date)
        {
            var data = DriverShiftSchedulesRepository.GetTakeOffDriversCount(scode,date);
            return data;
        }

        public void Delete(int id)
        {
           this.DriverShiftSchedulesRepository.Delete(id);
        }

        public void Save(InputEntity inputEntity)
        {
            inputEntity.UpdateDate = DateTime.Now;

            var driverShiftArrangement = this.Mapper.Map<DriverShiftArrangement>(inputEntity);

            if (inputEntity.Id == 0)
            {
                this.DriverShiftSchedulesRepository.Insert(driverShiftArrangement);
            }
            else
            {
                driverShiftArrangement.TakeOffDate = driverShiftArrangement.TakeOffDate.AddDays(1);

                this.DriverShiftSchedulesRepository.Update(driverShiftArrangement);
            }
        }
    }
    
}
