﻿using LightYear.JunFuTrans.BL.BE.DriverDispatch;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.DriverDispatch;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LightYear.JunFuTrans.BL.Services.DriverDispatch
{
    public class DriverDispatchService : IDriverDispatchService
    {
        public IDriverDispatchRepository DriverDispatchRespoitory { get; set; }

        public DriverDispatchService(IDriverDispatchRepository driverDispatchRepository)
        {
            DriverDispatchRespoitory = driverDispatchRepository;
        }

        public void UpdateMDSD(string customerCode, string checkNumber, string md, string sd)
        {

            DriverDispatchRespoitory.UpdateMDSD(customerCode, checkNumber, md, sd);

            return;
        }

        public List<TbItemCode> GetScanItemOption(string scanItem)
        {
            List<TbItemCode> output = new List<TbItemCode>();

            if (scanItem.Equals("5"))
                output = DriverDispatchRespoitory.GetPickUpOption();

            return output;
        }

        public List<DriverDispatchEntity> GetDispatchPickUpList(string driverCode)
        {
            var output = DriverDispatchRespoitory.GetTheThirdPlatformDispatchPickUpList(driverCode)
                         .Where(a => a.ActualPickUpCount < a.ShouldPickUpCount).ToList();
            
            var general = DriverDispatchRespoitory.GetGeneralDispatchPickUpList(driverCode)
                          .Where(a => a.ActualPickUpCount == 0).ToList();

            output.AddRange(general);

            for (int i = 0; i < output.Count; i++)
            {
                if (output.Where(o => o.DriverCode == output[i].DriverCode 
                    && o.DispatchDate == output[i].DispatchDate 
                    && o.CustomerTel == output[i].CustomerTel 
                    && o.CustomerName == output[i].CustomerName 
                    && o.CustomerAddress == output[i].CustomerAddress
                    && o.CustomerCode == output[i].CustomerCode
                    && o.ShouldPickUpCount == output[i].ShouldPickUpCount
                    && o.ActualPickUpCount == output[i].ActualPickUpCount
                    && o.PickUpType == output[i].PickUpType).Count() > 1)
                {
                    output.Remove(output[i]);
                    i--; //index在remove之後會改變，要減回來。
                }
            }
            return output;
        }

        public List<CheckNumberListWithinDispathEntity> GetCheckNumberListWithinDispath(DispatchJsonEntity dispatchJsonEntity)
        {
            List<TbItemCode> scanItemOption = DriverDispatchRespoitory.GetAllPickUpOption();

            var output = DriverDispatchRespoitory.GetGeneralCheckNumberListWithinDispath(dispatchJsonEntity);
            output.AddRange(DriverDispatchRespoitory.GetTheThirdPlatformCheckNumberListWithinDispath(dispatchJsonEntity));

            for (int i = 0; i < output.Count; i++)
            {
                if (output.Where(o => o.IsShip == output[i].IsShip 
                                   && o.Pieces == output[i].Pieces 
                                   && o.SendContact == output[i].SendContact 
                                   && o.ShipCategory == output[i].ShipCategory
                                   && o.CheckNumber == output[i].CheckNumber).Count() > 1)
                {
                    output.Remove(output[i]);
                    i--; //index在remove之後會改變，要減回來。
                }
            }
                       
            foreach (var item in output)
            {
                item.ShipCategory = (scanItemOption.Where(a => a.CodeId.Equals(item.ShipCategory)).FirstOrDefault()?.CodeName) ?? "";
            }

            return output;
        }

        public Driver GetDriverMDSD(string driver)
        {
            return DriverDispatchRespoitory.GetDriverMDSD(driver);
        }
    }
}
