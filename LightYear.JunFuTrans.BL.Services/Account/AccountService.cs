﻿using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.Menu;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.Utilities.Security;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace LightYear.JunFuTrans.BL.Services.Account
{
    public class AccountService : IAccountService
    {
        public IDriverRepository DriverRepository { get; set; }

        public IEmpRepository EmpRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public IAccountsRepository AccountsRepository { get; set; }

        public IRoleRepository RoleRepository { get; set; }

        public IUserRoleMappingRepository UserRoleMappingRepository { get; set; }

        public IFunctionRoleMappingRepository FunctionRoleMappingRepository { get; set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public AccountService(IDriverRepository driverRepository, IEmpRepository empRepository, IStationRepository stationRepository, IAccountsRepository accountsRepository, IRoleRepository roleRepository, IUserRoleMappingRepository userRoleMappingRepository, IFunctionRoleMappingRepository functionRoleMappingRepository, ICustomerRepository customerRepository, IMapper mapper)
        {
            this.DriverRepository = driverRepository;
            this.EmpRepository = empRepository;
            this.StationRepository = stationRepository;
            this.AccountsRepository = accountsRepository;
            this.RoleRepository = roleRepository;
            this.UserRoleMappingRepository = userRoleMappingRepository;
            this.FunctionRoleMappingRepository = functionRoleMappingRepository;
            this.CustomerRepository = customerRepository;
            this.Mapper = mapper;
        }

        /// <summary>
        /// 由帳號密碼取得司機物件
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="password">密碼</param>
        /// <returns></returns>
        public DriverEntity GetDriver(string account, string password)
        {
            string encodePassword = SkyEyes.GetEncodePassword(password);

            var driver = this.DriverRepository.GetDriverByAccountAndPassword(account, encodePassword);

            var driverEntity = this.Mapper.Map<DriverEntity>(driver);

            return driverEntity;
        }

        /// <summary>
        /// 由帳號密碼取得使用者資料
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="password">密碼</param>
        /// <returns>使用者資料</returns>
        public UserInfoEntity DriverLogin(string account, string password)
        {
            string encodePassword = SkyEyes.GetEncodePassword(password);

            var driver = this.DriverRepository.GetDriverByAccountAndPassword(account, encodePassword);

            var userInfoEntity = DriverToUserInfo(driver);

            return userInfoEntity;
        }

        /// <summary>
        /// 由帳號取得使用者資料
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns>使用者資料</returns>
        public UserInfoEntity GetUserInfoByAccount(string account)
        {
            var driver = this.DriverRepository.GetByDriverCode(account);

            var userInfoEntity = DriverToUserInfo(driver);

            return userInfoEntity;
        }

        /// <summary>
        /// 內部使用，把司機變成使用者資料
        /// </summary>
        /// <param name="driver">司機(資料庫物件)</param>
        /// <returns></returns>
        private UserInfoEntity DriverToUserInfo(TbDriver driver)
        {
            if (driver == null)
            {
                return null;
            }

            var userInfoEntity = this.Mapper.Map<UserInfoEntity>(driver);

            userInfoEntity.AccountType = BE.Enumeration.AccountType.Driver;

            //因為司機資料的站所不準，所以查員工個人資料的站所
            var emp = this.EmpRepository.GetByEmpCode(driver.EmpCode);

            if (emp != null && emp.Station != null && emp.Station.Length > 0)
            {
                try
                {
                    int stationId = Convert.ToInt32(emp.Station.Trim());
                    var station = this.StationRepository.GetById(stationId);

                    userInfoEntity.Station = station.StationScode;

                    userInfoEntity.StationShowName = string.Format("{0}({1})", station.StationName, station.StationScode);
                }
                catch
                {
                    //dummy
                }
            }

            int driverId = Decimal.ToInt32(driver.DriverId);
            //取得使用者角色
            List<UserRoleMapping> userRoleMappings = this.UserRoleMappingRepository.GetAllUserRoleMappingsByUserIdAndUserType(driverId, 1);

            foreach (UserRoleMapping userRoleMapping in userRoleMappings)
            {
                userInfoEntity.RoleId.Add(userRoleMapping.RoleId);
                Role role = this.RoleRepository.GetRole(userRoleMapping.RoleId);

                userInfoEntity.RoleName.Add(role.RoleName);
            }


            return userInfoEntity;
        }

        public UserInfoEntity SkyEyeLogin(string account, string password)
        {
            string encodePassword = SkyEyes.GetEncodePassword(password);

            var accountEntity = this.AccountsRepository.GetByAccountAndPassword(account, encodePassword);

            if (accountEntity == null)
            {
                return null;
            }

            var userInfoEntity = this.Mapper.Map<UserInfoEntity>(accountEntity);

            userInfoEntity.AccountType = BE.Enumeration.AccountType.Customer;

            int accountId = Decimal.ToInt32(accountEntity.AccountId);
            //取得使用者角色
            List<UserRoleMapping> userRoleMappings = this.UserRoleMappingRepository.GetAllUserRoleMappingsByUserIdAndUserType(accountId, 2);

            foreach (UserRoleMapping userRoleMapping in userRoleMappings)
            {
                userInfoEntity.RoleId.Add(userRoleMapping.RoleId);
                Role role = this.RoleRepository.GetRole(userRoleMapping.RoleId);

                userInfoEntity.RoleName.Add(role.RoleName);
            }

            //取得所屬站所
            var emp = this.EmpRepository.GetByEmpCode(accountEntity.EmpCode);

            if (emp != null && emp.Station != null && emp.Station.Length > 0)
            {
                try
                {
                    int stationId = Convert.ToInt32(emp.Station.Trim());
                    var station = this.StationRepository.GetById(stationId);

                    userInfoEntity.Station = station.StationScode;

                    userInfoEntity.StationShowName = string.Format("{0}({1})", station.StationName, station.StationScode);
                }
                catch
                {
                    //dummy
                }
            }

            return userInfoEntity;
        }

        public UserInfoEntity JunFuLogin(string account, string password)
        {
            string encodePassword = SkyEyes.GetEncodePassword(password);

            var accountEntity = this.AccountsRepository.GetJunFuAccounts(account, encodePassword);

            if (accountEntity == null)
            {
                return null;
            }

            var userInfoEntity = this.Mapper.Map<UserInfoEntity>(accountEntity);

            userInfoEntity.AccountType = BE.Enumeration.AccountType.Customer;

            int accountId = Decimal.ToInt32(accountEntity.AccountId);
            //取得使用者角色
            List<UserRoleMapping> userRoleMappings = this.UserRoleMappingRepository.GetAllUserRoleMappingsByUserIdAndUserType(accountId, 2);

            foreach (UserRoleMapping userRoleMapping in userRoleMappings)
            {
                userInfoEntity.RoleId.Add(userRoleMapping.RoleId);
                Role role = this.RoleRepository.GetRole(userRoleMapping.RoleId);

                userInfoEntity.RoleName.Add(role.RoleName);
            }

            //取得所屬站所
            var emp = this.EmpRepository.GetByEmpCode(accountEntity.EmpCode);

            if (emp != null && emp.Station != null && emp.Station.Length > 0)
            {
                try
                {
                    int stationId = Convert.ToInt32(emp.Station.Trim());
                    var station = this.StationRepository.GetById(stationId);

                    userInfoEntity.Station = station.StationScode;

                    userInfoEntity.StationShowName = string.Format("{0}({1})", station.StationName, station.StationScode);
                }
                catch
                {
                    //dummy
                }
            }

            return userInfoEntity;
        }

        /// <summary>
        /// 先用司機登入，如果不行改用一般登入
        /// </summary>
        /// <param name="account"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public UserInfoEntity CommonLogin(string account, string password)
        {
            var userInfoEntity = this.DriverLogin(account, password);

            if (userInfoEntity == null)
            {
                userInfoEntity = this.SkyEyeLogin(account, password);
            }

            return userInfoEntity;
        }

        public UserInfoEntity JunFuCommonLogin(string account, string password)
        {
            var JunFuInfoEntity = this.JunFuLogin(account, password);

            return JunFuInfoEntity;
        }

        public List<RoleInfoEntity> GetAllRoles(string company)
        {
            List<Role> roles;
            if (company.Length == 0)
                roles = RoleRepository.GetAllRoles();
            else
                roles = RoleRepository.GetJFAllRoles();

            var roleEntities = this.Mapper.Map<List<RoleInfoEntity>>(roles);

            return roleEntities;
        }

        public RoleInfoEntity GetRole(int id)
        {
            var data = this.RoleRepository.GetRole(id);

            var roleEntity = this.Mapper.Map<RoleInfoEntity>(data);

            return roleEntity;
        }

        public void SaveRole(ref RoleInfoEntity roleInfoEntity)
        {
            var role = this.Mapper.Map<Role>(roleInfoEntity);

            if (roleInfoEntity.RoleId == 0)
            {
                int id = this.RoleRepository.Insert(role);
                roleInfoEntity.RoleId = id;
            }
            else
            {
                this.RoleRepository.Update(role);
            }
        }

        public void DeleteRole(int id)
        {
            this.UserRoleMappingRepository.DeleteByRoleId(id);
            this.FunctionRoleMappingRepository.DeleteByRoleId(id);
            this.RoleRepository.Delete(id);
        }

        /// <summary>
        /// 取得會員資料
        /// </summary>
        /// <param name="startWith"></param>
        /// <returns></returns>
        public KendoSelectUserEntity GetStartWith(string startWith)
        {
            List<TbAccount> getUserData;
            //沒資料時抓全部，否則抓開頭資料
            if (startWith == string.Empty)
            {
                getUserData = this.AccountsRepository.GetNumAccounts(100);
            }
            else
            {
                getUserData = this.AccountsRepository.GetAccountsStartWith(startWith, 100);
            }

            var userEntities = this.Mapper.Map<List<UserInfoEntity>>(getUserData);

            KendoSelectUserEntity kendoSelectUserEntity = new KendoSelectUserEntity
            {
                results = userEntities,
                __count = userEntities.Count
            };

            return kendoSelectUserEntity;

        }

        public KendoSelectUserEntity GetJFStartWith(string startWith, string managerType = "1")
        {
            List<TbAccount> getUserData;
            //沒資料時抓全部，否則抓開頭資料
            if (startWith == string.Empty)
            {
                getUserData = AccountsRepository.GetJFNumAccounts(100, managerType);
            }
            else
            {
                getUserData = AccountsRepository.GetJFAccountsStartWith(startWith, 100, managerType);
            }

            var userEntities = Mapper.Map<List<UserInfoEntity>>(getUserData);

            KendoSelectUserEntity kendoSelectUserEntity = new KendoSelectUserEntity
            {
                results = userEntities,
                __count = userEntities.Count
            };

            return kendoSelectUserEntity;
        }

        public KendoSelectUserEntity GetDriversStartWith(string startWith)
        {
            List<TbDriver> getUserData;
            //沒資料時抓全部，否則抓開頭資料
            if (startWith == string.Empty)
            {
                getUserData = this.DriverRepository.GetNumAccounts(100);
            }
            else
            {
                getUserData = this.DriverRepository.GetAccountsStartWith(startWith, 100);
            }

            var userEntities = this.Mapper.Map<List<UserInfoEntity>>(getUserData);

            KendoSelectUserEntity kendoSelectUserEntity = new KendoSelectUserEntity
            {
                results = userEntities,
                __count = userEntities.Count
            };

            return kendoSelectUserEntity;

        }

        /// <summary>
        /// 由序號取得會員資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="accountType"></param>
        /// <returns></returns>
        public UserInfoEntity GetUserById(int userId, int accountType)
        {

            if (accountType == 1)
            {
                var accountEntity = this.DriverRepository.GetById(userId);
                var userInfoEntity = this.Mapper.Map<UserInfoEntity>(accountEntity);
                return userInfoEntity;

            }
            else
            {
                var accountEntity = this.AccountsRepository.GetById(userId);

                var userInfoEntity = this.Mapper.Map<UserInfoEntity>(accountEntity);
                return userInfoEntity;

            }


        }

        public List<FunctionFrontendSettingEntity> GetAllRolesFrontendSetting()
        {
            List<FunctionFrontendSettingEntity> functionFrontendSettingEntities = new List<FunctionFrontendSettingEntity>();

            var roles = this.RoleRepository.GetAllRoles();

            foreach (Role role in roles)
            {
                FunctionFrontendSettingEntity functionFrontendSettingEntity = new FunctionFrontendSettingEntity
                {
                    id = role.Id,
                    text = role.RoleName,
                    @checked = false
                };

                functionFrontendSettingEntities.Add(functionFrontendSettingEntity);
            }

            return functionFrontendSettingEntities;
        }

        public List<FunctionFrontendSettingEntity> GetAllRolesFrontendSettingByUserIdAndAccountType(int userId, int accountType, string company = "")
        {
            //取得使用者的角色設定
            List<UserRoleMapping> userRoleMappings = this.UserRoleMappingRepository.GetAllUserRoleMappingsByUserIdAndUserType(userId, accountType);

            var findData = userRoleMappings.ToDictionary(p => p.RoleId);

            List<FunctionFrontendSettingEntity> functionFrontendSettingEntities = new List<FunctionFrontendSettingEntity>();

            List<Role> roles;
            if (company.Length == 0)
                roles = RoleRepository.GetAllRoles();
            else
                roles = RoleRepository.GetJFAllRoles();

            foreach (Role role in roles)
            {
                bool checkedFunction = false;

                if (findData.ContainsKey(role.Id))
                {
                    checkedFunction = true;
                }

                FunctionFrontendSettingEntity functionFrontendSettingEntity = new FunctionFrontendSettingEntity
                {
                    id = role.Id,
                    text = role.RoleName,
                    @checked = checkedFunction
                };

                functionFrontendSettingEntities.Add(functionFrontendSettingEntity);
            }

            return functionFrontendSettingEntities;
        }

        public void SaveUserRole(int userId, int accountType, int[] roleIds)
        {
            //先刪除所有使用者的角色
            this.UserRoleMappingRepository.DeleteByUserIdAndAccountType(userId, accountType);

            //再一個個寫入
            foreach (int roleId in roleIds)
            {
                UserRoleMapping userRoleMapping = new UserRoleMapping
                {
                    UserId = userId,
                    UserType = accountType,
                    RoleId = roleId
                };

                this.UserRoleMappingRepository.Insert(userRoleMapping);
            }

        }

        public List<UserSimpleForFrontend> GetUserSimpleByRoleId(int roleId)
        {
            List<UserSimpleForFrontend> userSimpleForFrontends = new List<UserSimpleForFrontend>();

            //先取得角色擁有的使用者
            var userDatas = this.UserRoleMappingRepository.GetAllUserRoleMappingsByRoleId(roleId);

            foreach (UserRoleMapping userRoleMapping in userDatas)
            {
                string name = string.Empty;

                string typeName = string.Empty;

                if (userRoleMapping.UserType == 1)
                {
                    typeName = "司機帳號";

                    TbDriver tbDriver = this.DriverRepository.GetById(Convert.ToDecimal(userRoleMapping.UserId));

                    name = string.Format("{0}-({1})", tbDriver.DriverCode, tbDriver.DriverName);
                }
                else
                {
                    typeName = "一般帳號";

                    TbAccount tbAccount = this.AccountsRepository.GetById(Convert.ToDecimal(userRoleMapping.UserId));

                    name = string.Format("{0}-({1})", tbAccount.AccountCode, tbAccount.UserName);
                }


                UserSimpleForFrontend userSimpleForFrontend = new UserSimpleForFrontend
                {
                    Id = userRoleMapping.UserId,
                    AccountType = userRoleMapping.UserType,
                    UserName = name,
                    TypeName = typeName
                };

                userSimpleForFrontends.Add(userSimpleForFrontend);
            }

            return userSimpleForFrontends;
        }

        public UserInfoWithAddressEntity LoginForNop(string account, string password)
        {
            UserInfoEntity userInfoEntity = this.SkyEyeLogin(account, password);

            if (userInfoEntity == null)
            {
                return null;
            }

            UserInfoWithAddressEntity userInfoWithAddressEntity = new UserInfoWithAddressEntity
            {
                AccountType = userInfoEntity.AccountType,
                UserAccount = userInfoEntity.UserAccount,
                CookieExpiration = userInfoEntity.CookieExpiration,
                RoleId = userInfoEntity.RoleId,
                RoleName = userInfoEntity.RoleName,
                ActionStation = userInfoEntity.ActionStation,
                ShowName = userInfoEntity.ShowName,
                Station = userInfoEntity.Station,
                StationShowName = userInfoEntity.StationShowName,
                UsersId = userInfoEntity.UsersId,
                UserName = userInfoEntity.UserName
            };

            //取得額外資訊
            var accountEntity = this.AccountsRepository.GetByAccountCode(account);

            //如果是客戶
            if (accountEntity.CustomerCode != null && accountEntity.CustomerCode.Length > 0)
            {
                var customer = this.CustomerRepository.GetByCustomerCode(accountEntity.CustomerCode);

                if (customer != null)
                {
                    string firstName = (customer.ShipmentsPrincipal.Length > 1) ? customer.ShipmentsPrincipal.Substring(1) : customer.CustomerName;
                    string lastName = (customer.ShipmentsPrincipal.Length > 1) ? customer.ShipmentsPrincipal.Substring(0, 1) : string.Empty;
                    string email = (accountEntity.UserEmail != null && accountEntity.UserEmail.Length > 0) ? accountEntity.UserEmail : customer.ShipmentsEmail;
                    string companyName = accountEntity.UserName;
                    string county = customer.ShipmentsArea;
                    string city = customer.ShipmentsCity;
                    string address = customer.ShipmentsRoad;
                    string phone = customer.Telephone;
                    string uniformNumbers = customer.UniformNumbers;

                    userInfoWithAddressEntity.FirstName = firstName;
                    userInfoWithAddressEntity.LastName = lastName;
                    userInfoWithAddressEntity.Email = email;
                    userInfoWithAddressEntity.Company = companyName;
                    userInfoWithAddressEntity.County = county;
                    userInfoWithAddressEntity.City = city;
                    userInfoWithAddressEntity.Address = address;
                    userInfoWithAddressEntity.PhoneNumber = phone;
                    userInfoWithAddressEntity.TaxIdNumber = uniformNumbers;
                }
            }

            return userInfoWithAddressEntity;
        }
    }
}
