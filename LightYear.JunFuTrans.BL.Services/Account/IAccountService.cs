﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.Menu;
using LightYear.JunFuTrans.DA.JunFuTransDb;

namespace LightYear.JunFuTrans.BL.Services.Account
{
    public interface IAccountService
    {
        /// <summary>
        /// 由帳號密碼取得司機物件
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="password">密碼</param>
        /// <returns></returns>
        DriverEntity GetDriver(string account, string password);

        /// <summary>
        /// 由帳號密碼取得使用者資料
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="password">密碼</param>
        /// <returns>使用者資料</returns>
        UserInfoEntity DriverLogin(string account, string password);

        UserInfoEntity SkyEyeLogin(string account, string password);

        UserInfoEntity CommonLogin(string account, string password);

        UserInfoEntity JunFuCommonLogin(string account, string password);

        UserInfoWithAddressEntity LoginForNop(string account, string password);

        /// <summary>
        /// 由帳號取得使用者資料
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns>使用者資料</returns>
        UserInfoEntity GetUserInfoByAccount(string account);

        /// <summary>
        /// 列出角色
        /// </summary>
        /// <returns></returns>
        List<RoleInfoEntity> GetAllRoles(string company);

        /// <summary>
        /// 取得角色
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        RoleInfoEntity GetRole(int id);

        /// <summary>
        /// 儲存角色
        /// </summary>
        /// <param name="roleInfoEntity"></param>
        void SaveRole(ref RoleInfoEntity roleInfoEntity);

        /// <summary>
        /// 刪除角色
        /// </summary>
        /// <param name="id"></param>
        void DeleteRole(int id);


        List<FunctionFrontendSettingEntity> GetAllRolesFrontendSetting();

        List<FunctionFrontendSettingEntity> GetAllRolesFrontendSettingByUserIdAndAccountType(int userId, int accountType, string company = "");

        /// <summary>
        /// 取得以startWith開頭的帳號
        /// </summary>
        /// <param name="startWith">開頭字串</param>
        /// <returns></returns>
        KendoSelectUserEntity GetStartWith(string startWith);

        KendoSelectUserEntity GetJFStartWith(string startWith, string managerType = "1");

        KendoSelectUserEntity GetDriversStartWith(string startWith);

        UserInfoEntity GetUserById(int userId, int accountType);

        void SaveUserRole(int userId, int accountType, int[] roleIds);

        List<UserSimpleForFrontend> GetUserSimpleByRoleId(int roleId);
       
    }
}
