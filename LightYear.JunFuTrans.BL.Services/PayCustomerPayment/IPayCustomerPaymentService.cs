﻿using LightYear.JunFuTrans.BL.BE.PayCustomerPayment;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.PayCustomerPayment
{
    public interface IPayCustomerPaymentService
    {
        byte[] Export();

        string GetUniqueFileName(string fileName);

        void InsertBatch(List<DA.JunFuDb.PayCustomerPayment> tempEntities);

        List<FrontendEntity> GetAll(DateTime start, DateTime end, string station = null, string customer = null);
    }
}
