﻿using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DriverToStationPayment
{
    public interface IStationCollectMoneyService
    {
        IEnumerable<StationCollectMoneyEntity> GetStationCollectMoneyByTimeAndStation(DateTime date, string stationScode);

        int UpdatePaymentAndPaymentBalance(StationCollectMoneyEntity entity);
    }
}
