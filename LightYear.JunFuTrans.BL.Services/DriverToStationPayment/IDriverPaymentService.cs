﻿using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.DriverToStationPayment
{
    public interface IDriverPaymentService
    {
        ViewCreatedSheetEntity GetDriverTodayCreatedSheet(string driverCode);

        IEnumerable<DriverPaymentDetailEntity> GetDriverTodayCreatedDetails(string driverCode);

        IEnumerable<DriverPaymentDetailEntity> GetPaymentDetialEntityExceptCreated(string driverCode);        

        //List<string> GetDriverDeliverySuccessCheckNumbers(string driverCode);

        DriverPaymentDetailEntity[] GetDetailEntityByChekNum(List<string> checkNumList);

        CategoryCountEntity CalculateCategoryCount(DriverPaymentDetailEntity[] detailEntity);

        DriverPaymentDetailEntity[] RemoveNoMoneyList(DriverPaymentDetailEntity[] detailEntity);

        int SaveDriverPayment(DriverPaymentEntity entity);

        DriverPaymentEntity GetPaymentSheetById(int paymentId);

        IEnumerable<PaymentSheetPrintFormatEntity> GetPaymentSheetByDriverAndDate(string driverCode, DateTime date);

        //string GetSubpoenaCategoryShowName(string subpoeanaCategory);

        List<DriverPaymentEntity> GetListByStationAndTime(DateTime start, DateTime end, string station);

        //List<DriverPaymentEntity> GetAll();

        List<string> GetAllStationName();

        List<CashFlowDailyEntity> GetCashFlowDailyEntityListByStationAndTime(DateTime start, DateTime end, string stationSCode);
    }
}
