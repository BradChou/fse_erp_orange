﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.BL.BE.TodayPickUp;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.TodayPickUp;

namespace LightYear.JunFuTrans.BL.Services.TodayPickUp
{
    public class TodayPickUpService : ITodayPickUpService
    {
        public TodayPickUpService(ITodayPickUpRepository todayPickUpRepository, IMapper mapper)
        {
            this.TodayPickUpRepository = todayPickUpRepository;

            this.Mapper = mapper;
        }

        public ITodayPickUpRepository TodayPickUpRepository { get; set; }
        public IMapper Mapper { get; private set; }
        public List<TodayPickUpEntity> TodayPickUpEntities(TodayPickUpInputEntity todayPickUpInputEntity)
        {
            int counter = 0;            //計數器

            //List<TtDeliveryScanLog> todayPickUpRepositoryEntities = TodayPickUpRepository.TodayPickUpRepositoryEntities(todayPickUpInputEntity);
            List<TtDeliveryScanLog> todayPickUpRepositoryEntities = TodayPickUpRepository.TodayPickUpRepositoryEntities2(todayPickUpInputEntity);
            //var todayPickUpEntities = Mapper.Map<List<TodayPickUpEntity>>(todayPickUpRepositoryEntities);
            List<TodayPickUpEntity> todayPickUpEntities = new List<TodayPickUpEntity>();
            foreach (var i in todayPickUpRepositoryEntities)
            {
                TodayPickUpEntity test = new TodayPickUpEntity()
                {
                    CheckNumber = i.CheckNumber,
                    Pieces = 1,
                    ScanItem = i.ScanItem,
                    ScanDate = i.ScanDate,
                    ReceiveStatus = i.ReceiveOption
                };
                todayPickUpEntities.Add(test);
            }                                                                   //****以上這段須修正成綠色那段

            foreach (var i in todayPickUpEntities)
            {
                counter += 1;
                i.Index = counter;
                string[] date = i.ScanDate.ToString().Split(' ');
                i.FrontendShipDate = date[0] + " " + date[2];

                if (i.ScanItem == "5" && i.ReceiveStatus == "20")                                         //*****為啥有空白鍵啊?
                {
                    i.PickUpStatus = "已集貨";
                }
                else
                {
                    i.PickUpStatus = "集貨失敗";
                }
            }

            return todayPickUpEntities;
        }

        public TodayPickUpCount TodayPickUpCount(TodayPickUpInputEntity todayPickUpInputEntity)
        {
            int counter = 0;            //計數器
            int SuccessCounter = 0;     //計數器
            int FailCounter = 0;     //計數器

            TodayPickUpCount TodayPickUpCount = new TodayPickUpCount();

            //List<TtDeliveryScanLog> todayPickUpRepositoryEntities = TodayPickUpRepository.TodayPickUpRepositoryEntities(todayPickUpInputEntity);
            List<TtDeliveryScanLog> todayPickUpRepositoryEntities = TodayPickUpRepository.TodayPickUpRepositoryEntities2(todayPickUpInputEntity);
            //var todayPickUpEntities = Mapper.Map<List<TodayPickUpEntity>>(todayPickUpRepositoryEntities);
            List<TodayPickUpEntity> todayPickUpEntities = new List<TodayPickUpEntity>();
            foreach (var i in todayPickUpRepositoryEntities)
            {
                TodayPickUpEntity test = new TodayPickUpEntity()
                {
                    CheckNumber = i.CheckNumber,
                    Pieces = 1,
                    ScanItem = i.ScanItem,
                    ScanDate = i.ScanDate,
                    ReceiveStatus = i.ReceiveOption
                };
                todayPickUpEntities.Add(test);
            }                                                                   //****以上這段須修正成綠色那段

            foreach (var i in todayPickUpEntities)
            {
                counter += 1;
                i.Index = counter;
                string[] date = i.ScanDate.ToString().Split(' ');
                i.FrontendShipDate = date[0] + " " + date[2];

                if (i.ScanItem == "5" && i.ReceiveStatus == "20")                                         //*****為啥有空白鍵啊?
                {
                    i.PickUpStatus = "已集貨";
                    SuccessCounter += 1;
                }
                else
                {
                    i.PickUpStatus = "集貨失敗";
                    FailCounter += 1;
                }
            }
            TodayPickUpCount.SuccessCount = SuccessCounter.ToString();
            TodayPickUpCount.FailCount = FailCounter.ToString();
            
            return TodayPickUpCount;
        }

        public string GetDriverNameByDriverCode(string driverCode)
        {
            string result = TodayPickUpRepository.GetDriverNameByDriverCode(driverCode);
            return result;
        }
        public string GetDriverStationByScode(string scode)
        {
            string result = TodayPickUpRepository.GetDriverStationByScode(scode);
            return result;
        }
    }
}
