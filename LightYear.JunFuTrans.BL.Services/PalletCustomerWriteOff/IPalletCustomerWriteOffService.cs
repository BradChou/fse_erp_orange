﻿using LightYear.JunFuTrans.BL.BE.PalletCustomerWriteOff;
using LightYear.JunFuTrans.DA.JunFuDb;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.PalletCustomerWriteOff
{
    public interface IPalletCustomerWriteOffService
    {
        List<PalletCustomerWriteOffEntity> GetNotWriteOffCustomerMonthDetail(DateTime startDate, DateTime date, string shortCustomerCode = null);
        List<PalletCustomerDropDownList> GetTbSuppliersAndCheckoutDate();
        List<ShowInFrontend> GetTotalPaymentAndPlatesByOneSupplier(DateTime checkoutDate);
        List<PalletCustomerWriteOffEntity> GetAlreadyWriteOffMonthCustomer(DateTime startDate, DateTime endDate, string shortCustomerCode = null);
        List<Detail> GetMonthCustomerAlreadyWriteOffDetail(DateTime startDate, DateTime endDate, string shortCustomerCode = null);
        List<FindCustomerEntity> FindCustomerEntities(string shortCustomerCode);
        List<PalletCustomerRequestPaymentLog> GetHistoryByCustomerCodeAndMonths(DateTime startMonth, DateTime endMonth, string shortCustomerCode);
        void UpdateClosingDate(List<string> checknumbers, DateTime closingDate);
        void InsertPaymentLog(PalletCustomerWriteOffEntity palletCustomerRequestPaymentLog, DateTime closingDate, string shortCustomerCode);
        DateTime ForManualGetLastMonthClosingDateOrLastManualClosingDate(DateTime closingDate, string shortCustomerCode);
        decimal[] GetLineChart(string customerCode, int year);
        byte[] Export(DateTime start, DateTime end, List<PalletCustomerWriteOffEntity> mainData);
        Stream GenerateZipFile(List<InputJson> inputJsons);
        byte[] ExportAlreadyWriteOffExcel(DateTime start, DateTime end, string shortCustomerCode);
        byte[] ExportNotWriteOffExcel(DateTime start, DateTime end, string shortCustomerCode);
        DateTime ForManualGetLastClosingDate(DateTime closingDate, string shortCustomerCode);
    }
}
