﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.CbmDataLog;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.Services.CbmDataLog
{
    public interface ICbmDataLogService
    {
        bool InsertIntoCbmDataLog(string date, int station);
        List<CbmDataEntity> GetData(CbmDataLogInputEntity input);
        List<CbmDataEntity> GetDataForCheckNumber(string CheckNumber);
        void UpdateCbmDataLog(CbmUpdateDataEntity entities);
    }
}
