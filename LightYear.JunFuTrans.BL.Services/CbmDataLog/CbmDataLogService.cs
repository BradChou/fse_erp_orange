﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Linq;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.CbmDataLog;
using LightYear.JunFuTrans.BL.BE.CbmDataLog;




namespace LightYear.JunFuTrans.BL.Services.CbmDataLog
{
    public class CbmDataLogService : ICbmDataLogService
    {
        IConfiguration Configuration;
        ICbmDataLogRepository CbmDataLogRepository;

        public CbmDataLogService(IConfiguration configuration, ICbmDataLogRepository cbmDataLogRepository)
        {
            Configuration = configuration;
            CbmDataLogRepository = cbmDataLogRepository;
        }
        public bool InsertIntoCbmDataLog(string date, int station)
        {
            DateTime dt = new DateTime(int.Parse(date.Substring(0, 4)), int.Parse(date.Substring(4, 2)), int.Parse(date.Substring(6, 2)), int.Parse(date.Substring(8, 2)), int.Parse(date.Substring(10, 2)), int.Parse(date.Substring(12, 2)));
            string filePath = "";
            string picPath = "";
            string driverCode = "";//特殊情況需要掃發送所使用的driver_code
            bool isSuccess = true;
            if (station == 29)
            {
                filePath = Configuration.GetValue<string>("S3CbmPhotoDaYuanLog");
                picPath = Configuration.GetValue<string>("S3CbmPhotoDaYuanPic");
                driverCode = Configuration.GetValue<string>("DaYuanScanItemSevenDriverCode");
            }
            else
            {
                filePath = Configuration.GetValue<string>("S3CbmPhotoTaiChungLog");
                picPath = Configuration.GetValue<string>("S3CbmPhotoTaiChungPic");
                driverCode = Configuration.GetValue<string>("TaiChungScanItemSevenDriverCode");
            }
            DirectoryInfo dirs = new DirectoryInfo(filePath);
            IEnumerable<FileInfo> files = dirs.GetFiles("*.csv").Where(x => x.CreationTime < dt && x.CreationTime > dt.AddDays(-1)).OrderBy(x => x.CreationTime);
            foreach (var f in files)
            {
                if (!CbmDataLogRepository.IsAlreadyCompletedScheduleLogByFileName(f.Name, station.ToString()))
                {
                    CbmDataLogRecord logData = new CbmDataLogRecord()
                    {
                        FileName = f.Name,
                        Cdate = DateTime.Now,
                        DataSource = station.ToString()
                    };
                    CbmDataLogRepository.LogSchedule(logData);

                    string[] contents = File.ReadAllLines(f.FullName);
                    List<DA.JunFuDb.CbmDataLog> dataList = new List<DA.JunFuDb.CbmDataLog>();
                    DA.JunFuDb.CbmDataLog lastAbnormalEntity = new DA.JunFuDb.CbmDataLog();

                    List<TcDeliveryRequest> deliveryRequests = new List<TcDeliveryRequest>();

                    for (var r = 0; r < contents.Length; r++)
                    {
                        string[] cells = contents[r].Split(@",");
                        decimal length;
                        decimal width;
                        decimal height;
                        decimal cbm;
                        DateTime scanTime;

                        DA.JunFuDb.CbmDataLog entity = new DA.JunFuDb.CbmDataLog()
                        {
                            FileName = f.Name,
                            FileRow = r + 1,
                            Cdate = DateTime.Now,
                            CheckNumber = cells[0],
                            Length = decimal.TryParse(cells[2], out length) ? double.Parse(cells[2]) : default(double?),
                            Width = decimal.TryParse(cells[3], out width) ? double.Parse(cells[3]) : default(double?),
                            Height = decimal.TryParse(cells[4], out height) ? double.Parse(cells[4]) : default(double?),
                            Cbm = decimal.TryParse(cells[5], out cbm) ? double.Parse(cells[5]) : default(double?),
                            S3PicUri = (DateTime.TryParse(cells[6], out scanTime) && cells[8] != "" ? picPath + DateTime.Parse(cells[6]).ToString("yyyyMMdd") + @"\" + cells[8] : "").Replace("-00.jpg", "-08.jpg"),
                            ScanTime = DateTime.TryParse(cells[6], out scanTime) ? DateTime.Parse(cells[6]) : default(DateTime?),
                            DataSource = station.ToString(),
                            ScanResult = cells[7],
                            IsLog = false
                        };

                        dataList.Add(entity);

                        if (cells[0] == "NOREAD")
                        {
                            lastAbnormalEntity.CheckNumber = entity.CheckNumber;
                            lastAbnormalEntity.Length = entity.Length;
                            lastAbnormalEntity.Width = entity.Width;
                            lastAbnormalEntity.Height = entity.Height;
                            lastAbnormalEntity.Cbm = entity.Cbm;
                            lastAbnormalEntity.S3PicUri = entity.S3PicUri;
                        }

                        if (entity.ScanResult == "Hand")
                        {
                            TtDeliveryScanLog scanLog = new TtDeliveryScanLog()     //特殊資料補掃發送
                            {
                                DriverCode = driverCode,
                                CheckNumber = entity.CheckNumber,
                                ScanItem = "7",
                                ScanDate = entity.ScanTime
                            };
                            CbmDataLogRepository.CbmSpecialDataInsertIntoScanLog(scanLog);
                        }

                        if (entity.Length == null && lastAbnormalEntity.CheckNumber == "NOREAD") //上一筆是NOREAD，則此筆寫入上一筆資訊
                        {
                            DA.JunFuDb.CbmDataLog noreadEntity = new DA.JunFuDb.CbmDataLog()
                            {
                                CheckNumber = cells[0],
                                Length = lastAbnormalEntity.Length,
                                Width = lastAbnormalEntity.Width,
                                Height = lastAbnormalEntity.Height,
                                Cbm = lastAbnormalEntity.Cbm,
                                FileName = entity.FileName,
                                FileRow = entity.FileRow,
                                Cdate = entity.Cdate.GetValueOrDefault(),
                                S3PicUri = lastAbnormalEntity.S3PicUri,
                                ScanTime = entity.ScanTime,
                                DataSource = entity.DataSource,
                                ScanResult = entity.ScanResult,
                                IsLog = false
                            };
                            dataList[dataList.Count - 1].IsLog = true;  //覆蓋上兩筆(一筆noread，一筆無丈量資料)
                            dataList[dataList.Count - 2].IsLog = true;
                            dataList.Add(noreadEntity);

                            lastAbnormalEntity.CheckNumber = null;
                            lastAbnormalEntity.Length = null;
                            lastAbnormalEntity.Width = null;
                            lastAbnormalEntity.Height = null;
                            lastAbnormalEntity.Cbm = null;
                            lastAbnormalEntity.FileName = null;
                            lastAbnormalEntity.FileRow = null;
                            lastAbnormalEntity.Cdate = null;
                            lastAbnormalEntity.S3PicUri = null;
                            lastAbnormalEntity.ScanTime = null;
                            lastAbnormalEntity.DataSource = null;
                            lastAbnormalEntity.ScanResult = null;

                            decimal requestId = CbmDataLogRepository.FindMaxDeliveryRequestIdByCheckNumber(noreadEntity.CheckNumber);
                            if (requestId > 0)
                            {
                                TcDeliveryRequest request = CbmDataLogRepository.FindDeliveryRequestById(requestId);
                                request.Udate = DateTime.Now;
                                request.CbmLength = noreadEntity.Length;
                                request.CbmHeight = noreadEntity.Width;
                                request.CbmWeight = noreadEntity.Height;
                                request.CbmCont = noreadEntity.Cbm;
                                if (station == 29)
                                {
                                    request.CatchCbmPicPathFromS3 = noreadEntity.S3PicUri;
                                }
                                else if (station == 49)
                                {
                                    request.CatchTaichungCbmPicPathFromS3 = noreadEntity.S3PicUri;
                                }
                                deliveryRequests.Add(request);
                            }

                        }
                        else if (cells[0] != "NOREAD")
                        {
                            lastAbnormalEntity.CheckNumber = null;
                            lastAbnormalEntity.Length = null;
                            lastAbnormalEntity.Width = null;
                            lastAbnormalEntity.Height = null;
                            lastAbnormalEntity.Cbm = null;
                            lastAbnormalEntity.FileName = null;
                            lastAbnormalEntity.FileRow = null;
                            lastAbnormalEntity.Cdate = null;
                            lastAbnormalEntity.S3PicUri = null;
                            lastAbnormalEntity.ScanTime = null;
                            lastAbnormalEntity.DataSource = null;
                            lastAbnormalEntity.ScanResult = null;
                        }
                    }

                    isSuccess = CbmDataLogRepository.InsertIntoCbmDataLogBulky(dataList) == false ? false : isSuccess;
                    CbmDataLogRepository.UpdateDeliveryRequest(deliveryRequests);

                    try
                    {
                        CbmDataLogRecord logDataCompleted = CbmDataLogRepository.FindUncompletedScheduleLogByFileName(f.Name, station.ToString());
                        if (logDataCompleted != null)
                        {
                            logDataCompleted.CompleteDate = DateTime.Now;
                            logDataCompleted.IsSuccess = isSuccess;

                            CbmDataLogRepository.UpdateSchdule(logDataCompleted);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            return isSuccess;
        }

        public List<CbmDataEntity> GetData(CbmDataLogInputEntity input)
        {
            List<CbmDataEntity> data = CbmDataLogRepository.GetAbnormalData(input);

            string s3Url = Configuration.GetValue<string>("S3Url");

             data = data.OrderBy(x => x.ScanTime).ToList();

            foreach (var d in data)
            {
                d.S3PicUri = d.S3PicUri.Length > 0 ? s3Url + d.S3PicUri : "";
                d.S3PicUri2 = d.S3PicUri2.Length > 0 ? s3Url + d.S3PicUri2 : "";

                d.Sum = (double)d.Length + (double)d.Height + (double)d.Width;

                if (d.Sum <= 600)
                {
                    d.Size = "S60";
                }
                else if (d.Sum <= 900)
                {
                    d.Size = "S90";
                }
                else if (d.Sum <= 1200)
                {
                    d.Size = "S120";
                }
                else
                {
                    d.Size = "S150";
                }
            }


            return data;
        }

        public List<CbmDataEntity> GetDataForCheckNumber(string CheckNumber)
        {
            CbmDataEntity CheckNumberScanTime = CbmDataLogRepository.GetDataForCheckNumberScanTime(CheckNumber);

            DateTime ScanTime = DateTime.Parse(CheckNumberScanTime.ScanTime.ToString());

            DateTime StartTime = ScanTime.AddMilliseconds(-60000);

            DateTime EndTime = ScanTime.AddMilliseconds(+60000);

            List<CbmDataEntity> data = CbmDataLogRepository.GetDataForCheckNumber(CheckNumber, StartTime,EndTime);

            string s3Url = Configuration.GetValue<string>("S3Url");

            data = data.OrderBy(x => x.ScanTime).ToList();

            foreach (var d in data)
            {
                d.S3PicUri = d.S3PicUri.Length > 0 ? s3Url + d.S3PicUri : "";
                d.S3PicUri2 = d.S3PicUri2.Length > 0 ? s3Url + d.S3PicUri2 : "";

                d.Sum = (double)d.Length + (double)d.Height + (double)d.Width;

                if (d.Sum <= 600)
                {
                    d.Size = "S60";
                }
                else if (d.Sum <= 900)
                {
                    d.Size = "S90";
                }
                else if (d.Sum <= 1200)
                {
                    d.Size = "S120";
                }
                else
                {
                    d.Size = "S150";
                }
            }


            return data;
        }

        public void UpdateCbmDataLog(CbmUpdateDataEntity entities)
        {
            List<DA.JunFuDb.CbmDataLog> createData = entities.Models;
            List<DA.JunFuDb.CbmDataLog> updateData = new List<DA.JunFuDb.CbmDataLog>();
            List<TcDeliveryRequest> updateDeliveryRequestData = new List<TcDeliveryRequest>();
            string s3Url = Configuration.GetValue<string>("S3Url");

            int count = createData.Count;
            for (var i = 0; i < count; i++)
            {
                //更新僅有圖片的檔案
                DA.JunFuDb.CbmDataLog cbmS3PicUriCbmDataLog = CbmDataLogRepository.GetCbmDataLogById(createData[i].Id);
                cbmS3PicUriCbmDataLog.IsLog = true;
                cbmS3PicUriCbmDataLog.Udate = DateTime.Now;
                updateData.Add(cbmS3PicUriCbmDataLog);


                //更新僅有貨號的檔案
                int id = CbmDataLogRepository.FindCbmDataLogId(createData[i].CheckNumber);
                if (id > 0)
                {
                    DA.JunFuDb.CbmDataLog cbmCheckNumberCbmDataLog = CbmDataLogRepository.GetCbmDataLogById(id);
                    cbmCheckNumberCbmDataLog.IsLog = true;
                    cbmCheckNumberCbmDataLog.Udate = DateTime.Now;
                    updateData.Add(cbmCheckNumberCbmDataLog);
                }

                decimal requestId = CbmDataLogRepository.FindMaxDeliveryRequestIdByCheckNumber(createData[i].CheckNumber);
                if (requestId > 0)
                {
                    TcDeliveryRequest request = CbmDataLogRepository.FindDeliveryRequestById(requestId);
                    request.Udate = DateTime.Now;
                    request.CbmLength = createData[i].Length;
                    request.CbmHeight = createData[i].Width;
                    request.CbmWeight = createData[i].Height;
                    request.CbmCont = createData[i].Cbm;
                    if (createData[i].DataSource == "29")
                    {
                        request.CatchCbmPicPathFromS3 = createData[i].S3PicUri.Replace(s3Url, "");
                    }
                    else if (createData[i].DataSource == "49")
                    {
                        request.CatchTaichungCbmPicPathFromS3 = createData[i].S3PicUri.Replace(s3Url, "");
                    }
                    updateDeliveryRequestData.Add(request);
                }

                createData[i].Id = 0;
                createData[i].Cdate = DateTime.Now;
                createData[i].Udate = DateTime.Now;
                createData[i].ScanTime = createData[i].ScanTime == null ? DateTime.Now : ((DateTime)(createData[i].ScanTime)).AddHours(8);        //時差
                createData[i].S3PicUri = createData[i].S3PicUri.Replace(s3Url, "");
                createData[i].DataSource = "99";
            }

            CbmDataLogRepository.UpdateDeliveryRequest(updateDeliveryRequestData);
            CbmDataLogRepository.UpdateCbmDataLog(updateData);


            //新增合併後的資料
            CbmDataLogRepository.InsertIntoCbmDataLogBulky(createData);
        }
    }
}
