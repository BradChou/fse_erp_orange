﻿using LightYear.JunFuTrans.BL.BE.ContactlessDelivery;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.ContactlessDelivery
{
    public interface IContactlessDeliveryService
    {
        string CheckIfScanOrNot(string checknumber);
        List<ContactlessDeliveryEntity> CheckReceiveContactTel(string checknumber, string tel = "");
        bool InsertContactlessDeliveryCheckNumberIntoScanLog(string checkNumber, string pieces);
        string CheckLatestDeliveryDriverNullOrNot(string checknumber);
    }
}
