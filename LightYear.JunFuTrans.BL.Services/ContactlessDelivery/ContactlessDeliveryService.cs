﻿using LightYear.JunFuTrans.BL.BE.ContactlessDelivery;
using LightYear.JunFuTrans.DA.Repositories.ContactlessDelivery;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace LightYear.JunFuTrans.BL.Services.ContactlessDelivery
{
    public class ContactlessDeliveryService : IContactlessDeliveryService
    {
        public IContactlessDeliveryRepository ContactlessDeliveryRepository { get; set; }

        public ContactlessDeliveryService(IContactlessDeliveryRepository contactlessDeliveryRepository)
        {
            ContactlessDeliveryRepository = contactlessDeliveryRepository;
        }

        public string CheckIfScanOrNot(string checknumber)
        {
            var scanDate = ContactlessDeliveryRepository.CheckIfScanOrNot(checknumber);

            if (scanDate != null)
                return scanDate.Value.ToString("yyyy/MM/dd HH:mm");
            else
                return ""; 
        }

        public string CheckLatestDeliveryDriverNullOrNot(string checknumber)
        {
           return  ContactlessDeliveryRepository.CheckLatestDeliveryDriverNullOrNot(checknumber) ?? "";
        }

        public List<ContactlessDeliveryEntity> CheckReceiveContactTel(string checknumber, string tel = "")
        {
            List<ContactlessDeliveryEntity> data = new List<ContactlessDeliveryEntity>();

            if (string.IsNullOrEmpty(tel))
                data = ContactlessDeliveryRepository.GetReceiveContactDataByChecknumber(checknumber);
            else
                data = ContactlessDeliveryRepository.CheckDeliveryDriverCode(checknumber, tel);

            string Pattern = "."; //比對換行符號外的任意一個字元
            Regex regex = new Regex(Pattern);

            foreach (var a in data)
            {
                var stars = a.ReceiveContact.Substring(1, a.ReceiveContact.Length - 2);
                stars = regex.Replace(stars, "*");
                a.ReceiveContact = a.ReceiveContact.Substring(0, 1) + stars + a.ReceiveContact.Substring(a.ReceiveContact.Length - 1);

                int lastThreeCodeIndex1 = a.ReceiveTel1.Length > 2 ? a.ReceiveTel1.Length - 3 : 0;
                a.ReceiveTel1 = string.IsNullOrEmpty(a.ReceiveTel1) == true ? "" : a.ReceiveTel1.Substring(0, lastThreeCodeIndex1) + "***";

                int lastThreeCodeIndex2 = a.ReceiveTel2.Length > 2 ? a.ReceiveTel2.Length - 3 : 0;
                a.ReceiveTel2 = string.IsNullOrEmpty(a.ReceiveTel2) == true ? "" : a.ReceiveTel2.Substring(0, lastThreeCodeIndex2) + "***";
            }

            return data;
        }

        public bool InsertContactlessDeliveryCheckNumberIntoScanLog(string checkNumber, string pieces)
        {
            var driverCode = ContactlessDeliveryRepository.FindLatestDeliveryDriver(checkNumber);
            var success = ContactlessDeliveryRepository.InsertContactlessDeliveryCheckNumberIntoScanLog(driverCode, checkNumber, int.Parse(pieces));
            return success;
        }

    }
}
