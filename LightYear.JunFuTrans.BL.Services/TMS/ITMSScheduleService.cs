﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.TMS;
using LightYear.JunFuTrans.DA.Repositories.TMS;
using LightYear.JunFuTrans.DA.JunFuDb;

namespace LightYear.JunFuTrans.BL.Services.TMS
{
    public class RelayStop
    {
        public RelayStop(string stopID, string arriveAt, string dispatchAt)
        {
            this.StopID = Int32.Parse(stopID);
            this.ArriveAt = arriveAt;
            this.DispatchAt = dispatchAt;
        }

        public int StopID { get; set; }

        public string ArriveAt { get; set; }

        public string DispatchAt { get; set; }
    }

    public class NewRunForm
    {
        public NewRunForm(string carType, string runType, string status, string attribute, string from, string to, string sundayDispatch, string mondayDispatch, string tuesdayDispatch, string wednesdayDispatch, string thursdayDispatch, string fridayDispatch, string saturdayDispatch, string goStartAt, string goEndAt, string backStartAt, string backEndAt, List<RelayStop> goRelays, List<RelayStop> backRelays, string goRelayCount, string backRelayCount)
        {
            this.CarType = carType;
            this.RunType = runType;
            this.Status = status;
            this.Attribute = attribute;
            this.From = from;
            this.To = to;
            this.SundayDispatch = sundayDispatch;
            this.MondayDispatch = mondayDispatch;
            this.TuesdayDispatch = tuesdayDispatch;
            this.WednesdayDispatch = wednesdayDispatch;
            this.ThursdayDispatch = thursdayDispatch;
            this.FridayDispatch = fridayDispatch;
            this.SaturdayDispatch = saturdayDispatch;
            this.GoStartAt = goStartAt;
            this.GoEndAt = goEndAt;
            this.BackStartAt = backStartAt;
            this.BackEndAt = backEndAt;
            this.GoRelays = goRelays;
            this.BackRelays = backRelays;
            this.GoRelayCount = goRelayCount;
            this.BackRelayCount = backRelayCount;
        }

        public string CarType { get; set; }
        public string RunType { get; set; }
        public string Status { get; set; }
        public string Attribute { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string SundayDispatch { get; set; }
        public string MondayDispatch { get; set; }
        public string TuesdayDispatch { get; set; }
        public string WednesdayDispatch { get; set; }
        public string ThursdayDispatch { get; set; }
        public string FridayDispatch { get; set; }
        public string SaturdayDispatch { get; set; }
        public string GoStartAt { get; set; }
        public string GoEndAt { get; set; }
        public string BackStartAt { get; set; }
        public string BackEndAt { get; set; }
        public List<RelayStop> GoRelays { get; set; }
        public List<RelayStop> BackRelays { get; set; }
        public string GoRelayCount { get; set; }
        public string BackRelayCount { get; set; }

    }

    public interface ITMSScheduleService
    {
        /// <summary>
        /// 轉換班次資料格式
        /// </summary>
        /// <param name="run">班次資料 model</param>
        /// <returns></returns>
        TMSScheduleEntity ConvertToScheduleEntity(BSectionFixedRun run);

        /// <summary>
        /// 取得固定班次表資料
        /// </summary>
        /// <param name="status">篩選資料狀態: "normal"(正常), "stopping"(停運), "all"(全部)</param>
        /// <returns></returns>
        List<TMSScheduleEntity> GetSchedule(OperationStatus operationStatus);

        /// <summary>
        /// 更新班次
        /// </summary>
        /// <param name="run">班次資料</param>
        /// <param name="goRelays">去程停靠點</param>
        /// <param name="backRelays">回程停靠點</param>
        /// <returns></returns>
        BSectionFixedRun UpdateARun(BSectionFixedRun run, List<RelayStop> goRelays, List<RelayStop> backRelays);

        /// <summary>
        /// 建立新班次
        /// </summary>
        /// <param name="run">班次資料</param>
        /// <param name="goRelays">去程停靠點</param>
        /// <param name="backRelays">回程停靠點</param>
        /// <returns></returns>
        BSectionFixedRun CreateNewRun(BSectionFixedRun run, List<RelayStop> goRelays, List<RelayStop> backRelays);

        /// <summary>
        /// 取得運輸車型資料
        /// </summary>
        /// <returns></returns>
        List<CarType> GetCarTypes();

        /// <summary>
        /// 取得運輸流向資料
        /// </summary>
        /// <returns></returns>
        List<RunFlowType> GetRunFlowTypes();

        /// <summary>
        /// 取得B段停靠點資料
        /// </summary>
        /// <returns></returns>
        List<BSectionStop> GetBSectionStops();

        /// <summary>
        /// 依照停靠點 ID 取得區配商資料
        /// </summary>
        /// <returns></returns>
        List<TbSupplier> GetSuppliersByStationID(int id);

        /// <summary>
        /// 取得區配商資料
        /// </summary>
        /// <returns></returns>
        List<TbSupplier> GetSuppliers();

        /// <summary>
        /// 建立B段停靠點
        /// </summary>
        /// <returns></returns>
        BSectionStop CreateNewBSectionStop(BSectionStop newStopData, List<int> supplierIDs);

        /// <summary>
        /// 更新B段停靠點
        /// </summary>
        /// <returns></returns>
        BSectionStop UpdateBSectionStop(BSectionStop newStopData, List<int> supplierIDs);
    }
}
