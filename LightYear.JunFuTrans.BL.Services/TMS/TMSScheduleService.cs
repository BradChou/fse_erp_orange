﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.TMS;
using LightYear.JunFuTrans.DA.Repositories.TMS;
using LightYear.JunFuTrans.DA.JunFuDb;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.TMS
{
    public class TMSScheduleService : ITMSScheduleService
    {

        public IBSectionFixedRunRepository BSectionFixedRunRepository { get; set; }

        public TMSScheduleService(IBSectionFixedRunRepository bSectionFixedRunRepository)
        {
            this.BSectionFixedRunRepository = bSectionFixedRunRepository;
        }

        public TMSScheduleEntity ConvertToScheduleEntity(BSectionFixedRun run)
        {
            var runNumber = run.RunNumber;

            var from = this.BSectionFixedRunRepository.GetStopById(run.FromId);
            var to = this.BSectionFixedRunRepository.GetStopById(run.ToId);
            var route = from.Name + " - " + to.Name;

            var relayList = this.BSectionFixedRunRepository.GetBSectionRelayStopList(run.Id);

            var relay = relayList.Count() > 0 ? "有" : "無";
            var carType = this.BSectionFixedRunRepository.GetCarTypeById(run.CarTypeId);

            var runFlowType = this.BSectionFixedRunRepository.GetRunFlowTypeById(run.RunFlowTypeId);

            var dispatchDays = "";
            dispatchDays += run.DoesSundayDispatch ? "日" : "";
            dispatchDays += run.DoesMondayDispatch ? "一" : "";
            dispatchDays += run.DoesTuesdayDispatch ? "二" : "";
            dispatchDays += run.DoesWednesdayDispatch ? "三" : "";
            dispatchDays += run.DoesThursdayDispatch ? "四" : "";
            dispatchDays += run.DoesFridayDispatch ? "五" : "";
            dispatchDays += run.DoesSaturdayDispatch ? "六" : "";

            dispatchDays = String.Join("、", dispatchDays.Split(""));

            var goTime = run.GoStartAt.ToString("HH:mm") + " - " + run.GoEndAt.ToString("HH:mm");
            var backTime = run.BackStartAt.ToString("HH:mm") + " - " + run.BackEndAt.ToString("HH:mm");
            var status = run.IsActive ? "正常" : "停運";
            var attribute = run.IsLtl ? "棧板與零擔" : "棧板";

            List<TMSScheduleRelayEntity> relays = new List<TMSScheduleRelayEntity>();

            foreach (var relayStop in relayList)
            {
                RunFlowType flow = this.BSectionFixedRunRepository.GetRunFlowTypeById(relayStop.RunFlowTypeId);
                relays.Add(new TMSScheduleRelayEntity
                {
                    Id = relayStop.Id,
                    Name = relayStop.Name,
                    ArriveAt = relayStop.ArriveAt.ToString("HH:mm"),
                    DispatchAt = relayStop.DispatchAt.ToString("HH:mm"),
                    Flow = flow.Name
                });
            }

            TMSScheduleEntity result = new TMSScheduleEntity
            {
                Id = run.Id,
                RunNumber = runNumber,
                Route = route,
                From = from.Name,
                To = to.Name,
                Relay = relay,
                CarType = carType.Name,
                MaxLoading = carType.MaxLoading,
                RunType = runFlowType.Name,
                DispatchDays = dispatchDays,
                GoTime = goTime,
                BackTime = backTime,
                Status = status,
                Attribute = attribute,
                RelayList = relays,
            };

            return result;
        }

        public List<TMSScheduleEntity> GetSchedule(OperationStatus operationStatus)
        {
            List<BSectionFixedRun> data = this.BSectionFixedRunRepository.GetBSectionFixedRunList(operationStatus);

            List<TMSScheduleEntity> result = new List<TMSScheduleEntity>();

            foreach (BSectionFixedRun run in data)
            {
                result.Add(ConvertToScheduleEntity(run));
            }

            return result;
        }

        private void CreateRelaysMapping(int runID, List<RelayStop> relays, int runFlowTypeID)
        {
            foreach (var relay in relays)
            {
                var newRelay = new BSectionMiddleStopMapping
                {
                    StopId = relay.StopID,
                    RunId = runID,
                    ArriveAt = Convert.ToDateTime("2000-01-01 " + relay.ArriveAt),
                    DispatchAt = Convert.ToDateTime("2000-01-01 " + relay.DispatchAt),
                    RunFlowTypeId = runFlowTypeID // 去程
                };

                this.BSectionFixedRunRepository.InsertBSectionMiddleStopMapping(newRelay);
            }
        }

        public BSectionFixedRun UpdateARun(BSectionFixedRun run, List<RelayStop> newGoRelays, List<RelayStop> newBackRelays)
        {
            BSectionFixedRun updatedRun = this.BSectionFixedRunRepository.UpdateBSectionFixedRun(run);

            if (updatedRun == null)
            {
                return null;
            }

            this.BSectionFixedRunRepository.DeleteAllBSectionMiddleStopMappingsByRunID(updatedRun.Id);

            // 去程: RunFlowTypeId = 1
            this.CreateRelaysMapping(updatedRun.Id, newGoRelays, 1);

            // 回程: RunFlowTypeId = 2
            this.CreateRelaysMapping(updatedRun.Id, newBackRelays, 2);

            return updatedRun;
        }
        public BSectionFixedRun CreateNewRun(BSectionFixedRun run, List<RelayStop> goRelays, List<RelayStop> backRelays)
        {
            bool isLtl = run.IsLtl;
            int maxRunNumber = this.BSectionFixedRunRepository.GetMaxNumberByAttribute(isLtl);

            // 產生流水號
            run.RunNumber = maxRunNumber + 1;

            var newRun = this.BSectionFixedRunRepository.InsertBSectionFixedRun(run);

            // 去程: RunFlowTypeId = 1
            this.CreateRelaysMapping(newRun.Id, goRelays, 1);

            // 回程: RunFlowTypeId = 1
            this.CreateRelaysMapping(newRun.Id, backRelays, 2);

            return newRun;
        }

        public List<CarType> GetCarTypes()
        {
            List<CarType> result = this.BSectionFixedRunRepository.GetCarTypes();

            return result;
        }

        public List<RunFlowType> GetRunFlowTypes()
        {
            List<RunFlowType> result = this.BSectionFixedRunRepository.GetRunFlowTypes();

            return result;
        }
        public List<BSectionStop> GetBSectionStops()
        {
            List<BSectionStop> result = this.BSectionFixedRunRepository.GetBSectionStops();

            return result;
        }

        public List<TbSupplier> GetSuppliersByStationID(int id)
        {
            List<TbSupplier> result = this.BSectionFixedRunRepository.GetSuppliersByStationID(id);
            return result;
        }

        public List<TbSupplier> GetSuppliers()
        {
            List<TbSupplier> result = this.BSectionFixedRunRepository.GetSuppliers();

            return result;
        }

        public BSectionStop CreateNewBSectionStop(BSectionStop newStopData, List<int> supplierIDs)
        {
            BSectionStop newStop = this.BSectionFixedRunRepository.CreateNewBSectionStop(newStopData);

            foreach (var supplierID in supplierIDs)
            {
                this.BSectionFixedRunRepository.CreateNewStopSupplierMapping(newStop.Id, supplierID);
            }

            return newStop;
        }

        public BSectionStop UpdateBSectionStop(BSectionStop newStopData, List<int> supplierIDs)
        {
            this.BSectionFixedRunRepository.DeleteAllBSectionStopSupplierMappingByStopId(newStopData.Id);

            BSectionStop updatedStop = this.BSectionFixedRunRepository.UpdateBSectionStop(newStopData);

            foreach (var supplierID in supplierIDs)
            {
                this.BSectionFixedRunRepository.CreateNewStopSupplierMapping(updatedStop.Id, supplierID);
            }

            return updatedStop;
        }
    }
}
