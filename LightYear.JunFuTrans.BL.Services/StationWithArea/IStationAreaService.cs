﻿using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;

namespace LightYear.JunFuTrans.BL.Services.StationWithArea
{
    public interface IStationAreaService
    {
        List<StationAreaEntity> GetAll(string stationSCode, string station_level, string station_area, string management);

        KendoSelectAreaEntity GetAreaList(bool addTotal = false);

        KendoSelectAreaEntity GetAreaListFromTbStation(bool addTotal = false);

        KendoSelectStationEntity GetStationsByAreaName(string areaName, bool addAll = true, string station_level="", string station_area="", string management="",string station_scode="");

        KendoSelectStationEntity GetStationsByAreaNameFromTbStation(string areaName, bool addAll = true);

        KendoSelectStationEntity GetStationsByStationSCode(string stationSCode,string station_level,string station_area,string management,string station_scode);

        KendoSelectStationEntity GetStationsByLevel(string stationLevel, string stationArea, string management, string stationSCode);

        StationAreaEntity GetStationAreaById(int id);

        StationAreaEntity GetByStationScode(string stationScode);

        StationAreaEntity Insert(StationAreaEntity stationAreaEntity);

        void Update(StationAreaEntity stationAreaEntity);

        void Delete(int id);

        KendoSelectStationEntity GetStarWith(string startWith, bool addTotal = false, bool isNeedAllStation = true, string station_level="", string station_area="", string management="",string station_scode="",string station="");

        List<Post5CodeMappingEntity> GetPost5CodeMappingEntityByStationSCode(string stationSCode, string station_level, string station_area, string management,string station_scode);

        bool SavePost5CodeMappingEntity(Post5CodeMappingEntity post5CodeMappingEntity, string station_level, string station_area, string management, string station_scode);

        byte[] Export();

        string GetUniqueFileName(string fileName);

        void InsertBatch(List<DA.JunFuTransDb.OrgArea> tempEntities);

        List<string> GetAllStationName(string station_level, string station_area, string management, string station_scode);


        List<string> GetAllStationScode(string station_level, string station_area, string management, string station_scode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids">路段序號</param>
        /// <param name="dataValue">修改值</param>
        /// <param name="type">1:修改sd, 2:修改md, 3:修改堆罍區</param>
        void BatchChangOrgData(List<int> ids, string dataValue, string type);
    }
}
