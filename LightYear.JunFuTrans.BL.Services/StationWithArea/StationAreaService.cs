﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.DeliveryRate;
using LightYear.JunFuTrans.BL.BE.StationArea;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.Station;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.IO;
using System.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;


namespace LightYear.JunFuTrans.BL.Services.StationWithArea
{
    public class StationAreaService : IStationAreaService
    {
        public IStationAreaRepository StationAreaRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public IOrgAreaRepository OrgAreaRepository { get; set; }

        public IMapper Mapper { get; set; }

        public StationAreaService(IStationAreaRepository stationAreaRepository, IStationRepository stationRepository, IOrgAreaRepository orgAreaRepository, IMapper mapper)
        {
            StationAreaRepository = stationAreaRepository;
            StationRepository = stationRepository;
            OrgAreaRepository = orgAreaRepository;
            Mapper = mapper;
        }

        public List<StationAreaEntity> GetAll(string station_level,string station_area,string management,string station_scode)
        {
            List<TbStation> stationList = StationRepository.GetAll();
            List<StationAreaEntity> stationAreasList = Mapper.Map<List<StationAreaEntity>>(stationList);
            foreach (var s in stationAreasList)
            {
                s.Id = 0;
                var savedData = StationAreaRepository.GetByStationScode(s.StationSCode);
                if (savedData != null)
                {
                    s.Id = savedData.Id;
                    s.Area = savedData.Area;
                }
            }
            return stationAreasList;
        }

        public StationAreaEntity GetStationAreaById(int id)
        {
            var stationArea = StationAreaRepository.GetStationAreaById(id);
            return Mapper.Map<StationAreaEntity>(stationArea);
        }
        public StationAreaEntity GetByStationScode(string stationScode)
        {
            var stationArea = StationAreaRepository.GetByStationScode(stationScode);
            if (stationArea == null)
            {
                return new StationAreaEntity();
            }
            else
            {
                return Mapper.Map<StationAreaEntity>(stationArea);
            }
        }

        public StationAreaEntity Insert(StationAreaEntity stationAreaEntity)
        {
            stationAreaEntity.CreateDate = DateTime.Now;
            stationAreaEntity.UpdateDate = DateTime.Now;

            StationArea stationArea = Mapper.Map<StationArea>(stationAreaEntity);
            int id = StationAreaRepository.Insert(stationArea);
            stationAreaEntity.Id = id;

            return stationAreaEntity;
        }

        public void Update(StationAreaEntity stationAreaEntity)
        {
            if (stationAreaEntity.Id == 0)
            {
                Insert(stationAreaEntity);
                return;
            }
            stationAreaEntity.UpdateDate = DateTime.Now;
            StationArea stationArea = Mapper.Map<StationArea>(stationAreaEntity);
            StationAreaRepository.Update(stationArea);
        }

        public void Delete(int id)
        {
            StationAreaRepository.Delete(id);
        }

        public KendoSelectAreaEntity GetAreaList(bool addTotal = false)
        {
            List<string> areas = StationAreaRepository.GetAreaList();

            if (addTotal)
            {
                areas.Insert(0, "全選");
            }

            return new KendoSelectAreaEntity { results = areas };
        }

        public KendoSelectAreaEntity GetAreaListFromTbStation(bool addTotal = false)
        {
            List<string> areas = StationRepository.GetAreaList();

            if (addTotal)
            {
                areas.Insert(0, "全選");
            }

            return new KendoSelectAreaEntity { results = areas };
        }

        public KendoSelectStationEntity GetStarWith(string startWith, bool addTotal = false, bool isNeedAllStation = true, string station_level = "", string station_area = "", string memanagement = "",string station_scode="",string station="")
        {
            List<TbStation> tbStations = new List<TbStation>();

            //沒資料時抓全部，否則抓開頭資料
            if (startWith == string.Empty  || startWith == "全部")
            {
                if (station_level==null)
                {
                    tbStations = this.StationRepository.GetAll2(station_level, station_area, memanagement, station_scode, station);
                }
                else
                if (station_level.Equals("1"))
                {
                    tbStations = this.StationRepository.GetAll2(station_level, station_area, memanagement, station_scode, station);
                }
                else if (station_level.Equals("2"))
                {
                    tbStations = this.StationRepository.GetAll2(station_level, station_area, memanagement, station_scode, station);
                }
                else if (station_level.Equals("4"))
                {
                    tbStations = this.StationRepository.GetAll2(station_level, station_area, memanagement, station_scode, station);
                }
                else if (station_level.Equals("5"))
                {
                    //tbStations = this.StationRepository.GetAll2(station_level, station_area, memanagement, station_scode);
                    tbStations = this.StationRepository.GetAll();
                }
                else
                {

                    tbStations = this.StationRepository.GetAll();
                }
            }
            else
            {
                tbStations = this.StationRepository.GetStartWith(startWith);
            }

            var stationEntities = this.Mapper.Map<List<StationAreaEntity>>(tbStations);

            if (addTotal)
            {
                /*
                StationAreaEntity emptyStationAreaEntity = new StationAreaEntity
                {
                    StationCode = string.Empty,
                    StationName = string.Empty,
                    ShowName = "未選擇"
                };*/

                StationAreaEntity stationAreaEntity = new StationAreaEntity
                {
                    Id=-1,
                    StationCode = "-1",
                    StationSCode = "-1",
                    StationName = string.Empty,
                    ShowName = "全部站所"
                };

                //stationEntities.Insert(0, emptyStationAreaEntity);

                //if (isNeedAllStation)
                //{
                    stationEntities.Insert(0, stationAreaEntity);
                //}
            }

            KendoSelectStationEntity kendoSelectStationEntity = new KendoSelectStationEntity
            {
                results = stationEntities,
                __count = stationEntities.Count
            };

            return kendoSelectStationEntity;
        }

        public KendoSelectStationEntity GetStationsByAreaName(string areaName, bool addAll = true, string station_level = "", string station_area = "", string management = "",string station_scode="")
        {
            List<StationArea> stationAreas = StationAreaRepository.GetStationsByAreaName(areaName);
            KendoSelectStationEntity kendoSelectStationEntity = new KendoSelectStationEntity();
            kendoSelectStationEntity.results = Mapper.Map<List<StationAreaEntity>>(stationAreas);

            if (addAll)
                kendoSelectStationEntity.results.Insert(0, new StationAreaEntity
                {
                    ShowName = "區域內全選",
                    StationCode = "0",
                    StationSCode = "0"
                });

            return kendoSelectStationEntity;
        }

        public KendoSelectStationEntity GetStationsByAreaNameFromTbStation(string areaName, bool addAll = true)
        {
            List<TbStation> stationAreas = StationRepository.GetStationByArea(areaName);
            KendoSelectStationEntity kendoSelectStationEntity = new KendoSelectStationEntity();
            kendoSelectStationEntity.results = Mapper.Map<List<StationAreaEntity>>(stationAreas);

            if (addAll)
                kendoSelectStationEntity.results.Insert(0, new StationAreaEntity
                {
                    ShowName = "區域內全選",
                    StationCode = "0",
                    StationSCode = "0"
                });

            return kendoSelectStationEntity;
        }

        public List<Post5CodeMappingEntity> GetPost5CodeMappingEntityByStationSCode(string stationSCode,string station_level,string station_area, string managment, string station_scode)
        {
            var data = this.OrgAreaRepository.GetByStationSCode(stationSCode,station_level,managment,station_area,station_scode);
            List<Post5CodeMappingEntity> post5CodeMappingEntities = Mapper.Map<List<Post5CodeMappingEntity>>(data);

            return post5CodeMappingEntities;
        }

        public bool SavePost5CodeMappingEntity(Post5CodeMappingEntity post5CodeMappingEntity, string station_level, string station_area, string management, string station_scode)
        {
            OrgArea orgArea = Mapper.Map<OrgArea>(post5CodeMappingEntity);

            if (!GetAllStationName(station_level, station_area, management, station_scode).Contains(orgArea.StationName) || !GetAllStationScode(station_level, station_area, management, station_scode).Contains(orgArea.StationScode))
            {
            }
            //    return false;


            //}
            //else
            //{
                if (orgArea.Id == 0)
                {

                    this.OrgAreaRepository.Insert(orgArea);
                }
                else
                {
                    this.OrgAreaRepository.Update(orgArea);
                }

                return true;
            //}

        }

        public KendoSelectStationEntity GetStationsByStationSCode(string stationSCode, string station_level, string station_area, string management,string station_scode)
        {
            List<StationAreaEntity> stationAreas = new List<StationAreaEntity>();
            TbStation tbStation = this.StationRepository.GetByScode(stationSCode);

            StationAreaEntity stationAreaEntityFirst = new StationAreaEntity
            {
                StationCode = string.Empty,
                StationName = string.Empty,
                ShowName = "全部"
            };

            stationAreas.Add(stationAreaEntityFirst);

            if (tbStation != null)
            {
                StationAreaEntity stationAreaEntity = new StationAreaEntity
                {
                    StationCode = tbStation.StationCode,
                    StationSCode = tbStation.StationScode,
                    StationName = tbStation.StationName,
                    ShowName = tbStation.StationName
                };

                stationAreas.Add(stationAreaEntity);
            }

            KendoSelectStationEntity kendoSelectStationEntity = new KendoSelectStationEntity();
            kendoSelectStationEntity.results = stationAreas;


            return kendoSelectStationEntity;
        }

        public KendoSelectStationEntity GetStationsByLevel(string stationLevel, string stationArea, string management, string stationSCode)
        {

            List<TbStation> tbStations = new List<TbStation>();


            tbStations = this.StationRepository.GetByLevel(stationLevel, stationArea, management, stationSCode);

            var stationEntities = this.Mapper.Map<List<StationAreaEntity>>(tbStations);

            StationAreaEntity emptyStationAreaEntity = new StationAreaEntity
            {
                StationCode = string.Empty,
                StationName = string.Empty,
                ShowName = "未選擇"
            };

            StationAreaEntity stationAreaEntity = new StationAreaEntity
            {
                StationCode = "-1",
                StationSCode = "-1",
                StationName = string.Empty,
                ShowName = "全部站所"
            };

            stationEntities.Insert(0, emptyStationAreaEntity);
            stationEntities.Insert(1, stationAreaEntity);

            if (stationLevel == "5") { stationEntities.Insert(1, stationAreaEntity); }

            KendoSelectStationEntity kendoSelectStationEntity = new KendoSelectStationEntity
            {
                results = stationEntities,
                __count = stationEntities.Count
            };

            return kendoSelectStationEntity;

        }



        public byte[] Export()
        {

            var memoryStream = new MemoryStream();
            using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {

                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());



                var sheets = workbookPart.Workbook.AppendChild(new Sheets());
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "工作表1"


                });




                // 從 Worksheet 取得要編輯的 SheetData
                var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


                // 建立資料列物件
                var row1 = new Row();
                // 在資料列中插入欄位
                row1.Append(
                    new Cell() { CellValue = new CellValue("郵政投遞區"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("郵遞區號3碼(必填)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("郵遞區號5碼(必填)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("縣市(必填)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("鄉鎮市區(必填)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("路段(必填)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("配送路號(必填)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("站所代碼(必填)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("站所名稱(必填)"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("配區SD"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("配區MD"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("堆疉區"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("接駁碼"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("集區SD"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("集區MD"), DataType = CellValues.String }


                );


                //var row = new Row();
                //row.Append(
                //    new Cell() { CellValue = new CellValue("台北郵局松山投遞股-21區"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("105"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("10561"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("臺北市"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("松山區"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("吉祥路"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("全"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("14"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("內湖"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("2"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("23"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("1"), DataType = CellValues.String },
                //    new Cell() { CellValue = new CellValue("範例"), DataType = CellValues.String }
                //);

                // 插入資料列 
                sheetData.AppendChild(row1);
                //sheetData.InsertAfter(row1, row);


                DataValidations dataValidations = new DataValidations();



                workbookPart.Workbook.Save();



                //sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                document.Close();
            }
            return memoryStream.ToArray();
        }



        public string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName) //只有檔名
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName); //.副檔名
        }
        public void InsertBatch(List<DA.JunFuTransDb.OrgArea> tempEntities)
        {
            var city = tempEntities.Select(a => a.City).ToList();
            var area = tempEntities.Select(a => a.Area).ToList();
            var road = tempEntities.Select(a => a.Road).ToList();
            var scoop = tempEntities.Select(a => a.Scoop).ToList();


            var DataToUpdate = OrgAreaRepository.IsInsertDataOrNot(city, area, road, scoop);

            if (DataToUpdate == null)
            {
                //OrgAreaRepository.InsertBatch(tempEntities); 
            }
            else
            {
                var needToUpdateDatas = DataToUpdate.Select(a => a.Road).ToList();
                var needToInsert = tempEntities.Where(a => !needToUpdateDatas.Contains(a.Road)).ToList();
                OrgAreaRepository.InsertBatch(needToInsert);

                var needToUpdate = tempEntities.Where(a => needToUpdateDatas.Contains(a.Road)).ToList();

                foreach (var n in DataToUpdate)
                {
                    n.Office = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.Office).FirstOrDefault();
                    n.Zip3A = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.Zip3A).FirstOrDefault();
                    n.Zipcode = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.Zipcode).FirstOrDefault();
                    n.City = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.City).FirstOrDefault();
                    n.Area = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.Area).FirstOrDefault();
                    n.Road = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.Road).FirstOrDefault();
                    n.Scoop = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.Scoop).FirstOrDefault();
                    n.StationScode = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.StationScode).FirstOrDefault();
                    n.StationName = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.StationName).FirstOrDefault();
                    n.MdNo = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.MdNo).FirstOrDefault();
                    n.SdNo = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.SdNo).FirstOrDefault();
                    n.PutOrder = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.PutOrder).FirstOrDefault();
                    n.ShuttleStationCode = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.ShuttleStationCode).FirstOrDefault();
                    n.SendSD = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.SendSD).FirstOrDefault();
                    n.SendMD = needToUpdate.Where(a => a.City.Equals(n.City) && a.Area.Equals(n.Area) && a.Road.Equals(n.Road) && a.Scoop.Equals(n.Scoop)).Select(a => a.SendMD).FirstOrDefault();


                    OrgAreaRepository.Update(n);
                }
            }
        }

        public List<string> GetAllStationName(string station_level, string station_area, string management, string station_scode)
        {
            List<TbStation> stationList = StationRepository.GetAll();
            List<string> stationNames = new List<string>();

            foreach (var s in stationList)
            {
                stationNames.Add(s.StationName);
            }
            return stationNames;
        }

        public List<string> GetAllStationScode(string station_level, string station_area, string management, string station_scode)
        {
            List<TbStation> stationList = StationRepository.GetAll();
            List<string> stationScode = new List<string>();

            foreach (var s in stationList)
            {
                stationScode.Add(s.StationScode);
            }
            return stationScode;
        }
        public void BatchChangOrgData(List<int> ids, string dataValue, string type)
        {
            switch (type)
            {
                case "1":
                    this.OrgAreaRepository.BatchUpdateSD(ids, dataValue);
                    break;
                case "2":
                    this.OrgAreaRepository.BatchUpdateMD(ids, dataValue);
                    break;
                case "3":
                    this.OrgAreaRepository.BatchUpdatePutOrder(ids, dataValue);
                    break;
                case "4":
                    this.OrgAreaRepository.BatchUpdateShuttleStationCode(ids, dataValue);
                    break;
                case "5":
                    this.OrgAreaRepository.BatchUpdateSendSD(ids, dataValue);
                    break;
                case "6":
                    this.OrgAreaRepository.BatchUpdateSendMD(ids, dataValue);
                    break;
            }
        }
    }
}
