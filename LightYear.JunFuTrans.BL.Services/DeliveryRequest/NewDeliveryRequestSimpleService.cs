﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.BusinessReport;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.Statics;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.Services.Barcode;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.PickUpRequest;
using LightYear.JunFuTrans.DA.Repositories.DeliveryException;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.Utilities.Reports;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using LightYear.JunFuTrans.DA.Repositories.CbmSize;
using Microsoft.Extensions.Configuration;
using JunFuTrans.DA.JunFuTrans.DA;
using JunFuTrans.DA.JunFuTrans.condition;
using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;
using Common;
using Common.Model;

namespace LightYear.JunFuTrans.BL.Services.DeliveryRequest
{
    public class NewDeliveryRequestSimpleService : INewDeliveryRequestSimpleService
    {

        public IDeliveryRequestModifyRepository DeliveryRequestModifyRepository { get; set; }

        public IPickUpRequestRepository PickUpRequestRepository { get; set; }

        public ICheckNumberSDMappingRepository CheckNumberSDMappingRepository { get; set; }

        public IOrgAreaRepository OrgAreaRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public IDeliveryScanLogRepository DeliveryScanLogRepository { get; set; }

        public IPickupRequestForApiuserRepository PickupRequestForApiuserRepository { get; set; }

        public ICheckNumberPreheadRepository CheckNumberPreheadRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public ICbmSizeRepository CbmSizeRepository { get; set; }

        public NewDeliveryRequestSimpleService(
            IDeliveryRequestModifyRepository deliveryRequestModifyRepository,
            IPickUpRequestRepository pickUpRequestRepository,
            ICheckNumberSDMappingRepository checkNumberSDMappingRepository,
            IOrgAreaRepository orgAreaRepository,
            IStationRepository stationRepository,
            ICustomerRepository customerRepository,
            IDriverRepository driverRepository,
            IDeliveryScanLogRepository deliveryScanLogRepository,
            IPickupRequestForApiuserRepository pickupRequestForApiuserRepository,
            IMapper mapper,
            ICheckNumberPreheadRepository checkNumberPreheadRepository,
            ICbmSizeRepository cbmSizeRepository
            )
        {
            this.DeliveryRequestModifyRepository = deliveryRequestModifyRepository;
            this.PickUpRequestRepository = pickUpRequestRepository;
            this.CheckNumberSDMappingRepository = checkNumberSDMappingRepository;
            this.OrgAreaRepository = orgAreaRepository;
            this.StationRepository = stationRepository;
            this.CustomerRepository = customerRepository;
            this.DriverRepository = driverRepository;
            this.DeliveryScanLogRepository = deliveryScanLogRepository;
            this.PickupRequestForApiuserRepository = pickupRequestForApiuserRepository;
            this.Mapper = mapper;
            CheckNumberPreheadRepository = checkNumberPreheadRepository;
            CbmSizeRepository = cbmSizeRepository;
        }


        static void Log(string action, string message, string customer_code, string check_number, string result)
        {
            string EdiApiDataLogType = "2001";
            EDI_api_new_data_log_DA _EDI_api_new_data_log_DA = new EDI_api_new_data_log_DA();
            _EDI_api_new_data_log_DA.InsertEDI_api_log(new EDI_api_new_data_log_Condition { type = EdiApiDataLogType, action = action, message = message, cdate = DateTime.Now, customer_code = customer_code, check_number = check_number,result =result });

        }

        static void ApiAddressParseLog(DateTime start_time, Exception ex, DeliveryRequestApiEntity deliveryRequestApiEntity, TcDeliveryRequest returnDeliveryRequest, string getJson)
        {
            try
            {
                ApiAddressParseLog_Condition _ApiAddressParseLog = new ApiAddressParseLog_Condition
                {
                    api_type = "4",
                    customer_code = string.IsNullOrEmpty(deliveryRequestApiEntity.CustomerCode) ? "" : deliveryRequestApiEntity.CustomerCode,
                    order_number = string.IsNullOrEmpty(deliveryRequestApiEntity.OrderNumber) ? "" : deliveryRequestApiEntity.OrderNumber,
                    other_param = "",
                    receive_address = string.IsNullOrEmpty(deliveryRequestApiEntity.ReceiverAddress) ? "" : deliveryRequestApiEntity.ReceiverAddress,
                    receive_station = string.IsNullOrEmpty(returnDeliveryRequest.AreaArriveCode) ? "" : returnDeliveryRequest.AreaArriveCode,
                    send_address = string.IsNullOrEmpty(deliveryRequestApiEntity.SenderAddress) ? "" : deliveryRequestApiEntity.SenderAddress,
                    send_station = string.IsNullOrEmpty(returnDeliveryRequest.SendStationScode) ? "" : returnDeliveryRequest.SendStationScode,
                    result_message = "false",
                    source_data = getJson.ToString(),
                    exception_message = ex.Message.ToString(),
                    start_time = start_time,
                    end_time = DateTime.Now
                };
                ApiAddressParseLog_DA _ApiAddressParseLog_DA = new ApiAddressParseLog_DA();
                _ApiAddressParseLog_DA.InsertApiAddressParseLog(_ApiAddressParseLog);
            }
            catch 
            { 
                      
            
            }
        }
        static void ApiAddressParseLog(DateTime start_time, TcDeliveryRequest returnDeliveryRequest, string getJson)
        {

            //ApiAddressParseLog_DA _ApiAddressParseLog_DA = new ApiAddressParseLog_DA();
            //_ApiAddressParseLog_DA.InsertApiAddressParseLog(new ApiAddressParseLog_Condition { api_type = api_type, customer_code = customer_code, log_id = log_id, order_number = order_number, other_param = other_param, receive_address = receive_address, receive_station = receive_station, send_address = send_address, send_station = send_station, result_message = result_message, exception_message = exception_message, source_data = source_data, start_time = start_time, end_time = end_time });

            ApiAddressParseLog_Condition _ApiAddressParseLog = new ApiAddressParseLog_Condition
            {
                api_type = "4",
                customer_code = returnDeliveryRequest.CustomerCode,
                order_number = returnDeliveryRequest.OrderNumber,
                other_param = "",
                receive_address = returnDeliveryRequest.ReceiveCity + returnDeliveryRequest.ReceiveArea + returnDeliveryRequest.ReceiveAddress,
                receive_station = returnDeliveryRequest.AreaArriveCode,
                send_address = returnDeliveryRequest.SendCity + returnDeliveryRequest.SendArea + returnDeliveryRequest.SendAddress,
                send_station = returnDeliveryRequest.SendStationScode,
                result_message = "true",
                exception_message = "",
                source_data = getJson.ToString(),
                start_time = start_time,
                end_time = DateTime.Now,
                check_number = returnDeliveryRequest.CheckNumber
            };
            ApiAddressParseLog_DA _ApiAddressParseLog_DA = new ApiAddressParseLog_DA();
            _ApiAddressParseLog_DA.InsertApiAddressParseLog(_ApiAddressParseLog);

        }

        public List<EDIResultEntity> EDIAPI_New(string token, string getJson, bool addPickUpRequest = true)
        {
            return EDIAPI_New2(token, getJson, addPickUpRequest);
        }

        public List<EDIResultEntity> EDIAPI(string token, string getJson, bool addPickUpRequest = true)
        {
            Log("request", "start", "", "", "");
            Log("token", token, "", "", "");
            Log("getjson", getJson, "", "", "");

            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();

            //先驗證token
            try
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                var payload = JsonConvert.DeserializeObject<PayloadEntity>(json);

                string customerCode = payload.info.account_code;

                //28800是utc
                Int64 nowSpan = Convert.ToInt32((DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds) - 28800 - 600;
                if (payload.exp < nowSpan)
                {
                    throw new Exception("token expire!");
                }

                string stopShippingCode = CustomerRepository.GetByCustomerCode(customerCode).StopShippingCode;
                if (stopShippingCode != "0")
                {
                    throw new Exception("the account code is not active");
                }

                Log("request", "end", "", "", "");
                var insertEDI = EDIAPIJsonInsert(getJson, customerCode, addPickUpRequest);

                eDIResultEntities.AddRange(insertEDI);

            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.Message

                };

                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                var payload = JsonConvert.DeserializeObject<PayloadEntity>(json);
                string customerCode = payload.info.account_code;

                Log("errorMsg", ex.Message.ToString(), customerCode, "", "fasle");
                Log("request", "end", "", "", "");
                eDIResultEntities.Add(eDIResultEntity);

            }

            return eDIResultEntities;

        }

        private List<EDIResultEntity> EDIAPIJsonInsert(string getJson, string customCode = "", bool addPickUpRequest = true)
        {
            Log("respond", "start", "", "", "");
            bool isShopee = customCode == "F1300600002" || customCode == "F2900210002" ? true : false;      //蝦皮與光年甲配都視為蝦皮

            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();

            TcCbmSize[] cbmSizes = CbmSizeRepository.GetAllCbmSize();

            DateTime start_time = DateTime.Now;

            try
            {
                var deliveryRequestApiEntities = JsonConvert.DeserializeObject<List<DeliveryRequestApiEntity>>(getJson);


                foreach (DeliveryRequestApiEntity deliveryRequestApiEntity in deliveryRequestApiEntities)
                {
                    EDIResultEntity eDIResultEntity = new EDIResultEntity();

                    eDIResultEntity.Result = true;

                    TcDeliveryRequest returnDeliveryRequest = new TcDeliveryRequest();

                    //重複訂單編號
                    TcDeliveryRequest CheckReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);

                    try
                    {

                        if (customCode.Length > 0 && deliveryRequestApiEntity.CustomerCode.Length > 0 && deliveryRequestApiEntity.CustomerCode != customCode)
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "客戶代碼不一致";

                            eDIResultEntities.Add(eDIResultEntity);
                            continue;
                        }

                        //TcDeliveryRequest CheckReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);
                        //if (CheckReverseOrderNumberData != null)
                        //{
                        //    throw new Exception("已有相同訂單編號");
                        //}

                        //一開始匯入的正物流checknumber，之後作為逆物流單號寫回return_check_number之用
                        string orignalCheckNumber = deliveryRequestApiEntity.CheckNumber;
                        string orignalCustomerCode = deliveryRequestApiEntity.CustomerCode;
                        //用invoice_dec的正物流訂單編號取回原正物流貨號
                        string orginalShopeeCheckNumber = "";
                        if (isShopee & deliveryRequestApiEntity.Reverse == "Y" & deliveryRequestApiEntity.InvoiceDesc != "")
                        {
                            TcDeliveryRequest ReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.InvoiceDesc, deliveryRequestApiEntity.CustomerCode);

                            if (ReverseOrderNumberData != null)
                            {
                                orginalShopeeCheckNumber = ReverseOrderNumberData.CheckNumber;
                            }

                        }

                        //判斷是否已有逆物流單號
                        //if(orignalCheckNumber.Length != 0 || orignalCheckNumber != null)
                        //{
                        //    var checkNumber = this.DeliveryRequestModifyRepository.GetDataByCheckNumber(orignalCheckNumber, orignalCustomerCode);
                        //    if (checkNumber != null & deliveryRequestApiEntity.Reverse == "Y")
                        //    {
                        //       throw new Exception("此貨號已有逆物流單號");                              
                        //    }
                        //}


                        if (deliveryRequestApiEntity.CollectionMoney > 20000)
                        {
                            throw new Exception("代收貨款不能超過20000元");
                        }

                        //蝦皮逆物流一定要有訂單編號，只有填訂單編號要帶出正物流商品資訊，先從ordernumber抓出checknumber，再從checknumber撈
                        //if (isShopee && deliveryRequestApiEntity.Reverse == "Y")
                        //{

                        //    TcDeliveryRequest ReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);

                        //    if (ReverseOrderNumberData != null)
                        //    {
                        //        if (deliveryRequestApiEntity.OrderNumber != "")
                        //        {
                        //            if (ReverseOrderNumberData.ReturnCheckNumber != "")
                        //            {
                        //                throw new Exception("已經有逆物流編號");
                        //            }
                        //            else if (ReverseOrderNumberData.CheckNumber != "")
                        //            {
                        //                //取得正物流的check_number
                        //                deliveryRequestApiEntity.CheckNumber = ReverseOrderNumberData.CheckNumber;
                        //            }

                        //        }
                        //        else
                        //        {
                        //            throw new Exception("沒有訂單編號");
                        //        }
                        //    }

                        //}
                        //一般件逆物流不一定要有訂單編號，只有填訂單編號要帶出正物流商品資訊，先從ordernumber抓出checknumber，再從checknumber撈
                        //TcDeliveryRequest ReverseOrderNumberDataForReturnCheckNumber = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);

                        //if (!isShopee && deliveryRequestApiEntity.Reverse == "Y")
                        //{

                        //    TcDeliveryRequest ReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);

                        //    if (ReverseOrderNumberData != null)
                        //    {
                        //        if (deliveryRequestApiEntity.OrderNumber != "")
                        //        {
                        //            if (ReverseOrderNumberData.ReturnCheckNumber != "")
                        //            {
                        //                throw new Exception("已經有逆物流編號");
                        //            }
                        //            else if (ReverseOrderNumberData.CheckNumber != "")
                        //            {
                        //                deliveryRequestApiEntity.CheckNumber = ReverseOrderNumberData.CheckNumber;
                        //            }

                        //        }
                        //    }
                        //}

                        //  TcDeliveryRequest ReverseCheckNumberData = new TcDeliveryRequest();

                        //逆物流只有填貨號時需要帶出正物流的商品資訊寫入逆物流

                        var ReverseCheckNumberData = this.DeliveryRequestModifyRepository.GetDataByCheckNumber(deliveryRequestApiEntity.CheckNumber, deliveryRequestApiEntity.CustomerCode);

                        var ReverseSendStationScode = this.DeliveryRequestModifyRepository.GetSendStationScodeByCheckNumber(deliveryRequestApiEntity.CheckNumber, deliveryRequestApiEntity.CustomerCode);



                        //if (deliveryRequestApiEntity.CheckNumber.Length != 0 & deliveryRequestApiEntity.Reverse == "Y")
                        //{
                        //    deliveryRequestApiEntity.SendStationScode = ReverseSendStationScode.StationCode;
                        //    deliveryRequestApiEntity.OrderNumber = ReverseCheckNumberData.OrderNumber;
                        //    deliveryRequestApiEntity.ReceiveCustomerCode = ReverseCheckNumberData.ReceiveCustomerCode;
                        //    deliveryRequestApiEntity.ReceiveContact = ReverseCheckNumberData.ReceiveContact;
                        //    deliveryRequestApiEntity.ReceiveTel1 = ReverseCheckNumberData.ReceiveTel1;
                        //    deliveryRequestApiEntity.ReceiveTel2 = ReverseCheckNumberData.ReceiveTel2;
                        //    deliveryRequestApiEntity.ReceiverAddress = ReverseCheckNumberData.SendCity + ReverseCheckNumberData.SendArea + ReverseCheckNumberData.SendAddress;
                        //    deliveryRequestApiEntity.Plates = Convert.ToInt32(ReverseCheckNumberData.Plates);
                        //    deliveryRequestApiEntity.Weight = Convert.ToInt32(ReverseCheckNumberData.CbmWeight);
                        //    deliveryRequestApiEntity.SubpoenaCategory = ReverseCheckNumberData.SubpoenaCategory;
                        //    deliveryRequestApiEntity.CollectionMoney = Convert.ToInt32(ReverseCheckNumberData.CollectionMoney);
                        //    deliveryRequestApiEntity.SendContact = ReverseCheckNumberData.SendContact;
                        //    deliveryRequestApiEntity.SendTel = ReverseCheckNumberData.SendTel;
                        //    deliveryRequestApiEntity.SenderAddress = ReverseCheckNumberData.ReceiveCity + ReverseCheckNumberData.ReceiveArea + ReverseCheckNumberData.ReceiveAddress;
                        //    deliveryRequestApiEntity.SupplierCode = ReverseCheckNumberData.SupplierCode;
                        //    deliveryRequestApiEntity.PrintDate = ReverseCheckNumberData.PrintDate;
                        //    deliveryRequestApiEntity.CbmSize = ReverseCheckNumberData.CbmSize.ToString();
                        //    deliveryRequestApiEntity.ArriveAssignDate = ReverseCheckNumberData.ArriveAssignDate;
                        //    deliveryRequestApiEntity.TimePeriod = ReverseCheckNumberData.TimePeriod;
                        //    deliveryRequestApiEntity.InvoiceDesc = ReverseCheckNumberData.InvoiceDesc;
                        //    deliveryRequestApiEntity.RoundTrip = ReverseCheckNumberData.RoundTrip.ToString();
                        //    deliveryRequestApiEntity.ArticleNumber = ReverseCheckNumberData.ArticleNumber;
                        //    deliveryRequestApiEntity.SendPlatform = ReverseCheckNumberData.SendPlatform;
                        //    deliveryRequestApiEntity.ArticleName = ReverseCheckNumberData.ArticleName;
                        //    deliveryRequestApiEntity.ReturnCheckNumber = ReverseCheckNumberData.CheckNumber;
                        //}

                        //逆物流要將收、寄件件地址互換;電話也要互換
                        //string reverse_send_address = deliveryRequestApiEntity.ReceiverAddress;
                        //string reverse_receive_address = deliveryRequestApiEntity.SenderAddress;
                        //string reverse_send_tel = deliveryRequestApiEntity.ReceiveTel1;
                        //string reverse_receive_tel = deliveryRequestApiEntity.SendTel;

                        if (deliveryRequestApiEntity.CheckNumber.Length != 0 & deliveryRequestApiEntity.Reverse == "Y")
                        {
                            deliveryRequestApiEntity.ReceiverAddress = ReverseCheckNumberData.SendCity + ReverseCheckNumberData.SendArea + ReverseCheckNumberData.SendAddress;
                            deliveryRequestApiEntity.SenderAddress = ReverseCheckNumberData.ReceiveCity + ReverseCheckNumberData.ReceiveArea + ReverseCheckNumberData.ReceiveAddress;
                            deliveryRequestApiEntity.ReceiveTel1 = ReverseCheckNumberData.SendTel;
                            deliveryRequestApiEntity.SendTel = ReverseCheckNumberData.ReceiveTel1;
                            deliveryRequestApiEntity.ReturnCheckNumber = ReverseCheckNumberData.CheckNumber;
                            deliveryRequestApiEntity.SendStationScode = ReverseSendStationScode.StationCode;
                        }
                        //else if (deliveryRequestApiEntity.CheckNumber.Length == 0 & deliveryRequestApiEntity.Reverse == "Y")
                        //{
                        //    deliveryRequestApiEntity.ReceiverAddress = reverse_send_address;
                        //    deliveryRequestApiEntity.SenderAddress = reverse_receive_address;
                        //    deliveryRequestApiEntity.ReceiveTel1 = reverse_send_tel;
                        //    deliveryRequestApiEntity.SendTel = reverse_receive_tel;
                        //}



                        //先轉為tcDeliveryRequestApiEntity
                        var tcDeliveryRequest = this.Mapper.Map<TcDeliveryRequest>(deliveryRequestApiEntity);

                        var customer = this.CustomerRepository.GetByCustomerCode(tcDeliveryRequest.CustomerCode);

                        eDIResultEntity.OrderNumber = tcDeliveryRequest.OrderNumber;

                        //無代碼代碼時，使用外部傳入的
                        if (deliveryRequestApiEntity.CustomerCode == null || deliveryRequestApiEntity.CustomerCode.Length == 0)
                        {
                            tcDeliveryRequest.CustomerCode = customCode;
                        }

                        //取得收件地址
                        //FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(deliveryRequestApiEntity.ReceiverAddress);
                        var receiverCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.ReceiverAddress);

                        tcDeliveryRequest.ReceiveCity = receiverCity.city;
                        tcDeliveryRequest.ReceiveArea = receiverCity.area;

                        string realReceiveAddress = ChineseConverter.Convert(deliveryRequestApiEntity.ReceiverAddress, ChineseConversionDirection.SimplifiedToTraditional);

                        if (string.IsNullOrEmpty(receiverCity.ToString()) || string.IsNullOrEmpty(receiverCity.city) || string.IsNullOrEmpty(receiverCity.area))
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "無法正確取得收件人的行政區，將無法正確寄送";

                            eDIResultEntities.Add(eDIResultEntity);

                            //記錄開單失敗訊息
                            var exception = eDIResultEntity.Msg;
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = exception,
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now,
                                receiveCity = receiverCity.ToString(),
                                receive_city = receiverCity.city,
                                receive_area = receiverCity.area

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);

                            continue;

                        }

                        if (receiverCity.city.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(receiverCity.city, "");
                        }

                        if (receiverCity.area.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(receiverCity.area, "");
                        }

                        tcDeliveryRequest.ReceiveAddress = realReceiveAddress;

                        //取得寄件地址
                        var sendCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.SenderAddress);

                        tcDeliveryRequest.SendCity = sendCity.city;
                        tcDeliveryRequest.SendArea = sendCity.area;

                        string realSendAddress = deliveryRequestApiEntity.SenderAddress;

                        if (sendCity.city != null && sendCity.city.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(sendCity.city, "");
                        }

                        if (sendCity.area != null && sendCity.area.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(sendCity.area, "");
                        }

                        tcDeliveryRequest.SendAddress = realSendAddress;

                        //在沒有寫入貨號的情況下，加入逆物流的sendstationscode
                        if (deliveryRequestApiEntity.CheckNumber.Length != 0 & deliveryRequestApiEntity.Reverse == "Y")
                        {
                            var ReverseSendStationScodeByAdress = this.DeliveryRequestModifyRepository.GetSendStationScodeByAdress(sendCity.city, sendCity.area);
                            tcDeliveryRequest.SendStationScode = ReverseSendStationScodeByAdress.StationCode;
                        }

                        //判斷取件站所(另外要再寫入取件相關表)
                        FSEAddressEntity fSEAddressEntitySend = new FSEAddressEntity(deliveryRequestApiEntity.SenderAddress);

                        var sendDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntitySend);

                        bool bNotSend = true;

                        if (fSEAddressEntitySend.OrgAddress == "桃園市龜山區航空城JUNFU物流倉")
                        {
                            bNotSend = false;
                        }

                        OrgArea orgAreaSend = new OrgArea();

                        if (sendDatas.Count > 0)
                        {
                            foreach (OrgArea orgArea in sendDatas)
                            {
                                //無站所或聯運區的排除
                                if (orgArea.StationScode != null && orgArea.StationScode != "99" && orgArea.StationScode != "95")
                                {
                                    orgAreaSend = orgArea;
                                    bNotSend = false;
                                    break;
                                }
                            }
                        }

                        if (bNotSend && addPickUpRequest == true && isShopee)
                        {
                            throw new Exception("寄件地址不在服務範圍內！");
                        }

                        //取得托運單
                        var distributor = this.DeliveryRequestModifyRepository.GetDistributor(receiverCity.city, receiverCity.area);

                        if (!isShopee)
                        {
                            tcDeliveryRequest.SupplierCode = customer.SupplierCode;
                        }
                        else if (distributor.supplier_code != "*9聯運")
                        {
                            tcDeliveryRequest.SupplierCode = distributor.supplier_code;
                        }

                        //蝦皮逆物流地址要先轉回北轉站
                        if (deliveryRequestApiEntity.ReceiverAddress == "桃園市楊梅區和平路576號" & deliveryRequestApiEntity.Reverse == "Y")
                        {
                            distributor.area_arrive_code = "29";
                            distributor.supplier_name = "北轉站";
                        }

                        tcDeliveryRequest.AreaArriveCode = distributor.area_arrive_code;

                        eDIResultEntity.Area = distributor.supplier_name;
                        eDIResultEntity.AreaId = distributor.area_arrive_code;

                        if (isShopee)
                        {
                            var station = this.DeliveryRequestModifyRepository.GetDistributor(sendCity.city, sendCity.area);
                            tcDeliveryRequest.SendStationScode = station.area_arrive_code;
                            if (tcDeliveryRequest.CbmSize != null)
                            {
                                //TcCbmSize[] cbmSizes = CbmSizeRepository.GetAllCbmSize();
                                TcCbmSize cbmSize = cbmSizes.Where(s => s.CbmId == tcDeliveryRequest.CbmSize.ToString()).FirstOrDefault();
                                if (cbmSize != null)
                                {
                                    tcDeliveryRequest.CbmLength = cbmSize.DefaultCbmLength;
                                    tcDeliveryRequest.CbmWidth = cbmSize.DefaultCbmWidth;
                                    tcDeliveryRequest.CbmHeight = cbmSize.DefaultCbmHeight;
                                    tcDeliveryRequest.CbmWeight = cbmSize.DefaultCbmWeight;
                                    tcDeliveryRequest.CbmCont = cbmSize.DefaultCbmCont;
                                }
                            }
                        }
                        else if (deliveryRequestApiEntity.Reverse == "N")
                        {
                            tcDeliveryRequest.SendStationScode = customer.SupplierCode.Replace("F", "");
                        }

                        //TcDeliveryRequest returnDeliveryRequest;

                        //如果是蝦皮，先查是否已有重復的訂單
                        if (isShopee && deliveryRequestApiEntity.OrderNumber != null && deliveryRequestApiEntity.OrderNumber.Length > 0)
                        {
                            //先驗證是否有重復訂單
                            var checkDeliveryRequest = this.DeliveryRequestModifyRepository.GetTcDeliveryRequestByCustomerCodeAndOrderNumber(customCode, deliveryRequestApiEntity.OrderNumber);

                            if (checkDeliveryRequest != null && deliveryRequestApiEntity.Reverse != "Y")
                            {
                                //查證資料是否集貨
                                var myScanLog = this.DeliveryScanLogRepository.GetByCheckNumber(checkDeliveryRequest.CheckNumber);

                                if (myScanLog == null || myScanLog.Count == 0)
                                {
                                    returnDeliveryRequest = checkDeliveryRequest;
                                    returnDeliveryRequest.ReceiveCity = tcDeliveryRequest.ReceiveCity;
                                    returnDeliveryRequest.ReceiveArea = tcDeliveryRequest.ReceiveArea;
                                    returnDeliveryRequest.ReceiveAddress = tcDeliveryRequest.ReceiveAddress;
                                    returnDeliveryRequest.SendCity = tcDeliveryRequest.SendCity;
                                    returnDeliveryRequest.SendArea = tcDeliveryRequest.SendArea;
                                    returnDeliveryRequest.SendAddress = tcDeliveryRequest.SendAddress;
                                    returnDeliveryRequest.SupplierCode = tcDeliveryRequest.SupplierCode;
                                    returnDeliveryRequest.AreaArriveCode = tcDeliveryRequest.AreaArriveCode;
                                    returnDeliveryRequest.SendStationScode = tcDeliveryRequest.SendStationScode;
                                    returnDeliveryRequest.CbmSize = tcDeliveryRequest.CbmSize;
                                    returnDeliveryRequest.CbmLength = tcDeliveryRequest.CbmLength;
                                    returnDeliveryRequest.CbmWidth = tcDeliveryRequest.CbmWidth;
                                    returnDeliveryRequest.CbmHeight = tcDeliveryRequest.CbmHeight;
                                    returnDeliveryRequest.CbmWeight = tcDeliveryRequest.CbmWeight;
                                    returnDeliveryRequest.CbmCont = tcDeliveryRequest.CbmCont;
                                }
                                else
                                {
                                    throw new Exception("此訂單編號已存在並取件");
                                }
                            }
                            else
                            {
                                if (CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                                {
                                    returnDeliveryRequest = tcDeliveryRequest;
                                }
                                else 
                                {
                                    returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                                }
                                    
                            }
                        }
                        else if (isShopee)
                        {
                            throw new Exception("無訂單編號");
                        }
                        else
                        {
                            returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                        }

                        //計算託運單費用
                        var shipFeeInfo = this.DeliveryRequestModifyRepository.GetShipFee(Convert.ToInt64(returnDeliveryRequest.RequestId));

                        returnDeliveryRequest.SupplierFee = shipFeeInfo.supplier_fee;
                        returnDeliveryRequest.TotalFee = shipFeeInfo.supplier_fee;

                        // 預購袋扣掉貨號數量
                        if (customer.ProductType == 2 || customer.ProductType == 3)
                        {
                            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
                            webServiceSoapClient.UpdateCheckNumberSettingAsync(customCode, 1);
                        }

                        //逆物流不可建立來回件
                        if (deliveryRequestApiEntity.RoundTrip == "Y" && deliveryRequestApiEntity.Reverse == "Y")
                        {
                            throw new Exception("逆物流貨件不可建立來回件，請修改REQUEST");
                        }

                        //逆物流的DeliveryType要改為"R"
                        if (deliveryRequestApiEntity.Reverse == "Y")
                        {
                            returnDeliveryRequest.DeliveryType = "R";
                        }

                        //重複訂單編號會回傳原來貨號 移到上面
                        //TcDeliveryRequest CheckReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);

                        if (CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                        {
                            returnDeliveryRequest.CheckNumber = CheckReverseOrderNumberData.CheckNumber.ToString();
                            eDIResultEntity.CheckNumber = CheckReverseOrderNumberData.CheckNumber.ToString();
                        }
                        else
                        {
                            if (deliveryRequestApiEntity.CheckNumber.Length == 0 & deliveryRequestApiEntity.Reverse != "Y")
                            {
                                long checkNumberTemp = 100000000 + Convert.ToInt64(returnDeliveryRequest.RequestId);
                                int tail = Convert.ToInt32(returnDeliveryRequest.RequestId % 7);

                                string checkNumberPrefix = "";
                                if (isShopee & deliveryRequestApiEntity.Reverse != "Y")
                                {
                                    checkNumberPrefix = "700";
                                }
                                else
                                {
                                    checkNumberPrefix = CheckNumberPreheadRepository.GetPrefixByProductType(customer.ProductType.ToString());
                                }
                                string currentCheckNumber = string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());

                                //string currentCheckNumber = string.Format("{0}{1}", checkNumberPrefix, ReverseCheckNumber);

                                returnDeliveryRequest.CheckNumber = currentCheckNumber.ToString();

                                eDIResultEntity.CheckNumber = currentCheckNumber;
                            }
                            //逆物流
                            else if (isShopee & deliveryRequestApiEntity.Reverse == "Y")
                            {
                                long checkNumberTemp = 100000000 + Convert.ToInt64(returnDeliveryRequest.RequestId);
                                int tail = Convert.ToInt32(returnDeliveryRequest.RequestId % 7);


                                string checkNumberPrefix = "";

                                checkNumberPrefix = "702";

                                string currentCheckNumber = string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());

                                //string currentCheckNumber = string.Format("{0}{1}", checkNumberPrefix, ReverseCheckNumber);

                                returnDeliveryRequest.CheckNumber = currentCheckNumber.ToString();

                                eDIResultEntity.CheckNumber = currentCheckNumber;

                            }
                            else if (!isShopee & deliveryRequestApiEntity.Reverse == "Y")
                            {
                                long checkNumberTemp = 100000000 + Convert.ToInt64(returnDeliveryRequest.RequestId);
                                int tail = Convert.ToInt32(returnDeliveryRequest.RequestId % 7);


                                string checkNumberPrefix = "";

                                checkNumberPrefix = "990";

                                string currentCheckNumber = string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());

                                //string currentCheckNumber = string.Format("{0}{1}", checkNumberPrefix, ReverseCheckNumber);

                                returnDeliveryRequest.CheckNumber = currentCheckNumber.ToString();

                                eDIResultEntity.CheckNumber = currentCheckNumber;

                            }
                        }

                        //計算sdmd資訊
                        FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(returnDeliveryRequest.ReceiveAddress, returnDeliveryRequest.ReceiveCity, returnDeliveryRequest.ReceiveArea);

                        var eachDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntity);

                        if (eachDatas.Count > 0)
                        {
                            CheckNumberSdMapping checkNumberSdMapping = new CheckNumberSdMapping
                            {
                                CheckNumber = returnDeliveryRequest.CheckNumber,
                                RequestId = (long)returnDeliveryRequest.RequestId,
                                OrgAreaId = eachDatas[0].Id,
                                Md = eachDatas[0].MdNo,
                                Sd = eachDatas[0].SdNo,
                                PutOrder = eachDatas[0].PutOrder,
                                Cdate = DateTime.Now,
                                Udate = DateTime.Now
                            };

                            eDIResultEntity.Md = checkNumberSdMapping.Md;
                            eDIResultEntity.Sd = checkNumberSdMapping.Sd;
                            eDIResultEntity.Putorder = checkNumberSdMapping.PutOrder;
                            //eDIResultEntity.Area = eachDatas[0].StationName;
                            //eDIResultEntity.AreaId = eachDatas[0].StationScode;


                            if (CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                            {
                                
                            }
                            else
                            {
                               this.CheckNumberSDMappingRepository.Insert(checkNumberSdMapping);
                            }
                            
                            //站所資訊
                            //var stationCode = this.StationRepository.GetStationCodeByStationScode(eachDatas[0].StationScode);
                            var stationCode = customer.SupplierCode;
                            returnDeliveryRequest.SupplierCode = stationCode;
                            returnDeliveryRequest.ReceiveZip = eachDatas[0].Zipcode;
                        }

                        //存檔
                        if (CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                        {

                        }
                        else
                        {
                            this.DeliveryRequestModifyRepository.Update(returnDeliveryRequest);
                        }

                        //蝦皮逆物流將逆物流單號寫回正物流
                        if (isShopee & orginalShopeeCheckNumber.Length != 0 && deliveryRequestApiEntity.Reverse == "Y")
                        {
                            tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
                            _tcDeliveryRequests_DA.Update_return_checknumber(orginalShopeeCheckNumber, returnDeliveryRequest.CheckNumber);
                        }
                        //蝦皮逆物流將正物流單號寫入return_checknumber
                        if (isShopee & orginalShopeeCheckNumber.Length != 0 && deliveryRequestApiEntity.Reverse == "Y")
                        {
                            tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
                            _tcDeliveryRequests_DA.Update_return_checknumber(returnDeliveryRequest.CheckNumber, orginalShopeeCheckNumber);
                        }

                        //產出逆物流單號，寫回原本正物流的return_check_number
                        if (!isShopee & orignalCheckNumber.Length != 0 && deliveryRequestApiEntity.Reverse == "Y")
                        {
                            tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
                            _tcDeliveryRequests_DA.Update_return_checknumber(orignalCheckNumber, returnDeliveryRequest.CheckNumber);
                        }

                        //一般件從ordernumber產出逆物流單號，寫回原本正物流的return_check_number
                        //if (!isShopee & orignalCheckNumber.Length == 0 && deliveryRequestApiEntity.Reverse == "Y" && ReverseOrderNumberDataForReturnCheckNumber.CheckNumber.Length != 0)
                        //{
                        //    tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
                        //    _tcDeliveryRequests_DA.Update_return_checknumber(ReverseOrderNumberDataForReturnCheckNumber.CheckNumber, returnDeliveryRequest.CheckNumber);
                        //}



                        Log("save", JsonConvert.SerializeObject(returnDeliveryRequest).ToString(), returnDeliveryRequest.CustomerCode, returnDeliveryRequest.CheckNumber, "true");

                        ApiAddressParseLog(start_time, returnDeliveryRequest, getJson);

                        if (addPickUpRequest)
                        {
                            //產生取貨資料
                            if (isShopee)
                            {
                                PickupRequestForApiuser pickupRequestForApiuser = new PickupRequestForApiuser
                                {
                                    CheckNumber = returnDeliveryRequest.CheckNumber,
                                    CustomerCode = returnDeliveryRequest.CustomerCode,
                                    Md = orgAreaSend.MdNo,
                                    ReassignMd = orgAreaSend.MdNo,
                                    Pieces = (returnDeliveryRequest.Pieces == null || returnDeliveryRequest.Pieces == 0) ? returnDeliveryRequest.Plates : returnDeliveryRequest.Pieces,
                                    Putorder = orgAreaSend.PutOrder,
                                    RequestDate = DateTime.Now,
                                    Sd = orgAreaSend.SdNo,
                                    SendArea = orgAreaSend.Area,
                                    SendCity = orgAreaSend.City,
                                    SendRoad = fSEAddressEntitySend.TransAddress,
                                    SendTel = returnDeliveryRequest.SendTel,
                                    SupplierCode = this.StationRepository.GetStationCodeByStationScode(orgAreaSend.StationScode)
                                };

                                if (CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                                {

                                }
                                else
                                {
                                  this.PickupRequestForApiuserRepository.Insert(pickupRequestForApiuser);
                                }
                            }
                            else
                            {
                                PickUpRequestLog pickUpRequestLog = new PickUpRequestLog();
                                pickUpRequestLog.RequestCustomerCode = deliveryRequestApiEntity.CustomerCode;
                                pickUpRequestLog.PickUpPieces = deliveryRequestApiEntity.Pieces;

                                PickUpRequestRepository.Insert(pickUpRequestLog);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        eDIResultEntity.Result = false;
                        eDIResultEntity.Msg = ex.Message.ToString();

                        //記錄開單失敗訊息
                        ApiAddressParseLog(start_time, ex, deliveryRequestApiEntity, returnDeliveryRequest, getJson);

                    }
                    eDIResultEntities.Add(eDIResultEntity);
                }
            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.Message.ToString(),

                    
                };
                eDIResultEntities.Add(eDIResultEntity);
            }
            Log("respond", JsonConvert.SerializeObject(eDIResultEntities).ToString(), customCode, "", eDIResultEntities[0].Result.ToString());
            Log("respond", "end", "", "", "");
            return eDIResultEntities;

        }

        public List<EDIResultEntity> EDIAPI_New2(string token, string getJson, bool addPickUpRequest = true)
        {
            Log("request", "start","","","");
            Log("token", token, "", "", "");
            Log("getjson", getJson, "", "", "");

            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();

            //先驗證token
            try
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                var payload = JsonConvert.DeserializeObject<PayloadEntity>(json);

                string customerCode = payload.info.account_code;

                //28800是utc
                Int64 nowSpan = Convert.ToInt32((DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds) - 28800 - 600;
                if (payload.exp < nowSpan)
                {
                    throw new Exception("token expire!");
                }

                string stopShippingCode = CustomerRepository.GetByCustomerCode(customerCode).StopShippingCode;
                if(stopShippingCode != "0")
                {
                    throw new Exception("the account code is not active");
                }
               
                Log("request", "end","","","");
                var insertEDI = EDIAPIJsonInsert_New2(getJson, customerCode, addPickUpRequest);

                eDIResultEntities.AddRange(insertEDI);

            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.Message

                };

                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                var payload = JsonConvert.DeserializeObject<PayloadEntity>(json);
                string customerCode = payload.info.account_code;

                Log("errorMsg", ex.Message.ToString(), customerCode, "","fasle");
                Log("request", "end","","","");
                eDIResultEntities.Add(eDIResultEntity);

            }

            return eDIResultEntities;

        }

        private List<EDIResultEntity> EDIAPIJsonInsert_New2(string getJson, string customCode = "", bool addPickUpRequest = true)
        {
            Log("respond", "start","","","");
            bool isShopee = customCode == "F1300600002" || customCode == "F2900210002" ? true : false;      //蝦皮與光年甲配都視為蝦皮

            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();

            TcCbmSize[] cbmSizes = CbmSizeRepository.GetAllCbmSize();

            DateTime start_time = DateTime.Now;


            DeliveryRequestApiEntity _deliveryRequestApiEntity = new DeliveryRequestApiEntity();

            TcDeliveryRequest returnDeliveryRequest = new TcDeliveryRequest();
            try
            {
                var deliveryRequestApiEntities = JsonConvert.DeserializeObject<List<DeliveryRequestApiEntity>>(getJson);

                foreach (DeliveryRequestApiEntity deliveryRequestApiEntity in deliveryRequestApiEntities)
                {

                    _deliveryRequestApiEntity = deliveryRequestApiEntity;

                    if (deliveryRequestApiEntity.ReceiverAddress != "桃園市楊梅區和平路576號")
                    {
                        throw new Exception("收件地址非蝦皮退貨倉庫");
                    }


                    //TcDeliveryRequest returnDeliveryRequest = new TcDeliveryRequest();


                    //圖霸收件地址解析
                    AddressParsingInfo receiveAddressinfo = new AddressParsingInfo();

                    try
                    {
                        receiveAddressinfo = new Tool().GetAddressParsing(deliveryRequestApiEntity.ReceiverAddress, customCode, "3");
                    }
                    catch (Exception e)
                    {
                        ApiAddressParseLog(start_time, e, deliveryRequestApiEntity, returnDeliveryRequest, getJson);
                        throw new Exception("寄件地址解析有誤");
                    }

                    if (string.IsNullOrEmpty(receiveAddressinfo.StationCode))
                    {
                        return EDIAPIJsonInsert(getJson, customCode, addPickUpRequest);
                    }


                    //圖霸寄件地址解析

                    AddressParsingInfo sendAddressInfo = new AddressParsingInfo();
                    try
                    {
                        sendAddressInfo = new Tool().GetAddressParsing(deliveryRequestApiEntity.SenderAddress, customCode, "3");
                    }
                    catch (Exception e)
                    {
                        throw new Exception("收件地址解析有誤");
                    }


                    if (string.IsNullOrEmpty(sendAddressInfo.StationCode))
                    {

                        throw new Exception("寄件地址有誤");

                    }



                    //var sendAddressInfo = new Tool().GetAddressParsing(deliveryRequestApiEntity.SenderAddress, customCode, "4");

                    //if (sendAddressInfo.StationCode == "" )
                    //{
                    //    var exception = "寄件地址有誤";

                    //    throw new Exception(exception);

                    //}
                    //var receiveAddressinfo = new Tool().GetAddressParsing(deliveryRequestApiEntity.ReceiverAddress, customCode, "4");

                    //if (receiveAddressinfo.StationCode == "" )
                    //{
                    //    var exception = "收件地址有誤";

                    //    throw new Exception(exception);

                    //}
                    //測試API
                    //try
                    //{
                    //    var info = new Tool().GetAddressParsing(deliveryRequestApiEntity.SenderAddress, customCode, "3");
                    //}
                    //catch (Exception)
                    //{

                    //}
                    ////測試API
                    //try
                    //{
                    //    var info = new Tool().GetAddressParsing(deliveryRequestApiEntity.ReceiverAddress, customCode, "3");
                    //}
                    //catch (Exception)
                    //{

                    //}

                    EDIResultEntity eDIResultEntity = new EDIResultEntity();

                    eDIResultEntity.Result = true;

                    //重複訂單編號會回傳原來貨號
                    TcDeliveryRequest CheckReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);

                    try
                    {

                        if (customCode.Length > 0 && deliveryRequestApiEntity.CustomerCode.Length > 0 && deliveryRequestApiEntity.CustomerCode != customCode)
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "客戶代碼不一致";

                            eDIResultEntities.Add(eDIResultEntity);
                            continue;
                        }

                       

                        //一開始匯入的正物流checknumber，之後作為逆物流單號寫回return_check_number之用
                        string orignalCheckNumber = deliveryRequestApiEntity.CheckNumber;
                        string orignalCustomerCode = deliveryRequestApiEntity.CustomerCode;
                        //用invoice_dec的正物流訂單編號取回原正物流貨號
                        string orginalShopeeCheckNumber = "";
                        if(isShopee & deliveryRequestApiEntity.Reverse == "Y" & deliveryRequestApiEntity.InvoiceDesc != "")
                        {
                            TcDeliveryRequest ReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.InvoiceDesc, deliveryRequestApiEntity.CustomerCode);
                            
                            if(ReverseOrderNumberData != null)
                            {
                              orginalShopeeCheckNumber = ReverseOrderNumberData.CheckNumber;
                            }
                            
                        }

                        //判斷是否已有逆物流單號
                        //if(orignalCheckNumber.Length != 0 || orignalCheckNumber != null)
                        //{
                        //    var checkNumber = this.DeliveryRequestModifyRepository.GetDataByCheckNumber(orignalCheckNumber, orignalCustomerCode);
                        //    if (checkNumber != null & deliveryRequestApiEntity.Reverse == "Y")
                        //    {
                        //       throw new Exception("此貨號已有逆物流單號");                              
                        //    }
                        //}
                        
 
                        if (deliveryRequestApiEntity.CollectionMoney > 20000)
                        {
                            throw new Exception("代收貨款不能超過20000元");
                        }

                        //蝦皮逆物流一定要有訂單編號，只有填訂單編號要帶出正物流商品資訊，先從ordernumber抓出checknumber，再從checknumber撈
                        //if (isShopee && deliveryRequestApiEntity.Reverse == "Y")
                        //{

                        //    TcDeliveryRequest ReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);

                        //    if(ReverseOrderNumberData != null )
                        //    {
                        //        if (deliveryRequestApiEntity.OrderNumber != "")
                        //        {
                        //            if ( ReverseOrderNumberData.ReturnCheckNumber != "")
                        //            {                                     
                        //                throw new Exception("已經有逆物流編號");
                        //            }
                        //            else if(ReverseOrderNumberData.CheckNumber != "")
                        //            {
                        //                //取得正物流的check_number
                        //                deliveryRequestApiEntity.CheckNumber = ReverseOrderNumberData.CheckNumber;
                        //            }

                        //        }
                        //        else
                        //        {
                        //            throw new Exception("沒有訂單編號");
                        //        }
                        //    }
                                             
                        //}
                        //一般件逆物流不一定要有訂單編號，只有填訂單編號要帶出正物流商品資訊，先從ordernumber抓出checknumber，再從checknumber撈
                        //TcDeliveryRequest ReverseOrderNumberDataForReturnCheckNumber = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);
                        
                        //if (!isShopee && deliveryRequestApiEntity.Reverse == "Y")
                        //{

                        //    TcDeliveryRequest ReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);

                        //    if (ReverseOrderNumberData != null)
                        //    {
                        //        if (deliveryRequestApiEntity.OrderNumber != "")
                        //        {
                        //            if (ReverseOrderNumberData.ReturnCheckNumber != "")
                        //            {
                        //                throw new Exception("已經有逆物流編號");
                        //            }
                        //            else if (ReverseOrderNumberData.CheckNumber != "")
                        //            {
                        //                deliveryRequestApiEntity.CheckNumber = ReverseOrderNumberData.CheckNumber;
                        //            }

                        //        }
                        //    }
                        //}

                        //  TcDeliveryRequest ReverseCheckNumberData = new TcDeliveryRequest();

                        //逆物流只有填貨號時需要帶出正物流的商品資訊寫入逆物流

                        var ReverseCheckNumberData = this.DeliveryRequestModifyRepository.GetDataByCheckNumber(deliveryRequestApiEntity.CheckNumber, deliveryRequestApiEntity.CustomerCode);

                        var ReverseSendStationScode = this.DeliveryRequestModifyRepository.GetSendStationScodeByCheckNumber(deliveryRequestApiEntity.CheckNumber, deliveryRequestApiEntity.CustomerCode);

                        

                        //if (deliveryRequestApiEntity.CheckNumber.Length != 0 & deliveryRequestApiEntity.Reverse == "Y")
                        //{
                        //    deliveryRequestApiEntity.SendStationScode = ReverseSendStationScode.StationCode;
                        //    deliveryRequestApiEntity.OrderNumber = ReverseCheckNumberData.OrderNumber;
                        //    deliveryRequestApiEntity.ReceiveCustomerCode = ReverseCheckNumberData.ReceiveCustomerCode;
                        //    deliveryRequestApiEntity.ReceiveContact = ReverseCheckNumberData.ReceiveContact;
                        //    deliveryRequestApiEntity.ReceiveTel1 = ReverseCheckNumberData.ReceiveTel1;
                        //    deliveryRequestApiEntity.ReceiveTel2 = ReverseCheckNumberData.ReceiveTel2;
                        //    deliveryRequestApiEntity.ReceiverAddress = ReverseCheckNumberData.SendCity + ReverseCheckNumberData.SendArea+ ReverseCheckNumberData.SendAddress;
                        //    deliveryRequestApiEntity.Plates = Convert.ToInt32(ReverseCheckNumberData.Plates);
                        //    deliveryRequestApiEntity.Weight = Convert.ToInt32(ReverseCheckNumberData.CbmWeight);
                        //    deliveryRequestApiEntity.SubpoenaCategory = ReverseCheckNumberData.SubpoenaCategory;
                        //    deliveryRequestApiEntity.CollectionMoney = Convert.ToInt32(ReverseCheckNumberData.CollectionMoney);
                        //    deliveryRequestApiEntity.SendContact = ReverseCheckNumberData.SendContact;
                        //    deliveryRequestApiEntity.SendTel = ReverseCheckNumberData.SendTel;
                        //    deliveryRequestApiEntity.SenderAddress = ReverseCheckNumberData.ReceiveCity + ReverseCheckNumberData.ReceiveArea + ReverseCheckNumberData.ReceiveAddress;
                        //    deliveryRequestApiEntity.SupplierCode = ReverseCheckNumberData.SupplierCode;
                        //    deliveryRequestApiEntity.PrintDate = ReverseCheckNumberData.PrintDate;
                        //    deliveryRequestApiEntity.CbmSize = ReverseCheckNumberData.CbmSize.ToString();
                        //    deliveryRequestApiEntity.ArriveAssignDate = ReverseCheckNumberData.ArriveAssignDate;
                        //    deliveryRequestApiEntity.TimePeriod = ReverseCheckNumberData.TimePeriod;
                        //    deliveryRequestApiEntity.InvoiceDesc = ReverseCheckNumberData.InvoiceDesc;
                        //    deliveryRequestApiEntity.RoundTrip =  ReverseCheckNumberData.RoundTrip.ToString();
                        //    deliveryRequestApiEntity.ArticleNumber = ReverseCheckNumberData.ArticleNumber;
                        //    deliveryRequestApiEntity.SendPlatform = ReverseCheckNumberData.SendPlatform;
                        //    deliveryRequestApiEntity.ArticleName = ReverseCheckNumberData.ArticleName;
                        //    deliveryRequestApiEntity.ReturnCheckNumber = ReverseCheckNumberData.CheckNumber;
                        //}

                        //逆物流要將收、寄件件地址互換;電話也要互換
                        //string reverse_send_address = deliveryRequestApiEntity.ReceiverAddress;
                        //string reverse_receive_address = deliveryRequestApiEntity.SenderAddress;
                        //string reverse_send_tel = deliveryRequestApiEntity.ReceiveTel1;
                        //string reverse_receive_tel = deliveryRequestApiEntity.SendTel;

                        if (deliveryRequestApiEntity.CheckNumber.Length != 0 & deliveryRequestApiEntity.Reverse == "Y")
                        {
                            deliveryRequestApiEntity.ReceiverAddress = ReverseCheckNumberData.SendCity + ReverseCheckNumberData.SendArea + ReverseCheckNumberData.SendAddress;
                            deliveryRequestApiEntity.SenderAddress = ReverseCheckNumberData.ReceiveCity + ReverseCheckNumberData.ReceiveArea + ReverseCheckNumberData.ReceiveAddress;
                            deliveryRequestApiEntity.ReceiveTel1 = ReverseCheckNumberData.SendTel;
                            deliveryRequestApiEntity.SendTel = ReverseCheckNumberData.ReceiveTel1;
                            deliveryRequestApiEntity.ReturnCheckNumber = ReverseCheckNumberData.CheckNumber;
                            deliveryRequestApiEntity.SendStationScode = ReverseSendStationScode.StationCode;
                        }
                        //else if (deliveryRequestApiEntity.CheckNumber.Length == 0 & deliveryRequestApiEntity.Reverse == "Y")
                        //{
                        //    deliveryRequestApiEntity.ReceiverAddress = reverse_send_address;
                        //    deliveryRequestApiEntity.SenderAddress = reverse_receive_address;
                        //    deliveryRequestApiEntity.ReceiveTel1 = reverse_send_tel;
                        //    deliveryRequestApiEntity.SendTel = reverse_receive_tel;
                        //}



                        //先轉為tcDeliveryRequestApiEntity
                        var tcDeliveryRequest = this.Mapper.Map<TcDeliveryRequest>(deliveryRequestApiEntity);

                        var customer = this.CustomerRepository.GetByCustomerCode(tcDeliveryRequest.CustomerCode);

                        eDIResultEntity.OrderNumber = tcDeliveryRequest.OrderNumber;

                        //無代碼代碼時，使用外部傳入的
                        if (deliveryRequestApiEntity.CustomerCode == null || deliveryRequestApiEntity.CustomerCode.Length == 0)
                        {
                            tcDeliveryRequest.CustomerCode = customCode;
                        }

                        //取得收件地址
                        //FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(deliveryRequestApiEntity.ReceiverAddress);

                        //原有拆解地址
                        var originReceiverCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.ReceiverAddress);

                        AddressParsingInfo receiverCity = new AddressParsingInfo();

                        receiverCity.City = receiveAddressinfo.City;
                        receiverCity.Town = receiveAddressinfo.Town;
                        receiverCity.Road = receiveAddressinfo.Road;
                        receiverCity.StationCode = receiveAddressinfo.StationCode;
                        receiverCity.PostZip3 = receiveAddressinfo.PostZip3;
                        receiverCity.StackCode = receiveAddressinfo.StackCode;
                        receiverCity.MotorcycleDriverCode = receiveAddressinfo.MotorcycleDriverCode;
                        receiverCity.SalesDriverCode = receiveAddressinfo.SalesDriverCode;
                        receiverCity.StationName = receiveAddressinfo.StationName;

                        tcDeliveryRequest.ReceiveCity = originReceiverCity.city;
                        tcDeliveryRequest.ReceiveArea = originReceiverCity.area;

                        string realReceiveAddress = ChineseConverter.Convert(deliveryRequestApiEntity.ReceiverAddress, ChineseConversionDirection.SimplifiedToTraditional);

                        if (originReceiverCity.city.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(originReceiverCity.city, "");
                        }

                        if (originReceiverCity.area.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(originReceiverCity.area, "");
                        }

                        tcDeliveryRequest.ReceiveAddress = realReceiveAddress;


                       

                        if( string.IsNullOrEmpty(receiverCity.ToString()) || string.IsNullOrEmpty(receiverCity.City)  || string.IsNullOrEmpty(receiverCity.Town) )
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "無法正確取得收件人的行政區，將無法正確寄送";

                            eDIResultEntities.Add(eDIResultEntity);

                            //記錄開單失敗訊息
                            var exception = eDIResultEntity.Msg;
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = exception,
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now,
                                receiveCity = receiverCity.ToString(),
                                receive_city = receiverCity.City,
                                receive_area = receiverCity.Town

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);

                            continue;

                        }



                        //取得寄件地址

                        //原有拆解地址
                        var originSendCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.SenderAddress);

                        AddressParsingInfo senderCity = new AddressParsingInfo();

                        senderCity.City = sendAddressInfo.City;
                        senderCity.Town = sendAddressInfo.Town;
                        senderCity.Road = sendAddressInfo.Road;
                        senderCity.StationCode = sendAddressInfo.StationCode;
                        senderCity.PostZip3 = sendAddressInfo.PostZip3;
                        senderCity.StackCode = sendAddressInfo.StackCode;
                        senderCity.MotorcycleDriverCode = sendAddressInfo.MotorcycleDriverCode;
                        senderCity.SalesDriverCode = sendAddressInfo.SalesDriverCode;
                        senderCity.StationName = sendAddressInfo.StationName;

                        tcDeliveryRequest.SendCity = originSendCity.city;
                        tcDeliveryRequest.SendArea = originSendCity.area;

                        string realSendAddress = deliveryRequestApiEntity.SenderAddress;

                        if (originSendCity.city != null && originSendCity.city.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(originSendCity.city, "");
                        }

                        if (originSendCity.area != null && originSendCity.area.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(originSendCity.area, "");
                        }

                        tcDeliveryRequest.SendAddress = realSendAddress;

                        //在沒有寫入貨號的情況下，加入逆物流的sendstationscode
                        if(deliveryRequestApiEntity.CheckNumber.Length != 0 & deliveryRequestApiEntity.Reverse == "Y")
                        {
                            var ReverseSendStationScodeByAdress = this.DeliveryRequestModifyRepository.GetSendStationScodeByAdress(senderCity.City, senderCity.Town);
                            tcDeliveryRequest.SendStationScode = ReverseSendStationScodeByAdress.StationCode;
                        }
                      
                        //判斷取件站所(另外要再寫入取件相關表)
                        //FSEAddressEntity fSEAddressEntitySend = new FSEAddressEntity(deliveryRequestApiEntity.SenderAddress);
                        //原拆解地址
                        //var sendDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntitySend);

                        bool bNotSend = true;

                        if(deliveryRequestApiEntity.SenderAddress == "桃園市龜山區航空城JUNFU物流倉")
                        {
                            bNotSend = false;
                        }

                        //OrgArea orgAreaSend = new OrgArea();

                        //if (sendDatas.Count > 0)
                        //{
                        //    foreach (OrgArea orgArea in sendDatas)
                        //    {
                        //        //無站所或聯運區的排除
                        //        if (orgArea.StationScode != null && orgArea.StationScode != "99" && orgArea.StationScode != "95")
                        //        {
                        //            orgAreaSend = orgArea;
                        //            bNotSend = false;
                        //            break;
                        //        }
                        //    }
                        //}

                        if (senderCity.StationCode != null && senderCity.StationCode != "99" && senderCity.StationCode != "95")
                        {
                            //orgAreaSend = orgArea;
                            bNotSend = false;
                            //break;
                        }

                        if (bNotSend && addPickUpRequest == true &&isShopee)
                        {
                            throw new Exception("寄件地址不在服務範圍內！");
                        }

                        //取得托運單
                        //var distributor = this.DeliveryRequestModifyRepository.GetDistributor(receiverCity.city, receiverCity.area);

                        if(!isShopee)
                        {
                            tcDeliveryRequest.SupplierCode = customer.SupplierCode;
                        }
                        else if (("F" + senderCity.StationCode) != "*9聯運")
                        {
                            tcDeliveryRequest.SupplierCode = "F" + senderCity.StationCode;
                        }
                        //蝦皮逆物流地址要先轉回北轉站
                        if(deliveryRequestApiEntity.ReceiverAddress == "桃園市楊梅區和平路576號" & deliveryRequestApiEntity.Reverse == "Y")
                        {
                            receiverCity.StationCode = "29";
                            receiverCity.StationName = "北轉站";
                        }
                        
                        tcDeliveryRequest.AreaArriveCode = receiverCity.StationCode;

                        eDIResultEntity.Area = receiverCity.StationName;
                        eDIResultEntity.AreaId = receiverCity.StationCode;
                        
                        
                        //蝦皮 預設貨品尺寸
                        if (isShopee)
                        {
                            //原有拆地址
                            //var station = this.DeliveryRequestModifyRepository.GetDistributor(sendCity.city, sendCity.area);
                            tcDeliveryRequest.SendStationScode = senderCity.StationCode;
                            if (tcDeliveryRequest.CbmSize != null)
                            {
                                //TcCbmSize[] cbmSizes = CbmSizeRepository.GetAllCbmSize();
                                TcCbmSize cbmSize = cbmSizes.Where(s => s.CbmId == tcDeliveryRequest.CbmSize.ToString()).FirstOrDefault();
                                if (cbmSize != null)
                                {
                                    tcDeliveryRequest.CbmLength = cbmSize.DefaultCbmLength;
                                    tcDeliveryRequest.CbmWidth = cbmSize.DefaultCbmWidth;
                                    tcDeliveryRequest.CbmHeight = cbmSize.DefaultCbmHeight;
                                    tcDeliveryRequest.CbmWeight = cbmSize.DefaultCbmWeight;
                                    tcDeliveryRequest.CbmCont = cbmSize.DefaultCbmCont;
                                }
                            }
                        }
                        else if (deliveryRequestApiEntity.Reverse == "N")
                        {
                            tcDeliveryRequest.SendStationScode = customer.SupplierCode.Replace("F", "");
                        }


                        //非蝦皮 預設貨品尺寸
                        //if (!isShopee && tcDeliveryRequest.CbmSize != null)
                        //{
                        //    TcCbmSize cbmSize = cbmSizes.Where(s => s.CbmId == tcDeliveryRequest.CbmSize.ToString()).FirstOrDefault();
                        //    if (cbmSize != null)
                        //    {
                        //        tcDeliveryRequest.CbmLength = cbmSize.DefaultCbmLength;
                        //        tcDeliveryRequest.CbmWidth = cbmSize.DefaultCbmWidth;
                        //        tcDeliveryRequest.CbmHeight = cbmSize.DefaultCbmHeight;
                        //        tcDeliveryRequest.CbmWeight = cbmSize.DefaultCbmWeight;
                        //        tcDeliveryRequest.CbmCont = cbmSize.DefaultCbmCont;
                        //    }
                        //}

                        //TcDeliveryRequest returnDeliveryRequest;

                        //如果是蝦皮，先查是否已有重復的訂單
                        if ( isShopee && deliveryRequestApiEntity.OrderNumber != null && deliveryRequestApiEntity.OrderNumber.Length > 0)
                        {
                            //先驗證是否有重復訂單
                            var checkDeliveryRequest = this.DeliveryRequestModifyRepository.GetTcDeliveryRequestByCustomerCodeAndOrderNumber(customCode, deliveryRequestApiEntity.OrderNumber);

                            if ( checkDeliveryRequest != null && deliveryRequestApiEntity.Reverse != "Y")
                            {
                                //查證資料是否集貨
                                var myScanLog = this.DeliveryScanLogRepository.GetByCheckNumber(checkDeliveryRequest.CheckNumber);

                                if( myScanLog == null || myScanLog.Count == 0 )
                                {
                                    returnDeliveryRequest = checkDeliveryRequest;
                                    returnDeliveryRequest.ReceiveCity = tcDeliveryRequest.ReceiveCity;
                                    returnDeliveryRequest.ReceiveArea = tcDeliveryRequest.ReceiveArea;
                                    returnDeliveryRequest.ReceiveAddress = tcDeliveryRequest.ReceiveAddress;
                                    returnDeliveryRequest.SendCity = tcDeliveryRequest.SendCity;
                                    returnDeliveryRequest.SendArea = tcDeliveryRequest.SendArea;
                                    returnDeliveryRequest.SendAddress = tcDeliveryRequest.SendAddress;
                                    returnDeliveryRequest.SupplierCode = tcDeliveryRequest.SupplierCode;
                                    returnDeliveryRequest.AreaArriveCode = tcDeliveryRequest.AreaArriveCode;
                                    returnDeliveryRequest.SendStationScode = tcDeliveryRequest.SendStationScode;
                                    returnDeliveryRequest.CbmSize = tcDeliveryRequest.CbmSize;
                                    returnDeliveryRequest.CbmLength = tcDeliveryRequest.CbmLength;
                                    returnDeliveryRequest.CbmWidth = tcDeliveryRequest.CbmWidth;
                                    returnDeliveryRequest.CbmHeight = tcDeliveryRequest.CbmHeight;
                                    returnDeliveryRequest.CbmWeight = tcDeliveryRequest.CbmWeight;
                                    returnDeliveryRequest.CbmCont = tcDeliveryRequest.CbmCont;
                                }
                                else
                                {
                                    throw new Exception("此訂單編號已存在並取件");
                                }
                            }
                            else
                            {
                                if (CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                                {
                                    returnDeliveryRequest = tcDeliveryRequest;
                                }
                                else
                                {
                                  returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                                }                        
                            }
                        }
                        else if( isShopee)
                        {
                            throw new Exception("無訂單編號");
                        }
                        else
                        {
                            returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                        }

                        //計算託運單費用
                        var shipFeeInfo = this.DeliveryRequestModifyRepository.GetShipFee(Convert.ToInt64(returnDeliveryRequest.RequestId));

                        returnDeliveryRequest.SupplierFee = shipFeeInfo.supplier_fee;
                        returnDeliveryRequest.TotalFee = shipFeeInfo.supplier_fee;

                        // 預購袋扣掉貨號數量
                        if(customer.ProductType == 2 || customer.ProductType == 3)
                        {
                            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
                            webServiceSoapClient.UpdateCheckNumberSettingAsync(customCode, 1);
                        }

                        //逆物流不可建立來回件
                        if(deliveryRequestApiEntity.RoundTrip == "Y" && deliveryRequestApiEntity.Reverse =="Y")
                        {
                            throw new Exception("逆物流貨件不可建立來回件，請修改REQUEST");
                        }

                        //逆物流的DeliveryType要改為"R"
                        if(deliveryRequestApiEntity.Reverse == "Y")
                        {
                            returnDeliveryRequest.DeliveryType = "R";
                        }
                        
                        
                        //重複訂單編號會回傳原來貨號   移到上面
                        //TcDeliveryRequest CheckReverseOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);
                        
                        if( CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                        {
                            returnDeliveryRequest.CheckNumber = CheckReverseOrderNumberData.CheckNumber.ToString();
                            eDIResultEntity.CheckNumber = CheckReverseOrderNumberData.CheckNumber.ToString();
                        }
                        else
                        {
                            if (deliveryRequestApiEntity.CheckNumber.Length == 0 & deliveryRequestApiEntity.Reverse != "Y")
                            {
                                long checkNumberTemp = 100000000 + Convert.ToInt64(returnDeliveryRequest.RequestId);
                                int tail = Convert.ToInt32(returnDeliveryRequest.RequestId % 7);

                                string checkNumberPrefix = "";
                                if (isShopee & deliveryRequestApiEntity.Reverse != "Y")
                                {
                                    checkNumberPrefix = "700";
                                }
                                else
                                {
                                    checkNumberPrefix = CheckNumberPreheadRepository.GetPrefixByProductType(customer.ProductType.ToString());
                                }
                                string currentCheckNumber = string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());

                                //string currentCheckNumber = string.Format("{0}{1}", checkNumberPrefix, ReverseCheckNumber);

                                returnDeliveryRequest.CheckNumber = currentCheckNumber.ToString();

                                eDIResultEntity.CheckNumber = currentCheckNumber;
                            }
                            //逆物流
                            else if (isShopee & deliveryRequestApiEntity.Reverse == "Y")
                            {
                                long checkNumberTemp = 100000000 + Convert.ToInt64(returnDeliveryRequest.RequestId);
                                int tail = Convert.ToInt32(returnDeliveryRequest.RequestId % 7);


                                string checkNumberPrefix = "";

                                checkNumberPrefix = "702";

                                string currentCheckNumber = string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());

                                //string currentCheckNumber = string.Format("{0}{1}", checkNumberPrefix, ReverseCheckNumber);

                                returnDeliveryRequest.CheckNumber = currentCheckNumber.ToString();

                                eDIResultEntity.CheckNumber = currentCheckNumber;

                            }
                            else if (!isShopee & deliveryRequestApiEntity.Reverse == "Y")
                            {
                                long checkNumberTemp = 100000000 + Convert.ToInt64(returnDeliveryRequest.RequestId);
                                int tail = Convert.ToInt32(returnDeliveryRequest.RequestId % 7);


                                string checkNumberPrefix = "";

                                checkNumberPrefix = "990";

                                string currentCheckNumber = string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());

                                //string currentCheckNumber = string.Format("{0}{1}", checkNumberPrefix, ReverseCheckNumber);

                                returnDeliveryRequest.CheckNumber = currentCheckNumber.ToString();

                                eDIResultEntity.CheckNumber = currentCheckNumber;

                            }
                        }

                        //如果沒使用區間，主動產生check_number
                        //正物流
                        

                        //計算sdmd資訊
                        //FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(returnDeliveryRequest.ReceiveAddress, returnDeliveryRequest.ReceiveCity, returnDeliveryRequest.ReceiveArea);
                        //原有拆SDMD
                        //var eachDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntity);

                        if (receiverCity != null)
                        {
                            CheckNumberSdMapping checkNumberSdMapping = new CheckNumberSdMapping
                            {
                                CheckNumber = returnDeliveryRequest.CheckNumber,
                                RequestId = (long)returnDeliveryRequest.RequestId,
                                OrgAreaId = Convert.ToInt32(receiverCity.StationCode),
                                Md = receiverCity.MotorcycleDriverCode,
                                Sd = receiverCity.SalesDriverCode,
                                PutOrder = receiverCity.StackCode,
                                Cdate = DateTime.Now,
                                Udate = DateTime.Now
                            };

                            eDIResultEntity.Md = checkNumberSdMapping.Md;
                            eDIResultEntity.Sd = checkNumberSdMapping.Sd;
                            eDIResultEntity.Putorder = checkNumberSdMapping.PutOrder;
                            eDIResultEntity.Area = receiverCity.StationName;
                            eDIResultEntity.AreaId = receiverCity.StationCode;


                            if (CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                            {

                            }
                            else
                            {
                                this.CheckNumberSDMappingRepository.Insert(checkNumberSdMapping);
                            }
                            
                            //站所資訊
                            //var stationCode = this.StationRepository.GetStationCodeByStationScode(eachDatas[0].StationScode);
                            var stationCode = customer.SupplierCode;
                            returnDeliveryRequest.SupplierCode = stationCode;
                            returnDeliveryRequest.ReceiveZip = receiverCity.PostZip3;
                        }

                        //存檔
                        if (CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                        {

                        }
                        else
                        {
                            this.DeliveryRequestModifyRepository.Update(returnDeliveryRequest);
                        }

                        EDI_api_new_data_log_DA _EDI_api_new_data_log_DA = new EDI_api_new_data_log_DA();
                        _EDI_api_new_data_log_DA.InsertAPIorigin(customCode, "GZ_NEW_API");
                        

                        ApiAddressParseLog(start_time, returnDeliveryRequest, getJson);

                        //蝦皮逆物流將逆物流單號寫回正物流
                        if (isShopee & orginalShopeeCheckNumber.Length != 0 && deliveryRequestApiEntity.Reverse == "Y")
                        {
                            tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
                            _tcDeliveryRequests_DA.Update_return_checknumber(orginalShopeeCheckNumber, returnDeliveryRequest.CheckNumber);
                        }
                        //蝦皮逆物流將正物流單號寫入return_checknumber
                        if (isShopee & orginalShopeeCheckNumber.Length != 0 && deliveryRequestApiEntity.Reverse == "Y")
                        {
                            tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
                            _tcDeliveryRequests_DA.Update_return_checknumber(returnDeliveryRequest.CheckNumber, orginalShopeeCheckNumber);
                        }

                        //產出逆物流單號，寫回原本正物流的return_check_number
                        if (!isShopee & orignalCheckNumber.Length != 0 && deliveryRequestApiEntity.Reverse == "Y")
                        {
                            tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
                            _tcDeliveryRequests_DA.Update_return_checknumber(orignalCheckNumber, returnDeliveryRequest.CheckNumber);
                        }

                        //一般件從ordernumber產出逆物流單號，寫回原本正物流的return_check_number
                        //if (!isShopee & orignalCheckNumber.Length == 0 && deliveryRequestApiEntity.Reverse == "Y" && ReverseOrderNumberDataForReturnCheckNumber.CheckNumber.Length != 0)
                        //{
                        //    tcDeliveryRequests_DA _tcDeliveryRequests_DA = new tcDeliveryRequests_DA();
                        //    _tcDeliveryRequests_DA.Update_return_checknumber(ReverseOrderNumberDataForReturnCheckNumber.CheckNumber, returnDeliveryRequest.CheckNumber);
                        //}



                        Log("save", JsonConvert.SerializeObject(returnDeliveryRequest).ToString(), returnDeliveryRequest.CustomerCode, returnDeliveryRequest.CheckNumber,"true");
                        
                        if (addPickUpRequest)
                        {
                            //產生取貨資料
                            if (isShopee)
                            {
                                PickupRequestForApiuser pickupRequestForApiuser = new PickupRequestForApiuser
                                {
                                    CheckNumber = returnDeliveryRequest.CheckNumber,
                                    CustomerCode = returnDeliveryRequest.CustomerCode,
                                    Md = senderCity.MotorcycleDriverCode,
                                    ReassignMd = senderCity.MotorcycleDriverCode,
                                    Pieces = (returnDeliveryRequest.Pieces == null || returnDeliveryRequest.Pieces == 0) ? returnDeliveryRequest.Plates : returnDeliveryRequest.Pieces,
                                    Putorder = senderCity.StackCode,
                                    RequestDate = DateTime.Now,
                                    Sd = senderCity.SalesDriverCode,
                                    SendArea = originSendCity.area,
                                    SendCity = originSendCity.city,
                                    SendRoad = deliveryRequestApiEntity.SenderAddress,
                                    SendTel = returnDeliveryRequest.SendTel,
                                    SupplierCode = "F"+senderCity.StationCode
                                    //SupplierCode = this.StationRepository.GetStationCodeByStationScode(orgAreaSend.StationScode)
                                };
                                if (CheckReverseOrderNumberData != null && !string.IsNullOrEmpty(CheckReverseOrderNumberData.CheckNumber.ToString()))
                                {

                                }
                                else
                                {
                                  this.PickupRequestForApiuserRepository.Insert(pickupRequestForApiuser);
                                }
                                
                            }
                            else
                            {
                                PickUpRequestLog pickUpRequestLog = new PickUpRequestLog();
                                pickUpRequestLog.RequestCustomerCode = deliveryRequestApiEntity.CustomerCode;
                                pickUpRequestLog.PickUpPieces = deliveryRequestApiEntity.Pieces;

                                PickUpRequestRepository.Insert(pickUpRequestLog);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        eDIResultEntity.Result = false;
                        eDIResultEntity.Msg = ex.Message.ToString();

                        //記錄開單失敗訊息
                        ApiAddressParseLog(start_time, ex, deliveryRequestApiEntity, returnDeliveryRequest, getJson);

                    }
                    eDIResultEntities.Add(eDIResultEntity);
                }
            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.Message.ToString()

                };

                ApiAddressParseLog(start_time, ex, _deliveryRequestApiEntity, returnDeliveryRequest, getJson);

                eDIResultEntities.Add(eDIResultEntity);
               
            }
            Log("respond", JsonConvert.SerializeObject(eDIResultEntities).ToString(), customCode, "", eDIResultEntities[0].Result.ToString());
            Log("respond", "end","","","");

            return eDIResultEntities;

        }

        public TcDeliveryRequest GetRequest(long requestId)
        {
            var returnData = this.DeliveryRequestModifyRepository.SelectById(requestId);
            return returnData;
        }

        public List<ApiScanLogEntity> GetScanLogByCustomerCodeAndTimeZone(string customerCode, DateTime start, DateTime end, bool isCheckNumberNull = true)
        {
            List<ApiScanLogEntity> data = this.DeliveryScanLogRepository.GetApiScanLogByCustomerCodeAndTimeZone(customerCode, start, end, isCheckNumberNull);

            List<string> stationCodes = data.Select(d => d.stationCode).Distinct().ToList();

            var stations = this.StationRepository.GetTbStations(stationCodes);

            var driverCodes = data.Select(d => d.driverCode).Distinct().ToList();

            var drivers = this.DriverRepository.GetDriversByDriverCodes(driverCodes);

            var arriveCodes = this.DeliveryScanLogRepository.GetItemCode("AO");

            //針對有回應訊息進行修正
            arriveCodes["H"] = "貨故結案";

            var errorCodes = this.DeliveryScanLogRepository.GetItemCode("EO");

            List<ApiScanLogEntity> returnData = new List<ApiScanLogEntity>();

            foreach (ApiScanLogEntity apiScanLogEntity in data)
            {
                try
                {
                    apiScanLogEntity.driverName = (drivers.ContainsKey(apiScanLogEntity.driverCode.Trim())) ? drivers[apiScanLogEntity.driverCode.Trim()].DriverName : string.Empty;

                    apiScanLogEntity.stationName = (stations.ContainsKey(apiScanLogEntity.stationCode)) ? stations[apiScanLogEntity.stationCode].StationName : string.Empty;

                    if (apiScanLogEntity.deliveryType == "R")
                    {
                        //逆物流時，調整對應的逆物流單號

                        string reCheckNumber = apiScanLogEntity.checkNumber;

                        apiScanLogEntity.checkNumber = apiScanLogEntity.returnCheckNumber;

                        apiScanLogEntity.returnCheckNumber = reCheckNumber;
                    }

                    if (apiScanLogEntity.scanName == "3")
                    {
                        if (apiScanLogEntity.itemCodes != null)
                            apiScanLogEntity.status = (arriveCodes.ContainsKey(apiScanLogEntity.itemCodes)) ? arriveCodes[apiScanLogEntity.itemCodes] : string.Empty;
                    }
                    else //除了配達歷程之外，其他歷程不回傳狀態
                    {
                        if (apiScanLogEntity.itemCodes != null)
                            apiScanLogEntity.status = string.Empty;
                        apiScanLogEntity.itemCodes = string.Empty;
                    }

                    switch (apiScanLogEntity.scanName)
                    {
                        case "1":
                            apiScanLogEntity.scanName = "到著";
                            break;
                        case "2":
                            apiScanLogEntity.scanName = "配送";
                            break;
                        case "3":
                        case "4":
                            apiScanLogEntity.scanName = "配達";
                            break;
                        case "5":
                            apiScanLogEntity.scanName = "集貨";
                            break;
                        case "6":
                            apiScanLogEntity.scanName = "卸集";
                            break;
                        case "7":
                            apiScanLogEntity.scanName = "發送";
                            break;
                        default:
                            break;

                    }

                    //去除毫秒
                    if( apiScanLogEntity.scanDate != null && apiScanLogEntity.scanDate != DateTime.MinValue && apiScanLogEntity.scanDate.Millisecond > 0 )
                    {
                        apiScanLogEntity.scanDate = apiScanLogEntity.scanDate.AddMilliseconds(-apiScanLogEntity.scanDate.Millisecond);
                    }

                    returnData.Add(apiScanLogEntity);
                }
                catch
                {
                    continue;
                }
            }

            return returnData;
        }

        public ApiPickupLogReturnEntity GetPickupLogReturnEntityByCustomer(string customerCode)
        {
            //先取得過去24小時集貨的資料
            DateTime start = DateTime.Now.AddHours(-24);

            var pickedupData = this.DeliveryScanLogRepository.GetLastPickUpScan(customerCode, start);

            var pickedupCheckNumbers = (from request in pickedupData select request.CheckNumber).Distinct().ToList();

            var pickedupRequestInfo = this.DeliveryScanLogRepository.GetByCheckNumberList(pickedupCheckNumbers);

            //整理最後取件的時間
            Dictionary<string, DateTime> keyValuePairs = new Dictionary<string, DateTime>();
            Dictionary<string, string> driverCodes = new Dictionary<string, string>();
            Dictionary<string, string> notPickDrivers = new Dictionary<string, string>();
            Dictionary<string, int> checkNumberGetNum = new Dictionary<string, int>();
            Dictionary<string, bool> checkNumberInsert = new Dictionary<string, bool>();

            //取得司機資訊
            var driverCodeList = (from scan in pickedupData select scan.DriverCode).Distinct().ToList();
            var driverInfo = this.DriverRepository.GetDriversByDriverCodes(driverCodeList);

            //取得集貨失敗的代碼
            var receiveOptions = this.DeliveryScanLogRepository.GetReceiveOptionCode();
            Dictionary<string, string> receiveOptionsPairs = receiveOptions.ToDictionary(p => p.CodeId, p => p.CodeName);


            foreach (TtDeliveryScanLog ttDeliveryScanLog in pickedupData)
            {
                TcDeliveryRequest tcDeliveryRequest = pickedupRequestInfo[ttDeliveryScanLog.CheckNumber];

                string addressData = string.Format("{0}{1}{2}{3}", tcDeliveryRequest.SendContact, tcDeliveryRequest.SendCity, tcDeliveryRequest.SendArea, tcDeliveryRequest.SendAddress);

                //地址的最晚取件時間
                if (!keyValuePairs.ContainsKey(addressData))
                {
                    keyValuePairs.Add(addressData, (DateTime)ttDeliveryScanLog.ScanDate);
                    driverCodes.Add(addressData, ttDeliveryScanLog.DriverCode);
                }
                else if (keyValuePairs[addressData] < ttDeliveryScanLog.ScanDate)
                {
                    keyValuePairs[addressData] = (DateTime)ttDeliveryScanLog.ScanDate;
                    driverCodes[addressData] = ttDeliveryScanLog.DriverCode;
                }

                //取件個數
                int nPiece = 1;
                if (ttDeliveryScanLog.Pieces != null && ttDeliveryScanLog.Pieces != 0)
                {
                    nPiece = (int)ttDeliveryScanLog.Pieces;
                }

                if (!checkNumberGetNum.ContainsKey(ttDeliveryScanLog.CheckNumber))
                {
                    checkNumberGetNum.Add(ttDeliveryScanLog.CheckNumber, nPiece);
                }
                else
                {
                    checkNumberGetNum[ttDeliveryScanLog.CheckNumber] += nPiece;
                }

                //集貨成功才用集貨成功資訊
                bool pickupSuccess = false;

                if (ttDeliveryScanLog.ReceiveOption == null || ttDeliveryScanLog.ReceiveOption == string.Empty || ttDeliveryScanLog.ReceiveOption == "20")
                {
                    pickupSuccess = true;
                }

                //是否已放入輸出資料的初始化
                if (pickupSuccess && !checkNumberInsert.ContainsKey(ttDeliveryScanLog.CheckNumber))
                {
                    checkNumberInsert.Add(ttDeliveryScanLog.CheckNumber, false);
                }
            }

            //取得過去3天還沒集貨的資料
            DateTime putStart = DateTime.Now.AddDays(-3);
            var notPickedInfo = this.DeliveryScanLogRepository.GetLastNotPickUpScan(customerCode, putStart);

            List<TcDeliveryRequest> goAndNotGetData = new List<TcDeliveryRequest>();

            foreach (TcDeliveryRequest tcDeliveryRequest1 in notPickedInfo)
            {
                string addressData1 = string.Format("{0}{1}{2}{3}", tcDeliveryRequest1.SendContact, tcDeliveryRequest1.SendCity, tcDeliveryRequest1.SendArea, tcDeliveryRequest1.SendAddress);

                if (keyValuePairs.ContainsKey(addressData1) && keyValuePairs[addressData1] > tcDeliveryRequest1.Cdate)
                {
                    goAndNotGetData.Add(tcDeliveryRequest1);
                    notPickDrivers.Add(tcDeliveryRequest1.CheckNumber, driverCodes[addressData1]);
                }
            }

            //開始產生資訊
            List<ApiPickupLogEntity> returnData = new List<ApiPickupLogEntity>();

            //已集貨資訊
            foreach (TtDeliveryScanLog ttDeliveryScanLog1 in pickedupData)
            {
                //集貨成功才用集貨成功資訊
                bool pickupSuccess = false;

                if (ttDeliveryScanLog1.ReceiveOption == null || ttDeliveryScanLog1.ReceiveOption == string.Empty || ttDeliveryScanLog1.ReceiveOption == "20")
                {
                    pickupSuccess = true;
                }

                //成功集貨的
                if (pickupSuccess && checkNumberInsert.ContainsKey(ttDeliveryScanLog1.CheckNumber) && !checkNumberInsert[ttDeliveryScanLog1.CheckNumber])
                {
                    string myCustomerCode = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].CustomerCode : customerCode;
                    string deliveryType = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].DeliveryType : "無資訊";

                    string driverCode = ttDeliveryScanLog1.DriverCode ?? string.Empty;
                    string driverType = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverType : "CS";
                    string driverTypeCode = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverTypeCode : "0";
                    ttDeliveryScanLog1.ScanDate = RemoveMilliSecond(ttDeliveryScanLog1.ScanDate);
                    pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate = RemoveMilliSecond(pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate);

                    ApiPickupLogEntity apiPickupLogEntity = new ApiPickupLogEntity
                    {
                        ActualPickup = true,
                        ActualPickupPieces = checkNumberGetNum[ttDeliveryScanLog1.CheckNumber],
                        CheckNumber = ttDeliveryScanLog1.CheckNumber,
                        CustomerCode = myCustomerCode,
                        DeliveryType = deliveryType,
                        DriverCode = driverCode,
                        DriverType = driverType,
                        DriverTypeCode = driverTypeCode,
                        FalseItem = string.Empty,
                        ScanDate = ttDeliveryScanLog1.ScanDate,
                        ScanName = "集貨",
                        ShipDate = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate == null ? (DateTime)ttDeliveryScanLog1.ScanDate : (DateTime)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate,
                        ShouldPickupPieces = (int)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].Pieces,
                        SupplierCode = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].SupplierCode
                    };

                    returnData.Add(apiPickupLogEntity);

                    checkNumberInsert[ttDeliveryScanLog1.CheckNumber] = true;
                }

                //集貨失敗的情況
                if (!pickupSuccess)
                {
                    string myCustomerCode = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].CustomerCode : customerCode;
                    string deliveryType = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].DeliveryType : "無資訊";

                    string driverCode = ttDeliveryScanLog1.DriverCode ?? string.Empty;
                    string driverType = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverType : "CS";
                    string driverTypeCode = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverTypeCode : "0";
                    ttDeliveryScanLog1.ScanDate = RemoveMilliSecond(ttDeliveryScanLog1.ScanDate);
                    pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate = RemoveMilliSecond(pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate);

                    ApiPickupLogEntity apiPickupLogEntity = new ApiPickupLogEntity
                    {
                        ActualPickup = false,
                        ActualPickupPieces = checkNumberGetNum[ttDeliveryScanLog1.CheckNumber],
                        CheckNumber = ttDeliveryScanLog1.CheckNumber,
                        CustomerCode = myCustomerCode,
                        DeliveryType = deliveryType,
                        DriverCode = driverCode,
                        DriverType = driverType,
                        DriverTypeCode = driverTypeCode,
                        FalseItem = receiveOptionsPairs[ttDeliveryScanLog1.ReceiveOption],
                        ScanDate = ttDeliveryScanLog1.ScanDate,
                        ScanName = "集貨",
                        ShipDate = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate == null ? (DateTime)ttDeliveryScanLog1.ScanDate : (DateTime)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate,
                        ShouldPickupPieces = (int)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].Pieces,
                        SupplierCode = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].SupplierCode
                    };

                    returnData.Add(apiPickupLogEntity);
                }
            }

            //未集貨的部分，不主動回拋無件可取貨態
            //foreach (TcDeliveryRequest tcDeliveryRequest2 in goAndNotGetData)
            //{
            //    string driverCode = (notPickDrivers.ContainsKey(tcDeliveryRequest2.CheckNumber)) ? notPickDrivers[tcDeliveryRequest2.CheckNumber] : string.Empty;
            //    string driverType = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverType : "CS";
            //    string driverTypeCode = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverTypeCode : "0";

            //    string addressData2 = string.Format("{0}{1}{2}{3}", tcDeliveryRequest2.SendContact, tcDeliveryRequest2.SendCity, tcDeliveryRequest2.SendArea, tcDeliveryRequest2.SendAddress);
            //    keyValuePairs[addressData2] = (DateTime)RemoveMilliSecond(keyValuePairs[addressData2]);

            //    ApiPickupLogEntity apiPickupLogEntity = new ApiPickupLogEntity
            //    {
            //        ActualPickup = false,
            //        ActualPickupPieces = 0,
            //        CheckNumber = tcDeliveryRequest2.CheckNumber,
            //        CustomerCode = tcDeliveryRequest2.CustomerCode,
            //        DeliveryType = tcDeliveryRequest2.DeliveryType,
            //        DriverCode = driverCode,
            //        DriverType = driverType,
            //        DriverTypeCode = driverTypeCode,
            //        FalseItem = "無件可取",
            //        ScanDate = null,
            //        ScanName = "集貨",
            //        ShipDate = keyValuePairs[addressData2],
            //        ShouldPickupPieces = (int)tcDeliveryRequest2.Pieces,
            //        SupplierCode = tcDeliveryRequest2.SupplierCode
            //    };

            //    returnData.Add(apiPickupLogEntity);
            //}

            ApiPickupLogReturnEntity apiPickupLogReturnEntity = new ApiPickupLogReturnEntity();

            apiPickupLogReturnEntity.PickupInfo = returnData;

            return apiPickupLogReturnEntity;
        }

        public ApiCbmInfoReturnEntity GetMeasuredCbmInfo(string customerCode)
        {
            //取得過去24小時量的資訊
            /*DateTime start = DateTime.Now.AddHours(-24);
            DateTime end = DateTime.Now;*/

            DateTime now = DateTime.Now;
            DateTime yesterday = now.AddDays(-1);
            DateTime start = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, 5, 0, 0);
            DateTime end = new DateTime(now.Year, now.Month, now.Day, 5, 0, 0);

            var requests = this.DeliveryScanLogRepository.GetMeasuredInfo(customerCode, start, end);

            List<ApiCbmInfoEntity> apiCbmInfoEntities = new List<ApiCbmInfoEntity>();

            foreach (TcDeliveryRequest tcDeliveryRequest in requests)
            {
                if (tcDeliveryRequest.CbmLength == null || tcDeliveryRequest.CbmHeight == null || tcDeliveryRequest.CbmWidth == null)
                {
                    tcDeliveryRequest.CbmLength = 200;
                    tcDeliveryRequest.CbmHeight = 200;
                    tcDeliveryRequest.CbmWidth = 190;
                    tcDeliveryRequest.CbmCont = 0.3;
                    tcDeliveryRequest.CbmWeight = 1.6;
                }

                double sum = (double)tcDeliveryRequest.CbmLength + (double)tcDeliveryRequest.CbmHeight + (double)tcDeliveryRequest.CbmWidth;

                string size = "";

                if (sum <= 600)
                {
                    size = "S60";
                }
                else if (sum <= 900)
                {
                    size = "S90";
                }
                else if (sum <= 1200)
                {
                    size = "S120";
                }
                else
                {
                    size = "S150";
                }



                ApiCbmInfoEntity apiCbmInfoEntity = new ApiCbmInfoEntity
                {
                    CheckNumber = tcDeliveryRequest.CheckNumber,
                    Size = size,
                    Length = (Double)tcDeliveryRequest.CbmLength,
                    Width = (Double)tcDeliveryRequest.CbmWidth,
                    Height = (Double)tcDeliveryRequest.CbmHeight,
                    SidesSum = sum,
                    Cbm = (Double)tcDeliveryRequest.CbmCont,
                    Weight = tcDeliveryRequest.CbmWeight ?? 0,
                    MeasurementTime = DateTime.Today
                };

                apiCbmInfoEntities.Add(apiCbmInfoEntity);
            }

            ApiCbmInfoReturnEntity apiCbmInfoReturnEntity = new ApiCbmInfoReturnEntity
            {
                CbmInfo = apiCbmInfoEntities
            };

            return apiCbmInfoReturnEntity;
        }
        private DateTime? RemoveMilliSecond(DateTime? dt)
        {
            if (dt != null)
                return ((DateTime)dt).AddMilliseconds(-((DateTime)dt).Millisecond);
            else
                return null;
        }
    }
}
