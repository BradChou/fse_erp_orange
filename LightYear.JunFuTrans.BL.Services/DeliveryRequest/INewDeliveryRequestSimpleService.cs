﻿using System;
using System.Collections.Generic;
using System.Text;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.Statics;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.BL.BE.DriverToStationPayment;
using LightYear.JunFuTrans.BL.BE.Api;

namespace LightYear.JunFuTrans.BL.Services.DeliveryRequest
{
    public interface INewDeliveryRequestSimpleService
    {
        public List<EDIResultEntity> EDIAPI_New(string token, string getJson, bool addPickUpRequest = true);

        public TcDeliveryRequest GetRequest(long requestId);

        public List<ApiScanLogEntity> GetScanLogByCustomerCodeAndTimeZone(string customerCode, DateTime start, DateTime end, bool isCheckNumberNull = true);

        public ApiPickupLogReturnEntity GetPickupLogReturnEntityByCustomer(string customerCode);

        public ApiCbmInfoReturnEntity GetMeasuredCbmInfo(string customerCode);

        
    }
}
