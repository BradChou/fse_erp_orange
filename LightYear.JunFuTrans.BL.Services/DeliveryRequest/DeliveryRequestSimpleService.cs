﻿using AutoMapper;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.BusinessReport;
using LightYear.JunFuTrans.BL.BE.DeliveryRequest;
using LightYear.JunFuTrans.BL.BE.PickUpRequest;
using LightYear.JunFuTrans.BL.BE.Enumeration;
using LightYear.JunFuTrans.BL.BE.Statics;
using LightYear.JunFuTrans.BL.BE.Api;
using LightYear.JunFuTrans.BL.Services.Barcode;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.PickUpRequest;
using LightYear.JunFuTrans.DA.Repositories.DeliveryException;
using LightYear.JunFuTrans.DA.Repositories.DeliveryRequest;
using LightYear.JunFuTrans.DA.Repositories.Station;
using LightYear.JunFuTrans.DA.Repositories.Customer;
using LightYear.JunFuTrans.Utilities.Reports;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using LightYear.JunFuTrans.DA.Repositories.CbmSize;
using JunFuTrans.DA.JunFuTrans.DA;
using JunFuTrans.DA.JunFuTrans.condition;
using LightYear.JunFuTrans.DA.Repositories.CustomerChecknumberSetting;
using Common;
using Common.Model;
using System.Threading.Tasks;
using System.Threading;

namespace LightYear.JunFuTrans.BL.Services.DeliveryRequest
{
    public class DeliveryRequestSimpleService : IDeliveryRequestSimpleService
    {

        public IDeliveryRequestModifyRepository DeliveryRequestModifyRepository { get; set; }

        public IPickUpRequestRepository PickUpRequestRepository { get; set; }

        public ICheckNumberSDMappingRepository CheckNumberSDMappingRepository { get; set; }

        public IOrgAreaRepository OrgAreaRepository { get; set; }

        public IStationRepository StationRepository { get; set; }

        public ICustomerRepository CustomerRepository { get; set; }

        public IDriverRepository DriverRepository { get; set; }

        public IDeliveryScanLogRepository DeliveryScanLogRepository { get; set; }

        public IPickupRequestForApiuserRepository PickupRequestForApiuserRepository { get; set; }

        public ICheckNumberPreheadRepository CheckNumberPreheadRepository { get; set; }

        public IMapper Mapper { get; private set; }

        public ICbmSizeRepository CbmSizeRepository { get; set; }

        public ICustomerChecknumberSettingRepository CustomerChecknumberSettingRepository { get; set; }



        public DeliveryRequestSimpleService(
                IDeliveryRequestModifyRepository deliveryRequestModifyRepository,
                IPickUpRequestRepository pickUpRequestRepository,
                ICheckNumberSDMappingRepository checkNumberSDMappingRepository,
                IOrgAreaRepository orgAreaRepository,
                IStationRepository stationRepository,
                ICustomerRepository customerRepository,
                IDriverRepository driverRepository,
                IDeliveryScanLogRepository deliveryScanLogRepository,
                IPickupRequestForApiuserRepository pickupRequestForApiuserRepository,
                IMapper mapper,
                ICheckNumberPreheadRepository checkNumberPreheadRepository,
                ICbmSizeRepository cbmSizeRepository,
                ICustomerChecknumberSettingRepository customerChecknumberSettingRepository


                )
        {
            this.DeliveryRequestModifyRepository = deliveryRequestModifyRepository;
            this.PickUpRequestRepository = pickUpRequestRepository;
            this.CheckNumberSDMappingRepository = checkNumberSDMappingRepository;
            this.OrgAreaRepository = orgAreaRepository;
            this.StationRepository = stationRepository;
            this.CustomerRepository = customerRepository;
            this.DriverRepository = driverRepository;
            this.DeliveryScanLogRepository = deliveryScanLogRepository;
            this.PickupRequestForApiuserRepository = pickupRequestForApiuserRepository;
            this.Mapper = mapper;
            CheckNumberPreheadRepository = checkNumberPreheadRepository;
            CbmSizeRepository = cbmSizeRepository;
            CustomerChecknumberSettingRepository = customerChecknumberSettingRepository;

        }

        public DeliveryRequestSimpleService(DeliveryRequestModifyRepository deliveryRequestModifyRepository, PickUpRequestRepository pickUpRequestRepository, CheckNumberSDMappingRepository checkNumberSDMappingRepository, OrgAreaRepository orgAreaRepository, StationRepository stationRepository, CustomerRepository customerRepository, DriverRepository driverRepository, DeliveryScanLogRepository deliveryScanLogRepository, PickupRequestForApiuserRepository pickupRequestForApiuserRepository, IMapper mapper, CheckNumberPreheadRepository checkNumberPreheadRepository, CbmSizeRepository cbmSizeRepository)
        {
            DeliveryRequestModifyRepository = deliveryRequestModifyRepository;
            PickUpRequestRepository = pickUpRequestRepository;
            CheckNumberSDMappingRepository = checkNumberSDMappingRepository;
            OrgAreaRepository = orgAreaRepository;
            StationRepository = stationRepository;
            CustomerRepository = customerRepository;
            DriverRepository = driverRepository;
            DeliveryScanLogRepository = deliveryScanLogRepository;
            PickupRequestForApiuserRepository = pickupRequestForApiuserRepository;
            Mapper = mapper;
            CheckNumberPreheadRepository = checkNumberPreheadRepository;
            CbmSizeRepository = cbmSizeRepository;
        }

        static void Log(string action, string message, string customer_code, string check_number, string result)
        {
            string EdiApiDataLogType = "2002";
            EDI_api_new_data_log_DA _EDI_api_new_data_log_DA = new EDI_api_new_data_log_DA();
            _EDI_api_new_data_log_DA.InsertEDI_api_log(new EDI_api_new_data_log_Condition { type = EdiApiDataLogType, action = action, message = message, cdate = DateTime.Now, customer_code = customer_code, check_number = check_number, result = result });
            

        }
        static void ApiAddressParseLog(DateTime start_time, Exception ex, DeliveryRequestApiEntity deliveryRequestApiEntity, TcDeliveryRequest returnDeliveryRequest, string getJson)
        {
            ApiAddressParseLog_Condition _ApiAddressParseLog = new ApiAddressParseLog_Condition
            {
                api_type = "3",
                customer_code = deliveryRequestApiEntity.CustomerCode,
                order_number = deliveryRequestApiEntity.OrderNumber,
                other_param = "",
                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                receive_station = returnDeliveryRequest.AreaArriveCode,
                send_address = deliveryRequestApiEntity.SenderAddress,
                send_station = returnDeliveryRequest.SendStationScode,
                result_message = "false",
                source_data = getJson.ToString(),
                exception_message = ex.Message.ToString(),
                start_time = start_time,
                end_time = DateTime.Now
            };
            ApiAddressParseLog_DA _ApiAddressParseLog_DA = new ApiAddressParseLog_DA();
            _ApiAddressParseLog_DA.InsertApiAddressParseLog(_ApiAddressParseLog);


        }
        static void ApiAddressParseLog(DateTime start_time, TcDeliveryRequest returnDeliveryRequest, string getJson)
        {

            //ApiAddressParseLog_DA _ApiAddressParseLog_DA = new ApiAddressParseLog_DA();
            //_ApiAddressParseLog_DA.InsertApiAddressParseLog(new ApiAddressParseLog_Condition { api_type = api_type, customer_code = customer_code, log_id = log_id, order_number = order_number, other_param = other_param, receive_address = receive_address, receive_station = receive_station, send_address = send_address, send_station = send_station, result_message = result_message, exception_message = exception_message, source_data = source_data, start_time = start_time, end_time = end_time });

            ApiAddressParseLog_Condition _ApiAddressParseLog = new ApiAddressParseLog_Condition
            {
                api_type = "3",
                customer_code = returnDeliveryRequest.CustomerCode,
                order_number = returnDeliveryRequest.OrderNumber,
                other_param = "",
                receive_address = returnDeliveryRequest.ReceiveCity + returnDeliveryRequest.ReceiveArea + returnDeliveryRequest.ReceiveAddress,
                receive_station = returnDeliveryRequest.AreaArriveCode,
                send_address = returnDeliveryRequest.SendCity + returnDeliveryRequest.SendArea + returnDeliveryRequest.SendAddress,
                send_station = returnDeliveryRequest.SendStationScode,
                result_message = "true",
                exception_message = "",
                source_data = getJson.ToString(),
                start_time = start_time,
                end_time = DateTime.Now,
                check_number = returnDeliveryRequest.CheckNumber
            };
            ApiAddressParseLog_DA _ApiAddressParseLog_DA = new ApiAddressParseLog_DA();
            _ApiAddressParseLog_DA.InsertApiAddressParseLog(_ApiAddressParseLog);

        }

        private AddressParsingInfo PreGetAddressParsing(string address, string customCode, string comeform)
        {
            //return new Tool().GetAddressParsing(address, customCode, comeform);
            AddressParsingInfo addressParse = new AddressParsingInfo();
            var tArr = new List<Task>();

            var t = Task.Factory.StartNew(() =>
            {
                return addressParse = new Tool().GetAddressParsing(address, customCode, comeform);
            });
            tArr.Add(t);
            Task.WaitAll(tArr.ToArray(), 2000);

            return addressParse;
        }

        private SpecialAreaManage PreGetSpecialAreaFee(string address, string customCode, string comeform)
        {
            try
            {
                SpecialAreaManage addressParse = new SpecialAreaManage();
                var tArr = new List<Task>();

                var t = Task.Factory.StartNew(() =>
                {
                    return addressParse = new Tool().GetSpecialAreaFee(address, customCode, comeform);
                });
                tArr.Add(t);
                Task.WaitAll(tArr.ToArray(), 2000);

                return addressParse;
            }
            catch (Exception)
            {

                throw;
            }


        }


        public List<EDIResultEntity> EDIAPI(string token, string getJson, bool addPickUpRequest = true)
        {
            return EDIAPI_NEW(token, getJson, addPickUpRequest);
        }


        private List<EDIResultEntity> EDIAPI_NEW(string token, string getJson, bool addPickUpRequest = true)
        {
            Log("request", "start", "", "", "");
            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();

            //先驗證token
            try
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                var payload = JsonConvert.DeserializeObject<PayloadEntity>(json);

                string customerCode = payload.info.account_code;

                //28800是utc
                Int64 nowSpan = Convert.ToInt64((DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds) - 28800 - 600;
                if (payload.exp < nowSpan)
                {
                    var exception = "token expire!";

                    throw new Exception(exception);
                }

                string stopShippingCode = CustomerRepository.GetByCustomerCode(customerCode).StopShippingCode;
                if (stopShippingCode != "0")
                {
                    var exception = "the account code is not active";

                    throw new Exception(exception);
                }

                Log("token", token, "", "", "");
                Log("getjson", getJson, "", "", "");
                Log("request", "end", "", "", "");

                var insertEDI = EDIAPIJsonInsert_NEW(getJson, customerCode, addPickUpRequest);

                eDIResultEntities.AddRange(insertEDI);

                if (eDIResultEntities.Count > 0)
                {
                    EDIResultEntity_Condition _EDIResultEntity = new EDIResultEntity_Condition()
                    {
                        CheckNumber = eDIResultEntities[0].CheckNumber,
                        Result = eDIResultEntities[0].Result,
                        Msg = eDIResultEntities[0].Msg,
                        OrderNumber = eDIResultEntities[0].OrderNumber,
                        Sd = eDIResultEntities[0].Sd,
                        Md = eDIResultEntities[0].Md,
                        Putorder = eDIResultEntities[0].Putorder,
                        Area = eDIResultEntities[0].Area,
                        AreaId = eDIResultEntities[0].AreaId,
                        Cdate = DateTime.Now

                    };

                    EDIResultEntity_DA _EDIResultEntity_DA = new EDIResultEntity_DA();
                    _EDIResultEntity_DA.InsertEDIResult(_EDIResultEntity.CheckNumber, _EDIResultEntity.Result, _EDIResultEntity.Msg, _EDIResultEntity.OrderNumber, _EDIResultEntity.Sd, _EDIResultEntity.Md, _EDIResultEntity.Putorder, _EDIResultEntity.Area, _EDIResultEntity.AreaId, _EDIResultEntity.Cdate);
                
                }


            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.Message
                };

                //記錄開單失敗訊息

                ShopeeException _ShopeeException = new ShopeeException
                {
                    exception = ex.Message,
                    cdate = DateTime.Now,
                    json = getJson

                };

                ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                _ShopeeException_DA.InsertShopeeException(_ShopeeException);

                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                var payload = JsonConvert.DeserializeObject<PayloadEntity>(json);
                string customerCode = payload.info.account_code;

                Log("errorMsg", ex.Message.ToString(), customerCode, "", "false");
                Log("request", "end", "", "", "");
                eDIResultEntities.Add(eDIResultEntity);
            }


            return eDIResultEntities;
        }

        private List<EDIResultEntity> EDIAPIJsonInsert_NEW(string getJson, string customCode = "", bool addPickUpRequest = true)
        {
            Log("respond", "start", "", "", "");
            bool isShopee = customCode == "F1300600002" || customCode == "F2900210002" ? true : false;      //蝦皮與光年甲配都視為蝦皮

            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();

            TcCbmSize[] cbmSizes = CbmSizeRepository.GetAllCbmSize();



            try
            {
                var deliveryRequestApiEntities = JsonConvert.DeserializeObject<List<DeliveryRequestApiEntity>>(getJson);


                foreach (DeliveryRequestApiEntity deliveryRequestApiEntity in deliveryRequestApiEntities)
                {

                    DateTime start_time = DateTime.Now;

                    EDIResultEntity eDIResultEntity = new EDIResultEntity();

                    eDIResultEntity.Result = true;

                    TcDeliveryRequest returnDeliveryRequest = new TcDeliveryRequest();

                    //回傳重複訂單編號
                    //check_number_sd_mapping_DA _check_number_sd_mapping = new check_number_sd_mapping_DA();

                    //var resutlt =  _check_number_sd_mapping.CheckSDMD(deliveryRequestApiEntity.CheckNumber);

                    tcDeliveryRequests_DA tcDeliveryRequests = new tcDeliveryRequests_DA();

                    string receiveAddressinfoCity = string.Empty;
                    string receiveAddressinfoTown = string.Empty;
                    string receiveAddressinfoRoad = string.Empty;
                    string receiveAddressinfoStationCode = string.Empty;
                    string receiveAddressinfoPostZip3 = string.Empty;
                    string receiveAddressinfoStackCode = string.Empty;
                    string receiveAddressinfoMotorcycleDriverCode = string.Empty;
                    string receiveAddressinfoSalesDriverCode = string.Empty;
                    string receiveAddressinfoStationName = string.Empty;
                    string receiveAddressinfoShuttleStationCode = string.Empty;
                    int? receiveAddressinfoSpecialAreaFee = 0;
                    string receiveAddressinfoSpecialAreaId = string.Empty;


                    string sendAddressInfoCity = string.Empty;
                    string sendAddressInfoTown = string.Empty;
                    string sendAddressInfoRoad = string.Empty;
                    string sendAddressInfoStationCode = string.Empty;
                    string sendAddressInfoPostZip3 = string.Empty;
                    string sendAddressInfoStackCode = string.Empty;
                    string sendAddressInfoMotorcycleDriverCode = string.Empty;
                    string sendAddressInfoSalesDriverCode = string.Empty;
                    string sendAddressInfoStationName = string.Empty;


                    if (isShopee)
                    {
                        var result = new tcDeliveryRequests_Condition();
                        result = tcDeliveryRequests.CheckSDMD(deliveryRequestApiEntity.OrderNumber);

                        if (result != null && isShopee)
                        {
                            eDIResultEntity.Result = true;
                            eDIResultEntity.CheckNumber = result.check_number;
                            eDIResultEntity.AreaId = result.area_arrive_code.ToString();
                            eDIResultEntity.OrderNumber = result.order_number;
                            eDIResultEntity.Area = result.station_name;

                            eDIResultEntities.Add(eDIResultEntity);


                            return eDIResultEntities;
                        }
                    }


                    //重複訂單編號會回傳原來貨號
                    //TcDeliveryRequest CheckOrderNumberData = new TcDeliveryRequest();

                    //if(!string.IsNullOrEmpty(deliveryRequestApiEntity.OrderNumber))
                    //{
                    //    CheckOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);
                    //}

                    //代收貨款是否超過2萬
                    if (deliveryRequestApiEntity.CollectionMoney > 20000)
                    {

                        //記錄開單失敗訊息
                        var exception = "代收貨款超過20,000上限，請重新輸入";

                        ShopeeException _ShopeeException = new ShopeeException
                        {
                            exception = exception,
                            cdate = DateTime.Now,
                            json = getJson,
                            order_number = deliveryRequestApiEntity.OrderNumber,
                            send_address = deliveryRequestApiEntity.SenderAddress,
                            receive_address = deliveryRequestApiEntity.ReceiverAddress,
                            customer_code = deliveryRequestApiEntity.CustomerCode,
                            start_time = start_time,
                            end_time = DateTime.Now

                        };

                        ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                        _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                        throw new Exception(exception);

                    }

                    SpecialAreaManage SpecialAreaAddressinfo = new SpecialAreaManage();

                    SpecialAreaAddressinfo = PreGetSpecialAreaFee(deliveryRequestApiEntity.ReceiverAddress, customCode, "3");

                    if (!string.IsNullOrEmpty(SpecialAreaAddressinfo.StationCode))
                    {
                        receiveAddressinfoCity = string.IsNullOrEmpty(SpecialAreaAddressinfo.City) ? string.Empty : SpecialAreaAddressinfo.City;
                        receiveAddressinfoTown = string.IsNullOrEmpty(SpecialAreaAddressinfo.Town) ? string.Empty : SpecialAreaAddressinfo.Town;
                        receiveAddressinfoRoad = string.IsNullOrEmpty(SpecialAreaAddressinfo.Road) ? string.Empty : SpecialAreaAddressinfo.Road;
                        receiveAddressinfoStationCode = string.IsNullOrEmpty(SpecialAreaAddressinfo.StationCode) ? string.Empty : SpecialAreaAddressinfo.StationCode;
                        receiveAddressinfoStackCode = string.IsNullOrEmpty(SpecialAreaAddressinfo.StackCode) ? string.Empty : SpecialAreaAddressinfo.StackCode;
                        receiveAddressinfoMotorcycleDriverCode = string.IsNullOrEmpty(SpecialAreaAddressinfo.MotorcycleDriverCode) ? string.Empty : SpecialAreaAddressinfo.MotorcycleDriverCode;
                        receiveAddressinfoSalesDriverCode = string.IsNullOrEmpty(SpecialAreaAddressinfo.SalesDriverCode) ? string.Empty : SpecialAreaAddressinfo.SalesDriverCode;

                        receiveAddressinfoShuttleStationCode = string.IsNullOrEmpty(SpecialAreaAddressinfo.ShuttleStationCode) ? string.Empty : SpecialAreaAddressinfo.ShuttleStationCode;
                        receiveAddressinfoSpecialAreaFee = SpecialAreaAddressinfo.Fee == null ? 0 : SpecialAreaAddressinfo.Fee;
                        receiveAddressinfoSpecialAreaId = string.IsNullOrEmpty(SpecialAreaAddressinfo.id.ToString()) ? string.Empty : SpecialAreaAddressinfo.id.ToString();

                        tbStation_DA _tbStation = new tbStation_DA();
                        var StationName = _tbStation.GetStationName(SpecialAreaAddressinfo.StationCode);
                        if (StationName != null)
                        {
                            receiveAddressinfoStationName = string.IsNullOrEmpty(StationName.station_name) ? string.Empty : StationName.station_name;
                        }

                        eDIResultEntity.Msg = "特服區需加收費用";
                    }
                    else
                    {


                        //圖霸收件地址解析
                        var receiveAddressinfo = new AddressParsingInfo();

                        try
                        {

                            receiveAddressinfo = PreGetAddressParsing(deliveryRequestApiEntity.ReceiverAddress, customCode, "3");

                            receiveAddressinfoCity = string.IsNullOrEmpty(receiveAddressinfo.City) ? string.Empty : receiveAddressinfo.City;
                            receiveAddressinfoTown = string.IsNullOrEmpty(receiveAddressinfo.Town) ? string.Empty : receiveAddressinfo.Town;
                            receiveAddressinfoRoad = string.IsNullOrEmpty(receiveAddressinfo.Road) ? string.Empty : receiveAddressinfo.Road;
                            receiveAddressinfoStationCode = string.IsNullOrEmpty(receiveAddressinfo.StationCode) ? string.Empty : receiveAddressinfo.StationCode;
                            receiveAddressinfoStackCode = string.IsNullOrEmpty(receiveAddressinfo.StackCode) ? string.Empty : receiveAddressinfo.StackCode;
                            receiveAddressinfoMotorcycleDriverCode = string.IsNullOrEmpty(receiveAddressinfo.MotorcycleDriverCode) ? string.Empty : receiveAddressinfo.MotorcycleDriverCode;
                            receiveAddressinfoSalesDriverCode = string.IsNullOrEmpty(receiveAddressinfo.SalesDriverCode) ? string.Empty : receiveAddressinfo.SalesDriverCode;
                            receiveAddressinfoStationName = string.IsNullOrEmpty(receiveAddressinfo.StationName) ? string.Empty : receiveAddressinfo.StationName;
                            receiveAddressinfoShuttleStationCode = string.IsNullOrEmpty(receiveAddressinfo.ShuttleStationCode) ? string.Empty : receiveAddressinfo.ShuttleStationCode;

                            if (string.IsNullOrEmpty(receiveAddressinfoStationCode))
                            {
                                ShopeeException _ShopeeException = new ShopeeException
                                {
                                    exception = "收件地址解析失敗",
                                    cdate = DateTime.Now,
                                    json = getJson,
                                    order_number = deliveryRequestApiEntity.OrderNumber,
                                    send_address = deliveryRequestApiEntity.SenderAddress,
                                    receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                    customer_code = deliveryRequestApiEntity.CustomerCode,
                                    start_time = start_time,
                                    end_time = DateTime.Now,
                                };

                                ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                                _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                                return EDIAPIJsonInsert_OLD(getJson, customCode, addPickUpRequest);
                            }
                        }
                        catch (Exception e)
                        {
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = "收件地址解析失敗",
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now,
                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                            return EDIAPIJsonInsert_OLD(getJson, customCode, addPickUpRequest);
                            //throw new Exception("收件地址解析有誤");
                        }

                        //if (string.IsNullOrEmpty(receiveAddressinfo.StationCode))
                        //{
                        //    return EDIAPIJsonInsert_OLD(getJson, customCode, addPickUpRequest);
                        //}

                    }


                    //圖霸寄件地址解析

                    AddressParsingInfo sendAddressInfo = new AddressParsingInfo();
                    try
                    {
                        sendAddressInfo = PreGetAddressParsing(deliveryRequestApiEntity.SenderAddress, customCode, "3");

                        sendAddressInfoCity = string.IsNullOrEmpty(sendAddressInfo.City) ? string.Empty : sendAddressInfo.City;
                        sendAddressInfoTown = string.IsNullOrEmpty(sendAddressInfo.Town) ? string.Empty : sendAddressInfo.Town;
                        sendAddressInfoRoad = string.IsNullOrEmpty(sendAddressInfo.Road) ? string.Empty : sendAddressInfo.Road;
                        sendAddressInfoStationCode = string.IsNullOrEmpty(sendAddressInfo.StationCode) ? string.Empty : sendAddressInfo.StationCode;
                        sendAddressInfoPostZip3 = string.IsNullOrEmpty(sendAddressInfo.PostZip33) ? string.Empty : sendAddressInfo.PostZip33;
                        sendAddressInfoStackCode = string.IsNullOrEmpty(sendAddressInfo.StackCode) ? string.Empty : sendAddressInfo.StackCode;
                        sendAddressInfoMotorcycleDriverCode = string.IsNullOrEmpty(sendAddressInfo.sendMotorcycleDriverCode) ? string.Empty : sendAddressInfo.sendMotorcycleDriverCode;
                        sendAddressInfoSalesDriverCode = string.IsNullOrEmpty(sendAddressInfo.sendSalesDriverCode) ? string.Empty : sendAddressInfo.sendSalesDriverCode;
                        sendAddressInfoStationName = string.IsNullOrEmpty(sendAddressInfo.StationName) ? string.Empty : sendAddressInfo.StationName;


                        if (string.IsNullOrEmpty(sendAddressInfo.StationCode))
                        {
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = "寄件地址解析有誤",
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now,
                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                            return EDIAPIJsonInsert_OLD(getJson, customCode, addPickUpRequest);
                        }
                    }
                    catch (Exception e)
                    {
                        ShopeeException _ShopeeException = new ShopeeException
                        {
                            exception = "寄件地址解析失敗",
                            cdate = DateTime.Now,
                            json = getJson,
                            order_number = deliveryRequestApiEntity.OrderNumber,
                            send_address = deliveryRequestApiEntity.SenderAddress,
                            receive_address = deliveryRequestApiEntity.ReceiverAddress,
                            customer_code = deliveryRequestApiEntity.CustomerCode,
                            start_time = start_time,
                            end_time = DateTime.Now,
                        };

                        ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                        _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                        return EDIAPIJsonInsert_OLD(getJson, customCode, addPickUpRequest);
                        //throw new Exception("寄件地址解析有誤");
                    }

                    try
                    {

                        if (customCode.Length > 0 && deliveryRequestApiEntity.CustomerCode.Length > 0 && deliveryRequestApiEntity.CustomerCode != customCode)
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "客戶代碼不一致";


                            //記錄開單失敗訊息

                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = eDIResultEntity.Msg,
                                cdate = DateTime.Now,
                                json = getJson

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                            eDIResultEntities.Add(eDIResultEntity);
                            continue;
                        }

                        //文件pieces誤寫為plates
                        deliveryRequestApiEntity.Pieces = deliveryRequestApiEntity.Plates;

                        deliveryRequestApiEntity.Plates = 0;

                        //先轉為tcDeliveryRequestApiEntity
                        var tcDeliveryRequest = this.Mapper.Map<TcDeliveryRequest>(deliveryRequestApiEntity);

                        var customer = this.CustomerRepository.GetByCustomerCode(tcDeliveryRequest.CustomerCode);

                        eDIResultEntity.OrderNumber = tcDeliveryRequest.OrderNumber;

                        //無代碼代碼時，使用外部傳入的
                        if (deliveryRequestApiEntity.CustomerCode == null || deliveryRequestApiEntity.CustomerCode.Length == 0)
                        {
                            tcDeliveryRequest.CustomerCode = customCode;
                        }



                        tbCustomers_DA _tbCustomers = new tbCustomers_DA();

                        var CustomerProductType = new tbCustomers_Condition();

                        CustomerProductType = _tbCustomers.GetCustomerProductType(customCode);

                        if (CustomerProductType != null)
                        {
                            if (CustomerProductType.product_type == 1)
                            {
                                if (deliveryRequestApiEntity.Pieces > 1)
                                {
                                    if (CustomerProductType.is_new_customer == true)
                                    {
                                        tcDeliveryRequest.ProductId = "CM000030";
                                    }
                                    else
                                    {
                                        tcDeliveryRequest.ProductId = "CM000036";
                                    }

                                    if (deliveryRequestApiEntity.CbmSize == "1")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S060";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "2")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S090";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "6")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S110";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "3")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S120";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "4")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S150";
                                    }

                                }
                                else
                                {
                                    if (CustomerProductType.is_new_customer == true)
                                    {
                                        tcDeliveryRequest.ProductId = "CM000030";
                                    }
                                    else
                                    {
                                        tcDeliveryRequest.ProductId = "CS000035";
                                    }

                                    if (deliveryRequestApiEntity.CbmSize == "1")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S060";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "2")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S090";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "6")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S110";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "3")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S120";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "4")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S150";
                                    }
                                }
                            }
                            else if (CustomerProductType.product_type == 2)
                            {
                                tcDeliveryRequest.ProductId = "PS000031";
                                tcDeliveryRequest.SpecCodeId = "B003";
                            }
                            else if (CustomerProductType.product_type == 3)
                            {
                                tcDeliveryRequest.ProductId = "PS000034";
                                tcDeliveryRequest.SpecCodeId = "X001";

                            }
                        }


                        if (CustomerProductType != null)
                        {
                            if ((CustomerProductType.product_type == 2 || CustomerProductType.product_type == 3) & receiveAddressinfoStationCode == "95")
                            {
                                eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                                eDIResultEntity.Result = false;
                                eDIResultEntity.Msg = "非預購袋可配送區域";


                                //記錄開單失敗訊息

                                ShopeeException _ShopeeException = new ShopeeException
                                {
                                    exception = "非預購袋可配送區域",
                                    cdate = DateTime.Now,
                                    json = getJson,
                                    order_number = deliveryRequestApiEntity.OrderNumber,
                                    send_address = deliveryRequestApiEntity.SenderAddress,
                                    receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                    customer_code = deliveryRequestApiEntity.CustomerCode,
                                    start_time = start_time,
                                    end_time = DateTime.Now,

                                };

                                ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                                _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                                eDIResultEntities.Add(eDIResultEntity);
                                continue;





                            }
                        }

                        //取得收件地址
                        //FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(deliveryRequestApiEntity.ReceiverAddress);

                        //原有的拆解地址
                        var originReceiverCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.ReceiverAddress);

                        AddressParsingInfo receiverCity = new AddressParsingInfo();

                        receiverCity.City = receiveAddressinfoCity;
                        receiverCity.Town = receiveAddressinfoTown;
                        receiverCity.Road = receiveAddressinfoRoad;
                        receiverCity.StationCode = receiveAddressinfoStationCode;
                        receiverCity.PostZip3 = receiveAddressinfoPostZip3;
                        receiverCity.StackCode = receiveAddressinfoStackCode;
                        receiverCity.MotorcycleDriverCode = receiveAddressinfoMotorcycleDriverCode;
                        receiverCity.SalesDriverCode = receiveAddressinfoSalesDriverCode;
                        receiverCity.StationName = receiveAddressinfoStationName;


                        tcDeliveryRequest.ReceiveCity = originReceiverCity.city;
                        tcDeliveryRequest.ReceiveArea = originReceiverCity.area;

                        string realReceiveAddress = deliveryRequestApiEntity.ReceiverAddress;

                        if (originReceiverCity.city.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(originReceiverCity.city, "");
                        }

                        if (originReceiverCity.area.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(originReceiverCity.area, "");
                        }

                        tcDeliveryRequest.ReceiveAddress = realReceiveAddress;


                        //tcDeliveryRequest.ReceiveCity = originReceiverCity.city;
                        //tcDeliveryRequest.ReceiveArea = originReceiverCity.area;


                        //if (receiverCity.area == "樸子市")
                        //{
                        //    receiverCity.area = "朴子市";
                        //    tcDeliveryRequest.ReceiveArea = "朴子市";
                        //}
                        //else
                        //{
                        //    tcDeliveryRequest.ReceiveArea = receiverCity.area;
                        //}



                        if (string.IsNullOrEmpty(receiverCity.ToString()) || string.IsNullOrEmpty(receiverCity.City) || string.IsNullOrEmpty(receiverCity.Town))
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "無法正確取得收件人的行政區，將無法正確寄送";

                            eDIResultEntities.Add(eDIResultEntity);

                            //記錄開單失敗訊息
                            var exception = eDIResultEntity.Msg;
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = exception,
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now,
                                receiveCity = receiverCity.ToString(),
                                receive_city = receiverCity.City,
                                receive_area = receiverCity.Town

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);

                            continue;
                        }



                        //取得寄件地址
                        //原有的拆解地址
                        var originSendCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.SenderAddress);

                        AddressParsingInfo senderCity = new AddressParsingInfo();

                        senderCity.City = sendAddressInfoCity;
                        senderCity.Town = sendAddressInfo.Town;
                        senderCity.Road = sendAddressInfo.Road;
                        senderCity.StationCode = sendAddressInfo.StationCode;
                        senderCity.PostZip3 = sendAddressInfo.PostZip3;
                        senderCity.StackCode = sendAddressInfo.StackCode;
                        senderCity.MotorcycleDriverCode = sendAddressInfo.MotorcycleDriverCode;
                        senderCity.SalesDriverCode = sendAddressInfo.SalesDriverCode;
                        senderCity.StationName = sendAddressInfo.StationName;

                        tcDeliveryRequest.SendCity = originSendCity.city;
                        tcDeliveryRequest.SendArea = originSendCity.area;

                        string realSendAddress = deliveryRequestApiEntity.SenderAddress;

                        if (originSendCity.city != null && originSendCity.city.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(originSendCity.city, "");
                        }

                        if (originSendCity.area != null && originSendCity.area.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(originSendCity.area, "");
                        }

                        tcDeliveryRequest.SendAddress = realSendAddress;

                        //判斷取件站所(另外要再寫入取件相關表)
                        FSEAddressEntity fSEAddressEntitySend = new FSEAddressEntity(deliveryRequestApiEntity.SenderAddress);
                        //原拆解地址
                        var sendDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntitySend);

                        bool bNotSend = true;

                        if (deliveryRequestApiEntity.SenderAddress == "桃園市龜山區航空城JUNFU物流倉")
                        {
                            bNotSend = false;
                        }

                        //OrgArea orgAreaSend = new OrgArea();

                        //if (sendDatas.Count > 0)
                        //{
                        //    foreach (OrgArea orgArea in sendDatas)
                        //    {
                        //        //無站所或聯運區的排除
                        //        if (orgArea.StationScode != null && orgArea.StationScode != "99" && orgArea.StationScode != "95")
                        //        {
                        //            orgAreaSend = orgArea;
                        //            bNotSend = false;
                        //            break;
                        //        }
                        //    }
                        //}

                        if (senderCity.StationCode != null && senderCity.StationCode != "99" && senderCity.StationCode != "95")
                        {
                            //orgAreaSend = orgArea;
                            bNotSend = false;
                            //break;
                        }

                        if (bNotSend && addPickUpRequest == true)
                        {

                            //記錄開單失敗訊息
                            var exception = "寄件地址不在服務範圍內！";
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = exception,
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                            throw new Exception(exception);

                        }

                        //取得托運單
                        //原有拆地址
                        //var distributor = this.DeliveryRequestModifyRepository.GetDistributor(originReceiverCity.city, originReceiverCity.area);

                        //distributor.supplier_code = "F" + senderCity.StationCode;
                        //distributor.supplier_name = senderCity.StationName;
                        //distributor.area_arrive_code = receiverCity.StationCode;


                        if (!isShopee)
                        {
                            tcDeliveryRequest.SupplierCode = customer.SupplierCode;
                        }
                        else if (("F" + senderCity.StationCode) != "*9聯運")
                        {
                            tcDeliveryRequest.SupplierCode = "F" + senderCity.StationCode;
                        }

                        tcDeliveryRequest.AreaArriveCode = receiverCity.StationCode;

                        eDIResultEntity.Area = receiverCity.StationName;
                        eDIResultEntity.AreaId = receiverCity.StationCode;

                        if (isShopee)
                        {
                            //原有拆地址
                            //var station = this.DeliveryRequestModifyRepository.GetDistributor(sendCity.city, sendCity.area);

                            tcDeliveryRequest.SendStationScode = senderCity.StationCode;

                            if (tcDeliveryRequest.CbmSize != null)
                            {
                                TcCbmSize cbmSize = cbmSizes.Where(s => s.CbmId == tcDeliveryRequest.CbmSize.ToString()).FirstOrDefault();
                                if (cbmSize != null)
                                {
                                    tcDeliveryRequest.CbmLength = cbmSize.DefaultCbmLength;
                                    tcDeliveryRequest.CbmWidth = cbmSize.DefaultCbmWidth;
                                    tcDeliveryRequest.CbmHeight = cbmSize.DefaultCbmHeight;
                                    tcDeliveryRequest.CbmWeight = cbmSize.DefaultCbmWeight;
                                    tcDeliveryRequest.CbmCont = cbmSize.DefaultCbmCont;
                                }
                            }
                        }
                        else
                        {
                            tcDeliveryRequest.SendStationScode = customer.SupplierCode.Replace("F", "");
                        }
                        //非蝦皮 預設貨品尺寸
                        //if (!isShopee && tcDeliveryRequest.CbmSize != null)
                        //{
                        //    TcCbmSize cbmSize = cbmSizes.Where(s => s.CbmId == tcDeliveryRequest.CbmSize.ToString()).FirstOrDefault();
                        //    if (cbmSize != null)
                        //    {
                        //        tcDeliveryRequest.CbmLength = cbmSize.DefaultCbmLength;
                        //        tcDeliveryRequest.CbmWidth = cbmSize.DefaultCbmWidth;
                        //        tcDeliveryRequest.CbmHeight = cbmSize.DefaultCbmHeight;
                        //        tcDeliveryRequest.CbmWeight = cbmSize.DefaultCbmWeight;
                        //        tcDeliveryRequest.CbmCont = cbmSize.DefaultCbmCont;
                        //    }
                        //}
                        //TcDeliveryRequest returnDeliveryRequest;

                        //加入特服區ID
                        try
                        {
                            tcDeliveryRequest.SpecialAreaId = Convert.ToInt32(receiveAddressinfoSpecialAreaId);
                        }
                        catch (Exception)
                        {

                        }



                        //加入特服區費用
                        tcDeliveryRequest.SpecialAreaFee = receiveAddressinfoSpecialAreaFee;

                        //加入接泊碼
                        tcDeliveryRequest.ShuttleStationCode = receiveAddressinfoShuttleStationCode == null ? "" : receiveAddressinfoShuttleStationCode;
                        //tcDeliveryRequest.ShuttleStationCode = receiveAddressinfo.ShuttleStationCode;


                        //FOR行動派遣
                        tcDeliveryRequest.SendCode = sendAddressInfoStationCode == null ? "" : sendAddressInfoStationCode;
                        tcDeliveryRequest.SendMD = sendAddressInfoMotorcycleDriverCode == null ? "" : sendAddressInfoMotorcycleDriverCode;
                        tcDeliveryRequest.SendSD = sendAddressInfoSalesDriverCode == null ? "" : sendAddressInfoSalesDriverCode;
                        tcDeliveryRequest.ReceiveCode = receiveAddressinfoStationCode == null ? "" : receiveAddressinfoStationCode;
                        tcDeliveryRequest.ReceiveMD = receiveAddressinfoMotorcycleDriverCode == null ? "" : receiveAddressinfoMotorcycleDriverCode;
                        tcDeliveryRequest.ReceiveSD = receiveAddressinfoSalesDriverCode == null ? "" : receiveAddressinfoSalesDriverCode;


                        //如果是蝦皮，先查是否已有重復的訂單
                        if (isShopee && deliveryRequestApiEntity.OrderNumber != null && deliveryRequestApiEntity.OrderNumber.Length > 0)
                        {
                            //先驗證是否有重復訂單
                            var checkDeliveryRequest = this.DeliveryRequestModifyRepository.GetTcDeliveryRequestByCustomerCodeAndOrderNumber(customCode, deliveryRequestApiEntity.OrderNumber);

                            if (checkDeliveryRequest != null)
                            {
                                //查證資料是否集貨
                                var myScanLog = this.DeliveryScanLogRepository.GetByCheckNumber(checkDeliveryRequest.CheckNumber);

                                if (myScanLog == null || myScanLog.Count == 0)
                                {
                                    returnDeliveryRequest = checkDeliveryRequest;
                                    returnDeliveryRequest.ReceiveCity = tcDeliveryRequest.ReceiveCity;
                                    returnDeliveryRequest.ReceiveArea = tcDeliveryRequest.ReceiveArea;
                                    returnDeliveryRequest.ReceiveAddress = tcDeliveryRequest.ReceiveAddress;
                                    returnDeliveryRequest.SendCity = tcDeliveryRequest.SendCity;
                                    returnDeliveryRequest.SendArea = tcDeliveryRequest.SendArea;
                                    returnDeliveryRequest.SendAddress = tcDeliveryRequest.SendAddress;
                                    returnDeliveryRequest.SupplierCode = tcDeliveryRequest.SupplierCode;
                                    returnDeliveryRequest.AreaArriveCode = tcDeliveryRequest.AreaArriveCode;
                                    returnDeliveryRequest.SendStationScode = tcDeliveryRequest.SendStationScode;
                                    returnDeliveryRequest.CbmSize = tcDeliveryRequest.CbmSize;
                                    returnDeliveryRequest.CbmLength = tcDeliveryRequest.CbmLength;
                                    returnDeliveryRequest.CbmWidth = tcDeliveryRequest.CbmWidth;
                                    returnDeliveryRequest.CbmHeight = tcDeliveryRequest.CbmHeight;
                                    returnDeliveryRequest.CbmWeight = tcDeliveryRequest.CbmWeight;
                                    returnDeliveryRequest.CbmCont = tcDeliveryRequest.CbmCont;
                                }
                                else
                                {

                                    //記錄開單失敗訊息
                                    var exception = "此訂單編號已存在並取件";
                                    ShopeeException _ShopeeException = new ShopeeException
                                    {
                                        exception = exception,
                                        cdate = DateTime.Now,
                                        json = getJson

                                    };

                                    ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                                    _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                                    throw new Exception(exception);

                                }
                            }
                            else
                            {
                                returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                            }
                        }
                        else if (isShopee)
                        {

                            //記錄開單失敗訊息
                            var exception = "無訂單編號";
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = exception,
                                cdate = DateTime.Now,
                                json = getJson
                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                            throw new Exception(exception);

                        }
                        else
                        {
                            returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                        }

                        //計算託運單費用
                        var shipFeeInfo = this.DeliveryRequestModifyRepository.GetShipFee(Convert.ToInt64(returnDeliveryRequest.RequestId));

                        returnDeliveryRequest.SupplierFee = shipFeeInfo.supplier_fee;
                        returnDeliveryRequest.TotalFee = shipFeeInfo.supplier_fee;

                        // 預購袋扣掉貨號數量
                        if (customer.ProductType == 2 || customer.ProductType == 3)
                        {
                            //JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
                            //webServiceSoapClient.UpdateCheckNumberSettingAsync(customCode, 1);

                            //CustomerChecknumberSetting customerChecknumberSetting = new CustomerChecknumberSetting
                            //{

                            //    CustomerCode = deliveryRequestApiEntity.CustomerCode,
                            //    UsedCount = CustomerChecknumberSettingRepository.GetUsedCount(deliveryRequestApiEntity.CustomerCode) + 1
                            //};

                            int i = Int32.Parse(CustomerChecknumberSettingRepository.GetRemainCount(deliveryRequestApiEntity.CustomerCode));

                            if (i < 1) { throw new Exception("預購袋/超值箱數量不足"); }


                            var info = CustomerChecknumberSettingRepository.GetUsedCount(deliveryRequestApiEntity.CustomerCode);

                            if (info != null)
                            {
                                info.UsedCount = info.UsedCount.Value + 1;
                            }

                            CustomerChecknumberSettingRepository.UpdateUsedCount(info);




                        }

                        //如果沒使用區間，主動產生check_number
                        if (deliveryRequestApiEntity.CheckNumber.Length == 0 && (returnDeliveryRequest.CheckNumber == null || returnDeliveryRequest.CheckNumber.Length == 0))
                        {
                            long checkNumberTemp = 100000000 + Convert.ToInt64(returnDeliveryRequest.RequestId);
                            int tail = Convert.ToInt32(returnDeliveryRequest.RequestId % 7);

                            string checkNumberPrefix = "";
                            if (isShopee)
                            {
                                checkNumberPrefix = "700";
                            }
                            else
                            {
                                checkNumberPrefix = CheckNumberPreheadRepository.GetPrefixByProductType(customer.ProductType.ToString());
                            }

                            string currentCheckNumber = string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());

                            returnDeliveryRequest.CheckNumber = currentCheckNumber;

                            eDIResultEntity.CheckNumber = currentCheckNumber;
                        }
                        else if (returnDeliveryRequest.CheckNumber != null && returnDeliveryRequest.CheckNumber.Length > 0)
                        {
                            eDIResultEntity.CheckNumber = returnDeliveryRequest.CheckNumber;
                        }

                        //計算sdmd資訊
                        //FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(returnDeliveryRequest.ReceiveAddress, returnDeliveryRequest.ReceiveCity, returnDeliveryRequest.ReceiveArea);
                        //原有拆SDMD
                        //var eachDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntity);

                        if (receiverCity != null)
                        {
                            CheckNumberSdMapping checkNumberSdMapping = new CheckNumberSdMapping
                            {
                                CheckNumber = returnDeliveryRequest.CheckNumber,
                                RequestId = (long)returnDeliveryRequest.RequestId,
                                OrgAreaId = Convert.ToInt32(receiverCity.StationCode),
                                Md = receiverCity.MotorcycleDriverCode,
                                Sd = receiverCity.SalesDriverCode,
                                PutOrder = receiverCity.StackCode,
                                Cdate = DateTime.Now,
                                Udate = DateTime.Now
                            };

                            eDIResultEntity.Md = checkNumberSdMapping.Md;
                            eDIResultEntity.Sd = checkNumberSdMapping.Sd;
                            eDIResultEntity.Putorder = checkNumberSdMapping.PutOrder;
                            eDIResultEntity.Area = receiverCity.StationName;
                            eDIResultEntity.AreaId = receiverCity.StationCode;
                            eDIResultEntity.ShuttleStationCode = tcDeliveryRequest.ShuttleStationCode;

                            this.CheckNumberSDMappingRepository.Insert(checkNumberSdMapping);

                            //if (CheckOrderNumberData != null && !string.IsNullOrEmpty(CheckOrderNumberData.CheckNumber))
                            //{

                            //}
                            //else
                            //{

                            //}

                            //站所資訊
                            //var stationCode = this.StationRepository.GetStationCodeByStationScode(eachDatas[0].StationScode);
                            var stationCode = customer.SupplierCode;
                            returnDeliveryRequest.SupplierCode = stationCode;
                            returnDeliveryRequest.ReceiveZip = receiverCity.PostZip3;
                        }


                        for (int i = 1; i < 10; i++)
                        {
                            try
                            {
                                ShoppeeApiProblem _ShoppeeApiProblem = new ShoppeeApiProblem
                                {
                                    CheckNumber = returnDeliveryRequest.CheckNumber,
                                    OrderNumber = returnDeliveryRequest.OrderNumber,
                                    step = i,
                                    Type = "1",
                                    Cdate = DateTime.Now,

                                };

                                ShoppeeApiProblem_DA _ShoppeeApiProblem_DA = new ShoppeeApiProblem_DA();
                                _ShoppeeApiProblem_DA.InsertShoppeeApiProblem(_ShoppeeApiProblem);


                                //TcDeliveryRequest _tcDeliveryRequest = new TcDeliveryRequest
                                //{
                                //    CheckNumber = returnDeliveryRequest.CheckNumber
                                //};
                                //tcDeliveryRequests_DA _TcDeliveryRequest_DA = new tcDeliveryRequests_DA();
                                //_TcDeliveryRequest_DA.Update_checknumber(returnDeliveryRequest.CheckNumber, returnDeliveryRequest.RequestId.ToString());

                                //存檔
                                this.DeliveryRequestModifyRepository.Update(returnDeliveryRequest);

                                break;
                            }
                            catch (Exception e)
                            {

                                Thread.Sleep(100);

                                ShoppeeApiProblem _ShoppeeApiProblem = new ShoppeeApiProblem
                                {
                                    CheckNumber = returnDeliveryRequest.CheckNumber,
                                    OrderNumber = returnDeliveryRequest.OrderNumber,
                                    step = i,
                                    Exception = e.ToString(),
                                    Type = "2",
                                    Cdate = DateTime.Now,

                                };

                                ShoppeeApiProblem_DA _ShoppeeApiProblem_DA = new ShoppeeApiProblem_DA();
                                _ShoppeeApiProblem_DA.InsertShoppeeApiProblem(_ShoppeeApiProblem);

                            }

                        }


                        Log("save", JsonConvert.SerializeObject(returnDeliveryRequest).ToString(), returnDeliveryRequest.CustomerCode, returnDeliveryRequest.CheckNumber, "true");
                        EDI_api_new_data_log_DA _Log_DA = new EDI_api_new_data_log_DA();
                        _Log_DA.InsertAPIorigin(returnDeliveryRequest.CustomerCode, "GZ_NEW_API");
                        //記全部的log
                        ApiAddressParseLog(start_time, returnDeliveryRequest, getJson);


                        if (addPickUpRequest)
                        {
                            //產生取貨資料
                            if (isShopee)
                            {
                                PickupRequestForApiuser pickupRequestForApiuser = new PickupRequestForApiuser
                                {
                                    CheckNumber = returnDeliveryRequest.CheckNumber,
                                    CustomerCode = returnDeliveryRequest.CustomerCode,
                                    Md = senderCity.MotorcycleDriverCode,
                                    ReassignMd = senderCity.MotorcycleDriverCode,
                                    Pieces = (returnDeliveryRequest.Pieces == null || returnDeliveryRequest.Pieces == 0) ? returnDeliveryRequest.Plates : returnDeliveryRequest.Pieces,
                                    Putorder = senderCity.StackCode,
                                    RequestDate = DateTime.Now,
                                    Sd = senderCity.SalesDriverCode,
                                    SendArea = originSendCity.area,
                                    SendCity = originSendCity.city,
                                    SendRoad = deliveryRequestApiEntity.SenderAddress,
                                    SendTel = returnDeliveryRequest.SendTel,
                                    SupplierCode = "F" + senderCity.StationCode
                                    //SupplierCode = this.StationRepository.GetStationCodeByStationScode(orgAreaSend.StationScode)
                                };


                                for (int i = 1; i < 10; i++)
                                {
                                    try
                                    {
                                        ShoppeeApiProblem _ShoppeeApiProblem = new ShoppeeApiProblem
                                        {
                                            CheckNumber = returnDeliveryRequest.CheckNumber,
                                            OrderNumber = returnDeliveryRequest.OrderNumber,
                                            step = i,
                                            Type = "3",
                                            Cdate = DateTime.Now,

                                        };

                                        ShoppeeApiProblem_DA _ShoppeeApiProblem_DA = new ShoppeeApiProblem_DA();
                                        _ShoppeeApiProblem_DA.InsertShoppeeApiProblem(_ShoppeeApiProblem);

                                        this.PickupRequestForApiuserRepository.Insert(pickupRequestForApiuser);

                                        break;
                                    }
                                    catch (Exception e)
                                    {

                                        Thread.Sleep(100);

                                        ShoppeeApiProblem _ShoppeeApiProblem = new ShoppeeApiProblem
                                        {
                                            CheckNumber = returnDeliveryRequest.CheckNumber,
                                            OrderNumber = returnDeliveryRequest.OrderNumber,
                                            step = i,
                                            Exception = e.ToString(),
                                            Type = "4",
                                            Cdate = DateTime.Now,

                                        };

                                        ShoppeeApiProblem_DA _ShoppeeApiProblem_DA = new ShoppeeApiProblem_DA();
                                        _ShoppeeApiProblem_DA.InsertShoppeeApiProblem(_ShoppeeApiProblem);

                                    }
                                }

                            }
                        }

                        //寫入結算計算材積的表格
                        string CBM = string.Empty;
                        try
                        {
                            //CustomerProductType = _tbCustomers.GetCustomerProductType(customCode);

                            if (CustomerProductType != null)
                            {
                                if (CustomerProductType.product_type == 1)
                                {
                                    if (deliveryRequestApiEntity.CbmSize == "1")
                                    {
                                        CBM = "S060";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "2")
                                    {
                                        CBM = "S090";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "6")
                                    {
                                        CBM = "S110";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "3")
                                    {
                                        CBM = "S120";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "4")
                                    {
                                        CBM = "S150";
                                    }
                                    else
                                    {
                                        CBM = "S090";
                                    }
                                }
                                else if (CustomerProductType.product_type == 2)
                                {
                                    CBM = "B003";
                                }
                                else if (CustomerProductType.product_type == 3)
                                {
                                    CBM = "X003";
                                }
                            }

                            CBMDetailLog_DA _CBMDetailLog_DA = new CBMDetailLog_DA();

                            CBMDetailLog_Condition CBMDetailLogEnity = new CBMDetailLog_Condition
                            {
                                ComeFrom = "1",
                                CheckNumber = returnDeliveryRequest.CheckNumber,
                                CBM = CBM == null ? "S090" : CBM,
                                //Length,
                                //Width,
                                //Height,
                                CreateDate = DateTime.Now,
                                CreateUser = returnDeliveryRequest.CustomerCode
                            };

                            _CBMDetailLog_DA.InsertCBMDetailLog(CBMDetailLogEnity);
                        }
                        catch (Exception)
                        {


                        };


                    }
                    catch (Exception ex)
                    {
                        ////eDIResultEntity.Result = false;
                        eDIResultEntity.Msg = ex.Message.ToString();

                        //記錄開單失敗訊息
                        ApiAddressParseLog(start_time, ex, deliveryRequestApiEntity, returnDeliveryRequest, getJson);

                        if (ex.Message.Contains("傳遞到 LEFT 或 SUBSTRING 函數的參數長度無效"))
                        {

                            //記錄開單失敗訊息
                            ApiAddressParseLog(start_time, ex, deliveryRequestApiEntity, returnDeliveryRequest, getJson);

                            //記錄開單失敗訊息

                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = ex.ToString(),
                                cdate = DateTime.Now,
                                json = getJson

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);
                        }
                    }



                    eDIResultEntities.Add(eDIResultEntity);
                }
            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.Message.ToString()
                };

                //記錄開單失敗訊息

                ShopeeException _ShopeeException = new ShopeeException
                {
                    exception = ex.Message.ToString(),
                    cdate = DateTime.Now,
                    json = getJson

                };

                ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                _ShopeeException_DA.InsertShopeeException(_ShopeeException);



                eDIResultEntities.Add(eDIResultEntity);
            }


            Log("respond", JsonConvert.SerializeObject(eDIResultEntities).ToString(), customCode, "", eDIResultEntities[0].Result.ToString());
            Log("respond", "end", "", "", "");

            return eDIResultEntities;
        }

        private List<EDIResultEntity> EDIAPI_OLD(string token, string getJson, bool addPickUpRequest = true)
        {
            Log("request", "start", "", "", "");
            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();



            //先驗證token
            try
            {
                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                var payload = JsonConvert.DeserializeObject<PayloadEntity>(json);

                string customerCode = payload.info.account_code;

                //28800是utc
                Int64 nowSpan = Convert.ToInt64((DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds) - 28800 - 600;
                if (payload.exp < nowSpan)
                {
                    var exception = "token expire!";

                    throw new Exception(exception);
                }

                string stopShippingCode = CustomerRepository.GetByCustomerCode(customerCode).StopShippingCode;
                if (stopShippingCode != "0")
                {
                    var exception = "the account code is not active";

                    throw new Exception(exception);
                }

                Log("token", token, "", "", "");
                Log("getjson", getJson, "", "", "");
                Log("request", "end", "", "", "");

                var insertEDI = EDIAPIJsonInsert_OLD(getJson, customerCode, addPickUpRequest);

                eDIResultEntities.AddRange(insertEDI);
            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.Message
                };

                //記錄開單失敗訊息

                ShopeeException _ShopeeException = new ShopeeException
                {
                    exception = ex.Message,
                    cdate = DateTime.Now,
                    json = getJson

                };

                ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                _ShopeeException_DA.InsertShopeeException(_ShopeeException);

                var json = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                var payload = JsonConvert.DeserializeObject<PayloadEntity>(json);
                string customerCode = payload.info.account_code;

                Log("errorMsg", ex.Message.ToString(), customerCode, "", "false");
                Log("request", "end", "", "", "");
                eDIResultEntities.Add(eDIResultEntity);
            }

            return eDIResultEntities;
        }

        private List<EDIResultEntity> EDIAPIJsonInsert_OLD(string getJson, string customCode = "", bool addPickUpRequest = true)
        {
            Log("respond", "start", "", "", "");
            bool isShopee = customCode == "F1300600002" || customCode == "F2900210002" ? true : false;      //蝦皮與光年甲配都視為蝦皮

            List<EDIResultEntity> eDIResultEntities = new List<EDIResultEntity>();

            TcCbmSize[] cbmSizes = CbmSizeRepository.GetAllCbmSize();


            try
            {
                var deliveryRequestApiEntities = JsonConvert.DeserializeObject<List<DeliveryRequestApiEntity>>(getJson);


                foreach (DeliveryRequestApiEntity deliveryRequestApiEntity in deliveryRequestApiEntities)
                {
                    DateTime start_time = DateTime.Now;

                    EDIResultEntity eDIResultEntity = new EDIResultEntity();

                    eDIResultEntity.Result = true;


                    TcDeliveryRequest returnDeliveryRequest = new TcDeliveryRequest();

                    //重複訂單編號會回傳原來貨號
                    //TcDeliveryRequest CheckOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);

                    //TcDeliveryRequest CheckOrderNumberData = new TcDeliveryRequest();

                    //if (!string.IsNullOrEmpty(deliveryRequestApiEntity.OrderNumber))
                    //{
                    //    CheckOrderNumberData = this.DeliveryRequestModifyRepository.GetDataByOrderNumber(deliveryRequestApiEntity.OrderNumber, deliveryRequestApiEntity.CustomerCode);
                    //}


                    try
                    {

                        if (customCode.Length > 0 && deliveryRequestApiEntity.CustomerCode.Length > 0 && deliveryRequestApiEntity.CustomerCode != customCode)
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "客戶代碼不一致";


                            //記錄開單失敗訊息

                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = eDIResultEntity.Msg,
                                cdate = DateTime.Now,
                                json = getJson

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                            eDIResultEntities.Add(eDIResultEntity);
                            continue;
                        }

                        //文件pieces誤寫為plates
                        deliveryRequestApiEntity.Pieces = deliveryRequestApiEntity.Plates;

                        deliveryRequestApiEntity.Plates = 0;

                        //先轉為tcDeliveryRequestApiEntity
                        var tcDeliveryRequest = this.Mapper.Map<TcDeliveryRequest>(deliveryRequestApiEntity);

                        var customer = this.CustomerRepository.GetByCustomerCode(tcDeliveryRequest.CustomerCode);

                        eDIResultEntity.OrderNumber = tcDeliveryRequest.OrderNumber;

                        //無代碼代碼時，使用外部傳入的
                        if (deliveryRequestApiEntity.CustomerCode == null || deliveryRequestApiEntity.CustomerCode.Length == 0)
                        {
                            tcDeliveryRequest.CustomerCode = customCode;
                        }


                        //tbCustomers_DA _tbCustomers = new tbCustomers_DA();

                        //var CustomerProductType = new tbCustomers_Condition();

                        //CustomerProductType = _tbCustomers.GetCustomerProductType(customCode);

                        //if (CustomerProductType != null)
                        //{
                        //    if (CustomerProductType.product_type == 1)
                        //    {
                        //        if (deliveryRequestApiEntity.Pieces > 1)
                        //        {
                        //            tcDeliveryRequest.ProductId = "CM000036";
                        //            if (deliveryRequestApiEntity.CbmSize == "1")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S060";
                        //            }
                        //            else if (deliveryRequestApiEntity.CbmSize == "2")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S090";
                        //            }
                        //            else if (deliveryRequestApiEntity.CbmSize == "6")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S110";
                        //            }
                        //            else if (deliveryRequestApiEntity.CbmSize == "3")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S120";
                        //            }
                        //            else if (deliveryRequestApiEntity.CbmSize == "4")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S150";
                        //            }

                        //        }
                        //        else
                        //        {
                        //            tcDeliveryRequest.ProductId = "CS000035";
                        //            if (deliveryRequestApiEntity.CbmSize == "1")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S060";
                        //            }
                        //            else if (deliveryRequestApiEntity.CbmSize == "2")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S090";
                        //            }
                        //            else if (deliveryRequestApiEntity.CbmSize == "6")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S110";
                        //            }
                        //            else if (deliveryRequestApiEntity.CbmSize == "3")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S120";
                        //            }
                        //            else if (deliveryRequestApiEntity.CbmSize == "4")
                        //            {
                        //                tcDeliveryRequest.SpecCodeId = "S150";
                        //            }
                        //        }
                        //    }
                        //    else if (CustomerProductType.product_type == 2)
                        //    {
                        //        tcDeliveryRequest.ProductId = "PS000031";
                        //        tcDeliveryRequest.SpecCodeId = "B003";
                        //    }
                        //    else if (CustomerProductType.product_type == 3)
                        //    {
                        //        tcDeliveryRequest.ProductId = "PS000034";
                        //        tcDeliveryRequest.SpecCodeId = "X001";

                        //    }
                        //}

                        tbCustomers_DA _tbCustomers = new tbCustomers_DA();

                        var CustomerProductType = new tbCustomers_Condition();

                        CustomerProductType = _tbCustomers.GetCustomerProductType(customCode);

                        if (CustomerProductType != null)
                        {
                            if (CustomerProductType.product_type == 1)
                            {
                                if (deliveryRequestApiEntity.Pieces > 1)
                                {
                                    if (CustomerProductType.is_new_customer == true)
                                    {
                                        tcDeliveryRequest.ProductId = "CM000030";
                                    }
                                    else
                                    {
                                        tcDeliveryRequest.ProductId = "CM000036";
                                    }

                                    if (deliveryRequestApiEntity.CbmSize == "1")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S060";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "2")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S090";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "6")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S110";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "3")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S120";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "4")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S150";
                                    }

                                }
                                else
                                {
                                    if (CustomerProductType.is_new_customer == true)
                                    {
                                        tcDeliveryRequest.ProductId = "CM000030";
                                    }
                                    else
                                    {
                                        tcDeliveryRequest.ProductId = "CS000035";
                                    }

                                    if (deliveryRequestApiEntity.CbmSize == "1")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S060";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "2")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S090";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "6")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S110";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "3")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S120";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "4")
                                    {
                                        tcDeliveryRequest.SpecCodeId = "S150";
                                    }
                                }
                            }
                            else if (CustomerProductType.product_type == 2)
                            {
                                tcDeliveryRequest.ProductId = "PS000031";
                                tcDeliveryRequest.SpecCodeId = "B003";
                            }
                            else if (CustomerProductType.product_type == 3)
                            {
                                tcDeliveryRequest.ProductId = "PS000034";
                                tcDeliveryRequest.SpecCodeId = "X001";

                            }
                        }




                        //取得收件地址
                        //FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(deliveryRequestApiEntity.ReceiverAddress);
                        var receiverCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.ReceiverAddress);



                        tcDeliveryRequest.ReceiveCity = receiverCity.city;

                        if (receiverCity.area == "樸子市")
                        {
                            receiverCity.area = "朴子市";
                            tcDeliveryRequest.ReceiveArea = "朴子市";
                        }
                        else
                        {
                            tcDeliveryRequest.ReceiveArea = receiverCity.area;
                        }



                        string realReceiveAddress = deliveryRequestApiEntity.ReceiverAddress;

                        if (string.IsNullOrEmpty(receiverCity.ToString()) || string.IsNullOrEmpty(receiverCity.city) || string.IsNullOrEmpty(receiverCity.area))
                        {
                            eDIResultEntity.OrderNumber = deliveryRequestApiEntity.OrderNumber;
                            eDIResultEntity.Result = false;
                            eDIResultEntity.Msg = "無法正確取得收件人的行政區，將無法正確寄送";

                            eDIResultEntities.Add(eDIResultEntity);

                            //記錄開單失敗訊息
                            var exception = eDIResultEntity.Msg;
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = exception,
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now,
                                receiveCity = receiverCity.ToString(),
                                receive_city = receiverCity.city,
                                receive_area = receiverCity.area


                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);





                            continue;

                        }

                        if (receiverCity.city.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(receiverCity.city, "");
                        }

                        if (receiverCity.area.Length > 0)
                        {
                            realReceiveAddress = realReceiveAddress.Replace(receiverCity.area, "");
                        }

                        tcDeliveryRequest.ReceiveAddress = realReceiveAddress;

                        //取得寄件地址
                        var sendCity = this.DeliveryRequestModifyRepository.GetAddressCityInfo(deliveryRequestApiEntity.SenderAddress);

                        tcDeliveryRequest.SendCity = sendCity.city;
                        tcDeliveryRequest.SendArea = sendCity.area;

                        string realSendAddress = deliveryRequestApiEntity.SenderAddress;

                        if (sendCity.city != null && sendCity.city.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(sendCity.city, "");
                        }

                        if (sendCity.area != null && sendCity.area.Length > 0)
                        {
                            realSendAddress = realSendAddress.Replace(sendCity.area, "");
                        }

                        tcDeliveryRequest.SendAddress = realSendAddress;

                        //判斷取件站所(另外要再寫入取件相關表)
                        FSEAddressEntity fSEAddressEntitySend = new FSEAddressEntity(deliveryRequestApiEntity.SenderAddress);

                        var sendDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntitySend);

                        bool bNotSend = true;

                        if (fSEAddressEntitySend.OrgAddress == "桃園市龜山區航空城JUNFU物流倉")
                        {
                            bNotSend = false;
                        }

                        OrgArea orgAreaSend = new OrgArea();

                        if (sendDatas.Count > 0)
                        {
                            foreach (OrgArea orgArea in sendDatas)
                            {
                                //無站所或聯運區的排除
                                if (orgArea.StationScode != null && orgArea.StationScode != "99" && orgArea.StationScode != "95" && orgArea.StationScode != "91" && orgArea.StationScode != "92")
                                {
                                    orgAreaSend = orgArea;
                                    bNotSend = false;
                                    break;
                                }
                            }
                        }

                        if (bNotSend && addPickUpRequest == true)
                        {

                            //記錄開單失敗訊息
                            var exception = "寄件地址不在服務範圍內！";
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = exception,
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                //send_station = senderCity.StationCode,
                                //delivery_station = receiverCity.StationCode
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                            throw new Exception(exception);

                        }

                        //取得托運單
                        var distributor = this.DeliveryRequestModifyRepository.GetDistributor(receiverCity.city, receiverCity.area);

                        if (!isShopee)
                        {
                            tcDeliveryRequest.SupplierCode = customer.SupplierCode;
                        }
                        else if (distributor.supplier_code != "*9聯運")
                        {
                            tcDeliveryRequest.SupplierCode = distributor.supplier_code;
                        }

                        tcDeliveryRequest.AreaArriveCode = distributor.area_arrive_code;

                        eDIResultEntity.Area = distributor.supplier_name;
                        eDIResultEntity.AreaId = distributor.area_arrive_code;

                        if (isShopee)
                        {
                            var station = this.DeliveryRequestModifyRepository.GetDistributor(sendCity.city, sendCity.area);
                            tcDeliveryRequest.SendStationScode = station.area_arrive_code;
                            if (tcDeliveryRequest.CbmSize != null)
                            {
                                TcCbmSize cbmSize = cbmSizes.Where(s => s.CbmId == tcDeliveryRequest.CbmSize.ToString()).FirstOrDefault();
                                if (cbmSize != null)
                                {
                                    tcDeliveryRequest.CbmLength = cbmSize.DefaultCbmLength;
                                    tcDeliveryRequest.CbmWidth = cbmSize.DefaultCbmWidth;
                                    tcDeliveryRequest.CbmHeight = cbmSize.DefaultCbmHeight;
                                    tcDeliveryRequest.CbmWeight = cbmSize.DefaultCbmWeight;
                                    tcDeliveryRequest.CbmCont = cbmSize.DefaultCbmCont;
                                }
                            }
                        }
                        else
                        {
                            tcDeliveryRequest.SendStationScode = customer.SupplierCode.Replace("F", "");
                        }

                        //先移到try的上面
                        //TcDeliveryRequest returnDeliveryRequest;

                        //代收貨款是否超過2萬
                        if (deliveryRequestApiEntity.CollectionMoney > 20000)
                        {

                            //記錄開單失敗訊息
                            var exception = "代收貨款超過20,000上限，請重新輸入";
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = exception,
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                send_station = returnDeliveryRequest.SendStationScode,
                                delivery_station = returnDeliveryRequest.AreaArriveCode,
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                            throw new Exception(exception);




                        }


                        //計算收件sdmd資訊
                        FSEAddressEntity ReceivefSEAddressEntity = new FSEAddressEntity(tcDeliveryRequest.ReceiveAddress, tcDeliveryRequest.ReceiveCity, tcDeliveryRequest.ReceiveArea);

                        var ReceiveeachDatas = this.OrgAreaRepository.GetByFSEAddressEntity(ReceivefSEAddressEntity);

                        if (ReceiveeachDatas.Count > 0)
                        {
                            CheckNumberSdMapping checkNumberSdMapping = new CheckNumberSdMapping
                            {
                                Md = ReceiveeachDatas[0].MdNo,
                                Sd = ReceiveeachDatas[0].SdNo,
                            };

                            tcDeliveryRequest.ReceiveCode = tcDeliveryRequest.AreaArriveCode == null ? "" : tcDeliveryRequest.AreaArriveCode;
                            tcDeliveryRequest.ReceiveMD = checkNumberSdMapping.Md == null ? "" : checkNumberSdMapping.Md;
                            tcDeliveryRequest.ReceiveSD = checkNumberSdMapping.Sd == null ? "" : checkNumberSdMapping.Sd;

                        }

                        //計算寄件sdmd資訊
                        FSEAddressEntity SendfSEAddressEntity = new FSEAddressEntity(tcDeliveryRequest.SendAddress, tcDeliveryRequest.SendCity, tcDeliveryRequest.SendArea);

                        var SendeachDatas = this.OrgAreaRepository.GetByFSEAddressEntity(SendfSEAddressEntity);

                        if (SendeachDatas.Count > 0)
                        {
                            CheckNumberSdMapping checkNumberSdMapping = new CheckNumberSdMapping
                            {

                                Md = SendeachDatas[0].SendMD,
                                Sd = SendeachDatas[0].SendSD,

                            };

                            tcDeliveryRequest.SendCode = tcDeliveryRequest.SendStationScode == null ? "" : tcDeliveryRequest.SendStationScode;
                            tcDeliveryRequest.SendMD = checkNumberSdMapping.Md == null ? "" : checkNumberSdMapping.Md;
                            tcDeliveryRequest.SendSD = checkNumberSdMapping.Sd == null ? "" : checkNumberSdMapping.Sd;

                        }


                        //如果是蝦皮，先查是否已有重復的訂單
                        if (isShopee && deliveryRequestApiEntity.OrderNumber != null && deliveryRequestApiEntity.OrderNumber.Length > 0)
                        {
                            //先驗證是否有重復訂單
                            var checkDeliveryRequest = this.DeliveryRequestModifyRepository.GetTcDeliveryRequestByCustomerCodeAndOrderNumber(customCode, deliveryRequestApiEntity.OrderNumber);

                            if (checkDeliveryRequest != null)
                            {
                                //查證資料是否集貨
                                var myScanLog = this.DeliveryScanLogRepository.GetByCheckNumber(checkDeliveryRequest.CheckNumber);

                                if (myScanLog == null || myScanLog.Count == 0)
                                {
                                    returnDeliveryRequest = checkDeliveryRequest;
                                    returnDeliveryRequest.ReceiveCity = tcDeliveryRequest.ReceiveCity;
                                    returnDeliveryRequest.ReceiveArea = tcDeliveryRequest.ReceiveArea;
                                    returnDeliveryRequest.ReceiveAddress = tcDeliveryRequest.ReceiveAddress;
                                    returnDeliveryRequest.SendCity = tcDeliveryRequest.SendCity;
                                    returnDeliveryRequest.SendArea = tcDeliveryRequest.SendArea;
                                    returnDeliveryRequest.SendAddress = tcDeliveryRequest.SendAddress;
                                    returnDeliveryRequest.SupplierCode = tcDeliveryRequest.SupplierCode;
                                    returnDeliveryRequest.AreaArriveCode = tcDeliveryRequest.AreaArriveCode;
                                    returnDeliveryRequest.SendStationScode = tcDeliveryRequest.SendStationScode;
                                    returnDeliveryRequest.CbmSize = tcDeliveryRequest.CbmSize;
                                    returnDeliveryRequest.CbmLength = tcDeliveryRequest.CbmLength;
                                    returnDeliveryRequest.CbmWidth = tcDeliveryRequest.CbmWidth;
                                    returnDeliveryRequest.CbmHeight = tcDeliveryRequest.CbmHeight;
                                    returnDeliveryRequest.CbmWeight = tcDeliveryRequest.CbmWeight;
                                    returnDeliveryRequest.CbmCont = tcDeliveryRequest.CbmCont;
                                }
                                else
                                {

                                    //記錄開單失敗訊息
                                    var exception = "此訂單編號已存在並取件";
                                    ShopeeException _ShopeeException = new ShopeeException
                                    {
                                        exception = exception,
                                        cdate = DateTime.Now,
                                        json = getJson

                                    };

                                    ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                                    _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                                    throw new Exception(exception);

                                }
                            }
                            else
                            {
                                returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                            }
                        }
                        else if (isShopee)
                        {

                            //記錄開單失敗訊息
                            var exception = "無訂單編號";
                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = exception,
                                cdate = DateTime.Now,
                                json = getJson
                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);


                            throw new Exception(exception);

                        }
                        else
                        {
                            returnDeliveryRequest = this.DeliveryRequestModifyRepository.Insert(tcDeliveryRequest);
                        }

                        //計算託運單費用
                        var shipFeeInfo = this.DeliveryRequestModifyRepository.GetShipFee(Convert.ToInt64(returnDeliveryRequest.RequestId));

                        returnDeliveryRequest.SupplierFee = shipFeeInfo.supplier_fee;
                        returnDeliveryRequest.TotalFee = shipFeeInfo.supplier_fee;

                        // 預購袋扣掉貨號數量
                        if (customer.ProductType == 2 || customer.ProductType == 3)
                        {
                            //JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
                            //webServiceSoapClient.UpdateCheckNumberSettingAsync(customCode, 1);

                            //CustomerChecknumberSetting customerChecknumberSetting = new CustomerChecknumberSetting
                            //{

                            //    CustomerCode = deliveryRequestApiEntity.CustomerCode,
                            //    UsedCount = CustomerChecknumberSettingRepository.GetUsedCount(deliveryRequestApiEntity.CustomerCode) + 1
                            //};

                            int i = Int32.Parse(CustomerChecknumberSettingRepository.GetRemainCount(deliveryRequestApiEntity.CustomerCode));

                            if (i < 1) { throw new Exception("預購袋/超值箱數量不足"); }


                            var info = CustomerChecknumberSettingRepository.GetUsedCount(deliveryRequestApiEntity.CustomerCode);

                            if (info != null)
                            {
                                info.UsedCount = info.UsedCount.Value + 1;
                            }

                            CustomerChecknumberSettingRepository.UpdateUsedCount(info);




                        }

                        //如果沒使用區間，主動產生check_number
                        if (deliveryRequestApiEntity.CheckNumber.Length == 0 && (returnDeliveryRequest.CheckNumber == null || returnDeliveryRequest.CheckNumber.Length == 0))
                        {
                            long checkNumberTemp = 100000000 + Convert.ToInt64(returnDeliveryRequest.RequestId);
                            int tail = Convert.ToInt32(returnDeliveryRequest.RequestId % 7);

                            string checkNumberPrefix = "";
                            if (isShopee)
                            {
                                checkNumberPrefix = "700";
                            }
                            else
                            {
                                checkNumberPrefix = CheckNumberPreheadRepository.GetPrefixByProductType(customer.ProductType.ToString());
                            }

                            string currentCheckNumber = string.Format("{0}{1}{2}", checkNumberPrefix, checkNumberTemp.ToString().Substring(1), tail.ToString());

                            returnDeliveryRequest.CheckNumber = currentCheckNumber;

                            eDIResultEntity.CheckNumber = currentCheckNumber;
                        }
                        else if (returnDeliveryRequest.CheckNumber != null && returnDeliveryRequest.CheckNumber.Length > 0)
                        {
                            eDIResultEntity.CheckNumber = returnDeliveryRequest.CheckNumber;
                        }

                        //計算sdmd資訊
                        FSEAddressEntity fSEAddressEntity = new FSEAddressEntity(returnDeliveryRequest.ReceiveAddress, returnDeliveryRequest.ReceiveCity, returnDeliveryRequest.ReceiveArea);

                        var eachDatas = this.OrgAreaRepository.GetByFSEAddressEntity(fSEAddressEntity);

                        if (eachDatas.Count > 0)
                        {
                            CheckNumberSdMapping checkNumberSdMapping = new CheckNumberSdMapping
                            {
                                CheckNumber = returnDeliveryRequest.CheckNumber,
                                RequestId = (long)returnDeliveryRequest.RequestId,
                                OrgAreaId = eachDatas[0].Id,
                                Md = eachDatas[0].MdNo,
                                Sd = eachDatas[0].SdNo,
                                PutOrder = eachDatas[0].PutOrder,
                                Cdate = DateTime.Now,
                                Udate = DateTime.Now
                            };

                            eDIResultEntity.Md = checkNumberSdMapping.Md;
                            eDIResultEntity.Sd = checkNumberSdMapping.Sd;
                            eDIResultEntity.Putorder = checkNumberSdMapping.PutOrder;
                            //eDIResultEntity.Area = eachDatas[0].StationName;
                            //eDIResultEntity.AreaId = eachDatas[0].StationScode;

                            this.CheckNumberSDMappingRepository.Insert(checkNumberSdMapping);
                            //if (CheckOrderNumberData != null && !string.IsNullOrEmpty(CheckOrderNumberData.CheckNumber.ToString()))
                            //{

                            //}
                            //else
                            //{

                            //}


                            //站所資訊
                            //var stationCode = this.StationRepository.GetStationCodeByStationScode(eachDatas[0].StationScode);
                            var stationCode = customer.SupplierCode;
                            returnDeliveryRequest.SupplierCode = stationCode;
                            returnDeliveryRequest.ReceiveZip = eachDatas[0].Zipcode;
                        }

                        for (int i = 1; i < 10; i++)
                        {
                            try
                            {
                                ShoppeeApiProblem _ShoppeeApiProblem = new ShoppeeApiProblem
                                {
                                    CheckNumber = returnDeliveryRequest.CheckNumber,
                                    OrderNumber = returnDeliveryRequest.OrderNumber,
                                    step = i,
                                    Type = "5",
                                    Cdate = DateTime.Now,
                                };

                                ShoppeeApiProblem_DA _ShoppeeApiProblem_DA = new ShoppeeApiProblem_DA();
                                _ShoppeeApiProblem_DA.InsertShoppeeApiProblem(_ShoppeeApiProblem);


                                //TcDeliveryRequest _tcDeliveryRequest = new TcDeliveryRequest
                                //{
                                //    CheckNumber = returnDeliveryRequest.CheckNumber
                                //};
                                //tcDeliveryRequests_DA _TcDeliveryRequest_DA = new tcDeliveryRequests_DA();
                                //_TcDeliveryRequest_DA.Update_checknumber(returnDeliveryRequest.CheckNumber, returnDeliveryRequest.RequestId.ToString());

                                //存檔
                                this.DeliveryRequestModifyRepository.Update(returnDeliveryRequest);

                                break;
                            }
                            catch (Exception e)
                            {

                                Thread.Sleep(100);

                                ShoppeeApiProblem _ShoppeeApiProblem = new ShoppeeApiProblem
                                {
                                    CheckNumber = returnDeliveryRequest.CheckNumber,
                                    OrderNumber = returnDeliveryRequest.OrderNumber,
                                    step = i,
                                    Exception = e.ToString(),
                                    Type = "6",
                                    Cdate = DateTime.Now,

                                };

                                ShoppeeApiProblem_DA _ShoppeeApiProblem_DA = new ShoppeeApiProblem_DA();
                                _ShoppeeApiProblem_DA.InsertShoppeeApiProblem(_ShoppeeApiProblem);

                            }

                        }


                        //存檔
                        //this.DeliveryRequestModifyRepository.Update(returnDeliveryRequest);

                        Log("save", JsonConvert.SerializeObject(returnDeliveryRequest).ToString(), returnDeliveryRequest.CustomerCode, returnDeliveryRequest.CheckNumber, "true");
                        EDI_api_new_data_log_DA _EDI_api_new_data_log_DA = new EDI_api_new_data_log_DA();
                        
                        //寫LOG記錄從哪隻API進入
                        _EDI_api_new_data_log_DA.InsertAPIorigin(returnDeliveryRequest.CustomerCode, "GZ_OLD_API");
                        
                        //記全部的log
                        ApiAddressParseLog(start_time, returnDeliveryRequest, getJson);

                        if (addPickUpRequest)
                        {
                            //產生取貨資料
                            if (isShopee)
                            {
                                PickupRequestForApiuser pickupRequestForApiuser = new PickupRequestForApiuser
                                {
                                    CheckNumber = returnDeliveryRequest.CheckNumber,
                                    CustomerCode = returnDeliveryRequest.CustomerCode,
                                    Md = orgAreaSend.MdNo,
                                    ReassignMd = orgAreaSend.MdNo,
                                    Pieces = (returnDeliveryRequest.Pieces == null || returnDeliveryRequest.Pieces == 0) ? returnDeliveryRequest.Plates : returnDeliveryRequest.Pieces,
                                    Putorder = orgAreaSend.PutOrder,
                                    RequestDate = DateTime.Now,
                                    Sd = orgAreaSend.SdNo,
                                    SendArea = orgAreaSend.Area,
                                    SendCity = orgAreaSend.City,
                                    SendRoad = fSEAddressEntitySend.TransAddress,
                                    SendTel = returnDeliveryRequest.SendTel,
                                    SupplierCode = this.StationRepository.GetStationCodeByStationScode(orgAreaSend.StationScode)
                                };

                                for (int i = 1; i < 10; i++)
                                {
                                    try
                                    {
                                        ShoppeeApiProblem _ShoppeeApiProblem = new ShoppeeApiProblem
                                        {
                                            CheckNumber = returnDeliveryRequest.CheckNumber,
                                            OrderNumber = returnDeliveryRequest.OrderNumber,
                                            step = i,
                                            Type = "7",
                                            Cdate = DateTime.Now,

                                        };

                                        ShoppeeApiProblem_DA _ShoppeeApiProblem_DA = new ShoppeeApiProblem_DA();
                                        _ShoppeeApiProblem_DA.InsertShoppeeApiProblem(_ShoppeeApiProblem);

                                        this.PickupRequestForApiuserRepository.Insert(pickupRequestForApiuser);

                                        break;
                                    }
                                    catch (Exception e)
                                    {

                                        Thread.Sleep(100);

                                        ShoppeeApiProblem _ShoppeeApiProblem = new ShoppeeApiProblem
                                        {
                                            CheckNumber = returnDeliveryRequest.CheckNumber,
                                            OrderNumber = returnDeliveryRequest.OrderNumber,
                                            step = i,
                                            Exception = e.ToString(),
                                            Type = "8",
                                            Cdate = DateTime.Now,

                                        };

                                        ShoppeeApiProblem_DA _ShoppeeApiProblem_DA = new ShoppeeApiProblem_DA();
                                        _ShoppeeApiProblem_DA.InsertShoppeeApiProblem(_ShoppeeApiProblem);

                                    }
                                }

                            }
                            else
                            {


                                PickUpRequestLog pickUpRequestLog = new PickUpRequestLog();
                                pickUpRequestLog.RequestCustomerCode = deliveryRequestApiEntity.CustomerCode;
                                pickUpRequestLog.PickUpPieces = deliveryRequestApiEntity.Pieces;

                                PickUpRequestRepository.Insert(pickUpRequestLog);


                                CheckNumberRecord _CheckNumberRecord = new CheckNumberRecord
                                {

                                    customer_code = deliveryRequestApiEntity.CustomerCode,
                                    pieces_count = "-1",
                                    function_flag = "D1-1",
                                    update_date = DateTime.Now,
                                    adjustment_reason = "API打單",
                                    check_number = eDIResultEntity.CheckNumber,
                                    remain_count = CustomerChecknumberSettingRepository.GetRemainCount(deliveryRequestApiEntity.CustomerCode)
                                };
                                CheckNumberRecord_DA _CheckNumberRecord_DA = new CheckNumberRecord_DA();
                                _CheckNumberRecord_DA.Insertckeck_number_record(_CheckNumberRecord);

                            }
                        }

                        //寫入結算計算材積的表格
                        string CBM = string.Empty;
                        try
                        {
                            //CustomerProductType = _tbCustomers.GetCustomerProductType(customCode);

                            if (CustomerProductType != null)
                            {
                                if (CustomerProductType.product_type == 1)
                                {
                                    if (deliveryRequestApiEntity.CbmSize == "1")
                                    {
                                        CBM = "S060";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "2")
                                    {
                                        CBM = "S090";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "6")
                                    {
                                        CBM = "S110";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "3")
                                    {
                                        CBM = "S120";
                                    }
                                    else if (deliveryRequestApiEntity.CbmSize == "4")
                                    {
                                        CBM = "S150";
                                    }
                                    else
                                    {
                                        CBM = "S090";
                                    }
                                }
                                else if (CustomerProductType.product_type == 2)
                                {
                                    CBM = "B003";
                                }
                                else if (CustomerProductType.product_type == 3)
                                {
                                    CBM = "X003";
                                }
                            }

                            CBMDetailLog_DA _CBMDetailLog_DA = new CBMDetailLog_DA();

                            CBMDetailLog_Condition CBMDetailLogEnity = new CBMDetailLog_Condition
                            {
                                ComeFrom = "1",
                                CheckNumber = returnDeliveryRequest.CheckNumber,
                                CBM = CBM == null ? "S090" : CBM,
                                //Length,
                                //Width,
                                //Height,
                                CreateDate = DateTime.Now,
                                CreateUser = returnDeliveryRequest.CustomerCode
                            };

                            _CBMDetailLog_DA.InsertCBMDetailLog(CBMDetailLogEnity);
                        }
                        catch (Exception)
                        {


                        };
                    }
                    catch (Exception ex)
                    {
                        eDIResultEntity.Result = false;
                        eDIResultEntity.Msg = ex.Message.ToString();

                        //記錄開單失敗訊息
                        ApiAddressParseLog(start_time, ex, deliveryRequestApiEntity, returnDeliveryRequest, getJson);

                        if (ex.Message.Contains("傳遞到 LEFT 或 SUBSTRING 函數的參數長度無效"))
                        {

                            //記錄開單失敗訊息
                            //ApiAddressParseLog(start_time, ex, deliveryRequestApiEntity, returnDeliveryRequest);

                            ShopeeException _ShopeeException = new ShopeeException
                            {
                                exception = ex.ToString(),
                                cdate = DateTime.Now,
                                json = getJson,
                                order_number = deliveryRequestApiEntity.OrderNumber,
                                send_address = deliveryRequestApiEntity.SenderAddress,
                                receive_address = deliveryRequestApiEntity.ReceiverAddress,
                                send_station = returnDeliveryRequest.SendStationScode,
                                delivery_station = returnDeliveryRequest.AreaArriveCode,
                                customer_code = deliveryRequestApiEntity.CustomerCode,
                                start_time = start_time,
                                end_time = DateTime.Now

                            };

                            ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                            _ShopeeException_DA.InsertShopeeException(_ShopeeException);
                        }
                    }



                    eDIResultEntities.Add(eDIResultEntity);
                }
            }
            catch (Exception ex)
            {
                EDIResultEntity eDIResultEntity = new EDIResultEntity
                {
                    Result = false,
                    Msg = ex.Message.ToString()
                };

                //記錄開單失敗訊息

                ShopeeException _ShopeeException = new ShopeeException
                {
                    exception = ex.Message.ToString(),
                    cdate = DateTime.Now,
                    json = getJson

                };

                ShopeeException_DA _ShopeeException_DA = new ShopeeException_DA();
                _ShopeeException_DA.InsertShopeeException(_ShopeeException);



                eDIResultEntities.Add(eDIResultEntity);
            }


            Log("respond", JsonConvert.SerializeObject(eDIResultEntities).ToString(), customCode, "", eDIResultEntities[0].Result.ToString());
            Log("respond", "end", "", "", "");
            return eDIResultEntities;
        }

        public TcDeliveryRequest GetRequest(long requestId)
        {
            var returnData = this.DeliveryRequestModifyRepository.SelectById(requestId);

            return returnData;
        }

        public List<ApiScanLogEntity> GetScanLogByCustomerCodeAndTimeZone(string customerCode, DateTime start, DateTime end, bool isCheckNumberNull = true)
        {
            List<ApiScanLogEntity> data = this.DeliveryScanLogRepository.GetApiScanLogByCustomerCodeAndTimeZone(customerCode, start, end, isCheckNumberNull);

            List<string> stationCodes = data.Select(d => d.stationCode).Distinct().ToList();

            var stations = this.StationRepository.GetTbStations(stationCodes);

            var driverCodes = data.Select(d => d.driverCode).Distinct().ToList();

            var drivers = this.DriverRepository.GetDriversByDriverCodes(driverCodes);

            var arriveCodes = this.DeliveryScanLogRepository.GetItemCode("AO");

            //針對有回應訊息進行修正
            arriveCodes["H"] = "貨故結案";

            var errorCodes = this.DeliveryScanLogRepository.GetItemCode("EO");

            List<ApiScanLogEntity> returnData = new List<ApiScanLogEntity>();

            foreach (ApiScanLogEntity apiScanLogEntity in data)
            {
                try
                {
                    apiScanLogEntity.driverName = (drivers.ContainsKey(apiScanLogEntity.driverCode.Trim())) ? drivers[apiScanLogEntity.driverCode.Trim()].DriverName : string.Empty;

                    apiScanLogEntity.stationName = (stations.ContainsKey(apiScanLogEntity.stationCode)) ? stations[apiScanLogEntity.stationCode].StationName : string.Empty;

                    if (apiScanLogEntity.deliveryType == "R")
                    {
                        //逆物流時，調整對應的逆物流單號

                        string reCheckNumber = apiScanLogEntity.checkNumber;

                        apiScanLogEntity.checkNumber = apiScanLogEntity.returnCheckNumber;

                        apiScanLogEntity.returnCheckNumber = reCheckNumber;
                    }

                    if (apiScanLogEntity.scanName == "3")
                    {
                        if (apiScanLogEntity.itemCodes != null)
                            apiScanLogEntity.status = (arriveCodes.ContainsKey(apiScanLogEntity.itemCodes)) ? arriveCodes[apiScanLogEntity.itemCodes] : string.Empty;
                    }
                    else //除了配達歷程之外，其他歷程不回傳狀態
                    {
                        if (apiScanLogEntity.itemCodes != null)
                            apiScanLogEntity.status = string.Empty;
                        apiScanLogEntity.itemCodes = string.Empty;
                    }

                    switch (apiScanLogEntity.scanName)
                    {
                        case "1":
                            apiScanLogEntity.scanName = "到著";
                            apiScanLogEntity.scan_item = "1";
                            break;
                        case "2":
                            apiScanLogEntity.scanName = "配送";
                            apiScanLogEntity.scan_item = "2";
                            break;
                        case "3":
                        case "4":
                            apiScanLogEntity.scanName = "配達";
                            apiScanLogEntity.scan_item = "3";
                            break;
                        case "5":
                            apiScanLogEntity.scanName = "集貨";
                            apiScanLogEntity.scan_item = "5";
                            break;
                        case "6":
                            apiScanLogEntity.scanName = "卸集";
                            apiScanLogEntity.scan_item = "6";
                            break;
                        case "7":
                            apiScanLogEntity.scanName = "發送";
                            apiScanLogEntity.scan_item = "7";
                            break;
                        default:
                            break;

                    }

                    //去除毫秒
                    if (apiScanLogEntity.scanDate != null && apiScanLogEntity.scanDate != DateTime.MinValue && apiScanLogEntity.scanDate.Millisecond > 0)
                    {
                        apiScanLogEntity.scanDate = apiScanLogEntity.scanDate.AddMilliseconds(-apiScanLogEntity.scanDate.Millisecond);
                    }

                    returnData.Add(apiScanLogEntity);
                }
                catch
                {
                    continue;
                }
            }

            return returnData;
        }

        public ApiPickupLogReturnEntity GetPickupLogReturnEntityByCustomer(string customerCode)
        {
            //先取得過去24小時集貨的資料
            DateTime start = DateTime.Now.AddHours(-24);

            var pickedupData = this.DeliveryScanLogRepository.GetLastPickUpScan(customerCode, start);

            var pickedupCheckNumbers = (from request in pickedupData select request.CheckNumber).Distinct().ToList();

            var pickedupRequestInfo = this.DeliveryScanLogRepository.GetByCheckNumberList(pickedupCheckNumbers);

            //整理最後取件的時間
            Dictionary<string, DateTime> keyValuePairs = new Dictionary<string, DateTime>();
            Dictionary<string, string> driverCodes = new Dictionary<string, string>();
            Dictionary<string, string> notPickDrivers = new Dictionary<string, string>();
            Dictionary<string, int> checkNumberGetNum = new Dictionary<string, int>();
            Dictionary<string, bool> checkNumberInsert = new Dictionary<string, bool>();

            //取得司機資訊
            var driverCodeList = (from scan in pickedupData select scan.DriverCode).Distinct().ToList();
            var driverInfo = this.DriverRepository.GetDriversByDriverCodes(driverCodeList);

            //取得集貨失敗的代碼
            var receiveOptions = this.DeliveryScanLogRepository.GetReceiveOptionCode();
            Dictionary<string, string> receiveOptionsPairs = receiveOptions.ToDictionary(p => p.CodeId, p => p.CodeName);


            foreach (TtDeliveryScanLog ttDeliveryScanLog in pickedupData)
            {
                TcDeliveryRequest tcDeliveryRequest = pickedupRequestInfo[ttDeliveryScanLog.CheckNumber];

                string addressData = string.Format("{0}{1}{2}{3}", tcDeliveryRequest.SendContact, tcDeliveryRequest.SendCity, tcDeliveryRequest.SendArea, tcDeliveryRequest.SendAddress);

                //地址的最晚取件時間
                if (!keyValuePairs.ContainsKey(addressData))
                {
                    keyValuePairs.Add(addressData, (DateTime)ttDeliveryScanLog.ScanDate);
                    driverCodes.Add(addressData, ttDeliveryScanLog.DriverCode);
                }
                else if (keyValuePairs[addressData] < ttDeliveryScanLog.ScanDate)
                {
                    keyValuePairs[addressData] = (DateTime)ttDeliveryScanLog.ScanDate;
                    driverCodes[addressData] = ttDeliveryScanLog.DriverCode;
                }

                //取件個數
                int nPiece = 1;
                if (ttDeliveryScanLog.Pieces != null && ttDeliveryScanLog.Pieces != 0)
                {
                    nPiece = (int)ttDeliveryScanLog.Pieces;
                }

                if (!checkNumberGetNum.ContainsKey(ttDeliveryScanLog.CheckNumber))
                {
                    checkNumberGetNum.Add(ttDeliveryScanLog.CheckNumber, nPiece);
                }
                else
                {
                    checkNumberGetNum[ttDeliveryScanLog.CheckNumber] += nPiece;
                }

                //集貨成功才用集貨成功資訊
                bool pickupSuccess = false;

                if (ttDeliveryScanLog.ReceiveOption == null || ttDeliveryScanLog.ReceiveOption == string.Empty || ttDeliveryScanLog.ReceiveOption == "20")
                {
                    pickupSuccess = true;
                }

                //是否已放入輸出資料的初始化
                if (pickupSuccess && !checkNumberInsert.ContainsKey(ttDeliveryScanLog.CheckNumber))
                {
                    checkNumberInsert.Add(ttDeliveryScanLog.CheckNumber, false);
                }
            }

            //取得過去3天還沒集貨的資料
            DateTime putStart = DateTime.Now.AddDays(-3);
            var notPickedInfo = this.DeliveryScanLogRepository.GetLastNotPickUpScan(customerCode, putStart);

            List<TcDeliveryRequest> goAndNotGetData = new List<TcDeliveryRequest>();

            foreach (TcDeliveryRequest tcDeliveryRequest1 in notPickedInfo)
            {
                string addressData1 = string.Format("{0}{1}{2}{3}", tcDeliveryRequest1.SendContact, tcDeliveryRequest1.SendCity, tcDeliveryRequest1.SendArea, tcDeliveryRequest1.SendAddress);

                if (keyValuePairs.ContainsKey(addressData1) && keyValuePairs[addressData1] > tcDeliveryRequest1.Cdate)
                {
                    goAndNotGetData.Add(tcDeliveryRequest1);
                    notPickDrivers.Add(tcDeliveryRequest1.CheckNumber, driverCodes[addressData1]);
                }
            }

            //開始產生資訊
            List<ApiPickupLogEntity> returnData = new List<ApiPickupLogEntity>();

            //已集貨資訊
            foreach (TtDeliveryScanLog ttDeliveryScanLog1 in pickedupData)
            {
                //集貨成功才用集貨成功資訊
                bool pickupSuccess = false;

                if (ttDeliveryScanLog1.ReceiveOption == null || ttDeliveryScanLog1.ReceiveOption == string.Empty || ttDeliveryScanLog1.ReceiveOption == "20")
                {
                    pickupSuccess = true;
                }

                //成功集貨的
                if (pickupSuccess && checkNumberInsert.ContainsKey(ttDeliveryScanLog1.CheckNumber) && !checkNumberInsert[ttDeliveryScanLog1.CheckNumber])
                {
                    string myCustomerCode = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].CustomerCode : customerCode;
                    string deliveryType = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].DeliveryType : "無資訊";

                    string driverCode = ttDeliveryScanLog1.DriverCode ?? string.Empty;
                    string driverType = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverType : "CS";
                    string driverTypeCode = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverTypeCode : "0";
                    ttDeliveryScanLog1.ScanDate = RemoveMilliSecond(ttDeliveryScanLog1.ScanDate);
                    pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate = RemoveMilliSecond(pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate);

                    ApiPickupLogEntity apiPickupLogEntity = new ApiPickupLogEntity
                    {
                        ActualPickup = true,
                        ActualPickupPieces = checkNumberGetNum[ttDeliveryScanLog1.CheckNumber],
                        CheckNumber = ttDeliveryScanLog1.CheckNumber,
                        CustomerCode = myCustomerCode,
                        DeliveryType = deliveryType,
                        DriverCode = driverCode,
                        DriverType = driverType,
                        DriverTypeCode = driverTypeCode,
                        FalseItem = string.Empty,
                        ScanDate = ttDeliveryScanLog1.ScanDate,
                        ScanName = "集貨",
                        ShipDate = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate == null ? (DateTime)ttDeliveryScanLog1.ScanDate : (DateTime)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate,
                        ShouldPickupPieces = (int)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].Pieces,
                        SupplierCode = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].SupplierCode
                    };

                    returnData.Add(apiPickupLogEntity);

                    checkNumberInsert[ttDeliveryScanLog1.CheckNumber] = true;
                }

                //集貨失敗的情況
                if (!pickupSuccess)
                {
                    string myCustomerCode = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].CustomerCode : customerCode;
                    string deliveryType = (pickedupRequestInfo.ContainsKey(ttDeliveryScanLog1.CheckNumber)) ? pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].DeliveryType : "無資訊";

                    string driverCode = ttDeliveryScanLog1.DriverCode ?? string.Empty;
                    string driverType = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverType : "CS";
                    string driverTypeCode = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverTypeCode : "0";
                    ttDeliveryScanLog1.ScanDate = RemoveMilliSecond(ttDeliveryScanLog1.ScanDate);
                    pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate = RemoveMilliSecond(pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate);

                    ApiPickupLogEntity apiPickupLogEntity = new ApiPickupLogEntity
                    {
                        ActualPickup = false,
                        ActualPickupPieces = checkNumberGetNum[ttDeliveryScanLog1.CheckNumber],
                        CheckNumber = ttDeliveryScanLog1.CheckNumber,
                        CustomerCode = myCustomerCode,
                        DeliveryType = deliveryType,
                        DriverCode = driverCode,
                        DriverType = driverType,
                        DriverTypeCode = driverTypeCode,
                        FalseItem = receiveOptionsPairs[ttDeliveryScanLog1.ReceiveOption],
                        ScanDate = ttDeliveryScanLog1.ScanDate,
                        ScanName = "集貨",
                        ShipDate = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate == null ? (DateTime)ttDeliveryScanLog1.ScanDate : (DateTime)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].ShipDate,
                        ShouldPickupPieces = (int)pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].Pieces,
                        SupplierCode = pickedupRequestInfo[ttDeliveryScanLog1.CheckNumber].SupplierCode
                    };

                    returnData.Add(apiPickupLogEntity);
                }
            }

            //未集貨的部分，不主動回拋無件可取貨態
            //foreach (TcDeliveryRequest tcDeliveryRequest2 in goAndNotGetData)
            //{
            //    string driverCode = (notPickDrivers.ContainsKey(tcDeliveryRequest2.CheckNumber)) ? notPickDrivers[tcDeliveryRequest2.CheckNumber] : string.Empty;
            //    string driverType = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverType : "CS";
            //    string driverTypeCode = (driverInfo.ContainsKey(driverCode)) ? driverInfo[driverCode].DriverTypeCode : "0";

            //    string addressData2 = string.Format("{0}{1}{2}{3}", tcDeliveryRequest2.SendContact, tcDeliveryRequest2.SendCity, tcDeliveryRequest2.SendArea, tcDeliveryRequest2.SendAddress);
            //    keyValuePairs[addressData2] = (DateTime)RemoveMilliSecond(keyValuePairs[addressData2]);

            //    ApiPickupLogEntity apiPickupLogEntity = new ApiPickupLogEntity
            //    {
            //        ActualPickup = false,
            //        ActualPickupPieces = 0,
            //        CheckNumber = tcDeliveryRequest2.CheckNumber,
            //        CustomerCode = tcDeliveryRequest2.CustomerCode,
            //        DeliveryType = tcDeliveryRequest2.DeliveryType,
            //        DriverCode = driverCode,
            //        DriverType = driverType,
            //        DriverTypeCode = driverTypeCode,
            //        FalseItem = "無件可取",
            //        ScanDate = null,
            //        ScanName = "集貨",
            //        ShipDate = keyValuePairs[addressData2],
            //        ShouldPickupPieces = (int)tcDeliveryRequest2.Pieces,
            //        SupplierCode = tcDeliveryRequest2.SupplierCode
            //    };

            //    returnData.Add(apiPickupLogEntity);
            //}

            ApiPickupLogReturnEntity apiPickupLogReturnEntity = new ApiPickupLogReturnEntity();

            apiPickupLogReturnEntity.PickupInfo = returnData;

            return apiPickupLogReturnEntity;
        }

        public ApiCbmInfoReturnEntity GetMeasuredCbmInfo(string customerCode)
        {
            //取得過去24小時量的資訊
            /*DateTime start = DateTime.Now.AddHours(-24);
            DateTime end = DateTime.Now;*/

            DateTime now = DateTime.Now;
            DateTime yesterday = now.AddDays(-1);
            DateTime start = new DateTime(yesterday.Year, yesterday.Month, yesterday.Day, 5, 0, 0);
            DateTime end = new DateTime(now.Year, now.Month, now.Day, 5, 0, 0);

            var requests = this.DeliveryScanLogRepository.GetMeasuredInfo(customerCode, start, end);

            List<ApiCbmInfoEntity> apiCbmInfoEntities = new List<ApiCbmInfoEntity>();

            foreach (TcDeliveryRequest tcDeliveryRequest in requests)
            {
                if (tcDeliveryRequest.CbmLength == null || tcDeliveryRequest.CbmHeight == null || tcDeliveryRequest.CbmWidth == null)
                {
                    tcDeliveryRequest.CbmLength = 200;
                    tcDeliveryRequest.CbmHeight = 200;
                    tcDeliveryRequest.CbmWidth = 190;
                    tcDeliveryRequest.CbmCont = 0.3;
                    tcDeliveryRequest.CbmWeight = 1.6;
                }

                double sum = (double)tcDeliveryRequest.CbmLength + (double)tcDeliveryRequest.CbmHeight + (double)tcDeliveryRequest.CbmWidth;

                int MeasureSize = 0;//丈量機量到的材積

                if (sum <= 600)
                {
                    MeasureSize = 60;
                }
                else if (sum <= 900)
                {
                    MeasureSize = 90;
                }
                else if (sum <= 1200)
                {
                    MeasureSize = 120;
                }
                else
                {
                    MeasureSize = 150;
                }

                string CbmSize = tcDeliveryRequest.SpecCodeId;//賣方自己填的

                int Seller_CbmSize = 0;

                switch (CbmSize)
                {
                    case "S060":
                        Seller_CbmSize = 60;
                        break;
                    case "S090":
                        Seller_CbmSize = 90;
                        break;
                    case "S120":
                        Seller_CbmSize = 120;
                        break;
                    case "S150":
                        Seller_CbmSize = 150;
                        break;
                    default:
                        break;
                }

                string Return_Size = string.Empty;

                if (Seller_CbmSize > MeasureSize)
                {
                    Return_Size = "S" + Seller_CbmSize.ToString();
                }
                else if (MeasureSize > Seller_CbmSize)
                {
                    Return_Size = "S" + MeasureSize.ToString();
                }
                else
                {
                    Return_Size = "S" + MeasureSize.ToString();
                }



                ApiCbmInfoEntity apiCbmInfoEntity = new ApiCbmInfoEntity
                {
                    CheckNumber = tcDeliveryRequest.CheckNumber,
                    Size = Return_Size,
                    Length = (Double)tcDeliveryRequest.CbmLength,
                    Width = (Double)tcDeliveryRequest.CbmWidth,
                    Height = (Double)tcDeliveryRequest.CbmHeight,
                    SidesSum = sum,
                    Cbm = (Double)tcDeliveryRequest.CbmCont,
                    Weight = tcDeliveryRequest.CbmWeight ?? 0,
                    MeasurementTime = DateTime.Today
                };

                apiCbmInfoEntities.Add(apiCbmInfoEntity);
            }

            ApiCbmInfoReturnEntity apiCbmInfoReturnEntity = new ApiCbmInfoReturnEntity
            {
                CbmInfo = apiCbmInfoEntities
            };

            return apiCbmInfoReturnEntity;
        }
        private DateTime? RemoveMilliSecond(DateTime? dt)
        {
            if (dt != null)
                return ((DateTime)dt).AddMilliseconds(-((DateTime)dt).Millisecond);
            else
                return null;
        }
    }
}
