﻿using LightYear.JunFuTrans.BL.BE.PalletACSectionShouldPay;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.PalletACSectionShouldPay;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DocumentFormat.OpenXml.Drawing;
using System.IO;
using OfficeOpenXml;
using System.Globalization;
using OfficeOpenXml.Style;
using System.IO.Compression;

namespace LightYear.JunFuTrans.BL.Services.PalletACSectionShouldPay
{
    public class PalletACSectionShouldPayService : IPalletACSectionShouldPayService
    {
        public IPalletACSectionShouldPayRepository PalletACSectionShouldPayRepository { get; set; }

        public PalletACSectionShouldPayService(IPalletACSectionShouldPayRepository palletACSectionShouldPayRepository)
        {
            PalletACSectionShouldPayRepository = palletACSectionShouldPayRepository;
        }

        public List<SuppliersEntity> GetSuppliers()
        {
            var output = PalletACSectionShouldPayRepository.GetSuppliers();
            output.Insert(0, new SuppliersEntity { SupplierCode = "All", SupplierName = "全部區配商" });
            return output;
        }

        public List<CSectionOriginalShippingFee> GetCSectionFee()
        {
            return PalletACSectionShouldPayRepository.GetCSectionOriginalShippingFee();
        }

        public List<ArriveOptionIsPaymentRequired> GetArriveOptionIsPaymentRequired()
        {
            return PalletACSectionShouldPayRepository.GetArriveOptionIsPaymentRequired();
        }

        /// <summary>
        /// 開始算AC段的應付費用
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="supplierCode"></param>
        /// <param name="checkNumber"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public List<PalletACSectionShouldPayEntity> GetACSectionShouldPayDetails(DateTime start, DateTime end, string supplierCode, string checkNumber, string taskId)
        {
            var ACShouldPayRequestIds = new List<int>();
            var EditedMainData = new List<PalletACSectionShouldPayEntity>();
            var outputFromACShouldPay = new List<PalletACSectionShouldPayEntity>();
            var MainData = new List<PalletACSectionShouldPayEntity>();
            var ASectionSupplierAndDriver = new List<ACSectionSupplierAndDriver>();

            if (!string.IsNullOrEmpty(checkNumber) || (!string.IsNullOrEmpty(checkNumber) && !string.IsNullOrEmpty(taskId)))
            {
                MainData = PalletACSectionShouldPayRepository.GetACSectionShouldPayDetailsByCheckNumber(checkNumber);
                ASectionSupplierAndDriver = PalletACSectionShouldPayRepository.GetASectionSupplierAndDriverByCheckNumber(checkNumber);
            }
            else if (!string.IsNullOrEmpty(taskId))
            {
                MainData = PalletACSectionShouldPayRepository.GetACSectionShouldPayDetailsByTaskId(taskId);
                ASectionSupplierAndDriver = PalletACSectionShouldPayRepository.GetASectionSupplierAndDriverByTaskId(taskId);
            }
            else
            {
                MainData = PalletACSectionShouldPayRepository.GetACSectionShouldPayDetails(start, end.AddDays(1));
                ASectionSupplierAndDriver = PalletACSectionShouldPayRepository.GetASectionSupplierAndDriver(start, end.AddDays(1));
            }

            var MainDateRequestId = MainData.Select(a => a.RequestId).ToList();
            var ACShouldPay = PalletACSectionShouldPayRepository.GetPalletAcSectionShouldPayByRequestIds(MainDateRequestId);
            if (ACShouldPay != null)
            { ACShouldPayRequestIds = ACShouldPay.Select(a => a.RequestId ?? 0).ToList(); }

            var CDiscount = PalletACSectionShouldPayRepository.GetCSectionFloatingOilPriceDiscount();
            var CSectionOriginalShippingFee = PalletACSectionShouldPayRepository.GetCSectionOriginalShippingFee();
            var ArriveOptionIsPaymentRequired = PalletACSectionShouldPayRepository.GetArriveOptionIsPaymentRequired();
            var remote = PalletACSectionShouldPayRepository.GetACRemoteSections();

            int remainder = 0;

            //已編輯過AC段費用的貨號，直接採用編輯後的費用
            if (ACShouldPayRequestIds.Count > 0)
            {
                EditedMainData = MainData.Where(a => ACShouldPayRequestIds.Contains(Convert.ToInt32(a.RequestId))).ToList();

                outputFromACShouldPay = (from m in EditedMainData
                                         join a in ACShouldPay on Convert.ToInt32(m.RequestId) equals a.RequestId ?? 0
                                         select new PalletACSectionShouldPayEntity
                                         {
                                             ArriveOption = m.ArriveOption,
                                             ASectionPayment = a.ASectionPayment ?? 0,
                                             CShippingFee = a.CSectionShippingFee,
                                             RemoteFee = a.RemoteSectionFee,
                                             CSectionSpecialFee = a.SpecialFee,
                                             CSectionPayment = (a.CSectionShippingFee + a.RemoteSectionFee + a.SpecialFee) ?? 0,
                                             UpdateUser = a.UpdateUser,
                                             PalletACSectionShouldPayId = a.Id,
                                             RequestId = m.RequestId,
                                             TaskId = m.TaskId,
                                             UserName = m.UserName,
                                             PrintDate = m.PrintDate,
                                             CustomerCode = m.CustomerCode,
                                             CheckNumber = m.CheckNumber,
                                             SendCity = m.SendCity,
                                             SendContact = m.SendContact,
                                             ReceiveContact = m.ReceiveContact,
                                             ReceiveCity = m.ReceiveCity,
                                             Plates = m.Plates,
                                             Pieces = m.Pieces,
                                             AShipDate = m.AShipDate,
                                             SpecialNeed = m.SpecialNeed,
                                             ArriveStation = m.ArriveStation,
                                             SendStation = m.SendStation,
                                             CDriver = m.CDriver,
                                             CSectionSupplier = m.CSectionSupplier,
                                             SendStationCode = m.SendStationCode,
                                             ReceiveAddress = m.ReceiveAddress
                                         }).ToList();
            }
            //未編輯過AC段費用的貨號
            var output = (from m in (MainData.Except(EditedMainData))
                          join c in CSectionOriginalShippingFee on new { SupplierArea = m.SendCity.Length > 2 ? m.SendCity.Substring(0, 3) : m.SendCity, ReceiveArea = m.ReceiveCity.Length > 2 ? m.ReceiveCity.Substring(0, 3) : m.ReceiveCity, SupplierNo = m.CSectionSupplierNo } equals new { SupplierArea = c.SupplierArea, ReceiveArea = c.ReceiveArea, SupplierNo = c.SupplierNo } into cTemp
                          from c in cTemp.DefaultIfEmpty(new CSectionOriginalShippingFee { C1 = default, C2 = default, C3 = default, C4 = default, C5 = default, C6 = default })
                          select new
                          {
                              ASectionPayment = m.SpecialNeed.Contains("手工") ? ((m.Plates ?? 0) * 100) + (Math.DivRem(m.Plates ?? 0, 7, out remainder) * 1350) + (remainder * 200) :
                                                                                 Math.DivRem(m.Plates ?? 0, 7, out remainder) * 1350 + (remainder * 200),
                              CShippingFee = m.CSectionPayment == 0 ? 0 : (m.Plates == 1 ? m.Plates * (c.C1 + CDiscount.OnePalletPriceBalance) :
                                             m.Plates == 2 ? m.Plates * (c.C2 + CDiscount.TwoPalletPriceBalance) :
                                             m.Plates == 3 ? m.Plates * (c.C3 + CDiscount.ThreePalletPriceBalance) :
                                             m.Plates == 4 ? m.Plates * (c.C4 + CDiscount.FourPalletPriceBalance) :
                                             m.Plates == 5 ? m.Plates * (c.C5 + CDiscount.FivePalletPriceBalance) :
                                             m.Plates >= 6 ? m.Plates * (c.C6 + CDiscount.SixPalletPriceBalance) : 0),
                              CSectionSpecialFee = m.CSectionPayment == 0 ? 0 : (m.SpecialNeed.Contains("手工") ? ((m.Plates ?? 0) * 100) : 0),
                              ArriveOption = m.ArriveOption,
                              RequestId = m.RequestId,
                              TaskId = m.TaskId,
                              UserName = m.UserName,
                              PrintDate = m.PrintDate,
                              CustomerCode = m.CustomerCode,
                              CheckNumber = m.CheckNumber,
                              SendCity = m.SendCity,
                              SendContact = m.SendContact,
                              ReceiveContact = m.ReceiveContact,
                              ReceiveCity = m.ReceiveCity,
                              Plates = m.Plates,
                              Pieces = m.Pieces,
                              AShipDate = m.AShipDate,
                              SpecialNeed = m.SpecialNeed,
                              ArriveStation = m.ArriveStation,
                              SendStation = m.SendStation,
                              CDriver = m.CDriver,
                              CSectionSupplier = m.CSectionSupplier,
                              SendStationCode = m.SendStationCode,
                              ReceiveAddress = m.ReceiveAddress
                          }).Distinct().ToList();

            //未編輯過AC段費用的貨號裝到class裡
            var distinctOutput = output.Select(m => new
            PalletACSectionShouldPayEntity
            {
                //ASectionSupplier = m.ASectionSupplier,
                //ADriver = m.ADriver,
                ASectionPayment = m.ASectionPayment,
                CShippingFee = m.CShippingFee,
                CSectionSpecialFee = m.CSectionSpecialFee,
                ArriveOption = m.ArriveOption,
                RequestId = m.RequestId,
                TaskId = m.TaskId,
                UserName = m.UserName,
                PrintDate = m.PrintDate,
                CustomerCode = m.CustomerCode,
                CheckNumber = m.CheckNumber,
                SendCity = m.SendCity,
                SendContact = m.SendContact,
                ReceiveContact = m.ReceiveContact,
                ReceiveCity = m.ReceiveCity,
                Plates = m.Plates,
                Pieces = m.Pieces,
                AShipDate = m.AShipDate,
                SpecialNeed = m.SpecialNeed,
                ArriveStation = m.ArriveStation,
                SendStation = m.SendStation,
                CDriver = m.CDriver,
                CSectionSupplier = m.CSectionSupplier,
                UpdateUser = "",
                PalletACSectionShouldPayId = 0,
                SendStationCode = m.SendStationCode,
                ReceiveAddress = m.ReceiveAddress
            }).ToList();

            //未編輯過AC段費用的貨號 進行例外的費用運算
            foreach (var item in distinctOutput)
            {
                if (item.CShippingFee < 0)
                    item.CShippingFee = 0;

                if ((item.ArriveOption != null && (item.ArriveOption.Equals("2") || item.ArriveOption.Equals("1"))) ||
                    (item.ReceiveContact.Contains("站址") || item.SpecialNeed.Contains("站址")))
                {
                    item.CShippingFee = 0;
                    item.CSectionSpecialFee = 0;
                    item.RemoteFee = 0;
                    item.CSectionPayment = 0;
                }
                else
                {
                    decimal? appendPrice = remote.Where(a => string.Concat(a.City, a.Area).Equals(item.ReceiveCity)).FirstOrDefault()?.AppendPrice;
                    item.RemoteFee = Convert.ToInt32(appendPrice ?? 0);
                    item.CSectionPayment = item.CShippingFee ?? 0 + item.RemoteFee ?? 0 + item.CSectionSpecialFee ?? 0;
                }
            }


            var prepareToJoinASection = distinctOutput.Union(outputFromACShouldPay).ToList();

            var leftOuterJoin = (from m in prepareToJoinASection
                                 join ASection in ASectionSupplierAndDriver on m.CheckNumber equals ASection.CheckNumber into temp
                                 from ASection in temp.DefaultIfEmpty(new ACSectionSupplierAndDriver { CheckNumber = m.CheckNumber, DriverCodeAndName = "系統補集貨", SupplierCodeAndName = "系統補集貨" })
                                 orderby m.PrintDate
                                 select new PalletACSectionShouldPayEntity
                                 {
                                     ASectionSupplier = ASection.SupplierCodeAndName,
                                     ADriver = ASection.DriverCodeAndName,
                                     AScanDate= ASection.ScanDate,
                                     ArriveOption = m.ArriveOption,
                                     ASectionPayment = m.ASectionPayment,
                                     CShippingFee = m.CShippingFee,
                                     RemoteFee = m.RemoteFee,
                                     CSectionSpecialFee = m.CSectionSpecialFee,
                                     CSectionPayment = m.CSectionPayment,
                                     UpdateUser = m.UpdateUser,
                                     PalletACSectionShouldPayId = m.PalletACSectionShouldPayId,
                                     RequestId = m.RequestId,
                                     TaskId = m.TaskId,
                                     UserName = m.UserName,
                                     PrintDate = m.PrintDate,
                                     CustomerCode = m.CustomerCode,
                                     CheckNumber = m.CheckNumber,
                                     SendCity = m.SendCity,
                                     SendContact = m.SendContact,
                                     ReceiveContact = m.ReceiveContact,
                                     ReceiveCity = m.ReceiveCity,
                                     Plates = m.Plates,
                                     Pieces = m.Pieces,
                                     AShipDate = m.AShipDate,
                                     SpecialNeed = m.SpecialNeed,
                                     ArriveStation = m.ArriveStation,
                                     SendStation = m.SendStation,
                                     CDriver = m.CDriver,
                                     CSectionSupplier = m.CSectionSupplier,
                                     SendStationCode = m.SendStationCode,
                                     ReceiveAddress = m.ReceiveAddress
                                 }).ToList();

            foreach (var item in leftOuterJoin)
            {
                if (item.ADriver.Equals("系統補集貨") || (item.SendStationCode.Equals(item.CustomerCode.Substring(0, 3))))
                    item.ASectionPayment = 0;

                item.ArriveOption = ArriveOptionIsPaymentRequired.Where(a => a.CodeId.Equals(item.ArriveOption)).FirstOrDefault()?.CodeName;
            }

            //只查A或C段區配商加日期的時候
            if (string.IsNullOrEmpty(taskId) && string.IsNullOrEmpty(checkNumber) && !string.IsNullOrEmpty(supplierCode) && supplierCode != "All")
            {
                var selectSupplierCode = leftOuterJoin
                                         .Where(a => (!string.IsNullOrEmpty(a.CSectionSupplier) && a.CSectionSupplier.Substring(0, 3).Equals(supplierCode))
                                                      || a.ASectionSupplier.Substring(0, 3).Equals(supplierCode)).OrderBy(a => a.PrintDate).ToList();
                return selectSupplierCode;
            }

            return leftOuterJoin;
        }

        /// <summary>
        /// 算全部區配商AC段每月請款單
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="supplierCode"></param>
        /// <returns></returns>
        public List<ShowAllSupplierEntity> GetAllSupplierACSectionShouldPay(DateTime start, DateTime end, string supplierCode)
        {
            var data = GetACSectionShouldPayDetails(start, end, "All", "", "");

            var ASectionSuppliers = data.GroupBy(a => a.ASectionSupplier).Select(b => new { Supplier = b.Key, Apayment = b.Sum(a => a.ASectionPayment) }).ToList();
            var CSectionSuppliers = data.GroupBy(a => a.CSectionSupplier).Select(b => new { Supplier = b.Key, Cpayment = b.Sum(a => a.CSectionPayment) }).ToList();

            var leftOuterJoin = (from a in ASectionSuppliers
                                 join c in CSectionSuppliers on a.Supplier equals c.Supplier into temp
                                 from c in temp.DefaultIfEmpty(new { Supplier = a.Supplier, Cpayment = default(int) })
                                 select new { Supplier = a.Supplier ?? "", Apayment = a.Apayment, Cpayment = c.Cpayment }).ToList();
            var rightOuterJoin = (from c in CSectionSuppliers
                                  join a in ASectionSuppliers on c.Supplier equals a.Supplier into temp
                                  from a in temp.DefaultIfEmpty(new { Supplier = c.Supplier, Apayment = default(int) })
                                  select new { Supplier = c.Supplier ?? "", Apayment = a.Apayment, Cpayment = c.Cpayment }).ToList();

            var fullOuterJoin = supplierCode == "All" ?
                                leftOuterJoin.Union(rightOuterJoin).Where(a => a.Supplier != "系統補集貨").Distinct() :
                                leftOuterJoin.Union(rightOuterJoin).Where(a => a.Supplier != "系統補集貨"
                                && (a.Supplier.Length > 0 ? a.Supplier.Substring(0, 3).Equals(supplierCode) : a.Supplier != "")).Distinct();

            var showAllSupplierEntities = fullOuterJoin.Select(t => new ShowAllSupplierEntity
            {
                SupplierName = t.Supplier,
                ASectionPayment = t.Apayment,
                CSectionPayment = t.Cpayment,
                Tax = Math.Round((decimal)((t.Apayment + t.Cpayment) * 0.05), 0, MidpointRounding.AwayFromZero),
                TotalPayment = Math.Round((decimal)((t.Apayment + t.Cpayment) * 1.05), 0, MidpointRounding.AwayFromZero),
                RequestMonth = string.Concat(end.Year, "年", end.Month, "月")
            }).ToList();

            return showAllSupplierEntities;
        }

        /// <summary>
        /// 單一區配商一個月份的AC段應付費用
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="supplierCode"></param>
        /// <returns></returns>
        public List<OneSupplierEntity> GetOneSupplierACSectionShouldPay(DateTime start, DateTime end, string supplierCode)
        {
            var data = GetACSectionShouldPayDetails(start, end, supplierCode, "", "");

            //foreach (var d in data)
            //{
            //    try
            //    {
            //        var Route = d.CSectionSupplier.Substring(0, 3).Equals(supplierCode) ? "C段" : "A段";
            //        var Payment = d.CSectionSupplier.Substring(0, 3).Equals(supplierCode) ? d.CSectionPayment : d.ASectionPayment;

            //        var aa = string.IsNullOrEmpty(d.CSectionSupplier) ? string.Empty : d.CSectionSupplier.Substring(0, 3).Equals(supplierCode) ? "C段" : "A段";

            //    }
            //    catch (Exception e)
            //    {

            //    }
            //}

            var Aoutput = data.Select(m => new //OneSupplierEntity
            {
                PrintDate = m.PrintDate,
                CustomerCode = m.CustomerCode,
                TaskId = m.TaskId,
                CheckNumber = m.CheckNumber,
                SendCity = m.SendCity,
                SendContact = m.SendContact,
                ReceiveContact = m.ReceiveContact,
                ReceiveCity = m.ReceiveCity,
                Plates = m.Plates,
                Pieces = m.Pieces,
                ArriveStation = m.ArriveStation,
                SendStation = m.SendStation,
                Route = string.IsNullOrEmpty(m.CSectionSupplier) ? string.Empty : m.CSectionSupplier.Substring(0, 3).Equals(supplierCode) ? "C段" : "A段",
            Payment = string.IsNullOrEmpty(m.CSectionSupplier) ? 0:m.CSectionSupplier.Substring(0, 3).Equals(supplierCode) ? m.CSectionPayment : m.ASectionPayment
            }).ToList();

            var Coutput = data.Select(m => new //OneSupplierEntity
            {
                PrintDate = m.PrintDate,
                CustomerCode = m.CustomerCode,
                TaskId = m.TaskId,
                CheckNumber = m.CheckNumber,
                SendCity = m.SendCity,
                SendContact = m.SendContact,
                ReceiveContact = m.ReceiveContact,
                ReceiveCity = m.ReceiveCity,
                Plates = m.Plates,
                Pieces = m.Pieces,
                ArriveStation = m.ArriveStation,
                SendStation = m.SendStation,
                Route = m.ASectionSupplier.Substring(0, 3).Equals(supplierCode) ? "A段" : "C段",
                Payment = m.ASectionSupplier.Substring(0, 3).Equals(supplierCode) ? m.ASectionPayment : m.CSectionPayment
            }).ToList();

            var output = Aoutput.Union(Coutput).Distinct().ToList();
            var total = output.Sum(a => a.Payment);

            OneSupplierPayment oneSupplierPayment = new OneSupplierPayment
            {
                Total = total,
                Tax = Math.Round((decimal)(total * 0.05), 0, MidpointRounding.AwayFromZero),
                TotalPayment = Math.Round((decimal)(total * 1.05), 0, MidpointRounding.AwayFromZero)
            };

            var final = output.OrderBy(a => a.CheckNumber).Select(m => new OneSupplierEntity
            {
                PrintDate = m.PrintDate,
                CustomerCode = m.CustomerCode,
                TaskId = m.TaskId,
                CheckNumber = m.CheckNumber,
                SendCity = m.SendCity,
                SendContact = m.SendContact,
                ReceiveContact = m.ReceiveContact,
                ReceiveCity = m.ReceiveCity,
                Plates = m.Plates,
                Pieces = m.Pieces,
                ArriveStation = m.ArriveStation,
                SendStation = m.SendStation,
                Route = m.Route,
                Payment = m.Payment,
                MyPayment = oneSupplierPayment
            }).ToList();

            return final;
        }

        public void Update(PalletAcSectionShouldPay palletAcSectionShouldPay)
        {
            palletAcSectionShouldPay.UpdateDate = DateTime.Now;
            PalletACSectionShouldPayRepository.Update(palletAcSectionShouldPay);
        }

        public int Insert(PalletAcSectionShouldPay palletAcSectionShouldPay)
        {
            palletAcSectionShouldPay.UpdateDate = DateTime.Now;
            return PalletACSectionShouldPayRepository.Insert(palletAcSectionShouldPay);
        }

        public byte[] Export(DateTime end, string supplierCode)
        {
            var path = System.IO.Path.Combine(
                     Directory.GetCurrentDirectory(), "DownloadFiles",
                      "A段C段對帳明細.xlsx");

            var start = end.AddMonths(-1).AddDays(1);

            var mainData = GetACSectionShouldPayDetails(start, end, supplierCode.Substring(0, 3), "", "");

            //算A段
            var ASection = mainData.Where(a => a.ASectionSupplier.Equals(supplierCode)).Distinct().ToList();
            var ASectionPlates = ASection.GroupBy(a => a.PrintDate.Value.Date)
                .Select(b => new { APrintDate = b.Key.Date, APlates = b.Sum(a => a.Plates ?? 0), AMoney = b.Sum(a => a.ASectionPayment) }).ToList();

            //算C段
            
            
                var CSection = mainData.Where(a => (string.IsNullOrEmpty(a.CSectionSupplier) ? string.Empty : a.CSectionSupplier).Equals(supplierCode)).Distinct().ToList();
                var CSectionPlates = CSection.GroupBy(a => a.PrintDate.Value.Date)
                    .Select(b => new { CPrintDate = b.Key.Date, CPlates = b.Sum(a => a.Plates ?? 0), CMoney = b.Sum(a => a.CSectionPayment) }).ToList();
            
            var leftOuterJoin = (from a in ASectionPlates
                                 join c in CSectionPlates on a.APrintDate equals c.CPrintDate into temp
                                 from c in temp.DefaultIfEmpty(new { CPrintDate = a.APrintDate, CPlates = default(int), CMoney = default(int) })
                                 select new { PrintDate = a.APrintDate, APlates = a.APlates, AMoney = a.AMoney, CPlates = c.CPlates, CMoney = c.CMoney }).ToList();

            var rightOuterJoin = (from c in CSectionPlates
                                  join a in ASectionPlates on c.CPrintDate equals a.APrintDate into temp
                                  from a in temp.DefaultIfEmpty(new { APrintDate = c.CPrintDate, APlates = default(int), AMoney = default(int) })
                                  select new { PrintDate = c.CPrintDate, APlates = a.APlates, AMoney = a.AMoney, CPlates = c.CPlates, CMoney = c.CMoney }).ToList();

            var Union = leftOuterJoin.Union(rightOuterJoin).OrderBy(a => a.PrintDate).ToList();

            var total = Union.Sum(a => a.AMoney + a.CMoney);
            var tax = Math.Round((decimal)(total * 0.05), 0, MidpointRounding.AwayFromZero);
            var totalPayment = Math.Round((decimal)(total * 1.05), 0, MidpointRounding.AwayFromZero);

            using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                //載入Excel檔案
                using (ExcelPackage ep = new ExcelPackage(fs))
                {
                    #region sheet 支出證明單
                    ExcelWorksheet sheet1 = ep.Workbook.Worksheets["支出證明單"];
                    sheet1.Cells[4, 4].Value = supplierCode.Substring(supplierCode.IndexOf("-") + 1);
                    sheet1.Cells[5, 4].Value = totalPayment + "(含稅)";
                    sheet1.Cells[6, 4].Value = (end.Year - 1911) + "年" + end.Month + "月 A段C段費用";
                    sheet1.Cells[6, 17].Value = total;
                    sheet1.Cells[7, 17].Value = tax;
                    sheet1.Cells[8, 17].Value = totalPayment;
                    #endregion

                    #region sheet 總表
                    ExcelWorksheet sheet2 = ep.Workbook.Worksheets["總表"];
                    sheet2.Cells.Style.Font.Size = 11;
                    sheet2.Cells[3, 3].Value = supplierCode;
                    for (int row = 0; row < Union.Count; row++)
                    {
                        sheet2.Cells.Style.Font.Name = "微軟正黑體";
                        sheet2.Cells.Style.Font.Size = 11;
                        sheet2.Row(6 + row).Height = 18;
                        sheet2.Cells[6 + row, 1].Value = row + 1;
                        sheet2.Cells[6 + row, 2].Value = Union[row].PrintDate.Date.ToString("yyyy/MM/dd");
                        sheet2.Cells[6 + row, 3].Value = Union[row].APlates;
                        sheet2.Cells[6 + row, 4].Value = Union[row].AMoney;
                        sheet2.Cells[6 + row, 5].Value = Union[row].CPlates;
                        sheet2.Cells[6 + row, 6].Value = Union[row].CMoney;
                        sheet2.Cells[6 + row, 1, 6 + row, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin; //列 欄 列 欄
                        sheet2.Cells[6 + row, 1, 6 + row, 6].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        sheet2.Cells[6 + row, 1, 6 + row, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet2.Cells[6 + row, 6].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                        sheet2.Cells[6 + row, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    }
                    sheet2.Cells[6 + Union.Count, 1].Value = "小計";
                    sheet2.Cells[6 + Union.Count, 1, 6 + Union.Count, 6].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    sheet2.Cells[6 + Union.Count, 1, 6 + Union.Count, 2].Merge = true; //列 欄 列 欄
                    sheet2.Cells[6 + Union.Count, 3].Value = Union.Sum(a => a.APlates);
                    sheet2.Cells[6 + Union.Count, 4].Value = Union.Sum(a => a.AMoney);
                    sheet2.Cells[6 + Union.Count, 5].Value = Union.Sum(a => a.CPlates);
                    sheet2.Cells[6 + Union.Count, 6].Value = Union.Sum(a => a.CMoney);
                    sheet2.Cells[6 + Union.Count + 1, 1].Value = "合計";
                    sheet2.Cells[6 + Union.Count + 1, 1, 6 + Union.Count + 1, 2].Merge = true;
                    sheet2.Cells[6 + Union.Count + 1, 6].Value = total;
                    sheet2.Cells[6 + Union.Count + 2, 1].Value = "稅額";
                    sheet2.Cells[6 + Union.Count + 2, 1, 6 + Union.Count + 2, 2].Merge = true;
                    sheet2.Cells[6 + Union.Count + 2, 6].Value = tax;
                    sheet2.Cells[6 + Union.Count + 3, 1].Value = "總合計";
                    sheet2.Cells[6 + Union.Count + 3, 1, 6 + Union.Count + 3, 2].Merge = true;
                    sheet2.Cells[6 + Union.Count + 3, 6].Value = totalPayment;
                    sheet2.Cells[6 + Union.Count, 1, 6 + Union.Count + 3, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    sheet2.Cells[6 + Union.Count, 1, 6 + Union.Count + 3, 6].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet2.Cells[6 + Union.Count, 1, 6 + Union.Count + 3, 6].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    sheet2.Cells[6 + Union.Count, 1, 6 + Union.Count + 3, 1].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                    sheet2.Cells[6 + Union.Count, 6, 6 + Union.Count + 3, 6].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    sheet2.Cells[6 + Union.Count + 3, 1, 6 + Union.Count + 3, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                    sheet2.Cells[6 + Union.Count + 8, 3].Value = "審核：";
                    sheet2.Cells.Style.Font.Size = 11;
                    sheet2.Cells[6 + Union.Count + 8, 3].Style.Font.Size = 18;
                    sheet2.Cells[6 + Union.Count + 8, 5].Value = "製表：";
                    sheet2.Cells[6 + Union.Count + 8, 5].Style.Font.Size = 18;
                    sheet2.Cells[1, 1].Style.Font.Size = 20;
                    sheet2.Cells[2, 1].Style.Font.Size = 14;
                    sheet2.Cells[3, 1, 3, 3].Style.Font.Size = 16;
                    sheet2.Cells.Style.Font.Name = "微軟正黑體";
                    #endregion

                    #region sheet A段明細
                    ExcelWorksheet sheet3 = ep.Workbook.Worksheets["A段明細"];
                    sheet3.Cells.Style.Font.Name = "微軟正黑體";
                    sheet3.Cells.Style.Font.Size = 10;
                    for (int row = 0; row < ASection.Count; row++)
                    {
                        sheet3.Cells[row + 2, 1].Value = row + 1;
                        sheet3.Cells[row + 2, 2].Value = ASection[row].PrintDate.Value.Date.ToString("yyyy/MM/dd");
                        sheet3.Cells[row + 2, 3].Value = ASection[row].TaskId;
                        sheet3.Cells[row + 2, 4].Value = ASection[row].CheckNumber;
                        sheet3.Cells[row + 2, 5].Value = ASection[row].SendStation;
                        sheet3.Cells[row + 2, 6].Value = ASection[row].SendContact;
                        sheet3.Cells[row + 2, 7].Value = ASection[row].ReceiveContact;
                        sheet3.Cells[row + 2, 8].Value = ASection[row].ReceiveCity.Substring(0, 3);
                        sheet3.Cells[row + 2, 9].Value = ASection[row].ReceiveCity.Substring(3);
                        sheet3.Cells[row + 2, 10].Value = ASection[row].Pieces;
                        sheet3.Cells[row + 2, 11].Value = ASection[row].Plates;
                        sheet3.Cells[row + 2, 12].Value = ASection[row].ReceiveAddress;
                        sheet3.Cells[row + 2, 13].Value = ASection[row].ADriver;
                        sheet3.Cells[row + 2, 14].Value = ASection[row].ASectionPayment;
                        sheet3.Cells[row + 2, 1, row + 2, 14].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet3.Cells[row + 2, 1, row + 2, 14].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        sheet3.Cells[row + 2, 1, row + 2, 14].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    }
                    #endregion

                    #region sheet C段明細
                    ExcelWorksheet sheet4 = ep.Workbook.Worksheets["C段明細"];
                    sheet4.Cells.Style.Font.Name = "微軟正黑體";
                    sheet4.Cells.Style.Font.Size = 10;
                    for (int row = 0; row < CSection.Count; row++)
                    {
                        sheet4.Cells[row + 2, 1].Value = row + 1;
                        sheet4.Cells[row + 2, 2].Value = CSection[row].PrintDate.Value.Date.ToString("yyyy/MM/dd");
                        sheet4.Cells[row + 2, 3].Value = CSection[row].CheckNumber;
                        sheet4.Cells[row + 2, 4].Value = CSection[row].SendStation;
                        sheet4.Cells[row + 2, 5].Value = CSection[row].SendContact;
                        sheet4.Cells[row + 2, 6].Value = CSection[row].ReceiveContact;
                        sheet4.Cells[row + 2, 7].Value = CSection[row].ReceiveCity.Substring(0, 3);
                        sheet4.Cells[row + 2, 8].Value = CSection[row].ReceiveCity.Substring(3);
                        sheet4.Cells[row + 2, 9].Value = CSection[row].Pieces;
                        sheet4.Cells[row + 2, 10].Value = CSection[row].Plates;
                        sheet4.Cells[row + 2, 11].Value = CSection[row].ReceiveAddress;
                        sheet4.Cells[row + 2, 12].Value = CSection[row].CDriver;
                        sheet4.Cells[row + 2, 13].Value = CSection[row].CShippingFee;
                        sheet4.Cells[row + 2, 14].Value = CSection[row].RemoteFee;
                        sheet4.Cells[row + 2, 15].Value = CSection[row].CSectionSpecialFee;
                        sheet4.Cells[row + 2, 16].Value = CSection[row].CSectionPayment;
                        sheet4.Cells[row + 2, 1, row + 2, 16].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        sheet4.Cells[row + 2, 1, row + 2, 16].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        sheet4.Cells[row + 2, 1, row + 2, 16].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    }
                    #endregion

                    return ep.GetAsByteArray();
                }
            }
        }

        public Stream GenerateZipFile(List<InputJson> inputJsons)
        {
            var filenamesAndBytes = new Dictionary<string, byte[]>();

            foreach (var item in inputJsons)
            {
                var endYear = Convert.ToInt32(item.RequestMonth.Substring(0, item.RequestMonth.IndexOf("年")));
                var endMonth = Convert.ToInt32(item.RequestMonth.Substring(item.RequestMonth.IndexOf("年") + 1, item.RequestMonth.IndexOf("月") - item.RequestMonth.IndexOf("年") - 1));
                var endDate = new DateTime(endYear, endMonth, 25);
                byte[] oneExcel = Export(endDate, item.SupplierName);
                filenamesAndBytes.Add(DateTime.Now.ToString("yyyy_MM_dd") + "_" + item.SupplierName + "_" + endMonth + "月份請款單.xlsx", oneExcel);
            }

            var zip = ZipData(filenamesAndBytes);
            var memoryStream = new MemoryStream(zip);

            return memoryStream;
        }
        public byte[] ZipData(Dictionary<string, byte[]> data)
        {
            using (var zipStream = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Update))
                {
                    foreach (var fileName in data.Keys)
                    {
                        var entry = zipArchive.CreateEntry(fileName);
                        using (var entryStream = entry.Open())
                        {
                            byte[] bytes = data[fileName];
                            entryStream.Write(bytes, 0, bytes.Length);
                        }
                    }
                }
                return zipStream.ToArray();
            }
        }
    }
}
