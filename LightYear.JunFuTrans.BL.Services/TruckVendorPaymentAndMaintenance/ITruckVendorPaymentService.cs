﻿using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.BL.Services.TruckVendorPaymentAndMaintenance
{
    public interface ITruckVendorPaymentService
    {
        List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendor(DateTime start, DateTime end, string vendor);
        List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor(DateTime start, DateTime end, string vendor);
        PaymentEntity GetIncomeAndExpenditure(DateTime start, DateTime end, string vendor);
        List<TruckVendorPaymentLog> GetLatestUploadData(string ImportRandomCode = "");
        List<TbAccount> GetSuppliersAccount();
        TbAccount GetByAccount(string account);
        TruckVendor GetTruckVendorByTruckVendorAccountCode(string accountCode);

        List<TruckVendor> GetTruckVendors();

        List<DropDownListEntity> GetFeeTypes();

        List<DropDownListEntity> GetCompanies();

        List<FrontEndShowEntity> GetIncomeFeeType();

        List<FrontEndShowEntity> GetExpenditureFeeType();

        Task<bool> SaveExpenditureFeeType(FrontEndShowEntity entity);

        Task<bool> SaveIncomeFeeType(FrontEndShowEntity entity);

        List<TbItemCode> GetCompanyTbItemCodes();
        List<TruckVendor> GetTruckVendorsForMaintenance();


        void SaveTruckVendor(TruckVendor truckVendor);
        List<int> Save(List<TruckVendorPaymentLog> truckVendorPaymentLog);
        void Update(TruckVendorPaymentLog truckVendorPaymentLog);

        void UpdateVendorLog(TruckVendorPaymentLog truckVendorPaymentLog);

        void Delete(TruckVendorPaymentLogFrontEndShowEntity entity);

        byte[] Export();

        List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByTimePeriodAndDuty(DateTime start, DateTime end, string dutyDept);

        List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByDuty(string dutyDept);

        List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByTimePeriod(DateTime start, DateTime end);

        KendoSelectDeptEntity GetJFDept(bool addTotal = false);
    }
}
