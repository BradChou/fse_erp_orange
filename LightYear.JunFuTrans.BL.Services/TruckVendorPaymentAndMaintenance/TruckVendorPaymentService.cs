﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LightYear.JunFuTrans.BL.BE.Account;
using LightYear.JunFuTrans.BL.BE.TruckVendorPaymentAndMaintenance;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.JunFuTransDb;
using LightYear.JunFuTrans.DA.Repositories.TruckVendorPaymentAndMaintenance;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using AutoMapper;
using LightYear.JunFuTrans.DA.Repositories.Account;
using LightYear.JunFuTrans.DA.Repositories.Station;
using System.Threading.Tasks;

namespace LightYear.JunFuTrans.BL.Services.TruckVendorPaymentAndMaintenance
{
    public class TruckVendorPaymentService : ITruckVendorPaymentService
    {
        public ITruckVendorPaymentRepository TruckVendorPaymentRepository { get; set; }
        public ITruckVendorMaintenanceRepository TruckVendorMaintenanceRepository { get; set; }
        public IAccountsRepository AccountsRepository { get; set; }
        public JunFuDbContext JunFuDbContext { get; set; }
        public IMapper Mapper { get; private set; }
        public IStationAreaRepository StationAreaRepository { get; set; }

        public ITbItemCodesRepository TbItemCodesRepository { get; set; }

     public TruckVendorPaymentService(ITruckVendorPaymentRepository truckVendorPaymentRepository, ITruckVendorMaintenanceRepository truckVendorMaintenanceRepository
            , JunFuDbContext junFuDbContext, IMapper mapper, IAccountsRepository accountsRepository, IStationAreaRepository station, ITbItemCodesRepository tbItemCodesRepository)
        {
            TruckVendorPaymentRepository = truckVendorPaymentRepository;
            TruckVendorMaintenanceRepository = truckVendorMaintenanceRepository;
            JunFuDbContext = junFuDbContext;
            this.Mapper = mapper;
            AccountsRepository = accountsRepository;
            StationAreaRepository = station;
            TbItemCodesRepository = tbItemCodesRepository;
        }

        public List<TruckVendorPaymentLog> GetAllByMonthAndTruckVendor(DateTime start, DateTime end, string vendor)
        {
            var feetypes = TruckVendorPaymentRepository.GetFeeTypes();
            var companies = TruckVendorPaymentRepository.GetCompanies();
            var vendornames = TruckVendorPaymentRepository.GetTruckVendors();

            List<TruckVendorPaymentLog> data = TruckVendorPaymentRepository.GetAllByMonthAndTruckVendor1(start, end, vendor);

            foreach (var d in data)
            {
                foreach (var f in feetypes)
                {
                    if (d.FeeType == f.Id)
                    {
                        d.FeeType = f.Name;
                        break;
                    }
                }

                foreach (var c in companies)
                {
                    if (d.Company == c.Id)
                    {
                        d.Company = c.Name;
                        break;
                    }
                }

                foreach (var v in vendornames)
                {
                    if (d.VendorName == v.Id.ToString())
                    {
                        d.VendorName = v.VendorName;
                        break;
                    }
                }
            }
            return data;
        }

        public List<DisbursementEntity> GetDisbursementByMonthAndTruckVendor(DateTime start, DateTime end, string vendor)
        {
            List<DropDownListEntity> feetypes = TruckVendorPaymentRepository.GetFeeTypes();
            List<DisbursementEntity> disbursementEntities = TruckVendorPaymentRepository.GetDisbursementByMonthAndTruckVendor(start, end, vendor);

            foreach (var d in disbursementEntities)
            {
                d.FeeType = feetypes.Where(a => a.Id == d.FeeType).FirstOrDefault()?.Name;
            }
            return disbursementEntities;
        }

        public PaymentEntity GetIncomeAndExpenditure(DateTime start, DateTime end, string vendor)
        {
            return TruckVendorPaymentRepository.GetIncomeAndExpenditure(start, end, vendor);
        }

        public List<int> Save(List<TruckVendorPaymentLog> truckVendorPaymentLogs)
        {
            var feetypes = GetFeeTypes();
            var companies = GetCompanies();
            var vendornames = GetTruckVendors();
            List<int> ids = new List<int>();

            if (truckVendorPaymentLogs[0].Id == 0 && truckVendorPaymentLogs[0].ImportRandomCode != null) //整批匯入
            {
                foreach (var truckVendorPaymentLog in truckVendorPaymentLogs)
                {
                    truckVendorPaymentLog.FeeType = feetypes.Where(a => a.Name.Equals(truckVendorPaymentLog.FeeType)).FirstOrDefault()?.Id;
                    truckVendorPaymentLog.Company = companies.Where(a => a.Name.Equals(truckVendorPaymentLog.Company)).FirstOrDefault()?.Id;
                    truckVendorPaymentLog.VendorName = vendornames.Where(a => a.VendorName.Equals(truckVendorPaymentLog.VendorName)).FirstOrDefault()?.Id.ToString();
                    truckVendorPaymentLog.CreateDate = DateTime.Now;
                }
                ids = TruckVendorPaymentRepository.InsertBatch(truckVendorPaymentLogs);
                return ids;
            }

            var forInsert = truckVendorPaymentLogs.Where(a => a.Id == 0 && a.ImportRandomCode == null).ToList(); // 多筆新增

            if (forInsert.Count > 0)
            {
                foreach (var truckVendorPaymentLog in forInsert)
                {
                    truckVendorPaymentLog.FeeType = feetypes.Where(a => a.Name.Equals(truckVendorPaymentLog.FeeType)).FirstOrDefault()?.Id;
                    truckVendorPaymentLog.Company = companies.Where(a => a.Name.Equals(truckVendorPaymentLog.Company)).FirstOrDefault()?.Id;
                    truckVendorPaymentLog.VendorName = vendornames.Where(a => a.VendorName.Equals(truckVendorPaymentLog.VendorName)).FirstOrDefault()?.Id.ToString();
                    truckVendorPaymentLog.ImportRandomCode = "";
                    truckVendorPaymentLog.RegisteredDate = truckVendorPaymentLog.RegisteredDate.Value.AddHours(8); //kendo grid新增或編輯時間要加8小時，因為台灣在UTC+8
                    truckVendorPaymentLog.CreateDate = DateTime.Now;
                }
                ids = TruckVendorPaymentRepository.InsertBatch(forInsert);
            }

            return ids;
        }

        public void Update(TruckVendorPaymentLog truckVendorPaymentLog)
        {
            var feetypes = GetFeeTypes();
            var companies = GetCompanies();
            var vendornames = GetTruckVendors();
            truckVendorPaymentLog.FeeType = feetypes.Where(a => a.Name.Equals(truckVendorPaymentLog.FeeType)).FirstOrDefault()?.Id;
            truckVendorPaymentLog.Company = companies.Where(a => a.Name.Equals(truckVendorPaymentLog.Company)).FirstOrDefault()?.Id;
            truckVendorPaymentLog.VendorName = vendornames.Where(a => a.VendorName.Equals(truckVendorPaymentLog.VendorName)).FirstOrDefault()?.Id.ToString();
            truckVendorPaymentLog.UpdateDate = DateTime.Now;
            TruckVendorPaymentRepository.Update(truckVendorPaymentLog);
        }

        public void UpdateVendorLog(TruckVendorPaymentLog truckVendorPaymentLog)
        {
            var feetypes = GetFeeTypes();
            var companies = GetCompanies();
            var vendornames = GetTruckVendors();
            truckVendorPaymentLog.FeeType = feetypes.Where(a => a.Name.Equals(truckVendorPaymentLog.FeeType)).FirstOrDefault()?.Id;
            truckVendorPaymentLog.Company = companies.Where(a => a.Name.Equals(truckVendorPaymentLog.Company)).FirstOrDefault()?.Id;
            truckVendorPaymentLog.VendorName = vendornames.Where(a => a.VendorName.Equals(truckVendorPaymentLog.VendorName)).FirstOrDefault()?.Id.ToString();
            truckVendorPaymentLog.UpdateDate = DateTime.Now;
            truckVendorPaymentLog.RegisteredDate = truckVendorPaymentLog.RegisteredDate;
            truckVendorPaymentLog.CarLicense = truckVendorPaymentLog.CarLicense;
            truckVendorPaymentLog.Payment = truckVendorPaymentLog.Payment;
            truckVendorPaymentLog.Memo = truckVendorPaymentLog.Memo;
            TruckVendorPaymentRepository.UpdateVendorPayment(truckVendorPaymentLog);
        }

        public void Delete(TruckVendorPaymentLogFrontEndShowEntity entity)
        {
            TruckVendorPaymentLog truckVendorPaymentLog = Mapper.Map<TruckVendorPaymentLog>(entity);

            TruckVendorPaymentRepository.Delete(truckVendorPaymentLog);
        }

        public byte[] Export()
        {
            var memoryStream = new MemoryStream();
            using (var document = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {
                var workbookPart = document.AddWorkbookPart();
                workbookPart.Workbook = new Workbook();

                var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                var sheets = workbookPart.Workbook.AppendChild(new Sheets());
                sheets.Append(new Sheet()
                {
                    Id = workbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "工作表1"
                });

                // 從 Worksheet 取得要編輯的 SheetData
                var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                // 建立資料列物件
                var row = new Row();
                // 在資料列中插入欄位
                row.Append(
                    new Cell() { CellValue = new CellValue("發生日期"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("交易項目"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("供應商名稱"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("車號"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("金額"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("摘要"), DataType = CellValues.String },
                    new Cell() { CellValue = new CellValue("公司別"), DataType = CellValues.String }
                );
                // 插入資料列 
                sheetData.AppendChild(row);

                List<string> FeeTypesName = GetFeeTypes().Select(a => a.Name).ToList();
                string DropDownFeeTypeName = string.Join(",", FeeTypesName);

                //List<string> TruckVendorsName = GetTruckVendors().Select(a => a.VendorName).ToList();
                //string DropDownTruckVendorsName = string.Join(",", TruckVendorsName);

                List<string> CompaniesName = GetCompanies().Select(a => a.Name).ToList();
                string DropDownCompaniesName = string.Join(",", CompaniesName);

                DataValidations dataValidations = new DataValidations();

                //交易項目下拉選單
                DataValidation dataValidation = new DataValidation()
                {
                    Type = DataValidationValues.List,
                    AllowBlank = true,
                    SequenceOfReferences = new ListValue<StringValue>() { InnerText = "B:B" }
                };
                Formula1 formula = new Formula1();
                formula.Text = "\"" + DropDownFeeTypeName + "\"";

                //公司別下拉選單
                DataValidation dataValidation1 = new DataValidation()
                {
                    Type = DataValidationValues.List,
                    AllowBlank = true,
                    SequenceOfReferences = new ListValue<StringValue>() { InnerText = "G:G" }
                };
                Formula1 formula1 = new Formula1();
                formula1.Text = "\"" + DropDownCompaniesName + "\"";

                dataValidation.Append(formula);
                dataValidation1.Append(formula1);
                dataValidations.Append(dataValidation);
                dataValidations.Append(dataValidation1);

                worksheetPart.Worksheet.AppendChild(dataValidations);

                workbookPart.Workbook.Save();
                document.Close();
            }
            return memoryStream.ToArray();
        }

        public List<TruckVendorPaymentLog> GetLatestUploadData(string ImportRandomCode = "")
        {
            var feetypes = TruckVendorPaymentRepository.GetFeeTypes();
            var companies = TruckVendorPaymentRepository.GetCompanies();
            var vendornames = TruckVendorPaymentRepository.GetTruckVendors();

            List<TruckVendorPaymentLog> data = TruckVendorPaymentRepository.GetLatestUploadData(ImportRandomCode);

            int i = 1;
            foreach (var d in data)
            {
                foreach (var f in feetypes)
                {
                    if (d.FeeType == f.Id)
                    {
                        d.FeeType = f.Name;
                        break;
                    }
                }

                foreach (var c in companies)
                {
                    if (d.Company == c.Id)
                    {
                        d.Company = c.Name;
                        break;
                    }
                }

                foreach (var v in vendornames)
                {
                    if (d.VendorName == v.Id.ToString())
                    {
                        d.VendorName = v.VendorName;
                        break;
                    }
                }

                d.Id = i++;
            }
            return data;
        }

        public List<TbAccount> GetSuppliersAccount()
        {
            return AccountsRepository.GetSuppliersAccount();
        }

        public TbAccount GetByAccount(string account)
        {
            return AccountsRepository.GetByAccountCode(account);
        }

        public TruckVendor GetTruckVendorByTruckVendorAccountCode(string accountCode)
        {
            return TruckVendorPaymentRepository.GetTruckVendorByTruckVendorAccountCode(accountCode);
        }

        #region 供應商
        public List<TruckVendor> GetTruckVendorsForMaintenance()
        {
            return TruckVendorMaintenanceRepository.GetTruckVendorsForMaintenance();
        }

        public void SaveTruckVendor(TruckVendor truckVendor)
        {
            truckVendor.VendorName = truckVendor.VendorName.Trim();

            if (truckVendor.Id == 0)
            {
                truckVendor.CreateDate = DateTime.Now;
                TruckVendorMaintenanceRepository.Insert(truckVendor);
            }
            else
            {
                truckVendor.UpdateDate = DateTime.Now;
                TruckVendorMaintenanceRepository.Update(truckVendor);
            }
        }
        #endregion

        #region 公司別

        public List<TbItemCode> GetCompanyTbItemCodes()
        {
            return TruckVendorMaintenanceRepository.GetCompanyTbItemCodes();
        }

        #endregion

        #region 交易項目

        public List<FrontEndShowEntity> GetIncomeFeeType()
        {
            List<TbItemCode> incomeFeeItemCodeList = TruckVendorMaintenanceRepository.GetIncomeFeeTypeTbItemCodes();
            List<TbItemMappingCode> incomeFeeMappingJFDeptList = TruckVendorMaintenanceRepository.GetJFDeptFeeTypeMappingTbItemCodes();
            List<TbItemCode> JFDeptList = TruckVendorMaintenanceRepository.GetJFDepartment();

            var data = from incomeFee in incomeFeeItemCodeList
                       join incomeFeeMappingJFDept in incomeFeeMappingJFDeptList on incomeFee.CodeId equals incomeFeeMappingJFDept.MappingBId into temp
                       from incomeFeeMappingJFDept in temp.DefaultIfEmpty()
                       join JFDept in JFDeptList on incomeFeeMappingJFDept?.MappingAId equals JFDept?.CodeId into temp2
                       from JFDept in temp2.DefaultIfEmpty()
                       select new FrontEndShowEntity
                       {
                           Seq = incomeFee.Seq,
                           CodeId = incomeFee.CodeId,
                           CodeName = incomeFee.CodeName,
                           ActiveFlag = incomeFee.ActiveFlag,
                           DutyDept = JFDept?.CodeName
                       };

            return data.ToList();
        }

        public List<FrontEndShowEntity> GetExpenditureFeeType()
        {
            List<TbItemCode> expendFeeItemCode = TruckVendorMaintenanceRepository.GetExpenditureFeeTypeTbItemCodes();
            List<TbItemMappingCode> expendFeeMappingJFDeptList = TruckVendorMaintenanceRepository.GetJFDeptFeeTypeMappingTbItemCodes();
            List<TbItemCode> JFDeptList = TruckVendorMaintenanceRepository.GetJFDepartment();

            var data = from expendFee in expendFeeItemCode
                       join incomeFeeMappingJFDept in expendFeeMappingJFDeptList on expendFee.CodeId equals incomeFeeMappingJFDept.MappingBId into temp
                       from incomeFeeMappingJFDept in temp.DefaultIfEmpty()
                       join JFDept in JFDeptList on incomeFeeMappingJFDept?.MappingAId equals JFDept?.CodeId into temp2
                       from JFDept in temp2.DefaultIfEmpty()
                       select new FrontEndShowEntity 
                       { Seq = expendFee.Seq, CodeId = expendFee.CodeId, CodeName = expendFee.CodeName, 
                           ActiveFlag = expendFee.ActiveFlag, DutyDept = JFDept?.CodeName };

            return data.ToList();
        }

        public async Task<bool> SaveExpenditureFeeType(FrontEndShowEntity entity)
        {
            TbItemCode tbItemCode = Mapper.Map<TbItemCode>(entity);
            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
            await webServiceSoapClient.SaveExpenditureFeeTypeAsync(tbItemCode.CodeName.Trim(), tbItemCode.ActiveFlag ?? false, tbItemCode.Seq);

            // TbMappingItemCode A:Dept -> B:expendFee
            int deptCodeId;
            int.TryParse(entity.DutyDept, out deptCodeId);

            int codeId;
            codeId = entity.CodeId == "" || entity.CodeId == "0" ? int.Parse(TruckVendorMaintenanceRepository.GetLastFeeCodeId()) : int.Parse(entity.CodeId);

            int mappingId = TruckVendorMaintenanceRepository.GetMappingIdByFeeType(codeId);

            return await webServiceSoapClient.UpdateJfDeptFeeTypeMappingCodeAsync(mappingId, deptCodeId, codeId, true);
        }

        public async Task<bool> SaveIncomeFeeType(FrontEndShowEntity entity)
        {
            TbItemCode tbItemCode = Mapper.Map<TbItemCode>(entity);
            JunFuWebServiceInService.WebServiceSoapClient webServiceSoapClient = new JunFuWebServiceInService.WebServiceSoapClient(JunFuWebServiceInService.WebServiceSoapClient.EndpointConfiguration.WebServiceSoap);
            await webServiceSoapClient.SaveIncomeFeeTypeAsync(tbItemCode.CodeName.Trim(), tbItemCode.ActiveFlag ?? false, tbItemCode.Seq);

            // TbMappingItemCode A:Dept -> B:expendFee
            int deptCodeId;
            int.TryParse(entity.DutyDept, out deptCodeId);

            int codeId;
            codeId = entity.CodeId == "" || entity.CodeId == "0" ? int.Parse(TruckVendorMaintenanceRepository.GetLastFeeCodeId()) : int.Parse(entity.CodeId);

            int mappingId = TruckVendorMaintenanceRepository.GetMappingIdByFeeType(codeId);

            return await webServiceSoapClient.UpdateJfDeptFeeTypeMappingCodeAsync(mappingId, deptCodeId, codeId, true);
        }
        #endregion

        #region 下拉選單

        public List<DropDownListEntity> GetFeeTypes()
        {
            return TruckVendorPaymentRepository.GetFeeTypes();
        }

        public List<DropDownListEntity> GetCompanies()
        {
            return TruckVendorPaymentRepository.GetCompanies();
        }

        public List<TruckVendor> GetTruckVendors()
        {
            return TruckVendorPaymentRepository.GetTruckVendors();
        }
        #endregion

        #region 編輯收支明細
        public List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByTimePeriodAndDuty(DateTime start, DateTime end, string dutyDept)
        {
            List<string> feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(dutyDept);

            var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByTimePeriodAndFeeType(start, end, feeTypes.ToArray());

            var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);

            paymentEntity = ConvertCodeIdToName(paymentEntity);

            return paymentEntity;
        }

        public List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByDuty(string dutyDept)
        {
            List<string> feeTypes = TbItemCodesRepository.GetFeeTypesCodeByDeptCodeId(dutyDept);

            var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByFeeType(feeTypes.ToArray());

            var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);

            paymentEntity = ConvertCodeIdToName(paymentEntity);

            return paymentEntity;
        }

        public List<TruckVendorPaymentLogFrontEndShowEntity> GetFrontEndEntityByTimePeriod(DateTime start, DateTime end)
        {
            var paymentLogRepository = TruckVendorPaymentRepository.GetPaymentByTimePeriod(start, end);

            var paymentEntity = this.Mapper.Map<List<TruckVendorPaymentLogFrontEndShowEntity>>(paymentLogRepository);

            return ConvertCodeIdToName(paymentEntity);
        }

        private List<TruckVendorPaymentLogFrontEndShowEntity> ConvertCodeIdToName(List<TruckVendorPaymentLogFrontEndShowEntity> paymentEntity)
        {
            var dutyMapping = TbItemCodesRepository.GetDutyDeptMapping();
            var department = TruckVendorMaintenanceRepository.GetJFDepartment();
            var company = TruckVendorMaintenanceRepository.GetCompanyTbItemCodes();
            var feeType = TruckVendorMaintenanceRepository.GetFeeTypeTbItemCodes();
            var vendors = TruckVendorMaintenanceRepository.GetTruckVendorsForMaintenance();

            foreach (var e in paymentEntity)
            {
                if (e.FeeType != null && dutyMapping.ContainsKey(e.FeeType))
                {
                    var deptId = dutyMapping[e.FeeType];
                    e.DutyDept = department.FirstOrDefault(c => c.CodeId == deptId)?.CodeName;
                }

                e.CompanyType = company.FirstOrDefault(c => c.CodeId == e.CompanyType)?.CodeName;
                e.FeeType = feeType.FirstOrDefault(c => c.CodeId == e.FeeType)?.CodeName;
                e.VendorName = vendors.FirstOrDefault(c => c.Id.ToString() == e.VendorName)?.VendorName;
            }

            return paymentEntity;
        }

        public KendoSelectDeptEntity GetJFDept(bool addTotal = false)
        {
            List<JFDepartmentDropDownListEntity> areas = StationAreaRepository.GetJFDepartment();

            if (addTotal)
            {
                areas.Insert(0, new JFDepartmentDropDownListEntity("*", "全部部門"));
            }

            return new KendoSelectDeptEntity { results = areas };
        }
        #endregion
    }
}
