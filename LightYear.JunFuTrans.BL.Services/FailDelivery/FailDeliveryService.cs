﻿using LightYear.JunFuTrans.BL.BE.FailDelivery;
using LightYear.JunFuTrans.DA.JunFuDb;
using LightYear.JunFuTrans.DA.Repositories.FailDelivery;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using AutoMapper;

namespace LightYear.JunFuTrans.BL.Services.FailDelivery
{
    public class FailDeliveryService : IFailDeliveryService
    {
        public FailDeliveryService(IFailDeliveryRepository failDeliveryRepository, IMapper mapper)
        {
            this.FailDeliveryRepository = failDeliveryRepository;
            this.Mapper = mapper;
        }

        public IFailDeliveryRepository FailDeliveryRepository { get; set; }
        public IMapper Mapper { get; private set; }

        public List<FailDeliveryEntity> GetFailDeliveryList(FailDeliveryInputEntity failDeliveryInputEntity)
        {
            if (failDeliveryInputEntity.CustomerCode == "請選擇一客戶")
            {
                failDeliveryInputEntity.CustomerCode = "全客代";
            }
            if (failDeliveryInputEntity.CustomerCode.IndexOf("F") != -1)
            {
                failDeliveryInputEntity.CustomerCode = GetAccountCodeAndStationInString(failDeliveryInputEntity.CustomerCode);
            }
            if (failDeliveryInputEntity.SupplierStation.IndexOf("F") != -1)
            {
                failDeliveryInputEntity.SupplierCode = GetAccountCodeAndStationInString(failDeliveryInputEntity.SupplierStation);
            }

            List<StationEntity> stationList = FailDeliveryRepository.GetAllStations();

            if (failDeliveryInputEntity.StartInput == null || failDeliveryInputEntity.EndInput == null)
            {
                failDeliveryInputEntity.Start = DateTime.Parse(DateTime.Now.ToString().Split(" ")[0]);
                failDeliveryInputEntity.End = DateTime.Parse(DateTime.Now.ToString().Split(" ")[0]);
            }
            else
            {
                failDeliveryInputEntity.Start = DateTime.Parse(failDeliveryInputEntity.StartInput.ToString());      //由於左邊是DateTime,右邊是DateTime?，才多ToString步驟
                failDeliveryInputEntity.End = DateTime.Parse(failDeliveryInputEntity.EndInput.ToString());
            }

            if (failDeliveryInputEntity.SupplierStation == "全部站所" & failDeliveryInputEntity.IsMaster == true)
            {
                failDeliveryInputEntity.SupplierCode = "All";
            }
            else if (failDeliveryInputEntity.IsMaster == false)
            {
                failDeliveryInputEntity.SupplierCode = failDeliveryInputEntity.AccountStation;
            }

            if (failDeliveryInputEntity.CustomerCode == "全客代")
            {
                if (failDeliveryInputEntity.IsMaster == true)
                {
                    failDeliveryInputEntity.CustomerCode = "All";
                }
                else
                {
                    failDeliveryInputEntity.CustomerCode = "Partial";
                }
            }

            if (failDeliveryInputEntity.AbnormalRemarks == 0 & (failDeliveryInputEntity.AbnormalRemarksText != null & failDeliveryInputEntity.AbnormalRemarksText != ""))
            {
                failDeliveryInputEntity.AbnormalRemarks = AbnormalRemarkTextInterpreter(failDeliveryInputEntity.AbnormalRemarksText);
            }

            foreach (var i in stationList)
            {
                if (i.Name == failDeliveryInputEntity.SupplierStation)
                {
                    failDeliveryInputEntity.SupplierCode = i.Code;
                }
            }

            List<FailDeliveryEntity> failDeliveryList = FailDeliveryRepository.FailDeliveryEntities(failDeliveryInputEntity);
            /*if (failDeliveryInputEntity.IsMaster)
            {
                if (failDeliveryInputEntity.CustomerCode != "All")
                {
                    failDeliveryList = FailDeliveryRepository.FailDeliveryEntitiesOneAccount(failDeliveryInputEntity);
                } 
                else if (failDeliveryInputEntity.SupplierCode != "All")
                {
                    failDeliveryList = FailDeliveryRepository.FailDeliveryEntitiesOneStationAllAccount(failDeliveryInputEntity);
                } 
                else
                {
                    failDeliveryList = FailDeliveryRepository.FailDeliveryEntitiesAllStationAllAccount(failDeliveryInputEntity);
                }
            }
            else
            {
                if (failDeliveryInputEntity.CustomerCode != "All")
                {
                    failDeliveryList = FailDeliveryRepository.FailDeliveryEntitiesOneAccount(failDeliveryInputEntity);
                }
                else
                {
                    failDeliveryList = FailDeliveryRepository.FailDeliveryEntitiesLoginStationAllAccount(failDeliveryInputEntity);
                }
            }*/

            for (var i = 0; i < failDeliveryList.Count; i++)
            {
                if (failDeliveryList[i].ExceptionOption != "3")
                {
                    failDeliveryList.RemoveAt(i);
                    i--;
                }
            }

            List<CustomerShortNameEntity> shortNameList = FailDeliveryRepository.GetCustomerShortNameEntities();

            foreach (var i in failDeliveryList)
            {
                foreach (var j in shortNameList)
                {
                    if (i.CustomerCode == j.CustomerCode)
                    {
                        i.CustomerShortName = j.CustomerShortName;
                    }
                }

                foreach (var j in stationList)
                {
                    if (i.SupplierCode == j.Code)
                    {
                        i.SupplierStation = j.Name;
                    }
                    if (i.ArriveCode == j.Scode)
                    {
                        i.ArriveStation = j.Name;
                    }
                }

                if (i.AbnormalRemarks != 0)
                {
                    i.AbnormalRemarksText = AbnormalRemarkInterpreter(i.AbnormalRemarks);
                }

                i.ShipDateString = ChangeDateTimeForm(DateTime.Parse(i.ShipDate.ToString()));
            }

            for (var i = 0; i < failDeliveryList.Count; i++)
            {
                failDeliveryList[i].Index = i + 1;
            }

            if (failDeliveryList.Count == 0)
            {
                FailDeliveryEntity emptyEntity = new FailDeliveryEntity()
                {
                    ShipDate = DateTime.Now,
                    SupplierStation = "查無紀錄",
                    ArriveStation = "查無紀錄",
                    CheckNumber = "查無紀錄",
                    SupplierPieces = 0,
                    CustomerCode = "查無紀錄"
                };
                failDeliveryList.Add(emptyEntity);
            }

            return failDeliveryList;
        }

        private string AbnormalRemarkInterpreter(int abnormalRemarks)        //從異常代碼轉為中文
        {
            switch (abnormalRemarks)
            {
                case 0:
                    return "未選擇";
                case 1:
                    return "貨件多出";
            }

            return "異常狀態錯誤";
        }

        private int AbnormalRemarkTextInterpreter(string abnormalRemarksText)        //從中文轉為代碼
        {
            switch (abnormalRemarksText)
            {
                case "未選擇":
                    return 0;
                case "貨件多出":
                    return 1;
            }

            return -1;
        }

        public List<StationEntity> GetAllStations()
        {
            List<StationEntity> stationList = FailDeliveryRepository.GetAllStations();
            return stationList;
        }

        public List<CustomerShortNameEntity> GetAllCustomers()
        {
            List<CustomerShortNameEntity> customerList = FailDeliveryRepository.GetCustomerShortNameEntities();
            return customerList;
        }
        public string ChangeDateTimeForm(DateTime dateTime)     //將日期轉換為yyyy/MM/dd格式的string
        {
            string year = dateTime.ToString().Split(" ")[0].Split("/")[0];
            string month = ChangeIntFormat(dateTime.ToString().Split(" ")[0].Split("/")[1]);
            string day = ChangeIntFormat(dateTime.ToString().Split(" ")[0].Split("/")[2]);


            string result = year + month + day;
            return result;
        }

        public string ChangeIntFormat(string i)
        {
            if (int.Parse(i) < 10)
            {
                return "0" + i;
            }
            return i;
        }

        public List<CustomerDownListEntity> GetCustomerByStation(string station)
        {
            List<TbCustomer> customerRepositoryEntities = FailDeliveryRepository.GetCustomersByStation(station);
            List<CustomerDownListEntity> customerEntities = this.Mapper.Map<List<CustomerDownListEntity>>(customerRepositoryEntities);
            return customerEntities;
        }

        private string GetAccountCodeAndStationInString(string str)
        {
            int WhereIsTheF = str.IndexOf("F");
            string result = "";
            if (WhereIsTheF != -1)
            {
                str = str.Substring(WhereIsTheF);
                List<string> tempStringList = new List<string>();
                for (var i = 0; i < str.Length; i++)
                {
                    tempStringList.Add(str.Substring(i, 1));
                }
                byte[] bytesArray = Encoding.Default.GetBytes(str);

                for (var i = 0; i < bytesArray.Length; i++)
                {
                    if ((bytesArray[i] > 47 & bytesArray[i] < 58) || bytesArray[i] == 70)                 //70為F，判定式前面為數字
                    {
                        result += tempStringList[i];
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return result;
        }
    }
}